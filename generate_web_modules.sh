#!/bin/sh
set -e
npx ttsc -b

(cd LIB_infrastructure && npx snowpack --dest ../dist/web_modules)
(cd LIB_design-system && npx snowpack --dest ../dist/web_modules)
(cd WEB_admin && npx snowpack --dest ../dist/web_modules)
(cd 3rd-party && npx webpack-cli)
npx json-merger ./import-config.json dist/web_modules/import-map.json --pretty --output dist/web_modules/import-map.json
npx babel dist -d dist