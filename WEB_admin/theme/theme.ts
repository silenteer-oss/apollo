import originStyled, {
  css as originCss,
  createGlobalStyle as originCreateGlobalStyle,
  withTheme as originWithTheme,
  ThemeProvider as OriginThemeProvider,
  ThemeConsumer as OriginThemeConsumer,
  keyframes
} from 'styled-components';

import type {
  ThemedBaseStyledInterface,
  BaseThemedCssFunction,
  BaseWithThemeFnInterface,
  ThemeProviderProps,
  ThemedStyledProps,
  Interpolation,
  CSSObject,
  InterpolationFunction,
  GlobalStyleComponent,
} from 'styled-components';


import {
  BaseDefaultTheme
} from '@infrastructure/themes/default';
import {
  IDefaultTheme
} from '@infrastructure/themes/default';

export { getAllComponentTheme } from '@infrastructure/themes/default';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IViewsTheme { }

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IAdminTheme extends IDefaultTheme<IViewsTheme> { }

export class AdminTheme extends BaseDefaultTheme<IViewsTheme> { }

// -------------------------------------------------------------------- //

export function getAllViewTheme(theme: IAdminTheme): IViewsTheme {
  return {};
}

// -------------------------------------------------------------------- //

const styled = originStyled as ThemedBaseStyledInterface<IAdminTheme>;
const css = originCss as BaseThemedCssFunction<IAdminTheme>;
const withTheme = originWithTheme as BaseWithThemeFnInterface<IAdminTheme>;
const createGlobalStyle = originCreateGlobalStyle as <P extends object = {}>(
  first:
    | TemplateStringsArray
    | CSSObject
    | InterpolationFunction<ThemedStyledProps<P, IAdminTheme>>,
  ...interpolations: Array<Interpolation<ThemedStyledProps<P, IAdminTheme>>>
) => GlobalStyleComponent<P, IAdminTheme>;
const ThemeProvider = OriginThemeProvider as React.ComponentClass<
  ThemeProviderProps<IAdminTheme, IAdminTheme>,
  any
>;
const ThemeConsumer = OriginThemeConsumer as React.ExoticComponent<
  React.ConsumerProps<IAdminTheme>
>;

export {
  styled,
  css,
  createGlobalStyle,
  keyframes,
  withTheme,
  ThemeProvider,
  ThemeConsumer,
};

export const theme = new AdminTheme();
