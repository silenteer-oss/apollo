import React from 'react';
import ReactDOM from 'react-dom';
import RootPage from './pages/root/root-page.styled';
import { theme, getAllViewTheme, getAllComponentTheme } from '@admin/theme';

// import '@admin/assets/styles/index.scss';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

ReactDOM.render(<RootPage theme={theme} />, document.getElementById('root'));
