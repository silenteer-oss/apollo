import React from 'react';
import { Flex } from '@design-system/core/components';
import { hashPassword } from '@admin/common/webworker';
import { MicroEmployeeModule } from '../../micro-modules';

export function EmployeePage(props: { className?: string }) {
  const { className } = props;
  return (
    <Flex auto className={className} my="1rem" mx="2rem">
      <MicroEmployeeModule
        hashPassword={hashPassword}
        fallback={<div className="loader" />}
      />
    </Flex>
  );
}
