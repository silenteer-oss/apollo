import { EmployeePage } from './user.page';
import styled from 'styled-components';

export default styled(EmployeePage)`
  position: relative;
  > div {
    display: flex;
    flex: 1;
  }
`;
