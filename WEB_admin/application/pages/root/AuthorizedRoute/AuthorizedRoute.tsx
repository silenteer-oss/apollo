import React from 'react';
import { RouteProps, Route, Redirect } from 'react-router-dom';
import { IAuthorizedRouteProps, IAuthorizedRouteState } from './models';
import ApplicationService from '../../../services/ApplicationService';

export class AuthorizedRoute extends React.PureComponent<
  IAuthorizedRouteProps & RouteProps,
  IAuthorizedRouteState
> {
  state: IAuthorizedRouteState = {
    hasAlreadyBackupAdmin: undefined,
  };

  componentDidMount() {
    ApplicationService.checkAdminBackupStatus().then(data => {
      this.setState({ hasAlreadyBackupAdmin: data });
    });
  }

  render() {
    const { path } = this.props;
    const { hasAlreadyBackupAdmin } = this.state;

    if (hasAlreadyBackupAdmin === undefined) {
      return null;
    }

    let redirect = path;

    if (!hasAlreadyBackupAdmin) {
      redirect = '/backup';
    }

    if (redirect === path) {
      return <Route {...this.props} />;
    }

    return <Redirect path={path} to={redirect} />;
  }
}
