export interface IAuthorizedRouteProps {
  path: string;
}

export interface IAuthorizedRouteState {
  hasAlreadyBackupAdmin: boolean;
}
