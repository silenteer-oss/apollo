import React from 'react';
import {
  Redirect,
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import { Dialog, Toaster } from '@blueprintjs/core';
import { initWorker, terminateWorker } from '@admin/common/webworker';
import {
  lazyComponent,
  ErrorBoundary,
  EnsureSupportedBrowser,
} from '@design-system/core/components';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { setupBrowser } from '@infrastructure/browser';
import {
  ThemeProvider,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
} from '@admin/theme';
import {
  IAdminTheme
} from '@admin/theme';
import ApplicationService from '../../services/ApplicationService';
import HomePage from '../home/home-page.styled';
import { AuthorizedRoute } from '@admin/application/pages/root/AuthorizedRoute';

const LazyLoginPage = lazyComponent(() => import('../auth/auth.styled'));
const BackupAdminView = lazyComponent(() => import('../backup/backup-admin.styled'));

interface RootPageProps {
  className?: string;
  theme?: IAdminTheme;
}

interface RootPageState {
  configs?: {
    [key: string]: object;
  };
  user?: {
    careProviderId: string;
  };
}

export class RootPage extends React.PureComponent<
  RootPageProps,
  RootPageState
  > {
  private _apiErrorRedirect = {
    401: '/login',
    403: '/login',
  };

  state: RootPageState = {};

  constructor(props: RootPageProps) {
    super(props);

    initWorker();

    ApplicationService.init().then(data => this.setState({ ...data }));
  }

  componentWillUnmount() {
    terminateWorker();
  }

  render() {
    const { className, theme } = this.props;
    const { configs, user } = this.state;

    if (!configs || !user) {
      return null;
    }

    return (
      <div className={className}>
        <ThemeProvider theme={theme}>
          <GlobalStyleContextProvider
            styles={
              {
                // main: mixGlobalStyle(theme),
                // blueprint: mixCustomBlueprintStyle(theme),
              }
            }
          >
            <ErrorBoundary
              user={user}
              sentry={configs.sentry}
              Dialog={Dialog}
              Toaster={Toaster}
              apiErrorRedirect={this._apiErrorRedirect}
            >
              <EnsureSupportedBrowser
                features={['indexDB', 'storageManager']}
                unsupportedBrowserMessages={{
                  title: 'Unsupported browser',
                  content:
                    'In order to use full features from application, you should use latest version of Chrome or Firefox',
                }}
                deniedPermissionMessages={{
                  title: 'Denied permission',
                  content:
                    'Try to reload browser and accept permission from popup.',
                }}
                onRequestPermission={setupBrowser}
              >
                <Router>
                  <Switch>
                    <AuthorizedRoute path="/app" component={HomePage} />
                    <Route path="/backup" component={BackupAdminView} />
                    <Route path="/login" component={LazyLoginPage} />
                    <Redirect exact path="/" to="/app" />
                  </Switch>
                </Router>
              </EnsureSupportedBrowser>
            </ErrorBoundary>
          </GlobalStyleContextProvider>
        </ThemeProvider>
      </div>
    );
  }
}
