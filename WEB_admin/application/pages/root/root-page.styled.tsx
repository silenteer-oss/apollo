import { RootPage } from './root-page.page';
import styled from 'styled-components';

export default styled(RootPage)`
  width: 100%;
  height: 100%;
`;
