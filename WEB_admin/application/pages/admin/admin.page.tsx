import React from 'react';
import { Flex } from '@design-system/core/components';
import { MicroAdminModule } from '../../micro-modules';

interface AdminPropsModule {
  className?: string;
}

export function AdminPage(props: AdminPropsModule) {
  const { className } = props;
  return (
    <Flex className={className} p="2rem">
      <MicroAdminModule />
    </Flex>
  );
}
