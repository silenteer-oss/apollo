import { Flex } from '@design-system/core/components';
import React from 'react';
import { Redirect, Route, Switch, RouteProps } from 'react-router-dom';
import { lazyComponent } from '@design-system/core/components';
import HeaderView from '../../components/header.styled';
import SidebarView from '../../components/sidebar.styled';

const CareProviderDeviceList = lazyComponent(() =>
  import('../care-provider-device/CareProviderDeviceStyled')
);
const AdminView = lazyComponent(() => import('../admin/admin.styled'));
const EmployeeView = lazyComponent(() => import('../user/user.styled'));

interface HomePageProps extends RouteProps {
  className?: string;
}

function HomePage(props: HomePageProps) {
  const { className } = props;

  return (
    <div className={className}>
      <div className="sl-layout">
        <HeaderView className="sl-navbar" />
        <div className="sl-sidebar">
          <SidebarView />
        </div>
        <Flex auto className="sl-content">
          <Switch>
            <Route path="/app/user" component={EmployeeView} />
            <Route path="/app/admin" component={AdminView} />
            <Route path="/app/devices" component={CareProviderDeviceList} />
            <Redirect path="/app" to="/app/user" />
          </Switch>
        </Flex>
      </div>
    </div>
  );
}

export { HomePage };
