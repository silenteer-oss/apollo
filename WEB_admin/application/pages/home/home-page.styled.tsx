import { HomePage } from './home-page.page';
import styled from 'styled-components';

export default styled(HomePage)`
  .sl-navbar {
    grid-area: navbar;
    height: 70px;
  }

  .sl-sidebar {
    grid-area: sidebar;
    /* width: 300px; */
  }

  .sl-content {
    grid-area: main;
    /* display: flex; */
  }

  .sl-layout {
    display: grid;
    height: 100vh;
    grid-template-columns: 300px minmax(300px, auto);
    grid-template-rows: 70px;
    grid-template-areas:
      'navbar navbar navbar navbar'
      'sidebar main main main';
  }
  .bp3-vertical,
  .bp3-tab-panel {
    width: 100%;
  }
  div[role='tablist'] {
    width: 20rem;
    border-right: 1px solid #ebf1f5;
  }
  .bp3-tab-indicator-wrapper,
  div[role='tab'] {
    height: 60px !important;
    display: flex;
    align-items: center;
  }
`;
