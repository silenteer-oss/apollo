import { AuthenticationPage } from './auth.page';
import styled from 'styled-components';

export default styled(AuthenticationPage)`
  .sl-login-box {
    width: 30rem;
    align-self: center;
    input,
    .ls-code-label {
      text-align: center;
      font-size: 2rem !important;
      letter-spacing: 1.4px;
      margin-top: 1rem;
      &::placeholder {
        font-size: 16px;
        text-transform: none;
        margin-top: 0px;
        text-align: center;
      }
    }

    button {
      margin-top: 1rem;
    }
  }
`;
