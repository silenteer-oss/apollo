import { Flex } from '@design-system/core/components';
import * as React from 'react';
import { MicroAuthenticationModule } from '../../micro-modules';

function AuthenticationPage(props: {className ?: string}) {
  const { className } = props;
  return (
    <Flex className={className} my="1rem" mx="2rem" auto justify="center">
      <MicroAuthenticationModule />
    </Flex>
  );
}

export { AuthenticationPage };
