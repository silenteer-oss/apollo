import React from 'react';
import { Flex } from '@design-system/core/components';
import { MicroBackupAdminModule } from '../../micro-modules';

interface BackupAdminPropsModule {
  className?: string;
}

export function BackupAdminPage(props: BackupAdminPropsModule) {
  const { className } = props;
  return (
    <Flex className={className} my="1rem" mx="2rem" auto justify="center">
      <MicroBackupAdminModule />
    </Flex>
  );
}
