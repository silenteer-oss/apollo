import * as React from 'react';
import { MicroDeviceModule } from '../../micro-modules';
import { Flex } from '@design-system/core/components';

type CareProviderDeviceListPageProps = {
  className?: string
}

function CareProviderDeviceListPage(props: CareProviderDeviceListPageProps) {
  const { className } = props;
  return (
    <Flex className={className} my="1rem" mx="2rem" auto>
      <MicroDeviceModule fallback={<div className="loader" />} />
    </Flex>
  );
}

export { CareProviderDeviceListPage };
