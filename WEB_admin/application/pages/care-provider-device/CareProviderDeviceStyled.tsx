import * as React from 'react';
import { CareProviderDeviceListPage } from './CareProviderDevicePage';
import styled from 'styled-components';

const CareProviderDeviceListStyled = styled(CareProviderDeviceListPage)`
  position: relative;
`;
export default CareProviderDeviceListStyled;
