import React from 'react';
import { i18n } from '../i18n';

interface HeaderProps {
  className?: string;
  trusting?: boolean;
}

function HeaderView(
  props: HeaderProps
): React.FunctionComponentElement<HeaderProps> {
  const { className } = props;

  return (
    <nav className={`bp3-navbar ${className}`}>
      <div className="bp3-navbar-group bp3-align-left">
        <div className="bp3-navbar-heading">{i18n.vi.CARE_PROVIDER}</div>
      </div>
    </nav>
  );
}

export { HeaderView };
