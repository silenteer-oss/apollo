import { SidebarView } from './sidebar.view';
import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';

export default styled(SidebarView)`
  margin-left: 2rem;
  margin-top: 1rem;
  button {
    font-size: 14px !important;
    height: 24px !important;
    min-height: 32px !important;
  }
  .bp3-intent-primary {
    background-color: ${Colors.BLUE4} !important;
    border: none !important;
    box-shadow: none !important;
  }
`;
