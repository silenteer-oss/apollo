import styled from 'styled-components';
import { HeaderView } from './header.view';
import { Colors } from '@blueprintjs/core';

export default styled(HeaderView)`
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${Colors.BLUE1};
  color: white;
  .bp3-button[class*='bp3-icon-']::before {
    color: white;
  }
`;
