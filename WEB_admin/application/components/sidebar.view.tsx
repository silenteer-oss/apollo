import { Alignment, Button, Intent } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { i18n } from '../i18n';

function Sidebar(props: RouteComponentProps & { className?: string }) {
  const { className, history } = props;
  const gotoPractuceUserPage = () => history.push('/app/user');
  const gotoDevicePage = () => history.push('/app/devices');
  // const gotoAdminPAge = () => history.push('/app/admin');

  const getIntent = (type: string) => {
    return location.pathname.includes(type) ? Intent.PRIMARY : Intent.NONE;
  };

  const getMinimal = (type: string) => {
    return !location.pathname.includes(type);
  };

  return (
    <div className={className}>
      <Flex column>
        {/* <Button
          large
          alignText={Alignment.LEFT}
          minimal={getMinimal('admin')}
          text="Administrator"
          onClick={gotoAdminPAge}
          intent={getIntent('admin')}
        /> */}
        <Button
          large
          alignText={Alignment.LEFT}
          text={i18n.vi.sidebar.EMPLOYEE_TITLE}
          onClick={gotoPractuceUserPage}
          minimal={getMinimal('user')}
          intent={getIntent('user')}
        />
        <Button
          large
          alignText={Alignment.LEFT}
          text={i18n.vi.sidebar.DEVICE_TITLE}
          onClick={gotoDevicePage}
          minimal={getMinimal('device')}
          intent={getIntent('device')}
        />
      </Flex>
    </div>
  );
}

const SidebarView = withRouter(Sidebar);
export { SidebarView };
