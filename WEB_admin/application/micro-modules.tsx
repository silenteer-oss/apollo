import { loadMicroModuleView } from '@design-system/core/components';
import { AdminViewProps } from '@admin/module_admin';
import { ADMIN_VIEW_ID } from '@admin/module_admin/module-config';
import { AuthenticationMainViewProps } from '@admin/module_authentication';
import { AUTHENTICATION_VIEW_ID } from '@admin/module_authentication/module-config';
import { EmployeeProps } from '@admin/module_employee';
import { EMPLOYEE_VIEW_ID } from '@admin/module_employee/module-config';
import { TrustedCareProviderDeviceProps } from '@admin/module_trusted-care-provider-device';
import { TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID } from '@admin/module_trusted-care-provider-device/module-config';
import { getApplicationConfig } from './application-config';
import { BackupAdminViewProps } from '@admin/module_backup/views/SecurityQuestionInfoModel';
import { BACKUP_ADMIN_VIEW_ID } from '@admin/module_backup/module-config';
import { withTheme } from '@admin/theme';
import React from 'react';

const {
  modules: {
    AdminModule,
    authentication,
    BackupAdminModule,
    EmployeeModule,
    TrustedCareProviderDeviceModule,
  },
} = getApplicationConfig();

export const MicroAdminModule: any = loadMicroModuleView<AdminViewProps>({
  url: AdminModule.publicAssetPath,
  module: 'admin',
  view: AdminModule.views[ADMIN_VIEW_ID],
  app: "WEB_admin"
});

export const MicroBackupAdminModule: any = withTheme<
  React.FunctionComponent<BackupAdminViewProps>
>(props => {
  const View = loadMicroModuleView<BackupAdminViewProps>({
    url: BackupAdminModule.publicAssetPath,
    module: 'backup',
    view: BackupAdminModule.views[BACKUP_ADMIN_VIEW_ID],
    app: "WEB_admin"
  });
  return <View {...props} />;
});

export const MicroAuthenticationModule: any = loadMicroModuleView<
  AuthenticationMainViewProps
>({
  url: authentication.publicAssetPath,
  module: authentication.id,
  view: authentication.views[AUTHENTICATION_VIEW_ID],
  app: "WEB_admin"
});

export const MicroEmployeeModule: any = loadMicroModuleView<EmployeeProps>({
  url: EmployeeModule.publicAssetPath,
  module: 'employee',
  view: EmployeeModule.views[EMPLOYEE_VIEW_ID],

  app: "WEB_admin"
});

export const MicroDeviceModule: any = loadMicroModuleView<
  TrustedCareProviderDeviceProps
>({
  url: TrustedCareProviderDeviceModule.publicAssetPath,
  module: 'trusted-care-provider-device',
  app: "WEB_admin",
  view:
    TrustedCareProviderDeviceModule.views[TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID],
});
