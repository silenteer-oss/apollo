import { AdminApi } from '@infrastructure/resource/AdminResource';
import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';

function loadAppConfig() {
  return AdminApi.getAllConfigs().then(result => result.data);
}

function loadCareProviderInfo() {
  return AdminApi.getMyCareProviderInfo().then(result => result.data);
}

function checkAdminBackupStatus() {
  return SysadminApi.verifyBackupSucceed().then(result => result.data);
}

function init() {
  return Promise.all([loadAppConfig(), loadCareProviderInfo()])
    .then(([configs, careProviderInfo]) => ({
      configs,
      user: {
        careProviderId: careProviderInfo.careProviderId,
      },
    }))
    .catch(ex => {
      console.error(ex);
      return {
        configs: {} as any,
        user: {} as any,
      };
    });
}

export default {
  init,
  checkAdminBackupStatus,
};
