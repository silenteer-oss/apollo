export const i18n = {
  vi: {
    CARE_PROVIDER: 'Quản lý Bệnh viện',
    sidebar: {
      EMPLOYEE_TITLE: 'Tài khoản',
      DEVICE_TITLE: 'Thiết bị',
    },
  },
  en: {
    CARE_PROVIDER: 'Care Provider',
    sidebar: {
      EMPLOYEE_TITLE: 'Users',
      DEVICE_TITLE: 'Trusted devices',
    },
  },
};
