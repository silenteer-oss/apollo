import type {
  IApplicationConfig,
  IModuleConfig,
} from '@design-system/infrastructure/configs/environment';

import type {
  IModuleViews as IAdminModuleViews
} from '@admin/module_admin/module-config';

import {
  ADMIN_MODULE_ID,
  getModuleConfig as getAdminModuleConfig
} from '@admin/module_admin/module-config';

import type {
  IModuleViews as IAuthenticationModuleViews,
} from '@admin/module_authentication/module-config';

import {
  AUTHENTICATION_MODULE_ID,
  getModuleConfig as getAuthenticationModuleConfig
} from '@admin/module_authentication/module-config';

import type {
  IModuleViews as IBackupAdminModuleViews
} from '@admin/module_backup/module-config';

import {
  BACKUP_ADMIN_MODULE_ID,
  getModuleConfig as getBackupAdminModuleConfig
} from '@admin/module_backup/module-config';

import type {
  IModuleViews as IEmployeeModuleViews
} from '@admin/module_employee/module-config';

import {
  EMPLOYEE_MODULE_ID,
  getModuleConfig as getEmployeeModuleConfig
} from '@admin/module_employee/module-config';

import {
  TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID,
  getModuleConfig as getTrustedCareProviderDeviceModuleConfig
} from '@admin/module_trusted-care-provider-device/module-config';

import type {
  IModuleViews as ITrustedCareProviderDeviceModuleViews,

} from '@admin/module_trusted-care-provider-device/module-config';

export interface IApplicationModules {
  [ADMIN_MODULE_ID]: IModuleConfig<IAdminModuleViews>;
  [AUTHENTICATION_MODULE_ID]: IModuleConfig<IAuthenticationModuleViews>;
  [BACKUP_ADMIN_MODULE_ID]: IModuleConfig<IBackupAdminModuleViews>;
  [EMPLOYEE_MODULE_ID]: IModuleConfig<IEmployeeModuleViews>;
  [TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID]: IModuleConfig<
    ITrustedCareProviderDeviceModuleViews
  >;
}

export function getApplicationConfig(): IApplicationConfig<
  IApplicationModules
> {
  return {
    id: 'AdminApplication',
    name: 'Admin Application',
    host: '0.0.0.0',
    port: 4000,
    publicAssetPath: `/`,
    modules: {
      [ADMIN_MODULE_ID]: getAdminModuleConfig(),
      [AUTHENTICATION_MODULE_ID]: getAuthenticationModuleConfig(),
      [BACKUP_ADMIN_MODULE_ID]: getBackupAdminModuleConfig(),
      [EMPLOYEE_MODULE_ID]: getEmployeeModuleConfig(),
      [TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID]: getTrustedCareProviderDeviceModuleConfig(),
    },
  };
}
