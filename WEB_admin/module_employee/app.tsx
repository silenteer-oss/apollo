import { Button, Dialog } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import employeeService, { Employee } from './EmployeeService';
import React, { useEffect } from 'react';
import UserListView from './views/EmployeeListStyled';
import UserView from './views/EmployeeStyled';
import { i18n } from './i18n';

export interface EmployeeContext {
  users: Employee[];
  loading: boolean;
  onCreate?: (user: Employee) => void;
  onDelete?: (user: Employee) => void;
}

export interface EmployeeProps {
  hashPassword: (plainPassword: string, salt: string) => Promise<string>;
}

export const EmployeeContext = React.createContext<EmployeeContext>(
  {} as EmployeeContext
);

function AppView(
  props: EmployeeProps
): React.FunctionComponentElement<{
  hashPassword: (plainPassword: string, salt: string) => Promise<string>;
}> {
  const { hashPassword } = props;
  const [open, setOpenState] = React.useState(false);
  const [users, setEmployeeList] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const onCloseDialog = () => setOpenState(false);
  const onOpenDialog = () => setOpenState(true);

  const onCreate = (user: Employee) => {
    setEmployeeList([...users, user]);
    onCloseDialog();
  };

  const onDelete = (user: Employee) => {
    setEmployeeList(users.filter(usr => usr.id !== user.id));
  };

  useEffect(() => {
    employeeService.getListUserInfo().then(userList => {
      setEmployeeList(userList);
      setLoading(false);
    });
  }, []);

  return (
    <EmployeeContext.Provider value={{ users, onCreate, onDelete, loading }}>
      <Flex column>
        <Flex justify="flex-end">
          <Button
            className="bp3-button"
            onClick={onOpenDialog}
            intent="primary"
          >
            {i18n.vi.ADD_USER}
          </Button>
        </Flex>
        <Flex auto pt="0.6rem">
          <UserListView hashPassword={hashPassword} />
        </Flex>
        <Dialog
          isOpen={open}
          onClose={onCloseDialog}
          title={i18n.vi.ADD_USER}
          canOutsideClickClose={false}
        >
          <UserView hashPassword={hashPassword} />
        </Dialog>
      </Flex>
    </EmployeeContext.Provider>
  );
}

export { AppView };
