import {
  AdminApi,
  AdminApiModel,
} from '@infrastructure/resource/AdminResource';
import {
  ProfileApi,
  ProfileApiModel,
} from '@infrastructure/resource/ProfileResource';
import { AxiosResponse } from 'axios';

export default {
  createUser: (
    user: AdminApiModel.UpsertEmployeeRequest
  ): Promise<AdminApiModel.EmployeeDto> =>
    AdminApi.createEmployee(user).then(({ data }) => data),

  getUserList: (): Promise<AdminApiModel.EmployeeDto[]> =>
    AdminApi.getAll().then(({ data }) => data),

  deleteUser: (userId: string): Promise<string> =>
    AdminApi.deleteById(userId).then(({ data }) => data),

  getUserListInfo: (
    ids: string[]
  ): Promise<ProfileApiModel.EmployeeProfileResponse[]> =>
    ProfileApi.getEmployeeProfileByIds({ originalIds: ids }).then(
      ({ data }) => data
    ),
  resetEmployeePassword: (
    userId: string,
    resetPasswordRequest: AdminApiModel.EmployeeResetPasswordRequest
  ): Promise<AxiosResponse<void>> =>
    AdminApi.resetPassword(userId, resetPasswordRequest),
  getPasswordSeed: (accountId: string): Promise<string> =>
    AdminApi.getSeed(accountId).then(({ data }) => data),
};
