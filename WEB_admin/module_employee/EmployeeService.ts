import { getRandomBytes } from '@infrastructure/crypto';
import employeeResource from './EmployeeResource';
import { AdminApiModel } from '@infrastructure/resource/AdminResource';
import { AxiosResponse } from 'axios';

export type EMPLOYEE_TYPE = AdminApiModel.UserType;

export interface Employee {
  id?: string;
  fullName: string;
  accountId?: string;
  careProvideId?: string;
  type: AdminApiModel.UserType;
  gender?: AdminApiModel.Gender;
  mobileNumber?: number;
  dob?: number;
  telephoneNumber?: number;
  address?: string;
  email?: string;
  userId?: string;
  password?: string;
}

function getUserList() {
  return employeeResource.getUserList();
}

async function createUser(
  user: Employee,
  hashPasswordHandler: (plainPassword: string, salt: string) => Promise<string>
): Promise<Employee> {
  const seed = await getRandomBytes(16);
  const hashedPassword = await hashPasswordHandler(user.password, seed);
  const postedUser: AdminApiModel.UpsertEmployeeRequest = {
    ...user,
    password: hashedPassword,
    seed,
    type: user.type,
  };

  return employeeResource.createUser(postedUser).then(newUser => ({
    ...newUser,
    fullName: user.fullName,
  }));
}

function deleteUser(userId: string) {
  return employeeResource.deleteUser(userId);
}

function getListUserInfo(): Promise<Employee[]> {
  return getUserList().then(employees => {
    const ids = employees.map(user => user.id);
    return employeeResource.getUserListInfo(ids).then(identityUsers => {
      return employees.map(user => {
        const identityUser = identityUsers.find(iusr => iusr.id === user.id);
        return { ...user, ...identityUser };
      });
    });
  });
}

async function resetEmployeePassword(
  userId: string,
  accountId: string,
  newPassword: string,
  hashPasswordHandler: (plainPassword: string, salt: string) => Promise<string>
): Promise<AxiosResponse<void>> {
  const seed = await employeeResource.getPasswordSeed(accountId);
  const hashedPassword = await hashPasswordHandler(newPassword, seed);
  const payload: AdminApiModel.EmployeeResetPasswordRequest = {
    newPassword: hashedPassword,
  };
  return employeeResource.resetEmployeePassword(userId, payload);
}

export default {
  getUserList,
  createUser,
  deleteUser,
  getListUserInfo,
  resetEmployeePassword,
};
