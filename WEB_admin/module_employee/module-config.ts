import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const EMPLOYEE_MODULE_ID = 'EmployeeModule';

export const EMPLOYEE_VIEW_ID = 'EmployeeView';

export interface IModuleViews {
  [EMPLOYEE_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: EMPLOYEE_MODULE_ID,
    name: 'Employee Module',
    publicAssetPath: `/module/employee`,
    views: {
      EmployeeView: EMPLOYEE_VIEW_ID,
    },
  };
}
