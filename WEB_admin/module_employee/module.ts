import { EMPLOYEE_MODULE_ID, EMPLOYEE_VIEW_ID } from './module-config';
import { AppView } from './app';

export default {
  [EMPLOYEE_MODULE_ID]: {
    [EMPLOYEE_VIEW_ID]: AppView,
  },
};
