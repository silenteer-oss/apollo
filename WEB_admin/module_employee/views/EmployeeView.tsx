import { Classes } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React, { useContext, useState } from 'react';
import { EmployeeContext } from '../app';
import employeeService, { Employee, EMPLOYEE_TYPE } from '../EmployeeService';
import { AdminApiModel } from '@infrastructure/resource/AdminResource';
import { i18n } from '../i18n';

export interface EmployeeProps {
  className?: string;
  hashPassword: (plainPassword: string, salt: string) => Promise<string>;
}

function EmployeeView(
  props: EmployeeProps
): React.FunctionComponentElement<EmployeeProps> {
  const { className, hashPassword } = props;
  const [fullName, setName] = useState('');
  const [role, setRole] = useState(AdminApiModel.UserType.DOCTOR);
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const context = useContext(EmployeeContext);

  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setName(event.target.value);
  };

  const onRoleChange = (event: React.FormEvent<HTMLFieldSetElement>) => {
    event.persist();
    setRole((event.target as HTMLInputElement).value as EMPLOYEE_TYPE);
  };

  const onPasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setPassword(event.target.value);
  };

  const _onCreate = async () => {
    const newUser: Employee = {
      type: role,
      fullName,
      password,
    };
    setLoading(true);
    const responseUser = await employeeService.createUser(
      newUser,
      hashPassword
    );
    context.onCreate({ ...responseUser, type: role });
    setLoading(false);
  };

  return (
    <div className={className}>
      <div className={Classes.DIALOG_BODY}>
        <input
          autoFocus
          autoComplete="off"
          placeholder={i18n.vi.dialog.FULL_NAME_PLACE_HOLDER}
          className="bp3-input bp3-large bp3-fill"
          onChange={onNameChange}
          value={fullName}
        />
        <input
          autoComplete="off"
          type="password"
          placeholder={i18n.vi.dialog.PASSWORD_PLACE_HOLDER}
          className="bp3-input bp3-large bp3-fill"
          onChange={onPasswordChange}
          value={password}
        />

        <fieldset id="type" onChange={onRoleChange} defaultValue={role}>
          <Flex>
            <label className="bp3-control bp3-radio">
              <input
                type="radio"
                name="type"
                value="DOCTOR"
                checked={role === AdminApiModel.UserType.DOCTOR}
              />
              <span className="bp3-control-indicator" />
              {i18n.vi.TYPE_DOCTOR}
            </label>
            <label className="bp3-control bp3-radio">
              <input
                type="radio"
                name="type"
                value="NURSE"
                checked={role === AdminApiModel.UserType.NURSE}
              />
              <span className="bp3-control-indicator" />
              {i18n.vi.TYPE_NURSE}
            </label>
            <label className="bp3-control bp3-radio">
              <input
                type="radio"
                name="type"
                value="RECEPTIONIST"
                checked={role === AdminApiModel.UserType.RECEPTIONIST}
              />
              <span className="bp3-control-indicator" />
              {i18n.vi.TYPE_RECEPTIONIST}
            </label>
          </Flex>
        </fieldset>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <button
          type="button"
          onClick={_onCreate}
          className="bp3-button bp3-large bp3-intent-primary bp3-fill"
          disabled={loading || !fullName.trim() || !password.trim()}
        >
          {loading
            ? i18n.vi.dialog.ACTION_CREATING
            : i18n.vi.dialog.ACTION_CREATE}
        </button>
      </div>
    </div>
  );
}

export { EmployeeView };
