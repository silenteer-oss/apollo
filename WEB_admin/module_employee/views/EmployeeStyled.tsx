import styled from 'styled-components';
import { EmployeeView } from './EmployeeView';

export default styled(EmployeeView)`
  input + * {
    margin-top: 1rem;
  }
  fieldset {
    display: flex;
    margin: 0;
    padding: 0;
    border: none;
    padding-top: 16px;
    & label + * {
      margin-left: 8px;
    }
  }
`;
