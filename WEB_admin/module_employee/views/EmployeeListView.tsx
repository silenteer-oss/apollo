import { Alert, Button, Intent, Toaster } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import employeeService, { Employee, EMPLOYEE_TYPE } from '../EmployeeService';
import React, { useContext, useMemo, useState } from 'react';
import { EmployeeContext } from '../app';
import { useRef } from 'react';
import { i18n } from '../i18n';

interface EmployeeListProps {
  className?: string;
  hashPassword: (plainPassword: string, salt: string) => Promise<string>;
}

type CONFIRM_TYPE = 'remove' | 'reset';

function EmployeeList(
  props: EmployeeListProps
): React.FunctionComponentElement<EmployeeListProps> {
  const { className, hashPassword } = props;

  const [open, setConfirmState] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [confirmType, setConfirmType] = useState<CONFIRM_TYPE>('remove');
  const [randomPassword, setRandomPassword] = useState<string>('');
  const [selectedUser, setSelectedUser] = useState<Employee>({} as Employee);

  const toasterRef = useRef<Toaster>(null);
  const context = useContext(EmployeeContext);

  const _onDelete = (user: Employee) => () => {
    setConfirmType('remove');
    setSelectedUser(user);
    setConfirmState(true);
  };

  const _onResetPassword = (user: Employee) => () => {
    setConfirmType('reset');
    setSelectedUser(user);
    const randomStringPassword = Math.random()
      .toString(36)
      .substr(2, 12);
    setRandomPassword(randomStringPassword);
    setConfirmState(true);
  };

  const handleConfirmShowPassword = () => {
    setRandomPassword('');
    setShowPassword(false);
  };

  const handleMoveCancel = () => setConfirmState(false);
  const handleMoveConfirm = () => {
    if (confirmType === 'reset') {
      employeeService
        .resetEmployeePassword(
          selectedUser.id,
          selectedUser.accountId,
          randomPassword,
          hashPassword
        )
        .then(() => {
          setConfirmState(false);
          setShowPassword(true);
        });
    } else {
      employeeService.deleteUser(selectedUser.id).then(() => {
        setConfirmState(false);
        context.onDelete(selectedUser);
        toasterRef.current.show({ message: 'User was removed!' });
      });
    }
  };

  const getUserTypeLabel = (type: EMPLOYEE_TYPE) => {
    switch (type) {
      case 'DOCTOR':
        return i18n.vi.TYPE_DOCTOR;
      case 'NURSE':
        return i18n.vi.TYPE_NURSE;
      case 'RECEPTIONIST':
        return i18n.vi.TYPE_RECEPTIONIST;
      default:
        return '';
    }
  };

  return useMemo(
    () => (
      <Flex auto className={className} column>
        <Flex className="sl-user-ls-header">
          <Flex w="50%">{i18n.vi.table.HEADER_FULL_NAME}</Flex>
          <Flex w="20%" justify="space-between">
            {i18n.vi.table.HEADER_TYPE}
          </Flex>
          <Flex w="30%">{i18n.vi.table.HEADER_ACTION}</Flex>
        </Flex>
        {context.loading ? (
          <div className="loader" />
        ) : (
          context.users.map((user: Employee) => (
            <Flex key={user.id} className="sl-user-ls-row">
              <Flex w="50%">{user.fullName}</Flex>
              <Flex w="20%">{getUserTypeLabel(user.type)}</Flex>
              <Flex w="30%">
                <Button
                  intent={Intent.DANGER}
                  text={i18n.vi.table.ACTION_REMOVE}
                  small
                  onClick={_onDelete(user)}
                />
                <Button
                  style={{ marginLeft: 10 }}
                  intent={Intent.WARNING}
                  text={i18n.vi.table.ACTION_RESET_PASSWORD}
                  small
                  onClick={_onResetPassword(user)}
                />
              </Flex>
            </Flex>
          ))
        )}
        <Alert
          cancelButtonText={
            confirmType === 'reset'
              ? i18n.vi.RESET_PASSWORD_CANCEL
              : i18n.vi.REMOVE_USER_CANCEL
          }
          confirmButtonText={
            confirmType === 'reset'
              ? i18n.vi.RESET_PASSWORD_OK
              : i18n.vi.REMOVE_USER_OK
          }
          icon={confirmType === 'reset' ? 'key' : 'trash'}
          intent={confirmType === 'reset' ? Intent.WARNING : Intent.DANGER}
          isOpen={open}
          onCancel={handleMoveCancel}
          onConfirm={handleMoveConfirm}
        >
          <p>
            {confirmType === 'reset'
              ? `${i18n.vi.RESET_PASSWORD_CONFIRMATION}
              ${selectedUser.fullName} ?`
              : `${i18n.vi.REMOVE_USER_CONFIRMATION}
              ${selectedUser.fullName} ?`}
          </p>
        </Alert>

        <Alert
          confirmButtonText={i18n.vi.RESET_PASSWORD_CLOSE}
          icon="key"
          intent={Intent.SUCCESS}
          isOpen={showPassword}
          onConfirm={handleConfirmShowPassword}
        >
          <p>{`${i18n.vi.RESET_PASSWORD_INFORM} :`}</p>
          <h1>{randomPassword}</h1>
        </Alert>

        <Toaster ref={toasterRef} />
      </Flex>
    ),
    [context.users, open, showPassword, context.loading]
  );
}

export { EmployeeList };
