import React from 'react';
import ReactDOM from 'react-dom';
import {
  initWorker,
  terminateWorker,
  hashPassword,
} from '@admin/common/webworker';
import { AppView } from './app';

import '@admin/assets/styles/index.scss';
export class RootView extends React.PureComponent {
  constructor(props: Readonly<{}>) {
    super(props);

    initWorker();
  }

  componentWillUnmount() {
    terminateWorker();
  }

  render() {
    return <AppView hashPassword={hashPassword} />;
  }
}

ReactDOM.render(<RootView />, document.getElementById('root'));
