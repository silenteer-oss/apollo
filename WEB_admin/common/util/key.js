export const keyUtil = {
  arrayBufferToString: thing => {
    if (typeof thing == 'string') {
      return thing;
    }
    return new window['dcodeIO'].ByteBuffer.wrap(thing).toString('binary');
  },

  stringToArrayBuffer: thing => {
    if (thing === undefined) {
      return undefined;
    }

    if (thing === Object(thing)) {
      if (thing.__proto__ == new ArrayBuffer().__proto__) {
        return thing;
      }
    }

    let str;

    if (typeof thing == 'string') {
      str = thing;
    } else {
      throw new Error(
        'Tried to convert a non-string of type ' +
          typeof thing +
          ' to an array buffer'
      );
    }
    return new window['dcodeIO'].ByteBuffer.fromBinary(str).toArrayBuffer();
  },

  combineBuffer: (buffer1, buffer2) => {
    const tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
    tmp.set(new Uint8Array(buffer1), 0);
    tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
    return tmp.buffer;
  },

  combineVersion: buffer => {
    const tmp = new Uint8Array(buffer.byteLength + 1);
    tmp[0] = (3 << 4) | 3;
    tmp.set(new Uint8Array(buffer), 1);
    return tmp.buffer;
  },
};
