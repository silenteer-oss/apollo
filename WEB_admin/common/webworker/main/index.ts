import { initMainThread } from '@infrastructure/webworker/main';
import { hashPasswordHandler } from '@infrastructure/crypto/services/webworker/main';

export function initWorker() {
  const url: string = import.meta.url;
  const workerPath = new URL("../background/index.js", url);

  initMainThread({
    worker: new Worker(workerPath, {
      type: 'module',
    }),
    messageEventHandlers: [hashPasswordHandler],
  });
}
