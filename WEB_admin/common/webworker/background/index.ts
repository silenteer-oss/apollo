import { initBackgroundThread } from '@infrastructure/webworker/background';
import { hashPasswordHandler } from '@infrastructure/crypto/services/webworker/background';

initBackgroundThread(hashPasswordHandler);
