import { IDateFormatProps } from '@blueprintjs/datetime';

export const DATE_CONFIGS: IDateFormatProps = {
  formatDate: date => date.toLocaleDateString(),
  parseDate: str => new Date(str),
  placeholder: 'MM/DD/YYYY',
};
