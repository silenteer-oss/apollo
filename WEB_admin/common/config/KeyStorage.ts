import { generateSealKeyPair } from '@infrastructure/crypto';
import { cryptoDatabase } from '@admin/services/client-database';

export interface KeyPair {
  pubKey: string;
  privKey: string;
}

export interface SignedKeyPair {
  keyId: number;
  keyPair: KeyPair;
  signature: string;
}

export interface PreKeyPair {
  keyId: string;
  keyPair: KeyPair;
}

export interface SignalBundleKey {
  userId?: string;
  registrationId: string;
  identityKeyPair: KeyPair;
  signedPreKey: SignedKeyPair;
  preKeys: PreKeyPair[];
}

export async function createCareProviderKeyPair() {
  const careProviderKeyPair = await cryptoDatabase.keyPairStorage.get(
    'CARE_PROVIDER'
  );

  if (!careProviderKeyPair) {
    const sealKeyPair = await generateSealKeyPair();

    await cryptoDatabase.keyPairStorage.put({
      key: 'CARE_PROVIDER',
      value: sealKeyPair,
    });
  }
}
