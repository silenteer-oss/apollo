import { IToastProps, Intent } from '@blueprintjs/core';

export const CARE_PROVIDER_KEY_PAIR = 'care_provider_key_pair';
export const CARE_PROVIDER_SENDER_KEYS = 'care_provider_sender_keys';
export const CARE_PROVIDER_SIGNING_PRIVATE_KEY =
  'care_provider_signing_private_key';

export const UNKNOW_ERROR: IToastProps = {
  message: 'Oops!! Server error. Please try again',
  intent: Intent.DANGER,
  icon: 'error',
};
