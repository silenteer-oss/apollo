import React from 'react';

export interface FormProps {
  className?: string;
  style?: React.CSSProperties;
  children?: any;
  onSubmit?: (event: React.FormEvent<HTMLFormElement>) => void;
}

export function Form(props: FormProps) {
  const { children, className, onSubmit, ...rest } = props;
  return (
    <form className={className} {...rest} onSubmit={onSubmit}>
      {children}
    </form>
  );
}
