import React from 'react';
import { Flex } from '@design-system/core/components';

export interface FormItemProps {
  className?: string;
  label: string;
  component: JSX.Element;
}

export function FormItem(
  props: FormItemProps
): React.FunctionComponentElement<FormItemProps> {
  const { label, component, className } = props;
  return (
    <Flex column className={className}>
      <label className="sl-form-item-title">{label}</label>
      {component}
    </Flex>
  );
}
