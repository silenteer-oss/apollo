import { FormItem as OriginFormItem } from './field.view';
import styled from 'styled-components';

export const FormItem = styled(OriginFormItem)`
  .sl-form-item-title {
    font-weight: 600;
    margin-bottom: 4px;
  }
  fieldset > * + * {
    margin-left: 8px;
  }
`;
