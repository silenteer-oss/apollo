import { Form as OriginForm } from './form.view';
import styled from 'styled-components';

export const Form = styled(OriginForm)`
  padding: 1rem;
  fieldset {
    display: flex;
    margin: 0;
    padding: 0;
    border: none;
  }
  div[class^='FormItem-'] + *:not(button) {
    padding-top: 1rem;
  }
`;
