import React from 'react';
import { IIconProps, Icon } from '@blueprintjs/core';
import styled from 'styled-components';

interface SLIconProps extends IIconProps {
  cursor?: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

const IconContainer = styled.div`
  cursor: ${(props: { cursor: string }) => props.cursor || 'auto'};
`;

function SLIcon(props: SLIconProps) {
  const { cursor, onClick, ...rest } = props;

  return (
    <IconContainer cursor={cursor} onClick={onClick}>
      <Icon {...rest} />
    </IconContainer>
  );
}

export { SLIcon };
