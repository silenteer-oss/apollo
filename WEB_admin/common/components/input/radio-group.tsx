import {
  IOptionProps,
  IRadioGroupProps,
  RadioGroup as BRadioGroup,
  Classes,
} from '@blueprintjs/core';
import React, { useState } from 'react';

type OptionProps = {
  name: string;
} & IOptionProps;

type RadioGroupProps = {
  defaultValue?: string | number;
  items: OptionProps[];
  onChange?: (value: string | number) => void;
} & Pick<IRadioGroupProps, Exclude<keyof IRadioGroupProps, 'onChange'>>;

function RadioGroup(
  props: RadioGroupProps
): React.FunctionComponentElement<RadioGroupProps> {
  const { defaultValue, items = [], inline, ...rest } = props;
  const [value, setValue] = useState(defaultValue);

  const onSelect = (event: React.FormEvent<HTMLInputElement>): void => {
    setValue(event.currentTarget.value);
  };

  return (
    <BRadioGroup {...rest} onChange={onSelect} selectedValue={value}>
      {items.map(radio => {
        const { label, ...inputProps } = radio;
        return (
          <label
            key={radio.label}
            className={`bp3-control bp3-radio ${inline && Classes.INLINE}`}
          >
            <input type="radio" {...inputProps} />
            <span className="bp3-control-indicator" />
            {radio.label}
          </label>
        );
      })}
    </BRadioGroup>
  );
}

export { RadioGroup };
