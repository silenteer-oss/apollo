import { SLTable } from './table';
import styled from 'styled-components';

export default styled(SLTable)`
  .bp3-table-cell,
  .bp3-table-row-name {
    height: 100%;
    display: flex;
    align-items: center;

    .bp3-table-row-name-text {
      text-align: center;
    }
  }
`;
