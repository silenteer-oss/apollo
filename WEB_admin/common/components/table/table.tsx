import { Cell, Column, ITableProps, Table } from '@blueprintjs/table';
import { Flex } from '@design-system/core/components';
import React from 'react';

interface SLTableProps extends ITableProps {
  dataSource: any[];
  className?: string;
}

const formatColumnName = (columnName: string): string => {
  return columnName
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, firstCharacter => firstCharacter.toUpperCase());
};

function SLTable(
  props: SLTableProps
): React.FunctionComponentElement<SLTableProps> {
  const { dataSource, ...rest } = props;

  if (dataSource.length === 0) {
    return (
      <Flex justify="center" auto>
        No data
      </Flex>
    );
  }

  const renderCells = (rowIndex: number, columnIndex: number): JSX.Element => {
    const dataRow = dataSource[rowIndex];
    return <Cell>{dataRow[Object.keys(dataRow)[columnIndex]]}</Cell>;
  };

  const renderColumns = (): JSX.Element[] => {
    const _columns = dataSource.map(dataRow => {
      return Object.keys(dataRow).map((fieldName, columnIndex: number) => (
        <Column
          key={columnIndex}
          name={formatColumnName(fieldName)}
          cellRenderer={renderCells}
        />
      ));
    });
    return _columns.flat();
  };

  return (
    <Table
      {...rest}
      numRows={dataSource.length}
      defaultRowHeight={30}
      defaultColumnWidth={150}
    >
      {renderColumns()}
    </Table>
  );
}

export { SLTable };
