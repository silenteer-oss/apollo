import { CryptoDatabase } from '@infrastructure/crypto/services/client-database';

export * from '@infrastructure/crypto/services/client-database';
export const cryptoDatabase = new CryptoDatabase();
