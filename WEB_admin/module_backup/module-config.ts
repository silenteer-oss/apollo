import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const BACKUP_ADMIN_MODULE_ID = 'BackupAdminModule';

export const BACKUP_ADMIN_VIEW_ID = 'BackupAdminAppView';

export interface IModuleViews {
  [BACKUP_ADMIN_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: BACKUP_ADMIN_MODULE_ID,
    name: 'Backup Module',
    publicAssetPath: `/module/backup`,
    views: {
      BackupAdminAppView: BACKUP_ADMIN_VIEW_ID,
    },
  };
}
