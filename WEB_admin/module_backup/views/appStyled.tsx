import { styled } from '@admin/theme';
import { getCssClass } from '@design-system/infrastructure/utils';
import { BackupAdminViewProps } from './SecurityQuestionInfoModel';
import BackupAdminAppView from '@admin/module_backup/views/app';
import {
  mixCustomBlueprintStyle,
  mixGlobalStyle,
} from '@admin/theme';

export default styled(BackupAdminAppView).attrs(({ className }) => ({
  className: getCssClass('sl-BackupAdminAppView', className),
})) <BackupAdminViewProps>`
  ${props => {
    return `
          ${mixGlobalStyle(props.theme)};
          ${mixCustomBlueprintStyle(props.theme)};
    `;
  }}
`;
