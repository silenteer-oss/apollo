import { IItemRendererProps, Select } from '@blueprintjs/select';
import {
  Field,
  FieldProps,
  Form,
  Formik,
  FormikActions,
  FormikErrors,
} from 'formik';
import {
  Alignment,
  Button,
  Classes,
  FormGroup,
  Icon,
  InputGroup,
  MenuItem,
  Toaster,
} from '@blueprintjs/core';
import {
  BodyTextL,
  BodyTextM,
  Box,
  Flex,
  H1,
  H2,
  H4,
} from '@design-system/core/components';
import React from 'react';
import { i18n } from '../i18n';

import { isEmpty, isEmptyObject } from '@design-system/infrastructure/utils';
import { getCssClass } from '@design-system/infrastructure/utils';

import {
  ISecurityQuestionValues,
  ICreateSecurityQuestionState,
  SecurityQuestionViewProps,
  SECURITY_QUESTION_OPTIONS,
} from './SecurityQuestionInfoModel';
import SecurityQuestionService from '@admin/module_backup/SecurityQuestionService';
import { SecurityQuestionAnswer } from '@silenteer/hermes/resource/sysadmin-app-resource/model';

const SecretQuestionSelect = Select.ofType<string>();

class SecurityQuestionView extends React.Component<
  SecurityQuestionViewProps,
  ICreateSecurityQuestionState
  > {
  constructor(props: SecurityQuestionViewProps) {
    super(props);

    this.state = {
      loading: false,
      errors: {},
      questionVals: [null, null, null],
      filterOptions: SECURITY_QUESTION_OPTIONS,
    };
  }

  toaster: Toaster;

  render() {
    const {
      className,
      isOpen,
      adminUserId,
      theme: { space, foreground },
    } = this.props;
    const { loading, errors } = this.state;
    const selectedAdmin =
      this.props.selectedAdmin || ({} as ISecurityQuestionValues);

    const initialValues = {
      careProviderId: adminUserId,
      question1: selectedAdmin.question1 || null,
      answer1: selectedAdmin.answer1 || '',
      question2: selectedAdmin.question2 || null,
      answer2: selectedAdmin.answer2 || '',
      question3: selectedAdmin.question3 || null,
      answer3: selectedAdmin.answer3 || '',
    };
    return (
      isOpen && (
        <Flex className={className} auto column align="center" justify="center">
          <img src="../../assets/images/logo.svg" />
          <H1 padding={'24px 0 8px 0'}>{i18n.vi.ACCOUNT_SECURITY}</H1>
          <BodyTextL
            fontSize="20px"
            lineHeight="28px"
            color={foreground['03']}
            padding={'0 0 16px 0'}
          >
            {adminUserId}
          </BodyTextL>
          <Formik
            initialValues={initialValues}
            onSubmit={this.onSaveForm}
            render={({ values, isSubmitting }) => {
              const { errors } = this.state;

              return (
                <Form>
                  <Flex>
                    <H4>{i18n.vi.SECURITY_QUESTIONS}</H4>
                  </Flex>
                  <Flex>
                    <BodyTextM
                      color={foreground['02']}
                      padding={'10px 0 14px 0'}
                    >
                      {i18n.vi.SECURITY_QUESTION_DESCRIPTION}
                    </BodyTextM>
                  </Flex>
                  <Box className="question-select">
                    <Field
                      name="question1"
                      render={({ field, form }: FieldProps<{}>) => {
                        const onSelect = (item: string) => {
                          const { setFieldValue } = form;
                          setFieldValue(field.name, item);
                          this.handleQuestionValChange(item, 0);
                        };

                        return (
                          <FormGroup
                            label={this.renderRequireLabel(
                              i18n.vi.SECURITY_QUESTION_1
                            )}
                            helperText={errors.question1}
                            className={
                              errors.question1 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <SecretQuestionSelect
                              className={getCssClass(
                                'sl-select cy-question1-select',
                                errors.question1 ? Classes.INTENT_DANGER : ''
                              )}
                              filterable={false}
                              items={['Select', ...this.getAvailableOptions()]}
                              activeItem={this.state.questionVals[0]}
                              onItemSelect={onSelect}
                              itemRenderer={this.renderTitleItem}
                              popoverProps={{
                                minimal: true,
                                captureDismiss: true,
                                fill: true,
                              }}
                            >
                              <Button
                                fill
                                alignText={Alignment.LEFT}
                                text={
                                  this.state.filterOptions
                                    .filter(questionOption => {
                                      return (
                                        field.value === questionOption.value
                                      );
                                    })
                                    .map(item => item.label)
                                    .shift() || (
                                    <div
                                      style={{
                                        height: 20,
                                        color: '#9ea9b2',
                                      }}
                                    >
                                      {i18n.vi.CHOOSE_QUESTIONS}
                                    </div>
                                  )
                                }
                                rightIcon={<Icon icon="caret-down" />}
                              />
                            </SecretQuestionSelect>
                          </FormGroup>
                        );
                      }}
                    />
                  </Box>
                  <Flex className="name-input">
                    <Field
                      name="answer1"
                      render={({ field }: FieldProps<{}>) => {
                        return (
                          <FormGroup
                            label={this.renderRequireLabel(i18n.vi.ANSWER)}
                            helperText={errors.answer1}
                            className={
                              errors.answer1 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <InputGroup
                              {...field}
                              autoFocus
                              disabled={loading}
                              placeholder={i18n.vi.INPUT_ANSWER}
                            //required
                            />
                          </FormGroup>
                        );
                      }}
                    />
                  </Flex>
                  <Box className="question-select">
                    <Field
                      name="question2"
                      render={({ field, form }: FieldProps<{}>) => {
                        const onSelect = (item: string) => {
                          const { setFieldValue } = form;
                          setFieldValue(field.name, item);
                          this.handleQuestionValChange(item, 1);
                        };
                        return (
                          <FormGroup
                            label={this.renderRequireLabel(
                              i18n.vi.SECURITY_QUESTION_2
                            )}
                            helperText={errors.question2}
                            className={
                              errors.question2 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <SecretQuestionSelect
                              className={getCssClass(
                                'sl-select cy-question2-select',
                                errors.question2 ? Classes.INTENT_DANGER : ''
                              )}
                              filterable={false}
                              items={this.getAvailableOptions()}
                              activeItem={this.state.questionVals[1]}
                              onItemSelect={onSelect}
                              itemRenderer={this.renderTitleItem}
                              popoverProps={{
                                minimal: true,
                                captureDismiss: true,
                                fill: true,
                              }}
                            >
                              <Button
                                fill
                                alignText={Alignment.LEFT}
                                text={
                                  this.state.filterOptions
                                    .filter(questionOption => {
                                      return (
                                        field.value === questionOption.value
                                      );
                                    })
                                    .map(item => item.label)
                                    .shift() || (
                                    <div
                                      style={{
                                        height: 20,
                                        color: '#9ea9b2',
                                      }}
                                    >
                                      {i18n.vi.CHOOSE_QUESTIONS}
                                    </div>
                                  )
                                }
                                rightIcon={<Icon icon="caret-down" />}
                              />
                            </SecretQuestionSelect>
                          </FormGroup>
                        );
                      }}
                    />
                  </Box>
                  <Flex className="name-input">
                    <Field
                      name="answer2"
                      render={({ field }: FieldProps<{}>) => {
                        return (
                          <FormGroup
                            label={this.renderRequireLabel(i18n.vi.ANSWER)}
                            helperText={errors.answer2}
                            className={
                              errors.answer2 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <InputGroup
                              {...field}
                              autoFocus
                              disabled={loading}
                              placeholder={i18n.vi.INPUT_ANSWER}
                            />
                          </FormGroup>
                        );
                      }}
                    />
                  </Flex>
                  <Box className="question-select">
                    <Field
                      name="question3"
                      render={({ field, form }: FieldProps<{}>) => {
                        const onSelect = (item: string) => {
                          const { setFieldValue } = form;
                          setFieldValue(field.name, item);
                          this.handleQuestionValChange(item, 2);
                        };

                        return (
                          <FormGroup
                            label={this.renderRequireLabel(
                              i18n.vi.SECURITY_QUESTION_3
                            )}
                            helperText={errors.question3}
                            className={
                              errors.question3 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <SecretQuestionSelect
                              className={getCssClass(
                                'sl-select cy-question3-select',
                                errors.question3 ? Classes.INTENT_DANGER : ''
                              )}
                              filterable={false}
                              items={[
                                i18n.vi.CHOOSE_QUESTIONS,
                                ...this.getAvailableOptions(),
                              ]}
                              activeItem={this.state.questionVals[2]}
                              onItemSelect={onSelect}
                              itemRenderer={this.renderTitleItem}
                              popoverProps={{
                                minimal: true,
                                captureDismiss: true,
                                fill: true,
                              }}
                            >
                              <Button
                                fill
                                alignText={Alignment.LEFT}
                                text={
                                  this.state.filterOptions
                                    .filter(questionOption => {
                                      return (
                                        field.value === questionOption.value
                                      );
                                    })
                                    .map(item => item.label)
                                    .shift() || (
                                    <div
                                      style={{
                                        height: 20,
                                        color: '#9ea9b2',
                                      }}
                                    >
                                      {i18n.vi.CHOOSE_QUESTIONS}
                                    </div>
                                  )
                                }
                                rightIcon={<Icon icon="caret-down" />}
                              />
                            </SecretQuestionSelect>
                          </FormGroup>
                        );
                      }}
                    />
                  </Box>
                  <Flex className="name-input">
                    <Field
                      name="answer3"
                      render={({ field }: FieldProps<{}>) => {
                        return (
                          <FormGroup
                            label={this.renderRequireLabel(i18n.vi.ANSWER)}
                            helperText={errors.answer3}
                            className={
                              errors.answer3 ? Classes.INTENT_DANGER : ''
                            }
                          >
                            <InputGroup
                              {...field}
                              autoFocus
                              disabled={loading}
                              placeholder={i18n.vi.INPUT_ANSWER}
                            />
                          </FormGroup>
                        );
                      }}
                    />
                  </Flex>
                  <Button
                    intent="primary"
                    type="submit"
                    disabled={isSubmitting}
                    className="bp3-button bp3-large bp3-fill"
                  >
                    <Flex align="center">{i18n.vi.CONTINUE}</Flex>
                  </Button>
                </Form>
              );
            }}
          />
        </Flex>
      )
    );
  }

  handleQuestionValChange = (option, index) => {
    const newQuestionVals = this.state.questionVals;
    newQuestionVals[index] = option;
    this.setState(state => {
      return {
        questionVals: newQuestionVals,
      };
    });
  };

  renderRequireLabel = (label: string) => {
    return (
      <Flex>
        {label}&nbsp;
        <Flex className="require-label">*</Flex>
      </Flex>
    );
  };

  getRandomInt = max => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  getAvailableOptions = () => {
    const availableOptionsLeft = this.state.filterOptions;
    return availableOptionsLeft
      .filter(questionOption => {
        return this.state.questionVals.indexOf(questionOption.value) === -1;
      })
      .map(item => item.value);
  };
  onTrustDevice = () => { };
  onSelect = () => { };
  onSaveForm = (
    values: ISecurityQuestionValues,
    formikBag: FormikActions<ISecurityQuestionValues>
  ) => {
    const {
      question1,
      answer1,
      question2,
      answer2,
      question3,
      answer3,
    } = values;

    let errors = {} as FormikErrors<ISecurityQuestionValues>;
    let careProviderId = this.props.adminUserId;

    if (isEmpty(question1, true)) {
      errors.question1 = i18n.vi.QUESTION_ERROR;
    }

    if (isEmpty(question2, true)) {
      errors.question2 = i18n.vi.QUESTION_ERROR;
    }

    if (isEmpty(question3, true)) {
      errors.question3 = i18n.vi.QUESTION_ERROR;
    }

    if (isEmpty(answer1, true)) {
      errors.answer1 = i18n.vi.ANSWER_ERROR;
    } else if (answer1.length > 128) {
      errors.answer1 = i18n.vi.ANSWER_ERROR_TOO_LONG;
    }

    if (isEmpty(answer2, true)) {
      errors.answer2 = i18n.vi.ANSWER_ERROR;
    } else if (answer1.length > 128) {
      errors.answer2 = i18n.vi.ANSWER_ERROR_TOO_LONG;
    }

    if (isEmpty(answer3, true)) {
      errors.answer3 = i18n.vi.ANSWER_ERROR;
    } else if (answer3.length > 128) {
      errors.answer3 = i18n.vi.ANSWER_ERROR_TOO_LONG;
    }

    let questionAnswer: SecurityQuestionAnswer[] = [];

    questionAnswer.push({
      question: question1,
      answer: answer1,
    });
    questionAnswer.push({
      question: question2,
      answer: answer2,
    });
    questionAnswer.push({
      question: question3,
      answer: answer3,
    });

    if (isEmptyObject(errors)) {
      SecurityQuestionService.createSecurityQuestion(
        careProviderId,
        questionAnswer
      ).then(data => {
        return this.props.onAddSecurityQuestion(
          careProviderId,
          data,
          questionAnswer
        );
      });
    } else {
      this.setState({ errors });
      formikBag.setSubmitting(false);
    }
  };

  renderTitleItem = (
    title: string,
    { handleClick, modifiers }: IItemRendererProps
  ): JSX.Element => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        key={title}
        onClick={handleClick}
        text={this.state.filterOptions
          .filter(questionOption => {
            return title === questionOption.value;
          })
          .map(item => item.label)
          .shift()}
        shouldDismissPopover
      />
    );
  };
}

export { SecurityQuestionView };
