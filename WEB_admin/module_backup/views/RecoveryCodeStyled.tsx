import { RecoveryCodeView } from './RecoveryCodeView';
import styled from 'styled-components';
import { scaleSpacePx } from '@design-system/core/styles';

export default styled(RecoveryCodeView)`
  width: ${scaleSpacePx(128)};
  margin: auto;
  align-self: center;

  .codes_container {
    display: flex;
    justify-content: center;
    align-content: center;
    flex-wrap: wrap;
  }

  .code_usage_warning {
    /* Orange */
    color: #ee9847;
  }

  .button_number {
    align-self: center;
    border-radius: 50%;
    width: 16px;
    height: 16px;
    background: #9aa7b2;
    color: #ffffff;
    text-align: center;
    font-size: 12px;
    line-height: 18px;
  }

  .codes_item {
    flex: 50%;
    padding-left: 38px;
  }

  .question_answer {
    display: flex;
    flex-direction: column;
    padding-left: 38px;
    padding-top: 1rem;
  }

  .button_group {
    padding-top: 20px;
    margin: 0 auto;
    width: ${scaleSpacePx(128)};
  }

  .two_button {
    display: inline-block;

    text-align: center;
    width: calc(50% - 9px);
  }

  .button_first {
    margin: 0 18px 0 0;
  }
`;
