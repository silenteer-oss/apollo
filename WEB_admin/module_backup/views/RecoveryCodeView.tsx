import { Button, ButtonGroup, Classes } from '@blueprintjs/core';
import {
  BodyTextL,
  BodyTextM,
  Flex,
  H1,
  H2,
  H3,
} from '@design-system/core/components';
import React, { useEffect, useState } from 'react';
import { i18n } from '../i18n';

import SecurityQuestionService from '../SecurityQuestionService';
import printJs from 'print-js';
import { IAdminTheme } from '@admin/theme/theme';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { SecurityQuestionViewProps } from '@admin/module_backup';
import { SECURITY_QUESTION_OPTIONS } from './SecurityQuestionInfoModel';
import { SecurityQuestionAnswer } from '@silenteer/hermes/resource/sysadmin-app-resource/model';

export interface RecoveryCodeViewProps {
  className?: string;
  theme?: IAdminTheme;
  isOpen?: boolean;
  careProviderId: string;
  codes: number[];
  questionAnswer: SecurityQuestionAnswer[];
}

export class RecoveryCodeViewInternal extends React.Component<
  RecoveryCodeViewProps & RouteComponentProps<any>
  > {
  constructor(props: RecoveryCodeViewProps & RouteComponentProps<any>) {
    super(props);
  }

  render() {
    const {
      codes,
      careProviderId,
      theme: { space, foreground },
      history,
      questionAnswer,
    } = this.props;
    return (
      this.props.isOpen && (
        <Flex {...this.props} auto column align="center" justify="center">
          <img src="../../assets/images/logo.svg" />
          <H1 padding={'24px 0 41px 0'}>{i18n.vi.ACCOUNT_SECURITY}</H1>
          <Flex auto column>
            <Flex>
              <div className="button_number">1</div>
              <Flex pl="21px">
                <H3>{i18n.vi.YOUR_RECOVERY_CODE}</H3>
              </Flex>
            </Flex>

            <BodyTextM color={foreground['02']} padding={'4px 0 10px 38px'}>
              {i18n.vi.RECOVERY_CODE_DESCRIPTION}
            </BodyTextM>

            {this.createCodes()}
            <Flex pt="2rem">
              <div className="button_number">2</div>
              <Flex pl="21px">
                <H3>{i18n.vi.CARE_PROVIDER_ID}</H3>
              </Flex>
            </Flex>
            <Flex pl="38px">
              <BodyTextM color={foreground['02']}>{careProviderId}</BodyTextM>
            </Flex>
            <Flex pt="41px">
              <div className="button_number">3</div>
              <Flex pl="21px">
                <H3>{i18n.vi.YOUR_SECURITY_QUESTION}</H3>
              </Flex>
            </Flex>
            {this.renderQuestionAnswer()}
            <Flex pt="1.5rem">
              <H2 className="code_usage_warning">
                {i18n.vi.CODE_USAGE_WARNING}
              </H2>
            </Flex>
          </Flex>
          <Flex className="button_group">
            <Button className="two_button button_first" onClick={this.printPdf}>
              {i18n.vi.PRINT}
            </Button>
            <Button
              className="two_button"
              intent="primary"
              onClick={this.downloadPdf}
            >
              {i18n.vi.DOWNLOAD}
            </Button>
          </Flex>
          <Flex pt="6px">
            <Button
              minimal
              intent="primary"
              className="bp3-button bp3-large bp3-fill cy-trust-btn"
              onClick={this.done}
            >
              {i18n.vi.DONE}
            </Button>
          </Flex>
        </Flex>
      )
    );
  }

  createCodes = () => {
    let codeList = [];

    for (let i = 0; i < this.props.codes.length; i++) {
      codeList.push(
        <Flex className="codes_item">
          <BodyTextL
            fontSize="20px"
            lineHeight="28px"
            color={this.props.theme.foreground['02']}
          >
            {this.props.codes[i]
              .toString()
              .replace(/(\d{4})/g, '$1 ')
              .replace(/(^\s+|\s+$)/, '')}
          </BodyTextL>
        </Flex>
      );
    }
    return <Flex className="codes_container">{codeList}</Flex>;
  };

  renderQuestionAnswer() {
    let questionAnswerHtml = [];

    for (let i = 0; i < this.props.questionAnswer.length; i++) {
      let questionAnswer = SECURITY_QUESTION_OPTIONS.find(
        ({ value }) => value === this.props.questionAnswer[i].question
      );
      if (questionAnswer) {
        questionAnswerHtml.push(
          <Flex pt={i > 0 ? '24px' : '0px'} auto column>
            <BodyTextM color={this.props.theme.foreground['02']}>
              {questionAnswer.label}
            </BodyTextM>
            <BodyTextM color={this.props.theme.foreground['02']}>
              {this.props.questionAnswer[i].answer}
            </BodyTextM>
          </Flex>
        );
      }
    }
    return <Flex className="question_answer">{questionAnswerHtml}</Flex>;
  }

  printPdf = () => {
    printJs('/api/app/sysadmin/admin/codes/downloadPdf');
  };

  downloadPdf = () => {
    SecurityQuestionService.downloadCodePdf();
  };

  done = () => {
    SecurityQuestionService.complete(this.props.careProviderId).then(() => {
      this.props.history.push(`/app/user`);
    });
  };
}

const RecoveryCodeView = withRouter(RecoveryCodeViewInternal);
export { RecoveryCodeView };
