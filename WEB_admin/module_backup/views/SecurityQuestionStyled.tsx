import { SecurityQuestionView } from './SecurityQuestionView';
import { styled } from '@admin/theme';
import { getCssClass } from '@design-system/infrastructure/utils';
import { SecurityQuestionViewProps } from './SecurityQuestionInfoModel';
import { scaleSpacePx } from '@design-system/core/styles';

export default styled(SecurityQuestionView).attrs(({ className }) => ({
  className: getCssClass('sl-SecurityQuestionView', className),
})) <SecurityQuestionViewProps>`
  ${props => {
    const { background, space, foreground } = props.theme;

    return `
      & {
        width: ${scaleSpacePx(128)};
        margin: auto;
      .bp3-form-content {
        .bp3-input {
          height: ${scaleSpacePx(10)};
        }
        .bp3-button {
          height: ${scaleSpacePx(10)};
          background: ${background.white}
        }
      }

      .question-select{
        .bp3-form-group:first-child {
              margin-bottom: ${space.xs};
        }
      }
      
      .require-label {
        color: ${foreground.error.base};
      }
        
      .name-input {
        flex-direction: column;
        padding-bottom:12px;
        .bp3-form-group:first-child {
          margin-bottom: ${space.s};
        }

        > .bp3-form-group {
          flex: 1;
          margin-bottom: 0px;


          .bp3-input-group {
            display: flex;

            > input {
              flex: 1
            }
          }
        }
      }
    `;
  }}
`;
