import { IAdminTheme } from '@admin/theme/theme';
import { SecurityQuestion, SecurityQuestionAnswer } from '@silenteer/hermes/resource/sysadmin-app-resource/model';

export interface BackupAdminViewProps {
  className?: string;
  theme?: IAdminTheme;
}

export interface SecurityQuestionViewProps {
  className?: string;
  theme?: IAdminTheme;
  adminUserId: string;
  isOpen?: boolean;
  onAddSecurityQuestion?: (careProviderId, data, questionAnswer) => void;
  selectedAdmin?: ISecurityQuestionValues;
}

export interface ICreateSecurityQuestionState {
  loading: boolean;
  errors?: any;
  questionVals: string[];
  filterOptions: ISelectOption[];
}

export interface ISelectOption {
  value: string;
  label: string;
}

export interface IBackupAdminState {
  loading: boolean;
  isOpenSecurityCode: boolean;
  isOpenSecurityQuestion: boolean;
  codes: number[];
  questionAnswer: SecurityQuestionAnswer[];
  careProviderId: string;
  alreadyBackup: boolean;
  adminUserId: string;
  errors?: any;
}

export const SECURITY_QUESTION_OPTIONS: ISelectOption[] = [
  {
    value: SecurityQuestion.CITYYOURPARENTMEET,
    label: 'Bố mẹ của bạn gặp nhau ở thành phố nào?',
  },
  {
    value: SecurityQuestion.YOURDELICIOUSFOOD,
    label: 'Món ăn bạn thích là món gì?',
  },
  {
    value: SecurityQuestion.YOURCHILDHOODNICKNAME,
    label: 'Biệt danh thời thơ ấu của bạn là gì?',
  },
  {
    value: SecurityQuestion.YOURFIRSTPETNAME,
    label: 'Tên thú cưng đầu tiên của bạn là gì?',
  },
  {
    value: SecurityQuestion.YOURCLOSESTFRIENDNAME,
    label: 'Tên của người bạn thân nhất của bạn?',
  },
  {
    value: SecurityQuestion.YOURFAVOURITECOLOR,
    label: 'Màu sắc mà bạn yêu thích là màu gì?',
  },
  {
    value: SecurityQuestion.YOURLUCKYNUMBER,
    label: 'Chữ số may mắn của bạn là gì?',
  },
  {
    value: SecurityQuestion.YOURFIRSTJOB,
    label: 'Công việc đầu tiên mà bạn làm là gì?',
  },
  {
    value: SecurityQuestion.YOURFIRSTPRIMARYSCHOOL,
    label: 'Tên trường tiểu học đầu tiên của bạn là gì?',
  },
]

export interface ISecurityQuestionValues {
  careProviderId?: string;
  question1?: SecurityQuestion;
  answer1?: string;
  question2?: SecurityQuestion;
  answer2?: string;
  question3?: SecurityQuestion;
  answer3?: string;
}
