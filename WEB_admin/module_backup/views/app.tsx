import React from 'react';
import RecoveryCodeView from './RecoveryCodeStyled';
import SecurityQuestionView from './SecurityQuestionStyled';
import {
  BackupAdminViewProps,
  IBackupAdminState,
} from './SecurityQuestionInfoModel';
import SecurityQuestionService from '@admin/module_backup/SecurityQuestionService';
import { withRouter, RouteComponentProps } from 'react-router-dom';

export class BackupAdminAppViewOriginal extends React.Component<
  BackupAdminViewProps & RouteComponentProps<any>,
  IBackupAdminState
  > {
  constructor(props: BackupAdminViewProps & RouteComponentProps<any>) {
    super(props);
    this.state = {
      loading: false,
      isOpenSecurityCode: false,
      isOpenSecurityQuestion: false,
      codes: [],
      questionAnswer: [],
      careProviderId: '',
      alreadyBackup: false,
      adminUserId: '',
      errors: {},
    };
  }

  componentDidMount() {
    SecurityQuestionService.getOrCreateAdminBackupInfo().then(data => {
      if (data) {
        if (data.careProviderBackupKey != null) {
          this.props.history.push(`/app/user`);
        } else {
          this.setState({
            isOpenSecurityQuestion: true,
            adminUserId: data.adminUserId,
          });
        }
      } else {
        this.setState({ isOpenSecurityQuestion: true });
      }
    });
  }

  render() {
    const { theme } = this.props;
    const {
      isOpenSecurityCode,
      codes,
      isOpenSecurityQuestion,
      questionAnswer,
      careProviderId,
      adminUserId,
    } = this.state;

    return (
      <div>
        <SecurityQuestionView
          adminUserId={adminUserId}
          theme={theme}
          isOpen={isOpenSecurityQuestion}
          onAddSecurityQuestion={this.onAddSecurityQuestion}
        />
        <RecoveryCodeView
          theme={theme}
          careProviderId={careProviderId}
          codes={codes}
          questionAnswer={questionAnswer}
          isOpen={isOpenSecurityCode}
        />
      </div>
    );
  }

  onAddSecurityQuestion = (careProviderId, data, questionAnswer) => {
    this.setState({
      isOpenSecurityCode: true,
      careProviderId: careProviderId,
      isOpenSecurityQuestion: false,
      codes: data,
      questionAnswer: questionAnswer,
    });
  };
}

export default withRouter(BackupAdminAppViewOriginal);
