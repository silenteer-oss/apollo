import BackupAdminAppView from './views/appStyled';
import { BACKUP_ADMIN_MODULE_ID, BACKUP_ADMIN_VIEW_ID } from './module-config';

export default {
  [BACKUP_ADMIN_MODULE_ID]: {
    [BACKUP_ADMIN_VIEW_ID]: BackupAdminAppView,
  },
};
