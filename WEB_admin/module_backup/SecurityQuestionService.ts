import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';
import { AuthenticationApi } from '@infrastructure/resource/AuthenticationResource';
import { createCareProviderKeyPair } from '@admin/common/config/KeyStorage';
import {
  BackupAdminRequest,
  SecurityQuestionAnswer,
} from '@silenteer/hermes/resource/sysadmin-app-resource/model';
import { CodeRequest } from '@silenteer/hermes/resource/sysadmin-app-resource/model/code-request';
import { cryptoDatabase } from '@admin/services/client-database';
import { ICryptoKeyPairSchema } from '@infrastructure/crypto/services/client-database/models';

function getPhraseCode(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6);
}

async function login(ticket: string, identity: string) {
  await SysadminApi.organizationLogin({ ticket, identity });
  await createCareProviderKeyPair();
}

async function createSecurityQuestion(
  adminUserId: string,
  questionAnswers: SecurityQuestionAnswer[]
) {
  const careProviderKeyPair = await cryptoDatabase.keyPairStorage.get(
    'CARE_PROVIDER'
  );
  let objectStr = btoa(JSON.stringify(careProviderKeyPair));
  let backupAdminRequest: BackupAdminRequest = {
    questionList: questionAnswers,
    careProviderBackupKey: objectStr,
  };
  return SysadminApi.createAdminBackupData(
    adminUserId,
    backupAdminRequest
  ).then(({ data }) => {
    return data;
  });
}

function downloadCodePdf() {
  SysadminApi.downloadCodesInPdf({ responseType: 'blob' }).then(({ data }) => {
    const url = window.URL.createObjectURL(data);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'admin-backup-codes.pdf');
    document.body.appendChild(link);
    link.click();
  });
}

async function loginByCode(adminUserId: string, recoverCode: string) {
  let codeRequest: CodeRequest = {
    recoveryCode: recoverCode,
  };
  return AuthenticationApi.verifyAdminRecoveryCode(
    adminUserId,
    codeRequest
  ).then(({ data }) => {
    let result: ICryptoKeyPairSchema = JSON.parse(
      atob(data.careProviderBackupKey)
    );
    cryptoDatabase.keyPairStorage.put({
      key: 'CARE_PROVIDER',
      value: result.value,
    });
  });
}

async function loginBySecurityQuestion(
  adminUserId: string,
  questionAnswer: SecurityQuestionAnswer
) {
  return AuthenticationApi.verifyAdminQuestionAnswer(
    adminUserId,
    questionAnswer
  ).then(({ data }) => {
    let result: ICryptoKeyPairSchema = JSON.parse(
      atob(data.careProviderBackupKey)
    );
    cryptoDatabase.keyPairStorage.put({
      key: 'CARE_PROVIDER',
      value: result.value,
    });
  });
}

async function complete(adminUserId: string) {
  const careProviderKeyPair = await cryptoDatabase.keyPairStorage.get(
    'CARE_PROVIDER'
  );
  let objectStr = btoa(JSON.stringify(careProviderKeyPair));
  return SysadminApi.complete(adminUserId, objectStr);
}

async function getOrCreateAdminBackupInfo() {
  return SysadminApi.getOrCreateAdminBackupInfo().then(({ data }) => {
    return data;
  });
}

function verifyAdminUserId(adminUserId: string) {
  return SysadminApi.verifyAdminUserId(adminUserId).then(({ data }) => {
    return data;
  });
}

export default {
  getPhraseCode,
  createSecurityQuestion,
  downloadCodePdf,
  loginByCode,
  loginBySecurityQuestion,
  complete,
  getOrCreateAdminBackupInfo,
  verifyAdminUserId,
};
