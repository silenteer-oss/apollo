import React from 'react';
import ReactDOM from 'react-dom';
import { Flex } from '@design-system/core/components';

import '@admin/assets/styles/index.scss';
import BackupAdminAppView from './views/appStyled';
import {
  ThemeProvider,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
  theme,
  getAllViewTheme,
  getAllComponentTheme,
} from '@admin/theme';
import '@admin/assets/styles/index.scss';
import { GlobalStyleContextProvider } from '@design-system/themes/context';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <GlobalStyleContextProvider
      styles={{
        main: mixGlobalStyle(theme),
        blueprint: mixCustomBlueprintStyle(theme),
      }}
    >
      <Flex my="1rem" mx="2rem" auto justify="center">
        <BackupAdminAppView theme={theme} />
      </Flex>
    </GlobalStyleContextProvider>
  </ThemeProvider>,
  document.getElementById('root')
);
