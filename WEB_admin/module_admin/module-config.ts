import type { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const ADMIN_MODULE_ID = 'AdminModule';

export const ADMIN_VIEW_ID = 'AdminView';

export interface IModuleViews {
  [ADMIN_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: ADMIN_MODULE_ID,
    name: 'Admin Module',
    publicAssetPath: `/module/admin`,
    views: {
      AdminView: ADMIN_VIEW_ID,
    },
  };
}
