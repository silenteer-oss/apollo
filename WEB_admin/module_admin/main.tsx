import React from 'react';
import ReactDOM from 'react-dom';
import { AppView } from './app';

import '@admin/assets/styles/index.scss';

function Main() {
  return <AppView />;
}

ReactDOM.render(<Main />, document.getElementById('root'));
