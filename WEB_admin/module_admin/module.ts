import { ADMIN_MODULE_ID, ADMIN_VIEW_ID } from './module-config';
import { AppView } from './app';

export default {
  [ADMIN_MODULE_ID]: {
    [ADMIN_VIEW_ID]: AppView,
  },
};
