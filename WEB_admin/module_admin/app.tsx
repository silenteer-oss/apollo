import { Button, Dialog, Icon, Intent } from '@blueprintjs/core';
import adminService from './admin.service';
import { Flex } from '@design-system/core/components';
import React, { createContext, useEffect, useState } from 'react';
import UserListView from './views/admin-list.styled';
import UserView from './views/admin.styled';
import { AdminUser } from './admin.service';

export interface AdminUserContext {
  users: AdminUser[];
  onCreate?: (user: AdminUser) => void;
  onDelete?: (user: AdminUser) => void;
}

export const AdminUserContext = createContext<AdminUserContext>(
  {} as AdminUserContext
);

function AppView(): React.FunctionComponentElement<{}> {
  const [open, setOpenState] = useState(false);
  const [users, updateAdminUserList] = useState<AdminUser[]>([]);

  const onCloseDialog = () => setOpenState(false);
  const onOpenDialog = () => setOpenState(true);

  const onCreate = (user: AdminUser) => {
    updateAdminUserList([...users, user]);
    onCloseDialog();
  };

  const onDelete = (user: AdminUser) => {
    updateAdminUserList(users.filter(usr => usr.id !== user.id));
  };

  useEffect(() => {
    adminService.getAdministrators().then(({ data }) => {
      updateAdminUserList(data);
    });
  }, []);

  return (
    <AdminUserContext.Provider value={{ users, onCreate, onDelete }}>
      <Flex column>
        <Flex justify="flex-end">
          <Button className="bp3-button bp3-large" onClick={onOpenDialog}>
            <Icon
              icon="plus"
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.PRIMARY}
            />
          </Button>
        </Flex>
        <Flex auto pt="0.6rem">
          <UserListView />
        </Flex>
        <Dialog isOpen={open} onClose={onCloseDialog} title="Add administrator">
          <UserView />
        </Dialog>
      </Flex>
    </AdminUserContext.Provider>
  );
}

export { AppView };
