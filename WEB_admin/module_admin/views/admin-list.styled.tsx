/* stylelint-disable no-descending-specificity */
import { Colors } from '@blueprintjs/core';
import styled from 'styled-components';
import { AdminUserList } from './admin-list.view';

export default styled(AdminUserList)`
  .sl-user-ls-header {
    height: 40px;
    background-color: ${Colors.LIGHT_GRAY4};
    border: 1px solid ${Colors.LIGHT_GRAY2};

    > div {
      height: inherit;
      align-items: center;
      padding-left: 8px;
      &:not(:first-child) {
        border-left: 1px solid ${Colors.LIGHT_GRAY2};
      }
    }
  }

  .sl-user-ls-row {
    height: 40px;
    align-items: center;
    border-bottom: 1px solid ${Colors.LIGHT_GRAY3};
    > div {
      padding-left: 8px;
    }
  }
`;
