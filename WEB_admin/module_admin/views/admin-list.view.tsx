import { Alert, Intent, Toaster } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React, { useContext, useMemo, useRef, useState } from 'react';
import { AdminUserContext } from '../app';
import { AdminUser } from '../admin.service';

interface AdminListProps {
  className?: string;
}

function AdminUserList(
  props: AdminListProps
): React.FunctionComponentElement<AdminListProps> {
  const { className } = props;

  const [open, setConfirmState] = useState<boolean>(false);
  const [selectedUser] = useState<AdminUser>({} as AdminUser);

  const toasterRef = useRef<Toaster>(null);
  const context = useContext(AdminUserContext);

  const handleMoveCancel = () => setConfirmState(false);
  const handleMoveConfirm = () => {};

  return useMemo(
    () => (
      <Flex auto className={className} column>
        <Flex className="sl-user-ls-header">
          <Flex w="50%">Fullname</Flex>
          <Flex w="30%" justify="space-between">
            Type
          </Flex>
          <Flex w="20%">Action</Flex>
        </Flex>
        {/* {context.users.map((user: PracticeUser) => (
          <Flex key={user.id} className="sl-user-ls-row">
            <Flex w="50%">{user.fullName}</Flex>
            <Flex w="30%">{user.type}</Flex>
            <Flex w="20%">
              <Button
                intent={Intent.DANGER}
                text="Remove"
                small
                onClick={_onDelete(user)}
              />
            </Flex>
          </Flex>
        ))} */}
        <Alert
          cancelButtonText="Cancel"
          confirmButtonText="Remove"
          icon="trash"
          intent={Intent.DANGER}
          isOpen={open}
          onCancel={handleMoveCancel}
          onConfirm={handleMoveConfirm}
        >
          <p>
            Are you sure you want to remove the user {selectedUser.fullName}?
          </p>
        </Alert>

        <Toaster ref={toasterRef} />
      </Flex>
    ),
    [context.users, open]
  );
}

export { AdminUserList };
