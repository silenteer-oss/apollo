import styled from 'styled-components';
import { AdminUserView } from './admin.view';

export default styled(AdminUserView)`
  fieldset {
    display: flex;
    margin: 0;
    padding: 0;
    border: none;
    padding-top: 16px;
    > label + * {
      margin-left: 8px;
    }
  }
`;
