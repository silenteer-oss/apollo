import { Classes } from '@blueprintjs/core';
import adminService from '../admin.service';
import React, { useContext, useState } from 'react';
import { AdminUserContext } from '../app';
import { AdminUser } from '../admin.service';

export interface AdminViewProps {
  className?: string;
}

function AdminUserView(
  props: AdminViewProps
): React.FunctionComponentElement<AdminViewProps> {
  const { className } = props;
  const [newUser, setNewUser] = useState<AdminUser>({} as AdminUser);

  const context = useContext(AdminUserContext);

  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setNewUser({ ...newUser, fullName: event.target.value });
  };

  const _onCreate = () => {
    adminService
      .createAdmin(newUser)
      .then(({ data }) => context.onCreate(data));
  };

  return (
    <div className={className}>
      <div className={Classes.DIALOG_BODY}>
        <input
          placeholder="Enter your fullname"
          className="bp3-input bp3-large bp3-fill"
          onChange={onNameChange}
        // value={fullName}
        />
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <button
          type="button"
          onClick={_onCreate}
          className="bp3-button bp3-large bp3-intent-primary bp3-fill"
        >
          Create
        </button>
      </div>
    </div>
  );
}

export { AdminUserView };
