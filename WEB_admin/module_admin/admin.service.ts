import axios, { AxiosPromise } from 'axios';

export interface AdminUser {
  id?: string;
  careProviderId?: string;
  fullName: string;
  type: string;
  dob?: number;
  gender?: number;
  mobileNumber?: number;
  telephoneNumber?: number;
  address?: string;
  email?: string;
  userId?: string;
}

function getAdministrators(): AxiosPromise<AdminUser[]> {
  return axios('/api/adminUserManagement/allUser');
}

function createAdmin(admin: AdminUser): AxiosPromise<AdminUser> {
  return axios.post('/api/adminUserManagement', admin);
}

export default {
  getAdministrators,
  createAdmin,
};
