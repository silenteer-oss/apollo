import { createContext } from 'react';
import { AdminApiModel } from '@infrastructure/resource/AdminResource';

interface ITrustCareProviderDeviceContext {
  devices: AdminApiModel.TrustedDeviceDto[];
  setDevices?: Function;
  loading: boolean;
  updateDevice?: Function;
}

export const TrustedCareProviderDeviceContext = createContext<
  ITrustCareProviderDeviceContext
>({
  devices: [],
  loading: true,
});

export interface TrustedCareProviderDeviceProps {
  className?: string;
}
