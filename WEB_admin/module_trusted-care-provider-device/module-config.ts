import type { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID =
  'TrustedCareProviderDeviceModule';

export const TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID =
  'TrustedCareProviderDeviceView';

export interface IModuleViews {
  [TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID,
    name: 'Trusted Care Provider Device Module',
    publicAssetPath: `/module/trusted-care-provider-device`,
    views: {
      TrustedCareProviderDeviceView: TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID,
    },
  };
}
