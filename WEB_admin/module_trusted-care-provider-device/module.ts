import {
  TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID,
  TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID,
} from './module-config';
import { AppView } from './app';

export default {
  [TRUSTED_CARE_PROVIDER_DEVICE_MODULE_ID]: {
    [TRUSTED_CARE_PROVIDER_DEVICE_VIEW_ID]: AppView,
  },
};
