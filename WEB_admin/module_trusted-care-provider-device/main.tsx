import React from 'react';
import ReactDOM from 'react-dom';
import { AppView } from './app';

import '@admin/assets/styles/index.scss';

ReactDOM.render(<AppView />, document.getElementById('root'));
