import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';
import { CareProdiverDeviceList } from './CareProviderDeviceListView';

export default styled(CareProdiverDeviceList)`
  position: relative;
  .sl-list-header {
    height: 40px;
    background-color: #ebf1f5;
  }
  .sl-list-header * {
    align-items: center;
    padding-left: 8px;
    & + * {
      border-left: 1px solid ${Colors.LIGHT_GRAY4};
    }
  }
  .sl-list-item > * {
    height: 40px;
    padding-left: 8px;
    border-bottom: 1px solid ${Colors.LIGHT_GRAY4};
  }

  .sl-list-item button {
    margin-right: 5px;
  }

  .sl-list-item-text {
    align-items: center;
  }
`;
