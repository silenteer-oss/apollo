import { UNKNOW_ERROR } from '@admin/common/constants';
import { Button, Classes, Toaster, InputGroup } from '@blueprintjs/core';
import { Flex, Box } from '@design-system/core/components';
import React, { useContext, useEffect, useRef, useState, memo } from 'react';
import careProviderDeviceService from '../CareProviderDeviceService';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import {
  TrustedCareProviderDeviceContext,
  TrustedCareProviderDeviceProps,
} from '../model';
import { i18n } from '../i18n';

let timeout;

function TrustedCareProviderDeviceView(
  props: TrustedCareProviderDeviceProps
): React.FunctionComponentElement<TrustedCareProviderDeviceProps> {
  const { className } = props;
  const [loading, setLoading] = useState(false);
  const [adminCode, setAdminCode] = useState(
    careProviderDeviceService.getPhraseCode()
  );
  const [errorMessage, setErrorMessage] = useState('');
  const toasterRef = useRef<Toaster>(null);
  const codeInputRef = useRef<HTMLInputElement>(null);
  const context = useContext(TrustedCareProviderDeviceContext);
  const [step, setStep] = useState(1);
  const [deviceName, setDeviceName] = useState('');

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onCareProviderCodeInputChange = () => {
    if (errorMessage) {
      setErrorMessage('');
    }
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  useEffect(() => {
    if (loading) {
      timeout = setTimeout(() => {
        setLoading(false);
        careProviderDeviceService.closeWS();
        toasterRef.current.show({
          message: i18n.vi.addDeviceDialog.PAIRING_ERROR_MESSAGE,
          intent: 'danger',
          icon: 'error',
        });
      }, WEB_SOCKET_PAIRING_TIMEOUT);
    }
    return () => clearTimeout(timeout);
  }, [loading]);

  ///////////////////////////////////////////////////////////////////////////////////////////

  const showError = () => {
    toasterRef.current.show(UNKNOW_ERROR);
    setLoading(false);
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onAddDevice = (): void | Promise<string | void> => {
    if (!codeInputRef.current.value.trim()) {
      setErrorMessage(i18n.vi.addDeviceDialog.PAIRING_VALIDATION_MESSAGE);
      return;
    }

    setLoading(true);
    return careProviderDeviceService.trustDevice(
      codeInputRef.current.value.trim().toUpperCase() + adminCode,
      deviceName,
      (data, err) => {
        if (!err) {
          return context.setDevices(data);
        }
        console.error(err);
        showError();
        careProviderDeviceService.closeWS();
      }
    );
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onRenew = () => {
    setAdminCode(careProviderDeviceService.getPhraseCode());
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onCancel = () => {
    setLoading(false);
    clearTimeout(timeout);
    careProviderDeviceService.closeWS();
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onChangeDeviceName = e => {
    setDeviceName(e.target.value);
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const onNextStep = () => {
    setStep(2);
  };

  const onBackStep = () => {
    setStep(1);
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const renderDeviceName = () => {
    return (
      <>
        <div className={Classes.DIALOG_BODY}>
          <Flex column>
            <Box pb="0.25rem">{i18n.vi.addDeviceDialog.DEVICE_NAME_LABEL}</Box>
            <InputGroup
              autoFocus
              required
              large
              value={deviceName}
              className={`bp3-large ${
                !deviceName || deviceName.length === 0
                  ? 'bp3-intent-danger'
                  : ''
              }`}
              placeholder={i18n.vi.addDeviceDialog.DEVICE_NAME_PLACE_HOLDER}
              onChange={onChangeDeviceName}
            />
          </Flex>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button
            intent="primary"
            onClick={onNextStep}
            disabled={!deviceName || deviceName.length === 0}
            className="bp3-button bp3-large bp3-fill"
          >
            <Flex align="center">{i18n.vi.addDeviceDialog.ACTION_NEXT}</Flex>
          </Button>
        </div>
      </>
    );
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  const renderInputCode = () => {
    return (
      <>
        <div className={Classes.DIALOG_BODY}>
          <Flex align="center" column>
            <Box pb="0.25rem">{i18n.vi.addDeviceDialog.PAIRING_CODE_LABEL}</Box>
            <input
              autoFocus
              ref={codeInputRef}
              disabled={loading}
              className={`ls-code-label bp3-input bp3-large ${
                errorMessage ? 'bp3-intent-danger' : ''
              }`}
              placeholder="_ _ _ _ _ _"
              maxLength={6}
              onChange={onCareProviderCodeInputChange}
            />
            <Flex mt="0.25" className="error">
              {errorMessage}
            </Flex>
          </Flex>
          <Flex pt="1rem" column align="center" justify="center">
            <p>{i18n.vi.addDeviceDialog.PAIRING_INFO_MESSAGE}</p>
            <label className="ls-code-label">{adminCode}</label>
            <span className="sl-issue">
              {`${i18n.vi.addDeviceDialog.PAIRING_CODE_WARNING_MESSAGE} `}
              {loading ? (
                <a onClick={onCancel}>
                  {i18n.vi.addDeviceDialog.PAIRING_CODE_CANCEL}
                </a>
              ) : (
                <a onClick={onRenew}>
                  {i18n.vi.addDeviceDialog.PAIRING_CODE_RENEW}
                </a>
              )}
            </span>
          </Flex>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button
              intent="none"
              onClick={onBackStep}
              disabled={loading}
              className="bp3-button bp3-large bp3-fill"
            >
              <Flex align="center">{i18n.vi.addDeviceDialog.ACTION_BACK}</Flex>
            </Button>

            <Button
              intent="primary"
              onClick={onAddDevice}
              disabled={loading}
              className="bp3-button bp3-large bp3-fill"
            >
              <Flex align="center">
                {loading
                  ? i18n.vi.addDeviceDialog.ACTION_TRUSTING
                  : i18n.vi.addDeviceDialog.ACTION_TRUST}
              </Flex>
            </Button>
          </div>
        </div>
      </>
    );
  };

  ///////////////////////////////////////////////////////////////////////////////////////////

  return (
    <Flex
      auto
      justify="center"
      mt={2}
      column
      className={`${className} cy-trusted-device-dialog`}
    >
      {step === 1 && renderDeviceName()}

      {step === 2 && renderInputCode()}

      <Toaster ref={toasterRef} />
    </Flex>
  );
}

export default memo(TrustedCareProviderDeviceView);
