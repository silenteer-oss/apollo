import styled from 'styled-components';
import TrustedCareProviderDeviceView from './TrustedCareProviderDeviceView';

export default styled(TrustedCareProviderDeviceView)`
  width: 30rem;
  align-self: center;
  .ls-code-label {
    text-align: center;
    font-size: 2rem !important;
    margin-top: 8px;
    text-transform: uppercase;
  }

  input::placeholder {
    font-size: 16px;
  }

  button {
    margin-top: 1rem;
  }

  .sl-issue {
    margin-top: 1rem;
  }
`;
