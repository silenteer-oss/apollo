import {
  Button,
  Popover,
  Intent,
  Classes,
  Dialog,
  InputGroup,
  Toaster,
} from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React, { useContext, useState } from 'react';

import { RouteProps } from 'react-router-dom';
import CareProviderDeviceService from '../CareProviderDeviceService';
import { AdminApiModel } from '@infrastructure/resource/AdminResource';
import { TrustedCareProviderDeviceContext } from '../model';
import { useRef } from 'react';
import { i18n } from '../i18n';

export interface DeviceListProps {
  className?: string;
}

function CareProdiverDeviceList(props: DeviceListProps & RouteProps) {
  const [openEdit, setOpenEditState] = useState(false);
  const [editDevice, setEditDevice] = useState(undefined);
  const toasterRef = useRef<Toaster>(null);

  const context = useContext(TrustedCareProviderDeviceContext);
  const { devices = [], setDevices, updateDevice } = context;

  const onRevokeDevice = (device: AdminApiModel.TrustedDeviceDto) => () => {
    CareProviderDeviceService.revokeDevice(device).then(() => {
      setDevices(devices.filter(d => d.id !== device.id));
    });
  };

  const onEditDevice = (device: AdminApiModel.TrustedDeviceDto) => () => {
    setOpenEditState(true);
    setEditDevice(device);
  };

  const onCancelEdit = () => setOpenEditState(false);

  const onTypeDeviceName = e => {
    setEditDevice({
      ...editDevice,
      deviceName: e.target.value,
    });
  };

  const onSaveEditDevice = () => {
    setOpenEditState(false);
    if (editDevice) {
      return CareProviderDeviceService.changeDeviceName(
        editDevice.id,
        editDevice.deviceName
      ).then(({ data }) => {
        toasterRef.current.show({
          message: i18n.vi.RENAME_DEVICE_SUCCESSFUL,
          intent: 'success',
          icon: 'info-sign',
        });
        updateDevice(data);
      });
    }
    throw Error(i18n.vi.RENAME_DEVICE_FAILED);
  };

  return (
    <Flex auto column {...props}>
      <Flex className="sl-list-header">
        <Flex w="30%">{i18n.vi.table.HEADER_TRUSTED_DEVICE}</Flex>
        <Flex w="25%">{i18n.vi.table.HEADER_DEVICE_NAME}</Flex>
        <Flex w="20%">{i18n.vi.table.HEADER_CREATE_DATE}</Flex>
        <Flex w="25%" />
      </Flex>
      {context.loading ? (
        <div className="loader" />
      ) : (
        devices.map((device: AdminApiModel.TrustedDeviceDto) => (
          <Flex key={device.identity} className="sl-list-item">
            <Flex className="sl-list-item-text" w="30%">
              {device.id}
            </Flex>
            <Flex className="sl-list-item-text" w="25%">
              {device.deviceName}
            </Flex>
            <Flex className="sl-list-item-text" w="20%">
              {new Date(device.createTime).toLocaleDateString('vi')}
            </Flex>
            <Flex justify="flex-start" align="center" w="25%">
              <Button
                onClick={onEditDevice(device)}
                text={i18n.vi.table.ACTION_RENAME}
              />
              <Popover>
                <Button text={i18n.vi.table.ACTION_REMOVE} intent="danger" />
                <Flex key="text" p="1rem" column>
                  <h4>{i18n.vi.REMOVE_DEVICE_CONFIRMATION}</h4>
                  <p>{i18n.vi.REMOVE_DEVICE_CONFIRMATION_MESSAGE}</p>
                  <Flex mt="1rem" justify="flex-end">
                    <Button
                      className={Classes.POPOVER_DISMISS}
                      style={{ marginRight: 10 }}
                    >
                      {i18n.vi.REMOVE_DEVICE_CONFIRMATION_CANCEL}
                    </Button>
                    <Button
                      intent={Intent.DANGER}
                      className={Classes.POPOVER_DISMISS}
                      onClick={onRevokeDevice(device)}
                    >
                      {i18n.vi.REMOVE_DEVICE_CONFIRMATION_OK}
                    </Button>
                  </Flex>
                </Flex>
              </Popover>
            </Flex>
          </Flex>
        ))
      )}
      <Dialog
        onClose={onCancelEdit}
        title={i18n.vi.editDeviceDialog.TITLE}
        isOpen={openEdit}
      >
        <div className={Classes.DIALOG_BODY}>
          <label>{i18n.vi.editDeviceDialog.DEVICE_NAME_LABEL}</label>
          <InputGroup
            value={editDevice ? editDevice.deviceName : ''}
            onChange={onTypeDeviceName}
            className={`bp3-large `} //${'bp3-intent-danger'}
            placeholder={i18n.vi.editDeviceDialog.DEVICE_NAME_PLACE_HOLDER}
          />
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button
            disabled={
              !editDevice ||
              !editDevice.deviceName ||
              editDevice.deviceName.length === 0
            }
            onClick={onSaveEditDevice}
            className="bp3-button bp3-large bp3-fill"
            intent={Intent.PRIMARY}
          >
            {i18n.vi.editDeviceDialog.ACTION_SAVE}
          </Button>
        </div>
      </Dialog>
      <Toaster ref={toasterRef} />
    </Flex>
  );
}

export { CareProdiverDeviceList };
