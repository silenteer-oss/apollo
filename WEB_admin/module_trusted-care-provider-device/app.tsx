import { Button, Dialog } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React, { useEffect, useState } from 'react';
import deviceService from './CareProviderDeviceService';
import { TrustedCareProviderDeviceContext } from './model';
import CareProviderDeviceListStyled from './views/CareProviderDeviceListStyled';
import AddTrustedCareProviderDevice from './views/TrustedCareProviderDeviceStyled';
import { AdminApiModel } from '@infrastructure/resource/AdminResource';
import { i18n } from './i18n';

function AppView() {
  const [open, setOpenState] = useState(false);
  const [devices, setDevices] = useState(
    [] as AdminApiModel.TrustedDeviceDto[]
  );
  const [loading, setLoading] = useState(true);

  const onCloseDialog = () => setOpenState(false);
  const onOpenDialog = () => setOpenState(true);
  const onAddDevice = (devices: AdminApiModel.TrustedDeviceDto[]) => {
    setDevices([...devices]);
    setOpenState(false);
  };

  const updateDevice = (device: AdminApiModel.TrustedDeviceDto) => {
    const foundIndex = devices.findIndex(value => value.id === device.id);
    if (foundIndex !== -1) {
      const updatedList = [...devices];
      updatedList[foundIndex] = device;
      setDevices(updatedList);
    }
  };

  useEffect(() => {
    deviceService.getAllDevices().then(deviceList => {
      setDevices(deviceList);
      setLoading(false);
    });
  }, []);

  return (
    <TrustedCareProviderDeviceContext.Provider
      value={{ devices, setDevices: onAddDevice, updateDevice, loading }}
    >
      <Flex auto column className="cy-care-provider-device-page">
        <Flex justify="flex-end">
          <Button
            className="bp3-button cy-add-btn"
            intent="primary"
            onClick={onOpenDialog}
          >
            {i18n.vi.ADD_TRUSTED_DEVICE}
          </Button>
        </Flex>
        <Flex auto pt="0.6rem">
          <CareProviderDeviceListStyled />
        </Flex>
        <Dialog
          isOpen={open}
          onClose={onCloseDialog}
          title={i18n.vi.addDeviceDialog.TITLE}
          canOutsideClickClose={false}
        >
          <AddTrustedCareProviderDevice />
        </Dialog>
      </Flex>
    </TrustedCareProviderDeviceContext.Provider>
  );
}

export { AppView };
