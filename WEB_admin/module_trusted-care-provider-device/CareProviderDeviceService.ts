import { seal } from '@infrastructure/crypto';
import {
  createPairingWs,
  MicronautsWebSocketClient,
} from '@infrastructure/websocket';
import { cryptoDatabase } from '@admin/services/client-database';
import {
  AdminApi,
  AdminApiModel,
} from '@infrastructure/resource/AdminResource';

let _ws: MicronautsWebSocketClient;

function getPhraseCode(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6)
    .toUpperCase();
}

function trustDevice(
  identity: string,
  deviceName: string,
  callback: (data: any, err?: Error) => void
) {
  _ws = createPairingWs(identity);

  _ws.onopen = () => {
    console.log('WS opened');
    AdminApi.addDevice({ identity, deviceName })
      .then(result => result.data)
      .catch(ex => callback(undefined, ex));
  };
  _ws.onclose = () => console.log('WS closed');
  _ws.onmessage = async (evt: MessageEvent) => {
    const { data } = evt;
    const responseFromCareProvider = JSON.parse(data);
    if (responseFromCareProvider.type === 'DEVICE_READY') {
      AdminApi.generateMemberDevice(identity).catch(e =>
        callback(responseFromCareProvider, e)
      );
    } else if (responseFromCareProvider.type === 'DEVICE_LOGIN') {
      getAllDevices().then(data => callback(data));
    } else if (
      responseFromCareProvider.type === 'CARE_PROVIDER_DEVICE_PUBLIC_KEY'
    ) {
      const { careProviderDevicePublicKey } = responseFromCareProvider;
      const careProviderKeyPair = await cryptoDatabase.keyPairStorage
        .get('CARE_PROVIDER')
        .then(data => data.value);
      const sealedCareProviderKeyPair = await seal(
        JSON.stringify(careProviderKeyPair),
        careProviderDevicePublicKey
      );

      _ws.send(
        JSON.stringify({
          type: 'CARE_PROVIDER_KEY_PAIR',
          careProviderKeyPair: sealedCareProviderKeyPair,
        })
      );
    } else if (responseFromCareProvider.type === 'CARE_PROVIDER_PAIRED') {
      _ws.send(JSON.stringify({ type: 'ADMIN_PAIRED' }));
      closeWS();
      getAllDevices().then(data => callback(data));
    }
  };
}

function revokeDevice(device: AdminApiModel.TrustedDeviceDto) {
  return AdminApi.revokeDevice(device.id);
}

function getAllDevices(): Promise<AdminApiModel.TrustedDeviceDto[]> {
  return AdminApi.getAllTrustedUserDeviceList().then(result => result.data);
}

function closeWS() {
  if (_ws) {
    _ws.close();
  }
}

function changeDeviceName(deviceId: string, deviceName: string) {
  return AdminApi.changeDeviceName(deviceId, { deviceName });
}

export default {
  getPhraseCode,
  trustDevice,
  getAllDevices,
  revokeDevice,
  closeWS,
  changeDeviceName,
};
