import React from 'react';
import ReactDOM from 'react-dom';
import { AuthenticationMainView } from './views/app';

import '@admin/assets/styles/index.scss';

ReactDOM.render(<AuthenticationMainView />, document.getElementById('root'));
