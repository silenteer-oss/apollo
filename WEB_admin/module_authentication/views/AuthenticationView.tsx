import { UNKNOW_ERROR } from '@admin/common/constants';
import { Button, Toaster } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import React, { useEffect, useRef, useState } from 'react';
import authService from '../AuthenticationService';
import { i18n } from '../i18n';

export interface AuthenticationViewProps {
  className?: string;
  setRecoverMode: Function;
}

let timeout;

function AuthenticationView(
  props: AuthenticationViewProps
): React.FunctionComponentElement<AuthenticationViewProps> {
  const [loading, setLoading] = useState(false);
  const [yourCode, setYourCode] = useState(authService.getPhraseCode());
  const [errorMessage, setErrorMessage] = useState('');
  const toasterRef = useRef<Toaster>(null);
  const codeInputRef = useRef<HTMLInputElement>(null);

  /////////////////////////////////////////////////////////////////////////////////////
  useEffect(() => {
    if (loading) {
      timeout = setTimeout(() => {
        setLoading(false);
        clearTimeout(timeout);
        authService.closeWS();
        toasterRef.current.show({
          message: i18n.vi.PAIRING_ERROR_MESSAGE,
          intent: 'danger',
          icon: 'error',
        });
      }, WEB_SOCKET_PAIRING_TIMEOUT);
    }

    return () => clearTimeout(timeout);
  }, [loading]);

  /////////////////////////////////////////////////////////////////////////////////////

  const onTrustDevice = () => {
    if (!codeInputRef.current.value.trim()) {
      setErrorMessage(i18n.vi.PAIRING_VALIDATION_MESSAGE);
      return;
    }

    setLoading(true);

    return authService.onAdminTrustDevice(
      yourCode + codeInputRef.current.value,
      (_, error) => {
        if (error) {
          setLoading(false);
          authService.closeWS();
          toasterRef.current.show(UNKNOW_ERROR);
          console.error(error);
        }
      }
    );
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onRenewCode = () => {
    setYourCode(authService.getPhraseCode());
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onSilentiumCodeInputChanged = () => {
    if (errorMessage) {
      setErrorMessage('');
    }
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onCancel = () => {
    setLoading(false);
    clearTimeout(timeout);
    authService.closeWS();
  };

  /////////////////////////////////////////////////////////////////////////////////////

  return (
    <div className="sl-login-box">
      <Flex {...props} auto column align="center" justify="center">
        <h1>{i18n.vi.PAIRING_CODE_LABEL}</h1>
        <Flex align="center" column>
          <input
            autoFocus
            ref={codeInputRef}
            className={`bp3-input bp3-large cy-code-input ${
              errorMessage ? 'bp3-intent-danger' : ''
              }`}
            placeholder={i18n.vi.PAIRING_CODE_PLACE_HOLDER}
            disabled={loading}
            onChange={onSilentiumCodeInputChanged}
          />
          <Flex mt="0.25rem" className="error">
            {errorMessage}
          </Flex>
        </Flex>
        <Flex column align="center" pt=".5rem">
          <p>{i18n.vi.PAIRING_INFO_MESSAGE}</p>
          <label className="ls-code-label">{yourCode}</label>
          <span className="sl-issue">
            {`${i18n.vi.PAIRING_CODE_WARNING_MESSAGE} `}
            {loading ? (
              <a onClick={onCancel}>{i18n.vi.PAIRING_CODE_CANCEL}</a>
            ) : (
                <a onClick={onRenewCode}>{i18n.vi.PAIRING_CODE_RENEW}</a>
              )}
          </span>
        </Flex>
        <Button
          intent="primary"
          onClick={onTrustDevice}
          className="bp3-button bp3-large bp3-fill cy-trust-btn"
          disabled={loading}
        >
          <Flex align="center">
            {loading ? i18n.vi.ACTION_TRUSTING : i18n.vi.ACTION_TRUST}
          </Flex>
        </Button>
        <br />
        <span>
          {i18n.vi.RECOVER_DESCRIPTION}{' '}
          <a
            className="recover-account-link"
            onClick={() => props.setRecoverMode(true)}
          >
            {i18n.vi.RECOVER_ACCOUNT_LABEL}
          </a>
        </span>
      </Flex>
      <Toaster ref={toasterRef} />
    </div>
  );
}

export { AuthenticationView };
