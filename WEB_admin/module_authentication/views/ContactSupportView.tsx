import React, { Dispatch, SetStateAction } from 'react';
import { Classes } from '@blueprintjs/core';

import { Flex } from '@design-system/core/components';
import { i18n } from '../i18n';
interface IContactSupportProps {
  className?: string;
  setRecoverMode: Dispatch<SetStateAction<boolean>>;
}
class ContactSupportView extends React.Component<IContactSupportProps> {
  constructor(props: IContactSupportProps) {
    super(props);
  }
  render() {
    const { className } = this.props;

    return (
      <div className={Classes.DIALOG_BODY}>
        <Flex align="center" className={className} column>
          <div>
            <img src="../assets/images/logo.svg" />
          </div>
          <div>
            <h1>{i18n.vi.CONTACT_SUPPORT}</h1>
          </div>
          <div className="account_lock">
            <label className="label_account_lock">{i18n.vi.LOCK_ACCOUNT}</label>
          </div>
          <Flex justify="space-around">
            <a onClick={this.cancelRecoverMode} className="btnLinkBack">
              {i18n.vi.BACK_TO_ADD_DEVICE}
            </a>
          </Flex>
        </Flex>
      </div>
    );
  }
  cancelRecoverMode = () => {
    const { setRecoverMode } = this.props;
    setRecoverMode(false);
  };
}

export { ContactSupportView };
