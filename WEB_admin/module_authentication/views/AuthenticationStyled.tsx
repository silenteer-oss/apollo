import { AuthenticationView } from './AuthenticationView';
import styled from 'styled-components';

export default styled(AuthenticationView)`
  width: 30rem;
  align-self: center;
  input,
  .ls-code-label {
    text-align: center;
    font-size: 2rem !important;
    letter-spacing: 1.4px;
    &::placeholder {
      font-size: 16px;
      text-transform: none;
      margin-top: 0px;
      text-align: center;
    }
  }

  .ls-code-label {
    margin-top: 8px;
  }

  .sl-issue {
    margin-top: 8px;
  }

  .recover-account-link {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    text-align: center;

    /* Primary */

    color: #137db8;
  }

  button {
    margin-top: 2rem;
  }
`;
