import styled from 'styled-components';
import { ContactSupportView } from './ContactSupportView';

export default styled(ContactSupportView)`
  width: 100%;

  .bp3-label {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #05304e;
  }

  .label-bold {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 32px;
    /* identical to box height, or 200% */

    letter-spacing: -0.005em;

    /* Text color 01 */

    color: #05304e;
  }

  .center-input input {
    text-align: center;
  }

  .account_lock {
    padding: 32px 0;
    max-width: 32rem;
  }

  .label_account_lock {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 28px;
    /* or 140% */

    text-align: center;

    /* Red */

    color: #f55e5e;
  }

  .btnLinkBack {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    text-align: center;

    /* Primary */

    color: #137db8;
    padding-top: 10px;
  }
`;
