import React, { Dispatch, SetStateAction } from 'react';
import {
  Alignment,
  Button,
  Classes,
  FormGroup,
  Icon,
  InputGroup,
  MenuItem,
  Radio,
  RadioGroup,
} from '@blueprintjs/core';

import { Box, Flex } from '@design-system/core/components';
import { i18n } from '../i18n';
import {
  Field,
  FieldProps,
  Form,
  Formik,
  FormikActions,
  FormikErrors,
} from 'formik';
import SecurityQuestionService from '@admin/module_backup/SecurityQuestionService';
import type { ISelectOption } from '@admin/module_backup/views/SecurityQuestionInfoModel';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { IItemRendererProps, Select } from '@blueprintjs/select';
import { getCssClass } from '@design-system/infrastructure/utils';

import {
  SecurityQuestion,
} from '@silenteer/hermes/resource/sysadmin-app-resource/model';

import type {
  SecurityQuestionAnswer,
} from '@silenteer/hermes/resource/sysadmin-app-resource/model';
import ContactSupportView from './ContactSupportViewStyled';

export interface ITrustedCareProviderRecoverProps {
  className?: string;
  setRecoverMode: Dispatch<SetStateAction<boolean>>;
}

interface ITrustedCareProviderRecoverState {
  redirect: boolean;
  accountLocked: boolean;
  filterOptions: ISelectOption[];
  errors?: any;
}

interface ITrustedCareProviderRecoveryValues {
  careProviderId: string;
  recoverMethod: string;
  question1: SecurityQuestion;
}

const SecretQuestionSelect = Select.ofType<string>();

export class TrustedCareProviderDeviceRecovery extends React.Component<
  ITrustedCareProviderRecoverProps & RouteComponentProps<any>,
  ITrustedCareProviderRecoverState
  > {
  private recoverValue = '';

  constructor(props: ITrustedCareProviderRecoverProps & RouteComponentProps<any>) {
    super(props);

    this.state = {
      redirect: false,
      accountLocked: false,
      errors: {},
      filterOptions: [
        {
          value: SecurityQuestion.CITYYOURPARENTMEET,
          label: 'Bố mẹ của bạn gặp nhau ở thành phố nào?',
        },
        {
          value: SecurityQuestion.YOURDELICIOUSFOOD,
          label: 'Món ăn bạn thích là món gì?',
        },
        {
          value: SecurityQuestion.YOURCHILDHOODNICKNAME,
          label: 'Biệt danh thời thơ ấu của bạn là gì?',
        },
        {
          value: SecurityQuestion.YOURFIRSTPETNAME,
          label: 'Tên thú cưng đầu tiên của bạn là gì?',
        },
        {
          value: SecurityQuestion.YOURCLOSESTFRIENDNAME,
          label: 'Tên của người bạn thân nhất của bạn?',
        },
        {
          value: SecurityQuestion.YOURFAVOURITECOLOR,
          label: 'Màu sắc mà bạn yêu thích là màu gì?',
        },
        {
          value: SecurityQuestion.YOURLUCKYNUMBER,
          label: 'Chữ số may mắn của bạn là gì?',
        },
        {
          value: SecurityQuestion.YOURFIRSTJOB,
          label: 'Công việc đầu tiên mà bạn làm là gì?',
        },
        {
          value: SecurityQuestion.YOURFIRSTPRIMARYSCHOOL,
          label: 'Tên trường tiểu học đầu tiên của bạn là gì?',
        },
      ],
    };
  }
  onChangeMethodValue = e => (this.recoverValue = e.target.value);

  cancelRecoverMode = () => {
    const { setRecoverMode } = this.props;
    setRecoverMode(false);
  };

  login4Restore = (
    values: {
      careProviderId: string;
      recoverMethod: string;
      question1: SecurityQuestion;
    },
    formikBag: FormikActions<ITrustedCareProviderRecoveryValues>
  ) => {
    const { careProviderId, recoverMethod, question1 } = values;

    let errors = {} as FormikErrors<ITrustedCareProviderRecoveryValues>;

    if (recoverMethod === 'CODE') {
      return this.verifyRecoveryCode(
        careProviderId,
        this.recoverValue.replace(' ', ''),
        formikBag,
        errors
      );
    } else {
      let questionAnswer: SecurityQuestionAnswer = {
        question: question1,
        answer: this.recoverValue,
      };
      return this.verifySecurityQuestion(
        careProviderId,
        questionAnswer,
        formikBag,
        errors
      );
    }
    // const { setRecoverMode } = this.props;
    // setRecoverMode(false);
  };

  verifyRecoveryCode = async (careProviderId, code, formikBag, errors) => {
    await SecurityQuestionService.loginByCode(careProviderId, code)
      .then(data => {
        this.setState({ redirect: true });
      })
      .catch(error => {
        if (error.response && error.response.status === 429) {
          this.setState({ accountLocked: true });
        }
        errors.careProviderId = i18n.vi.RECOVER_CODE_ERROR;

        this.setState({ errors });
        formikBag.setSubmitting(false);
      });
  };

  verifySecurityQuestion = async (
    careProviderId,
    questionAnswer,
    formikBag,
    errors
  ) => {
    await SecurityQuestionService.loginBySecurityQuestion(
      careProviderId,
      questionAnswer
    )
      .then(data => {
        this.setState({ redirect: true });
      })
      .catch(error => {
        if (error.response && error.response.status === 429) {
          this.setState({ accountLocked: true });
        }

        errors.careProviderId = i18n.vi.ANSWER_QUESTION_CODE_ERROR;
        this.setState({ errors });
        formikBag.setSubmitting(false);
      });
  };

  render() {
    const { className } = this.props;
    if (this.state.redirect) {
      this.props.history.push(`app`);
    }
    if (this.state.accountLocked) {
      return <ContactSupportView setRecoverMode={this.props.setRecoverMode} />;
    }
    return (
      <div className={Classes.DIALOG_BODY}>
        <Flex align="center" className={className} column>
          <div>
            <img src="../assets/images/logo.svg" />
          </div>
          <div>
            <h1>{i18n.vi.RECOVER_ACCOUNT_LABEL}</h1>
          </div>
          <div>
            <Formik
              initialValues={{
                careProviderId: '',
                recoverMethod: 'CODE',
                question1: '',
              }}
              onSubmit={this.login4Restore}
              render={({ setFieldValue, isSubmitting }) => {
                const { errors } = this.state;

                return (
                  <Form className="recoverForm">
                    <Field
                      className={'formLabel'}
                      name="careProviderId"
                      render={({ field }) => (
                        <FormGroup
                          label={i18n.vi.YOUR_ADMIN_ACCOUNT}
                          helperText={errors.careProviderId}
                          className={
                            errors.careProviderId ? Classes.INTENT_DANGER : ''
                          }
                        >
                          <InputGroup {...field} maxLength={50} required />
                        </FormGroup>
                      )}
                    />

                    <Field
                      className={'formLabel'}
                      name="recoverMethod"
                      render={({ field, form }) => (
                        <FormGroup>
                          <label className="label-bold">
                            {i18n.vi.CHOOSE_RECOVER_METHOD}
                          </label>
                          <br />
                          <RadioGroup
                            onChange={event =>
                              setFieldValue(
                                'recoverMethod',
                                event.currentTarget.value
                              )
                            }
                            selectedValue={field.value}
                          >
                            <Radio
                              label={i18n.vi.RECOVER_ANSWER_SECURITY_QUESTION}
                              value="QUESTION"
                            />
                            {field.value === 'QUESTION' && (
                              <>
                                <Box>
                                  <Field
                                    name="question1"
                                    render={({
                                      field,
                                      form,
                                    }: FieldProps<{}>) => {
                                      const onSelect = (item: string) => {
                                        const { setFieldValue } = form;
                                        setFieldValue(field.name, item);
                                      };

                                      return (
                                        <FormGroup
                                          label={i18n.vi.SECURITY_QUESTION_1}
                                          helperText={errors.question1}
                                          className={
                                            errors.question1
                                              ? Classes.INTENT_DANGER
                                              : ''
                                          }
                                        >
                                          <SecretQuestionSelect
                                            className={getCssClass(
                                              'sl-select cy-question1-select',
                                              errors.question1
                                                ? Classes.INTENT_DANGER
                                                : ''
                                            )}
                                            filterable={false}
                                            items={this.getAvailableOptions()}
                                            activeItem={field.value}
                                            onItemSelect={onSelect}
                                            itemRenderer={this.renderTitleItem}
                                            popoverProps={{
                                              minimal: true,
                                              captureDismiss: true,
                                              fill: true,
                                            }}
                                          >
                                            <Button
                                              fill
                                              alignText={Alignment.LEFT}
                                              text={
                                                this.state.filterOptions
                                                  .filter(questionOption => {
                                                    return (
                                                      field.value ===
                                                      questionOption.value
                                                    );
                                                  })
                                                  .map(item => item.label)
                                                  .shift() || <span />
                                              }
                                              rightIcon={
                                                <Icon icon="caret-down" />
                                              }
                                            />
                                          </SecretQuestionSelect>
                                        </FormGroup>
                                      );
                                    }}
                                  />
                                </Box>
                                <InputGroup
                                  onChange={this.onChangeMethodValue}
                                  placeholder={
                                    i18n.vi.RECOVER_ANSWER_PLACE_HOLDER
                                  }
                                  required
                                />
                              </>
                            )}

                            <Radio label={i18n.vi.RECOVER_CODE} value="CODE" />
                            {field.value === 'CODE' && (
                              <InputGroup
                                className="center-input"
                                onChange={this.onChangeMethodValue}
                                placeholder="_ _ _ _   _ _ _ _"
                                required
                              />
                            )}
                          </RadioGroup>
                        </FormGroup>
                      )}
                    />

                    <Button
                      intent="primary"
                      type="submit"
                      disabled={isSubmitting}
                      className="bp3-button bp3-large bp3-fill"
                    >
                      <Flex align="center">{i18n.vi.LOGIN}</Flex>
                    </Button>

                    <Flex justify="space-around">
                      <a
                        onClick={this.cancelRecoverMode}
                        className="btnLinkBack"
                      >
                        {i18n.vi.BACK_TO_ADD_DEVICE}
                      </a>
                    </Flex>
                  </Form>
                );
              }}
            />
          </div>
        </Flex>
      </div>
    );
  }
  getAvailableOptions = () => {
    const availableOptionsLeft = this.state.filterOptions;
    return availableOptionsLeft.map(item => item.value);
  };
  renderTitleItem = (
    title: string,
    { handleClick, modifiers }: IItemRendererProps
  ): JSX.Element => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        key={title}
        onClick={handleClick}
        text={this.state.filterOptions
          .filter(questionOption => {
            return title === questionOption.value;
          })
          .map(item => item.label)
          .shift()}
        shouldDismissPopover
      />
    );
  };
}

export const TrustedCareProviderDeviceRecover = withRouter(
  TrustedCareProviderDeviceRecovery
);
