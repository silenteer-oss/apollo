import React from 'react';
import Authentication from './AuthenticationStyled';
import TrustedCareProviderDeviceRecover from './TrustedCareProviderDeviceRecoverStyled';

interface IAuthenticationMainViewState {
  isRecoverMode: boolean;
}

export interface AuthenticationMainViewProps {
  className?: string;
}

class AuthenticationMainView extends React.Component<
  AuthenticationMainViewProps,
  IAuthenticationMainViewState
  > {
  constructor(props: Readonly<AuthenticationMainViewProps>) {
    super(props);
    this.state = {
      isRecoverMode: false,
    };
  }

  setRecoverMode = (value: boolean) => {
    this.setState({ isRecoverMode: value });
  };

  render() {
    const { isRecoverMode } = this.state;
    return (
      <>
        {isRecoverMode && (
          <TrustedCareProviderDeviceRecover
            setRecoverMode={this.setRecoverMode}
          />
        )}
        {!isRecoverMode && (
          <Authentication setRecoverMode={this.setRecoverMode} />
        )}
      </>
    );
  }
}

export { AuthenticationMainView };
