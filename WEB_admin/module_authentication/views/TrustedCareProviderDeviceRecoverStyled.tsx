import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';
import { TrustedCareProviderDeviceRecover } from './TrustedCareProviderDeviceRecover';

export default styled(TrustedCareProviderDeviceRecover)`
  width: 100%;

  .bp3-label {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #05304e;
  }

  .label-bold {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 32px;
    /* identical to box height, or 200% */

    letter-spacing: -0.005em;

    /* Text color 01 */

    color: #05304e;
  }

  .center-input input {
    text-align: center;
  }

  .btnLinkBack {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    text-align: center;

    /* Primary */

    color: #137db8;
    padding-top: 10px;
  }

  .bp3-radio {
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    /* or 143% */

    display: flex;
    align-items: center;

    /* Text color 01 */

    color: #05304e;
  }

  .bp3-input-group {
    background: #ffffff;
    /* Background 01 */

    border: 1px solid #dce0e4;
    box-sizing: border-box;
    border-radius: 4px;
  }

  .recoverForm {
    min-width: 500px;
  }
`;
