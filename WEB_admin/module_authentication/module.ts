import {
  AUTHENTICATION_MODULE_ID,
  AUTHENTICATION_VIEW_ID,
} from './module-config';
import { AuthenticationMainView } from './views/app';

export default {
  [AUTHENTICATION_MODULE_ID]: {
    [AUTHENTICATION_VIEW_ID]: AuthenticationMainView,
  },
};
