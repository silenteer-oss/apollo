import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';
import { createCareProviderKeyPair } from '@admin/common/config/KeyStorage';
import {
  createPairingWs,
  MicronautsWebSocketClient,
} from '@infrastructure/websocket';

let _ws: MicronautsWebSocketClient;

function getPhraseCode(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6);
}

async function login(ticket: string, identity: string) {
  await SysadminApi.organizationLogin({ ticket, identity });
  await createCareProviderKeyPair();
}

async function onAdminTrustDevice(
  identity = '',
  callback?: (data: any, err?: Error) => void
) {
  _ws = createPairingWs(identity);

  _ws.onmessage = (evt: MessageEvent) => {
    const { data } = evt;
    const responseFromSystem = JSON.parse(data);

    if (responseFromSystem.type === 'DEVICE_ADDED') {
      login(responseFromSystem.ticketId, identity)
        .then(() => {
          _ws.send(JSON.stringify({ type: 'ADMIN_PAIRED_SUCCESS' }));
        })
        .then(closeWS)
        .then(() => (location.href = location.origin + '/app'))
        .catch(e => {
          callback(null, e);
          closeWS();
        });
    }
  };

  _ws.onopen = (evt: Event) => {
    console.log(`connected to ${identity} channel`);
    SysadminApi.addTicket({ identity }).catch(e => {
      callback(null, e);
      closeWS();
    });
  };
}

function closeWS() {
  if (_ws) {
    _ws.close();
  }
}

export default {
  onAdminTrustDevice,
  getPhraseCode,
  closeWS,
};
