import { getUUID } from '@design-system/infrastructure/utils';
import { IMessageEvent } from '../models';

let _worker: Worker;
const _jobMap: {
  [key: string]: {
    resolve: (value: any) => void;
    reject: (reason: any) => void;
  };
} = {};

export function initMainThread(params: {
  worker: Worker;
  messageEventHandlers: Array<(messageEvent: IMessageEvent) => void>;
}): void {
  if (_worker) {
    throw new Error('A worker was created, shouldnt create more.');
  }

  const { worker, messageEventHandlers } = params;

  _worker = worker;
  _worker.onerror = error => console.error(error);
  _worker.onmessage = event => {
    const messageEvent = event.data as IMessageEvent;
    messageEventHandlers.forEach(messageEventHandler =>
      messageEventHandler(messageEvent)
    );
  };
}

export function terminateWorker(): void {
  if (_worker) {
    _worker.terminate();
  }
}

export function postMessage<T>(params: {
  type: string;
  data?: any;
}): Promise<T> {
  return new Promise((resolve, reject) => {
    const { type, data } = params;
    const id = getUUID();

    _jobMap[id] = { resolve, reject };

    _worker.postMessage({
      id,
      type,
      data,
    });
  });
}

export function handleMessage(messageEvent: IMessageEvent): void {
  const job = _jobMap[messageEvent.id];

  if (job) {
    if (messageEvent.status === 'RESOLVED') {
      job.resolve(messageEvent.data);
    } else if (messageEvent.status === 'REJECTED') {
      job.reject(messageEvent.data);
    }

    delete _jobMap[messageEvent.id];
  }
}
