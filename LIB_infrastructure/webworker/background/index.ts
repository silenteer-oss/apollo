import { IMessageEvent } from '../models';

const dedicatedWorker: DedicatedWorkerGlobalScope = self as any;

export { dedicatedWorker };

export function initBackgroundThread(
  ...messageEventHandlers: Array<(messageEvent: IMessageEvent) => Promise<void>>
): void {
  dedicatedWorker.addEventListener('message', async event => {
    const messageEvent = event.data as IMessageEvent;

    messageEventHandlers.forEach(messageEventHandler =>
      messageEventHandler(messageEvent)
    );
  });
}

export async function execute<TResult>(params: {
  id: string;
  type: string;
  func: () => Promise<TResult>;
}): Promise<void> {
  const { id, type, func } = params;

  try {
    const data = await func();

    handleResult({
      id,
      type,
      data,
    });
  } catch (error) {
    handleError({ id, type, error });
  }
}

export function handleResult(params: {
  id: string;
  type: string;
  data: any;
}): void {
  const { id, type, data } = params;

  dedicatedWorker.postMessage({
    id,
    type,
    status: 'RESOLVED',
    data,
  } as IMessageEvent);
}

export function handleError(params: {
  id: string;
  type: string;
  error: any;
}): void {
  const { id, type, error } = params;

  dedicatedWorker.postMessage({
    id,
    type,
    status: 'REJECTED',
    data: error.message,
  } as IMessageEvent);
}
