export interface IMessageEvent {
  id: string;
  status?: TMessageEventStatus;
  type: string;
  data?: any;
}

export type TMessageEventStatus = 'RESOLVED' | 'REJECTED';
