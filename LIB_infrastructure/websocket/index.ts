export * from '@silenteer/hermes/websocket';

export function getWsHostUrl(): string {
  if (!window) return undefined;

  const wsProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
  return `${wsProtocol}://${window.location.hostname}`;
}

import {
  createCommonWs as _createCommonWs,
  createPairingWs as _createPairingWs,
} from '@silenteer/hermes/websocket';
export const createCommonWs = () => _createCommonWs(getWsHostUrl());
export const createPairingWs = (pairingAddress: string) =>
  _createPairingWs(getWsHostUrl(), pairingAddress);
