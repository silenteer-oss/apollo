import { IMessageEvent } from '@infrastructure/webworker';
import { handleMessage } from '@infrastructure/webworker/main';
import { SIGNAL_MESSAGE_EVENT_TYPES } from '../models';

export function decryptMessageHandler(messageEvent: IMessageEvent): void {
  if (messageEvent.type === SIGNAL_MESSAGE_EVENT_TYPES.DECRYPT_MESSAGE) {
    handleMessage(messageEvent);
  }
}
