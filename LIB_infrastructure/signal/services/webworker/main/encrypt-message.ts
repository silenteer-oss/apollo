import { postMessage } from '@infrastructure/webworker/main';
import { SIGNAL_MESSAGE_EVENT_TYPES } from '../models';

export function encryptMessage(params: {
  conversationId: string;
  senderId: string;
  senderDeviceId: string;
  message: string;
}): Promise<{
  senderDevice: { senderId: string; senderDeviceId: string };
  message: string;
}> {
  return postMessage({
    type: SIGNAL_MESSAGE_EVENT_TYPES.ENCRYPT_MESSAGE,
    data: params,
  });
}
