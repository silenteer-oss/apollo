import { postMessage } from '@infrastructure/webworker/main';
import { SIGNAL_MESSAGE_EVENT_TYPES } from '../models';

export function decryptMessage(params: {
  conversationId: string;
  senderId: string;
  senderDeviceId: string;
  content: string;
}): Promise<{
  message: string;
}> {
  return postMessage({
    type: SIGNAL_MESSAGE_EVENT_TYPES.DECRYPT_MESSAGE,
    data: params,
  });
}
