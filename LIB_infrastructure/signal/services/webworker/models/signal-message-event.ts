import { TMessageEventStatus, IMessageEvent } from '@infrastructure/webworker';

export const SIGNAL_MESSAGE_EVENT_TYPES = {
  ENCRYPT_MESSAGE: 'ENCRYPT_MESSAGE',
  DECRYPT_MESSAGE: 'DECRYPT_MESSAGE',
};

export abstract class BaseEncryptMessageEvent implements IMessageEvent {
  id: string;
  status: TMessageEventStatus;
  type: string = SIGNAL_MESSAGE_EVENT_TYPES.ENCRYPT_MESSAGE;
  data: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    message: string;
  };
}

export abstract class BaseDecryptMessageEvent implements IMessageEvent {
  id: string;
  status: TMessageEventStatus;
  type: string = SIGNAL_MESSAGE_EVENT_TYPES.DECRYPT_MESSAGE;
  data: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    content: string;
  };
}
