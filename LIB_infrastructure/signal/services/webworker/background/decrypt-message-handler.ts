import { SignalAddress } from '@silenteer/hermes/signal';
import { IMessageEvent } from '@infrastructure/webworker';
import { execute } from '@infrastructure/webworker/background';
import { SIGNAL_MESSAGE_EVENT_TYPES, BaseDecryptMessageEvent } from '../models';
import { queueCipher } from '../../../services/biz';

export async function decryptMessageHandler(
  messageEvent: IMessageEvent
): Promise<void> {
  if (messageEvent.type === SIGNAL_MESSAGE_EVENT_TYPES.DECRYPT_MESSAGE) {
    const {
      id,
      type,
      data: { conversationId, senderId, senderDeviceId, content },
    } = messageEvent as BaseDecryptMessageEvent;

    await execute({
      id,
      type,
      func: () => {
        const address = new SignalAddress(
          conversationId,
          senderId,
          senderDeviceId
        );

        return new Promise((resolve, reject) => {
          return queueCipher.enqueueDecryptionJob({
            decryptionPayload: {
              address,
              ciphertext: content,
            },
            onDone: message => resolve({ message }),
            onError: error => reject(error),
          });
        });
      },
    });
  }
}
