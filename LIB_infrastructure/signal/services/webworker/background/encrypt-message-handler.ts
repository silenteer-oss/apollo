import { SignalAddress } from '@silenteer/hermes/signal';
import { IMessageEvent } from '@infrastructure/webworker';
import { execute } from '@infrastructure/webworker/background';
import { SIGNAL_MESSAGE_EVENT_TYPES, BaseEncryptMessageEvent } from '../models';
import { queueCipher } from '../../../services/biz';

export async function encryptMessageHandler(
  messageEvent: IMessageEvent
): Promise<void> {
  if (messageEvent.type === SIGNAL_MESSAGE_EVENT_TYPES.ENCRYPT_MESSAGE) {
    const {
      id,
      type,
      data: { conversationId, senderId, senderDeviceId, message },
    } = messageEvent as BaseEncryptMessageEvent;

    await execute({
      id,
      type,
      func: () => {
        const address = new SignalAddress(
          conversationId,
          senderId,
          senderDeviceId
        );

        return new Promise((resolve, reject) => {
          return queueCipher.enqueueEncryptionJob({
            encryptionPayload: {
              address,
              plaintext: message,
            },
            onDone: message =>
              resolve({ senderDevice: { senderId, senderDeviceId }, message }),
            onError: error => reject(error),
          });
        });
      },
    });
  }
}
