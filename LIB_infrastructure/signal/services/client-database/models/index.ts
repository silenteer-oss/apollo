export interface ISignalRecordSchema {
  key: string;
  value: Uint8Array;
}

export type TSignalSenderDeviceKey = 'CARE_PROVIDER';

export interface ISignalSenderDeviceSchema {
  key: TSignalSenderDeviceKey;
  value: {
    senderId: string;
    senderDeviceId: string;
  };
}
