import Dexie from 'dexie';
import { ISignalRecordSchema, ISignalSenderDeviceSchema } from './models';

export class SignalDatabase extends Dexie {
  recordStorage: Dexie.Table<ISignalRecordSchema, string>;
  senderDeviceStorage: Dexie.Table<ISignalSenderDeviceSchema, string>;

  public constructor() {
    super('signal-database');
    this.version(1).stores({
      'record-storage': 'key,value',
      'sender-device-storage': 'key,value',
    });
    this.recordStorage = this.table('record-storage');
    this.senderDeviceStorage = this.table('sender-device-storage');
  }
}
