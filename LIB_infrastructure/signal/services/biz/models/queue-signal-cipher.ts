import { QueueSignalCipher } from '@silenteer/hermes/signal';
import { signalStorage } from './signal-storage';

export const queueCipher = new QueueSignalCipher(signalStorage);
