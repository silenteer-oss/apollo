import {
  BaseSignalStorage,
  SignalInMemoryStorage,
} from '@silenteer/hermes/signal';
import { TSignalSenderDeviceKey, SignalDatabase } from '../../client-database';

const _signalDatabase = new SignalDatabase();

export const signalStorage: BaseSignalStorage = new SignalInMemoryStorage()
  .registerRecordSerializer((address, bytes) => {
    return _signalDatabase.recordStorage
      .put({ key: address, value: bytes })
      .then(() => {
        return;
      });
  })
  .registerRecordDeserializer(address => {
    return _signalDatabase.recordStorage
      .get(address)
      .then(item => (item ? item.value : undefined));
  })
  .registerSenderDeviceSerializer(senderDevice => {
    return _signalDatabase.senderDeviceStorage
      .put({
        key: 'CARE_PROVIDER' as TSignalSenderDeviceKey,
        value: senderDevice,
      })
      .then(() => {
        return;
      });
  })
  .registerSenderDeviceDeserializer(() => {
    return _signalDatabase.senderDeviceStorage
      .get('CARE_PROVIDER' as TSignalSenderDeviceKey)
      .then(item => (item ? item.value : undefined));
  });
