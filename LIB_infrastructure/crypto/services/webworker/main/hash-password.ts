import { postMessage } from '@infrastructure/webworker/main';
import { CRYPTO_MESSAGE_EVENT_TYPES } from '../models';

export function hashPassword(
  plainPassword: string,
  salt: string
): Promise<string> {
  return postMessage({
    type: CRYPTO_MESSAGE_EVENT_TYPES.HASH_PASSWORD,
    data: {
      plainPassword,
      salt,
    },
  });
}
