import { IMessageEvent } from '@infrastructure/webworker';
import { handleMessage } from '@infrastructure/webworker/main';
import { CRYPTO_MESSAGE_EVENT_TYPES } from '../models';

export function hashPasswordHandler(messageEvent: IMessageEvent): void {
  if (messageEvent.type === CRYPTO_MESSAGE_EVENT_TYPES.HASH_PASSWORD) {
    handleMessage(messageEvent);
  }
}
