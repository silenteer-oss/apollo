import { TMessageEventStatus, IMessageEvent } from '@infrastructure/webworker';

export const CRYPTO_MESSAGE_EVENT_TYPES = {
  HASH_PASSWORD: 'HASH_PASSWORD',
};

export abstract class BaseHashPasswordMessageEvent implements IMessageEvent {
  id: string;
  status: TMessageEventStatus;
  type: string = CRYPTO_MESSAGE_EVENT_TYPES.HASH_PASSWORD;
  data: {
    plainPassword: string;
    salt: string;
  };
}
