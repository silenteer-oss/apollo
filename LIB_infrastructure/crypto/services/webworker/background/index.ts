import { injectEngine as _injectEngine } from '@silenteer/hermes/crypto';
import { WebCryptoEngine } from '../../../crypto-web';

_injectEngine(WebCryptoEngine);

export * from './hash-password-handler';
