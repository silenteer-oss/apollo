import { hashPassword as originHashPassword } from '@silenteer/hermes/crypto';
import { IMessageEvent } from '@infrastructure/webworker';
import { execute } from '@infrastructure/webworker/background';
import {
  CRYPTO_MESSAGE_EVENT_TYPES,
  BaseHashPasswordMessageEvent,
} from '../models';

export async function hashPasswordHandler(
  messageEvent: IMessageEvent
): Promise<void> {
  if (messageEvent.type === CRYPTO_MESSAGE_EVENT_TYPES.HASH_PASSWORD) {
    const {
      id,
      type,
      data: { plainPassword, salt },
    } = messageEvent as BaseHashPasswordMessageEvent;

    await execute({
      id,
      type,
      func: () => originHashPassword(plainPassword, salt),
    });
  }
}
