import Dexie from 'dexie';
import { TCryptoKeyPairKey, ICryptoKeyPairSchema } from './models';

export * from './models';
export class CryptoDatabase extends Dexie {
  keyPairStorage: Dexie.Table<ICryptoKeyPairSchema, TCryptoKeyPairKey>;

  public constructor() {
    super('crypto-database');
    this.version(1).stores({
      'key-pair-storage': 'key,value',
    });
    this.keyPairStorage = this.table('key-pair-storage');
  }
}
