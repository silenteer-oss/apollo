export type TCryptoKeyPairKey = 'CARE_PROVIDER';

export interface ICryptoKeyPairSchema {
  key: TCryptoKeyPairKey;
  value: {
    pubKey: string;
    privKey: string;
  };
}
