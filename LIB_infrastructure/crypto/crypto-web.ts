import sodium from 'libsodium-wrappers';
import {
  ICryptoEngine,
  ISealKeyPair,
  ISignKeyPair,
} from '@silenteer/hermes/crypto';

async function sign(
  message: Uint8Array,
  privateKey: Uint8Array
): Promise<Uint8Array> {
  await sodium.ready;

  return sodium.crypto_sign_detached(message, privateKey);
}

async function verify(
  message: Uint8Array,
  signature: Uint8Array,
  publicKey: Uint8Array
): Promise<void> {
  await sodium.ready;

  const verified = sodium.crypto_sign_verify_detached(
    signature,
    message,
    publicKey
  );

  if (!verified) {
    throw new Error('Invalid signature');
  }
}

async function encrypt(
  iv: Uint8Array,
  key: Uint8Array,
  plaintext: Uint8Array
): Promise<Uint8Array> {
  await sodium.ready;
  return sodium.crypto_secretbox_easy(plaintext, iv, key);
}

async function decrypt(
  iv: Uint8Array,
  key: Uint8Array,
  ciphertext: Uint8Array
): Promise<Uint8Array> {
  await sodium.ready;
  return sodium.crypto_secretbox_open_easy(ciphertext, iv, key);
}

async function getRandomBytes(size: number): Promise<Uint8Array> {
  await sodium.ready;
  return sodium.randombytes_buf(size);
}

async function hash(
  outputSize: number,
  key: Uint8Array,
  data: Uint8Array
): Promise<Uint8Array> {
  await sodium.ready;

  return sodium.crypto_generichash(outputSize, data, key);
}

async function generateSignKeyPair(): Promise<ISignKeyPair> {
  await sodium.ready;

  const keyPair = sodium.crypto_sign_keypair();
  return {
    pubKey: keyPair.publicKey,
    privKey: keyPair.privateKey,
  };
}

async function generateSealKeyPair(): Promise<ISealKeyPair> {
  await sodium.ready;

  const keyPair = sodium.crypto_box_keypair();
  return {
    pubKey: keyPair.publicKey,
    privKey: keyPair.privateKey,
  };
}

async function seal(
  plaintext: Uint8Array,
  recipientPubKey: Uint8Array
): Promise<Uint8Array> {
  await sodium.ready;

  return sodium.crypto_box_seal(plaintext, recipientPubKey);
}

async function unseal(
  ciphertext: Uint8Array,
  recipientSealKeyPair: ISealKeyPair
): Promise<Uint8Array> {
  await sodium.ready;

  return sodium.crypto_box_seal_open(
    ciphertext,
    recipientSealKeyPair.pubKey,
    recipientSealKeyPair.privKey
  );
}

async function hashPassword(
  keyLength: number,
  plainPassword: Uint8Array,
  salt: Uint8Array,
  opsLimit: number,
  memLimit: number,
  algorithm: number
): Promise<Uint8Array> {
  await sodium.ready;

  return sodium.crypto_pwhash(
    keyLength,
    plainPassword,
    salt,
    opsLimit,
    memLimit,
    algorithm
  );
}

export const WebCryptoEngine: ICryptoEngine = {
  sign,
  verify,
  encrypt,
  decrypt,
  getRandomBytes,
  hash,
  generateSignKeyPair,
  generateSealKeyPair,
  seal,
  unseal,
  hashPassword,
};
