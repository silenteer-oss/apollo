import { injectEngine as _injectEngine } from '@silenteer/hermes/crypto';
import { WebCryptoEngine } from './crypto-web';

_injectEngine(WebCryptoEngine);

export * from '@silenteer/hermes/crypto';

export const injectEngine = () => {
  throw new Error('Please do not use this method');
};
