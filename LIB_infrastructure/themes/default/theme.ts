import {
  BaseMicroFrontendTheme,
} from '@design-system/themes/models';
import { IMicroFrontendTheme } from '@design-system/themes/models';
import { ICoreComponentsTheme } from '@design-system/core/models';
import * as CORE_STYLES_MODULE from '@design-system/core/styles';
import * as APP_STYLES_MODULE from './styles';

const { TYPO, OPACITY, RADIUS, SPACE, BREAKPOINT } = CORE_STYLES_MODULE;
const {
  COLOR,
  TYPO: {
    TYPO_FONT_FAMILY_DEFAULT,
    TYPO_FONT_SIZE_H1,
    TYPO_FONT_SIZE_H2,
    TYPO_FONT_SIZE_H3,
    TYPO_FONT_SIZE_H4,
    TYPO_FONT_SIZE_BODY_M,
    TYPO_FONT_SIZE_BODY_S,
    TYPO_FONT_SIZE_LINK,
    TYPO_FONT_WEIGHT_H1,
    TYPO_FONT_WEIGHT_H2,
    TYPO_FONT_WEIGHT_H3,
    TYPO_FONT_WEIGHT_H4,
    TYPO_FONT_WEIGHT_BODY_M,
    TYPO_FONT_WEIGHT_BODY_S,
    TYPO_FONT_WEIGHT_LINK,
    TYPO_LINE_HEIGHT_H1,
    TYPO_LINE_HEIGHT_H2,
    TYPO_LINE_HEIGHT_H3,
    TYPO_LINE_HEIGHT_H4,
    TYPO_LINE_HEIGHT_BODY_M,
    TYPO_LINE_HEIGHT_BODY_S,
    TYPO_LINE_HEIGHT_LINK,
  },
} = APP_STYLES_MODULE;

export interface IDefaultTheme<TViewsTheme>
  extends IMicroFrontendTheme<TViewsTheme> {
  // #region background
  background: {
    white: string;
    '01': string;
    '02': string;
    primary: {
      darker: string;
      base: string;
      lighter: string;
      contrast: {
        darker: string;
        base: string;
        lighter: string;
      };
    };
    second: {
      base: string;
      lighter: string;
    };
    error: {
      base: string;
      lighter: string;
      contrast: {
        base: string;
        lighter: string;
      };
    };
    warn: {
      base: string;
      lighter: string;
      contrast: {
        base: string;
        lighter: string;
      };
    };
    success: {
      base: string;
      lighter: string;
      contrast: {
        base: string;
        lighter: string;
      };
    };
  };
  // #endregion
  // #region foreground
  foreground: {
    white: string;
    '01': string;
    '02': string;
    '03': string;
    link: string;
    primary: {
      darker: string;
      base: string;
      lighter: string;
    };
    second: {
      base: string;
      lighter: string;
    };
    error: {
      base: string;
      lighter: string;
    };
    warn: {
      base: string;
      lighter: string;
    };
    success: {
      base: string;
      lighter: string;
    };
  };
  // #endregion
}

export abstract class BaseDefaultTheme<TViewsTheme>
  extends BaseMicroFrontendTheme<TViewsTheme>
  implements IDefaultTheme<TViewsTheme> {
  id = 'DefaultTheme';
  isLightTheme = true;
  isDarkTheme = false;
  typography = {
    h1: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_H1,
      fontWeight: TYPO_FONT_WEIGHT_H1,
      lineHeight: TYPO_LINE_HEIGHT_H1,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    h2: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_H2,
      fontWeight: TYPO_FONT_WEIGHT_H2,
      lineHeight: TYPO_LINE_HEIGHT_H2,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    h3: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_H3,
      fontWeight: TYPO_FONT_WEIGHT_H3,
      lineHeight: TYPO_LINE_HEIGHT_H3,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    h4: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_H4,
      fontWeight: TYPO_FONT_WEIGHT_H4,
      lineHeight: TYPO_LINE_HEIGHT_H4,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    // bodyL fallback to bodyM
    bodyL: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_BODY_M,
      fontWeight: TYPO_FONT_WEIGHT_BODY_M,
      lineHeight: TYPO_LINE_HEIGHT_BODY_M,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    bodyM: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_BODY_M,
      fontWeight: TYPO_FONT_WEIGHT_BODY_M,
      lineHeight: TYPO_LINE_HEIGHT_BODY_M,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    bodyS: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_BODY_S,
      fontWeight: TYPO_FONT_WEIGHT_BODY_S,
      lineHeight: TYPO_LINE_HEIGHT_BODY_S,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    link: {
      color: COLOR.COLOR_TEXT_LINK,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_LINK,
      fontWeight: TYPO_FONT_WEIGHT_LINK,
      lineHeight: TYPO_LINE_HEIGHT_LINK,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
    // quote fallback to bodyM
    quote: {
      color: COLOR.COLOR_TEXT_01,
      fontFamily: TYPO_FONT_FAMILY_DEFAULT,
      fontSize: TYPO_FONT_SIZE_BODY_M,
      fontWeight: TYPO_FONT_WEIGHT_BODY_M,
      lineHeight: TYPO_LINE_HEIGHT_BODY_M,
      letterSpacing: TYPO.TYPO_LETTER_SPACING_DEFAULT,
    },
  };
  background = {
    white: COLOR.COLOR_BACKGROUND_WHITE,
    '01': COLOR.COLOR_BACKGROUND_01,
    '02': COLOR.COLOR_BACKGROUND_02,
    primary: {
      darker: COLOR.COLOR_PALETTE_BLUE_DARKER,
      base: COLOR.COLOR_PALETTE_BLUE_BASE,
      lighter: COLOR.COLOR_PALETTE_BLUE_LIGHTER,
      contrast: {
        darker: COLOR.COLOR_TEXT_WHITE,
        base: COLOR.COLOR_TEXT_WHITE,
        lighter: COLOR.COLOR_TEXT_01,
      },
    },
    second: {
      base: COLOR.COLOR_PALETTE_ORANGE_BASE,
      lighter: COLOR.COLOR_PALETTE_ORGANGE_LIGHTER,
      contrast: {
        darker: COLOR.COLOR_TEXT_WHITE,
        base: COLOR.COLOR_TEXT_WHITE,
      },
    },
    error: {
      base: COLOR.COLOR_PALETTE_RED_BASE,
      lighter: COLOR.COLOR_PALETTE_RED_LIGHTER,
      contrast: {
        base: COLOR.COLOR_TEXT_WHITE,
        lighter: COLOR.COLOR_PALETTE_RED_BASE,
      },
    },
    warn: {
      base: COLOR.COLOR_PALETTE_ORANGE_BASE,
      lighter: COLOR.COLOR_PALETTE_ORGANGE_LIGHTER,
      contrast: {
        base: COLOR.COLOR_TEXT_WHITE,
        lighter: COLOR.COLOR_PALETTE_ORANGE_BASE,
      },
    },
    success: {
      base: COLOR.COLOR_PALETTE_GREEN_BASE,
      lighter: COLOR.COLOR_PALETTE_GREEN_LIGHTER,
      contrast: {
        base: COLOR.COLOR_TEXT_WHITE,
        lighter: COLOR.COLOR_PALETTE_GREEN_BASE,
      },
    },
  };
  foreground = {
    white: COLOR.COLOR_TEXT_WHITE,
    '01': COLOR.COLOR_TEXT_01,
    '02': COLOR.COLOR_TEXT_02,
    '03': COLOR.COLOR_TEXT_03,
    link: COLOR.COLOR_TEXT_LINK,
    primary: {
      darker: COLOR.COLOR_PALETTE_BLUE_DARKER,
      base: COLOR.COLOR_PALETTE_BLUE_BASE,
      lighter: COLOR.COLOR_PALETTE_BLUE_LIGHTER,
    },
    second: {
      base: COLOR.COLOR_PALETTE_ORANGE_BASE,
      lighter: COLOR.COLOR_PALETTE_ORGANGE_LIGHTER,
    },
    error: {
      base: COLOR.COLOR_PALETTE_RED_BASE,
      lighter: COLOR.COLOR_PALETTE_RED_LIGHTER,
    },
    warn: {
      base: COLOR.COLOR_PALETTE_ORANGE_BASE,
      lighter: COLOR.COLOR_PALETTE_ORGANGE_LIGHTER,
    },
    success: {
      base: COLOR.COLOR_PALETTE_GREEN_BASE,
      lighter: COLOR.COLOR_PALETTE_GREEN_LIGHTER,
    },
  };
  space = {
    xxs: SPACE.SPACE_XXS,
    xs: SPACE.SPACE_XS,
    s: SPACE.SPACE_S,
    m: SPACE.SPACE_M,
    l: SPACE.SPACE_L,
    xl: SPACE.SPACE_XL,
    xxl: SPACE.SPACE_XXL,
    xxxl: SPACE.SPACE_XXXL,
  };
  breakpoint = {
    '360px': BREAKPOINT.BREAKPOINT_360PX,
    '400px': BREAKPOINT.BREAKPOINT_400PX,
    '480px': BREAKPOINT.BREAKPOINT_480PX,
    '600px': BREAKPOINT.BREAKPOINT_600PX,
    '720px': BREAKPOINT.BREAKPOINT_720PX,
    '840px': BREAKPOINT.BREAKPOINT_840PX,
    '960px': BREAKPOINT.BREAKPOINT_960PX,
    '1024px': BREAKPOINT.BREAKPOINT_1024PX,
    '1280px': BREAKPOINT.BREAKPOINT_1280PX,
    '1440px': BREAKPOINT.BREAKPOINT_1440PX,
    '1600px': BREAKPOINT.BREAKPOINT_1600PX,
    '1920px': BREAKPOINT.BREAKPOINT_1920PX,
  };
  opacity = {
    87: OPACITY.OPACITY_87,
    60: OPACITY.OPACITY_60,
    38: OPACITY.OPACITY_38,
  };
  radius = {
    '2px': RADIUS.RADIUS_2PX,
    '4px': RADIUS.RADIUS_4PX,
    '8px': RADIUS.RADIUS_8PX,
    circle: RADIUS.RADIUS_CIRCLE,
  };
}

// ------------------------------------------------------------------------------------------- //

export function getAllComponentTheme<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): ICoreComponentsTheme {
  return {
    ...getTypographyTheme(theme),
    ...getLoadingStateTheme(theme),
  };
}

export function getTypographyTheme<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): ICoreComponentsTheme {
  return {
    typography: {
      ...theme.typography,
    },
  };
}

export function getLoadingStateTheme<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): ICoreComponentsTheme {
  const { background } = theme;

  return {
    loadingState: {
      backgroundColor: background.white,
      primaryBorderColor: background.primary.base,
      secondaryBorderColor: background['01'],
    },
  };
}
