import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomDatePickerStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { background, foreground, space } = theme;

  return `
    .bp3-datepicker {
      .bp3-icon-calendar {
        color: ${foreground.primary.base} !important;
      }

      .DayPicker {
        min-width: ${scaleSpacePx(60)};
        width: 100%;
      }

      .DayPicker-NavButton {
        min-width: initial;

        &.bp3-disabled {
          background: none;
        }
      }

      .DayPicker-Months {
        display: flex;

        .DayPicker-Month {
          flex: 1;
          width: 100%;

          .DayPicker-Caption {
            .bp3-datepicker-caption {
              margin: 0 ${scaleSpacePx(9)} ${space.xxs};

              .bp3-icon-double-caret-vertical {
                right: 0 !important;
              }
            }
          }
        }
      }

      .DayPicker-Day {
        &:hover {
          background-color: ${background['02']};
        }
        &.DayPicker-Day--selected {
          color: ${background.second.base};
          background-color: ${background.second.lighter};

          &:hover {
            color: ${background.second.base};
            background-color: ${background.second.lighter};
          }
        }
        &:not(.DayPicker-Day--selected).DayPicker-Day--today {
          background-color: ${background['01']};
        }
      }
    }
  `;
}
