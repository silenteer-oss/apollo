import { scaleSpacePx, mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomButtonStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, background, space, typography } = theme;

  return `
    a.bp3-button {
      min-height: ${scaleSpacePx(5)};
    }

    .bp3-button {
      min-width: ${scaleSpacePx(35)};
      padding: ${space.xs} ${space.s};
      box-shadow: none !important;
      background-image: none !important;

      ${mixTypography('bodyM', {
        color: typography.bodyM.color,
        fontFamily: typography.bodyM.fontFamily,
        fontSize: typography.bodyM.fontSize,
        fontWeight: typography.bodyM.fontWeight,
        lineHeight: typography.bodyM.lineHeight,
        letterSpacing: typography.bodyM.letterSpacing,
      })}

      &.bp3-large {
        padding: ${scaleSpacePx(3)} ${space.s};
      }

      &.bp3-disabled {
        &:not([class*=bp3-intent-]),
        &[class*=bp3-intent-] {
          .bp3-button-text {
            color: ${foreground['03']};
          }
          background-color: ${background['01']};
        }
      }

      &:not(.bp3-disabled) {
        &:not(.bp3-minimal) {
          &:not([class*=bp3-intent-]) {
            background-color: ${background['01']};
          }

          &[class*=bp3-intent-] {
            .bp3-button-text {
              color: ${foreground.white};
            }
          }
          &.bp3-intent-primary {
            background-color: ${background.primary.base};
          }
          &.bp3-intent-success {
            background-color: ${background.success.base};
          }
          &.bp3-intent-warning {
            background-color: ${background.warn.base};
          }
          &.bp3-intent-danger {
            background-color: ${background.error.base};
          }
        }

        &.bp3-minimal {
          background: none !important;

          &:not([class*=bp3-intent-]) {
            color: ${foreground['01']};
            .bp3-button-text {
              color: ${foreground['01']};
            }
          }

          &[class*=bp3-intent-] {
            &.bp3-intent-primary {
              color: ${foreground.primary.base};
              .bp3-button-text {
                color: ${foreground.primary.base} !important;
              }
            }
            &.bp3-intent-success {
              color: ${foreground.success.base};
              .bp3-button-text {
                color: ${foreground.success.base} !important;
              }
            }
            &.bp3-intent-warning {
              color: ${foreground.warn.base};
              .bp3-button-text {
                color: ${foreground.warn.base} !important;
              }
            }
            &.bp3-intent-danger {
              color: ${foreground.error.base};
              .bp3-button-text {
                color: ${foreground.error.base} !important;
              }
            }
          }
        }
      }
    }
  `;
}
