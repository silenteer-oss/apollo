import { IDefaultTheme } from '../../../../theme';

export function mixCustomHtmlTableStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space, background, foreground } = theme;

  return `
    table.bp3-html-table {
      width: 100%;
      border-bottom: 1px solid ${background['01']};
      border-color: ${background['01']};

      thead {
        background-color: ${background['02']};
      }

      td, th {
        padding: ${space.s};
        color: ${foreground['01']};
      }

      &.bp3-interactive {
        tbody tr{
          &:hover {
            td {
              background-color: ${background['02']};
            }
          }
        }
      }
    }
  `;
}
