import { IDefaultTheme } from '../../../../theme';

export function mixCustomPopoverStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  return `
    .bp3-popover {
      box-shadow: 0px 8px 24px rgba(16, 22, 26, 0.2), 0px 2px 4px rgba(16, 22, 26, 0.2), 0px 0px 0px rgba(16, 22, 26, 0.1);

      .bp3-popover-arrow {
        z-index: 20;
        margin-top: 1px;

        &:before {
          box-shadow: none;
        }
      }
    }
  `;
}
