import { scaleSpacePx, mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomDialogStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { background, foreground, space, typography } = theme;

  return `
    .bp3-dialog {
      padding-bottom: 0;
      border-radius: 0;
      background-color: ${background.white};

      &.bp3-dialog-large {
        margin: 0;
        width: calc(100vw - ${scaleSpacePx(10)});
        height: calc(100vh - ${scaleSpacePx(10)});

        &.bp3-dialog-content-center {
          .bp3-dialog-body {
          }
        }

        &.bp3-dialog-content-scrollable {
          .bp3-dialog-body {
            height: calc(100vh - ${scaleSpacePx(10)} - ${scaleSpacePx(15)} * 2);
            overflow: auto;
          }
        }
      }

      &.bp3-dialog-fullscreen {
        margin: 0;
        width: 100vw;
        height: 100vh;

        &.bp3-dialog-content-scrollable {
          .bp3-dialog-body {
            height: calc(100vh - ${scaleSpacePx(15)} * 2);
            overflow: auto;
          }
        }
      }

      .bp3-dialog-header {
        height: ${scaleSpacePx(15)};
        padding-left: ${space.m};
        padding-right: ${space.m};

        .bp3-icon {
          margin-right: ${space.xs};
          color: ${foreground['01']};
        }

        .bp3-heading {
          ${mixTypography('h1', {
            color: typography.h1.color,
            fontFamily: typography.h1.fontFamily,
            fontSize: typography.h1.fontSize,
            fontWeight: typography.h1.fontWeight,
            lineHeight: typography.h1.lineHeight,
            letterSpacing: typography.h1.letterSpacing,
          })}
        }

        .bp3-dialog-close-button {
          min-width: initial;
        }
      }

      .bp3-dialog-body {
        margin: 0;
        padding: ${space.m};
      }

      .bp3-dialog-footer {
        display: flex;
        align-items: center;
        height: ${scaleSpacePx(15)};
        margin: 0;
        padding: 0 ${space.m};
      }
    }
  `;
}
