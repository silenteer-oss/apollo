import { mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomInputStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space, typography, background } = theme;

  return `
    .bp3-input,
    textarea.bp3-input {
      padding: ${space.xs} ${space.s} !important;
      border: 1px solid ${background['01']};
      box-shadow: none;

      ${mixTypography('bodyM', {
        color: typography.bodyM.color,
        fontFamily: typography.bodyM.fontFamily,
        fontSize: typography.bodyM.fontSize,
        fontWeight: typography.bodyM.fontWeight,
        lineHeight: typography.bodyM.lineHeight,
        letterSpacing: typography.bodyM.letterSpacing,
      })}

      &:not([class*=bp3-intent-]) {
        &:focus {
          border: 1px solid ${background.second.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-primary,
      &.bp3-intent-primary:focus {
        border: 1px solid ${background.second.base};
        box-shadow: none;
      }

      &.bp3-intent-success,
      &.bp3-intent-success:focus {
        border: 1px solid ${background.success.base};
        box-shadow: none;
      }

      &.bp3-intent-warning,
      &.bp3-intent-warning:focus {
        border: 1px solid ${background.warn.base};
        box-shadow: none;
      }

      &.bp3-intent-danger,
      &.bp3-intent-danger:focus {
        border: 1px solid ${background.error.base};
        box-shadow: none;
      }
    }

    .bp3-input {
      height: initial;

      &:focus {
        box-shadow: none;
      }

      &.bp3-fill {
        width: calc(100% - ${space.s} * 2);
      }
    }
  `;
}
