import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomControlsStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, background, space } = theme;

  return `
    .bp3-control {
      &.bp3-align-right {
        padding-right: ${space.xs};
      }
      &:not(.bp3-align-right) {
        padding-left: ${space.xs};

        .bp3-control-indicator {
          margin-left: -${space.xs};
        }
      }

      &:not(.bp3-disabled):hover {
        > input:not(checked)~.bp3-control-indicator {
          background-color: ${background.white};
        }
      }

      &.bp3-radio {
        > input {
          width: ${space.s};
          height: ${space.s};

          &:checked~.bp3-control-indicator {
            background-color: ${background.white};
            background-image: none;
            border-color: ${background.second.base};
            box-shadow: none;

            &:before {
              background-image: radial-gradient(${background.second.base},${
    background.second.base
  } 32%,transparent 40%);
            }
          }
        }
      }

      &.bp3-checkbox {
        > input {
          width: ${space.s};
          height: ${space.s};

          &:checked~.bp3-control-indicator {
            background-color: ${background.second.base} !important;
            background-image: none;
            border-color: ${background.second.base};
            box-shadow: none;

            &:before {
              background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E");
            }
          }
        }
      }

      > input:focus~.bp3-control-indicator {
        outline: none;
        outline-offset: 0;
      }

      .bp3-control-indicator {
        width: ${scaleSpacePx(4)};
        height: ${scaleSpacePx(4)};
        margin-right: ${space.xs};
        border: 1px solid ${foreground['03']};
        background-color: ${background.white};
        background-image: none;
        box-shadow: none;

        &:before {
          width: ${scaleSpacePx(4)};
          height: ${scaleSpacePx(4)};
        }
      }
    }
  `;
}
