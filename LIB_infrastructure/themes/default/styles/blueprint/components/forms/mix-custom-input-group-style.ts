import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomInputGroupStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { background, space } = theme;

  return `
    .bp3-input-group {
      &:not([class*=bp3-intent-]) {
        .bp3-input:focus {
          border: 1px solid ${background.second.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-primary {
        .bp3-input,
        .bp3-input:focus  {
          border: 1px solid ${background.second.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-success {
        .bp3-input,
        .bp3-input:focus  {
          border: 1px solid ${background.success.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-warning {
        .bp3-input,
        .bp3-input:focus  {
          border: 1px solid ${background.warn.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-danger {
        .bp3-input,
        .bp3-input:focus  {
          border: 1px solid ${background.error.base};
          box-shadow: none;
        }
      }

      > .bp3-icon {
        margin: 10px 8px 10px 16px;
        ~ .bp3-input {
          width: calc(100% - ${scaleSpacePx(10)} - ${space.s});
          padding-left: ${scaleSpacePx(10)} !important;
        }
      }

      .bp3-input {
        width: calc(100% - ${space.s} * 2);
      }
    }
  `;
}
