import { mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomFormGroupStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, typography } = theme;

  return `
    .bp3-form-group {
      &.bp3-disabled {
        .bp3-label,
        .bp3-text-muted,
        .bp3-form-helper-text,
        .bp3-icon {
          color: ${foreground['03']} !important;;
        }
      }

      &:not(.bp3-disabled) {
        .bp3-icon {
          color: ${foreground.primary.base};
        }

        &.bp3-intent-primary {
          .bp3-form-helper-text,
          .bp3-icon {
            color: ${foreground.primary.base};
          }
        }

        &.bp3-intent-success {
          .bp3-form-helper-text,
          .bp3-icon {
            color: ${foreground.success.base};
          }
        }

        &.bp3-intent-warning {
          .bp3-form-helper-text,
          .bp3-icon {
            color: ${foreground.warn.base};
          }
        }

        &.bp3-intent-danger {
          .bp3-form-helper-text,
          .bp3-icon {
            color: ${foreground.error.base};
          }
        }
      }

      &.bp3-inline {
        align-items: center;
      }

      .bp3-form-helper-text {
        ${mixTypography('bodyS', {
          color: typography.bodyS.color,
          fontFamily: typography.bodyS.fontFamily,
          fontSize: typography.bodyS.fontSize,
          fontWeight: typography.bodyS.fontWeight,
          lineHeight: typography.bodyS.lineHeight,
          letterSpacing: typography.bodyS.letterSpacing,
        })}
      }
    }
  `;
}
