import { IDefaultTheme } from '../../../../theme';

export function mixCustomLabelStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, space } = theme;

  return `
    label.bp3-label {
      margin-bottom: ${space.xs};

      &.bp3-disabled {
        color: ${foreground['03']};
      }
    }
  `;
}
