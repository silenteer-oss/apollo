import { IDefaultTheme } from '../../../../theme';

export function mixCustomAlertStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space } = theme;

  return `
    .bp3-alert {
      padding: ${space.m} !important;

      &.bp3-alert-fill {
        .bp3-alert-footer {
          justify-content: space-between;

          > .bp3-button {
            flex: 1;
          }
        }
      }

      .bp3-alert-footer {
        margin-top: ${space.m};

        > .bp3-button {
          margin-left: 0;
          + .bp3-button {
            margin-right: ${space.m};
          }
        }
      }
    }
  `;
}
