import { mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomMenuStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space, typography, foreground, background } = theme;

  return `
    .bp3-menu {
      padding: 0;
      box-shadow: 0px 8px 24px rgba(16, 22, 26, 0.2), 0px 2px 4px rgba(16, 22, 26, 0.2), 0px 0px 0px rgba(16, 22, 26, 0.1);

      .bp3-menu-item {
        padding ${space.xs} ${space.s};

        &:hover {
          background-color: ${background['02']};
        }
        &.bp3-active {
          background-color: ${background.second.base};
        }

        &.bp3-intent-primary {
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.primary.base};
          }
        }
        &.bp3-intent-primary:hover,
        &.bp3-intent-primary.bp3-active {
          background-color: ${background.second.base};
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.white};
          }
        }

        &.bp3-intent-success {
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.success.base};
          }
        }
        &.bp3-intent-success:hover,
        &.bp3-intent-success.bp3-active {
          background-color: ${background.success.base};
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.white};
          }
        }

        &.bp3-intent-warning {
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.warn.base};
          }
        }
        &.bp3-intent-warning:hover
        &.bp3-intent-warning.bp3-active {
          background-color: ${background.warn.base};
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.white};
          }
        }

        &.bp3-intent-danger {
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.error.base};
          }
        }
        &.bp3-intent-danger:hover,
        &.bp3-intent-danger.bp3-active {
          background-color: ${background.error.base};
          > div,
          > .bp3-menu-item-label {
            color: ${foreground.white};
          }
        }

        > div {
          ${mixTypography('bodyM', {
            color: typography.bodyM.color,
            fontFamily: typography.bodyM.fontFamily,
            fontSize: typography.bodyM.fontSize,
            fontWeight: typography.bodyM.fontWeight,
            lineHeight: typography.bodyM.lineHeight,
            letterSpacing: typography.bodyM.letterSpacing,
          })}
        }
        > .bp3-menu-item-label {
          ${mixTypography('bodyM', {
            color: foreground['03'],
            fontFamily: typography.bodyM.fontFamily,
            fontSize: typography.bodyM.fontSize,
            fontWeight: typography.bodyM.fontWeight,
            lineHeight: typography.bodyM.lineHeight,
            letterSpacing: typography.bodyM.letterSpacing,
          })}
        }
      }
    }
  `;
}
