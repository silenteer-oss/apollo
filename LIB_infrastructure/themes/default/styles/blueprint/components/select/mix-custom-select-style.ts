import { IDefaultTheme } from '../../../../theme';

export function mixCustomSelectStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, background, radius } = theme;
  return `
    .bp3-select-popover {
      .bp3-menu {
        box-shadow: none;
      }

      .bp3-popover-content {
        padding: 0;
      }
    }

    .sl-select {
      &:not([class*=bp3-intent-]) {
        .bp3-popover-open {
          .bp3-button {
            border: 1px solid ${background.second.base};
          }
        }
      }

      &.bp3-intent-primary {
        .bp3-button {
          border: 1px solid ${background.second.base};
        }
      }

      &.bp3-intent-success {
        .bp3-button {
          border: 1px solid ${background.success.base};

          .bp3-icon {
            color: ${foreground.success.base};
          }
        }
      }

      &.bp3-intent-warning {
        .bp3-button {
          border: 1px solid ${background.warn.base};

          .bp3-icon {
            color: ${foreground.warn.base};
          }
        }
      }

      &.bp3-intent-danger {
        .bp3-button {
          border: 1px solid ${background.error.base};

          .bp3-icon {
            color: ${foreground.error.base};
          }
        }
      }

      .bp3-button {
        background-color: initial !important;
        border: 1px solid ${background['01']};
        border-radius: ${radius['4px']};

        .bp3-icon {
          color: ${foreground.primary.base};
        }
      }
    }
  `;
}
