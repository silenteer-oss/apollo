import { setAlpha } from '@design-system/infrastructure/utils';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomDividerStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground } = theme;

  return `
    .bp3-divider {
      border-bottom-color: ${setAlpha(foreground['03'], 0.3)};
      border-right-color: ${setAlpha(foreground['03'], 0.3)};
    }
  `;
}
