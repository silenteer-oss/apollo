import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomTagStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space, background, foreground } = theme;

  return `
    .bp3-tag {
      padding: 0 ${space.xs};
      min-height: ${scaleSpacePx(4)};
      background-color: ${background['01']};

      > .bp3-fill {
        color: ${foreground['02']}
      }

      .bp3-tag-remove {
        margin-top: 0;
        margin-bottom: 0;
        padding: 0;

        .bp3-icon {
          color: ${foreground['02']}
        }
      }
    }
  `;
}
