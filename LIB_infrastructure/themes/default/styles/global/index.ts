import * as CORE_STYLES_MODULE from '@design-system/core/styles';
import { mixNormalize } from '../normalize';
import { IDefaultTheme } from '../../theme';

const { mixTypography } = CORE_STYLES_MODULE;

export function mixGlobalStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const {
    typography: { h1, h2, h3, h4, bodyM, link, quote },
    space,
  } = theme;

  return `
    ${mixNormalize()}

    html {
      ${mixTypography('bodyM', {
        color: bodyM.color,
        fontFamily: bodyM.fontFamily,
        fontSize: bodyM.fontSize,
        fontWeight: bodyM.fontWeight,
        lineHeight: bodyM.lineHeight,
        letterSpacing: bodyM.letterSpacing,
      })}
    }

    h1 {
      ${mixTypography('h1', {
        color: h1.color,
        fontFamily: h1.fontFamily,
        fontSize: h1.fontSize,
        fontWeight: h1.fontWeight,
        lineHeight: h1.lineHeight,
        letterSpacing: h1.letterSpacing,
      })}
      margin-bottom: ${space.m};
    }
    h2 {
      ${mixTypography('h2', {
        color: h2.color,
        fontFamily: h2.fontFamily,
        fontSize: h2.fontSize,
        fontWeight: h2.fontWeight,
        lineHeight: h2.lineHeight,
        letterSpacing: h2.letterSpacing,
      })}
      margin-bottom: ${space.m};
    }
    h3 {
      ${mixTypography('h3', {
        color: h3.color,
        fontFamily: h3.fontFamily,
        fontSize: h3.fontSize,
        fontWeight: h3.fontWeight,
        lineHeight: h3.lineHeight,
        letterSpacing: h3.letterSpacing,
      })}
      margin-bottom: ${space.m};
    }
    h4 {
      ${mixTypography('h4', {
        color: h4.color,
        fontFamily: h4.fontFamily,
        fontSize: h4.fontSize,
        fontWeight: h4.fontWeight,
        lineHeight: h4.lineHeight,
        letterSpacing: h4.letterSpacing,
      })}
      margin-bottom: ${space.s};
    }
    p {
      ${mixTypography('bodyM', {
        color: bodyM.color,
        fontFamily: bodyM.fontFamily,
        fontSize: bodyM.fontSize,
        fontWeight: bodyM.fontWeight,
        lineHeight: bodyM.lineHeight,
        letterSpacing: bodyM.letterSpacing,
      })}
    }
    a,
    a > div,
    a > span {
      ${mixTypography('link', {
        color: link.color,
        fontFamily: link.fontFamily,
        fontSize: link.fontSize,
        fontWeight: link.fontWeight,
        lineHeight: link.lineHeight,
        letterSpacing: link.letterSpacing,
      })}
    }
    blockquote {
      ${mixTypography('quote', {
        color: quote.color,
        fontFamily: quote.fontFamily,
        fontSize: quote.fontSize,
        fontWeight: quote.fontWeight,
        lineHeight: quote.lineHeight,
        letterSpacing: quote.letterSpacing,
      })}
    }
    br {
      margin-bottom: ${space.s};
    }
    ul {
      padding-left: ${space.s};
    }
  `;
}
