export function getTextAlignClass(value?: string): string {
  return value ? `sl-text-align-${value}` : '';
}
