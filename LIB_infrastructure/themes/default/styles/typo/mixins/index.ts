import { TypoOptionType } from '@design-system/core/styles'
import {
  mixTypography as coreMixTypography,
} from '@design-system/core/styles';
import { FONT_FAMILY } from '../font-family';
import { FONT_SIZE } from '../font-size';
import { FONT_WEIGHT } from '../font-weight';
import { LINE_HEIGHT } from '../line-height';

export function mixTypography(
  type: 'h1' | 'h2' | 'h3' | 'bodyM' | 'bodyS' | 'link',
  options?: TypoOptionType
): string {
  let _options = {
    fontFamily: FONT_FAMILY.TYPO_FONT_FAMILY_DEFAULT,
    ...options,
  } as TypoOptionType;

  switch (type) {
    case 'h1':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_H1,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_H1,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_H1,
      };
      break;
    case 'h2':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_H2,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_H2,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_H2,
      };
      break;
    case 'h3':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_H3,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_H3,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_H3,
      };
      break;
    case 'bodyM':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_BODY_M,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_BODY_M,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_BODY_M,
      };
      break;
    case 'bodyS':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_BODY_S,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_BODY_S,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_BODY_S,
      };
      break;
    case 'link':
      _options = {
        ..._options,
        fontSize: FONT_SIZE.TYPO_FONT_SIZE_LINK,
        fontWeight: FONT_WEIGHT.TYPO_FONT_WEIGHT_LINK,
        lineHeight: LINE_HEIGHT.TYPO_LINE_HEIGHT_LINK,
      };
      break;
  }

  return coreMixTypography(type, _options);
}
