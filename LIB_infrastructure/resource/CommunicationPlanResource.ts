import { DefaultApi } from '@silenteer/hermes/resource/communication-plan-app-resource';

export const CommunicationPlanApi = new DefaultApi({
  basePath: window.location.origin,
});

import * as CommunicationPlanApiModel from '@silenteer/hermes/resource/communication-plan-app-resource/model';
export { CommunicationPlanApiModel };
