import { DefaultApi } from '@silenteer/hermes/resource/chat-app-resource';

export const ChatApi = new DefaultApi({ basePath: window.location.origin });

import * as ChatApiModel from '@silenteer/hermes/resource/chat-app-resource/model';
export { ChatApiModel };
