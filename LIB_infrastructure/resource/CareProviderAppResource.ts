import { DefaultApi } from '@silenteer/hermes/resource/careprovider-app-resource';

export const CareProviderApi = new DefaultApi({
  basePath: window.location.origin,
});

import * as CareProviderApiModel from '@silenteer/hermes/resource/careprovider-app-resource/model';
export { CareProviderApiModel };
