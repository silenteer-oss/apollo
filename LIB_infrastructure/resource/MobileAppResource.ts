import { DefaultApi } from '@silenteer/hermes/resource/mobile-app-resource';

export const MobileApi = new DefaultApi({ basePath: window.location.origin });

import * as MobileApiModel from '@silenteer/hermes/resource/mobile-app-resource/model';
export { MobileApiModel };
