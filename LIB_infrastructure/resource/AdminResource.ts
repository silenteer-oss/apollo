import { DefaultApi } from '@silenteer/hermes/resource/admin-app-resource';

export const AdminApi = new DefaultApi({ basePath: window.location.origin });

import * as AdminApiModel from '@silenteer/hermes/resource/admin-app-resource/model';
export { AdminApiModel };
