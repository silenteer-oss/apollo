import { DefaultApi } from '@silenteer/hermes/resource/authentication-resource';

export const AuthenticationApi = new DefaultApi({
  basePath: window.location.origin,
});

import * as AuthenticationApiModel from '@silenteer/hermes/resource/authentication-resource/model';
export { AuthenticationApiModel };
