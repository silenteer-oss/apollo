import { DefaultApi } from '@silenteer/hermes/resource/profile-resource';

export const ProfileApi = new DefaultApi({ basePath: window.location.origin });

import * as ProfileApiModel from '@silenteer/hermes/resource/profile-resource/model';
export { ProfileApiModel };
