import { DefaultApi } from '@silenteer/hermes/resource/sysadmin-app-resource';

export const SysadminApi = new DefaultApi({ basePath: window.location.origin });

import * as SysadminApiModel from '@silenteer/hermes/resource/sysadmin-app-resource/model';
export { SysadminApiModel };
