import { DefaultApi } from '@silenteer/hermes/resource/appointment-app-resource';

export const AppointmentApi = new DefaultApi({
  basePath: window.location.origin,
});

import * as AppointmentApiModel from '@silenteer/hermes/resource/appointment-app-resource/model';
export { AppointmentApiModel };
