const path = require('path'); //eslint-disable-line
const isDocker = require('is-docker')();

module.exports = function(config) {
  config.set({
    frameworks: ['mocha'],

    files: [
      { pattern: 'node_modules/expect.js/index.js' },
      { pattern: 'spec/**/*.ts' },
    ],

    preprocessors: {
      'spec/**/*.ts': ['webpack'],
    },

    webpack: {
      mode: 'development',
      devtool: '#inline-source-map',
      node: { global: true, fs: 'empty' },
      resolve: {
        extensions: ['.ts', '.js'],
        modules: ['node_modules', '../../../hermes/node_modules'],
        alias: {
          '@silenteer/hermes': path.resolve(__dirname, '../../../hermes/src'),
          '@infrastructure': path.resolve(__dirname, '../src'),
          '@test': path.resolve(__dirname, './src'),
        },
      },
      module: {
        rules: [
          {
            test: /\.ts$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
          },
        ],
      },
    },

    reporters: ['dots'],

    browsers: ['ChromeCustom'],
    customLaunchers: {
      ChromeCustom: {
        base: 'ChromeHeadless',
        // We must disable the Chrome sandbox when running Chrome inside Docker (Chrome's sandbox needs
        // more permissions than Docker allows by default)
        flags: isDocker ? ['--no-sandbox'] : []
      }
    },

    singleRun: true,

    client: {
      mocha: {
        timeout: 5000,
      },
    },
  });
};
