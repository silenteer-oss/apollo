import {
  SignalAddress,
  SignalSessionBuilder,
  SignalInMemoryStorage,
  SignalCipher,
} from '@infrastructure/signal';

export class SignalClient {
  name: string;
  deviceId: string;
  storage: SignalInMemoryStorage;

  constructor(name, deviceId = 'device1') {
    this.name = name;
    this.deviceId = deviceId;

    this.storage = new SignalInMemoryStorage();
    this.storage.registerRecordSerializer(() => Promise.resolve());
    this.storage.registerRecordDeserializer(() =>
      Promise.resolve(new Uint8Array())
    );
  }

  getSignalAddress(groupId: string) {
    return new SignalAddress(groupId, this.name, this.deviceId);
  }

  async createKeyDistributionTo(client: SignalClient, groupId = 'group1') {
    const sessionBuilder = new SignalSessionBuilder(this.storage);
    const result = await sessionBuilder.create(
      client.getSignalAddress(groupId)
    );
    return result.distributionMessage;
  }

  async processKeyDistributionFrom(
    client: SignalClient,
    keyDistribution: string,
    groupId = 'group1'
  ) {
    const sessionBuilder = new SignalSessionBuilder(this.storage);
    await sessionBuilder.process(
      client.getSignalAddress(groupId),
      keyDistribution
    );
  }

  async encryptMessageToSendTo(
    client: SignalClient,
    plaintext: string,
    groupId = 'group1'
  ) {
    const cipher = new SignalCipher(
      this.storage,
      client.getSignalAddress(groupId)
    );
    return await cipher.encrypt(plaintext);
  }

  async decryptMessageFrom(
    client: SignalClient,
    ciphertext: string,
    groupId = 'group1'
  ) {
    const cipher = new SignalCipher(
      this.storage,
      client.getSignalAddress(groupId)
    );
    return await cipher.decrypt(ciphertext);
  }
}
