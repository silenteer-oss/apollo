import {
  seal,
  unseal,
  generateSealKeyPair,
  getRandomBytes,
  generateSignKeyPair,
  sign,
  verify,
} from '@infrastructure/crypto';

describe('Crypto', () => {
  it('could unseal what it sealed', async () => {
    const randomContent = await getRandomBytes(99);
    const sealKeyPair = await generateSealKeyPair();

    const sealed = await seal(randomContent, sealKeyPair.pubKey);
    const unsealed = await unseal(sealed, sealKeyPair);

    expect(unsealed).to.equal(randomContent);
  });

  it('could verify what it signed', async () => {
    const randomContent = await getRandomBytes(99);
    const signKeyPair = await generateSignKeyPair();

    const signature = await sign(randomContent, signKeyPair.privKey);
    await verify(randomContent, signature, signKeyPair.pubKey);
  });
});
