module.exports = {
  extends: ['../../LIB_design-system/src/infrastructure/configs/common/eslint.js'],
  env: {
    mocha: true,
  },
  globals: {
    expect: "readonly"
  }
};
