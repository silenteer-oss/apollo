export async function setupBrowser(): Promise<boolean> {
  // Magic for Cypress, because cy.stub() is stupid !!!
  if (window['Cypress']) {
    console.log('Running e2e');
    return true;
  }

  let _persisted = await navigator.storage.persisted();
  if (_persisted) {
    console.warn('Storage has been persisted.');
    return true;
  }

  _persisted = await navigator.storage.persist();
  if (_persisted) {
    console.warn('Storage is persisted successfully.');
    return true;
  }

  const _failedPersistError = 'Failed to persist storage';
  if (!Notification) {
    console.error(_failedPersistError);
    return false;
  }

  const _permission = await Notification.requestPermission();
  if (_permission === 'granted') {
    _persisted = await navigator.storage.persist();
    if (_persisted) {
      console.warn('Storage is persisted successfully.');
      return true;
    }

    console.error(_failedPersistError);
    return false;
  }

  console.error(
    'Failed to persist storage because notification permission is denied.'
  );
  return false;
}
