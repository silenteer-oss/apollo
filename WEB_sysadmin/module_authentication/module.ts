import {
  AUTHENTICATION_MODULE_ID,
  AUTHENTICATION_VIEW_ID,
} from './module-config';
import LoginView from './view/login.styled';

export default {
  [AUTHENTICATION_MODULE_ID]: {
    [AUTHENTICATION_VIEW_ID]: LoginView,
  },
};
