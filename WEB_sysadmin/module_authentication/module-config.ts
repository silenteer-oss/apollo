import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const AUTHENTICATION_MODULE_ID = 'authentication';

export const AUTHENTICATION_VIEW_ID = 'AuthenticationView';

export interface IModuleViews {
  [AUTHENTICATION_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: AUTHENTICATION_MODULE_ID,
    name: 'Authentication Module',
    publicAssetPath: `/module/authentication`,
    views: {
      AuthenticationView: AUTHENTICATION_VIEW_ID,
    },
  };
}
