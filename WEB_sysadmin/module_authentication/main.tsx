import React from 'react';
import ReactDOM from 'react-dom';
import LoginView from '@sysadmin/module_authentication/view/login.styled';

import '@sysadmin/assets/styles/index.scss';

ReactDOM.render(<LoginView />, document.getElementById('root'));
