import { AuthenticationApi } from '@infrastructure/resource/AuthenticationResource';

interface GoogleUser {
  getAuthResponse: () => { id_token: string };
  getBasicProfile: () => { getEmail: () => string };
}

function successLogin(googleUser: GoogleUser) {
  const auth = googleUser.getAuthResponse();

  return AuthenticationApi.googleLogin({ idToken: auth.id_token })
    .then(() => {
      location.href = location.origin + '/app';
    })
    .catch(e => console.log(e));
}

export default {
  successLogin,
};
