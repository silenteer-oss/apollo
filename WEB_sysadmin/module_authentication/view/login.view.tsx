import { Button } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
// import GoogleSVG from '@sysadmin/assets/images/google.svg';
import { GOOGLE_AUTH } from '@sysadmin/common/google-auth';
import React from 'react';
import { RouteProps } from 'react-router-dom';
import authService from '../auth.service';

const imagePath = new URL("../../assets/images/google.svg", import.meta.url)

function LoginView(
  props: RouteProps & { className?: string }
): React.FunctionComponentElement<object> {
  function onFailure(error: any) {
    console.log(error);
  }

  React.useEffect(() => {
    window['gapi'].load('auth2', () => {
      const auth2 = window['gapi'].auth2.init({
        // eslint-disable-next-line @typescript-eslint/camelcase
        client_id: GOOGLE_AUTH.clientId,
        cookiepolicy: GOOGLE_AUTH.cookiepolicy,
      });
      auth2.attachClickHandler(
        document.getElementById('google-btn'),
        {},
        authService.successLogin,
        onFailure
      );
    });
  });

  const { className } = props;

  return (
    <Flex auto align="center" className={className} column>
      <nav className="bp3-navbar bp3-dark">
        <div className="bp3-navbar-group bp3-align-left">
          <div className="bp3-navbar-heading">Silentium</div>
        </div>
      </nav>
      <Flex
        justify="center"
        align="center"
        column
        className="sl-login-box"
        auto
      >
        <h1>Log In</h1>
        <Button id="google-btn" className="bp3-button bp3-large">
          <Flex>
            <img src={imagePath.pathname} width="24" />
            &nbsp;Continue with Google&nbsp;
          </Flex>
        </Button>
        <p>
          {`By clicking "Continue with Google" above, you acknowledge that you
          have read and understood, and agree to Silentium's and that you have
          read and understood, and consent to Silentium's .`}
        </p>
      </Flex>
    </Flex>
  );
}

export { LoginView };
