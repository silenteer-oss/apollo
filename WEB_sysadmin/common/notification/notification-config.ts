import { IToastProps } from '@blueprintjs/core';

export const unknownError: IToastProps = {
  message: 'Oops! Something went wrong. Please try again.',
  intent: 'danger',
  icon: 'error',
};
