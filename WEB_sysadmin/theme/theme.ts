import type {
  ThemedBaseStyledInterface,
  BaseThemedCssFunction,
  BaseWithThemeFnInterface,
  ThemeProviderProps,
  ThemedStyledProps,
  Interpolation,
  InterpolationFunction,
  GlobalStyleComponent,
} from 'styled-components';

import originStyled, {
  css as originCss,
  createGlobalStyle as originCreateGlobalStyle,
  withTheme as originWithTheme,
  ThemeProvider as OriginThemeProvider,
  ThemeConsumer as OriginThemeConsumer,
  keyframes
} from 'styled-components';

import type { CSSObject } from 'styled-components';
import {
  BaseDefaultTheme,
} from '@infrastructure/themes/default';

import type {
  IDefaultTheme,
} from '@infrastructure/themes/default';

export { getAllComponentTheme } from '@infrastructure/themes/default';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IViewsTheme { }

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ISysAdminTheme extends IDefaultTheme<IViewsTheme> { }

export class SysAdminTheme extends BaseDefaultTheme<IViewsTheme> { }

// -------------------------------------------------------------------- //

export function getAllViewTheme(theme: ISysAdminTheme): IViewsTheme {
  return {};
}

// -------------------------------------------------------------------- //

const styled = originStyled as ThemedBaseStyledInterface<ISysAdminTheme>;
const css = originCss as BaseThemedCssFunction<ISysAdminTheme>;
const withTheme = originWithTheme as BaseWithThemeFnInterface<ISysAdminTheme>;
const createGlobalStyle = originCreateGlobalStyle as <P extends object = {}>(
  first:
    | TemplateStringsArray
    | CSSObject
    | InterpolationFunction<ThemedStyledProps<P, ISysAdminTheme>>,
  ...interpolations: Array<Interpolation<ThemedStyledProps<P, ISysAdminTheme>>>
) => GlobalStyleComponent<P, ISysAdminTheme>;
const ThemeProvider = OriginThemeProvider as React.ComponentClass<
  ThemeProviderProps<ISysAdminTheme, ISysAdminTheme>,
  any
>;
const ThemeConsumer = OriginThemeConsumer as React.ExoticComponent<
  React.ConsumerProps<ISysAdminTheme>
>;

export {
  styled,
  css,
  createGlobalStyle,
  keyframes,
  withTheme,
  ThemeProvider,
  ThemeConsumer,
};

export const theme = new SysAdminTheme();
