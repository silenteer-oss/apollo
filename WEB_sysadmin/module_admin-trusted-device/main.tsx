import React from 'react';
import ReactDOM from 'react-dom';
import { AppView } from './app';

import '@sysadmin/assets/styles/index.scss';

function Main() {
  return <AppView />;
}

ReactDOM.render(<Main />, document.getElementById('root'));
