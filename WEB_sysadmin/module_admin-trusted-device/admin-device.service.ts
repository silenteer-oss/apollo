import {
  createPairingWs,
  MicronautsWebSocketClient,
} from '@infrastructure/websocket';
import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';
import { AppName } from '@silenteer/hermes/resource/sysadmin-app-resource/model';

let _ws: MicronautsWebSocketClient;
export interface Device {
  id: string;
  careProviderId: string;
  createTime: number;
  deviceName: string;
  careProviderName?: string;
  appName: AppName;
  apiKey?: string;
  appUid?: string;
}

export const EditDeviceMode = {
  EDIT_DEVICE_NAME: 'EDIT_DEVICE_NAME',
  EDIT_CARE_PROVIDER_INFO: 'EDIT_CARE_PROVIDER_INFO',
};

function getPhraseCode(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6);
}

function addDevice(
  identity = '',
  careProviderName = '',
  callback?: (data: Device[], error?: Error) => void
) {
  _ws = createPairingWs(identity);

  _ws.onmessage = (evt: MessageEvent) => {
    const { data } = evt;
    try {
      const parsedDate = JSON.parse(data);
      const { type } = parsedDate;

      if (type === 'ADMIN_PAIRED_SUCCESS') {
        return getAllDevices().then(({ data }) => {
          callback && callback(data);
          closeWS();
        });
      }
    } catch (e) {
      closeWS();
      callback(null, e);
    }
  };

  _ws.onopen = (evt: Event) => {
    console.log(`connected to ${identity} channel`);
    SysadminApi.addOrganization({ identity, careProviderName })
      .catch(e => {
        callback(null, e);
        closeWS();
      })
      .catch(ex => callback(undefined, ex));
  };
}

function getAllDevices(): any {
  return SysadminApi.getAllDevice();
}

function closeWS() {
  if (_ws) {
    _ws.close();
  }
}

export default {
  getPhraseCode,
  addDevice,
  getAllDevices,
  closeWS,
};
