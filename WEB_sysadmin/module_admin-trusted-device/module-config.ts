import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const ADMIN_TRUSTED_DEVICE_MODULE_ID = 'AdminTrustedDeviceModule';

export const ADMIN_TRUSTED_DEVICE_VIEW_ID = 'AdminTrustedDeviceView';

export interface IModuleViews {
  [ADMIN_TRUSTED_DEVICE_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: ADMIN_TRUSTED_DEVICE_MODULE_ID,
    name: 'Admin Trusted Module',
    publicAssetPath: `/WEB_sysadmin/module_admin-trusted-device`,
    views: {
      AdminTrustedDeviceView: ADMIN_TRUSTED_DEVICE_VIEW_ID,
    },
  };
}
