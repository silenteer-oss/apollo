import { Flex } from '@design-system/core/components';
import React from 'react';
import { Device, EditDeviceMode } from '../admin-device.service';
import {
  Button,
  Dialog,
  Classes,
  InputGroup,
  Intent,
  Toaster,
  HTMLTable,
  Divider,
  ButtonGroup,
  Tag,
} from '@blueprintjs/core';
import { useState } from 'react';
import { useRef } from 'react';
import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';
import StyledModelEditSms from './model-edit-sms-token.styled';
import { AppName } from '@silenteer/hermes/resource/sysadmin-app-resource/model';

export interface AdminDeviceListProps {
  className?: string;
  devices: Device[];
  updateDevice: Function;
  loading?: boolean;
  validateCareProviderName?: (deviceName: string) => string;
}

function AdminDeviceListView(props: AdminDeviceListProps) {
  const { devices = [] } = props;

  const [editDevice, setEditDevice] = useState<Device>(undefined);
  const [showEdit, setShowEdit] = useState(false);
  const [infoChanged, setInfoChanged] = useState(false);
  const [
    careProviderNameErrorMessage,
    setCareProviderNameErrorMessage,
  ] = useState('');
  const [editMode, setEditMode] = useState('');
  const [editTitle, setEditTitle] = useState('');
  const [editPlaceHolder, setEditPlaceHolder] = useState('');
  const [isOpenSms, setOpenSms] = useState(false);

  const toasterRef = useRef<Toaster>(null);

  const setTitleAndPlaceHolderModalEdit = (editMode: string) => {
    let title = '';
    let placeHolder = '';
    switch (editMode) {
      case EditDeviceMode.EDIT_DEVICE_NAME:
        title = "Edit device's name";
        placeHolder = "Device's name";
        break;
      case EditDeviceMode.EDIT_CARE_PROVIDER_INFO:
        title = "Edit care provider's info";
        placeHolder = "care provider's info";
        break;
    }
    setEditTitle(title);
    setEditPlaceHolder(placeHolder);
  };

  const onEditDevice = (device: Device, mode: string) => {
    switch (mode) {
      case EditDeviceMode.EDIT_CARE_PROVIDER_INFO:
        setEditMode(EditDeviceMode.EDIT_CARE_PROVIDER_INFO);
        setTitleAndPlaceHolderModalEdit(EditDeviceMode.EDIT_CARE_PROVIDER_INFO);
        break;
      case EditDeviceMode.EDIT_DEVICE_NAME:
        setEditMode(EditDeviceMode.EDIT_DEVICE_NAME);
        setTitleAndPlaceHolderModalEdit(EditDeviceMode.EDIT_DEVICE_NAME);
        break;

      default:
        setEditMode(EditDeviceMode.EDIT_DEVICE_NAME);
        setTitleAndPlaceHolderModalEdit(EditDeviceMode.EDIT_DEVICE_NAME);
    }

    setEditDevice(device);
    setShowEdit(true);
    setInfoChanged(false);
    setCareProviderNameErrorMessage('');
  };

  const getCurrentEditValue = (editDevice: Device) => {
    if (editMode === EditDeviceMode.EDIT_CARE_PROVIDER_INFO) {
      return editDevice.careProviderName;
    }
    return editDevice.deviceName;
  };

  const onTypeName = e => {
    if (editMode === EditDeviceMode.EDIT_CARE_PROVIDER_INFO) {
      setEditDevice({
        ...editDevice,
        careProviderName: e.target.value,
      });
    } else {
      setEditDevice({
        ...editDevice,
        deviceName: e.target.value,
      });
    }

    const oldName = (devices.find(device => device.id === editDevice.id) || {})
      .deviceName;
    setInfoChanged(oldName !== e.target.value);

    const { validateCareProviderName } = props;
    const error =
      validateCareProviderName && validateCareProviderName(e.target.value);

    setCareProviderNameErrorMessage(error);
  };

  const onSaveEditDevice = () => {
    if (editDevice) {
      setShowEdit(false);
      let editAPI = undefined;
      switch (editMode) {
        case EditDeviceMode.EDIT_CARE_PROVIDER_INFO:
          editAPI = SysadminApi.changeCareProviderInfo(editDevice.id, {
            careProviderName: editDevice.careProviderName,
            appName: editDevice.appName,
          });
          break;
        default:
          editAPI = SysadminApi.changeAdminDeviceName(editDevice.id, {
            deviceName: editDevice.deviceName,
          });
      }
      return editAPI.then(({ data }) => {
        const { updateDevice } = props;
        updateDevice(data);
        toasterRef.current.show({
          message: 'Update successfully',
          intent: 'success',
          icon: 'info-sign',
        });
      });
    } else {
      toasterRef.current.show({
        message: 'Update failed',
        intent: 'danger',
        icon: 'info-sign',
      });
    }
  };

  const onSelectAppName = e => {
    setEditDevice({
      ...editDevice,
      appName: e.target.value,
    });
    const oldAppName = (
      devices.find(device => device.id === editDevice.id) || {}
    ).appName;

    setInfoChanged(oldAppName !== e.target.value);
  };

  const setOpenUpdateSmsApi = (device: Device) => {
    setEditDevice(device);
    setOpenSms(true);
  };

  const onChangeSmsConfigSuccess = (device: Device) => {
    setOpenSms(false);
    const { updateDevice } = props;
    updateDevice(device);
    toasterRef.current.show({
      message: 'Update successfully',
      intent: 'success',
      icon: 'info-sign',
    });
  };

  const renderEditCareProvider = () => {
    return (
      <Dialog
        onClose={() => setShowEdit(false)}
        title={editTitle}
        isOpen={showEdit}
      >
        <div className={Classes.DIALOG_BODY}>
          {editMode === EditDeviceMode.EDIT_CARE_PROVIDER_INFO && (
            <div style={{ marginBottom: 15 }}>
              <label style={{ marginRight: 15 }}>App name</label>
              <div className="bp3-select .bp3-large">
                <select onChange={e => onSelectAppName(e)}>
                  {Object.keys(AppName).map(item => (
                    <option
                      selected={
                        ((editDevice && editDevice.appName) || AppName.ZOOP) ===
                        AppName[item]
                      }
                      key={item}
                      value={AppName[item]}
                    >
                      {AppName[item]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          )}

          <div>
            <label>{editPlaceHolder}</label>
            <InputGroup
              value={editDevice ? getCurrentEditValue(editDevice) : ''}
              onChange={onTypeName}
              className={`bp3-large `} //${'bp3-intent-danger'}
              placeholder={editPlaceHolder}
            />
          </div>
          <div>
            {careProviderNameErrorMessage && infoChanged && (
              <Flex
                style={{
                  fontStyle: 'italic',
                  marginTop: '8px',
                }}
              >
                {careProviderNameErrorMessage}
              </Flex>
            )}
          </div>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button
            disabled={
              !editDevice ||
              !editDevice.deviceName ||
              editDevice.deviceName.length === 0 ||
              !infoChanged
            }
            onClick={onSaveEditDevice}
            className="bp3-button bp3-large bp3-fill"
            intent={Intent.PRIMARY}
          >
            Save
          </Button>
        </div>
      </Dialog>
    );
  };

  if (props.loading) {
    return <div className="loader" />;
  }

  return (
    <Flex auto column {...props}>
      <Flex className="table-container">
        <Flex className="table-view">
          <HTMLTable>
            <thead className="header-title">
              <tr>
                <th>Care provider admin devices</th>
                <th>Device Name</th>
                <th>Care provider name</th>
                <th>App name</th>
                <th>Created date</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {devices.map((device: Device, index: number) => {
                return (
                  <tr key={index}>
                    <td className="name-column">{device.id}</td>
                    <td>{device.deviceName}</td>
                    <td>{device.careProviderName}</td>
                    <td>
                      <Tag
                        intent={
                          device.appName === AppName.ZOOP
                            ? 'success'
                            : 'warning'
                        }
                        key={index}
                      >
                        {device.appName}
                      </Tag>
                    </td>
                    <td>
                      {new Date(device.createTime).toLocaleDateString('vi')}
                    </td>
                    <td className="action-column">
                      <ButtonGroup>
                        <Button
                          onClick={() =>
                            onEditDevice(
                              device,
                              EditDeviceMode.EDIT_DEVICE_NAME
                            )
                          }
                        >
                          Change device name
                        </Button>
                        <Divider />
                        <Button
                          onClick={() =>
                            onEditDevice(
                              device,
                              EditDeviceMode.EDIT_CARE_PROVIDER_INFO
                            )
                          }
                        >
                          Change care provider info
                        </Button>
                        {device.appName !== AppName.ZOOP && [
                          <Divider key="divider" />,
                          <Button
                            key="btnEditSms"
                            onClick={() => setOpenUpdateSmsApi(device)}
                          >
                            Setting SMS
                          </Button>,
                        ]}
                      </ButtonGroup>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </HTMLTable>
        </Flex>
      </Flex>
      {renderEditCareProvider()}
      <StyledModelEditSms
        isOpen={isOpenSms}
        initValue={editDevice}
        onCancel={() => setOpenSms(false)}
        onChangeSuccess={device => onChangeSmsConfigSuccess(device)}
      />
      <Toaster ref={toasterRef} />
    </Flex>
  );
}

export { AdminDeviceListView };
