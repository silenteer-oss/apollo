import { Button, Classes, Toaster, InputGroup } from '@blueprintjs/core';
import { Flex, Box } from '@design-system/core/components';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import { unknownError } from '@sysadmin/common/notification/notification-config';
import React, { memo, useEffect, useRef, useState } from 'react';
import deviceService, { Device } from '../admin-device.service';

export interface AdminTrustedDeviceProps {
  className?: string;
  addDevice?: (device: Device[]) => void;
  validateCareProviderName?: (deviceName: string) => string;
}

let timeout: any;

function AdminTrustedDevice(
  props: AdminTrustedDeviceProps
): React.FunctionComponentElement<AdminTrustedDeviceProps> {
  const [loading, setLoading] = useState(false);
  const [yourCode, setYourCode] = useState(deviceService.getPhraseCode());
  const [errorMessage, setErrorMessage] = useState('');
  const [careProviderName, setCareProviderName] = useState('');
  const [
    careProviderNameErrorMessage,
    setCareProviderNameErrorMessage,
  ] = useState('');
  const [step, setStep] = useState(1);

  const toasterRef = useRef<Toaster>(null);
  const codeInputRef = useRef<HTMLInputElement>(null);

  /////////////////////////////////////////////////////////////////////////////////////

  useEffect(() => {
    if (loading) {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        setLoading(false);
        deviceService.closeWS();
        toasterRef.current.show({
          message: 'Something happened while pairing. Please try again',
          intent: 'danger',
          icon: 'error',
        });
      }, WEB_SOCKET_PAIRING_TIMEOUT);
    }

    return () => clearTimeout(timeout);
  }, [loading]);

  /////////////////////////////////////////////////////////////////////////////////////

  const addDeviceCallback = (data: null | Device[], error: Error) => {
    if (error) {
      toasterRef.current.show(unknownError);
      deviceService.closeWS();
      setLoading(false);
      return;
    }
    props.addDevice(data);
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onAddDevice = () => {
    if (!codeInputRef.current.value.trim()) {
      setErrorMessage('Code input is required');
      return;
    }

    setLoading(true);
    return deviceService.addDevice(
      codeInputRef.current.value + yourCode,
      careProviderName,
      addDeviceCallback
    );
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onRenewCode = () => {
    setYourCode(deviceService.getPhraseCode());
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onAdminCodeChange = () => {
    if (errorMessage) {
      setErrorMessage('');
    }
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onNextStep = () => {
    setStep(2);
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onBackStep = () => {
    setStep(1);
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onCancel = () => {
    setLoading(false);
    clearTimeout(timeout);
    deviceService.closeWS();
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const onChangeCareProviderName = e => {
    const { validateCareProviderName } = props;

    const error =
      validateCareProviderName && validateCareProviderName(e.target.value);

    setCareProviderNameErrorMessage(error);
    setCareProviderName(e.target.value);
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const renderCareProviderName = () => {
    return (
      <>
        <div className={Classes.DIALOG_BODY}>
          <Flex column>
            <Box pb="0.25rem">{`Care provider's name:`}</Box>
            <InputGroup
              autoFocus
              required
              large
              value={careProviderName}
              className={`bp3-large ${
                !careProviderName ||
                  careProviderName.length === 0 ||
                  !!careProviderNameErrorMessage
                  ? 'bp3-intent-danger'
                  : ''
                }`}
              placeholder="The care provider's name"
              onChange={onChangeCareProviderName}
            />
          </Flex>
          {careProviderNameErrorMessage && (
            <Flex className="error-message bp3-intent-danger">
              {careProviderNameErrorMessage}
            </Flex>
          )}
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button
            intent="primary"
            onClick={onNextStep}
            disabled={
              !careProviderName ||
              careProviderName.length === 0 ||
              !!careProviderNameErrorMessage
            }
            className="bp3-button bp3-large bp3-fill"
          >
            <Flex align="center">Next</Flex>
          </Button>
        </div>
      </>
    );
  };

  /////////////////////////////////////////////////////////////////////////////////////

  const renderInputCode = () => {
    return (
      <>
        <div className={Classes.DIALOG_BODY}>
          <Flex align="center" column>
            <label>{`Enter Care Provider's pairing code to enroll their admin device`}</label>
            <input
              autoFocus
              ref={codeInputRef}
              disabled={loading}
              className={`bp3-input bp3-large cy-code-input ${
                errorMessage ? 'bp3-intent-danger' : ''
                }`}
              placeholder="___ ___ ___ ___ ___ ___"
              onChange={onAdminCodeChange}
            />
            <Flex mt="0.25rem" className="error">
              {errorMessage}
            </Flex>
          </Flex>
          <Flex pt="1rem" column align="center">
            <p>
              The code below is yours, you have to send it to Care Provider
              admin device to begin pairing process.
            </p>
            <Flex
              justify="center"
              align="center"
              className="ls-pairing-code cy-code"
              column
            >
              <label>{yourCode}</label>
              <span>
                {`Have any issue with pairing code? `}
                {loading ? (
                  <a onClick={onCancel}>Cancel</a>
                ) : (
                    <a onClick={onRenewCode}>Renew</a>
                  )}
              </span>
            </Flex>
          </Flex>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button
              intent="none"
              onClick={onBackStep}
              className="bp3-button bp3-large bp3-fill"
            >
              <Flex align="center">Back</Flex>
            </Button>

            <Button
              intent="primary"
              onClick={onAddDevice}
              disabled={loading}
              className="bp3-button bp3-large bp3-fill cy-trust-btn"
            >
              <Flex align="center">{loading ? 'Enrolling ...' : 'Enroll'}</Flex>
            </Button>
          </div>
        </div>
      </>
    );
  };

  /////////////////////////////////////////////////////////////////////////////////////

  return (
    <Flex auto justify="center" mt={2} {...props} column>
      {step === 1 && renderCareProviderName()}
      {step === 2 && renderInputCode()}
      <Toaster ref={toasterRef} />
    </Flex>
  );
}

export default memo(AdminTrustedDevice);
