import { AdminDeviceListView } from './admin-device-list.view';
import styled from 'styled-components';
import { scaleSpace } from '@design-system/core/styles';

const AdminDeviceListViewStyled = styled(AdminDeviceListView)`
  ${() => {
    const headerHeight = scaleSpace(20);
    return `
      & {
        height: 100%;
        width: 100%;

        .header-title {
          background-color: #ebf1f5;
        }
        .table-container {
          flex: 1;
          padding: 0 2px;

          .table-view {
            width: 100%;
            max-height: calc(100vh - ${headerHeight}px);
            overflow: auto;

            > table {
              width: inherit;
            }

            th {
              box-shadow: none !important;
            }

            td {
              vertical-align: middle;
              box-shadow: none !important;
              border-bottom: 1px solid #ebf1f5;
            }

            .name-column {
              width: 400px;
            }

            .action-column {
              padding: 1px;
            }
          }
        }
      }
    `;
  }}
`;

export default AdminDeviceListViewStyled;
