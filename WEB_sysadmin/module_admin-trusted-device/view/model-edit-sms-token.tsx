import React from 'react';
import { InputGroup, Dialog, Classes, Button, Intent } from '@blueprintjs/core';
import { isEmpty } from '@design-system/infrastructure/utils';
import { Device } from '../admin-device.service';
import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';

export interface IProps {
  isOpen: boolean;
  onChangeSuccess: Function;
  initValue: Device;
  onCancel: Function;
}

export interface IState {
  apiKey: string;
  appUid: string;
}

class OriginalEditSmsTokenView extends React.Component<IProps, IState> {
  state = {
    apiKey: '',
    appUid: '',
  };

  onUpdateSmsToken = () => {
    const { onChangeSuccess, initValue } = this.props;
    const { apiKey, appUid } = this.state;
    SysadminApi.updateSmsApi(initValue.careProviderId, initValue.id, {
      apiKey,
      appUid,
    }).then(({ data }) => onChangeSuccess(data));
  };

  componentWillReceiveProps(nextProps) {
    const { initValue } = nextProps;
    if (initValue) {
      this.setState({
        apiKey: initValue.apiKey || '',
        appUid: initValue.appUid || '',
      });
    }
  }

  render() {
    const { isOpen } = this.props;
    const { apiKey, appUid } = this.state;
    return (
      <Dialog
        onClose={() => this.props.onCancel()}
        title="Edit Care Provider SMS Token"
        isOpen={isOpen}
      >
        <div className={Classes.DIALOG_BODY}>
          <label>App UID</label>
          <InputGroup
            value={appUid ? appUid : ''}
            onChange={e => this.setState({ appUid: e.target.value })}
            className={`bp3-large `} //${'bp3-intent-danger'}
            placeholder={'Input App UID'}
          />

          <label>Api key</label>
          <InputGroup
            value={apiKey ? apiKey : ''}
            onChange={e => this.setState({ apiKey: e.target.value })}
            className={`bp3-large `} //${'bp3-intent-danger'}
            placeholder={'API key'}
          />
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button
            disabled={isEmpty(apiKey) || isEmpty(appUid)}
            onClick={this.onUpdateSmsToken}
            className="bp3-button bp3-large bp3-fill"
            intent={Intent.PRIMARY}
          >
            Save
          </Button>
        </div>
      </Dialog>
    );
  }
}

export default OriginalEditSmsTokenView;
