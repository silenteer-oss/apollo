import AdminTrustedDevice from './admin-trusted-device.view';
import styled from 'styled-components';

export default styled(AdminTrustedDevice)`
  input {
    font-size: 2rem !important;
    text-align: center;
    &::placeholder {
        text-align: center;
        font-size: 1rem !important;
      }
  }
  .ls-pairing-code {
    label {
      font-size: 2rem !important;
      margin-left: 34px;
      width: 115px;
      margin-top: 8px;
    }
    span {
      margin-top: 1rem;
    }

    div > button {
      margin-top: 4px;
    }
  }

  .error-message {
    font-style: italic;
    margin-top: 8px;
  }

  .bp3-dialog-footer button {
    margin-top: 1rem;
  }
`;
