import { Button, Dialog, Toaster } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import React, { useEffect, useState } from 'react';
import deviceService from './admin-device.service';
import DeviceList from './view/admin-device-list.styled';
import AddTrustedDevice from './view/admin-trusted-device.styled';
import { Device } from './admin-device.service';
import { useRef } from 'react';

function AppView() {
  const [open, setOpenState] = useState(false);
  const [devices, setDevices] = useState([] as Device[]);
  const toasterRef = useRef<Toaster>(null);
  const [loading, setLoading] = useState(true);

  const onCloseDialog = () => setOpenState(false);
  const onOpenDialog = () => setOpenState(true);
  const onAddDevice = (devices: Device[]) => {
    setDevices(devices);
    setOpenState(false);
  };

  useEffect(() => {
    deviceService.getAllDevices().then(({ data }) => {
      setDevices(data);
      setLoading(false);
    });
  }, []);

  const updateDevice = (device: Device) => {
    const foundIndex = devices.findIndex(item => item.id === device.id);
    if (foundIndex !== -1) {
      const updatedDevices = [...devices];
      updatedDevices[foundIndex] = {
        ...updatedDevices[foundIndex],
        ...device,
      };
      setDevices(updatedDevices);
    }
  };

  const validateCareProviderName = (name: string) => {
    if (
      devices.map((device: Device) => device.deviceName).indexOf(name) !== -1
    ) {
      return 'Device name already existed. Please choose another name.';
    }

    if (/[`~,.<>;':"/[\]|{}()=_+-]/.test(name)) {
      return 'Device name should not contain special character';
    }

    return undefined;
  };

  return (
    <Flex auto column>
      <Flex justify="flex-end">
        <Button
          className="bp3-button cy-add-device-btn"
          intent="primary"
          text="Add care provider's admin device"
          onClick={onOpenDialog}
        />
      </Flex>
      <Flex auto pt="0.6rem">
        <DeviceList
          devices={devices}
          loading={loading}
          updateDevice={updateDevice}
          validateCareProviderName={validateCareProviderName}
        />
      </Flex>
      <Dialog
        isOpen={open}
        onClose={onCloseDialog}
        title="Add Care Provider's Admin device"
        canOutsideClickClose={false}
      >
        <AddTrustedDevice
          addDevice={onAddDevice}
          validateCareProviderName={validateCareProviderName}
        />
      </Dialog>

      <Toaster ref={toasterRef} />
    </Flex>
  );
}

export { AppView };
