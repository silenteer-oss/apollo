import {
  ADMIN_TRUSTED_DEVICE_MODULE_ID,
  ADMIN_TRUSTED_DEVICE_VIEW_ID,
} from './module-config';
import { AppView } from './app';

export default {
  [ADMIN_TRUSTED_DEVICE_MODULE_ID]: {
    [ADMIN_TRUSTED_DEVICE_VIEW_ID]: AppView,
  },
};
