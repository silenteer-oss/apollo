import React from 'react';
import { Flex } from '@design-system/core/components';

interface HeaderProps {
  className?: string;
}

function HeaderView(
  props: HeaderProps
): React.FunctionComponentElement<HeaderProps> {
  const { className } = props;

  return (
    <nav className={`bp3-navbar bp3-dark ${className}`}>
      <Flex auto justify="space-between">
        <div className="bp3-navbar-group">
          <div className="bp3-navbar-heading">Silentium.io</div>
        </div>
        {/* <div className="bp3-navbar-group"> */}
        {/* <button className="bp3-button bp3-minimal bp3-icon-diagram-tree">
            Organizations
          </button>
          <button className="bp3-button bp3-minimal bp3-icon-mobile-phone">
            Devices
          </button> */}
        {/* <span className="bp3-navbar-divider" />
          <button className="bp3-button bp3-minimal bp3-icon-user" />
          <button className="bp3-button bp3-minimal bp3-icon-cog" /> */}
        {/* </div> */}
      </Flex>
    </nav>
  );
}

export { HeaderView };
