import {
  lazyComponent,
  ErrorBoundary,
  EnsureSupportedBrowser,
} from '@design-system/core/components';
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {
  Redirect,
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import HomePage from './pages/home/home-page.styled';
import { Dialog, Toaster } from '@blueprintjs/core';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { setupBrowser } from '@infrastructure/browser';
import { ThemeProvider } from '@sysadmin/theme';
import { theme, getAllViewTheme, getAllComponentTheme } from '@sysadmin/theme';
// import '@sysadmin/assets/styles/index.scss';
import ApplicationService from './services/ApplicationService';

const LazyLoginPage = lazyComponent(() => import('./pages/auth/login.styled'));

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

const init = async () => {
  const data = await ApplicationService.init();
  const loggedIn = Object.keys(data.user).length !== 0;
  console.log(data)
  const apiErrorRedirect = {
    401: '/login',
    403: '/login',
  };

  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <GlobalStyleContextProvider
        styles={
          {
            // main: mixGlobalStyle(theme),
            // blueprint: mixCustomBlueprintStyle(theme),
          }
        }
      >
        <ErrorBoundary
          user={data.user}
          sentry={data.configs.sentry}
          Dialog={Dialog}
          Toaster={Toaster}
          apiErrorRedirect={apiErrorRedirect}
        >
          <EnsureSupportedBrowser
            features={['indexDB', 'storageManager']}
            unsupportedBrowserMessages={{
              title: 'Unsupported browser',
              content:
                'In order to use full features from application, you should use latest version of Chrome or Firefox',
            }}
            deniedPermissionMessages={{
              title: 'Denied permission',
              content:
                'Try to reload browser and accept permission from popup.',
            }}
            onRequestPermission={setupBrowser}
          >
            {
              loggedIn && <Router>
                <Switch>
                  <Route path="/app" component={HomePage} />
                  <Route path="/login" component={LazyLoginPage} />
                  <Redirect exact path="/" to="/app" />
                </Switch>
              </Router>
            }
            {
              !loggedIn && <LazyLoginPage />
            }
          </EnsureSupportedBrowser>
        </ErrorBoundary>
      </GlobalStyleContextProvider>
    </ThemeProvider>,
    document.getElementById('root')
  );
};

init();
