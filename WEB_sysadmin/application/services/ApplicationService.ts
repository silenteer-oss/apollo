import { SysadminApi } from '@infrastructure/resource/SysadminAppResource';

function loadAppConfig() {
  return SysadminApi.getAllConfigs().then(result => result.data);
}

function loadLoggedInUserInfo() {
  return SysadminApi.getMyLoggedInInfo().then(result => result.data);
}

function init() {
  return Promise.all([loadAppConfig(), loadLoggedInUserInfo()])
    .then(([configs, loggedInUserInfo]) => ({
      configs,
      user: {
        id: loggedInUserInfo.email,
      },
    }))
    .catch(ex => {
      console.error(ex);
      return {
        configs: {} as any,
        user: {} as any,
      };
    });
}

export default {
  init,
};
