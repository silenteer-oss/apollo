import { loadMicroModuleView } from '@design-system/core/components';
import { AUTHENTICATION_VIEW_ID } from '@sysadmin/module_authentication/module-config';
import { AdminTrustedDeviceProps } from '@sysadmin/module_admin-trusted-device';
import { ADMIN_TRUSTED_DEVICE_VIEW_ID } from '@sysadmin/module_admin-trusted-device/module-config';
import { getApplicationConfig } from './application-config';

const {
  modules: { authentication, AdminTrustedDeviceModule },
} = getApplicationConfig();

export const MicroAuthenticationModule: any = loadMicroModuleView({
  url: authentication.publicAssetPath,
  module: 'authentication',
  view: authentication.views[AUTHENTICATION_VIEW_ID],
  app: 'WEB_sysadmin'
});

export const MicroAdminTrustedDeviceModule: any = loadMicroModuleView<
  AdminTrustedDeviceProps
>({
  url: AdminTrustedDeviceModule.publicAssetPath,
  module: 'admin-trusted-device',
  view: AdminTrustedDeviceModule.views[ADMIN_TRUSTED_DEVICE_VIEW_ID],
  app: 'WEB_sysadmin'
});
