import {
  IApplicationConfig,
  IModuleConfig,
} from '@design-system/infrastructure/configs/environment';
import {
  AUTHENTICATION_MODULE_ID,
  IModuleViews as IAuthenticationModuleViews,
  getModuleConfig as getAuthenticationModuleConfig,
} from '@sysadmin/module_authentication/module-config';
import {
  ADMIN_TRUSTED_DEVICE_MODULE_ID,
  IModuleViews as IAdminTrustedDeviceModuleViews,
  getModuleConfig as getAdminTrustedDeviceModuleConfig,
} from '@sysadmin/module_admin-trusted-device/module-config';

export interface IApplicationModules {
  [AUTHENTICATION_MODULE_ID]: IModuleConfig<IAuthenticationModuleViews>;
  [ADMIN_TRUSTED_DEVICE_MODULE_ID]: IModuleConfig<
    IAdminTrustedDeviceModuleViews
  >;
}

export function getApplicationConfig(): IApplicationConfig<
  IApplicationModules
> {
  return {
    id: 'SysadminApplication',
    name: 'System Admin Application',
    host: '0.0.0.0',
    port: 3000,
    publicAssetPath: `/`,
    modules: {
      authentication: getAuthenticationModuleConfig(),
      AdminTrustedDeviceModule: getAdminTrustedDeviceModuleConfig(),
    },
  };
}
