import React from 'react';
import { MicroAuthenticationModule } from '../../micro-modules';
import { Flex } from '@design-system/core/components';

function AuthenticationPage(props: { className?: string }) {
  const { className } = props;
  return (
    <Flex className={className} column>
      <MicroAuthenticationModule />
    </Flex>
  );
}

export { AuthenticationPage };
