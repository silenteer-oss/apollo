import { AuthenticationPage } from './login.page';
import styled from 'styled-components';

export default styled(AuthenticationPage)`
  .sl-login-box {
    width: 30rem;

    .bp3-button-text {
      text-align: center;
      line-height: 40px;
    }

    p {
      padding: 1rem;
      text-align: center;
    }
  }
`;
