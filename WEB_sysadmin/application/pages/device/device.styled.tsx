import { DeviceListPage } from './device.page';
import styled from 'styled-components';

const DeviceListStyled = styled(DeviceListPage)`
  position: relative;
  height: 100%;
  width: 100%;
`;
export default DeviceListStyled;
