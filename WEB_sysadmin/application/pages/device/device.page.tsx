import * as React from 'react';
import { MicroAdminTrustedDeviceModule } from '../../micro-modules';
import { Flex } from '@design-system/core/components';

function DeviceListPage(props: { className: string }) {
  const { className } = props;
  return (
    <Flex className={className} p="2rem" auto>
      <MicroAdminTrustedDeviceModule fallback={<div className="loader" />} />
    </Flex>
  );
}

export { DeviceListPage };
