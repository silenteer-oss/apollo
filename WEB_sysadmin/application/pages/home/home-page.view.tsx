import { Flex } from '@design-system/core/components';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { lazyComponent } from '@design-system/core/components';
import HeaderView from '../../containers/header.styled';

const DeviceList = lazyComponent(() => import('../device/device.styled'));

function HomePage(props: { className?: string }) {
  return (
    <Flex auto column className={props.className}>
      <HeaderView />
      <Switch>
        <Route path="/app/devices" component={DeviceList} />
        <Redirect exact path="/app" to="/app/devices" />
      </Switch>
    </Flex>
  );
}

export { HomePage };
