const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin");
const path = require('path')

function make(entryName, { varName = entryName, filename = `${entryName}.js` } = {}) {
  return {
    mode: 'production',
    entry: {
      [entryName]: `./${entryName}.js`,
    },
    output: {
      filename,
      path: path.resolve(__dirname, '../dist/web_modules'),
      library: varName,
      libraryTarget: "var"
    },
    devtool: 'none',
    plugins: [
      new EsmWebpackPlugin()
    ]
  }
}


module.exports = [
  make('axios'),
  make('color'),
  make('sentry', { filename: '@sentry/browser' }),
  make('tslib'),
  make('url'),
  make('libsodium-wrappers', { varName: 'libsodium' }),
  make('dexie')
]