// tslint:disable
/// <reference path="../custom.d.ts" />
/**
 * zoop-careprovider-app
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface PairedRequest
 */
export interface PairedRequest {
    /**
     * 
     * @type {string}
     * @memberof PairedRequest
     */
    patientId: string;
    /**
     * 
     * @type {string}
     * @memberof PairedRequest
     */
    phone: string;
    /**
     * 
     * @type {string}
     * @memberof PairedRequest
     */
    deviceId: string;
}


