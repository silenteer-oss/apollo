// tslint:disable
/// <reference path="../custom.d.ts" />
/**
 * zoop-careprovider-app
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import { Gender } from './gender';

/**
 * 
 * @export
 * @interface PatientProfileResponse
 */
export interface PatientProfileResponse {
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    id: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    fullName: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    nickName: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    title: string;
    /**
     * 
     * @type {number}
     * @memberof PatientProfileResponse
     */
    dob: number;
    /**
     * 
     * @type {Gender}
     * @memberof PatientProfileResponse
     */
    gender: Gender;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    phone: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    email: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    address: string;
    /**
     * 
     * @type {string}
     * @memberof PatientProfileResponse
     */
    conversationId: string;
}


