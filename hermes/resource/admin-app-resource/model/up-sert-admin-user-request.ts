// tslint:disable
/// <reference path="../custom.d.ts" />
/**
 * zoop-admin-app
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface UpSertAdminUserRequest
 */
export interface UpSertAdminUserRequest {
    /**
     * 
     * @type {string}
     * @memberof UpSertAdminUserRequest
     */
    username: string;
    /**
     * 
     * @type {string}
     * @memberof UpSertAdminUserRequest
     */
    password: string;
    /**
     * 
     * @type {string}
     * @memberof UpSertAdminUserRequest
     */
    seed: string;
}


