// tslint:disable
/// <reference path="../custom.d.ts" />
/**
 * zoop-appointment-app
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @enum {string}
 */
export enum AppointmentFields {
    Id = 'id',
    PatientId = 'patientId',
    Department = 'department',
    StartDateTime = 'startDateTime',
    HourAndMinuteSpecified = 'hourAndMinuteSpecified',
    EndDateTime = 'endDateTime',
    Recurrence = 'recurrence',
    RootAppointmentId = 'rootAppointmentId',
    ConfirmedAppointmentDates = 'confirmedAppointmentDates',
    Reason = 'reason',
    DoctorIds = 'doctorIds',
    NurseIds = 'nurseIds',
    ReminderValue = 'reminderValue',
    ReminderUnit = 'reminderUnit',
    Note = 'note',
    InternalNote = 'internalNote',
    CancelReason = 'cancelReason',
    EditedFields = 'editedFields'
}



