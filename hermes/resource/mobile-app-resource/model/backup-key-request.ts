// tslint:disable
/// <reference path="../custom.d.ts" />
/**
 * zoop-mobile-app
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface BackupKeyRequest
 */
export interface BackupKeyRequest {
    /**
     * 
     * @type {string}
     * @memberof BackupKeyRequest
     */
    privateKey: string;
    /**
     * 
     * @type {string}
     * @memberof BackupKeyRequest
     */
    publicKey: string;
    /**
     * 
     * @type {string}
     * @memberof BackupKeyRequest
     */
    chatSession: string;
    /**
     * 
     * @type {string}
     * @memberof BackupKeyRequest
     */
    careProviderId: string;
    /**
     * 
     * @type {string}
     * @memberof BackupKeyRequest
     */
    conversationId: string;
}


