export interface ISignKeyPairString {
  pubKey: string;
  privKey: string;
}

export interface ISealKeyPairString {
  pubKey: string;
  privKey: string;
}

import * as Crypto from './crypto';
import { convertBytesToBinaryString, convertBinaryStringToBytes } from '../signal/byte-util'

export async function sign(
  message: string,
  privateKey: string
): Promise<string> {
  const result = await Crypto.sign(
    convertBinaryStringToBytes(message),
    convertBinaryStringToBytes(privateKey)
  )
  
  return convertBytesToBinaryString(result);
}

export async function verify(
  message: string,
  signature: string,
  publicKey: string
): Promise<void> {
  await Crypto.verify(
    convertBinaryStringToBytes(message),
    convertBinaryStringToBytes(signature),
    convertBinaryStringToBytes(publicKey)
  );
}

export async function encrypt(
  iv: string,
  key: string,
  plaintext: string
): Promise<string> {
  const result = await Crypto.encrypt(
    convertBinaryStringToBytes(iv),
    convertBinaryStringToBytes(key),
    convertBinaryStringToBytes(plaintext)
  )
  
  return convertBytesToBinaryString(result);
}

export async function decrypt(
  iv: string,
  key: string,
  ciphertext: string
): Promise<string> {
  const result = await Crypto.decrypt(
    convertBinaryStringToBytes(iv),
    convertBinaryStringToBytes(key),
    convertBinaryStringToBytes(ciphertext)
  )
  
  return convertBytesToBinaryString(result);
}

export async function getRandomBytes(size: number): Promise<string> {
  const result = await Crypto.getRandomBytes(size);
  return convertBytesToBinaryString(result);
}

export async function hash(
  key: string,
  data: string,
  length: number
): Promise<string> {
  const result = await Crypto.hash(
    convertBinaryStringToBytes(key),
    convertBinaryStringToBytes(data),
    length
  )
  
  return convertBytesToBinaryString(result);
}

export async function generateSignKeyPair(): Promise<ISignKeyPairString> {
  const result = await Crypto.generateSignKeyPair();

  return {
    pubKey: convertBytesToBinaryString(result.pubKey),
    privKey: convertBytesToBinaryString(result.privKey)
  }
}

export async function generateSealKeyPair(): Promise<ISealKeyPairString> {
  const result = await Crypto.generateSealKeyPair();

  return {
    pubKey: convertBytesToBinaryString(result.pubKey),
    privKey: convertBytesToBinaryString(result.privKey)
  }
}

export async function seal(
  plaintext: string,
  recipientPubKey: string
): Promise<string> {
  const result = await Crypto.seal(
    convertBinaryStringToBytes(plaintext),
    convertBinaryStringToBytes(recipientPubKey)
  )
  
  return convertBytesToBinaryString(result);
}

export async function unseal(
  ciphertext: string,
  recipientSealKeyPair: ISealKeyPairString
): Promise<string> {
  const result = await Crypto.unseal(
    convertBinaryStringToBytes(ciphertext),
    {
      pubKey: convertBinaryStringToBytes(recipientSealKeyPair.pubKey),
      privKey: convertBinaryStringToBytes(recipientSealKeyPair.privKey)
    }
  )
  
  return convertBytesToBinaryString(result);
}

export async function hashPassword(
  plainPassword: string,
  salt: string
): Promise<string> {
  const result = await Crypto.hashPassword(
    convertBinaryStringToBytes(plainPassword),
    convertBinaryStringToBytes(salt)
  );

  return convertBytesToBinaryString(result);
}
