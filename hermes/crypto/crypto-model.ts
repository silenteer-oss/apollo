export interface ISignKeyPair {
  pubKey: Uint8Array;
  privKey: Uint8Array;
}

export interface ISealKeyPair {
  pubKey: Uint8Array;
  privKey: Uint8Array;
}
  
export interface ICryptoEngine {
  sign(
    message: Uint8Array,
    privateKey: Uint8Array
  ): Promise<Uint8Array>;

  verify(
    message: Uint8Array,
    signature: Uint8Array,
    publicKey: Uint8Array
  ): Promise<void>;

  encrypt(
    iv: Uint8Array,
    key: Uint8Array,
    plaintext: Uint8Array
  ): Promise<Uint8Array>;

  decrypt(
    iv: Uint8Array,
    key: Uint8Array,
    ciphertext: Uint8Array
  ): Promise<Uint8Array>;

  getRandomBytes(size: number): Promise<Uint8Array>;

  hash(
    outputSize: number,
    key: Uint8Array,
    data: Uint8Array
  ): Promise<Uint8Array>;

  generateSignKeyPair(): Promise<ISignKeyPair>;

  generateSealKeyPair(): Promise<ISealKeyPair>;

  seal(
    plaintext: Uint8Array,
    recipientPubKey: Uint8Array
  ): Promise<Uint8Array>;

  unseal(
    ciphertext: Uint8Array,
    recipientSealKeyPair: ISealKeyPair
  ): Promise<Uint8Array>;

  hashPassword(
    keyLength: number,
    plainPassword: Uint8Array,
    salt: Uint8Array,
    opsLimit: number,
    memLimit: number,
    algorithm: number
  ): Promise<Uint8Array>;
}