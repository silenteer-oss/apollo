export function convertBytesToBinaryString(
  bytes: Uint8Array
): string {
  return String.fromCharCode.apply(null, new Uint8Array(bytes));

  // const buffer = bytes.buffer.slice(bytes.byteOffset, bytes.byteLength + bytes.byteOffset); 
  // return String.fromCharCode.apply(null, new Uint16Array(buffer));
}

export function convertBinaryStringToBytes(binaryString: string): Uint8Array {
  const bytes = new Uint8Array(binaryString.length);
  for (let i = 0, strLen = binaryString.length; i < strLen; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }
  return bytes;

  // const buffer = new ArrayBuffer(binaryString.length * 2); // 2 bytes for each char
  // const bufferView = new Uint16Array(buffer);
  // for (let i = 0, strLen = binaryString.length; i < strLen; i++) {
  //   bufferView[i] = binaryString.charCodeAt(i);
  // }
  // return new Uint8Array(buffer);
}