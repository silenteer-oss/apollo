import { ICryptoEngine, ISignKeyPair, ISealKeyPair } from './crypto-model';

let _cryptoEngine: ICryptoEngine;

export function injectEngine(engine: ICryptoEngine) {
  _cryptoEngine = engine;
}

export async function sign(
  message: Uint8Array,
  privateKey: Uint8Array
): Promise<Uint8Array> {
  return _cryptoEngine.sign(message, privateKey);
}

export async function verify(
  message: Uint8Array,
  signature: Uint8Array,
  publicKey: Uint8Array
): Promise<void> {
  _cryptoEngine.verify(message, signature, publicKey);
}

export async function encrypt(
  iv: Uint8Array,
  key: Uint8Array,
  plaintext: Uint8Array
): Promise<Uint8Array> {
  return _cryptoEngine.encrypt(iv, key, plaintext);
}

export async function decrypt(
  iv: Uint8Array,
  key: Uint8Array,
  ciphertext: Uint8Array
): Promise<Uint8Array> {
  return _cryptoEngine.decrypt(iv, key, ciphertext);
}

export async function getRandomBytes(size: number): Promise<Uint8Array> {
  return _cryptoEngine.getRandomBytes(size);
}

export async function hash(
  key: Uint8Array,
  data: Uint8Array,
  length: number = 32
): Promise<Uint8Array> {
  return _cryptoEngine.hash(length, key, data);
}

export async function generateSignKeyPair(): Promise<ISignKeyPair> {
  return _cryptoEngine.generateSignKeyPair();
}

export async function generateSealKeyPair(): Promise<ISealKeyPair> {
  return _cryptoEngine.generateSealKeyPair();
}

export async function seal(
  plaintext: Uint8Array,
  recipientPubKey: Uint8Array
): Promise<Uint8Array> {
  return _cryptoEngine.seal(plaintext, recipientPubKey);
}

export async function unseal(
  ciphertext: Uint8Array,
  recipientSealKeyPair: ISealKeyPair
): Promise<Uint8Array> {
  return _cryptoEngine.unseal(ciphertext, recipientSealKeyPair);
}

export async function hashPassword(
  plainPassword: Uint8Array,
  salt: Uint8Array
): Promise<Uint8Array> {
  // sodium.crypto_pwhash_OPSLIMIT_MODERATE = 3
  // sodium.crypto_pwhash_MEMLIMIT_MODERATE = 268435456
  // sodium.crypto_pwhash_ALG_ARGON2ID13 = 2
  return _cryptoEngine.hashPassword(32, plainPassword, salt, 3, 268435456, 2);
}
