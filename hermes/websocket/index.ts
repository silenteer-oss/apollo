const MAX_RETRY_TIMES = 3;
const RETRY_INTERVAL = 30 * 1000; // 30 seconds

const HEART_BEAT_INTERVAL = 30 * 1000; // 30 seconds
const HEART_BEAT_WAIT_TIME = 5 * 1000; // 5 seconds

export const WEB_SOCKET_PAIRING_TIMEOUT = 60000; // 1 minutes

export interface MicronautsWebSocketClientOptions {
  autoPing: boolean;
  autoReconnect: boolean;
}

export class MicronautsWebSocketClient {
  _url: string;
  _options: MicronautsWebSocketClientOptions;
  _webSocket: WebSocket;
  _webSocketReconnecting = false;

  _retriedTimes = 0;
  _pingInterval;
  _reconnectTimeout;

  _heartBeatTimeout;
  _forceQuit = false;

  onopen = (ev: Event) => { }
  onmessage = (ev: MessageEvent) => { }
  onclose = (ev: CloseEvent) => { }
  onerror = (ev: Event) => { }
  onreconnected = (ev: Event) => { }
  
  constructor(url: string, options?: MicronautsWebSocketClientOptions) {
    this._url = url;
    this._options = options || { autoPing: false, autoReconnect: false };
    this._open();
  }

  getReadyState = () => this._webSocket ? this._webSocket.readyState : undefined;
  
  _open() {
    this._webSocket = new WebSocket(this._url);

    this._webSocket.onopen = (ev: Event) => {
      this._retriedTimes = 0;

      if (this._webSocketReconnecting) {
        this._webSocketReconnecting = false;

        console.log(`[WebSocket] Reconnected ${this._url}`);
        this.onreconnected(ev);
      } else {
        console.log(`[WebSocket] Opened ${this._url}`);
        this.onopen(ev);
      }
    }

    this._webSocket.onmessage = (ev: MessageEvent) => {
      try {
        const data = JSON.parse(ev.data);
        if (data.topic === 'PONG') {
            this._heartBeatTimeout && clearTimeout(this._heartBeatTimeout);
            this._heartBeatTimeout = undefined;
            return;
        }
      } catch (e) {
        // Skip
      }

      console.log("[WebSocket] Message " + ev.data);
      this.onmessage(ev);
    }

    this._webSocket.onclose = (ev: CloseEvent) => {
      this._cleanUp();
      switch (ev.code) {
        case 1000:	// Normal
        case 401:   // Unauthorized
          console.log(`[WebSocket] Closed ${this._url}`, ev);
          this.onclose(ev);
          break;
        default:	// Abnormal
          if (!this._options.autoReconnect || this._forceQuit) {
            console.log(`[WebSocket] Closed ${this._url}. Reconnect option is false`, ev);
            this.onclose(ev);
            break;
          }
      
          if (++this._retriedTimes > MAX_RETRY_TIMES) {
            console.log(`[WebSocket] Closed ${this._url}. Cannot reconnect after ${MAX_RETRY_TIMES} retries`, ev);
            this.onclose(ev);
            break;
          }
      
          console.log(`[WebSocket] Will try to reconnect to ${this._url} after ${RETRY_INTERVAL / 1000} seconds. Attempts ${this._retriedTimes} / ${MAX_RETRY_TIMES}`, ev);
          this._reconnectTimeout = setTimeout(() => {
            console.log(`[WebSocket] Trying to reconnect to ${this._url}`);
            this._webSocketReconnecting = true;
            this._open();
          }, RETRY_INTERVAL);
          break;
      }
    }

    this._webSocket.onerror = (ev: Event) => {
      console.log(`[WebSocket] Error`, ev);
      this.onerror(ev);
    };

    if (this._options.autoPing) {
      this._pingInterval = setInterval(() => {
        this.send(JSON.stringify({topic: 'PING'}));

        this._heartBeatTimeout = setTimeout(() => {
          console.log(`[Web Socket] Do not receive pong in ${HEART_BEAT_WAIT_TIME / 1000} seconds after ping`);
          this._webSocket.close();
        }, HEART_BEAT_WAIT_TIME)
      }, HEART_BEAT_INTERVAL);
    }
  }

  send(data: string | ArrayBufferLike | Blob | ArrayBufferView) {
    try {
      this._webSocket.send(data);
    } catch (e) {
      this.onerror(e);
    }
  }

  close() {
    this._forceQuit = true;
    this._webSocket.close();
  }

  _cleanUp() {
    this._webSocket.onopen = undefined;
    this._webSocket.onmessage = undefined;
    this._webSocket.onerror = undefined;
    this._webSocket.onclose = undefined;

    this._pingInterval && clearInterval(this._pingInterval);
    this._pingInterval = undefined;

    this._heartBeatTimeout && clearTimeout(this._heartBeatTimeout);
    this._heartBeatTimeout = undefined;

    this._reconnectTimeout && clearTimeout(this._reconnectTimeout);
    this._reconnectTimeout = undefined;
  }
}

export const createCommonWs = (hostUrl: string) => new MicronautsWebSocketClient(
  `${hostUrl}/ws/common`,
  {autoPing: true, autoReconnect: true}
);

export const createPairingWs = (hostUrl: string, pairingAddress: string) => new MicronautsWebSocketClient(
  `${hostUrl}/ws/device/${pairingAddress}`,
  {autoPing: false, autoReconnect: false}
);