import { hash } from '../crypto/crypto';
import { ISignalMessageKey, create } from './signal-message-key';
import { textsecure } from './signal-proto/sender-key-proto';

const _MESSAGE_KEY_SEED = new Uint8Array(1);
_MESSAGE_KEY_SEED[0] = 0x01;

const _CHAIN_KEY_SEED = new Uint8Array(1);
_CHAIN_KEY_SEED[0] = 0x02;

export class SignalChain {
  get iteration(): number {
    return this._iteration;
  }

  get key(): Uint8Array {
    return this._key;
  }

  constructor(private _iteration: number, private _key: Uint8Array) {}

  async getMessageKey(): Promise<ISignalMessageKey> {
    const seed = await hash(this._key, _MESSAGE_KEY_SEED);
    return create(this._iteration, seed);
  }

  async getMessageKeyToStore(): Promise<
    textsecure.SenderKeyStateStructure.ISenderMessageKey
  > {
    const seed = await hash(this._key, _MESSAGE_KEY_SEED);
    return {
      iteration: this._iteration,
      seed,
    };
  }

  async getNext(): Promise<SignalChain> {
    const key = await hash(this._key, _CHAIN_KEY_SEED);
    return new SignalChain(this._iteration + 1, key);
  }
}
