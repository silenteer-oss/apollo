
/*eslint-disable*/
/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

// @ts-nocheck
// @ts-ignore
export const textsecure = $root.textsecure = (() => {

  /**
   * Namespace textsecure.
   * @exports textsecure
   * @namespace
   */
  const textsecure = {};

  textsecure.SenderKeyMessage = (function () {

    /**
     * Properties of a SenderKeyMessage.
     * @memberof textsecure
     * @interface ISenderKeyMessage
     * @property {number|null} [id] SenderKeyMessage id
     * @property {number|null} [iteration] SenderKeyMessage iteration
     * @property {Uint8Array|null} [ciphertext] SenderKeyMessage ciphertext
     */

    /**
     * Constructs a new SenderKeyMessage.
     * @memberof textsecure
     * @classdesc Represents a SenderKeyMessage.
     * @implements ISenderKeyMessage
     * @constructor
     * @param {textsecure.ISenderKeyMessage=} [properties] Properties to set
     */
    function SenderKeyMessage(properties) {
      if (properties)
        for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null)
            this[keys[i]] = properties[keys[i]];
    }

    /**
     * SenderKeyMessage id.
     * @member {number} id
     * @memberof textsecure.SenderKeyMessage
     * @instance
     */
    SenderKeyMessage.prototype.id = 0;

    /**
     * SenderKeyMessage iteration.
     * @member {number} iteration
     * @memberof textsecure.SenderKeyMessage
     * @instance
     */
    SenderKeyMessage.prototype.iteration = 0;

    /**
     * SenderKeyMessage ciphertext.
     * @member {Uint8Array} ciphertext
     * @memberof textsecure.SenderKeyMessage
     * @instance
     */
    SenderKeyMessage.prototype.ciphertext = $util.newBuffer([]);

    /**
     * Creates a new SenderKeyMessage instance using the specified properties.
     * @function create
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {textsecure.ISenderKeyMessage=} [properties] Properties to set
     * @returns {textsecure.SenderKeyMessage} SenderKeyMessage instance
     */
    SenderKeyMessage.create = function create(properties) {
      return new SenderKeyMessage(properties);
    };

    /**
     * Encodes the specified SenderKeyMessage message. Does not implicitly {@link textsecure.SenderKeyMessage.verify|verify} messages.
     * @function encode
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {textsecure.ISenderKeyMessage} message SenderKeyMessage message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyMessage.encode = function encode(message, writer) {
      if (!writer)
        writer = $Writer.create();
      if (message.id != null && message.hasOwnProperty("id"))
        writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.id);
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.iteration);
      if (message.ciphertext != null && message.hasOwnProperty("ciphertext"))
        writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.ciphertext);
      return writer;
    };

    /**
     * Encodes the specified SenderKeyMessage message, length delimited. Does not implicitly {@link textsecure.SenderKeyMessage.verify|verify} messages.
     * @function encodeDelimited
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {textsecure.ISenderKeyMessage} message SenderKeyMessage message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyMessage.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a SenderKeyMessage message from the specified reader or buffer.
     * @function decode
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {textsecure.SenderKeyMessage} SenderKeyMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyMessage.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader))
        reader = $Reader.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyMessage();
      while (reader.pos < end) {
        let tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.id = reader.uint32();
            break;
          case 2:
            message.iteration = reader.uint32();
            break;
          case 3:
            message.ciphertext = reader.bytes();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a SenderKeyMessage message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {textsecure.SenderKeyMessage} SenderKeyMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyMessage.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader))
        reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a SenderKeyMessage message.
     * @function verify
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    SenderKeyMessage.verify = function verify(message) {
      if (typeof message !== "object" || message === null)
        return "object expected";
      if (message.id != null && message.hasOwnProperty("id"))
        if (!$util.isInteger(message.id))
          return "id: integer expected";
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        if (!$util.isInteger(message.iteration))
          return "iteration: integer expected";
      if (message.ciphertext != null && message.hasOwnProperty("ciphertext"))
        if (!(message.ciphertext && typeof message.ciphertext.length === "number" || $util.isString(message.ciphertext)))
          return "ciphertext: buffer expected";
      return null;
    };

    /**
     * Creates a SenderKeyMessage message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {textsecure.SenderKeyMessage} SenderKeyMessage
     */
    SenderKeyMessage.fromObject = function fromObject(object) {
      if (object instanceof $root.textsecure.SenderKeyMessage)
        return object;
      let message = new $root.textsecure.SenderKeyMessage();
      if (object.id != null)
        message.id = object.id >>> 0;
      if (object.iteration != null)
        message.iteration = object.iteration >>> 0;
      if (object.ciphertext != null)
        if (typeof object.ciphertext === "string")
          $util.base64.decode(object.ciphertext, message.ciphertext = $util.newBuffer($util.base64.length(object.ciphertext)), 0);
        else if (object.ciphertext.length)
          message.ciphertext = object.ciphertext;
      return message;
    };

    /**
     * Creates a plain object from a SenderKeyMessage message. Also converts values to other types if specified.
     * @function toObject
     * @memberof textsecure.SenderKeyMessage
     * @static
     * @param {textsecure.SenderKeyMessage} message SenderKeyMessage
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    SenderKeyMessage.toObject = function toObject(message, options) {
      if (!options)
        options = {};
      let object = {};
      if (options.defaults) {
        object.id = 0;
        object.iteration = 0;
        if (options.bytes === String)
          object.ciphertext = "";
        else {
          object.ciphertext = [];
          if (options.bytes !== Array)
            object.ciphertext = $util.newBuffer(object.ciphertext);
        }
      }
      if (message.id != null && message.hasOwnProperty("id"))
        object.id = message.id;
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        object.iteration = message.iteration;
      if (message.ciphertext != null && message.hasOwnProperty("ciphertext"))
        object.ciphertext = options.bytes === String ? $util.base64.encode(message.ciphertext, 0, message.ciphertext.length) : options.bytes === Array ? Array.prototype.slice.call(message.ciphertext) : message.ciphertext;
      return object;
    };

    /**
     * Converts this SenderKeyMessage to JSON.
     * @function toJSON
     * @memberof textsecure.SenderKeyMessage
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    SenderKeyMessage.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SenderKeyMessage;
  })();

  textsecure.SenderKeyDistributionMessage = (function () {

    /**
     * Properties of a SenderKeyDistributionMessage.
     * @memberof textsecure
     * @interface ISenderKeyDistributionMessage
     * @property {number|null} [id] SenderKeyDistributionMessage id
     * @property {number|null} [iteration] SenderKeyDistributionMessage iteration
     * @property {Uint8Array|null} [chainKey] SenderKeyDistributionMessage chainKey
     * @property {Uint8Array|null} [signingKey] SenderKeyDistributionMessage signingKey
     */

    /**
     * Constructs a new SenderKeyDistributionMessage.
     * @memberof textsecure
     * @classdesc Represents a SenderKeyDistributionMessage.
     * @implements ISenderKeyDistributionMessage
     * @constructor
     * @param {textsecure.ISenderKeyDistributionMessage=} [properties] Properties to set
     */
    function SenderKeyDistributionMessage(properties) {
      if (properties)
        for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null)
            this[keys[i]] = properties[keys[i]];
    }

    /**
     * SenderKeyDistributionMessage id.
     * @member {number} id
     * @memberof textsecure.SenderKeyDistributionMessage
     * @instance
     */
    SenderKeyDistributionMessage.prototype.id = 0;

    /**
     * SenderKeyDistributionMessage iteration.
     * @member {number} iteration
     * @memberof textsecure.SenderKeyDistributionMessage
     * @instance
     */
    SenderKeyDistributionMessage.prototype.iteration = 0;

    /**
     * SenderKeyDistributionMessage chainKey.
     * @member {Uint8Array} chainKey
     * @memberof textsecure.SenderKeyDistributionMessage
     * @instance
     */
    SenderKeyDistributionMessage.prototype.chainKey = $util.newBuffer([]);

    /**
     * SenderKeyDistributionMessage signingKey.
     * @member {Uint8Array} signingKey
     * @memberof textsecure.SenderKeyDistributionMessage
     * @instance
     */
    SenderKeyDistributionMessage.prototype.signingKey = $util.newBuffer([]);

    /**
     * Creates a new SenderKeyDistributionMessage instance using the specified properties.
     * @function create
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {textsecure.ISenderKeyDistributionMessage=} [properties] Properties to set
     * @returns {textsecure.SenderKeyDistributionMessage} SenderKeyDistributionMessage instance
     */
    SenderKeyDistributionMessage.create = function create(properties) {
      return new SenderKeyDistributionMessage(properties);
    };

    /**
     * Encodes the specified SenderKeyDistributionMessage message. Does not implicitly {@link textsecure.SenderKeyDistributionMessage.verify|verify} messages.
     * @function encode
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {textsecure.ISenderKeyDistributionMessage} message SenderKeyDistributionMessage message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyDistributionMessage.encode = function encode(message, writer) {
      if (!writer)
        writer = $Writer.create();
      if (message.id != null && message.hasOwnProperty("id"))
        writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.id);
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.iteration);
      if (message.chainKey != null && message.hasOwnProperty("chainKey"))
        writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.chainKey);
      if (message.signingKey != null && message.hasOwnProperty("signingKey"))
        writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.signingKey);
      return writer;
    };

    /**
     * Encodes the specified SenderKeyDistributionMessage message, length delimited. Does not implicitly {@link textsecure.SenderKeyDistributionMessage.verify|verify} messages.
     * @function encodeDelimited
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {textsecure.ISenderKeyDistributionMessage} message SenderKeyDistributionMessage message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyDistributionMessage.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a SenderKeyDistributionMessage message from the specified reader or buffer.
     * @function decode
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {textsecure.SenderKeyDistributionMessage} SenderKeyDistributionMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyDistributionMessage.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader))
        reader = $Reader.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyDistributionMessage();
      while (reader.pos < end) {
        let tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.id = reader.uint32();
            break;
          case 2:
            message.iteration = reader.uint32();
            break;
          case 3:
            message.chainKey = reader.bytes();
            break;
          case 4:
            message.signingKey = reader.bytes();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a SenderKeyDistributionMessage message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {textsecure.SenderKeyDistributionMessage} SenderKeyDistributionMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyDistributionMessage.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader))
        reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a SenderKeyDistributionMessage message.
     * @function verify
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    SenderKeyDistributionMessage.verify = function verify(message) {
      if (typeof message !== "object" || message === null)
        return "object expected";
      if (message.id != null && message.hasOwnProperty("id"))
        if (!$util.isInteger(message.id))
          return "id: integer expected";
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        if (!$util.isInteger(message.iteration))
          return "iteration: integer expected";
      if (message.chainKey != null && message.hasOwnProperty("chainKey"))
        if (!(message.chainKey && typeof message.chainKey.length === "number" || $util.isString(message.chainKey)))
          return "chainKey: buffer expected";
      if (message.signingKey != null && message.hasOwnProperty("signingKey"))
        if (!(message.signingKey && typeof message.signingKey.length === "number" || $util.isString(message.signingKey)))
          return "signingKey: buffer expected";
      return null;
    };

    /**
     * Creates a SenderKeyDistributionMessage message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {textsecure.SenderKeyDistributionMessage} SenderKeyDistributionMessage
     */
    SenderKeyDistributionMessage.fromObject = function fromObject(object) {
      if (object instanceof $root.textsecure.SenderKeyDistributionMessage)
        return object;
      let message = new $root.textsecure.SenderKeyDistributionMessage();
      if (object.id != null)
        message.id = object.id >>> 0;
      if (object.iteration != null)
        message.iteration = object.iteration >>> 0;
      if (object.chainKey != null)
        if (typeof object.chainKey === "string")
          $util.base64.decode(object.chainKey, message.chainKey = $util.newBuffer($util.base64.length(object.chainKey)), 0);
        else if (object.chainKey.length)
          message.chainKey = object.chainKey;
      if (object.signingKey != null)
        if (typeof object.signingKey === "string")
          $util.base64.decode(object.signingKey, message.signingKey = $util.newBuffer($util.base64.length(object.signingKey)), 0);
        else if (object.signingKey.length)
          message.signingKey = object.signingKey;
      return message;
    };

    /**
     * Creates a plain object from a SenderKeyDistributionMessage message. Also converts values to other types if specified.
     * @function toObject
     * @memberof textsecure.SenderKeyDistributionMessage
     * @static
     * @param {textsecure.SenderKeyDistributionMessage} message SenderKeyDistributionMessage
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    SenderKeyDistributionMessage.toObject = function toObject(message, options) {
      if (!options)
        options = {};
      let object = {};
      if (options.defaults) {
        object.id = 0;
        object.iteration = 0;
        if (options.bytes === String)
          object.chainKey = "";
        else {
          object.chainKey = [];
          if (options.bytes !== Array)
            object.chainKey = $util.newBuffer(object.chainKey);
        }
        if (options.bytes === String)
          object.signingKey = "";
        else {
          object.signingKey = [];
          if (options.bytes !== Array)
            object.signingKey = $util.newBuffer(object.signingKey);
        }
      }
      if (message.id != null && message.hasOwnProperty("id"))
        object.id = message.id;
      if (message.iteration != null && message.hasOwnProperty("iteration"))
        object.iteration = message.iteration;
      if (message.chainKey != null && message.hasOwnProperty("chainKey"))
        object.chainKey = options.bytes === String ? $util.base64.encode(message.chainKey, 0, message.chainKey.length) : options.bytes === Array ? Array.prototype.slice.call(message.chainKey) : message.chainKey;
      if (message.signingKey != null && message.hasOwnProperty("signingKey"))
        object.signingKey = options.bytes === String ? $util.base64.encode(message.signingKey, 0, message.signingKey.length) : options.bytes === Array ? Array.prototype.slice.call(message.signingKey) : message.signingKey;
      return object;
    };

    /**
     * Converts this SenderKeyDistributionMessage to JSON.
     * @function toJSON
     * @memberof textsecure.SenderKeyDistributionMessage
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    SenderKeyDistributionMessage.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SenderKeyDistributionMessage;
  })();

  textsecure.SenderKeyStateStructure = (function () {

    /**
     * Properties of a SenderKeyStateStructure.
     * @memberof textsecure
     * @interface ISenderKeyStateStructure
     * @property {number|null} [senderKeyId] SenderKeyStateStructure senderKeyId
     * @property {textsecure.SenderKeyStateStructure.ISenderChainKey|null} [senderChainKey] SenderKeyStateStructure senderChainKey
     * @property {textsecure.SenderKeyStateStructure.ISenderSigningKey|null} [senderSigningKey] SenderKeyStateStructure senderSigningKey
     * @property {Array.<textsecure.SenderKeyStateStructure.ISenderMessageKey>|null} [senderMessageKeys] SenderKeyStateStructure senderMessageKeys
     */

    /**
     * Constructs a new SenderKeyStateStructure.
     * @memberof textsecure
     * @classdesc Represents a SenderKeyStateStructure.
     * @implements ISenderKeyStateStructure
     * @constructor
     * @param {textsecure.ISenderKeyStateStructure=} [properties] Properties to set
     */
    function SenderKeyStateStructure(properties) {
      this.senderMessageKeys = [];
      if (properties)
        for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null)
            this[keys[i]] = properties[keys[i]];
    }

    /**
     * SenderKeyStateStructure senderKeyId.
     * @member {number} senderKeyId
     * @memberof textsecure.SenderKeyStateStructure
     * @instance
     */
    SenderKeyStateStructure.prototype.senderKeyId = 0;

    /**
     * SenderKeyStateStructure senderChainKey.
     * @member {textsecure.SenderKeyStateStructure.ISenderChainKey|null|undefined} senderChainKey
     * @memberof textsecure.SenderKeyStateStructure
     * @instance
     */
    SenderKeyStateStructure.prototype.senderChainKey = null;

    /**
     * SenderKeyStateStructure senderSigningKey.
     * @member {textsecure.SenderKeyStateStructure.ISenderSigningKey|null|undefined} senderSigningKey
     * @memberof textsecure.SenderKeyStateStructure
     * @instance
     */
    SenderKeyStateStructure.prototype.senderSigningKey = null;

    /**
     * SenderKeyStateStructure senderMessageKeys.
     * @member {Array.<textsecure.SenderKeyStateStructure.ISenderMessageKey>} senderMessageKeys
     * @memberof textsecure.SenderKeyStateStructure
     * @instance
     */
    SenderKeyStateStructure.prototype.senderMessageKeys = $util.emptyArray;

    /**
     * Creates a new SenderKeyStateStructure instance using the specified properties.
     * @function create
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {textsecure.ISenderKeyStateStructure=} [properties] Properties to set
     * @returns {textsecure.SenderKeyStateStructure} SenderKeyStateStructure instance
     */
    SenderKeyStateStructure.create = function create(properties) {
      return new SenderKeyStateStructure(properties);
    };

    /**
     * Encodes the specified SenderKeyStateStructure message. Does not implicitly {@link textsecure.SenderKeyStateStructure.verify|verify} messages.
     * @function encode
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {textsecure.ISenderKeyStateStructure} message SenderKeyStateStructure message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyStateStructure.encode = function encode(message, writer) {
      if (!writer)
        writer = $Writer.create();
      if (message.senderKeyId != null && message.hasOwnProperty("senderKeyId"))
        writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.senderKeyId);
      if (message.senderChainKey != null && message.hasOwnProperty("senderChainKey"))
        $root.textsecure.SenderKeyStateStructure.SenderChainKey.encode(message.senderChainKey, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
      if (message.senderSigningKey != null && message.hasOwnProperty("senderSigningKey"))
        $root.textsecure.SenderKeyStateStructure.SenderSigningKey.encode(message.senderSigningKey, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
      if (message.senderMessageKeys != null && message.senderMessageKeys.length)
        for (let i = 0; i < message.senderMessageKeys.length; ++i)
          $root.textsecure.SenderKeyStateStructure.SenderMessageKey.encode(message.senderMessageKeys[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
      return writer;
    };

    /**
     * Encodes the specified SenderKeyStateStructure message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.verify|verify} messages.
     * @function encodeDelimited
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {textsecure.ISenderKeyStateStructure} message SenderKeyStateStructure message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyStateStructure.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a SenderKeyStateStructure message from the specified reader or buffer.
     * @function decode
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {textsecure.SenderKeyStateStructure} SenderKeyStateStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyStateStructure.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader))
        reader = $Reader.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyStateStructure();
      while (reader.pos < end) {
        let tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.senderKeyId = reader.uint32();
            break;
          case 2:
            message.senderChainKey = $root.textsecure.SenderKeyStateStructure.SenderChainKey.decode(reader, reader.uint32());
            break;
          case 3:
            message.senderSigningKey = $root.textsecure.SenderKeyStateStructure.SenderSigningKey.decode(reader, reader.uint32());
            break;
          case 4:
            if (!(message.senderMessageKeys && message.senderMessageKeys.length))
              message.senderMessageKeys = [];
            message.senderMessageKeys.push($root.textsecure.SenderKeyStateStructure.SenderMessageKey.decode(reader, reader.uint32()));
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a SenderKeyStateStructure message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {textsecure.SenderKeyStateStructure} SenderKeyStateStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyStateStructure.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader))
        reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a SenderKeyStateStructure message.
     * @function verify
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    SenderKeyStateStructure.verify = function verify(message) {
      if (typeof message !== "object" || message === null)
        return "object expected";
      if (message.senderKeyId != null && message.hasOwnProperty("senderKeyId"))
        if (!$util.isInteger(message.senderKeyId))
          return "senderKeyId: integer expected";
      if (message.senderChainKey != null && message.hasOwnProperty("senderChainKey")) {
        let error = $root.textsecure.SenderKeyStateStructure.SenderChainKey.verify(message.senderChainKey);
        if (error)
          return "senderChainKey." + error;
      }
      if (message.senderSigningKey != null && message.hasOwnProperty("senderSigningKey")) {
        let error = $root.textsecure.SenderKeyStateStructure.SenderSigningKey.verify(message.senderSigningKey);
        if (error)
          return "senderSigningKey." + error;
      }
      if (message.senderMessageKeys != null && message.hasOwnProperty("senderMessageKeys")) {
        if (!Array.isArray(message.senderMessageKeys))
          return "senderMessageKeys: array expected";
        for (let i = 0; i < message.senderMessageKeys.length; ++i) {
          let error = $root.textsecure.SenderKeyStateStructure.SenderMessageKey.verify(message.senderMessageKeys[i]);
          if (error)
            return "senderMessageKeys." + error;
        }
      }
      return null;
    };

    /**
     * Creates a SenderKeyStateStructure message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {textsecure.SenderKeyStateStructure} SenderKeyStateStructure
     */
    SenderKeyStateStructure.fromObject = function fromObject(object) {
      if (object instanceof $root.textsecure.SenderKeyStateStructure)
        return object;
      let message = new $root.textsecure.SenderKeyStateStructure();
      if (object.senderKeyId != null)
        message.senderKeyId = object.senderKeyId >>> 0;
      if (object.senderChainKey != null) {
        if (typeof object.senderChainKey !== "object")
          throw TypeError(".textsecure.SenderKeyStateStructure.senderChainKey: object expected");
        message.senderChainKey = $root.textsecure.SenderKeyStateStructure.SenderChainKey.fromObject(object.senderChainKey);
      }
      if (object.senderSigningKey != null) {
        if (typeof object.senderSigningKey !== "object")
          throw TypeError(".textsecure.SenderKeyStateStructure.senderSigningKey: object expected");
        message.senderSigningKey = $root.textsecure.SenderKeyStateStructure.SenderSigningKey.fromObject(object.senderSigningKey);
      }
      if (object.senderMessageKeys) {
        if (!Array.isArray(object.senderMessageKeys))
          throw TypeError(".textsecure.SenderKeyStateStructure.senderMessageKeys: array expected");
        message.senderMessageKeys = [];
        for (let i = 0; i < object.senderMessageKeys.length; ++i) {
          if (typeof object.senderMessageKeys[i] !== "object")
            throw TypeError(".textsecure.SenderKeyStateStructure.senderMessageKeys: object expected");
          message.senderMessageKeys[i] = $root.textsecure.SenderKeyStateStructure.SenderMessageKey.fromObject(object.senderMessageKeys[i]);
        }
      }
      return message;
    };

    /**
     * Creates a plain object from a SenderKeyStateStructure message. Also converts values to other types if specified.
     * @function toObject
     * @memberof textsecure.SenderKeyStateStructure
     * @static
     * @param {textsecure.SenderKeyStateStructure} message SenderKeyStateStructure
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    SenderKeyStateStructure.toObject = function toObject(message, options) {
      if (!options)
        options = {};
      let object = {};
      if (options.arrays || options.defaults)
        object.senderMessageKeys = [];
      if (options.defaults) {
        object.senderKeyId = 0;
        object.senderChainKey = null;
        object.senderSigningKey = null;
      }
      if (message.senderKeyId != null && message.hasOwnProperty("senderKeyId"))
        object.senderKeyId = message.senderKeyId;
      if (message.senderChainKey != null && message.hasOwnProperty("senderChainKey"))
        object.senderChainKey = $root.textsecure.SenderKeyStateStructure.SenderChainKey.toObject(message.senderChainKey, options);
      if (message.senderSigningKey != null && message.hasOwnProperty("senderSigningKey"))
        object.senderSigningKey = $root.textsecure.SenderKeyStateStructure.SenderSigningKey.toObject(message.senderSigningKey, options);
      if (message.senderMessageKeys && message.senderMessageKeys.length) {
        object.senderMessageKeys = [];
        for (let j = 0; j < message.senderMessageKeys.length; ++j)
          object.senderMessageKeys[j] = $root.textsecure.SenderKeyStateStructure.SenderMessageKey.toObject(message.senderMessageKeys[j], options);
      }
      return object;
    };

    /**
     * Converts this SenderKeyStateStructure to JSON.
     * @function toJSON
     * @memberof textsecure.SenderKeyStateStructure
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    SenderKeyStateStructure.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    SenderKeyStateStructure.SenderChainKey = (function () {

      /**
       * Properties of a SenderChainKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @interface ISenderChainKey
       * @property {number|null} [iteration] SenderChainKey iteration
       * @property {Uint8Array|null} [seed] SenderChainKey seed
       */

      /**
       * Constructs a new SenderChainKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @classdesc Represents a SenderChainKey.
       * @implements ISenderChainKey
       * @constructor
       * @param {textsecure.SenderKeyStateStructure.ISenderChainKey=} [properties] Properties to set
       */
      function SenderChainKey(properties) {
        if (properties)
          for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * SenderChainKey iteration.
       * @member {number} iteration
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @instance
       */
      SenderChainKey.prototype.iteration = 0;

      /**
       * SenderChainKey seed.
       * @member {Uint8Array} seed
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @instance
       */
      SenderChainKey.prototype.seed = $util.newBuffer([]);

      /**
       * Creates a new SenderChainKey instance using the specified properties.
       * @function create
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderChainKey=} [properties] Properties to set
       * @returns {textsecure.SenderKeyStateStructure.SenderChainKey} SenderChainKey instance
       */
      SenderChainKey.create = function create(properties) {
        return new SenderChainKey(properties);
      };

      /**
       * Encodes the specified SenderChainKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderChainKey.verify|verify} messages.
       * @function encode
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderChainKey} message SenderChainKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderChainKey.encode = function encode(message, writer) {
        if (!writer)
          writer = $Writer.create();
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.iteration);
        if (message.seed != null && message.hasOwnProperty("seed"))
          writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.seed);
        return writer;
      };

      /**
       * Encodes the specified SenderChainKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderChainKey.verify|verify} messages.
       * @function encodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderChainKey} message SenderChainKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderChainKey.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a SenderChainKey message from the specified reader or buffer.
       * @function decode
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {textsecure.SenderKeyStateStructure.SenderChainKey} SenderChainKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderChainKey.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
          reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyStateStructure.SenderChainKey();
        while (reader.pos < end) {
          let tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.iteration = reader.uint32();
              break;
            case 2:
              message.seed = reader.bytes();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a SenderChainKey message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {textsecure.SenderKeyStateStructure.SenderChainKey} SenderChainKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderChainKey.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
          reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a SenderChainKey message.
       * @function verify
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      SenderChainKey.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
          return "object expected";
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          if (!$util.isInteger(message.iteration))
            return "iteration: integer expected";
        if (message.seed != null && message.hasOwnProperty("seed"))
          if (!(message.seed && typeof message.seed.length === "number" || $util.isString(message.seed)))
            return "seed: buffer expected";
        return null;
      };

      /**
       * Creates a SenderChainKey message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {textsecure.SenderKeyStateStructure.SenderChainKey} SenderChainKey
       */
      SenderChainKey.fromObject = function fromObject(object) {
        if (object instanceof $root.textsecure.SenderKeyStateStructure.SenderChainKey)
          return object;
        let message = new $root.textsecure.SenderKeyStateStructure.SenderChainKey();
        if (object.iteration != null)
          message.iteration = object.iteration >>> 0;
        if (object.seed != null)
          if (typeof object.seed === "string")
            $util.base64.decode(object.seed, message.seed = $util.newBuffer($util.base64.length(object.seed)), 0);
          else if (object.seed.length)
            message.seed = object.seed;
        return message;
      };

      /**
       * Creates a plain object from a SenderChainKey message. Also converts values to other types if specified.
       * @function toObject
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.SenderChainKey} message SenderChainKey
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      SenderChainKey.toObject = function toObject(message, options) {
        if (!options)
          options = {};
        let object = {};
        if (options.defaults) {
          object.iteration = 0;
          if (options.bytes === String)
            object.seed = "";
          else {
            object.seed = [];
            if (options.bytes !== Array)
              object.seed = $util.newBuffer(object.seed);
          }
        }
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          object.iteration = message.iteration;
        if (message.seed != null && message.hasOwnProperty("seed"))
          object.seed = options.bytes === String ? $util.base64.encode(message.seed, 0, message.seed.length) : options.bytes === Array ? Array.prototype.slice.call(message.seed) : message.seed;
        return object;
      };

      /**
       * Converts this SenderChainKey to JSON.
       * @function toJSON
       * @memberof textsecure.SenderKeyStateStructure.SenderChainKey
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      SenderChainKey.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return SenderChainKey;
    })();

    SenderKeyStateStructure.SenderMessageKey = (function () {

      /**
       * Properties of a SenderMessageKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @interface ISenderMessageKey
       * @property {number|null} [iteration] SenderMessageKey iteration
       * @property {Uint8Array|null} [seed] SenderMessageKey seed
       */

      /**
       * Constructs a new SenderMessageKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @classdesc Represents a SenderMessageKey.
       * @implements ISenderMessageKey
       * @constructor
       * @param {textsecure.SenderKeyStateStructure.ISenderMessageKey=} [properties] Properties to set
       */
      function SenderMessageKey(properties) {
        if (properties)
          for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * SenderMessageKey iteration.
       * @member {number} iteration
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @instance
       */
      SenderMessageKey.prototype.iteration = 0;

      /**
       * SenderMessageKey seed.
       * @member {Uint8Array} seed
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @instance
       */
      SenderMessageKey.prototype.seed = $util.newBuffer([]);

      /**
       * Creates a new SenderMessageKey instance using the specified properties.
       * @function create
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderMessageKey=} [properties] Properties to set
       * @returns {textsecure.SenderKeyStateStructure.SenderMessageKey} SenderMessageKey instance
       */
      SenderMessageKey.create = function create(properties) {
        return new SenderMessageKey(properties);
      };

      /**
       * Encodes the specified SenderMessageKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderMessageKey.verify|verify} messages.
       * @function encode
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderMessageKey} message SenderMessageKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderMessageKey.encode = function encode(message, writer) {
        if (!writer)
          writer = $Writer.create();
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.iteration);
        if (message.seed != null && message.hasOwnProperty("seed"))
          writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.seed);
        return writer;
      };

      /**
       * Encodes the specified SenderMessageKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderMessageKey.verify|verify} messages.
       * @function encodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderMessageKey} message SenderMessageKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderMessageKey.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a SenderMessageKey message from the specified reader or buffer.
       * @function decode
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {textsecure.SenderKeyStateStructure.SenderMessageKey} SenderMessageKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderMessageKey.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
          reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyStateStructure.SenderMessageKey();
        while (reader.pos < end) {
          let tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.iteration = reader.uint32();
              break;
            case 2:
              message.seed = reader.bytes();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a SenderMessageKey message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {textsecure.SenderKeyStateStructure.SenderMessageKey} SenderMessageKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderMessageKey.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
          reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a SenderMessageKey message.
       * @function verify
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      SenderMessageKey.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
          return "object expected";
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          if (!$util.isInteger(message.iteration))
            return "iteration: integer expected";
        if (message.seed != null && message.hasOwnProperty("seed"))
          if (!(message.seed && typeof message.seed.length === "number" || $util.isString(message.seed)))
            return "seed: buffer expected";
        return null;
      };

      /**
       * Creates a SenderMessageKey message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {textsecure.SenderKeyStateStructure.SenderMessageKey} SenderMessageKey
       */
      SenderMessageKey.fromObject = function fromObject(object) {
        if (object instanceof $root.textsecure.SenderKeyStateStructure.SenderMessageKey)
          return object;
        let message = new $root.textsecure.SenderKeyStateStructure.SenderMessageKey();
        if (object.iteration != null)
          message.iteration = object.iteration >>> 0;
        if (object.seed != null)
          if (typeof object.seed === "string")
            $util.base64.decode(object.seed, message.seed = $util.newBuffer($util.base64.length(object.seed)), 0);
          else if (object.seed.length)
            message.seed = object.seed;
        return message;
      };

      /**
       * Creates a plain object from a SenderMessageKey message. Also converts values to other types if specified.
       * @function toObject
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.SenderMessageKey} message SenderMessageKey
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      SenderMessageKey.toObject = function toObject(message, options) {
        if (!options)
          options = {};
        let object = {};
        if (options.defaults) {
          object.iteration = 0;
          if (options.bytes === String)
            object.seed = "";
          else {
            object.seed = [];
            if (options.bytes !== Array)
              object.seed = $util.newBuffer(object.seed);
          }
        }
        if (message.iteration != null && message.hasOwnProperty("iteration"))
          object.iteration = message.iteration;
        if (message.seed != null && message.hasOwnProperty("seed"))
          object.seed = options.bytes === String ? $util.base64.encode(message.seed, 0, message.seed.length) : options.bytes === Array ? Array.prototype.slice.call(message.seed) : message.seed;
        return object;
      };

      /**
       * Converts this SenderMessageKey to JSON.
       * @function toJSON
       * @memberof textsecure.SenderKeyStateStructure.SenderMessageKey
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      SenderMessageKey.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return SenderMessageKey;
    })();

    SenderKeyStateStructure.SenderSigningKey = (function () {

      /**
       * Properties of a SenderSigningKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @interface ISenderSigningKey
       * @property {Uint8Array|null} ["public"] SenderSigningKey public
       * @property {Uint8Array|null} ["private"] SenderSigningKey private
       */

      /**
       * Constructs a new SenderSigningKey.
       * @memberof textsecure.SenderKeyStateStructure
       * @classdesc Represents a SenderSigningKey.
       * @implements ISenderSigningKey
       * @constructor
       * @param {textsecure.SenderKeyStateStructure.ISenderSigningKey=} [properties] Properties to set
       */
      function SenderSigningKey(properties) {
        if (properties)
          for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * SenderSigningKey public.
       * @member {Uint8Array} public
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @instance
       */
      SenderSigningKey.prototype["public"] = $util.newBuffer([]);

      /**
       * SenderSigningKey private.
       * @member {Uint8Array} private
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @instance
       */
      SenderSigningKey.prototype["private"] = $util.newBuffer([]);

      /**
       * Creates a new SenderSigningKey instance using the specified properties.
       * @function create
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderSigningKey=} [properties] Properties to set
       * @returns {textsecure.SenderKeyStateStructure.SenderSigningKey} SenderSigningKey instance
       */
      SenderSigningKey.create = function create(properties) {
        return new SenderSigningKey(properties);
      };

      /**
       * Encodes the specified SenderSigningKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderSigningKey.verify|verify} messages.
       * @function encode
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderSigningKey} message SenderSigningKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderSigningKey.encode = function encode(message, writer) {
        if (!writer)
          writer = $Writer.create();
        if (message["public"] != null && message.hasOwnProperty("public"))
          writer.uint32(/* id 1, wireType 2 =*/10).bytes(message["public"]);
        if (message["private"] != null && message.hasOwnProperty("private"))
          writer.uint32(/* id 2, wireType 2 =*/18).bytes(message["private"]);
        return writer;
      };

      /**
       * Encodes the specified SenderSigningKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderSigningKey.verify|verify} messages.
       * @function encodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.ISenderSigningKey} message SenderSigningKey message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      SenderSigningKey.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a SenderSigningKey message from the specified reader or buffer.
       * @function decode
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {textsecure.SenderKeyStateStructure.SenderSigningKey} SenderSigningKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderSigningKey.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
          reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyStateStructure.SenderSigningKey();
        while (reader.pos < end) {
          let tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message["public"] = reader.bytes();
              break;
            case 2:
              message["private"] = reader.bytes();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a SenderSigningKey message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {textsecure.SenderKeyStateStructure.SenderSigningKey} SenderSigningKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      SenderSigningKey.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
          reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a SenderSigningKey message.
       * @function verify
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      SenderSigningKey.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
          return "object expected";
        if (message["public"] != null && message.hasOwnProperty("public"))
          if (!(message["public"] && typeof message["public"].length === "number" || $util.isString(message["public"])))
            return "public: buffer expected";
        if (message["private"] != null && message.hasOwnProperty("private"))
          if (!(message["private"] && typeof message["private"].length === "number" || $util.isString(message["private"])))
            return "private: buffer expected";
        return null;
      };

      /**
       * Creates a SenderSigningKey message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {textsecure.SenderKeyStateStructure.SenderSigningKey} SenderSigningKey
       */
      SenderSigningKey.fromObject = function fromObject(object) {
        if (object instanceof $root.textsecure.SenderKeyStateStructure.SenderSigningKey)
          return object;
        let message = new $root.textsecure.SenderKeyStateStructure.SenderSigningKey();
        if (object["public"] != null)
          if (typeof object["public"] === "string")
            $util.base64.decode(object["public"], message["public"] = $util.newBuffer($util.base64.length(object["public"])), 0);
          else if (object["public"].length)
            message["public"] = object["public"];
        if (object["private"] != null)
          if (typeof object["private"] === "string")
            $util.base64.decode(object["private"], message["private"] = $util.newBuffer($util.base64.length(object["private"])), 0);
          else if (object["private"].length)
            message["private"] = object["private"];
        return message;
      };

      /**
       * Creates a plain object from a SenderSigningKey message. Also converts values to other types if specified.
       * @function toObject
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @static
       * @param {textsecure.SenderKeyStateStructure.SenderSigningKey} message SenderSigningKey
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      SenderSigningKey.toObject = function toObject(message, options) {
        if (!options)
          options = {};
        let object = {};
        if (options.defaults) {
          if (options.bytes === String)
            object["public"] = "";
          else {
            object["public"] = [];
            if (options.bytes !== Array)
              object["public"] = $util.newBuffer(object["public"]);
          }
          if (options.bytes === String)
            object["private"] = "";
          else {
            object["private"] = [];
            if (options.bytes !== Array)
              object["private"] = $util.newBuffer(object["private"]);
          }
        }
        if (message["public"] != null && message.hasOwnProperty("public"))
          object["public"] = options.bytes === String ? $util.base64.encode(message["public"], 0, message["public"].length) : options.bytes === Array ? Array.prototype.slice.call(message["public"]) : message["public"];
        if (message["private"] != null && message.hasOwnProperty("private"))
          object["private"] = options.bytes === String ? $util.base64.encode(message["private"], 0, message["private"].length) : options.bytes === Array ? Array.prototype.slice.call(message["private"]) : message["private"];
        return object;
      };

      /**
       * Converts this SenderSigningKey to JSON.
       * @function toJSON
       * @memberof textsecure.SenderKeyStateStructure.SenderSigningKey
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      SenderSigningKey.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return SenderSigningKey;
    })();

    return SenderKeyStateStructure;
  })();

  textsecure.SenderKeyRecordStructure = (function () {

    /**
     * Properties of a SenderKeyRecordStructure.
     * @memberof textsecure
     * @interface ISenderKeyRecordStructure
     * @property {Array.<textsecure.ISenderKeyStateStructure>|null} [senderKeyStates] SenderKeyRecordStructure senderKeyStates
     */

    /**
     * Constructs a new SenderKeyRecordStructure.
     * @memberof textsecure
     * @classdesc Represents a SenderKeyRecordStructure.
     * @implements ISenderKeyRecordStructure
     * @constructor
     * @param {textsecure.ISenderKeyRecordStructure=} [properties] Properties to set
     */
    function SenderKeyRecordStructure(properties) {
      this.senderKeyStates = [];
      if (properties)
        for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null)
            this[keys[i]] = properties[keys[i]];
    }

    /**
     * SenderKeyRecordStructure senderKeyStates.
     * @member {Array.<textsecure.ISenderKeyStateStructure>} senderKeyStates
     * @memberof textsecure.SenderKeyRecordStructure
     * @instance
     */
    SenderKeyRecordStructure.prototype.senderKeyStates = $util.emptyArray;

    /**
     * Creates a new SenderKeyRecordStructure instance using the specified properties.
     * @function create
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {textsecure.ISenderKeyRecordStructure=} [properties] Properties to set
     * @returns {textsecure.SenderKeyRecordStructure} SenderKeyRecordStructure instance
     */
    SenderKeyRecordStructure.create = function create(properties) {
      return new SenderKeyRecordStructure(properties);
    };

    /**
     * Encodes the specified SenderKeyRecordStructure message. Does not implicitly {@link textsecure.SenderKeyRecordStructure.verify|verify} messages.
     * @function encode
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {textsecure.ISenderKeyRecordStructure} message SenderKeyRecordStructure message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyRecordStructure.encode = function encode(message, writer) {
      if (!writer)
        writer = $Writer.create();
      if (message.senderKeyStates != null && message.senderKeyStates.length)
        for (let i = 0; i < message.senderKeyStates.length; ++i)
          $root.textsecure.SenderKeyStateStructure.encode(message.senderKeyStates[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
      return writer;
    };

    /**
     * Encodes the specified SenderKeyRecordStructure message, length delimited. Does not implicitly {@link textsecure.SenderKeyRecordStructure.verify|verify} messages.
     * @function encodeDelimited
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {textsecure.ISenderKeyRecordStructure} message SenderKeyRecordStructure message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SenderKeyRecordStructure.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a SenderKeyRecordStructure message from the specified reader or buffer.
     * @function decode
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {textsecure.SenderKeyRecordStructure} SenderKeyRecordStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyRecordStructure.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader))
        reader = $Reader.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length, message = new $root.textsecure.SenderKeyRecordStructure();
      while (reader.pos < end) {
        let tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            if (!(message.senderKeyStates && message.senderKeyStates.length))
              message.senderKeyStates = [];
            message.senderKeyStates.push($root.textsecure.SenderKeyStateStructure.decode(reader, reader.uint32()));
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a SenderKeyRecordStructure message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {textsecure.SenderKeyRecordStructure} SenderKeyRecordStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SenderKeyRecordStructure.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader))
        reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a SenderKeyRecordStructure message.
     * @function verify
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    SenderKeyRecordStructure.verify = function verify(message) {
      if (typeof message !== "object" || message === null)
        return "object expected";
      if (message.senderKeyStates != null && message.hasOwnProperty("senderKeyStates")) {
        if (!Array.isArray(message.senderKeyStates))
          return "senderKeyStates: array expected";
        for (let i = 0; i < message.senderKeyStates.length; ++i) {
          let error = $root.textsecure.SenderKeyStateStructure.verify(message.senderKeyStates[i]);
          if (error)
            return "senderKeyStates." + error;
        }
      }
      return null;
    };

    /**
     * Creates a SenderKeyRecordStructure message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {textsecure.SenderKeyRecordStructure} SenderKeyRecordStructure
     */
    SenderKeyRecordStructure.fromObject = function fromObject(object) {
      if (object instanceof $root.textsecure.SenderKeyRecordStructure)
        return object;
      let message = new $root.textsecure.SenderKeyRecordStructure();
      if (object.senderKeyStates) {
        if (!Array.isArray(object.senderKeyStates))
          throw TypeError(".textsecure.SenderKeyRecordStructure.senderKeyStates: array expected");
        message.senderKeyStates = [];
        for (let i = 0; i < object.senderKeyStates.length; ++i) {
          if (typeof object.senderKeyStates[i] !== "object")
            throw TypeError(".textsecure.SenderKeyRecordStructure.senderKeyStates: object expected");
          message.senderKeyStates[i] = $root.textsecure.SenderKeyStateStructure.fromObject(object.senderKeyStates[i]);
        }
      }
      return message;
    };

    /**
     * Creates a plain object from a SenderKeyRecordStructure message. Also converts values to other types if specified.
     * @function toObject
     * @memberof textsecure.SenderKeyRecordStructure
     * @static
     * @param {textsecure.SenderKeyRecordStructure} message SenderKeyRecordStructure
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    SenderKeyRecordStructure.toObject = function toObject(message, options) {
      if (!options)
        options = {};
      let object = {};
      if (options.arrays || options.defaults)
        object.senderKeyStates = [];
      if (message.senderKeyStates && message.senderKeyStates.length) {
        object.senderKeyStates = [];
        for (let j = 0; j < message.senderKeyStates.length; ++j)
          object.senderKeyStates[j] = $root.textsecure.SenderKeyStateStructure.toObject(message.senderKeyStates[j], options);
      }
      return object;
    };

    /**
     * Converts this SenderKeyRecordStructure to JSON.
     * @function toJSON
     * @memberof textsecure.SenderKeyRecordStructure
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    SenderKeyRecordStructure.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SenderKeyRecordStructure;
  })();

  return textsecure;
})();

export { $root as default };
