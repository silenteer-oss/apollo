/*eslint-disable*/
import * as $protobuf from "protobufjs";
/** Namespace textsecure. */
export namespace textsecure {

  /** Properties of a SenderKeyMessage. */
  interface ISenderKeyMessage {

    /** SenderKeyMessage id */
    id?: (number | null);

    /** SenderKeyMessage iteration */
    iteration?: (number | null);

    /** SenderKeyMessage ciphertext */
    ciphertext?: (Uint8Array | null);
  }

  /** Represents a SenderKeyMessage. */
  class SenderKeyMessage implements ISenderKeyMessage {

    /**
     * Constructs a new SenderKeyMessage.
     * @param [properties] Properties to set
     */
    constructor(properties?: textsecure.ISenderKeyMessage);

    /** SenderKeyMessage id. */
    public id: number;

    /** SenderKeyMessage iteration. */
    public iteration: number;

    /** SenderKeyMessage ciphertext. */
    public ciphertext: Uint8Array;

    /**
     * Creates a new SenderKeyMessage instance using the specified properties.
     * @param [properties] Properties to set
     * @returns SenderKeyMessage instance
     */
    public static create(properties?: textsecure.ISenderKeyMessage): textsecure.SenderKeyMessage;

    /**
     * Encodes the specified SenderKeyMessage message. Does not implicitly {@link textsecure.SenderKeyMessage.verify|verify} messages.
     * @param message SenderKeyMessage message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: textsecure.ISenderKeyMessage, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified SenderKeyMessage message, length delimited. Does not implicitly {@link textsecure.SenderKeyMessage.verify|verify} messages.
     * @param message SenderKeyMessage message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: textsecure.ISenderKeyMessage, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a SenderKeyMessage message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns SenderKeyMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyMessage;

    /**
     * Decodes a SenderKeyMessage message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns SenderKeyMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyMessage;

    /**
     * Verifies a SenderKeyMessage message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string | null);

    /**
     * Creates a SenderKeyMessage message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns SenderKeyMessage
     */
    public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyMessage;

    /**
     * Creates a plain object from a SenderKeyMessage message. Also converts values to other types if specified.
     * @param message SenderKeyMessage
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: textsecure.SenderKeyMessage, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this SenderKeyMessage to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a SenderKeyDistributionMessage. */
  interface ISenderKeyDistributionMessage {

    /** SenderKeyDistributionMessage id */
    id?: (number | null);

    /** SenderKeyDistributionMessage iteration */
    iteration?: (number | null);

    /** SenderKeyDistributionMessage chainKey */
    chainKey?: (Uint8Array | null);

    /** SenderKeyDistributionMessage signingKey */
    signingKey?: (Uint8Array | null);
  }

  /** Represents a SenderKeyDistributionMessage. */
  class SenderKeyDistributionMessage implements ISenderKeyDistributionMessage {

    /**
     * Constructs a new SenderKeyDistributionMessage.
     * @param [properties] Properties to set
     */
    constructor(properties?: textsecure.ISenderKeyDistributionMessage);

    /** SenderKeyDistributionMessage id. */
    public id: number;

    /** SenderKeyDistributionMessage iteration. */
    public iteration: number;

    /** SenderKeyDistributionMessage chainKey. */
    public chainKey: Uint8Array;

    /** SenderKeyDistributionMessage signingKey. */
    public signingKey: Uint8Array;

    /**
     * Creates a new SenderKeyDistributionMessage instance using the specified properties.
     * @param [properties] Properties to set
     * @returns SenderKeyDistributionMessage instance
     */
    public static create(properties?: textsecure.ISenderKeyDistributionMessage): textsecure.SenderKeyDistributionMessage;

    /**
     * Encodes the specified SenderKeyDistributionMessage message. Does not implicitly {@link textsecure.SenderKeyDistributionMessage.verify|verify} messages.
     * @param message SenderKeyDistributionMessage message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: textsecure.ISenderKeyDistributionMessage, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified SenderKeyDistributionMessage message, length delimited. Does not implicitly {@link textsecure.SenderKeyDistributionMessage.verify|verify} messages.
     * @param message SenderKeyDistributionMessage message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: textsecure.ISenderKeyDistributionMessage, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a SenderKeyDistributionMessage message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns SenderKeyDistributionMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyDistributionMessage;

    /**
     * Decodes a SenderKeyDistributionMessage message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns SenderKeyDistributionMessage
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyDistributionMessage;

    /**
     * Verifies a SenderKeyDistributionMessage message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string | null);

    /**
     * Creates a SenderKeyDistributionMessage message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns SenderKeyDistributionMessage
     */
    public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyDistributionMessage;

    /**
     * Creates a plain object from a SenderKeyDistributionMessage message. Also converts values to other types if specified.
     * @param message SenderKeyDistributionMessage
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: textsecure.SenderKeyDistributionMessage, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this SenderKeyDistributionMessage to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a SenderKeyStateStructure. */
  interface ISenderKeyStateStructure {

    /** SenderKeyStateStructure senderKeyId */
    senderKeyId?: (number | null);

    /** SenderKeyStateStructure senderChainKey */
    senderChainKey?: (textsecure.SenderKeyStateStructure.ISenderChainKey | null);

    /** SenderKeyStateStructure senderSigningKey */
    senderSigningKey?: (textsecure.SenderKeyStateStructure.ISenderSigningKey | null);

    /** SenderKeyStateStructure senderMessageKeys */
    senderMessageKeys?: (textsecure.SenderKeyStateStructure.ISenderMessageKey[] | null);
  }

  /** Represents a SenderKeyStateStructure. */
  class SenderKeyStateStructure implements ISenderKeyStateStructure {

    /**
     * Constructs a new SenderKeyStateStructure.
     * @param [properties] Properties to set
     */
    constructor(properties?: textsecure.ISenderKeyStateStructure);

    /** SenderKeyStateStructure senderKeyId. */
    public senderKeyId: number;

    /** SenderKeyStateStructure senderChainKey. */
    public senderChainKey?: (textsecure.SenderKeyStateStructure.ISenderChainKey | null);

    /** SenderKeyStateStructure senderSigningKey. */
    public senderSigningKey?: (textsecure.SenderKeyStateStructure.ISenderSigningKey | null);

    /** SenderKeyStateStructure senderMessageKeys. */
    public senderMessageKeys: textsecure.SenderKeyStateStructure.ISenderMessageKey[];

    /**
     * Creates a new SenderKeyStateStructure instance using the specified properties.
     * @param [properties] Properties to set
     * @returns SenderKeyStateStructure instance
     */
    public static create(properties?: textsecure.ISenderKeyStateStructure): textsecure.SenderKeyStateStructure;

    /**
     * Encodes the specified SenderKeyStateStructure message. Does not implicitly {@link textsecure.SenderKeyStateStructure.verify|verify} messages.
     * @param message SenderKeyStateStructure message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: textsecure.ISenderKeyStateStructure, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified SenderKeyStateStructure message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.verify|verify} messages.
     * @param message SenderKeyStateStructure message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: textsecure.ISenderKeyStateStructure, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a SenderKeyStateStructure message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns SenderKeyStateStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyStateStructure;

    /**
     * Decodes a SenderKeyStateStructure message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns SenderKeyStateStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyStateStructure;

    /**
     * Verifies a SenderKeyStateStructure message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string | null);

    /**
     * Creates a SenderKeyStateStructure message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns SenderKeyStateStructure
     */
    public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyStateStructure;

    /**
     * Creates a plain object from a SenderKeyStateStructure message. Also converts values to other types if specified.
     * @param message SenderKeyStateStructure
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: textsecure.SenderKeyStateStructure, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this SenderKeyStateStructure to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace SenderKeyStateStructure {

    /** Properties of a SenderChainKey. */
    interface ISenderChainKey {

      /** SenderChainKey iteration */
      iteration?: (number | null);

      /** SenderChainKey seed */
      seed?: (Uint8Array | null);
    }

    /** Represents a SenderChainKey. */
    class SenderChainKey implements ISenderChainKey {

      /**
       * Constructs a new SenderChainKey.
       * @param [properties] Properties to set
       */
      constructor(properties?: textsecure.SenderKeyStateStructure.ISenderChainKey);

      /** SenderChainKey iteration. */
      public iteration: number;

      /** SenderChainKey seed. */
      public seed: Uint8Array;

      /**
       * Creates a new SenderChainKey instance using the specified properties.
       * @param [properties] Properties to set
       * @returns SenderChainKey instance
       */
      public static create(properties?: textsecure.SenderKeyStateStructure.ISenderChainKey): textsecure.SenderKeyStateStructure.SenderChainKey;

      /**
       * Encodes the specified SenderChainKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderChainKey.verify|verify} messages.
       * @param message SenderChainKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(message: textsecure.SenderKeyStateStructure.ISenderChainKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Encodes the specified SenderChainKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderChainKey.verify|verify} messages.
       * @param message SenderChainKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(message: textsecure.SenderKeyStateStructure.ISenderChainKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Decodes a SenderChainKey message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns SenderChainKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyStateStructure.SenderChainKey;

      /**
       * Decodes a SenderChainKey message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns SenderChainKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyStateStructure.SenderChainKey;

      /**
       * Verifies a SenderChainKey message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): (string | null);

      /**
       * Creates a SenderChainKey message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns SenderChainKey
       */
      public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyStateStructure.SenderChainKey;

      /**
       * Creates a plain object from a SenderChainKey message. Also converts values to other types if specified.
       * @param message SenderChainKey
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(message: textsecure.SenderKeyStateStructure.SenderChainKey, options?: $protobuf.IConversionOptions): { [k: string]: any };

      /**
       * Converts this SenderChainKey to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a SenderMessageKey. */
    interface ISenderMessageKey {

      /** SenderMessageKey iteration */
      iteration?: (number | null);

      /** SenderMessageKey seed */
      seed?: (Uint8Array | null);
    }

    /** Represents a SenderMessageKey. */
    class SenderMessageKey implements ISenderMessageKey {

      /**
       * Constructs a new SenderMessageKey.
       * @param [properties] Properties to set
       */
      constructor(properties?: textsecure.SenderKeyStateStructure.ISenderMessageKey);

      /** SenderMessageKey iteration. */
      public iteration: number;

      /** SenderMessageKey seed. */
      public seed: Uint8Array;

      /**
       * Creates a new SenderMessageKey instance using the specified properties.
       * @param [properties] Properties to set
       * @returns SenderMessageKey instance
       */
      public static create(properties?: textsecure.SenderKeyStateStructure.ISenderMessageKey): textsecure.SenderKeyStateStructure.SenderMessageKey;

      /**
       * Encodes the specified SenderMessageKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderMessageKey.verify|verify} messages.
       * @param message SenderMessageKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(message: textsecure.SenderKeyStateStructure.ISenderMessageKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Encodes the specified SenderMessageKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderMessageKey.verify|verify} messages.
       * @param message SenderMessageKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(message: textsecure.SenderKeyStateStructure.ISenderMessageKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Decodes a SenderMessageKey message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns SenderMessageKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyStateStructure.SenderMessageKey;

      /**
       * Decodes a SenderMessageKey message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns SenderMessageKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyStateStructure.SenderMessageKey;

      /**
       * Verifies a SenderMessageKey message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): (string | null);

      /**
       * Creates a SenderMessageKey message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns SenderMessageKey
       */
      public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyStateStructure.SenderMessageKey;

      /**
       * Creates a plain object from a SenderMessageKey message. Also converts values to other types if specified.
       * @param message SenderMessageKey
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(message: textsecure.SenderKeyStateStructure.SenderMessageKey, options?: $protobuf.IConversionOptions): { [k: string]: any };

      /**
       * Converts this SenderMessageKey to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a SenderSigningKey. */
    interface ISenderSigningKey {

      /** SenderSigningKey public */
      "public"?: (Uint8Array | null);

      /** SenderSigningKey private */
      "private"?: (Uint8Array | null);
    }

    /** Represents a SenderSigningKey. */
    class SenderSigningKey implements ISenderSigningKey {

      /**
       * Constructs a new SenderSigningKey.
       * @param [properties] Properties to set
       */
      constructor(properties?: textsecure.SenderKeyStateStructure.ISenderSigningKey);

      /** SenderSigningKey public. */
      public public: Uint8Array;

      /** SenderSigningKey private. */
      public private: Uint8Array;

      /**
       * Creates a new SenderSigningKey instance using the specified properties.
       * @param [properties] Properties to set
       * @returns SenderSigningKey instance
       */
      public static create(properties?: textsecure.SenderKeyStateStructure.ISenderSigningKey): textsecure.SenderKeyStateStructure.SenderSigningKey;

      /**
       * Encodes the specified SenderSigningKey message. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderSigningKey.verify|verify} messages.
       * @param message SenderSigningKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(message: textsecure.SenderKeyStateStructure.ISenderSigningKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Encodes the specified SenderSigningKey message, length delimited. Does not implicitly {@link textsecure.SenderKeyStateStructure.SenderSigningKey.verify|verify} messages.
       * @param message SenderSigningKey message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(message: textsecure.SenderKeyStateStructure.ISenderSigningKey, writer?: $protobuf.Writer): $protobuf.Writer;

      /**
       * Decodes a SenderSigningKey message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns SenderSigningKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyStateStructure.SenderSigningKey;

      /**
       * Decodes a SenderSigningKey message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns SenderSigningKey
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyStateStructure.SenderSigningKey;

      /**
       * Verifies a SenderSigningKey message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): (string | null);

      /**
       * Creates a SenderSigningKey message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns SenderSigningKey
       */
      public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyStateStructure.SenderSigningKey;

      /**
       * Creates a plain object from a SenderSigningKey message. Also converts values to other types if specified.
       * @param message SenderSigningKey
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(message: textsecure.SenderKeyStateStructure.SenderSigningKey, options?: $protobuf.IConversionOptions): { [k: string]: any };

      /**
       * Converts this SenderSigningKey to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a SenderKeyRecordStructure. */
  interface ISenderKeyRecordStructure {

    /** SenderKeyRecordStructure senderKeyStates */
    senderKeyStates?: (textsecure.ISenderKeyStateStructure[] | null);
  }

  /** Represents a SenderKeyRecordStructure. */
  class SenderKeyRecordStructure implements ISenderKeyRecordStructure {

    /**
     * Constructs a new SenderKeyRecordStructure.
     * @param [properties] Properties to set
     */
    constructor(properties?: textsecure.ISenderKeyRecordStructure);

    /** SenderKeyRecordStructure senderKeyStates. */
    public senderKeyStates: textsecure.ISenderKeyStateStructure[];

    /**
     * Creates a new SenderKeyRecordStructure instance using the specified properties.
     * @param [properties] Properties to set
     * @returns SenderKeyRecordStructure instance
     */
    public static create(properties?: textsecure.ISenderKeyRecordStructure): textsecure.SenderKeyRecordStructure;

    /**
     * Encodes the specified SenderKeyRecordStructure message. Does not implicitly {@link textsecure.SenderKeyRecordStructure.verify|verify} messages.
     * @param message SenderKeyRecordStructure message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: textsecure.ISenderKeyRecordStructure, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified SenderKeyRecordStructure message, length delimited. Does not implicitly {@link textsecure.SenderKeyRecordStructure.verify|verify} messages.
     * @param message SenderKeyRecordStructure message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: textsecure.ISenderKeyRecordStructure, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a SenderKeyRecordStructure message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns SenderKeyRecordStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader | Uint8Array), length?: number): textsecure.SenderKeyRecordStructure;

    /**
     * Decodes a SenderKeyRecordStructure message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns SenderKeyRecordStructure
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader | Uint8Array)): textsecure.SenderKeyRecordStructure;

    /**
     * Verifies a SenderKeyRecordStructure message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string | null);

    /**
     * Creates a SenderKeyRecordStructure message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns SenderKeyRecordStructure
     */
    public static fromObject(object: { [k: string]: any }): textsecure.SenderKeyRecordStructure;

    /**
     * Creates a plain object from a SenderKeyRecordStructure message. Also converts values to other types if specified.
     * @param message SenderKeyRecordStructure
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: textsecure.SenderKeyRecordStructure, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this SenderKeyRecordStructure to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }
}
