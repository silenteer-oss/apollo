import {
  combineVersion,
  combineBytes,
  convertBinaryStringToBytes,
  convertBytesToBinaryString,
  convertBytesToPlaintext,
  convertPlaintextToBytes
} from './byte-util';
import { sign, verify, encrypt, decrypt } from '../crypto/crypto';
import {
  createSenderKeyMessage,
  encodeSenderKeyMessage,
  decodeSenderKeyMessage,
} from './proto-util';
import { SignalState } from './signal-state';
import { ISignalMessageKey } from './signal-message-key';
import { SignalAddress } from './signal-address';
import { BaseSignalStorage } from './signal-storage';

export class SignalCipher {
  constructor(
    private _storage: BaseSignalStorage,
    private _address: SignalAddress
  ) {
    // eslint-disable-next-line prettier/prettier
  }

  async encrypt(plaintext: string): Promise<string> {
    const record = await this._storage.loadRecord(this._address);
    const state = record.getState();

    const { iteration, cipherKey, iv } = await state.chain.getMessageKey();
    const plaintextBytes = convertPlaintextToBytes(plaintext);
    const ciphertext = await encrypt(iv, cipherKey, plaintextBytes);

    const message = createSenderKeyMessage();
    message.id = state.id;
    message.iteration = iteration;
    message.ciphertext = ciphertext;

    const versionEncodedMessage = combineVersion(
      encodeSenderKeyMessage(message)
    );

    const signature = await sign(
      versionEncodedMessage,
      state.signatureKeyPair.privKey
    );
    const result = combineBytes(versionEncodedMessage, signature);

    const nextChain = await state.chain.getNext();
    state.chain = nextChain;
    this._storage.storeRecord(this._address, record);

    return convertBytesToBinaryString(result);
  }

  async decrypt(ciphertext: string): Promise<string> {
    const ciphertextBytes = convertBinaryStringToBytes(ciphertext);
    const version = ciphertextBytes[0];
    if ((version & 0xf) > 3 || version >> 4 < 3) {
      // min version > 3 or max version < 3
      throw new Error('Incompatible version number on messageBuffer');
    }

    const decodedMessage = decodeSenderKeyMessage(
      ciphertextBytes.slice(1, ciphertextBytes.byteLength - 64)
    );

    const record = await this._storage.loadRecord(this._address);
    if (record.isEmpty) {
      throw new Error(`Sender key not found for ${this._address}`);
    }

    const state = record.getState(decodedMessage.id);

    await verify(
      ciphertextBytes.slice(0, ciphertextBytes.byteLength - 64),
      ciphertextBytes.slice(
        ciphertextBytes.byteLength - 64,
        ciphertextBytes.byteLength
      ),
      state.signatureKeyPair.pubKey
    );

    const { cipherKey, iv } = await this._getMessageKey(
      state,
      decodedMessage.iteration
    );
    const result = await decrypt(iv, cipherKey, decodedMessage.ciphertext);

    this._storage.storeRecord(this._address, record);

    return convertBytesToPlaintext(result);
  }

  private async _getMessageKey(
    state: SignalState,
    iteration: number
  ): Promise<ISignalMessageKey> {
    let chain = state.chain;

    if (chain.iteration > iteration) {
      if (state.hasMessageKey(iteration)) {
        return state.removeMessageKey(iteration);
      } else {
        throw new Error(
          `Receive old message, iteration ${chain.iteration} > ${iteration}`
        );
      }
    }

    if (iteration - chain.iteration > 2000) {
      throw new Error('Over 2000 messages into the future!');
    }

    while (chain.iteration < iteration) {
      const messageKey = await chain.getMessageKeyToStore();
      state.addMessageKey(messageKey);
      chain = await chain.getNext();
    }

    const nextChain = await chain.getNext();
    state.chain = nextChain;
    return await chain.getMessageKey();
  }
}