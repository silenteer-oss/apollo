import {
  encodeSenderKeyDistributionMessage,
  decodeSenderKeyDistributionMessage,
  createSenderKeyDistributionMessage,
} from './proto-util';
import { combineVersion, convertBinaryStringToBytes, convertBytesToBinaryString } from './byte-util';
import { generateSignKeyPair, getRandomBytes } from '../crypto/crypto';
import { SignalAddress } from './signal-address';
import { SignalState } from './signal-state';
import { SignalRecord } from './signal-record';
import { BaseSignalStorage } from './signal-storage';

export interface CreateSessionResult {
  distributionMessage: string;
  signingPrivateKey: string;
}

export class SignalSessionBuilder {
  constructor(private _storage: BaseSignalStorage) {
    // eslint-disable-next-line prettier/prettier
  }

  async process(
    address: SignalAddress,
    distributionMessage: string,
    signingPrivateKey?: string
  ) {
    const distributionMessageBytes = convertBinaryStringToBytes(
      distributionMessage
    );
    const signingPrivateKeyBytes = signingPrivateKey && convertBinaryStringToBytes(signingPrivateKey);

    const version = distributionMessageBytes[0];
    if ((version & 0xf) > 3 || version >> 4 < 3) {
      // min version > 3 or max version < 3
      throw new Error('Incompatible version number on distributionMessage');
    }
    const messageProto = distributionMessageBytes.slice(
      1,
      distributionMessageBytes.byteLength
    );
    const message = decodeSenderKeyDistributionMessage(messageProto);

    const record = await this._storage.loadRecord(address);
    record.addState(
      new SignalState(message.id, message.iteration, message.chainKey, {
        pubKey: message.signingKey,
        privKey: signingPrivateKeyBytes,
      })
    );
    
    await this._storage.storeRecord(address, record);
  }

  async create(address: SignalAddress): Promise<CreateSessionResult> {
    let record = await this._storage.loadRecord(address);

    if (record.isEmpty) {
      record = new SignalRecord();
      record.addState(
        new SignalState(
          await this._generateSenderKeyId(),
          0,
          await getRandomBytes(32),
          await generateSignKeyPair()
        )
      );

      await this._storage.storeRecord(address, record);
    }

    const state = record.getState();

    const message = createSenderKeyDistributionMessage();
    message.id = state.id;
    message.iteration = state.chain.iteration;
    message.chainKey = state.chain.key;
    message.signingKey = state.signatureKeyPair.pubKey;

    const encodedMessage = encodeSenderKeyDistributionMessage(message);

    return {
      distributionMessage: convertBytesToBinaryString(combineVersion(encodedMessage)),
      signingPrivateKey: convertBytesToBinaryString(state.signatureKeyPair.privKey),
    };
  }
  
  async createWithState(address: SignalAddress, bytes: Uint8Array): Promise<CreateSessionResult> {
    let record = await this._storage.loadRecord(address);

    if (record.isEmpty) {
      record = SignalRecord.deserialize(bytes);

      await this._storage.storeRecord(address, record);
    }

    const state = record.getState();

    const message = createSenderKeyDistributionMessage();
    message.id = state.id;
    message.iteration = state.chain.iteration;
    message.chainKey = state.chain.key;
    message.signingKey = state.signatureKeyPair.pubKey;

    const encodedMessage = encodeSenderKeyDistributionMessage(message);

    return {
      distributionMessage: convertBytesToBinaryString(combineVersion(encodedMessage)),
      signingPrivateKey: convertBytesToBinaryString(state.signatureKeyPair.privKey),
    };
  }

  async _generateSenderKeyId() {
    const randomBytes = await getRandomBytes(2);
    const senderKeyId = new Uint16Array(randomBytes)[0];
    return senderKeyId & 0x3fff;
  }
}
