export interface ISignalSenderDevice {
  senderId: string;
  senderDeviceId: string;
}
