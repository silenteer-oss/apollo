export class SignalAddress {
  get groupId(): string {
    return this._groupId;
  }

  get userId(): string {
    return this._userId;
  }

  get deviceId(): string {
    return this._deviceId;
  }

  constructor(
    private _groupId: string,
    private _userId: string,
    private _deviceId: string
  ) {}

  static fromString(encodedAddress: string): SignalAddress {
    if (
      typeof encodedAddress !== 'string' ||
      !encodedAddress.match(/.*\.\d+/)
    ) {
      throw new Error('Invalid SignalAddress string');
    }
    const parts = encodedAddress.split('.');
    return new SignalAddress(parts[0], parts[1], parts[2]);
  }

  toString(): string {
    return `${this._groupId}.${this._userId}.${this._deviceId}`;
  }

  equals(address: SignalAddress): boolean {
    if (!(address instanceof SignalAddress)) {
      return false;
    }

    return (
      address.groupId === this.groupId &&
      address.userId === this.userId &&
      address.deviceId === this.deviceId
    );
  }
}
