import { textsecure } from './signal-proto/sender-key-proto';

export interface ISenderKeyMessage {
  id: number;
  iteration: number;
  ciphertext: Uint8Array;
}

export interface ISenderKeyDistributionMessage {
  id: number;
  iteration: number;
  chainKey: Uint8Array;
  signingKey: Uint8Array;
}

export function encodeSenderKeyMessage(message: ISenderKeyMessage): Uint8Array {
  return textsecure.SenderKeyMessage.encode({
    id: message.id,
    iteration: message.iteration,
    ciphertext: message.ciphertext,
  }).finish();
}
export function decodeSenderKeyMessage(
  messageBuffer: Uint8Array
): ISenderKeyMessage {
  const senderKeyMessage = textsecure.SenderKeyMessage.decode(messageBuffer);
  return {
    id: senderKeyMessage.id,
    iteration: senderKeyMessage.iteration,
    ciphertext: senderKeyMessage.ciphertext,
  };
}
export function createSenderKeyMessage(): ISenderKeyMessage {
  return new textsecure.SenderKeyMessage();
}

// -------------------------------------------------------------- //

export function encodeSenderKeyDistributionMessage(
  message: ISenderKeyDistributionMessage
): Uint8Array {
  return textsecure.SenderKeyDistributionMessage.encode({
    id: message.id,
    iteration: message.iteration,
    chainKey: message.chainKey,
    signingKey: message.signingKey,
  }).finish();
}
export function decodeSenderKeyDistributionMessage(
  messageBuffer: Uint8Array
): ISenderKeyDistributionMessage {
  const senderKeyDistributionMessage = textsecure.SenderKeyDistributionMessage.decode(messageBuffer);
  return {
    id: senderKeyDistributionMessage.id,
    iteration: senderKeyDistributionMessage.iteration,
    chainKey: senderKeyDistributionMessage.chainKey,
    signingKey: senderKeyDistributionMessage.signingKey,
  };
}
export function createSenderKeyDistributionMessage(): ISenderKeyDistributionMessage {
  return new textsecure.SenderKeyDistributionMessage();
}
