import {
  combineVersion,
  combineBytes,
  convertBinaryStringToBytes,
  convertBytesToBinaryString,
  convertBytesToPlaintext,
  convertPlaintextToBytes
} from "./byte-util";
import { sign, verify, encrypt, decrypt } from "../crypto/crypto";
import {
  createSenderKeyMessage,
  encodeSenderKeyMessage,
  decodeSenderKeyMessage
} from "./proto-util";
import { SignalState } from "./signal-state";
import { ISignalMessageKey } from "./signal-message-key";
import { BaseSignalStorage } from "./signal-storage";
import { SignalAddress } from "./signal-address";

interface EncryptionPayload {
  address: SignalAddress;
  plaintext: string;
}

export interface EncryptionJob {
  encryptionPayload: EncryptionPayload;
  onDone: (result: string) => void;
  onError?: (e: Error) => void;
}

interface DecryptionPayload {
  address: SignalAddress;
  ciphertext: string;
}

export interface DecryptionJob {
  decryptionPayload: DecryptionPayload;
  onDone: (result: string) => void;
  onError?: (e: Error) => void;
}

export class QueueSignalCipher {
  private _encryptionQueue: Array<EncryptionJob> = [];
  private _encrypting = false;
  private _decryptionQueue: Array<DecryptionJob> = [];
  private _decrypting = false;
  constructor(private _storage: BaseSignalStorage) {}

  enqueueEncryptionJob = async (encryptionJob: EncryptionJob) => {
    if (!encryptionJob) return;
    this._encryptionQueue.push(encryptionJob);
    if (!this._encrypting) {
      this._encrypting = true;
      await this.processEncryption();
    }
  };

  processEncryption = async () => {
    if (this._encryptionQueue.length === 0) {
      this._encrypting = false;
      return;
    }

    const encryptionJob = this._encryptionQueue.shift();
    
    const {
      encryptionPayload: { address, plaintext },
      onDone, 
      onError
    } = encryptionJob;
    try {
      const record = await this._storage.loadRecord(address);
      const state = record.getState();

      const { iteration, cipherKey, iv } = await state.chain.getMessageKey();
      const plaintextBytes = convertPlaintextToBytes(plaintext);
      const ciphertext = await encrypt(iv, cipherKey, plaintextBytes);

      const message = createSenderKeyMessage();
      message.id = state.id;
      message.iteration = iteration;
      message.ciphertext = ciphertext;

      const versionEncodedMessage = combineVersion(
        encodeSenderKeyMessage(message)
      );

      const signature = await sign(
        versionEncodedMessage,
        state.signatureKeyPair.privKey
      );
      const result = combineBytes(versionEncodedMessage, signature);

      const nextChain = await state.chain.getNext();
      state.chain = nextChain;

      await this._storage.storeRecord(address, record);
      onDone && onDone(convertBytesToBinaryString(result));
    } catch (e) {
      onError && onError(e);
    }

    await this.processEncryption();
  };

  enqueueDecryptionJob = async (decryptionJob: DecryptionJob) => {
    if (!decryptionJob) return;
    this._decryptionQueue.push(decryptionJob);
    if (!this._decrypting) {
      this._decrypting = true;
      await this.processDecryption();
    }
  };
  
  processDecryption = async () => {
    if (this._decryptionQueue.length === 0) {
      this._decrypting = false;
      return;
    }
    
    const decryptionJob = this._decryptionQueue.shift();
    const {
      decryptionPayload: { address, ciphertext },
      onDone,
      onError
    } = decryptionJob;

    try {
      const ciphertextBytes = convertBinaryStringToBytes(ciphertext);
      const version = ciphertextBytes[0];
      if ((version & 0xf) > 3 || version >> 4 < 3) {
        // min version > 3 or max version < 3
        throw new Error("Incompatible version number on messageBuffer");
      }

      const decodedMessage = decodeSenderKeyMessage(
        ciphertextBytes.slice(1, ciphertextBytes.byteLength - 64)
      );

      const record = await this._storage.loadRecord(address);
      if (record.isEmpty) {
        throw new Error(`Sender key not found for ${address}`);
      }

      const state = record.getState(decodedMessage.id);

      await verify(
        ciphertextBytes.slice(0, ciphertextBytes.byteLength - 64),
        ciphertextBytes.slice(
          ciphertextBytes.byteLength - 64,
          ciphertextBytes.byteLength
        ),
        state.signatureKeyPair.pubKey
      );
      
      const { cipherKey, iv } = await this._getMessageKey(
        state,
        decodedMessage.iteration
      );
      
      const result = await decrypt(iv, cipherKey, decodedMessage.ciphertext);

      await this._storage.storeRecord(address, record);
      onDone && onDone(convertBytesToPlaintext(result));
    } catch (e) {
      onError && onError(e);
    }

    await this.processDecryption();
  };

  private async _getMessageKey(
    state: SignalState,
    iteration: number
  ): Promise<ISignalMessageKey> {
    let chain = state.chain;
    if (chain.iteration > iteration) {
      if (state.hasMessageKey(iteration)) {
        return await state.removeMessageKey(iteration);
      } else {
        throw new Error(
          `Receive old message, iteration ${chain.iteration} > ${iteration}`
        );
      }
    }

    if (iteration - chain.iteration > 2000) {
      throw new Error("Over 2000 messages into the future!");
    }

    while (chain.iteration < iteration) {
      const messageKey = await chain.getMessageKeyToStore();
      state.addMessageKey(messageKey);
      chain = await chain.getNext();
    }

    const nextChain = await chain.getNext();
    state.chain = nextChain;
    return await chain.getMessageKey();
  }
}
