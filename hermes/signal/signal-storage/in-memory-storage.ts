import { SignalAddress } from '../signal-address';
import { SignalRecord } from '../signal-record';
import { ISignalSenderDevice } from '../signal-sender-device';
import { BaseSignalStorage } from './base-storage';

export class SignalInMemoryStorage extends BaseSignalStorage {
  private _recordStorage: { [key: string]: SignalRecord } = {};
  private _recordSerializer: (address: string, bytes: Uint8Array) => Promise<void>;
  private _recordDeserializer: (address: string) => Promise<Uint8Array>;
  private _senderDeviceSerializer: (senderDevice: ISignalSenderDevice) => Promise<void>;
  private _senderDeviceDeserializer: () => Promise<ISignalSenderDevice>;

  async storeRecord(addess: SignalAddress, record: SignalRecord): Promise<void> {
    if (!this._recordSerializer) {
      throw new Error(
        'Missing record serializer which will make this store not durable'
      ); 
    }

    const addressString = addess.toString();
    this._recordStorage[addressString] = record;

    await this._recordSerializer(addressString, record.serialize());
  }

  async loadRecord(address: SignalAddress): Promise<SignalRecord> {
    if (!this._recordDeserializer) {
      throw new Error(
        'Missing record deserializer which will make this store not durable'
      );
    }

    const addressString = address.toString();
    let record = this._recordStorage[addressString];

    // Attempt to revert if no record match with address 
    if (record === undefined) {
      const serializedRecord = await this._recordDeserializer(addressString);
      // If revert successfully, add record into store
      if (serializedRecord) {
        record = SignalRecord.deserialize(serializedRecord);
        this._recordStorage[addressString] = record;
      }
    }

    if (record) {
      return record.clone();
    } else {
      return new SignalRecord();
    }
  }

  registerRecordDeserializer(deserializer: (address: string) => Promise<Uint8Array>) {
    this._recordDeserializer = deserializer;
    return this;
  }

  registerRecordSerializer(
    serializer: (address: string, bytes: Uint8Array) => Promise<void>
  ) {
    this._recordSerializer = serializer;
    return this;
  }

  // ----------------------------------------------------- //

  async storeSenderDevice(senderDevice: ISignalSenderDevice): Promise<void> {
    if (!this._senderDeviceSerializer) {
      throw new Error(
        'Missing sender device serializer which will make this store not durable'
      );
    }

    await this._senderDeviceSerializer(senderDevice);
  }

  async loadSenderDevice(): Promise<ISignalSenderDevice> {
    if (!this._senderDeviceDeserializer) {
      throw new Error(
        'Missing sender device deserializer which will make this store not durable'
      );
    }

    return await this._senderDeviceDeserializer();;
  }
  registerSenderDeviceDeserializer(deserializer: () => Promise<ISignalSenderDevice>) {
    this._senderDeviceDeserializer = deserializer;
    return this;
  }
  registerSenderDeviceSerializer(serializer: (senderDevice: ISignalSenderDevice) => Promise<void>) {
    this._senderDeviceSerializer = serializer;
    return this;
  }
}
