import { SignalAddress } from '../signal-address';
import { SignalRecord } from '../signal-record';
import { ISignalSenderDevice } from '../signal-sender-device';

export abstract class BaseSignalStorage {
  abstract async storeRecord(addess: SignalAddress, record: SignalRecord): Promise<void>;

  abstract async loadRecord(addess: SignalAddress): Promise<SignalRecord>;

  abstract registerRecordDeserializer(deserializer: (address: string) => Promise<Uint8Array>): this;

  abstract registerRecordSerializer(serializer: (address: string, bytes: Uint8Array) => Promise<void>): this;


  
  abstract async storeSenderDevice(senderDevice: ISignalSenderDevice): Promise<void>;

  abstract async loadSenderDevice(): Promise<ISignalSenderDevice>;

  abstract registerSenderDeviceDeserializer(deserializer: () => Promise<ISignalSenderDevice>): this;

  abstract registerSenderDeviceSerializer(serializer: (senderDevice: ISignalSenderDevice) => Promise<void>): this;
}
