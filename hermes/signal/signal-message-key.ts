import { hash } from '../crypto/crypto';
import { combineBytes, convertBinaryStringToBytes } from './byte-util';

export interface ISignalMessageKey {
  iteration: number;
  seed: Uint8Array;
  cipherKey: Uint8Array;
  iv: Uint8Array;
}

export async function create(
  iteration: number,
  seed: Uint8Array
): Promise<ISignalMessageKey> {
  const derivedKey = await HKDF(
    seed,
    new Uint8Array(32),
    convertBinaryStringToBytes('NoiGroupMsg')
  );

  return {
    iteration,
    seed,
    cipherKey: derivedKey.slice(24, 56),
    iv: derivedKey.slice(0, 24),
  };
}

async function HKDF(ikm: Uint8Array, salt: Uint8Array, info: Uint8Array) {
  // This is a short version of HKDF which only run 2 round of hashing because in this case we only use first 56 bytes
  const prk = await hash(salt, ikm);
  const infoArray = new Uint8Array(info.byteLength + 1 + 32);

  infoArray.set(info, 32);
  infoArray[infoArray.length - 1] = 1;

  const T1 = await hash(prk, infoArray);
  infoArray.set(T1);
  infoArray[infoArray.length - 1] = 2;

  const T2 = await hash(prk, infoArray);

  return combineBytes(T1, T2);
}
