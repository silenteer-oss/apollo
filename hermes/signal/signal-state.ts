import { ISignalMessageKey, create } from './signal-message-key';
import { SignalChain } from './signal-chain';
import { ISignKeyPair } from '../crypto/crypto-model';
import { textsecure } from './signal-proto/sender-key-proto';

const _MAX_MESSAGE_KEYS = 2000;

export class SignalState {
  private _messageKeys: textsecure.SenderKeyStateStructure.ISenderMessageKey[] = [];
  private _chain: SignalChain;

  get id(): number {
    return this._id;
  }

  get iteration(): number {
    return this._iteration;
  }

  set iteration(iteration) {
    this._iteration = iteration;
  }

  get signatureKeyPair(): ISignKeyPair {
    return this._signatureKeyPair;
  }

  get chain(): SignalChain {
    return this._chain;
  }

  set chain(chain: SignalChain) {
    this._chain = new SignalChain(chain.iteration, chain.key);
  }

  constructor(
    private _id: number,
    private _iteration: number,
    chainKey: Uint8Array,
    private _signatureKeyPair: ISignKeyPair
  ) {
    this._chain = new SignalChain(_iteration, chainKey);
  }

  hasMessageKey(iteration: number): boolean {
    return this._messageKeys.some(key => key.iteration === iteration);
  }

  addMessageKey(
    messageKey: textsecure.SenderKeyStateStructure.ISenderMessageKey
  ) {
    this._messageKeys.push(messageKey);
    if (this._messageKeys.length > _MAX_MESSAGE_KEYS) {
      this._messageKeys.unshift();
    }
  }

  removeMessageKey(iteration: number): Promise<ISignalMessageKey | undefined> {
    const indexToRemove = this._messageKeys.findIndex(
      key => key.iteration === iteration
    );

    if (indexToRemove <= -1) return Promise.resolve(undefined);

    const messageKey = this._messageKeys.splice(indexToRemove, 1)[0];
    return create(messageKey.iteration, messageKey.seed);
  }

  toProto(): textsecure.ISenderKeyStateStructure {
    return {
      senderKeyId: this.id,
      senderChainKey: {
        iteration: this.chain.iteration,
        seed: this.chain.key,
      },
      senderSigningKey: {
        public: this._signatureKeyPair.pubKey,
        private:
          this._signatureKeyPair.privKey &&
          this._signatureKeyPair.privKey,
      },
      senderMessageKeys: this._messageKeys,
    };
  }

  static create(
    stateStructure: textsecure.ISenderKeyStateStructure
  ): SignalState {
    const state = new SignalState(
      stateStructure.senderKeyId,
      stateStructure.senderChainKey.iteration,
      stateStructure.senderChainKey.seed,
      {
        pubKey: stateStructure.senderSigningKey.public,
        privKey: stateStructure.senderSigningKey.private,
      }
    );

    state._messageKeys = stateStructure.senderMessageKeys;

    return state;
  }
}
