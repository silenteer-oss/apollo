export { convertBinaryStringToBytes, convertBytesToBinaryString } from '../crypto/byte-string';

export function convertBytesToPlaintext(
  bytes: Uint8Array
): string {
  const decoder = new TextDecoder();
  return decoder.decode(bytes);
}

export function convertPlaintextToBytes(plaintext: string): Uint8Array {
  const encoder = new TextEncoder();
  return encoder.encode(plaintext);
}

export function combineBytes(
  bytes1: Uint8Array,
  bytes2: Uint8Array
): Uint8Array {
  const result = new Uint8Array(bytes1.byteLength + bytes2.byteLength);
  result.set(bytes1, 0);
  result.set(bytes2, bytes1.byteLength);
  return result;
}

export function combineVersion(bytes: Uint8Array): Uint8Array {
  const result = new Uint8Array(bytes.byteLength + 1);
  result[0] = (3 << 4) | 3;
  result.set(bytes, 1);
  return result;
}
