import { SignalState } from './signal-state';
import { textsecure } from './signal-proto/sender-key-proto';

const _MAX_STATES = 5;

export class SignalRecord {
  private _states: SignalState[] = [];

  get isEmpty(): boolean {
    return this._states.length === 0;
  }

  getState(id?: number): SignalState {
    const states =
      id === undefined
        ? this._states
        : this._states.filter(state => state.id === id);

    if (states) {
      return states[0];
    } else {
      throw new Error('Key state not found in record.');
    }
  }

  addState(state: SignalState) {
    this._states.unshift(state);
    if (this._states.length > _MAX_STATES) {
      this._states.pop();
    }
  }

  clone(): SignalRecord {
    const clone = new SignalRecord();
    this._states.forEach(state => clone.addState(state));
    return clone;
  }

  serialize(): Uint8Array {
    return textsecure.SenderKeyRecordStructure.encode({
      senderKeyStates: this._states.map(s => s.toProto()),
    }).finish();
  }

  static deserialize(bytes: Uint8Array): SignalRecord {
    const recordStructure = textsecure.SenderKeyRecordStructure.decode(bytes);

    const record = new SignalRecord();
    record._states = recordStructure.senderKeyStates.map(s =>
      SignalState.create(s)
    );

    return record;
  }

  serializeItem(index: number): Uint8Array {
    if (!this._states || (index + 1) > this._states.length) return;
    this._states[index].iteration = 0;
    return textsecure.SenderKeyRecordStructure.encode({
      senderKeyStates: [this._states[index].toProto()],
    }).finish();
  }
}
