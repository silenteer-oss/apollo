const { declare } = require('@babel/helper-plugin-utils')
const { types: { importDeclaration, exportNamedDeclaration, exportAllDeclaration, stringLiteral } } = require('@babel/core')
const { existsSync, lstatSync } = require('fs')
const { resolve, extname, dirname } = require('path')

const isNodeModule = module => {
  try {
    require.resolve(module)
    return true
  } catch (e) {
    if (e.code === 'MODULE_NOT_FOUND') {
      return false
    }
    console.error(e)
  }
}

const skipModule = (module, { replace, extension }) => {
  // const relativePathOnly = module.startsWith(".") || module.startsWith("/")
  const isANodeModule = isNodeModule(module)
  const alreadyHasExtension = module.endsWith(extension);

  return isANodeModule || alreadyHasExtension
}

function buildImportPath(module, filename, extension = 'js') {
  const ext = extname(module)

  // Comes without extension, check if it's a file or a module (directory)
  const dirPath = resolve(dirname(filename), module)

  const filePath = `${dirPath}.${extension}`
  const fileExist = existsSync(filePath)

  if (fileExist) {
    return `${module}.${extension}`
  } else if (existsSync(dirPath) && lstatSync(dirPath).isDirectory()) {
    return `${module}/index.${extension}`
  }

  console.log('Ignoring', module)
  return undefined
}

const makeDeclaration =
  ({ declaration, args, replace = false, extension = 'js' }) =>
    (path, { file: { opts: { filename } } }) => {
      const { node: { source } } = path

      const shouldSkip = (!source || skipModule(source && source.value, { extension }))
      // const shouldSkip = !source
      if (shouldSkip) {
        return
      }

      const { value: module } = source
      const pathLiteral = buildImportPath(module, filename, extension)
      pathLiteral && path.replaceWith(
        declaration(
          ...args(path),
          stringLiteral(pathLiteral)
        )
      )
    }

module.exports = declare((api, options) => {
  api.assertVersion(7)
  return {
    name: 'add-import-extension',
    visitor: {
      ImportDeclaration: makeDeclaration({
        ...options,
        declaration: importDeclaration,
        args: ({ node: { specifiers } }) => [specifiers]
      }),
      ExportNamedDeclaration: makeDeclaration({
        ...options,
        declaration: exportNamedDeclaration,
        args: ({ node: { declaration, specifiers } }) => [declaration, specifiers]
      }),
      ExportAllDeclaration: makeDeclaration({
        ...options,
        declaration: exportAllDeclaration,
        args: () => []
      }),
      CallExpression: function (path, { file: { opts: { filename } } }) {
        if (path.node.callee.type === "Import"
          && path.node.arguments
          && path.node.arguments[0]
          && path.node.arguments[0].type === 'StringLiteral'
        ) {
          const importPath = path.node.arguments[0].value;

          const extension = extname(importPath)

          if (!extension.endsWith('.js')) {
            path.node.arguments[0].value = importPath + '.js'
          }
        }
      }
    }
  }
})
