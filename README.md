### HOW TO RUN
- `npm ` and all sub dirs
- `npm run prepare` in main
- `npm run build` in main
- `npm run serve` in main. Access localhost:4000. `/api/` will be served by zoop staging

OR

- `npm ` and all sub dirs
- and `npm run demo` to do all of those above step

### SHIT
- LoadMicroModule view simplified to load module file only
- Micromodule registration changed
- Use function to wrap lazyComponent
- Micromodule fail to load, need to addres that
- Path in webworker

### Proxy approach
- Path still need to be resolved to real path as esm doesn't accept path without "./"
- Proxy doesn't catch redirect
- Redirect is slow
- Forward has a problem of the context path when requiring component (like theme --> theme/index.js)

- Separate between dev build and production build. Dev build {tsc only}, Production build {tsc -> babel}