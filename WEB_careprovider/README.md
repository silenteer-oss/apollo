Guide to start examples

Run `./init.sh` to install node modules for all packages
Cd to `src/modules/product-list` and run `npm run start:prod`
Cd to `src/modules/product-detail` and run `npm run start:prod`
Cd to `src/modules/patient-management` and run `npm run start:prod`
Cd to `src/application` and run `npm run start:dev`

Note that:
Because project is strucuted by following multi-packages in one repo approach, so to avoid lint issues from editor extension/intelligence, you should open `careprovider.code-workspace` to work.
