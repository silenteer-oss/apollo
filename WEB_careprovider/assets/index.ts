const assetURL = import.meta['url']

export default function getPath(image: string): string {
  return new URL(image, assetURL).pathname
}