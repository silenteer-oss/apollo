import React from 'react';
import type { IButtonProps } from '@blueprintjs/core/src/components/button/abstractButton';

export const DEFAUT_DURATION_PROGRESS = 1000;

export interface IProgressButtonProps
  extends Omit<IButtonProps, 'onClick' | 'loading'> {
  duration?: number;
  onClick?: (event: React.MouseEvent<HTMLElement>) => Promise<void>;
}

export interface IProgressButtonState {
  isProgressing: boolean;
}
