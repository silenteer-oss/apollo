import React from 'react';
import { DISPLAYNAME_PREFIX } from '@blueprintjs/core/src/common';
import { Button } from '@blueprintjs/core/src/components/button/buttons';
import {
  DEFAUT_DURATION_PROGRESS,
  IProgressButtonProps,
  IProgressButtonState,
} from './models';

export class ProgressButton extends React.PureComponent<
  React.ButtonHTMLAttributes<HTMLButtonElement> & IProgressButtonProps,
  IProgressButtonState
> {
  static displayName = `${DISPLAYNAME_PREFIX}.ProgressButton`;

  private _timeout: any;

  state: IProgressButtonState = {
    isProgressing: false,
  };

  componentWillUnmount() {
    clearTimeout(this._timeout);
  }

  render() {
    const { onClick, duration, ...rest } = this.props;
    const { isProgressing } = this.state;

    return (
      <Button {...rest} loading={isProgressing} onClick={this._handleClick} />
    );
  }

  private _handleClick = (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ): void => {
    const { onClick, duration = DEFAUT_DURATION_PROGRESS } = this.props;
    const { isProgressing } = this.state;

    if (!onClick || isProgressing) {
      return;
    }

    this.setState({ isProgressing: true }, () => {
      const _startTime = Date.now();

      onClick(event).then(() => {
        const _endTime = Date.now();

        if (_endTime - _startTime >= duration) {
          this.setState({ isProgressing: false });
        } else {
          this._timeout = setTimeout(() => {
            this.setState({ isProgressing: false });
          }, duration - (_endTime - _startTime));
        }
      });
    });
  };
}
