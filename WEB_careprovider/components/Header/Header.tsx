import * as React from 'react';
import { Flex } from '@design-system/core/components';
import { IHeaderProps } from './HeaderModel';

export class OriginHeader extends React.PureComponent<IHeaderProps> {
  render() {
    const { className } = this.props;

    return (
      <Flex auto align="center" className={className}>
        {this.props.children}
      </Flex>
    );
  }
}
