// TODO: Need to fix import styled after restructure theme
import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { scaleSpacePx } from '@design-system/core/styles';
import { OriginHeader } from './Header';
import { IHeaderProps } from './HeaderModel';

export const Header = styled(OriginHeader).attrs(({ className }) => ({
  className: getCssClass('sl-Header', className),
}))<IHeaderProps>`
  ${props => {
    const { space } = props.theme;

    return `
      & {
        min-height: ${scaleSpacePx(20)};
        max-height: ${scaleSpacePx(20)};
        padding: 0 ${space.m}

        > * {
          width: 100%;
        }
      }
    `;
  }}
`;
