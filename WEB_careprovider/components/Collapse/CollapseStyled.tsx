// TODO: Need to fix import styled after restructure theme
import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
// import { scaleSpacePx } from '@design-system/core/styles';
import { OriginCollapse } from './Collapse';
import { ICollapseProps } from './CollapseModel';

export const Collapse = styled(OriginCollapse).attrs(({ className }) => ({
  className: getCssClass('sl-Collapse', className),
}))<ICollapseProps>`
  ${props => {
    const { space } = props.theme;

    return `
      & {
        .sl-collapse-header {
          justify-content: space-between;
          align-items: center;
          padding: ${space.xs} ${space.m};
          &:hover {
            cursor: pointer;
          }
        }

        img {
          width: 16px;
          height: 16px;
        }
      }
    `;
  }}
`;
