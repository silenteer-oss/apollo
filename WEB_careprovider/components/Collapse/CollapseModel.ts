export interface ICollapseProps {
  className?: string;
  title?: string;
  isOpen?: boolean;
}

export interface ICollapseState {
  isExpand: boolean;
}
