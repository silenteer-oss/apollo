import * as React from 'react';
import { Flex, H4 } from '@design-system/core/components';
import { Collapse } from '@blueprintjs/core';
import { ICollapseProps, ICollapseState } from './CollapseModel';

const ArrowUpIcon = new URL('./arrow-up.png', import.meta['url']).pathname
const ArrowDownIcon = new URL('./arrow-down.png', import.meta['url']).pathname

export class OriginCollapse extends React.PureComponent<
  ICollapseProps,
  ICollapseState
  > {
  state: ICollapseState = {
    isExpand: false,
  };

  render() {
    const { className, title, children } = this.props;
    const { isExpand } = this.state;

    const toggleView = () => {
      this.setState({ isExpand: !isExpand });
    };

    return (
      <Flex auto column className={className}>
        <Flex className="sl-collapse-header" onClick={toggleView}>
          <H4 padding={0}>{title}</H4>
          <img src={isExpand ? ArrowUpIcon : ArrowDownIcon} />
        </Flex>
        <Collapse isOpen={isExpand} keepChildrenMounted>
          {children}
        </Collapse>
      </Flex>
    );
  }
}
