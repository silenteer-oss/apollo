import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Flex, lazyComponent } from '@design-system/core/components';
import { PATIENT_PAGE_PATH } from '@careprovider/route';
import { IAppRoutePageProps } from '@careprovider/application/src/pages/AppRoute';

const LazyPatientPage = lazyComponent(import('../Patient/PatientPage.styled'));

export class OriginAppRoutePage extends React.PureComponent<
  IAppRoutePageProps
> {
  render() {
    const { className } = this.props;

    return (
      <Flex className={className}>
        <Switch>
          <Route
            exact
            path={PATIENT_PAGE_PATH}
            component={this.renderPatientManagementPage}
          />
          <Redirect exact path="/app" to={PATIENT_PAGE_PATH} />
          {/* Removes trailing slashes */}
          <Route
            path="/:url*(/+)"
            exact
            strict
            render={({ location }) => (
              <Redirect to={location.pathname.replace(/\/+$/, '')} />
            )}
          />
          {/* Removes duplicate slashes in the middle of the URL */}
          <Route
            path="/:url(.*//+.*)"
            exact
            strict
            render={({ match }) => (
              <Redirect to={`/${match.params.url.replace(/\/\/+/, '/')}`} />
            )}
          />
        </Switch>
      </Flex>
    );
  }

  renderPatientManagementPage = () => {
    return <LazyPatientPage />;
  };
}
