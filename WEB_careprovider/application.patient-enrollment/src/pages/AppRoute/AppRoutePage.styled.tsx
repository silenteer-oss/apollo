import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IAppRoutePageProps } from './models';
import { OriginAppRoutePage } from './AppRoutePage';

export const AppRoutePage = styled(OriginAppRoutePage).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppRoutePage', className),
  })
)<IAppRoutePageProps>`
  ${props => {
    return `
      & {
        width: 100%;
        height: 100%;
      }
    `;
  }}
`;
