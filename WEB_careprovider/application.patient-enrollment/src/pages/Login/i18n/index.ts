import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './login-page.patient-enrollment.json';

export const NAMESPACE_LOGIN_PAGE = 'login-page.patient-enrollment';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
