import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { IPatientPageProps } from './models';
import { MicroPatientListView } from '../../micro-modules';
import { IGlobalContext, withGlobalContext } from '@careprovider/context';

class OriginPatientPage extends React.PureComponent<
  IPatientPageProps & IGlobalContext & RouteComponentProps<any>
> {
  render() {
    const { className } = this.props;

    return (
      <div className={className}>
        <MicroPatientListView onUpdatedPatient={this._updatePatientProfile} />
      </div>
    );
  }

  private _updatePatientProfile = (id: string): void => {
    this.props.updatePatientProfiles([id]);
  };
}

export const PatientPage = withRouter(withGlobalContext(OriginPatientPage));
