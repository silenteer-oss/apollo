import { OriginRootPage } from './RootPage';
import { withStyledRootPage } from '@careprovider/application/src/pages/Root';

export const RootPage = withStyledRootPage(OriginRootPage);
