import { lazyComponent } from '@design-system/core/components';
import { withRootPage } from '@careprovider/application/src/pages/Root';
import { AppRoutePage } from '../AppRoute';
import { LoginPage } from '../Login';
import { AuthorizedRoute } from './components';

const TrustedDevicePage = lazyComponent(
  import('../TrustedDevice/TrustedDevice.styled')
);

export const OriginRootPage = withRootPage({
  AuthorizedRoute,
  AppRoutePage,
  LoginPage,
  TrustedDevicePage,
});
