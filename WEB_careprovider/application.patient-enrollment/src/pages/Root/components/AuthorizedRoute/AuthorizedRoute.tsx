import { getCareProviderKeyPairFromLocalStorage } from '@careprovider/services/client-database';
import { withAuthorizedRoute } from '@careprovider/application/src/pages/Root/components/AuthorizedRoute';

export const AuthorizedRoute = withAuthorizedRoute({
  getCareProviderKeyPair: getCareProviderKeyPairFromLocalStorage,
});
