import TrustedDevicePage from './TrustedDevice.styled';

export * from './models';
export { TrustedDevicePage };
