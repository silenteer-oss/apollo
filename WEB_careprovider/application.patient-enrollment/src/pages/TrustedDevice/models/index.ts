import { ICareProviderTheme } from '@careprovider/theme';

export interface ITrustedDevicePageProps {
  className?: string;
  theme?: ICareProviderTheme;
}
