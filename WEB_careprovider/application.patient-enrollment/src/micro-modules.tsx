import React from 'react';
import { loadMicroModuleView } from '@design-system/core/components';
import { withTheme } from '@careprovider/theme';
import { IPatientListProps } from '@careprovider/modules.patient-enrollment/patient-management';
import { PATIENT_LIST_VIEW_ID } from '@careprovider/modules.patient-enrollment/patient-management/module-config';
import { ITrustedDeviceViewProps } from '@careprovider/modules.patient-enrollment/trusted-device';
import { TRUSTED_DEVICE_VIEW_ID } from '@careprovider/modules.patient-enrollment/trusted-device/module-config';
import { getApplicationConfig } from '../application-config';

const {
  modules: { PatientManagementModule, TrustedDeviceModule },
} = getApplicationConfig();

export const MicroPatientListView = withTheme<
  React.FunctionComponent<IPatientListProps>
>(props => {
  const View = loadMicroModuleView<IPatientListProps>({
    url: PatientManagementModule.publicAssetPath,
    module: PatientManagementModule.id,
    view: PatientManagementModule.views[PATIENT_LIST_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//

export const MicroTrustedDeviceView = withTheme<
  React.FunctionComponent<ITrustedDeviceViewProps>
>(props => {
  const View = loadMicroModuleView<ITrustedDeviceViewProps>({
    url: TrustedDeviceModule.publicAssetPath,
    module: TrustedDeviceModule.id,
    view: TrustedDeviceModule.views[TRUSTED_DEVICE_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//
