import {
  IApplicationConfig,
  IModuleConfig,
} from '../../../LIB_design-system/infrastructure/configs/environment';
import {
  PATIENT_MANAGEMENT_MODULE_ID,
  IModuleViews as IPatientManagementModuleViews,
  getModuleConfig as getPatientManagementModuleConfig,
} from '../modules/patient-management/module-config';
import {
  TRUSTED_DEVICE_MODULE_ID,
  IModuleViews as ITrustedDeviceModuleViews,
  getModuleConfig as getTrustedDeviceModuleConfig,
} from '../modules/trusted-device/module-config';

export interface IApplicationModules {
  [PATIENT_MANAGEMENT_MODULE_ID]: IModuleConfig<IPatientManagementModuleViews>;
  [TRUSTED_DEVICE_MODULE_ID]: IModuleConfig<ITrustedDeviceModuleViews>;
}

export function getApplicationConfig(): IApplicationConfig<
  IApplicationModules
> {
  return {
    id: 'Patient Enrollment Application',
    name: 'Patient Enrollment Application',
    host: '0.0.0.0',
    port: 12345,
    publicAssetPath: `/`,
    modules: {
      PatientManagementModule: getPatientManagementModuleConfig(),
      TrustedDeviceModule: getTrustedDeviceModuleConfig(),
    },
  };
}
