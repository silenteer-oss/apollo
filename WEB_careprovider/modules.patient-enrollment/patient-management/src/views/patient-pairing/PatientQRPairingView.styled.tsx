import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IPatientQrScanningProps } from './models';
import OriginPatientQRPairingView from './PatientQRPairingView';
import { scaleSpacePx } from '@design-system/core/styles';

export const PatientQRPairingView = styled(OriginPatientQRPairingView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-PatientPairingView', className),
  })
)<IPatientQrScanningProps>`
  ${() => {
    return `
    & {
      display: flex;
      flex-direction: column;
      align-items: center;


      .description-text {
        padding-top: ${scaleSpacePx(10)};
      }

      .container {
        flex-direction: column;
        flex: auto;
        justify-content: center;
        align-items: center;

        .loading-text {
          padding-top: ${scaleSpacePx(15)};
          padding-bottom: ${scaleSpacePx(10)};
        }

        .loading {
          width: 400px;
          min-height: 50px;
          align-items: center;
          justify-content: center;
        }

        .action-container {
          padding: ${scaleSpacePx(4)};

          .scan-error-button {
            margin: ${scaleSpacePx(2)}
          }
        }
      }
    }`;
  }}
`;
