import { ICareProviderTheme } from 'theme';
import { IProfile } from '../../../services';

export * from '@careprovider/modules/patient-management/views/patient-pairing/PatientPairingModel';

export interface IPatientQrScanningProps {
  className?: string;
  theme?: ICareProviderTheme;
  patient: IProfile;
  onCallbackPairing: () => void;
  openPairingTab: () => void;
  closePairingDialog: () => void;
}

export interface IPatientQrScanningState {
  pairing: boolean;
  errorMessage: string;
}
