import React from 'react';
import {
  withI18nContext,
  II18nFixedNamespaceContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_QR_PAIRING_VIEW, qrViewI18nKeys } from './i18n';
import { IPatientQrScanningState, IPatientQrScanningProps } from './models';
import QrReader from 'react-qr-reader';
import { PatientPairingService } from './services';
import { getCareProviderKeyPairFromLocalStorage } from '@careprovider/services/client-database';
import { Toaster, Button, Intent } from '@blueprintjs/core';
import { UNKNOWN_ERROR } from 'common/constants';
import { LoadingState, Flex, BodyTextL, Svg } from 'core/components';
import SadIcon from '@careprovider/assets/images/red-sad-icon.svg';

class OriginalPatientQrScanning extends React.PureComponent<
  IPatientQrScanningProps & II18nFixedNamespaceContext,
  IPatientQrScanningState
> {
  state = { pairing: false, errorMessage: undefined };
  toaster: Toaster;

  render() {
    const { className } = this.props;
    const { errorMessage } = this.state;
    return (
      <div className={className}>
        {!errorMessage ? this.renderContent() : this.renderError()}
        <Toaster ref={ref => (this.toaster = ref)} />
      </div>
    );
  }

  renderContent = () => {
    const { pairing } = this.state;
    const { t } = this.props;
    return pairing ? (
      this.renderLoading()
    ) : (
      <>
        <BodyTextL className="description-text">
          {t(qrViewI18nKeys.scanDescription)}
        </BodyTextL>
        <QrReader
          delay={500}
          onError={data => {
            console.log('error' + data);
          }}
          onScan={this.onTrustPatientDevice}
          style={{ width: 500, margin: 40 }}
        />
      </>
    );
  };

  renderError = () => {
    const { t, closePairingDialog, openPairingTab } = this.props;
    return (
      <Flex className="container">
        <Svg src={SadIcon} width={40} height={40} />
        <BodyTextL>{t(qrViewI18nKeys.scanningError)}</BodyTextL>
        <Flex className="action-container">
          <Button className="scan-error-button" onClick={openPairingTab}>
            {t(qrViewI18nKeys.enterPairingCode)}
          </Button>
          <Button
            className="scan-error-button"
            intent={Intent.PRIMARY}
            onClick={() =>
              this.setState({
                pairing: false,
                errorMessage: undefined,
              })
            }
          >
            {t(qrViewI18nKeys.scanAgain)}
          </Button>
        </Flex>
        <Button intent={Intent.PRIMARY} minimal onClick={closePairingDialog}>
          {t(qrViewI18nKeys.scanLater)}
        </Button>
      </Flex>
    );
  };

  renderLoading = () => {
    const { t } = this.props;
    return (
      <Flex className="container">
        <BodyTextL className="loading-text">
          {t(qrViewI18nKeys.scanning)}
        </BodyTextL>
        <Flex className="loading">
          <LoadingState size={40} />
        </Flex>
      </Flex>
    );
  };

  onTrustPatientDevice = value => {
    if (!value) return;

    this.setState({ pairing: true });
    const { patient, onCallbackPairing } = this.props;

    const patientDeviceCode = value.trim().toUpperCase();
    PatientPairingService.onPairing(
      patient.id,
      patient.phone,
      patientDeviceCode,
      patient.conversationId,
      getCareProviderKeyPairFromLocalStorage,
      error => {
        console.log(error);
        if (error) {
          this.setState({ pairing: false });
          this.toaster.show(UNKNOWN_ERROR);
        } else {
          onCallbackPairing && onCallbackPairing();
        }
      }
    );
  };
}

export default withTheme(
  withI18nContext(OriginalPatientQrScanning, {
    namespace: NAMESPACE_QR_PAIRING_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
