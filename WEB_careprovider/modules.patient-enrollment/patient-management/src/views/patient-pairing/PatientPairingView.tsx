import { withI18nContext } from '@design-system/i18n/context';
import { getCareProviderKeyPairFromLocalStorage } from '@careprovider/services/client-database';
import { withPatientPairingView } from '@careprovider/modules/patient-management/src/views/patient-pairing';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_PATIENT_PAIRING_VIEW } from './i18n';

export default withTheme(
  withI18nContext(
    withPatientPairingView({
      getCareProviderKeyPair: getCareProviderKeyPairFromLocalStorage,
    }),
    {
      namespace: NAMESPACE_PATIENT_PAIRING_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    }
  )
);
