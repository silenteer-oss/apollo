import i18nSource from './patient-qr-pairing-view.patient-enrollment.json';
import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';

export * from '@careprovider/modules/patient-management/views/patient-pairing/i18n';
export const NAMESPACE_QR_PAIRING_VIEW =
  'patient-qr-pairing-view.patient-enrollment';

export const NAMESPACE_PATIENT_PAIRING_VIEW =
  'patient-pairing-view.patient-enrollment';

export const qrViewI18nKeys = getPropertyAsKeyMap(i18nSource);
