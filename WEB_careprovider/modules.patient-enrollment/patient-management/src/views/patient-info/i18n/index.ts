import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './patient-info-view.patient-enrollment.json';

export const NAMESPACE_PATIENT_INFO_VIEW =
  'patient-info-view.patient-enrollment';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
