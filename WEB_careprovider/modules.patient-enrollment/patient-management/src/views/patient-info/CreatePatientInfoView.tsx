import { withI18nContext } from '@design-system/i18n/context';
import { withCreatePatientInfoView } from '@careprovider/modules/patient-management/src/views/patient-info';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_PATIENT_INFO_VIEW } from './i18n';

export const CreatePatientInfoView = withTheme(
  withI18nContext(withCreatePatientInfoView(), {
    namespace: NAMESPACE_PATIENT_INFO_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
