import React from 'react';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import {
  GlobalContextProvider,
  GlobalContextConsumer,
} from '@careprovider/context';
import { getApplicationInitial } from '@careprovider/services/biz';
import {
  ThemeProvider,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
} from '@careprovider/theme';
import { IRootViewProps, IRootViewState } from './models';
import { PatientListView } from '../patient-list';

export class OriginRootView extends React.PureComponent<
  IRootViewProps,
  IRootViewState
> {
  state: IRootViewState = {};

  componentDidMount() {
    getApplicationInitial().then(appInitial => {
      this.setState({ appInitial });
    });
  }

  render() {
    const { className, theme } = this.props;
    const { appInitial, isInitedI18n } = this.state;

    return (
      <div className={className}>
        <I18nContextProvider
          {...getI18nInitOptions()}
          onInitedI18n={this._handleInitedI18n}
        >
          <ThemeProvider theme={theme}>
            <GlobalStyleContextProvider
              styles={{
                main: mixGlobalStyle(theme),
                blueprint: mixCustomBlueprintStyle(theme),
              }}
            >
              {appInitial && isInitedI18n && (
                <GlobalContextProvider userProfile={appInitial.userProfile}>
                  <GlobalContextConsumer>
                    {context => <PatientListView />}
                  </GlobalContextConsumer>
                </GlobalContextProvider>
              )}
            </GlobalStyleContextProvider>
          </ThemeProvider>
        </I18nContextProvider>
      </div>
    );
  }

  private _handleInitedI18n = () => {
    this.setState({ isInitedI18n: true });
  };
}
