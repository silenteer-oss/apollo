import { ICareProviderTheme } from '@careprovider/theme';

export function mixGlobalStyle(theme: ICareProviderTheme): string {
  return `
      .bp3-dialog {
        .pairing-tabs {
          align-items: center;
          justify-content: center;
          display: flex;
          flex-direction: column;
          flex: auto;
        }
      }
  `;
}
