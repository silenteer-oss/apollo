import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  Button,
  Classes,
  Dialog,
  HTMLTable,
  Intent,
  Alert,
  Tabs,
  Tab,
} from '@blueprintjs/core';
import {
  Flex,
  H1,
  H2,
  BodyTextM,
  LoadingState,
} from '@design-system/core/components';
import {
  isEmpty,
  getCssClass,
  doPromiseInDuration,
} from '@design-system/infrastructure/utils';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { Header } from '@careprovider/components';
import { dateTimeFormat } from '@careprovider/services/util';
import { getModuleConfig } from '../../../module-config';
import {
  IGender,
  IPatientCreateRequest,
  IProfile,
  IPatientUpdateRequest,
  PatientService,
} from '../../services';
import {
  ICreatePatientInfoProps,
  ICreatePatientInfoState,
  CreatePatientInfoView,
} from '../patient-info';
import {
  PatientPairingView,
  PatientPairingService,
  PatientQRPairingView,
} from '../patient-pairing';
import { NAMESPACE_PATIENT_LIST_VIEW, i18nKeys } from './i18n';
import { IPatientListProps, IPatientListState } from './models';
import { mixGlobalStyle } from './styles/mix-global-style';
import { withGlobalStyleContext, IGlobalStyleContext } from 'themes/context';

class OriginPatientListView extends React.PureComponent<
  IPatientListProps &
    II18nFixedNamespaceContext &
    RouteComponentProps &
    IGlobalStyleContext,
  IPatientListState
> {
  addPatientInfoDialog: any;
  pairingDialog: any;
  state = {
    isReady: false,
    isOpenPatientInfoDialog: false,
    isOpenPatientPairingDialog: false,
    isOpenPatientQRPairingDialog: false,
    patientList: [] as IProfile[],
    selectedPatient: undefined,
    offset: 0,
    openDeletionDialog: false,
    searchPhrase: '',
    isLoading: false,
    isOpenCloseEditPatientInfoAlert: false,
    selectedScanTabId: 'qr-tab',
  };
  editPatientInfoRef: any;
  setEditPatientInfoRef = (
    ref: React.PureComponent<
      ICreatePatientInfoProps & II18nFixedNamespaceContext,
      ICreatePatientInfoState
    >
  ) => {
    this.editPatientInfoRef = ref;
  };

  componentDidMount() {
    this.loadPatientList();
    this.props.addGlobalStyle('tabDialog', mixGlobalStyle(this.props.theme));
  }

  render() {
    const { theme, t } = this.props;
    const {
      isOpenPatientInfoDialog,
      isOpenPatientPairingDialog,
      isOpenPatientQRPairingDialog,
      selectedPatient,
      selectedScanTabId,
    } = this.state;

    return (
      <>
        {this.renderPatientView()}
        <Dialog
          className={getCssClass(
            'bp3-dialog-fullscreen',
            'bp3-dialog-content-scrollable'
          )}
          title={t(i18nKeys.createPatientDialogTitle)}
          onClose={this.closeAddPatientInfoDialog}
          isOpen={isOpenPatientInfoDialog && !selectedPatient}
          canOutsideClickClose={false}
        >
          <div className={Classes.DIALOG_BODY}>
            <div style={{ margin: 'auto' }}>
              <CreatePatientInfoView
                theme={theme}
                onAddPatient={this.onCreatePatient}
              />
            </div>
          </div>
        </Dialog>
        <Dialog
          className={getCssClass(
            'bp3-dialog-fullscreen',
            'bp3-dialog-content-scrollable'
          )}
          title={t(i18nKeys.updatePatientDialogTitle)}
          onClose={this.closeAddPatientInfoDialog}
          isOpen={isOpenPatientInfoDialog && selectedPatient}
          canOutsideClickClose={false}
        >
          <div className={Classes.DIALOG_BODY}>
            <div style={{ margin: 'auto' }}>
              <CreatePatientInfoView
                theme={theme}
                selectedPatient={selectedPatient}
                onEditPatient={this.onEditPatient}
                getInstance={this.setEditPatientInfoRef}
              />
            </div>
          </div>
        </Dialog>
        <Dialog
          className={getCssClass(
            'bp3-dialog-fullscreen',
            'bp3-dialog-content-scrollable'
          )}
          title={t(i18nKeys.pairingDialogTitle)}
          onClose={this.closePairingDialog}
          isOpen={isOpenPatientPairingDialog}
          canOutsideClickClose={false}
        >
          <div className={Classes.DIALOG_BODY}>
            <div style={{ margin: 'auto' }}>
              <PatientPairingView
                patient={selectedPatient}
                onSubmit={this.onCallbackPairing}
                theme={theme}
              />
            </div>
          </div>
        </Dialog>
        <Dialog
          className={getCssClass(
            'bp3-dialog-fullscreen',
            'bp3-dialog-content-scrollable'
          )}
          title={t(i18nKeys.pairingQRDialogTitle)}
          onClose={this.closeQRPairingDialog}
          isOpen={isOpenPatientQRPairingDialog}
          canOutsideClickClose={false}
        >
          <div className={Classes.DIALOG_BODY}>
            <Tabs
              className="pairing-tabs"
              onChange={newTabId => {
                this.setState({ selectedScanTabId: newTabId.toString() });
              }}
              selectedTabId={selectedScanTabId}
            >
              <Tab
                id="qr-tab"
                title={t(i18nKeys.scanQRTabName)}
                className="scan-tab"
                panel={
                  <PatientQRPairingView
                    patient={selectedPatient}
                    onCallbackPairing={this.onCallbackPairing}
                    openPairingTab={this.openPairingTab}
                    closePairingDialog={this.closeQRPairingDialog}
                  />
                }
              />
              <Tab
                id="pairing-tab"
                title={t(i18nKeys.pairingTabName)}
                panel={
                  <PatientPairingView
                    patient={selectedPatient}
                    onSubmit={this.onCallbackPairing}
                    theme={theme}
                  />
                }
              />
            </Tabs>
          </div>
        </Dialog>
        <Alert
          cancelButtonText={t(i18nKeys.cancelEditingPatientDialogCancelButton)}
          confirmButtonText={t(
            i18nKeys.cancelEditingPatientDialogConfirmButton
          )}
          intent={Intent.DANGER}
          isOpen={this.state.isOpenCloseEditPatientInfoAlert}
          onConfirm={this.quitEditingPatientInfo}
          onCancel={this.backToEditingPatientInfo}
        >
          <H2 padding="0 0 12px 0">
            {t(i18nKeys.cancelEditingPatientDialogTitle)}
          </H2>
          <p>{t(i18nKeys.cancelEditingPatientDialogMessage)}</p>
        </Alert>
      </>
    );
  }

  openPairingTab = () =>
    this.setState({
      selectedScanTabId: 'pairing-tab',
    });

  renderPatientView = () => {
    const { className, t } = this.props;
    const { patientList = [], isLoading } = this.state;

    return (
      <Flex auto column className={className}>
        <Header>
          <Flex className="header-title" align="center">
            <H1 margin={0}>{t(i18nKeys.title)}</H1>
            <Button
              className="add-patient-button"
              text={t(i18nKeys.createPatientButton)}
              intent={Intent.PRIMARY}
              onClick={this.openAddPatientInfoDialog}
            />
          </Flex>
        </Header>
        {isLoading && <LoadingState />}
        {patientList.length > 0
          ? this.renderPatientTable()
          : this.renderEmptyView()}
      </Flex>
    );
  };

  renderEmptyView = () => {
    const { searchPhrase } = this.state;
    const { t } = this.props;
    return (
      <Flex className="empty-container" column>
        {!isEmpty(searchPhrase) ? (
          <div
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.noPatientInSearch, {
                searchPhrase,
                interpolation: { escapeValue: false },
              }),
            }}
          />
        ) : (
          <div
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.noPatientInSystem, {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        )}
      </Flex>
    );
  };

  renderPatientTable = () => {
    const { patientList = [] } = this.state;
    const { theme, t } = this.props;

    const renderActions = (patient: IProfile) => {
      return (
        <Flex className="sl-btn-code-scan">
          {!patient.paired && (
            <Button
              intent={Intent.PRIMARY}
              minimal
              small
              text={
                <BodyTextM
                  color={theme.foreground.primary.base}
                  fontWeight="SemiBold"
                >
                  {t(i18nKeys.scanPatientCodeButton)}
                </BodyTextM>
              }
              onClick={this.openQRPairingDialog(patient)}
            />
          )}
        </Flex>
      );
    };

    return (
      <Flex className="table-container">
        <Flex className="table-view">
          <HTMLTable>
            <thead>
              <tr>
                <th className="name-column">{t(i18nKeys.fullNameLabel)}</th>
                <th className="gender-column">{t(i18nKeys.genderLabel)}</th>
                <th className="dob-column">{t(i18nKeys.birthdayLabel)}</th>
                <th className="tel-column">{t(i18nKeys.phoneLabel)}</th>
                <th className="action-column" />
              </tr>
            </thead>
            <tbody>
              {patientList.map(patient => {
                return (
                  <tr key={patient.id}>
                    <td>{this.renderPatientFullName(patient)}</td>
                    <td>{this.getGender(patient.gender)}</td>
                    <td>{dateTimeFormat(patient.dob, 'L')}</td>
                    <td>{patient.phone}</td>
                    <td className="action-column"> {renderActions(patient)}</td>
                  </tr>
                );
              })}
            </tbody>
          </HTMLTable>
        </Flex>
      </Flex>
    );
  };

  renderPatientFullName = patient => {
    return (
      <Flex>
        {patient.fullName}
        {!patient.paired && (
          <BodyTextM className={'pair-status'} fontWeight="SemiBold">
            Chưa kết nối
          </BodyTextM>
        )}
      </Flex>
    );
  };

  quitEditingPatientInfo = () => {
    this.setState({
      isOpenCloseEditPatientInfoAlert: false,
      isOpenPatientInfoDialog: false,
      selectedPatient: undefined,
    });
  };

  backToEditingPatientInfo = () => {
    this.setState({
      isOpenCloseEditPatientInfoAlert: false,
      isOpenPatientInfoDialog: true,
    });
  };

  openAddPatientInfoDialog = () => {
    this.setState({ isOpenPatientInfoDialog: true });
  };

  closeAddPatientInfoDialog = () => {
    if (this.editPatientInfoRef && this.editPatientInfoRef._isDirty) {
      this.setState({
        isOpenCloseEditPatientInfoAlert: true,
        isOpenPatientInfoDialog: true,
      });
    } else {
      this.setState({
        isOpenPatientInfoDialog: false,
        selectedPatient: undefined,
      });
    }
    this.editPatientInfoRef = undefined;
  };
  openQRPairingDialog = (patient: IProfile) => () => {
    this.setState({
      isOpenPatientQRPairingDialog: true,
      selectedPatient: patient,
      selectedScanTabId: 'qr-tab',
    });
  };
  openPairingDialog = (patient: IProfile) => () => {
    this.setState({
      isOpenPatientPairingDialog: true,
      selectedPatient: patient,
    });
  };
  closeQRPairingDialog = () => {
    this.setState({
      isOpenPatientQRPairingDialog: false,
      selectedPatient: undefined,
    });
  };
  closePairingDialog = () => {
    this.setState({
      isOpenPatientPairingDialog: false,
      selectedPatient: undefined,
    });
  };

  loadPatientList = () => {
    this.setLoading(true);
    PatientService.getPatientList(this.state.offset)
      .then(patientList => {
        const {
          params: { patientId },
        } = this.props.match as any;
        const { history } = this.props;
        if (patientId) {
          const patient = patientList.find(item => item.id === patientId);
          if (patient) {
            this.setState(
              {
                patientList,
                isLoading: false,
                selectedPatient: patient,
                isOpenPatientQRPairingDialog: true,
              },
              () => history.replace('/app/patient')
            );
            return;
          }
        }
        this.setState({ patientList, isLoading: false });
      })
      .catch(() => this.setLoading(false));
  };

  onCreatePatient = async (patient: IPatientCreateRequest) => {
    return doPromiseInDuration(
      PatientService.createPatient({
        fullName: patient.fullName,
        nickName: patient.nickName,
        title: patient.title,
        dob: patient.dob,
        gender: patient.gender,
        phone: patient.phone,
      })
    ).then(result => {
      this.onAddedPatient(result);
      PatientPairingService.generateAndBackupKey(result.conversationId, () => {
        const _data = localStorage.getItem('CARE_PROVIDER');
        return Promise.resolve(_data ? JSON.parse(_data) : undefined);
      });
      return result;
    });
  };

  onEditPatient = (patient: IPatientUpdateRequest) => {
    return doPromiseInDuration(
      PatientService.updatePatientProfile(patient)
    ).then(result => {
      this.onUpdatedPatient(result);
      return result;
    });
  };

  onAddedPatient = (patient: IProfile) => {
    const patientList = [
      ...this.state.patientList,
      { ...patient },
    ] as IProfile[];
    this.setState({
      patientList,
      selectedPatient: patient,
      isOpenPatientInfoDialog: false,
      isOpenPatientQRPairingDialog: true,
      isOpenPatientPairingDialog: false,
    });
  };

  onUpdatedPatient = (updatedPatient: IProfile) => {
    const patientList = this.state.patientList.map(patient => {
      if (patient.id === updatedPatient.id) {
        return { ...patient, ...updatedPatient };
      }
      return patient;
    });

    if (this.props.onUpdatedPatient) {
      this.props.onUpdatedPatient(updatedPatient.id);
    }

    this.setState({
      patientList,
      selectedPatient: undefined,
      isOpenPatientInfoDialog: false,
    });
  };

  onCallbackPairing = () => {
    this.setState({
      selectedPatient: undefined,
      selectedScanTabId: 'qr-tab',
      isOpenPatientPairingDialog: false,
      isOpenPatientQRPairingDialog: false,
    });
    this.loadPatientList();
  };

  onEnterCode = patient => {
    this.setState({
      isOpenPatientQRPairingDialog: false,
      isOpenPatientPairingDialog: true,
    });
  };

  getGender = (gender: IGender) => {
    const { t } = this.props;

    switch (gender) {
      case 'FEMALE':
        return t(i18nKeys.femaleLabel);
      case 'MALE':
        return t(i18nKeys.maleLabel);
      case 'OTHERS':
        return t(i18nKeys.otherGenderLabel);
      default:
        return '-';
    }
  };

  onPatientEdit = (patient: IProfile) => () => {
    this.setState({ selectedPatient: patient, isOpenPatientInfoDialog: true });
  };

  setLoading = (isLoading: boolean) => {
    this.setState({ isLoading });
  };
}

export const PatientListView = withRouter(
  withTheme(
    withGlobalStyleContext(
      withI18nContext(OriginPatientListView, {
        namespace: NAMESPACE_PATIENT_LIST_VIEW,
        publicAssetPath: getModuleConfig().publicAssetPath,
      })
    )
  )
);
