import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';
import { IPatientListProps } from './models';
import { styled } from '@careprovider/theme';
import { PatientListView as OriginPatientListView } from './PatientListView';

export const PatientListView = styled(OriginPatientListView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-PatientListView', className),
  })
)<IPatientListProps>`
  ${props => {
    const { space, foreground, background, radius } = props.theme;
    const headerHeight = scaleSpace(20);
    const headerItemHeight = scaleSpace(10);

    return `
      & {
        height: 100%;

        .header-title {
          > h1 {
            flex: 1;
          }

          > .bp3-input-group {
            margin-right: ${space.m};
            > .bp3-input {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
            > .bp3-icon {
              margin: 12px 6px 12px 12px;
            }
          }

          .add-patient-button {
            height: ${headerItemHeight}px;
          }
        }

        .sl-LoadingState {
          position: absolute;
          width: auto;
          height: auto;
          top: 80px;
          left: 24px;
          right: 24px;
          bottom: 0;
          background-color: ${setAlpha(background.white, 0.5)};
        }

        .empty-container {
          flex: 1;
          justify-content: center;
          align-items: center;
          padding: 0 ${space.m};
          text-align: center;
        }

        .table-container {
          flex: 1;
          padding: 0 ${space.m};

          .table-view {
            width: 100%;
            max-height: calc(100vh - ${headerHeight}px);
            overflow: auto;

            th {
              box-shadow: none !important;
              font-size: 12px;
            }

            td {
              vertical-align: middle;
              box-shadow: none !important;
              border-bottom: 1px solid ${background[`01`]};
            }

            .pair-status {
              padding: 0 ${space.xxs};
              margin: 0 ${space.xxs};
              border-radius: ${radius['4px']};
              text-align: center;
              width: 80px;
              font-size: 12px;
              color: ${background.error.base};
              background-color: ${background.error.lighter};
            }

            .gender-column {
              max-width: 75px;
            }
            .tel-column {
              max-width: 100px;
            }
            .dob-column {
              max-width: 75px;
            }

            .action-column {
              min-width: 70px;
              .sl-btn-code-scan {
                background: none;
                border: none;
                justify-content: center;
                .bp3-button-text {
                  color: ${foreground.primary.base}
                  font-weight: 600;
                }
              }
            }
          }
        }
      }
    `;
  }}
`;
