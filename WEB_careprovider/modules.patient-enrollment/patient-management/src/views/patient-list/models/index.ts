import { ICareProviderTheme } from '@careprovider/theme';
import { IProfile } from '../../../services';

export interface IPatientListProps {
  className?: string;
  theme?: ICareProviderTheme;
  onViewDetail?: (id: number) => void;
  onUpdatedPatient?: (id: string) => void;
}

export interface IPatientListState {
  patientList: IProfile[];
  isOpenPatientInfoDialog: boolean;
  isOpenPatientPairingDialog: boolean;
  isOpenPatientQRPairingDialog: boolean;
  isOpenCloseEditPatientInfoAlert: boolean;
  selectedPatient: IProfile;
  offset: number;
  openDeletionDialog: boolean;
  searchPhrase: string;
  isLoading: boolean;
  selectedScanTabId: string;
}
