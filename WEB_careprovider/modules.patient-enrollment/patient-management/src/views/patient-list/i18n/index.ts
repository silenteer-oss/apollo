import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './patient-list-view.patient-enrollment.json';

export const NAMESPACE_PATIENT_LIST_VIEW =
  'patient-list-view.patient-enrollment';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
