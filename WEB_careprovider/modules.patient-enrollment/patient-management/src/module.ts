import {
  PATIENT_MANAGEMENT_MODULE_ID,
  PATIENT_LIST_VIEW_ID,
  CREATE_PATIENT_INFO_VIEW_ID,
  PATIENT_PAIRING_VIEW_ID,
} from '../module-config';
import {
  PatientListView,
  CreatePatientInfoView,
  PatientPairingView,
} from './views/';

export default {
  [PATIENT_MANAGEMENT_MODULE_ID]: {
    [PATIENT_LIST_VIEW_ID]: PatientListView,
    [CREATE_PATIENT_INFO_VIEW_ID]: CreatePatientInfoView,
    [PATIENT_PAIRING_VIEW_ID]: PatientPairingView,
  },
};
