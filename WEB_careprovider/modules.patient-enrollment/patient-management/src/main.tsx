import React from 'react';
import ReactDOM from 'react-dom';
import {
  getAllComponentTheme,
  theme,
  getAllViewTheme,
} from '@careprovider/theme';
import { RootView } from './views';

import '@careprovider/assets/styles/index.scss';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

ReactDOM.render(<RootView theme={theme} />, document.getElementById('root'));
