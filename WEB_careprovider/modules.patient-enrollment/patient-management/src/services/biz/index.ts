import PatientService from '@careprovider/modules/patient-management/PatientManagementService';

export * from './models';
export { PatientService };
