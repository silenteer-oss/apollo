import { withI18nContext } from '@design-system/i18n/context';
import { withTrustedDeviceView } from '@careprovider/modules/trusted-device';
import { withTheme } from '@careprovider/theme';
import { storeCareProviderKeyPairToLocalStorage } from '@careprovider/services/client-database';
import { getModuleConfig } from '../module-config';
import { NAMESPACE_TRUSTED_DEVICE_VIEW } from './i18n';

export default withTheme(
  withI18nContext(
    withTrustedDeviceView({
      storeCareProviderKeyPair: storeCareProviderKeyPairToLocalStorage,
    }),
    {
      namespace: NAMESPACE_TRUSTED_DEVICE_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    }
  )
);
