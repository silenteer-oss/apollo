import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { ITrustedDeviceViewProps } from './models';
import TrustedDeviceView from './TrustedDeviceView';

export default styled(TrustedDeviceView).attrs(({ className }) => ({
  className: getCssClass('sl-TrustedDeviceView', className),
}))<ITrustedDeviceViewProps>`
  ${props => {
    const { space } = props.theme;

    return `
      & {
        padding: ${space.xl} ${space.m};

        a {
          cursor: pointer;
        }

        p {
          margin-bottom: ${space.s};
        }

        h3 {
          margin: ${space.s} 0;
        }

        .bp3-input-group .bp3-input {
          text-align: center;
          width: ${scaleSpacePx(70)};
          font-size: 48px;
          text-transform: uppercase;
          letter-spacing: 0.1em;
        }
      }
    `;
  }}
`;
