import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './trusted-device-view.patient-enrollment.json';

export const NAMESPACE_TRUSTED_DEVICE_VIEW =
  'trusted-device-view.patient-enrollment';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
