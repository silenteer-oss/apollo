#!/usr/bin/env bash
workspace=".."

init () {
  # [ -d "$1/node_modules" ] && rm -rf "$1/node_modules"
  # [ -f "$1/package-lock.json" ] && rm "$1/package-lock.json"
  # [ -f "$1/yarn.lock" ] && rm "$1/yarn.lock"
  cd $1 && yarn

  if [ $? -eq 0 ]
  then
    echo "$(tput setaf 2)-------------------- successed init $1 --------------------$(tput sgr 0)"
  else
    echo "$(tput setaf 1)-------------------- failed init $1 --------------------$(tput sgr 0)"
  fi

  cd -
}

init "$workspace/../hermes"
init "$workspace/LIB_design-system"
init "$workspace/LIB_infrastructure"
init "$workspace/WEB_careprovider"

exit
