export const PATIENT_PAGE_PATH = '/app/patient';
export const PATIENT_PAGE_PARING_PAGE_PATH = '/app/patient/:patientId';
export const APPOINTMENT_PAGE_PATH = '/app/appointment';
export const CONVERSATION_PAGE_PATH = '/app/conversation';
export const CONVERSATION_DETAIL_PAGE_PATH = '/app/conversation/:patientId';
export const COMMUNICATION_PLAN_PAGE_PATH = '/app/communication-plan';
export const COMMUNICATION_PLAN_DETAIL_PAGE_PATH =
  '/app/communication-plan/:planId';
export const COMMUNICATION_PLAN_PATIENT_PAGE_PATH =
  '/app/communication-plan-patient';
export const BROADCASTING_PAGE_PATH = '/app/broadcasting';
export const BROADCASTING_ADDING_PAGE_PATH = '/app/broadcasting/add';
export const BROADCASTING_EDITING_PAGE_PATH = '/app/broadcasting/edit/:id';
