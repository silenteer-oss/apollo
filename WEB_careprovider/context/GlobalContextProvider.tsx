import React from 'react';
import {
  IEmployeeProfile,
  IPatientProfile,
  getPatientProfiles,
  getAllEmployeeProfiles,
} from '@careprovider/services/biz';
import { initWorker, terminateWorker } from '@careprovider/services/webworker';
import { IGlobalContext, IGlobalProps, IGlobalState } from './models';
import { GlobalContext } from './GlobalContext';

export class GlobalContextProvider extends React.PureComponent<
  IGlobalProps,
  IGlobalState
> {
  private _allEmployeeProfilesPromise: Promise<IEmployeeProfile[]>;
  private _patientProfileMap: { [key: string]: Promise<IPatientProfile> } = {};
  private _globalContext: IGlobalContext;
  _tt: any;

  constructor(props: IGlobalProps) {
    super(props);

    this.state = {
      userProfile: props.userProfile,
    };

    this._globalContext = {
      userProfile: props.userProfile,
      setUserProfile: this._setUserProfile,
      getEmployeeProfiles: this._getEmployeeProfiles,
      reloadEmployeeProfiles: this._reloadEmployeeProfiles,
      getPatientProfiles: this._getPatientProfiles,
      updatePatientProfiles: this._updatePatientProfiles,
      getInitialState: props.getInitialState,
    };

    initWorker();
  }

  componentWillUnmount() {
    terminateWorker();
  }

  render() {
    return (
      <GlobalContext.Provider value={this._globalContext}>
        {this.props.children}
      </GlobalContext.Provider>
    );
  }

  private _setUserProfile = (profile: IEmployeeProfile) => {
    this._globalContext = {
      ...this._globalContext,
      userProfile: profile,
    };
  };

  private _getPatientProfiles = (ids: string[]): Promise<IPatientProfile[]> => {
    const newIds = ids.filter(id => !this._patientProfileMap[id]);

    if (newIds.length > 0) {
      const promise = getPatientProfiles(newIds);

      newIds.forEach(id => {
        this._patientProfileMap[id] = promise.then(result =>
          result.find(data => data.id === id)
        );
      });
    }

    return Promise.all([...ids.map(id => this._patientProfileMap[id])]);
  };

  private _updatePatientProfiles = (ids: string[]): void => {
    ids.forEach(id => {
      delete this._patientProfileMap[id];
    });
  };

  private _getEmployeeProfiles = (): Promise<IEmployeeProfile[]> => {
    if (!this._allEmployeeProfilesPromise) {
      this._allEmployeeProfilesPromise = getAllEmployeeProfiles();
    }
    return this._allEmployeeProfilesPromise;
  };

  private _reloadEmployeeProfiles = (): void => {
    this._allEmployeeProfilesPromise = getAllEmployeeProfiles();
    this._tt = this._getEmployeeProfiles();
    this._globalContext = {
      ...this._globalContext,
      // Create a new funtion to force rerender child component
      getEmployeeProfiles: () => this._getEmployeeProfiles(),
    };
    this.forceUpdate();
  };
}
