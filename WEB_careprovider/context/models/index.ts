import { IEmployeeProfile, IPatientProfile } from '../../services/biz';

export interface IGlobalContext {
  userProfile: IEmployeeProfile;
  setUserProfile: (profile: IEmployeeProfile) => void;
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
  reloadEmployeeProfiles: () => void;
  getPatientProfiles: (ids: string[]) => Promise<IPatientProfile[]>;
  updatePatientProfiles: (ids: string[]) => void;
  getInitialState: () => void;
}

export interface IGlobalState {
  userProfile: IEmployeeProfile;
}

export interface IGlobalProps {
  userProfile: IEmployeeProfile;
  getInitialState?: () => void;
}
