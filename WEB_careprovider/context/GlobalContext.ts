/* eslint-disable prettier/prettier */
import React from 'react';
import { IGlobalContext } from './models';

const defaultContext: IGlobalContext = {
  userProfile: undefined,
  setUserProfile: () => Promise.resolve(),
  getEmployeeProfiles: () => Promise.resolve(undefined),
  reloadEmployeeProfiles: () => Promise.resolve(undefined),
  getPatientProfiles: () => Promise.resolve(undefined),
  updatePatientProfiles: () => Promise.resolve(),
  getInitialState: () => Promise.resolve(),
};

export const GlobalContext = React.createContext<IGlobalContext>(
  defaultContext
);
