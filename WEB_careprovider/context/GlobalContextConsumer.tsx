import React, { ReactNode } from 'react';
import { IGlobalContext } from './models';
import { GlobalContext } from './GlobalContext';

export class GlobalContextConsumer extends React.PureComponent<{
  children: (value: IGlobalContext) => ReactNode;
}> {
  render() {
    return (
      <GlobalContext.Consumer>{this.props.children}</GlobalContext.Consumer>
    );
  }
}
