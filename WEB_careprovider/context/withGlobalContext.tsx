import React, { ComponentType, FunctionComponent } from 'react';
import { IGlobalContext } from './models';
import { GlobalContext } from './GlobalContext';

export function withGlobalContext<TProps>(
  Component: ComponentType<TProps & IGlobalContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <GlobalContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </GlobalContext.Consumer>
    );
  };
}
