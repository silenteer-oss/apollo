import { AppointmentContext, IAppointmentContext } from './AppointmentContext';
import { ComponentType, FunctionComponent } from 'react';
import React from 'react';

export function withAppointmentContext<TProps>(
  Component: ComponentType<TProps & IAppointmentContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <AppointmentContext.Consumer>
        {(contexts: IAppointmentContext) => (
          <Component {...props} {...contexts} />
        )}
      </AppointmentContext.Consumer>
    );
  };
}
