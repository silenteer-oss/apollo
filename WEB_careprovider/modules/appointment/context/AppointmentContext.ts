import { IEmployeeProfile } from '@careprovider/services/biz';
/* eslint-disable prettier/prettier */
import React from 'react';

export interface IAppointmentContext {
  getDoctors: () => Promise<{ dictionary: {[key: string]: IEmployeeProfile }, items: IEmployeeProfile[]}>;
  getNurses: () => Promise<{ dictionary: {[key: string]: IEmployeeProfile }, items: IEmployeeProfile[]}>;
  getReceptionists: () => Promise<{ dictionary: {[key: string]: IEmployeeProfile }, items: IEmployeeProfile[]}>;
  // getPatients: () => {[key: string]: IProfile};
  // addPatient: (patient: IProfile) => void;
}

const defaultContext: IAppointmentContext = {
  getDoctors: () => {
    console.log("Default context is used");
    return Promise.resolve(undefined);
  },
  getNurses: () => {
    console.log("Default context is used");
    return Promise.resolve(undefined);
  },
  getReceptionists: () => {
    console.log("Default context is used");
    return Promise.resolve(undefined);
  },
  // getPatients: () => {
  //   console.log("Default context is used");
  //   return undefined;
  // },
  // addPatient: () => {
  //   console.log("Default context is used");
  // }
};

export const AppointmentContext = React.createContext<IAppointmentContext>(
  defaultContext
);