import React from 'react';
import { AppointmentContext, IAppointmentContext } from './AppointmentContext';

import { IEmployeeProfile, UserType } from '@careprovider/services/biz';

interface AppointmentContextProviderState {
  doctors?: { [key: string]: IEmployeeProfile }; // all doctors, mapped by doctor's userId
  doctorArray?: IEmployeeProfile[];
  nurses?: { [key: string]: IEmployeeProfile }; // all nurses, mapped by nurse's userId
  nurseArray?: IEmployeeProfile[];
  receptionists?: { [key: string]: IEmployeeProfile }; // all receptionists, mapped by receptionist's userId
  receptionistArray?: IEmployeeProfile[];
  employeeProfileArray?: IEmployeeProfile[];
}

export interface AppointmentContextProviderProps {
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
}

export class AppointmentContextProvider extends React.Component<
  AppointmentContextProviderProps,
  AppointmentContextProviderState
> {
  private receptionistsPromise: Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }>;
  private doctorsPromise: Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }>;
  private nursesPromise: Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }>;
  private employeeProfilesPromise: Promise<IEmployeeProfile[]>;

  state: AppointmentContextProviderState = {};
  appointmentContext: IAppointmentContext;

  constructor(props) {
    super(props);
    this.appointmentContext = {
      getDoctors: this.getDoctors,
      getNurses: this.getNurses,
      getReceptionists: this.getReceptionists,
    };
  }

  getDoctors = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (!this.doctorsPromise) {
      this.doctorsPromise = new Promise(async (resolve, reject) => {
        if (this.state.doctors && this.state.doctorArray) {
          return resolve({
            dictionary: this.state.doctors,
            items: this.state.doctorArray,
          });
        }

        const doctors = (await this._getEmployeeProfiles()).filter(
          profile => profile.type === UserType.DOCTOR
        );

        this.setState(
          {
            doctors: doctors.reduce(
              (result, doctor) => ({ ...result, [doctor.id]: doctor }),
              {} as { [key: string]: IEmployeeProfile }
            ),
            doctorArray: doctors,
          },
          () => {
            resolve({
              dictionary: this.state.doctors,
              items: this.state.doctorArray,
            });
          }
        );
      });
    }

    return this.doctorsPromise;
  };

  getNurses = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (!this.nursesPromise) {
      this.nursesPromise = new Promise(async (resolve, reject) => {
        if (this.state.nurses && this.state.nurseArray) {
          return resolve({
            dictionary: this.state.nurses,
            items: this.state.nurseArray,
          });
        }

        const nurses = (await this._getEmployeeProfiles()).filter(
          profile => profile.type === UserType.NURSE
        );

        this.setState(
          {
            nurses: nurses.reduce(
              (result, nurse) => ({ ...result, [nurse.id]: nurse }),
              {} as { [key: string]: IEmployeeProfile }
            ),
            nurseArray: nurses,
          },
          () => {
            resolve({
              dictionary: this.state.nurses,
              items: this.state.nurseArray,
            });
          }
        );
      });
    }

    return this.nursesPromise;
  };

  getReceptionists = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (!this.receptionistsPromise) {
      this.receptionistsPromise = new Promise(async (resolve, reject) => {
        if (this.state.receptionists && this.state.receptionistArray) {
          return resolve({
            dictionary: this.state.receptionists,
            items: this.state.receptionistArray,
          });
        }

        const receptionists = (await this._getEmployeeProfiles()).filter(
          profile => profile.type === UserType.RECEPTIONIST
        );

        this.setState(
          {
            receptionists: receptionists.reduce(
              (result, receptionist) => ({
                ...result,
                [receptionist.id]: receptionist,
              }),
              {} as { [key: string]: IEmployeeProfile }
            ),
            receptionistArray: receptionists,
          },
          () => {
            resolve({
              dictionary: this.state.receptionists,
              items: this.state.receptionistArray,
            });
          }
        );
      });
    }

    return this.receptionistsPromise;
  };

  private _getEmployeeProfiles = async (): Promise<IEmployeeProfile[]> => {
    if (!this.employeeProfilesPromise) {
      this.employeeProfilesPromise = new Promise(async resolve => {
        if (this.state.employeeProfileArray) {
          return resolve(this.state.employeeProfileArray);
        }

        const { getEmployeeProfiles } = this.props;
        if (!getEmployeeProfiles) {
          this.setState({ employeeProfileArray: [] }, () => {
            resolve(this.state.employeeProfileArray);
          });
        }

        const employeeProfileArray = await getEmployeeProfiles();
        this.setState({ employeeProfileArray }, () => {
          resolve(this.state.employeeProfileArray);
        });
      });
    }

    return this.employeeProfilesPromise;
  };

  render() {
    return (
      <AppointmentContext.Provider value={this.appointmentContext}>
        {this.props.children}
      </AppointmentContext.Provider>
    );
  }
}
