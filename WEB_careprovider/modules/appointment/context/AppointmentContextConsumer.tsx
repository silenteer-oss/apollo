import React, { ReactNode } from 'react';
import { IAppointmentContext, AppointmentContext } from './AppointmentContext';

export class AppointmentContextConsumer extends React.PureComponent<{
  children: (value: IAppointmentContext) => ReactNode;
}> {
  render() {
    return (
      <AppointmentContext.Consumer>
        {this.props.children}
      </AppointmentContext.Consumer>
    );
  }
}
