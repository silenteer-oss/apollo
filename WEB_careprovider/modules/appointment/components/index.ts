import { Toaster, Position } from '@blueprintjs/core';

/** Singleton toaster instance. Create separate instances for different options. */
export const ModuleToaster = Toaster.create({
  className: 'appointment-toaster',
  position: Position.BOTTOM_LEFT,
});
