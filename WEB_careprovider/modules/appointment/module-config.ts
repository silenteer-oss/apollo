import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const APPOINTMENT_MODULE_ID = 'AppointmentModule';

export const APPOINTMENT_DETAILS_VIEW_ID = 'AppointmentDetailsView';
export const APPOINTMENT_CHANGE_LOG_VIEW_ID = 'AppointmentChangeLogView';
export const UPCOMING_APPOINTMENT_VIEW_ID = 'UpcomingAppointmentView';
export const APPOINTMENT_FILTER_VIEW_ID = 'AppointmentFilterView';
export const APPOINTMENT_COLUMN_LIST_VIEW_ID = 'AppointmentColumnListView';

export const APPOINTMENT_CONTEXT_PROVIDER_ID = 'AppointmentContextProvider';
export const APPOINTMENT_CONTEXT_CONSUMER_ID = 'AppointmentContextConsumer';

export interface IModuleViews {
  [APPOINTMENT_DETAILS_VIEW_ID]: string;
  [APPOINTMENT_CHANGE_LOG_VIEW_ID]: string;
  [UPCOMING_APPOINTMENT_VIEW_ID]: string;
  [APPOINTMENT_FILTER_VIEW_ID]: string;
  [APPOINTMENT_COLUMN_LIST_VIEW_ID]: string;
}
export interface IModuleProviders {
  [APPOINTMENT_CONTEXT_PROVIDER_ID]: string;
}
export interface IModuleConsumers {
  [APPOINTMENT_CONTEXT_CONSUMER_ID]: string;
}

export function getModuleConfig(): IModuleConfig<
  IModuleViews,
  IModuleProviders,
  IModuleConsumers
> {
  return {
    id: APPOINTMENT_MODULE_ID,
    name: 'Appointment Module',
    publicAssetPath: `/module/appointment`,
    views: {
      AppointmentDetailsView: APPOINTMENT_DETAILS_VIEW_ID,
      AppointmentChangeLogView: APPOINTMENT_CHANGE_LOG_VIEW_ID,
      UpcomingAppointmentView: UPCOMING_APPOINTMENT_VIEW_ID,
      AppointmentFilterView: APPOINTMENT_FILTER_VIEW_ID,
      AppointmentColumnListView: APPOINTMENT_COLUMN_LIST_VIEW_ID,
    },
    providers: {
      AppointmentContextProvider: APPOINTMENT_CONTEXT_PROVIDER_ID,
    },
    consumers: {
      AppointmentContextConsumer: APPOINTMENT_CONTEXT_CONSUMER_ID,
    },
  };
}
