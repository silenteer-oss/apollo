import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import {
  ThemeProvider,
  ThemeConsumer,
  theme,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
  getAllComponentTheme,
} from '@careprovider/theme';
import {
  AppointmentDetailsView,
  AppointmentChangeLogView,
  AppointmentFilterView,
} from './views';
import { IAppointmentContext, AppointmentContext } from './context';
import { Flex, Box } from '@design-system/core/components';

import '@careprovider/assets/styles/index.scss';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import { Gender } from '@silenteer/hermes/resource/profile-resource/model';
import { now } from '@careprovider/services/util';
import { IEmployeeProfile, UserType } from '@careprovider/services/biz';

theme.setComponentTheme(getAllComponentTheme);

class SandBox extends React.Component<any, { isInitedI18n: boolean }> {
  state = { isInitedI18n: false };

  render() {
    const { isInitedI18n } = this.state;

    const createAppointmentMessageBody = {
      id: '1',
      changeMakerId: 'r1',
      action: AppointmentApiModel.ActionType.CREATE,
      startDateTime: now(),
      doctorIds: ['d1'],
      note: 'note1',
      internalNote: 'internalNote1',
      reason: 'reason 1',
      departmentName: 'department 1',
    };
    const confirmAppointmentMessageBody = {
      id: '1',
      changeMakerId: 'r1',
      action: AppointmentApiModel.ActionType.CONFIRM,
      startDateTime: now(),
      doctorIds: ['d1'],
      note: 'note1',
      reason: 'reason 1',
      departmentName: 'department 1',
    };
    const cancelAppointmentMessageBody = {
      id: '1',
      changeMakerId: 'r1',
      action: AppointmentApiModel.ActionType.CANCEL,
      startDateTime: now(),
      doctorIds: ['d1'],
      note: 'note1',
      reason: 'reason 1',
      cancelReason: 'cancel reason 1',
      departmentName: 'department 1',
    };
    return (
      <I18nContextProvider
        {...getI18nInitOptions()}
        onInitedI18n={this._handleInitedI18n}
      >
        <ThemeProvider theme={theme}>
          <ThemeConsumer>
            {theme => (
              <GlobalStyleContextProvider
                styles={{
                  main: mixGlobalStyle(theme),
                  blueprint: mixCustomBlueprintStyle(theme),
                }}
              >
                {isInitedI18n && (
                  <DevAppointmentContextProvider>
                    <Flex style={{ justifyContent: 'space-between' }}>
                      <Box>
                        <AppointmentFilterView
                          onDateSelected={data =>
                            console.log('Filter date ', data)
                          }
                          onDoctorsSelected={data =>
                            console.log('Filter doctors ', data)
                          }
                          onStatusSelected={data =>
                            console.log('Filter status ', data)
                          }
                        />
                      </Box>
                      <Box>
                        <AppointmentChangeLogView
                          id={createAppointmentMessageBody.id}
                          changeMakerId={
                            createAppointmentMessageBody.changeMakerId
                          }
                          action={createAppointmentMessageBody.action}
                          startDateTime={
                            createAppointmentMessageBody.startDateTime
                          }
                          hourAndMinuteSpecified
                          doctorIds={createAppointmentMessageBody.doctorIds}
                          note={createAppointmentMessageBody.note}
                          internalNote={
                            createAppointmentMessageBody.internalNote
                          }
                          reason={createAppointmentMessageBody.reason}
                          departmentName={
                            createAppointmentMessageBody.departmentName
                          }
                        />
                      </Box>
                      <Box>
                        <AppointmentChangeLogView
                          id={confirmAppointmentMessageBody.id}
                          changeMakerId={
                            confirmAppointmentMessageBody.changeMakerId
                          }
                          action={confirmAppointmentMessageBody.action}
                          startDateTime={
                            confirmAppointmentMessageBody.startDateTime
                          }
                          hourAndMinuteSpecified
                          doctorIds={confirmAppointmentMessageBody.doctorIds}
                          note={confirmAppointmentMessageBody.note}
                          reason={confirmAppointmentMessageBody.reason}
                          departmentName={
                            confirmAppointmentMessageBody.departmentName
                          }
                        />
                      </Box>
                      <Box>
                        <AppointmentChangeLogView
                          id={cancelAppointmentMessageBody.id}
                          changeMakerId={
                            cancelAppointmentMessageBody.changeMakerId
                          }
                          action={cancelAppointmentMessageBody.action}
                          startDateTime={
                            cancelAppointmentMessageBody.startDateTime
                          }
                          hourAndMinuteSpecified
                          doctorIds={cancelAppointmentMessageBody.doctorIds}
                          note={cancelAppointmentMessageBody.note}
                          reason={cancelAppointmentMessageBody.reason}
                          cancelReason={
                            cancelAppointmentMessageBody.cancelReason
                          }
                          departmentName={
                            cancelAppointmentMessageBody.departmentName
                          }
                        />
                      </Box>
                      <Box>
                        <AppointmentDetailsView />
                      </Box>
                    </Flex>
                  </DevAppointmentContextProvider>
                )}
              </GlobalStyleContextProvider>
            )}
          </ThemeConsumer>
        </ThemeProvider>
      </I18nContextProvider>
    );
  }

  private _handleInitedI18n = () => {
    this.setState({ isInitedI18n: true });
  };
}

interface DevAppointmentContextProviderState {
  doctors?: { [key: string]: IEmployeeProfile }; // all doctors, mapped by doctor's userId
  doctorArray?: IEmployeeProfile[];
  nurses?: { [key: string]: IEmployeeProfile }; // all nurses, mapped by nurse's userId
  nurseArray?: IEmployeeProfile[];
  receptionists?: { [key: string]: IEmployeeProfile }; // all receptionists, mapped by receptionist's userId
  receptionistArray?: IEmployeeProfile[];
}

class DevAppointmentContextProvider extends React.Component<
  any,
  DevAppointmentContextProviderState
  > {
  state: DevAppointmentContextProviderState = {};
  appointmentContext: IAppointmentContext;

  constructor(props) {
    super(props);
    this.appointmentContext = {
      getDoctors: this.getDoctors,
      getNurses: this.getNurses,
      getReceptionists: this.getReceptionists,
      // getPatients: this.getPatients,
      // addPatient: this.addPatient,
    };
  }

  getDoctors = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (this.state.doctors && this.state.doctorArray) {
      return Promise.resolve({
        dictionary: this.state.doctors,
        items: this.state.doctorArray,
      });
    }
    const doctors = [
      {
        id: 'd1',
        userId: 'ud1',
        fullName: 'doctor 1',
        dob: 2341234124,
        gender: Gender.MALE,
        phone: '12341243134',
        email: '3msfsafsf@dfasdf.com',
        address: '',
        accountId: '123413',
        careProvideId: '452435',
        type: UserType.DOCTOR,
      },
    ];

    return new Promise(resolve => {
      this.setState(
        {
          doctors: doctors.reduce(
            (result, doctor) => ({ ...result, [doctor.id]: doctor }),
            {} as { [key: string]: IEmployeeProfile }
          ),
          doctorArray: doctors,
        },
        () => {
          resolve({
            dictionary: this.state.doctors,
            items: this.state.doctorArray,
          });
        }
      );
    });
  };

  getNurses = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (this.state.nurses && this.state.nurseArray) {
      return Promise.resolve({
        dictionary: this.state.nurses,
        items: this.state.nurseArray,
      });
    }
    const nurses = [
      {
        id: 'n1',
        userId: 'un1',
        fullName: 'nurse 1',
        dob: undefined,
        gender: undefined,
        phone: '2343343433',
        email: undefined,
        address: undefined,
        accountId: '123413',
        careProvideId: '452435',
        type: UserType.NURSE,
      },
    ];

    return new Promise(resolve => {
      this.setState(
        {
          nurses: nurses.reduce(
            (result, nurse) => ({ ...result, [nurse.id]: nurse }),
            {} as { [key: string]: IEmployeeProfile }
          ),
          nurseArray: nurses,
        },
        () => {
          resolve({
            dictionary: this.state.nurses,
            items: this.state.nurseArray,
          });
        }
      );
    });
  };

  getReceptionists = async (): Promise<{
    dictionary: { [key: string]: IEmployeeProfile };
    items: IEmployeeProfile[];
  }> => {
    if (this.state.receptionists && this.state.receptionistArray) {
      return Promise.resolve({
        dictionary: this.state.receptionists,
        items: this.state.receptionistArray,
      });
    }
    const receptionists = [
      {
        id: 'r1',
        userId: 'ur1',
        fullName: 'receptionist 1',
        dob: undefined,
        gender: undefined,
        phone: '5666666666',
        email: undefined,
        address: undefined,
        accountId: '123413',
        careProvideId: '452435',
        type: UserType.RECEPTIONIST,
      },
    ];

    return new Promise(resolve => {
      this.setState(
        {
          receptionists: receptionists.reduce(
            (result, receptionist) => ({
              ...result,
              [receptionist.id]: receptionist,
            }),
            {} as { [key: string]: IEmployeeProfile }
          ),
          receptionistArray: receptionists,
        },
        () => {
          resolve({
            dictionary: this.state.receptionists,
            items: this.state.receptionistArray,
          });
        }
      );
    });
  };

  render() {
    return (
      <AppointmentContext.Provider value={this.appointmentContext}>
        {this.props.children}
      </AppointmentContext.Provider>
    );
  }
}

ReactDOM.render(<SandBox />, document.getElementById('root'));
