import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';

export enum ReminderUnit {
  HOUR = 'HOUR',
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
}

export interface AppointmentDetail
  extends AppointmentApiModel.AppointmentResponse {
  patientName: string;
}
