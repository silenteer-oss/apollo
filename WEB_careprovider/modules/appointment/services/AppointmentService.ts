import {
  AppointmentApi,
  AppointmentApiModel,
} from '@infrastructure/resource/AppointmentResource';
import {
  ProfileApi,
  ProfileApiModel,
} from '@infrastructure/resource/ProfileResource';
import { AppointmentDetail } from './AppointmentServiceModel';

class AppointmentService {
  createAppointment = async (
    params: AppointmentApiModel.AppointmentRequest
  ): Promise<AppointmentApiModel.AppointmentResponse> => {
    let response = await AppointmentApi.create(params);
    return response.data;
  };

  searchPatient = async (query: string) => {
    let response = await ProfileApi.searchPatientProfile(query);
    return response.data;
  };

  getPatientIdentitiesByIds = async (
    originalIds: string[]
  ): Promise<ProfileApiModel.PatientProfileResponse[]> => {
    const response = await ProfileApi.getPatientProfileByIds({ originalIds });
    return response.data;
  };

  getUpcomingAppointment = async (
    patientId: string
  ): Promise<AppointmentApiModel.AppointmentResponse[]> => {
    let response = await AppointmentApi.getUpcomingsByPatient(patientId);
    return response.data;
  };

  editAppointment = async (
    appointmentId: string,
    params: AppointmentApiModel.AppointmentRequest
  ): Promise<AppointmentApiModel.AppointmentResponse> => {
    let response = await AppointmentApi.edit(appointmentId, params);
    return response.data;
  };

  cancelAppointment = async (
    appointmentId: string,
    cancelledDate: number,
    cancelReasonId: string
  ): Promise<boolean> => {
    let response = await AppointmentApi.cancel({
      appointmentId,
      cancelledDate,
      cancelReasonId,
    });

    return response.data;
  };

  findAppointments = async (
    from: number,
    to: number,
    doctorIds: string[],
    includeConfirmed: boolean,
    includePending: boolean,
    includeUnassigned: boolean
  ) => {
    const request: AppointmentApiModel.AppointmentSearchByCareProviderRequest = {
      from,
      to,
      doctorIds,
      includeConfirmed,
      includePending,
      includeUnassigned,
    };
    const appointmentsResponse = await AppointmentApi.findAppointments(request);
    const appointments = appointmentsResponse.data;

    if (appointments === undefined || appointments.length === 0) {
      return [];
    }

    const patientIds: string[] = [];
    for (const appointment of appointments) {
      if (!patientIds.includes(appointment.patientId)) {
        patientIds.push(appointment.patientId);
      }
    }

    const patientProfilesResponse = await ProfileApi.getPatientProfileByIds({
      originalIds: patientIds,
    });
    const patientProfiles = patientProfilesResponse.data;
    const patientId2Profile: {
      [key: string]: ProfileApiModel.PatientProfileResponse;
    } = {};
    for (const patientProfile of patientProfiles) {
      patientId2Profile[patientProfile.id] = patientProfile;
    }

    const appointmentDetails: AppointmentDetail[] = [];
    for (const appointment of appointments) {
      const patientProfile = patientId2Profile[appointment.patientId];
      appointmentDetails.push({
        ...appointment,
        patientName: patientProfile.fullName,
      });
    }
    return appointmentDetails;
  };

  getCancelReasonList = async (): Promise<
    AppointmentApiModel.AppointmentCancelReasonResponse[]
  > => {
    let response = await AppointmentApi.getCareProviderCancelReasonList();
    return response.data;
  };
}

export default new AppointmentService();
