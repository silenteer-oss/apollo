import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-change-log-view.json';

export const NAMESPACE_APPOINTMENT_CHANGE_LOG_VIEW =
  'appointment-change-log-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
