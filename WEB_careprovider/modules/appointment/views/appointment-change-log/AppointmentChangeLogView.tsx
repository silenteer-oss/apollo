import React from 'react';
import { Button } from '@blueprintjs/core';
import { Flex, BodyTextM } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { ProfileApiModel } from '@infrastructure/resource/ProfileResource';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../module-config';
import { withAppointmentContext, IAppointmentContext } from '../../context';
import AppointmentService from '../../services/AppointmentService';
import { NAMESPACE_APPOINTMENT_CHANGE_LOG_VIEW, i18nKeys } from './i18n';
import { AppointmentChangeLogRenderer } from './components';
import {
  IAppoinmentChangeLogViewProps,
  IAppoinmentChangeLogViewState,
} from './models';

class OriginAppointmentChangeLogView extends React.PureComponent<
  IAppoinmentChangeLogViewProps &
  IAppointmentContext &
  II18nFixedNamespaceContext,
  IAppoinmentChangeLogViewState
  > {
  state: IAppoinmentChangeLogViewState = {
    showDetails: false,
    doctors: undefined,
    nurses: undefined,
    receptionists: undefined,
    patientProfile: undefined,
  };

  componentDidMount() {
    const {
      getDoctors,
      getNurses,
      getReceptionists,
      changeMakerId,
      patientProfile: providedPatientProfile,
    } = this.props;
    Promise.all([getDoctors(), getNurses(), getReceptionists()]).then(
      ([
        doctors = { dictionary: {}, items: [] },
        nurses = { dictionary: {}, items: [] },
        receptionists = { dictionary: {}, items: [] },
      ]) => {
        if (
          doctors.dictionary[changeMakerId] ||
          nurses.dictionary[changeMakerId] ||
          receptionists.dictionary[changeMakerId]
        ) {
          this.setState({
            doctors: doctors.dictionary,
            nurses: nurses.dictionary,
            receptionists: receptionists.dictionary,
          });
        } else {
          if (
            providedPatientProfile &&
            providedPatientProfile.id === changeMakerId
          ) {
            this.setState({
              patientProfile: providedPatientProfile,
              doctors: doctors.dictionary,
              nurses: nurses.dictionary,
              receptionists: receptionists.dictionary,
            });
          } else {
            AppointmentService.getPatientIdentitiesByIds([changeMakerId]).then(
              (profiles: ProfileApiModel.PatientProfileResponse[]) => {
                if (profiles && profiles.length > 0) {
                  const patientProfile = profiles[0];
                  this.setState({
                    patientProfile,
                    doctors: doctors.dictionary,
                    nurses: nurses.dictionary,
                    receptionists: receptionists.dictionary,
                  });
                } else {
                  throw new Error(
                    `Cannot find profile for changemakerId ${changeMakerId}`
                  );
                }
              }
            );
          }
        }
      }
    );
  }

  render() {
    const {
      showDetails,
      doctors,
      nurses,
      receptionists,
      patientProfile,
    } = this.state;

    const {
      className,
      changeMakerId,
      action,
      startDateTime,
      hourAndMinuteSpecified,
      doctorIds,
      note,
      reason,
      cancelReason,
      departmentName,
      theme: { foreground },
      editedFields,
      t,
    } = this.props;

    if (!doctors || !nurses || !receptionists) {
      return (
        <Flex column align="center" className={className}>
          ...Loading
        </Flex>
      );
    }

    const changeMaker =
      doctors[changeMakerId] ||
      nurses[changeMakerId] ||
      receptionists[changeMakerId] ||
      patientProfile;

    let doctorNames;
    if (doctorIds && doctorIds.length > 0) {
      const doctorIdentities = doctorIds.map(
        doctorId => doctors[doctorId].fullName
      );
      doctorNames = doctorIdentities.join(', ');
    }

    return (
      <Flex column align="center" className={className}>
        <AppointmentChangeLogRenderer
          showDetails={showDetails}
          changeMakerName={changeMaker.fullName}
          startDateTime={startDateTime}
          hourAndMinuteSpecified={hourAndMinuteSpecified}
          doctorName={doctorNames}
          note={note}
          reason={reason}
          cancelReason={cancelReason}
          departmentName={departmentName}
          action={action}
          editedFields={editedFields}
        />
        <Button
          minimal
          onClick={this.toggleShowDetails}
          text={
            <BodyTextM color={foreground.primary.base} fontWeight="SemiBold">
              {showDetails
                ? t(i18nKeys.hideDetailButton)
                : t(i18nKeys.showDetailButton)}
            </BodyTextM>
          }
        />
      </Flex>
    );
  }

  toggleShowDetails = () => {
    this.setState(
      prevState => {
        return { showDetails: !prevState.showDetails };
      },
      () => {
        const { onToggleDetail } = this.props;
        if (onToggleDetail) {
          onToggleDetail();
        }
      }
    );
  };
}

export const AppointmentChangeLogView = withTheme(
  withAppointmentContext(
    withI18nContext(OriginAppointmentChangeLogView, {
      namespace: NAMESPACE_APPOINTMENT_CHANGE_LOG_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
