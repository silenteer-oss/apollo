import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { AppointmentChangeLogView as OriginAppointmentChangeLogView } from './AppointmentChangeLogView';
import { IAppoinmentChangeLogViewProps } from './models';

export const AppointmentChangeLogView = styled(
  OriginAppointmentChangeLogView
).attrs(({ className }) => ({
  className: getCssClass('sl-AppointmentChangeLogView', className),
}))<IAppoinmentChangeLogViewProps>`
  /* stylelint-disable */
`;
