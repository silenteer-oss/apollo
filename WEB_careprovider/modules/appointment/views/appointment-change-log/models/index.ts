import { ProfileApiModel } from '@infrastructure/resource/ProfileResource';
import { IEmployeeProfile, IPatientProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { RepeatUnit, RecurrenceType } from '../../appointment-details';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';

export interface IAppoinmentChangeLogViewProps {
  id: string;
  className?: string;
  theme?: ICareProviderTheme;
  changeMakerId: string;
  action: AppointmentApiModel.ActionType;
  startDateTime: number;
  hourAndMinuteSpecified: boolean;
  doctorIds?: string[];
  note?: string;
  internalNote?: string;
  reason?: string;
  cancelReason?: string;
  departmentName?: string;
  onToggleDetail?: () => void;
  editedFields?: AppointmentApiModel.AppointmentFields[];
  patientProfile?: IPatientProfile;
}

export interface IAppoinmentChangeLogViewState {
  showDetails: boolean;
  doctors: { [key: string]: IEmployeeProfile };
  nurses: { [key: string]: IEmployeeProfile };
  receptionists: { [key: string]: IEmployeeProfile };
  patientProfile: ProfileApiModel.PatientProfileResponse;
}

export interface Recurrence {
  repeatInterval: number;
  repeatUnit: RepeatUnit;
  numberOccurrence: number;
  type: RecurrenceType;
  additionalStartTimes: number[];
}
