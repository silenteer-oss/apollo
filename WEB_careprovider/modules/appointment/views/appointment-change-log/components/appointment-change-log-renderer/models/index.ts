import { ICareProviderTheme } from '@careprovider/theme';
import { IPatientProfile } from '@careprovider/services/biz';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';

export interface IAppointmentChangeLogRendererProps {
  className?: string;
  theme?: ICareProviderTheme;
  showDetails: boolean;
  changeMakerName: string;
  action: AppointmentApiModel.ActionType;
  startDateTime: number;
  hourAndMinuteSpecified: boolean;
  doctorName?: string;
  note?: string;
  reason?: string;
  cancelReason?: string;
  departmentName?: string;
  editedFields?: AppointmentApiModel.AppointmentFields[];
  patientProfile?: IPatientProfile;
}
