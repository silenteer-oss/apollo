import { styled } from '@careprovider/theme';
import { getCssClass } from '@design-system/infrastructure/utils';
import { AppointmentChangeLogRenderer as OriginAppointmentChangeLogRenderer } from './AppointmentChangeLogRenderer';
import { IAppointmentChangeLogRendererProps } from './models';

export const AppointmentChangeLogRenderer = styled(
  OriginAppointmentChangeLogRenderer
).attrs(({ className }) => ({
  className: getCssClass('sl-AppointmentChangeLogRenderer', className),
}))<IAppointmentChangeLogRendererProps>`
  ${props => {
    const { foreground } = props.theme;
    return `
    & {
        .CONFIRM {
          color: ${foreground.success.base};
        }
        .CANCEL {
          color: ${foreground.error.base};
        }
        .details-line {
          flex-wrap: wrap;
          justify-content: center;
          .highlight {
            color: ${foreground.warn.base};
          }
        }
      }`;
  }}
`;
