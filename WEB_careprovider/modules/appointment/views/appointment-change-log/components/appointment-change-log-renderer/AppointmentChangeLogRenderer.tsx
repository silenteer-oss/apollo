import React from 'react';
import { Flex, BodyTextM } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import { withTheme } from '@careprovider/theme';
import { dateTimeFormat } from '@careprovider/services/util';
import { getModuleConfig } from '@careprovider/modules/appointment/module-config';
import { IAppointmentChangeLogRendererProps } from './models';
import { NAMESPACE_APPOINTMENT_CHANGE_LOG_RENDERER, i18nKeys } from './i18n';

class OriginAppointmentChangeLogRenderer extends React.PureComponent<
  IAppointmentChangeLogRendererProps & II18nFixedNamespaceContext
  > {
  render() {
    const {
      className,
      showDetails,
      changeMakerName,
      startDateTime,
      hourAndMinuteSpecified,
      action,
      doctorName,
      note,
      reason,
      cancelReason,
      departmentName,
      editedFields = [],
      t,
    } = this.props;

    const headLine = (
      <div>
        <BodyTextM
          fontWeight="SemiBold"
          className={this.getActionStyleClass(action)}
        >
          {`${changeMakerName} ${this.getActionText(action, editedFields)}`}
        </BodyTextM>
      </div>
    );

    if (showDetails) {
      const departmentNameLine = departmentName ? (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.Department
              )
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.departmentLabel)}:&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.Department
              )
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {departmentName}
          </BodyTextM>
        </Flex>
      ) : null;

      const reasonLine = reason ? (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.Reason
              )
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.reasonLabel)}&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.Reason
              )
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {reason}
          </BodyTextM>
        </Flex>
      ) : null;

      const cancelReasonLine = cancelReason ? (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.Reason
              )
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.cancelReasonLabel)}&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.CancelReason
              )
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {cancelReason}
          </BodyTextM>
        </Flex>
      ) : null;

      const timeLine = (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.StartDateTime
              )
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.timeLabel)}&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.StartDateTime
              )
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {dateTimeFormat(
              startDateTime,
              hourAndMinuteSpecified ? 'L - LT' : 'L'
            )}
          </BodyTextM>
        </Flex>
      );

      const doctorLine = doctorName ? (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.DoctorIds
              )
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.doctorLabel)}&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(
                AppointmentApiModel.AppointmentFields.DoctorIds
              )
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {doctorName}
          </BodyTextM>
        </Flex>
      ) : null;

      const noteLine = note ? (
        <Flex className="details-line">
          <BodyTextM
            className={
              editedFields.includes(AppointmentApiModel.AppointmentFields.Note)
                ? 'highlight'
                : undefined
            }
            as="span"
          >
            {t(i18nKeys.noteLabel)}&nbsp;
          </BodyTextM>
          <BodyTextM
            className={
              editedFields.includes(AppointmentApiModel.AppointmentFields.Note)
                ? 'highlight'
                : undefined
            }
            as="span"
            fontWeight="SemiBold"
          >
            {note}
          </BodyTextM>
        </Flex>
      ) : null;

      return (
        <Flex align="center" className={className} column>
          {headLine}
          {departmentNameLine}
          {reasonLine}
          {cancelReasonLine}
          {timeLine}
          {doctorLine}
          {noteLine}
        </Flex>
      );
    } else {
      return (
        <Flex className={className} column>
          {headLine}
        </Flex>
      );
    }
  }

  getActionText = (
    action: AppointmentApiModel.ActionType,
    editedFields: AppointmentApiModel.AppointmentFields[]
  ): string => {
    const { t } = this.props;

    switch (action) {
      case AppointmentApiModel.ActionType.CREATE:
        return t(i18nKeys.scheduledMessage);
      case AppointmentApiModel.ActionType.UPDATE:
        return editedFields &&
          editedFields.includes(
            AppointmentApiModel.AppointmentFields.StartDateTime
          )
          ? t(i18nKeys.rescheduledMessage)
          : t(i18nKeys.updatedMessage);
      case AppointmentApiModel.ActionType.CANCEL:
        return t(i18nKeys.cancelledMessage);
      case AppointmentApiModel.ActionType.CONFIRM:
        return t(i18nKeys.confirmedMessage);
      default:
        throw new Error('not yet supported');
    }
  };

  getActionStyleClass = (action: AppointmentApiModel.ActionType): string => {
    switch (action) {
      case AppointmentApiModel.ActionType.CREATE:
        return '';
      case AppointmentApiModel.ActionType.UPDATE:
        return '';
      case AppointmentApiModel.ActionType.CANCEL:
        return AppointmentApiModel.ActionType.CANCEL;
      case AppointmentApiModel.ActionType.CONFIRM:
        return AppointmentApiModel.ActionType.CONFIRM;
      default:
        throw new Error('not yet supported');
    }
  };
}

export const AppointmentChangeLogRenderer = withTheme(
  withI18nContext(OriginAppointmentChangeLogRenderer, {
    namespace: NAMESPACE_APPOINTMENT_CHANGE_LOG_RENDERER,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
