import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-change-log-renderer.json';

export const NAMESPACE_APPOINTMENT_CHANGE_LOG_RENDERER =
  'appointment-change-log-renderer';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
