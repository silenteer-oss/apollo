import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { AppointmentDetailsView as OriginAppointmentDetailsView } from './AppointmentDetailsView';
import { IAppointmentDetailsViewProps } from './models';

export const AppointmentDetailsView = styled(
  OriginAppointmentDetailsView
).attrs(({ className }) => ({
  className: getCssClass('sl-AppointmentDetailsView', className),
}))<IAppointmentDetailsViewProps>`
  ${props => {
    const { space } = props.theme;
    return `
    & {
        .btn-set-appointment {
          padding: ${space.xs} ${space.s};
        }

      }`;
  }}
`;
