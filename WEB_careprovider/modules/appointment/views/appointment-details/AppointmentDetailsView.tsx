import React from 'react';
import { Button, Intent, H4 } from '@blueprintjs/core';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import { Flex } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { utc } from '@careprovider/services/util';
import { getModuleConfig } from '../../module-config';
import { ModuleToaster } from '../../components';
import AppointmentService from '../../services/AppointmentService';
import { IAppointmentDialogValues, AppointmentDialog } from './components';
import {
  IAppointmentDetailsViewProps,
  IAppointmentDetailsViewState,
} from './models';
import { NAMESPACE_APPOINTMENT_DETAILS_VIEW, i18nKeys } from './i18n';

class OriginAppointmentDetailsView extends React.Component<
  IAppointmentDetailsViewProps & II18nFixedNamespaceContext,
  IAppointmentDetailsViewState
  > {
  state: IAppointmentDetailsViewState = {
    isOpenAppointmentDialog: false,
  };

  render() {
    const { className, buttonLabel, t, icon } = this.props;

    return (
      <Flex className={className}>
        {icon ? (
          <Button
            onClick={this.openAppointmentDialog}
            icon={icon}
            minimal
            small
          />
        ) : (
            <Button
              className="btn-set-appointment"
              intent={Intent.PRIMARY}
              onClick={this.openAppointmentDialog}
            >
              {buttonLabel || t(i18nKeys.setAppointmentButton)}
            </Button>
          )}
        <AppointmentDialog
          {...this.props}
          isOpen={this.state.isOpenAppointmentDialog}
          handleClose={this.handleCloseAppointmentDialog}
          handleSubmit={this.handleSubmitAppointment}
          isEditingMode={this.props.appointmentId ? true : false}
        />
      </Flex>
    );
  }

  openAppointmentDialog = () =>
    this.setState({ isOpenAppointmentDialog: true });

  handleCloseAppointmentDialog = () =>
    this.setState({ isOpenAppointmentDialog: false });

  handleSubmitAppointment = (data: IAppointmentDialogValues) => {
    return this.props.appointmentId
      ? this.handleEditAppointment(this.props.appointmentId, data)
      : this.handleCreateAppointment(data);
  };

  handleCreateAppointment = (data: IAppointmentDialogValues) => {
    const { t } = this.props;
    const payload: AppointmentApiModel.AppointmentRequest = {
      repeatInterval: undefined,
      numberOccurrence: undefined,
      additionalStartTimes: undefined,
      ...data,
      doctorIds: data.doctorId ? [data.doctorId] : undefined,
      nurseIds: data.nurseId ? [data.nurseId] : undefined,
      // currently only support never-recurrence Appointment
      startDateTime: utc(data.appointmentDateTime.value),
      hourAndMinuteSpecified: data.appointmentDateTime.hourAndMinuteSpecified,
    };
    return AppointmentService.createAppointment(payload).then(() => {
      this.handleCloseAppointmentDialog();
      ModuleToaster.show({
        intent: Intent.SUCCESS,
        message: t(i18nKeys.setAppointmentSuccessfullyMessage),
      });
      if (this.props.onSubmit) {
        this.props.onSubmit();
      }
    });
  };

  handleEditAppointment = (
    appointmentId: string,
    data: IAppointmentDialogValues
  ) => {
    const { t } = this.props;
    const payload: AppointmentApiModel.AppointmentRequest = {
      repeatInterval: undefined,
      numberOccurrence: undefined,
      additionalStartTimes: undefined,
      ...data,
      doctorIds: data.doctorId ? [data.doctorId] : undefined,
      nurseIds: data.nurseId ? [data.nurseId] : undefined,
      startDateTime: utc(data.appointmentDateTime.value),
      hourAndMinuteSpecified: data.appointmentDateTime.hourAndMinuteSpecified,
    };
    return AppointmentService.editAppointment(appointmentId, payload).then(
      () => {
        this.handleCloseAppointmentDialog();
        ModuleToaster.show({
          intent: Intent.SUCCESS,
          message: t(i18nKeys.editAppointmentSuccessfullyMessage),
        });
        if (this.props.onSubmit) {
          this.props.onSubmit();
        }
      }
    );
  };
}

export const AppointmentDetailsView = withTheme(
  withI18nContext(OriginAppointmentDetailsView, {
    namespace: NAMESPACE_APPOINTMENT_DETAILS_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
