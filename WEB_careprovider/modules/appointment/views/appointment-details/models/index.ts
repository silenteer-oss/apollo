import { ICareProviderTheme } from '@careprovider/theme';

export interface IAppointmentDetailsViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId?: string;
  doctorIds?: string[];
  nurseIds?: string[];
  buttonLabel?: string;
  department?: string;
  reason?: string;
  recurrenceType?: RecurrenceType;
  appointmentDateTime?: IAppointmentDateTime;
  note?: string;
  internalNote?: string;
  reminderValue?: number;
  reminderUnit?: ReminderUnit;
  appointmentId?: string;
  icon?: React.ReactElement;
  onSubmit?: () => void;
}

export interface IAppointmentDetailsViewState {
  isOpenAppointmentDialog: boolean;
}

export interface IAppointmentDateTime {
  value: Date;
  hourAndMinuteSpecified: boolean;
}

export enum RecurrenceType {
  NEVER = 'NEVER',
  CUSTOM = 'CUSTOM',
  REPEAT = 'REPEAT',
}

export enum RepeatUnit {
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
  YEAR = 'YEAR',
}

export enum ReminderUnit {
  HOUR = 'HOUR',
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
}
