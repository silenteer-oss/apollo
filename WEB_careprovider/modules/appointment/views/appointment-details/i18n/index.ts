import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-details-view.json';

export const NAMESPACE_APPOINTMENT_DETAILS_VIEW = 'appointment-details-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
