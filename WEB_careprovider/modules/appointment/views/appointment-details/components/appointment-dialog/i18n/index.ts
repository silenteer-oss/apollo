import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-dialog.json';

export const NAMESPACE_APPOINTMENT_DIALOG = 'appointment-dialog';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
