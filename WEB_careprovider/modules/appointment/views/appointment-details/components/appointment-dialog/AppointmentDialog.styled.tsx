import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { scaleSpacePx } from '@design-system/core/styles';
import { IAppointmentDialogProps } from './models';
import { AppointmentDialog as OriginAppointmentDialog } from './AppointmentDialog';

export const AppointmentDialog = styled(OriginAppointmentDialog).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppointmentDialog', className),
  })
)<IAppointmentDialogProps>`
  ${props => {
    const { space } = props.theme;
    return `
  & {
      .sl-date-group {
        width: ${scaleSpacePx(75)};
      }

      .sl-time-group {
        margin-left: ${space.s};
        .sl-minute-select {
          margin-left: ${space.xs};
        }
      }
    }`;
  }}
`;
