export function debounced(delay: number, originalFunc) {
  let timerId;

  return function(...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      originalFunc(...args);
      timerId = null;
    }, delay);
  };
}
