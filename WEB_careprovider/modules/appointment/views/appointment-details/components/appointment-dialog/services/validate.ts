import i18next, { TFunction } from 'i18next';
import moment from 'moment';
import { NAMESPACE_APPOINTMENT_DIALOG, i18nKeys } from '../i18n';

export function required(value) {
  if (_isEmpty(value)) {
    const _t = _getFixedT();
    return _t(i18nKeys.required);
  }
  return null;
}

export function validDate(value) {
  if (!_isEmpty(value) && !moment(value).isValid()) {
    const _t = _getFixedT();
    return _t(i18nKeys.invalidDate);
  }
  return null;
}

export function validateStringLength(min: number, max: number) {
  return (value: string) => {
    const _t = _getFixedT();

    if (value === undefined || value === null) {
      if (min === 0) {
        return null;
      } else {
        return _t(i18nKeys.invalidShortString, { min });
      }
    }

    if (value.length < min) {
      return _t(i18nKeys.invalidShortString, { min });
    }

    if (value.length > max) {
      return _t(i18nKeys.invalidLongString, { max });
    }

    return null;
  };
}

// -------------------------------------------- //

function _getFixedT(): TFunction {
  return i18next.getFixedT(i18next.language, NAMESPACE_APPOINTMENT_DIALOG);
}

function _isEmpty(value: any): boolean {
  return (
    value === undefined || value === null || value === '' || value.length === 0
  );
}
