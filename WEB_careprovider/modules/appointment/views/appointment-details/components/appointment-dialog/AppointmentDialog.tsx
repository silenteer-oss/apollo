import React from 'react';
// Include the locale utils designed for moment
import MomentLocaleUtils from 'react-day-picker/moment';
import {
  Formik,
  Form,
  Field,
  FormikActions,
  FormikProps,
  FieldProps,
} from 'formik';
import {
  Classes,
  Dialog,
  InputGroup,
  Button,
  Intent,
  FormGroup,
  MenuItem,
  Icon,
  Alignment,
} from '@blueprintjs/core';
import { DateInput } from '@blueprintjs/datetime';
import {
  Select,
  Suggest,
  ItemRenderer,
  ItemPredicate,
  IItemRendererProps,
} from '@blueprintjs/select';
import { IconNames } from '@blueprintjs/icons';
import { Flex } from '@design-system/core/components';
import {
  withI18nContext,
  II18nFixedNamespaceContext,
} from '@design-system/i18n/context';
import { getCssClass } from '@design-system/infrastructure/utils';
import { ProfileApiModel } from '@infrastructure/resource/ProfileResource';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import { withTheme } from '@careprovider/theme';
import { IEmployeeProfile, UserType } from '@careprovider/services/biz';
import {
  dateTimeFormat,
  endOf,
  dateToMoment,
  isBefore,
  startOf,
  now,
} from '@careprovider/services/util';
import { getModuleConfig } from '../../../../module-config';
import {
  withAppointmentContext,
  IAppointmentContext,
} from '../../../../context';
import AppointmentService from '../../../../services/AppointmentService';
import { IAppointmentDateTime, ReminderUnit } from '../../models';
import { debounced, required, validateStringLength } from './services';
import {
  IAppointmentDialogProps,
  IAppointmentDialogValues,
  IAppointmentDialogState,
} from './models';
import { NAMESPACE_APPOINTMENT_DIALOG, i18nKeys } from './i18n';

const DATE_INPUT_CONFIG = {
  formatDate: (date: Date) => dateTimeFormat(date, 'DD/MM/YYYY'),
  parseDate: (str: string) => dateToMoment(str, 'DD/MM/YYYY').toDate(),
  placeholder: 'dd.mm.yyyy',
  locale: 'vi',
  localeUtils: MomentLocaleUtils,
};

const EmployeeSelect = Select.ofType<IEmployeeProfile>();
const PLACEHOLDER_DOCTOR: IEmployeeProfile = {
  id: undefined,
  fullName: '',
  dob: undefined,
  gender: undefined,
  phone: undefined,
  email: undefined,
  address: undefined,
  accountId: undefined,
  careProvideId: undefined,
  type: UserType.DOCTOR,
};
const PLACEHOLDER_NURSE = {
  id: undefined,
  fullName: '',
  dob: undefined,
  gender: undefined,
  phone: undefined,
  email: undefined,
  address: undefined,
  accountId: undefined,
  careProvideId: undefined,
  type: UserType.NURSE,
};

const TimeSelect = Select.ofType<string>();
const HOUR_07 = '7';
const HOURS: string[] = [
  '',
  HOUR_07,
  '8',
  '9',
  '10',
  '11',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
];

const MINUTE_OO = '0';
const MINUTES: string[] = ['', MINUTE_OO, '15', '30', '45'];

class OriginAppointmentDialog extends React.PureComponent<
  IAppointmentDialogProps & IAppointmentContext & II18nFixedNamespaceContext,
  IAppointmentDialogState
  > {
  state: IAppointmentDialogState = {
    needLoadReferenceDataFromContext: true,
    patients: [],
    doctors: [],
    nurses: [],
    doctorById: {},
    nurseById: {},
  };

  componentDidUpdate() {
    this.setReferenceDataToState();
  }

  render() {
    const { className, isOpen, isEditingMode, t } = this.props;

    if (!this.state.doctors || !this.state.nurses) {
      return (
        <Dialog
          className={getCssClass(className, 'bp3-dialog-fullscreen')}
          canEscapeKeyClose={false}
          isOpen={isOpen}
        >
          ...Loading
        </Dialog>
      );
    }

    const title = isEditingMode
      ? t(i18nKeys.editAppointmentTitle)
      : t(i18nKeys.setAppointmentTitle);

    const {
      patientId,
      doctorIds = [],
      nurseIds = [],
      handleClose,
      department = '',
      reason = '',
      recurrenceType = AppointmentApiModel.RecurrenceType.NEVER,
      appointmentDateTime = {
        value: endOf(+now(), 'day').toDate(),
        hourAndMinuteSpecified: false,
      },
      note = '',
      internalNote = '',
      reminderUnit = AppointmentApiModel.ReminderUnit.DAY,
      reminderValue = 1,
    } = this.props;

    const normalizedAppointmentDateTime = this.normalizeAppointmentDateTime(
      appointmentDateTime
    );

    const initialValues: IAppointmentDialogValues = {
      patientId: patientId,
      department: department,
      reason: reason,
      // backend supports multiple doctors for appointment but current frontend will have limitation of single doctor
      doctorId: doctorIds[0],
      // backend supports multiple nurses for appointment but current frontend will have limitation of single nurse
      nurseId: nurseIds[0],
      recurrenceType: recurrenceType,
      appointmentDateTime: normalizedAppointmentDateTime,
      repeatValue: null,
      repeatUnit: null,
      note: note,
      internalNote: internalNote,
      reminderUnit: reminderUnit,
      reminderValue: reminderValue,
      occurrenceOfRepeat: undefined,
    };

    const btnLabel = isEditingMode
      ? t(i18nKeys.editAppointmentButton)
      : t(i18nKeys.setAppointmentButton);

    return (
      <Dialog
        className={getCssClass(
          className,
          'bp3-dialog-fullscreen',
          'bp3-dialog-content-scrollable'
        )}
        canEscapeKeyClose={false}
        onClose={handleClose}
        isOpen={isOpen}
        title={title}
      >
        <Formik
          initialValues={initialValues}
          onSubmit={this.onSubmit}
          render={({
            isSubmitting,
            dirty,
            values,
            handleSubmit,
          }: FormikProps<IAppointmentDialogValues>) => (
              <>
                <Form className={Classes.DIALOG_BODY}>
                  <div style={{ margin: 'auto', width: '50%' }}>
                    {this.getPatientField()}
                    <Field
                      name="reason"
                      validate={(value: string) => {
                        const requiredError = required(value);
                        if (requiredError) {
                          return requiredError;
                        }
                        const lengthError = validateStringLength(0, 250)(value);
                        return lengthError;
                      }}
                    >
                      {({
                        field,
                        form,
                      }: FieldProps<IAppointmentDialogValues>) => {
                        const errorMessage = form.errors && form.errors['reason'];
                        return (
                          <FormGroup
                            label={t(i18nKeys.reasonLabel)}
                            helperText={errorMessage}
                            className={errorMessage ? Classes.INTENT_DANGER : ''}
                          >
                            <InputGroup
                              className={
                                errorMessage ? Classes.INTENT_DANGER : ''
                              }
                              {...field}
                            />
                          </FormGroup>
                        );
                      }}
                    </Field>
                    {this.getAppointmentDateField(values)}
                    {this.getDoctorsField()}
                    {this.getNursesField()}
                    <Field name="note" validate={validateStringLength(0, 250)}>
                      {({
                        field,
                        form,
                      }: FieldProps<IAppointmentDialogValues>) => {
                        const errorMessage = form.errors && form.errors['note'];
                        return (
                          <FormGroup
                            label={t(i18nKeys.noteLabel)}
                            labelInfo={t(i18nKeys.optionalLabel)}
                            helperText={errorMessage}
                            className={errorMessage ? Classes.INTENT_DANGER : ''}
                          >
                            <InputGroup {...field} />
                          </FormGroup>
                        );
                      }}
                    </Field>
                    <Field
                      name="internalNote"
                      validate={validateStringLength(0, 250)}
                    >
                      {({
                        field,
                        form,
                      }: FieldProps<IAppointmentDialogValues>) => {
                        const errorMessage =
                          form.errors && form.errors['internalNote'];
                        return (
                          <FormGroup
                            label={t(i18nKeys.internalNoteLabel)}
                            labelInfo={t(i18nKeys.optionalLabel)}
                            helperText={errorMessage}
                            className={errorMessage ? Classes.INTENT_DANGER : ''}
                          >
                            <InputGroup {...field} />
                          </FormGroup>
                        );
                      }}
                    </Field>
                  </div>
                </Form>
                <Flex className={Classes.DIALOG_FOOTER}>
                  <div style={{ margin: 'auto', width: '50%' }}>
                    <Button
                      intent={Intent.PRIMARY}
                      disabled={!dirty || isSubmitting}
                      loading={isSubmitting}
                      fill
                      onClick={() => handleSubmit()}
                    >
                      {btnLabel}
                    </Button>
                  </div>
                </Flex>
              </>
            )}
        />
      </Dialog>
    );
  }

  normalizeAppointmentDateTime = (
    appointmentDateTime: IAppointmentDateTime
  ): IAppointmentDateTime => {
    const momentDateTime = dateToMoment(appointmentDateTime.value);

    if (appointmentDateTime.hourAndMinuteSpecified) {
      const hourString = momentDateTime.hour().toString();
      const minuteString = momentDateTime.minute().toString();
      if (!HOURS.includes(hourString) || !MINUTES.includes(minuteString)) {
        // not a valid hour or minute
        // then ignore that, reset to end of day
        return {
          hourAndMinuteSpecified: false,
          value: momentDateTime.endOf('day').toDate(),
        };
      }
      return {
        hourAndMinuteSpecified: true,
        value: momentDateTime.toDate(),
      };
    } else {
      return {
        hourAndMinuteSpecified: false,
        value: momentDateTime.endOf('day').toDate(),
      };
    }
  };

  getPatientField = () => {
    const { t } = this.props;

    return (
      <Field
        name="patientId"
        validate={required}
        render={({ field, form }: FieldProps<IAppointmentDialogValues>) => {
          const onSelect = (item: ProfileApiModel.PatientProfileResponse) => {
            const { setFieldValue } = form;
            setFieldValue(field.name, item.id);
          };

          let selectedPatient;
          if (this.props.patientId && this.state.patients) {
            selectedPatient = this.state.patients.find(
              p => p.id === this.props.patientId
            );
          }

          const errorMessage = form.errors && form.errors['patientId'];

          return (
            <FormGroup
              label={t(i18nKeys.patientLabel)}
              helperText={errorMessage}
              className={errorMessage ? Classes.INTENT_DANGER : ''}
            >
              <Suggest // TODO use custom Suggest to override input renderer
                fill
                items={this.state.patients}
                onItemSelect={onSelect}
                inputValueRenderer={this.patientInputValueRenderer}
                itemRenderer={this.patientItemRenderer}
                itemPredicate={this.patientItemPredicate}
                popoverProps={{ minimal: true, captureDismiss: true }}
                onQueryChange={debounced(200, this.onQueryNameChange)}
                selectedItem={selectedPatient}
                disabled={
                  this.props.isEditingMode || this.props.patientId !== undefined
                }
                inputProps={{
                  className: errorMessage
                    ? `${Classes.INTENT_DANGER} cy-patientName-input`
                    : 'cy-patientName-input',
                  placeholder: t(i18nKeys.patientPlaceholder),
                }}
              />
            </FormGroup>
          );
        }}
      />
    );
  };

  onQueryNameChange = async (query: string) => {
    if (query && query.length > 0) {
      const patientResults = await AppointmentService.searchPatient(query);
      this.setState({ patients: patientResults });
    }
  };

  getDoctorsField = () => {
    const { t } = this.props;

    return (
      <Field
        name="doctorId"
        render={({ field, form }: FieldProps<IAppointmentDialogValues>) => {
          const onSelect = (item: IEmployeeProfile) => {
            const { setFieldValue } = form;
            setFieldValue(field.name, item.id);
          };

          const selectedDoctor: IEmployeeProfile = field.value
            ? this.state.doctorById[field.value]
            : undefined;

          return (
            <FormGroup
              label={t(i18nKeys.doctorLabel)}
              labelInfo={t(i18nKeys.optionalLabel)}
            >
              <EmployeeSelect
                className="sl-select cy-doctor-select"
                items={[PLACEHOLDER_DOCTOR, ...this.state.doctors]}
                activeItem={selectedDoctor}
                onItemSelect={onSelect}
                itemRenderer={this.employeeRenderer}
                itemPredicate={this.employeeItemPredicate}
                popoverProps={{
                  minimal: true,
                  captureDismiss: true,
                  fill: true,
                }}
              >
                <Button
                  fill
                  alignText={Alignment.LEFT}
                  text={
                    selectedDoctor
                      ? selectedDoctor.fullName
                      : t(i18nKeys.doctorPlaceholder)
                  }
                  rightIcon={<Icon icon="caret-down" />}
                />
              </EmployeeSelect>
            </FormGroup>
          );
        }}
      />
    );
  };

  getNursesField = () => {
    const { t } = this.props;

    return (
      <Field
        name="nurseId"
        render={({ field, form }: FieldProps<IAppointmentDialogValues>) => {
          const onSelect = (item: IEmployeeProfile) => {
            const { setFieldValue } = form;
            setFieldValue(field.name, item.id);
          };

          const selectedNurse: IEmployeeProfile = field.value
            ? this.state.nurseById[field.value]
            : undefined;
          return (
            <FormGroup
              label={t(i18nKeys.nurseLabel)}
              labelInfo={t(i18nKeys.optionalLabel)}
            >
              <EmployeeSelect
                className="sl-select cy-nurse-select"
                items={[PLACEHOLDER_NURSE, ...this.state.nurses]}
                activeItem={selectedNurse}
                onItemSelect={onSelect}
                itemRenderer={this.employeeRenderer}
                itemPredicate={this.employeeItemPredicate}
                popoverProps={{
                  minimal: true,
                  captureDismiss: true,
                  fill: true,
                }}
              >
                <Button
                  fill
                  alignText={Alignment.LEFT}
                  text={
                    selectedNurse
                      ? selectedNurse.fullName
                      : t(i18nKeys.nursePlaceholder)
                  }
                  rightIcon={<Icon icon="caret-down" intent="primary" />}
                />
              </EmployeeSelect>
            </FormGroup>
          );
        }}
      />
    );
  };

  getAppointmentDateField = (values: IAppointmentDialogValues) => {
    const { isEditingMode, t } = this.props;

    return (
      <Flex>
        <Field
          name={`appointmentDateTime`}
          validate={this.validateAppointmentDate}
          render={({ field, form }: FieldProps<IAppointmentDialogValues>) => {
            const { setFieldValue } = form;

            const {
              value: selectedDateTime,
              hourAndMinuteSpecified,
            }: IAppointmentDateTime = field.value;

            const currentDateTime = selectedDateTime
              ? dateToMoment(selectedDateTime)
              : undefined;
            const currentHourString = hourAndMinuteSpecified
              ? currentDateTime.hour().toString()
              : t(i18nKeys.hourPlaceholder);
            const currentMinuteString = hourAndMinuteSpecified
              ? currentDateTime.minute().toString()
              : t(i18nKeys.minutePlaceholder);

            const onDateChange = (newDate: Date, isUserChange: boolean) => {
              console.log('//// LOG onDateChange', newDate, isUserChange);
              if (isUserChange && newDate) {
                const newDateTime = endOf(newDate, 'day').clone();
                if (hourAndMinuteSpecified) {
                  newDateTime
                    .hour(currentDateTime.hour()) // keep previously specified hour if any
                    .minute(currentDateTime.minute()); // keep previously specified minute if any
                }
                setFieldValue(field.name, {
                  value: newDateTime.toDate(),
                  hourAndMinuteSpecified, // keep as is
                });
              } else if (isUserChange) {
                setFieldValue(field.name, {
                  value: undefined,
                  hourAndMinuteSpecified: false,
                });
              }
            };

            const onHourChange = (selectedHour: string) => {
              console.log('//// LOG onHourChange', selectedHour);
              if (
                // if unselect hour
                selectedHour === t(i18nKeys.hourPlaceholder)
              ) {
                // reset the time part to non-specified
                setFieldValue(field.name, {
                  value: currentDateTime.endOf('day').toDate(),
                  hourAndMinuteSpecified: false,
                });
              } else {
                const newDateTime = currentDateTime.hour(
                  Number.parseInt(selectedHour)
                );
                if (currentMinuteString === t(i18nKeys.minutePlaceholder)) {
                  newDateTime.minute(Number.parseInt(MINUTE_OO));
                } else {
                  newDateTime.minute(Number.parseInt(currentMinuteString));
                }
                setFieldValue(field.name, {
                  value: newDateTime.toDate(),
                  hourAndMinuteSpecified: true,
                });
              }
            };

            const onMinuteChange = (selecteMinute: string) => {
              console.log('//// LOG onMinuteChange', selecteMinute);
              if (
                // if unselect minute
                selecteMinute === t(i18nKeys.minutePlaceholder)
              ) {
                // reset the time part to non-specified
                setFieldValue(field.name, {
                  value: currentDateTime.endOf('day').toDate(),
                  hourAndMinuteSpecified: false,
                });
              } else {
                const newDateTime = currentDateTime.minute(
                  Number.parseInt(selecteMinute)
                );
                if (currentHourString === t(i18nKeys.hourPlaceholder)) {
                  newDateTime.hour(Number.parseInt(HOUR_07));
                } else {
                  newDateTime.hour(Number.parseInt(currentHourString));
                }
                setFieldValue(field.name, {
                  value: newDateTime.toDate(),
                  hourAndMinuteSpecified: true,
                });
              }
            };

            const dateInNextYear = new Date();
            dateInNextYear.setDate(new Date().getDate() + 365);
            const errorMessage = form.errors && form.errors.appointmentDateTime;
            const showErrorOnTimeGroup =
              errorMessage && errorMessage !== t(i18nKeys.required);
            return (
              <>
                <FormGroup
                  label={t(i18nKeys.dateLabel)}
                  helperText={errorMessage}
                  className={`sl-date-group ${
                    errorMessage ? Classes.INTENT_DANGER : ''
                    }`}
                >
                  <DateInput
                    inputProps={{
                      leftIcon: IconNames.CALENDAR,
                      className: errorMessage ? Classes.INTENT_DANGER : '',
                    }}
                    {...DATE_INPUT_CONFIG}
                    value={selectedDateTime}
                    onChange={onDateChange}
                    className="cy-appt-date-picker"
                    minDate={isEditingMode ? undefined : new Date()}
                    maxDate={dateInNextYear}
                  />
                </FormGroup>
                <FormGroup
                  label={t(i18nKeys.timeLabel)}
                  labelInfo={t(i18nKeys.optionalLabel)}
                  disabled={!currentDateTime}
                  className={`sl-time-group ${
                    showErrorOnTimeGroup ? Classes.INTENT_DANGER : ''
                    }`}
                >
                  <Flex>
                    <TimeSelect
                      className={`sl-select ${
                        showErrorOnTimeGroup ? Classes.INTENT_DANGER : ''
                        }`}
                      disabled={!currentDateTime}
                      filterable={false}
                      items={HOURS}
                      activeItem={currentHourString}
                      onItemSelect={onHourChange}
                      itemRenderer={this.hourOrMinuteRenderer}
                      popoverProps={{
                        minimal: true,
                        captureDismiss: true,
                        fill: true,
                      }}
                    >
                      <Button
                        disabled={!currentDateTime}
                        fill
                        alignText={Alignment.LEFT}
                        text={currentHourString}
                        rightIcon={<Icon icon="caret-down" intent="primary" />}
                      />
                    </TimeSelect>
                    <TimeSelect
                      className={`sl-select sl-minute-select ${
                        showErrorOnTimeGroup ? Classes.INTENT_DANGER : ''
                        }`}
                      disabled={!currentDateTime}
                      filterable={false}
                      items={MINUTES}
                      activeItem={currentMinuteString}
                      onItemSelect={onMinuteChange}
                      itemRenderer={this.hourOrMinuteRenderer}
                      popoverProps={{
                        minimal: true,
                        captureDismiss: true,
                        fill: true,
                      }}
                    >
                      <Button
                        disabled={!currentDateTime}
                        fill
                        alignText={Alignment.LEFT}
                        text={currentMinuteString}
                        rightIcon={<Icon icon="caret-down" intent="primary" />}
                      />
                    </TimeSelect>
                  </Flex>
                </FormGroup>
              </>
            );
          }}
        />
      </Flex>
    );
  };

  onSubmit = (
    values: IAppointmentDialogValues,
    formikBag: FormikActions<IAppointmentDialogValues>
  ) => {
    const {
      appointmentDateTime: { hourAndMinuteSpecified },
    } = values;
    let { reminderValue, reminderUnit } = values;

    if (!hourAndMinuteSpecified) {
      reminderValue = 24 + 24 - 7;
      reminderUnit = ReminderUnit.HOUR;
    }
    this.props
      .handleSubmit({ ...values, reminderUnit, reminderValue })
      .then(() => formikBag.setSubmitting(false));
  };

  validateAppointmentDate = ({
    value,
    hourAndMinuteSpecified,
  }: IAppointmentDateTime): string => {
    let error = required(value);
    const { t } = this.props;

    if (!error) {
      if (hourAndMinuteSpecified) {
        if (isBefore(value, +startOf(+now(), 'minute'))) {
          error = t(i18nKeys.invalidPastDatetime);
        }
        if (error === null) {
          if (value.getDay() === 0) {
            if (
              value.getHours() < 7 ||
              value.getHours() > 12 ||
              (value.getHours() === 12 && value.getMinutes() > 0)
            ) {
              error = t(i18nKeys.outOfBoundOnSundayDatetime);
            }
          } else {
            if (
              value.getHours() < 7 ||
              value.getHours() > 19 ||
              (value.getHours() === 19 && value.getMinutes() > 0)
            ) {
              error = t(i18nKeys.outOfBoundDatetime);
            }
          }
        }
      } else {
        if (isBefore(value, +endOf(+now(), 'day'))) {
          error = t(i18nKeys.invalidPastDatetime);
        }
      }
    }

    return error;
  };

  hourOrMinuteRenderer = (
    value: string,
    { handleClick, modifiers }: IItemRendererProps
  ): JSX.Element => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        key={value}
        onClick={handleClick}
        text={value}
        shouldDismissPopover
        style={{ height: '20px' }}
      />
    );
  };

  employeeRenderer = (
    profile: IEmployeeProfile,
    { handleClick, modifiers }: IItemRendererProps
  ): JSX.Element => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        key={profile.id}
        onClick={handleClick}
        text={profile.fullName}
        shouldDismissPopover
        style={{ height: '20px' }}
      />
    );
  };

  employeeItemPredicate: ItemPredicate<IEmployeeProfile> = (
    query,
    item,
    _index,
    exactMatch
  ) => {
    const normalizedName = item.fullName.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
      return normalizedName === normalizedQuery;
    } else {
      return normalizedName.indexOf(normalizedQuery) > -1;
    }
  };

  patientInputValueRenderer = (
    identity: ProfileApiModel.PatientProfileResponse
  ) => {
    return `${identity.fullName}\u0020\u0020\u0020\u0020${identity.phone}`;
  };

  patientItemRenderer: ItemRenderer<ProfileApiModel.PatientProfileResponse> = (
    identity,
    { handleClick, modifiers }
  ) => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        key={identity.id}
        onClick={handleClick}
        text={identity.fullName}
        label={identity.phone}
      />
    );
  };

  patientItemPredicate: ItemPredicate<
    ProfileApiModel.PatientProfileResponse
  > = (query, item, _index, exactMatch) => {
    return true;
  };

  setReferenceDataToState = () => {
    // load doctors, nurses, patients reference data only on first time display the popup, i.e. 1st time isOpen is change to true
    const { isOpen, getDoctors, getNurses, patientId } = this.props;
    const { needLoadReferenceDataFromContext } = this.state;
    const needToGetPatientProfile =
      isOpen &&
      patientId &&
      (!this.state.patients ||
        this.state.patients.findIndex(p => p.id === patientId) < 0);

    const promises = [
      ...(needLoadReferenceDataFromContext
        ? [getDoctors(), getNurses()]
        : [Promise.resolve(undefined), Promise.resolve(undefined)]),
      needToGetPatientProfile
        ? AppointmentService.getPatientIdentitiesByIds([patientId])
        : Promise.resolve(undefined),
    ];

    const promiseResolveHandler: (
      results: [
        {
          dictionary: {
            [key: string]: IEmployeeProfile;
          };
          items: IEmployeeProfile[];
        },
        {
          dictionary: {
            [key: string]: IEmployeeProfile;
          };
          items: IEmployeeProfile[];
        },
        ProfileApiModel.PatientProfileResponse[]
      ]
    ) => void = ([doctors, nurses, patientIdentities]) => {
      if (!needLoadReferenceDataFromContext && !needToGetPatientProfile) {
        return;
      }
      let newState: IAppointmentDialogState = {
        ...this.state,
      };

      if (needLoadReferenceDataFromContext) {
        newState.needLoadReferenceDataFromContext = false;
        if (doctors) {
          newState.doctorById = doctors.dictionary;
          newState.doctors = doctors.items;
        }
        if (nurses) {
          newState.nurseById = nurses.dictionary;
          newState.nurses = nurses.items;
        }
      }
      if (needToGetPatientProfile) {
        newState.patients = patientIdentities;
      }

      this.setState(newState);
    };

    Promise.all(promises).then(promiseResolveHandler);
  };
}

export const AppointmentDialog = withTheme(
  withAppointmentContext(
    withI18nContext(OriginAppointmentDialog, {
      namespace: NAMESPACE_APPOINTMENT_DIALOG,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
