import { IEmployeeProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { ProfileApiModel } from '@infrastructure/resource/ProfileResource';
import {
  IAppointmentDateTime,
  RecurrenceType,
  RepeatUnit,
  ReminderUnit,
} from '../../../models';

export interface IAppointmentDialogProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId?: string;
  doctorIds?: string[];
  nurseIds?: string[];
  isOpen: boolean;
  handleClose: () => void;
  handleSubmit: (value: IAppointmentDialogValues) => Promise<void>;
  isEditingMode?: boolean;
  department?: string;
  reason?: string;
  recurrenceType?: RecurrenceType;
  appointmentDateTime?: IAppointmentDateTime;
  note?: string;
  internalNote?: string;
  reminderValue?: number;
  reminderUnit?: ReminderUnit;
}

export interface IAppointmentDialogState {
  needLoadReferenceDataFromContext: boolean;
  patients: ProfileApiModel.PatientProfileResponse[];
  doctors: IEmployeeProfile[];
  nurses: IEmployeeProfile[];
  doctorById: { [key: string]: IEmployeeProfile };
  nurseById: { [key: string]: IEmployeeProfile };
}

export interface IAppointmentDialogValues {
  patientId: string;
  department: string;
  reason: string;
  doctorId: string;
  nurseId: string;
  recurrenceType: RecurrenceType;
  appointmentDateTime: { value: Date; hourAndMinuteSpecified: boolean }; // local date
  repeatValue: number;
  repeatUnit: RepeatUnit;
  occurrenceOfRepeat: number; // if never ending repeat, will be -1
  reminderValue: number;
  reminderUnit: ReminderUnit;
  note: string;
  internalNote: string;
}
