import React from 'react';
import { Subscription } from 'rxjs';
import { Popover } from '@blueprintjs/core';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import { Box, Flex, LoadingState } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '@careprovider/modules/appointment/module-config';
import AppointmentService from '../../services/AppointmentService';
import { IAppointmentContext, withAppointmentContext } from '../../context';
import { AppointmentDetail } from '../../services/AppointmentServiceModel';
import {
  ColumnListViewProps,
  ColumnListViewState,
  DoctorAppointments,
} from './models';
import {
  DoctorInfoCard,
  AppointmentInfoCard,
  AppointmentPopover,
} from './components';
import { NAMESPACE_APPOINTMENT_COLUMN_LIST_VIEW, i18nKeys } from './i18n';

const UNASSIGNED_DOCTOR_ID = '-1';
let UNASSIGNED_NAME: string;
const INITIAL_STATE: ColumnListViewState = {
  appointments: {
    total: 0,
    doctorAppointments: [],
  },
  doctorById: {},
  nurseById: {},
  loading: true,
};

class OriginAppointmentColumnListView extends React.Component<
  ColumnListViewProps & IAppointmentContext & II18nFixedNamespaceContext,
  ColumnListViewState
  > {
  private isInitialized = false;
  private _appointmentChangeLogSubscription: Subscription;
  private _reconnectedSubscription: Subscription;

  get isFilteredByDoctor(): boolean {
    const { filteredDoctorIds = [], includeUnassigned } = this.props;
    return filteredDoctorIds.length !== 0 || includeUnassigned;
  }

  get isFitleredByStatus(): boolean {
    const { includePending, includeConfirmed } = this.props;

    return includePending || includeConfirmed;
  }

  state = INITIAL_STATE;

  shouldComponentUpdate(nextProps, nextState) {
    return this.props !== nextProps || this.state !== nextState;
  }

  componentDidMount() {
    this._reconnectedSubscription = this.props.reconnected$.subscribe(() =>
      this.loadAppointmentsGroupByDoctor()
    );

    this._appointmentChangeLogSubscription = this.props.appointmentChangeLog$.subscribe(
      message => {
        const { doctorIds, appointmentId, action } = message;
        if (
          action === AppointmentApiModel.ActionType.CONFIRM ||
          action === AppointmentApiModel.ActionType.CANCEL
        ) {
          this.updateDoctorAppointments(doctorIds, appointmentId, action);
        }
      }
    );
    this.setReferenceDataToState();
  }

  componentWillUnmount() {
    if (this._reconnectedSubscription) {
      this._reconnectedSubscription.unsubscribe();
      this._reconnectedSubscription = null;
    }
    if (this._appointmentChangeLogSubscription) {
      this._appointmentChangeLogSubscription.unsubscribe();
      this._appointmentChangeLogSubscription = null;
    }
  }

  componentDidUpdate(prevProps: ColumnListViewProps) {
    const {
      filteredDoctorIds: doctorIds,
      from,
      to,
      autoRefreshTime,
      includePending,
      includeConfirmed,
      includeUnassigned,
    } = this.props;

    const {
      filteredDoctorIds: prevDoctorIds,
      from: prevFrom,
      to: prevTo,
      autoRefreshTime: prevAutoRefreshTime,
      includePending: prevIncludePending,
      includeConfirmed: prevIncludeConfirmed,
      includeUnassigned: prevIncludeUnassigned,
    } = prevProps;

    const filterChanged =
      from !== prevFrom ||
      to !== prevTo ||
      autoRefreshTime !== prevAutoRefreshTime ||
      includePending !== prevIncludePending ||
      includeConfirmed !== prevIncludeConfirmed ||
      includeUnassigned !== prevIncludeUnassigned ||
      (!doctorIds && prevDoctorIds) ||
      (doctorIds && !prevDoctorIds) ||
      (doctorIds &&
        prevDoctorIds &&
        (doctorIds.length !== prevDoctorIds.length ||
          doctorIds.some(id => prevDoctorIds.indexOf(id) < 0)));

    if (filterChanged || !this.isInitialized) {
      this.isInitialized = true;
      this.loadAppointmentsGroupByDoctor();
    }
  }

  render() {
    const { className, t } = this.props;
    const {
      appointments: { doctorAppointments = [], total },
      loading,
    } = this.state;

    if (!this.isFilteredByDoctor || !this.isFitleredByStatus) {
      return (
        <Flex className={className}>
          <Flex className="no-filters">
            <Box>{t(i18nKeys.filterGuide)}</Box>
          </Flex>
        </Flex>
      );
    }

    const columns = doctorAppointments.map(this.buildColumn);

    return (
      <Flex className={className}>
        {loading ? (
          <LoadingState />
        ) : (
            <Flex auto column>
              <Flex>{columns}</Flex>
              {total === 0 ? (
                <Flex className="is-empty">
                  <Box>{t(i18nKeys.noAppointment)}</Box>
                  <Box>{t(i18nKeys.createAppointmentGuide)}</Box>
                </Flex>
              ) : null}
            </Flex>
          )}
      </Flex>
    );
  }

  buildColumn = ({
    doctorId,
    doctorName,
    appointments = [],
  }: {
    doctorId: string;
    doctorName: string;
    appointments: AppointmentDetail[];
  }) => {
    return (
      <Flex key={doctorId} className="column">
        <DoctorInfoCard
          doctorName={doctorName}
          totalAppointment={appointments.length}
          showAvatar={doctorId !== UNASSIGNED_DOCTOR_ID}
        />
        {appointments
          .sort((ap1, ap2) => ap1.startDateTime - ap2.startDateTime)
          .map(app => {
            const { confirmedAppointmentDates = [], reason } = app;
            return (
              <Popover
                key={`${doctorId} + "_" + ${app.id}`}
                boundary="viewport"
                modifiers={{
                  arrow: { enabled: true },
                  flip: { enabled: true },
                  keepTogether: { enabled: true },
                  preventOverflow: { enabled: true },
                }}
              >
                <AppointmentInfoCard
                  patientName={app.patientName}
                  startDateTime={app.startDateTime}
                  hourAndMinuteSpecified={app.hourAndMinuteSpecified}
                  confirmedAppointmentDates={confirmedAppointmentDates}
                  description={reason}
                />
                <AppointmentPopover
                  appointment={app}
                  onCancelAppointmentOK={this.loadAppointmentsGroupByDoctor}
                  onEditAppointmentOK={this.loadAppointmentsGroupByDoctor}
                />
              </Popover>
            );
          })}
      </Flex>
    );
  };

  setReferenceDataToState = () => {
    const { getDoctors, getNurses } = this.props;

    const promises = [getDoctors(), getNurses()];

    Promise.all(promises).then(([doctors, nurses]) => {
      let newState = {
        doctorById: {},
        nurseById: {},
      };

      if (doctors) {
        newState.doctorById = doctors.dictionary;
      }
      if (nurses) {
        newState.nurseById = nurses.dictionary;
      }

      this.setState(newState);
    });
  };

  loadAppointmentsGroupByDoctor = async () => {
    const {
      from,
      to,
      filteredDoctorIds = [],
      includePending,
      includeConfirmed,
      includeUnassigned,
    } = this.props;

    // dont call backend if either doctors or status filter is not specified
    if (
      (filteredDoctorIds.length === 0 && !includeUnassigned) ||
      (!includePending && !includeConfirmed)
    ) {
      const doctorAppointments = this.buildDoctorAppointments([]);
      this.setState({
        appointments: { total: 0, doctorAppointments },
      });
      return;
    }

    this.setState({ loading: true });

    const appointments = await AppointmentService.findAppointments(
      from,
      to,
      filteredDoctorIds,
      includeConfirmed,
      includePending,
      includeUnassigned
    );

    const doctorAppointments = this.buildDoctorAppointments(appointments);

    this.setState({
      appointments: { total: appointments.length, doctorAppointments },
      loading: false,
    });
  };

  buildDoctorAppointments = (
    appointments: AppointmentDetail[]
  ): DoctorAppointments[] => {
    const associatedDoctorId2Appointment: {
      [key: string]: AppointmentDetail[];
    } = {};

    // build the map from doctorId to appointments
    for (const appointment of appointments) {
      const { doctorIds: associatedDoctorIds = [] } = appointment;

      if (
        associatedDoctorIds === undefined ||
        associatedDoctorIds === null ||
        associatedDoctorIds.length === 0
      ) {
        if (
          associatedDoctorId2Appointment[UNASSIGNED_DOCTOR_ID] === undefined
        ) {
          associatedDoctorId2Appointment[UNASSIGNED_DOCTOR_ID] = [];
        }
        associatedDoctorId2Appointment[UNASSIGNED_DOCTOR_ID].push(appointment);
      } else {
        for (const associatedDoctorId of associatedDoctorIds) {
          if (
            associatedDoctorId2Appointment[associatedDoctorId] === undefined
          ) {
            associatedDoctorId2Appointment[associatedDoctorId] = [];
          }
          associatedDoctorId2Appointment[associatedDoctorId].push(appointment);
        }
      }
    }

    let doctorIdsForColumnHeaders: string[] = [];
    const { filteredDoctorIds = [] } = this.props;
    const { includeUnassigned } = this.props;
    if (includeUnassigned) {
      doctorIdsForColumnHeaders.push(UNASSIGNED_DOCTOR_ID);
    }

    if (filteredDoctorIds !== undefined && filteredDoctorIds.length > 0) {
      doctorIdsForColumnHeaders.push(...filteredDoctorIds);
    }

    if (doctorIdsForColumnHeaders.length === 0) {
      return [];
    }

    const doctorAppointments: DoctorAppointments[] = [];
    let doctorName: string;
    for (const doctorId of doctorIdsForColumnHeaders) {
      if (doctorId === UNASSIGNED_DOCTOR_ID) {
        doctorName = UNASSIGNED_NAME;
      } else {
        const doctorProfile = this.state.doctorById[doctorId];
        doctorName = doctorProfile ? doctorProfile.fullName : '';
      }
      doctorAppointments.push({
        appointments: associatedDoctorId2Appointment[doctorId]
          ? associatedDoctorId2Appointment[doctorId]
          : [],
        doctorId,
        doctorName,
      });
    }

    doctorAppointments.sort((a, b) => {
      if (a.doctorName === UNASSIGNED_NAME) {
        return -1;
      }
      if (b.doctorName === UNASSIGNED_NAME) {
        return 1;
      }

      if (a.doctorName > b.doctorName) {
        return 1;
      } else if (a.doctorName < b.doctorName) {
        return -1;
      } else {
        return 0;
      }
    });

    return doctorAppointments;
  };

  updateDoctorAppointments = (
    doctorIds: string[] = [],
    appointmentId: string,
    action: AppointmentApiModel.ActionType
  ) => {
    const {
      appointments: { doctorAppointments = [] },
    } = this.state;
    let normalizedDoctorIds = doctorIds;
    if (!doctorIds || doctorIds.length === 0) {
      normalizedDoctorIds = [UNASSIGNED_DOCTOR_ID];
    }

    for (const doctorApps of doctorAppointments) {
      const { doctorId, appointments: docAppList } = doctorApps;

      if (normalizedDoctorIds.includes(doctorId)) {
        let foundAppointment: AppointmentDetail = undefined;
        let foundIndex = undefined;
        for (let index = 0; index < docAppList.length; index++) {
          const app = docAppList[index];
          if (app.id === appointmentId) {
            foundAppointment = app;
            foundIndex = index;
            break;
          }
        }
        if (foundAppointment) {
          if (action === AppointmentApiModel.ActionType.CONFIRM) {
            if (!foundAppointment.confirmedAppointmentDates) {
              foundAppointment.confirmedAppointmentDates = [];
            }
            foundAppointment.confirmedAppointmentDates.push(
              foundAppointment.startDateTime
            ); // only for recurrence.type = never

            this.setState(prevState => {
              return {
                appointments: {
                  total: prevState.appointments.total,
                  doctorAppointments: doctorAppointments,
                },
              };
            });
          } else {
            docAppList.splice(foundIndex, 1);

            this.setState(prevState => {
              return {
                appointments: {
                  total: prevState.appointments.total - 1,
                  doctorAppointments: doctorAppointments,
                },
              };
            });
          }
        }
      }
    }
  };
}

export const AppointmentColumnListView = withTheme(
  withAppointmentContext(
    withI18nContext(OriginAppointmentColumnListView, {
      namespace: NAMESPACE_APPOINTMENT_COLUMN_LIST_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
