import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-column-list-view.json';

export const NAMESPACE_APPOINTMENT_COLUMN_LIST_VIEW =
  'appointment-column-list-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
