import { ICareProviderTheme } from '@careprovider/theme';

export interface AppointmentInfoCardProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientName: string;
  startDateTime: number;
  hourAndMinuteSpecified: boolean;
  confirmedAppointmentDates?: number[];
  description?: string;
}
