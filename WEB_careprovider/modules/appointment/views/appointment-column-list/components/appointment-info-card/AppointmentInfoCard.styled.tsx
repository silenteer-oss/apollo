import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { AppointmentInfoCard as OriginAppointmentInfoCard } from './AppointmentInfoCard';
import { AppointmentInfoCardProps } from './models';

export const AppointmentInfoCard: any = styled(OriginAppointmentInfoCard).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppointmentInfoCard', className),
  })
) <AppointmentInfoCardProps>`
  ${props => {
    const { radius } = props.theme;
    return `
    & {
        flex-direction: column;
        min-width: 222px;
        min-height: 85px;
        max-width: 222px;
        max-height: 85px;
        border: 1px solid #DCE0E4;
        box-sizing: border-box;
        border-radius: ${radius['4px']};
        padding: ${scaleSpacePx(2)} ${scaleSpacePx(3)};
        overflow: hidden;

        .appointment-patient-name {
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;
        }
        .appointment-status {
          align-items: center;
          .status {
            margin-right: ${scaleSpacePx(3)};
          }
        }
        .appointment-description {
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;
        }
      }`;
  }}
`;
