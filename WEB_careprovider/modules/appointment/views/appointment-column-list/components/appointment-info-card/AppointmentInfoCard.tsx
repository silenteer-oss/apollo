import React from 'react';
import { withTheme } from '@careprovider/theme';
import { Flex, BodyTextM, BodyTextS } from '@design-system/core/components';
import { AppointmentInfoCardProps } from './models';
import { Icon } from '@blueprintjs/core';
import { dateTimeFormat } from '@careprovider/services/util';

class OriginAppointmentInfoCard extends React.Component<
  AppointmentInfoCardProps,
  any
> {
  render() {
    const {
      className,
      patientName,
      startDateTime,
      hourAndMinuteSpecified,
      confirmedAppointmentDates = [],
      description = '-',
      theme: { foreground },
    } = this.props;

    return (
      <Flex style={{ cursor: 'pointer' }} className={className}>
        <BodyTextM className="appointment-patient-name" fontWeight="SemiBold">
          {patientName}
        </BodyTextM>
        <Flex className="appointment-status">
          <Icon
            iconSize={8}
            className="status"
            icon="full-circle"
            color={
              confirmedAppointmentDates.length === 0
                ? foreground.warn.base
                : foreground.success.base
            }
          />
          <BodyTextS>
            {dateTimeFormat(startDateTime, hourAndMinuteSpecified ? 'LT' : 'L')}
          </BodyTextS>
        </Flex>
        <BodyTextS className="appointment-description">{description}</BodyTextS>
      </Flex>
    );
  }
}

export const AppointmentInfoCard = withTheme(OriginAppointmentInfoCard);
