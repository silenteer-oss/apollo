import { IEmployeeProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { AppointmentDetail } from '../../../../../services/AppointmentServiceModel';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';

export interface AppointmentPopoverProps {
  className?: string;
  theme?: ICareProviderTheme;
  appointment: AppointmentDetail;
  onCancelAppointmentOK: () => void;
  onEditAppointmentOK: () => void;
}

export interface AppointmentPopoverState {
  doctorById: { [key: string]: IEmployeeProfile };
  nurseById: { [key: string]: IEmployeeProfile };
  isCancelAlertOpen: boolean;
  isCancelReasonOpen: boolean;
  cancelReasonId: string;
  cancelReasonList: AppointmentApiModel.AppointmentCancelReasonResponse[];
}
