import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  Divider,
  Button,
  Alert,
  Intent,
  RadioGroup,
  Radio,
} from '@blueprintjs/core';
import { withTheme } from '@careprovider/theme';
import { getCssClass, isEmpty } from '@design-system/infrastructure/utils';
import { Flex, H4, BodyTextM, H2 } from '@design-system/core/components';
import { scaleSpacePx } from '@design-system/core/styles';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { dateTimeFormat } from '@careprovider/services/util';
import { ModuleToaster } from '@careprovider/modules/appointment/components';
import { getModuleConfig } from '../../../../module-config';
import {
  IAppointmentContext,
  withAppointmentContext,
} from '../../../../context';
import AppointmentService from '../../../../services/AppointmentService';
import { AppointmentDetailsView } from '../../../appointment-details';
import { AppointmentPopoverProps, AppointmentPopoverState } from './models';
import { NAMESPACE_APPOINTMENT_POPOVER, i18nKeys } from './i18n';

import getPath from '@careprovider/assets'

const CalendarIconUrl = getPath('images/calendar.svg');
const ReasonIconUrl = getPath('images/hospital-board.svg');
const EmployeeIconUrl = getPath('images/multiple-users.svg');
const InternalNoteIconUrl = getPath('images/paragraph-justified-align.svg');
const NoteIconUrl = getPath('@careprovider/assets/images/information-circle.svg');

class OriginAppointmentPopover extends React.Component<
  AppointmentPopoverProps &
  IAppointmentContext &
  RouteComponentProps<any> &
  II18nFixedNamespaceContext,
  AppointmentPopoverState
  > {
  state: AppointmentPopoverState = {
    doctorById: {},
    nurseById: {},
    isCancelAlertOpen: false,
    isCancelReasonOpen: false,
    cancelReasonId: undefined,
    cancelReasonList: [],
  };

  componentDidMount() {
    this.setReferenceDataToState();
  }

  render() {
    const {
      doctorById,
      nurseById,
      cancelReasonId,
      cancelReasonList,
      isCancelReasonOpen,
    } = this.state;
    const {
      className,
      appointment: {
        id,
        patientId,
        patientName,
        startDateTime,
        hourAndMinuteSpecified,
        reason = '-',
        doctorIds = [],
        nurseIds = [],
        note = '-',
        internalNote = '-',
        confirmedAppointmentDates = [],
        recurrence,
        reminderValue,
        reminderUnit,
      },
      t,
    } = this.props;

    if (isCancelReasonOpen && cancelReasonList.length === 0) {
      return null;
    }

    let defaultCancelReasonId = undefined;
    if (cancelReasonList.length > 0) {
      defaultCancelReasonId = cancelReasonList[0].id;
    }

    const startTimeString = `${dateTimeFormat(
      startDateTime,
      'L'
    )} - ${dateTimeFormat(startDateTime, 'hh:mm a')}`;

    const employeeList = [...doctorIds, ...nurseIds].map(id => {
      if (doctorById[id]) {
        return `${t(i18nKeys.doctorPrefix)} ${doctorById[id].fullName}`;
      } else if (nurseById[id]) {
        return `${t(i18nKeys.nursePrefix)} ${nurseById[id].fullName}`;
      } else {
        return id;
      }
    });

    const status =
      confirmedAppointmentDates.length > 0 ? 'Confirmed' : 'Pending';
    const statusClass = status.toLowerCase();

    return (
      <div className={className}>
        <div className="appointment-info">
          <H4 lineHeight={scaleSpacePx(5)}>{patientName}</H4>
          <div className="item">
            <div className="icon">
              <img src={CalendarIconUrl} />
            </div>
            <BodyTextM className="info">{startTimeString}</BodyTextM>
            <BodyTextM
              className={getCssClass('appointment-status', statusClass)}
              fontWeight="SemiBold"
            >
              {status === 'Confirmed' && t(i18nKeys.confirmedAppointmentStatus)}
              {status === 'Pending' && t(i18nKeys.pendingAppointmentStatus)}
            </BodyTextM>
          </div>
          <div className="item">
            <div className="icon">
              <img src={ReasonIconUrl} />
            </div>
            <BodyTextM className="info">{reason}</BodyTextM>
          </div>
          <div className="item">
            <div className="icon">
              <img src={EmployeeIconUrl} />
            </div>
            <BodyTextM className="info">{employeeList.join(', ')}</BodyTextM>
          </div>
          <div className="item">
            <div className="icon">
              <img src={NoteIconUrl} />
            </div>
            <BodyTextM className="info">{note}</BodyTextM>
          </div>
          <div className="item">
            <div className="icon">
              <img src={InternalNoteIconUrl} />
            </div>
            <BodyTextM className="info">{internalNote}</BodyTextM>
          </div>
        </div>
        <Divider />
        <Flex className="action-buttons ">
          <Button minimal intent="primary" onClick={this.gotoChat}>
            {t(i18nKeys.chatWithPatientButton)}
          </Button>
          <AppointmentDetailsView
            buttonLabel={t(i18nKeys.editAppointmentButton)}
            appointmentId={id}
            patientId={patientId}
            doctorIds={this.props.appointment.doctorIds}
            nurseIds={this.props.appointment.nurseIds}
            recurrenceType={recurrence.type}
            appointmentDateTime={{
              value: new Date(startDateTime),
              hourAndMinuteSpecified,
            }} // currently only applicable for NEVER recurrence type
            reminderValue={reminderValue}
            reminderUnit={reminderUnit}
            reason={this.props.appointment.reason}
            note={this.props.appointment.note}
            internalNote={this.props.appointment.internalNote}
            department={this.props.appointment.department}
            onSubmit={this.onEditOK}
          />
          <Button
            minimal
            intent="danger"
            className="cancel"
            onClick={this.showCancelAppointmentAlert}
          >
            {t(i18nKeys.cancelAppointmentButton)}
          </Button>
        </Flex>
        <Alert
          className="appointment-cancel-alert bp3-alert-fill"
          cancelButtonText={t(i18nKeys.alertNoButton)}
          confirmButtonText={t(i18nKeys.alertYesButton)}
          intent={Intent.DANGER}
          isOpen={this.state.isCancelAlertOpen}
          onConfirm={this.showCancelReasonAlert}
          onCancel={this.closeCancelAppointmentAlert}
        >
          <H2>{t(i18nKeys.alertMessage)}</H2>
        </Alert>
        <Alert
          className="upcoming-cancel-alert bp3-alert-fill"
          cancelButtonText={t(i18nKeys.cancelReasonDialogNoButton)}
          confirmButtonText={t(i18nKeys.cancelReasonDialogYesButton)}
          intent={Intent.DANGER}
          isOpen={this.state.isCancelReasonOpen}
          onConfirm={this.doCancelAppointment}
          onCancel={this.closeCancelReasonAlert}
        >
          <Flex column>
            <H2 margin="0 0 12px 0">{t(i18nKeys.editedReasonTitle)}</H2>
            <RadioGroup
              className="radio-group"
              onChange={this.setCancelReason}
              selectedValue={
                isEmpty(cancelReasonId) ? defaultCancelReasonId : cancelReasonId
              }
            >
              {cancelReasonList
                .filter(cancelReason => 'VI' === cancelReason.language)
                .map(item => (
                  <Radio
                    key={item.key}
                    label={item.content}
                    value={item.id}
                    className="inline-radio"
                  />
                ))}
            </RadioGroup>
          </Flex>
        </Alert>
      </div>
    );
  }

  gotoChat = () => {
    const { history } = this.props;
    history.push(`/app/conversation/${this.props.appointment.patientId}`);
  };

  showCancelAppointmentAlert = () => {
    this.setState({ isCancelAlertOpen: true });
  };

  closeCancelAppointmentAlert = () => {
    this.setState({ isCancelAlertOpen: false, cancelReasonId: undefined });
  };

  showCancelReasonAlert = () => {
    AppointmentService.getCancelReasonList().then(result => {
      this.setState({
        isCancelAlertOpen: false,
        isCancelReasonOpen: true,
        cancelReasonList: result,
      });
    });
  };

  closeCancelReasonAlert = () => {
    this.setState({
      isCancelReasonOpen: false,
      isCancelAlertOpen: false,
      cancelReasonId: undefined,
    });
  };

  setCancelReason = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ cancelReasonId: event.currentTarget.value });
  };

  doCancelAppointment = () => {
    const { appointment, t } = this.props;
    const { cancelReasonId, cancelReasonList } = this.state;
    if (!cancelReasonList && cancelReasonList.length === 0) {
      ModuleToaster.show({
        intent: Intent.DANGER,
        message: t(i18nKeys.noCancelMessage),
      });
      return;
    }

    AppointmentService.cancelAppointment(
      appointment.id,
      appointment.startDateTime,
      cancelReasonId || cancelReasonList[0].id
    ).then(success => {
      if (success) {
        ModuleToaster.show({
          intent: Intent.SUCCESS,
          message: t(i18nKeys.cancelSuccess),
        });
        this.setState(
          { isCancelAlertOpen: false, isCancelReasonOpen: false },
          this.props.onCancelAppointmentOK
        );
      }
    });
  };

  onEditOK = () => {
    if (this.props.onEditAppointmentOK) {
      this.props.onEditAppointmentOK();
    }
  };

  setReferenceDataToState = () => {
    const { getDoctors, getNurses } = this.props;

    const promises = [getDoctors(), getNurses()];

    Promise.all(promises).then(([doctors, nurses]) => {
      let newState: AppointmentPopoverState = {
        doctorById: {},
        nurseById: {},
      } as AppointmentPopoverState;

      if (doctors) {
        newState.doctorById = doctors.dictionary;
      }
      if (nurses) {
        newState.nurseById = nurses.dictionary;
      }

      this.setState(newState);
    });
  };
}

export const AppointmentPopover = withRouter(
  withTheme(
    withAppointmentContext(
      withI18nContext(OriginAppointmentPopover, {
        namespace: NAMESPACE_APPOINTMENT_POPOVER,
        publicAssetPath: getModuleConfig().publicAssetPath,
      })
    )
  )
);
