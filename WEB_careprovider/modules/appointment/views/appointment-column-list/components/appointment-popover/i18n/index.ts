import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-popover.json';

export const NAMESPACE_APPOINTMENT_POPOVER = 'appointment-popover';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
