import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { AppointmentPopover as OriginAppointmentPopover } from './AppointmentPopover';
import { AppointmentPopoverProps } from './models';

export const AppointmentPopover = styled(OriginAppointmentPopover).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppointmentPopover', className),
  })
)<AppointmentPopoverProps>`
  ${props => {
    const { background, foreground, space, radius, typography } = props.theme;
    return `
    & {
        display: flex;
        flex-direction: column;
        width: ${scaleSpacePx(98)};
        .appointment-info {
          display: flex;
          flex-direction: column;
          padding: ${space.s};
          .item {
            display: flex;
            align-items: flex-start;
            margin-top: ${space.s};

            > .icon {
              width: ${scaleSpacePx(4)};
              height: ${scaleSpacePx(4)};
              margin-right: ${scaleSpacePx(5)};
            }

            > .info {
              flex: 1;
            }

            > .appointment-status {
              padding: 0 ${scaleSpacePx(3)};
              border-radius: ${radius['4px']};
              text-align: center;

              &.pending {
                color: ${foreground.warn.base};
                background-color: ${background.warn.lighter};
              }
              &.confirmed {
                color: ${foreground.success.base};
                background-color: ${background.success.lighter};
              }
            }
          }

        }

        .action-buttons {
          padding: ${space.s} ${space.m};
          justify-content: flex-end;

          .bp3-button {
            min-width: initial;
            padding: 0;
            margin-right: ${space.m};
            background: none;
            border: none;
            .bp3-button-text {
              color: ${foreground.primary.base};
              font-weight: ${typography.link.fontWeight};
            }
            &.cancel {
              margin-right: 0;
              .bp3-button-text {
                color: ${foreground.error.base};
                font-weight: ${typography.link.fontWeight};
              }
            }
          }

          .sl-AppointmentDetailsView {
            .btn-set-appointment {
              min-width: initial;
              padding: 0;
              background: none !important;
              border: none;
              margin-right: ${space.m};

              .bp3-button-text {
                color: ${foreground.primary.base} !important;
                font-weight: ${typography.link.fontWeight};
              }
            }
          }
        }

      }`;
  }}
`;
