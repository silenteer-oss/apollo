import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { DoctorInfoCard as OriginDoctorInfoCard } from './DoctorInfoCard';
import { DoctorInfoCardProps } from './models';

export const DoctorInfoCard = styled(OriginDoctorInfoCard).attrs(
  ({ className }) => ({
    className: getCssClass('sl-DoctorInfoCard', className),
  })
)<DoctorInfoCardProps>`
  ${props => {
    const { background, radius } = props.theme;
    return `
    & {
        align-items: center;
        min-width: 222px;
        min-height: 60px;
        max-width: 222px;
        max-height: 60px;
        box-sizing: border-box;
        background-color: ${background['02']};
        border-radius: ${radius['4px']};
        padding: ${scaleSpacePx(3)};
        .avatar {
          margin-right: ${scaleSpacePx(1)};
          img {
            border-radius: ${radius.circle};
          }
        }
        .info {
          flex: 1;
          flex-direction: column;
          overflow: hidden;
          .name {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
          }
        }
      }`;
  }}
`;
