import React from 'react';
import { withTheme } from '@careprovider/theme';
import {
  Flex,
  Box,
  BodyTextM,
  BodyTextS,
} from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getModuleConfig } from '@careprovider/modules/appointment/module-config';
import { DoctorInfoCardProps } from './models';
import { NAMESPACE_DOCTOR_INFO_CARD, i18nKeys } from './i18n';

import getPath from '@careprovider/assets'
const DefaultDoctorAvatarUrl = getPath('images/default-doctor-avatar.png');

class OriginDoctorInfoCard extends React.Component<
  DoctorInfoCardProps & II18nFixedNamespaceContext
  > {
  render() {
    const {
      className,
      doctorName,
      totalAppointment = 0,
      showAvatar = true,
      t,
    } = this.props;

    return (
      <Flex className={className}>
        {showAvatar && (
          <Box className="avatar">
            <img src={DefaultDoctorAvatarUrl} />
          </Box>
        )}
        <Flex className="info">
          <BodyTextM className="name" fontWeight="SemiBold">
            {doctorName}
          </BodyTextM>
          <BodyTextS>
            {t(i18nKeys.appointmentLabel, { total: totalAppointment })}
          </BodyTextS>
        </Flex>
      </Flex>
    );
  }
}

export const DoctorInfoCard = withTheme(
  withI18nContext(OriginDoctorInfoCard, {
    namespace: NAMESPACE_DOCTOR_INFO_CARD,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
