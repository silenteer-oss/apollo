import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './doctor-info-card.json';

export const NAMESPACE_DOCTOR_INFO_CARD = 'doctor-info-card';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
