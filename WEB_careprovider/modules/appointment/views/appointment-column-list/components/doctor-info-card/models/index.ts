import { ICareProviderTheme } from '@careprovider/theme';

export interface DoctorInfoCardProps {
  className?: string;
  theme?: ICareProviderTheme;
  doctorName: string;
  totalAppointment: number;
  showAvatar?: boolean;
}
