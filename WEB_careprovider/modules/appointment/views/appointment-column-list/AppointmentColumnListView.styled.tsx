import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { AppointmentColumnListView as OriginAppointmentColumnListView } from './AppointmentColumnListView';
import { ColumnListViewProps } from './models';

export const AppointmentColumnListView = styled(
  OriginAppointmentColumnListView
).attrs(({ className }) => ({
  className: getCssClass('sl-AppointmentColumnListView', className),
}))<ColumnListViewProps>`
  ${props => {
    const { space } = props.theme;
    return `
      & {
        padding: 0 ${space.m};

        .is-empty {
          flex: 1;
          flex-direction: column;
          align-items: center;
          justify-content: center;
        }

        .column {
          flex-direction: column;
          margin-right: ${scaleSpacePx(2)};
          .sl-DoctorInfoCard {
            margin-bottom: ${scaleSpacePx(2)};
          }
          .sl-AppointmentInfoCard {
            margin-bottom: ${scaleSpacePx(2)};
          }
        }
      }`;
  }}
`;
