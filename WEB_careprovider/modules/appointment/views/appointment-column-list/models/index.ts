import { Observable } from 'rxjs';
import {
  IAppointmentChangeLog,
  IEmployeeProfile,
} from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { AppointmentDetail } from '../../../services/AppointmentServiceModel';

export interface ColumnListViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  filteredDoctorIds?: string[];
  from: number;
  to: number;
  autoRefreshTime?: number;
  includePending?: boolean;
  includeConfirmed?: boolean;
  includeUnassigned?: boolean;
  appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  reconnected$: Observable<void>;
}

export interface ColumnListViewState {
  appointments: {
    total: number;
    doctorAppointments: DoctorAppointments[];
  };
  doctorById: { [key: string]: IEmployeeProfile };
  nurseById: { [key: string]: IEmployeeProfile };
  loading: boolean;
}

export interface DoctorAppointments {
  doctorId: string;
  doctorName: string;
  appointments: AppointmentDetail[];
}
