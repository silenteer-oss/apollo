import React from 'react';
// Include the locale utils designed for moment
import MomentLocaleUtils from 'react-day-picker/moment';
import { DatePicker } from '@blueprintjs/datetime';
import { Flex, Box } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { Checkbox, Intent, Button } from '@blueprintjs/core';
import { getModuleConfig } from '@careprovider/modules/appointment/module-config';
import AppointmentService from '../../services/AppointmentService';
import { withAppointmentContext, IAppointmentContext } from '../../context';
import {
  IAppointmentFilterProps,
  IAppointmentFilterState,
  TAppointmentStatus,
  UNASSIGNED_DOCTOR_ID,
} from './AppointmentFilterModel';
import { NAMESPACE_APPOINTMENT_FILTER_VIEW, i18nKeys } from './i18n';

class OriginAppointmentFilterView extends React.PureComponent<
  IAppointmentFilterProps & IAppointmentContext & II18nFixedNamespaceContext,
  IAppointmentFilterState
  > {
  state: IAppointmentFilterState = {
    doctors: [],
    selectedDate: new Date().getTime(),
    selectedStatus: ['Confirmed', 'Pending'],
    selectedDoctorsId: [],
  };

  componentDidMount() {
    const { getDoctors } = this.props;
    const { doctors, selectedDate, selectedStatus } = this.state;

    if (!doctors || doctors.length === 0) {
      getDoctors().then(doctors => {
        const newSelectedDoctorsId = [];
        const { items } = doctors;
        items.map(item => newSelectedDoctorsId.push(item.id));
        newSelectedDoctorsId.push(UNASSIGNED_DOCTOR_ID);

        this.setState(
          {
            doctors: items,
            selectedDoctorsId: newSelectedDoctorsId,
          },
          () => {
            this.props.onDateSelected(selectedDate);
            this.props.onDoctorsSelected([...this.state.selectedDoctorsId]);
            this.props.onStatusSelected([...selectedStatus]);
          }
        );
      });
    }
  }

  render() {
    const {
      className,
      theme: { space },
      t,
    } = this.props;
    const {
      doctors,
      selectedStatus,
      selectedDoctorsId,
      selectedDate,
    } = this.state;

    const displayedDoctors = doctors.map(doctor => {
      return (
        <Checkbox
          key={doctor.id}
          checked={selectedDoctorsId.includes(doctor.id)}
          label={`${t(i18nKeys.doctorPrefix)} ${doctor.fullName}`}
          onChange={() => this.handleDoctorChange(doctor.id)}
        />
      );
    });

    displayedDoctors.splice(
      0,
      0,
      <Checkbox
        key={UNASSIGNED_DOCTOR_ID}
        checked={selectedDoctorsId.includes(UNASSIGNED_DOCTOR_ID)}
        label={t(i18nKeys.unassignedAppointmentTitle)}
        onChange={() => this.handleDoctorChange(UNASSIGNED_DOCTOR_ID)}
      />
    );

    const currentDate = new Date().getDate();
    const dateInLastYear = new Date();
    dateInLastYear.setDate(currentDate - 365);
    const dateInNextYear = new Date();
    dateInNextYear.setDate(currentDate + 365);

    return (
      <Flex column className={className}>
        <Box className="sl-calendar-container">
          <DatePicker
            locale="vi"
            localeUtils={MomentLocaleUtils}
            value={new Date(selectedDate)}
            onChange={(date, isUserChange) =>
              isUserChange && date && this.handleDateChange(date.getTime())
            }
            minDate={dateInLastYear}
            maxDate={dateInNextYear}
          />
        </Box>
        <Box p={space.s}>
          <Flex column className="filter-group status">
            <Flex align="center" className="filter-group-header">
              <h3>{t(i18nKeys.filterStatusTitle)}</h3>
            </Flex>
            <Flex column>
              <Checkbox
                className="confirmed"
                checked={selectedStatus.includes('Confirmed')}
                label={t(i18nKeys.confirmedStatusLabel)}
                onChange={() => this.handleStatusChange('Confirmed')}
              />
              <Checkbox
                className="pending"
                checked={selectedStatus.includes('Pending')}
                label={t(i18nKeys.pendingStatusLabel)}
                onChange={() => this.handleStatusChange('Pending')}
              />
            </Flex>
          </Flex>
          <Flex column className="filter-group">
            <Flex align="center" className="filter-group-header">
              <h3>{t(i18nKeys.filterDoctorTitle)}</h3>
              <Button
                minimal
                intent={Intent.PRIMARY}
                onClick={this.toggleSelectAllDoctors}
              >
                {selectedDoctorsId.length === doctors.length + 1
                  ? t(i18nKeys.unselectAllDoctorLabel)
                  : t(i18nKeys.selectAllDoctorLabel)}
              </Button>
            </Flex>
            <Flex column className="doctors-list">
              {displayedDoctors}
            </Flex>
          </Flex>
        </Box>
      </Flex>
    );
  }

  toggleSelectAllDoctors = () => {
    // selectedDoctorsId.length === doctors.length + 1
    this.setState(
      prevState => {
        let newIds = [];
        if (
          !prevState.selectedDoctorsId ||
          prevState.selectedDoctorsId.length < prevState.doctors.length + 1
        ) {
          newIds = [UNASSIGNED_DOCTOR_ID, ...prevState.doctors.map(d => d.id)];
        }
        return {
          ...prevState,
          selectedDoctorsId: newIds,
        };
      },
      () => this.props.onDoctorsSelected([...this.state.selectedDoctorsId])
    );
  };

  handleDateChange = (date: number) => {
    this.setState({ selectedDate: date }, () =>
      this.props.onDateSelected(this.state.selectedDate)
    );
  };

  handleStatusChange = (status: TAppointmentStatus) => {
    const { selectedStatus } = this.state;
    const temp = [...selectedStatus];
    if (temp.includes(status)) {
      temp.splice(temp.indexOf(status), 1);
    } else {
      temp.push(status);
    }
    this.setState({ selectedStatus: temp }, () =>
      this.props.onStatusSelected([...this.state.selectedStatus])
    );
  };

  handleDoctorChange = (doctorId: string) => {
    const { selectedDoctorsId } = this.state;
    const temp = [...selectedDoctorsId];
    if (temp.includes(doctorId)) {
      temp.splice(temp.indexOf(doctorId), 1);
    } else {
      temp.push(doctorId);
    }
    this.setState({ selectedDoctorsId: temp }, () =>
      this.props.onDoctorsSelected([...this.state.selectedDoctorsId])
    );
  };
}

export const AppointmentFilterView = withTheme(
  withAppointmentContext(
    withI18nContext(OriginAppointmentFilterView, {
      namespace: NAMESPACE_APPOINTMENT_FILTER_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
