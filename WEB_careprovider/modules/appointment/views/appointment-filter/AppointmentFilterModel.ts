import { IEmployeeProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export type TAppointmentStatus = 'Pending' | 'Confirmed';
export const UNASSIGNED_DOCTOR_ID = '-1';

export interface IAppointmentFilterProps {
  className?: string;
  theme?: ICareProviderTheme;
  onDateSelected: (date: number) => void;
  onStatusSelected: (status: TAppointmentStatus[]) => void;
  onDoctorsSelected: (doctorsId: string[]) => void;
}

export interface IAppointmentFilterState {
  doctors: IEmployeeProfile[];
  selectedDate: number;
  selectedStatus: TAppointmentStatus[];
  selectedDoctorsId: string[];
}
