import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-filter-view.json';

export const NAMESPACE_APPOINTMENT_FILTER_VIEW = 'appointment-filter-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
