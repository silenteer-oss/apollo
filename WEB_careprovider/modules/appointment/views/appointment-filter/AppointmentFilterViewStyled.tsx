import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { AppointmentFilterView as OriginAppointmentFilterView } from './AppointmentFilterView';
import { IAppointmentFilterProps } from './AppointmentFilterModel';

export const AppointmentFilterView = styled(OriginAppointmentFilterView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppointmentFilterView', className),
  })
)<IAppointmentFilterProps>`
  ${props => {
    const { space, background } = props.theme;
    return `
    & {
        .sl-calendar-container {
          padding: 0 ${scaleSpacePx(3)};
        }

        .filter-group {
          + .filter-group {
            margin-top: ${space.xs};
          }

          &.status {
            height: ${scaleSpacePx(26)};
          }

          .doctors-list {
            max-height: calc(100vh - ${scaleSpacePx(90)});
            overflow: auto;
          }

          .filter-group-header {
            margin-bottom: ${space.s};

            h3 {
              flex: 1;
              margin-bottom: 0;
            }

            .bp3-button {
              min-width: initial;
            }

          }

          .bp3-checkbox {
            + .bp3-checkbox {
              margin-top: ${space.xs};
            }

            &.confirmed {
              > input:checked ~ .bp3-control-indicator {
                background-color: ${background.success.base} !important;
                border-color: ${background.success.base} !important;
              }
            }

            &.pending {
              > input:checked ~ .bp3-control-indicator {
                background-color: ${background.warn.base} !important;
              }
            }
          }
        }
      }`;
  }}
`;
