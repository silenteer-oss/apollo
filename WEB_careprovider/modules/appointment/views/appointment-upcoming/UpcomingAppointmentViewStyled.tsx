import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { UpcomingAppointmentView as OriginUpcomingAppointmentView } from './UpcomingAppointmentView';
import { IUpcomingAppointmentProps } from './UpcomingAppointmentModel';

export const UpcomingAppointmentView = styled(
  OriginUpcomingAppointmentView
).attrs(({ className }) => ({
  className: getCssClass('sl-UpcomingAppointmentView', className),
}))<IUpcomingAppointmentProps>`
  ${props => {
    const { background, foreground } = props.theme;
    return `
      & {
        overflow-y: hidden;

        .upcoming-appointment {
          flex-direction: column;
          overflow-y: auto;
          .divider {
            &:last-child {
              display: none;
            }
          }
        }

        .bp3-button-text {
          color: ${foreground['01']} !important;
          font-weight: 600;
        }

        .btn-set-appointment {
          min-width: 100%;
          background-color: ${background['01']} !important;
        }
      }
    `;
  }}
`;
