import { Observable } from 'rxjs';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import {
  IAppointmentChangeLog,
  IEmployeeProfile,
} from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IUpcomingAppointmentProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId: string;
  appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  reconnected$: Observable<void>;
  onEditAppointment?: (patientId: string) => void;
  onCancelAppointment?: (patientId: string) => void;
}

export interface IUpcomingAppointmentState {
  showFullContent: boolean;
  upcomingAppointments: AppointmentApiModel.AppointmentResponse[];
  doctors: IEmployeeProfile[];
  nurses: IEmployeeProfile[];
  isCancelAlertOpen: boolean;
  isCancelReasonOpen: boolean;
  cancelReasonId: string;
  cancelReasonList: AppointmentApiModel.AppointmentCancelReasonResponse[];
  reasonListLoading: boolean;
  showAllAppointment: boolean;
  selectedAppointment?: AppointmentApiModel.AppointmentResponse;
}
