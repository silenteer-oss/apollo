import { ICareProviderTheme } from 'theme';
import { AppointmentResponse } from '@silenteer/hermes/resource/appointment-app-resource/model';
import { IEmployeeProfile } from 'services/biz';

export interface ICollapsibleAppointmentProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId: string;
  appointment: AppointmentResponse;
  doctors: IEmployeeProfile[];
  nurses: IEmployeeProfile[];
  expandByDefault?: boolean;
  onCancelAppointment: (appointment: AppointmentResponse) => void;
}

export interface ICollapsibleAppointmentState {
  expand: boolean;
}
