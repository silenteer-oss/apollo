import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { CollapsibleAppointmentView as OriginCollapsibleAppointmentView } from './CollapsibleAppointmentView';

export const CollapsibleAppointmentView = styled(
  OriginCollapsibleAppointmentView
).attrs(({ className }) => ({
  className: getCssClass('sl-CollapsibleAppointmentView', className),
}))`
  ${props => {
    const { space, background, foreground, radius } = props.theme;
    return `
      & {
        padding: ${scaleSpacePx(2)};
        margin: ${scaleSpacePx(1)} 0;

        .action-buttons {
          display: none;
          flex: 1;
        }

        &:hover {
          background-color: ${background.primary.lighter};
          .action-buttons {
              display: flex;
          }
        }

        .appointment-header {
          align-items: center;
          flex: 1;

          .appointment-title {
            align-items: center;
            .arrow {
              padding-right: ${scaleSpacePx(2)};
              padding-left: 0;

              &.collapsed {
                transform: rotate(180deg);
                padding-left: ${scaleSpacePx(2)};
                padding-right: 0;
              }
            }

            h4 {
              flex: 1;
            }
          }


          .appointment-status {
            padding: 0 ${scaleSpacePx(3)};
            border-radius: ${radius['4px']};
            text-align: center;

            &.pending {
              color: ${foreground.warn.base};
              background-color: ${background.warn.lighter};
            }
            &.confirmed {
              color: ${foreground.success.base};
              background-color: ${background.success.lighter};
            }
          }
        }

        .appointment {
          display: flex;
          align-items: flex-start;
          margin-top: ${space.s};
          padding: 0 ${scaleSpacePx(6)};

          > .icon {
            width: ${scaleSpacePx(4)};
            height: ${scaleSpacePx(4)};
            margin-top: 2px;
            margin-right: ${scaleSpacePx(5)};
          }

          > .info {
            flex: 1;
          }

          > .appointment-status {
            padding: 0 ${scaleSpacePx(3)};
            border-radius: ${radius['4px']};
            text-align: center;

            &.pending {
              color: ${foreground.warn.base};
              background-color: ${background.warn.lighter};
            }
            &.confirmed {
              color: ${foreground.success.base};
              background-color: ${background.success.lighter};
            }
          }
        }
      }
    `;
  }}
`;
