import React from 'react';
import { Icon, Button } from '@blueprintjs/core';
import {
  isNotEmpty,
  isNotNull,
  getCssClass,
} from '@design-system/infrastructure/utils';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { Flex, BodyTextS, BodyTextM, H4 } from '@design-system/core/components';
import { Collapse } from '@blueprintjs/core';
import { withTheme } from '@careprovider/theme';
import { dateTimeFormat } from '@careprovider/services/util';

import { AppointmentDetailsView } from '../../../appointment-details/AppointmentDetailsView';
import { NAMESPACE_COLLAPSIBLE_APPOINTMENT_VIEW, i18nKeys } from './i18n';
import {
  ICollapsibleAppointmentProps,
  ICollapsibleAppointmentState,
} from './CollapsibleAppointmentModel';
import { RecurrenceType } from '@silenteer/hermes/resource/appointment-app-resource/model';
import { getModuleConfig } from 'modules/appointment/module-config';

import getPath from '@careprovider/assets'

const ReasonIconUrl = getPath('images/hospital-board.svg');
const EmployeeIconUrl = getPath('images/multiple-users.svg');
const NoteIconUrl = getPath('images/information-circle.svg');
const InternalNoteIconUrl = getPath('images/paragraph-justified-align.svg');
const ArrowDownButton = getPath('images/arrow-down-button.svg');
const EditIcon = getPath('images/edit-icon.svg');
const TrashIcon = getPath('images/trash-icon.svg');

class OriginalCollapsibleAppointmentView extends React.PureComponent<
  II18nFixedNamespaceContext & ICollapsibleAppointmentProps,
  ICollapsibleAppointmentState
  > {
  constructor(props) {
    super(props);
    this.state = {
      expand: !!props.expandByDefault,
    };
  }

  render() {
    const {
      patientId,
      t,
      appointment,
      doctors,
      nurses,
      className,
      onCancelAppointment,
    } = this.props;
    const { expand } = this.state;

    const {
      note = '-',
      internalNote = '-',
      reason = '-',
      doctorIds,
      nurseIds,
      recurrence,
      startDateTime,
      hourAndMinuteSpecified,
      confirmedAppointmentDates,
      reminderValue,
      reminderUnit,
    } = appointment;

    const appointmentStatus = this.computeUpcomingAppointmentStatus(
      startDateTime,
      confirmedAppointmentDates
    );
    const appointmentStatusCssClass = appointmentStatus.toLowerCase();

    const upcomingAppointmentDateTime = dateTimeFormat(
      startDateTime,
      hourAndMinuteSpecified ? 'L - LT' : 'L'
    );

    const doctorList = doctors
      .filter(doctor => isNotNull(doctorIds) && doctorIds.includes(doctor.id))
      .map(doctor => `${t(i18nKeys.doctorPrefix)} ${doctor.fullName}`)
      .join(', ');

    const nurseList = nurses
      .filter(nurse => isNotNull(nurseIds) && nurseIds.includes(nurse.id))
      .map(nurse => `${t(i18nKeys.nursePrefix)} ${nurse.fullName}`)
      .join(', ');

    const treatmentAttendanceList = [];
    if (isNotEmpty(doctorList)) {
      treatmentAttendanceList.push(doctorList);
    }
    if (isNotEmpty(nurseList)) {
      treatmentAttendanceList.push(nurseList);
    }
    if (treatmentAttendanceList.length === 0) {
      treatmentAttendanceList.push('-');
    }

    const toggleView = () => {
      this.setState(prevState => ({
        expand: !prevState.expand,
      }));
    };

    return (
      <Flex auto column className={className}>
        <Flex className="appointment-header">
          <Flex auto>
            <Flex className="appointment-title" onClick={toggleView}>
              <img
                className={`arrow ${!expand ? 'collapsed' : ''}`}
                src={ArrowDownButton}
              />
              <H4 padding={0}>{upcomingAppointmentDateTime}</H4>
            </Flex>
            {expand && (
              <Flex className="action-buttons">
                <AppointmentDetailsView
                  appointmentId={appointment.id}
                  patientId={patientId}
                  doctorIds={doctorIds}
                  nurseIds={nurseIds}
                  recurrenceType={recurrence.type}
                  appointmentDateTime={{
                    value: new Date(startDateTime),
                    hourAndMinuteSpecified,
                  }} // currently only applicable for NEVER recurrence type
                  reminderValue={reminderValue}
                  reminderUnit={reminderUnit}
                  reason={appointment.reason}
                  note={appointment.note}
                  internalNote={appointment.internalNote}
                  department={appointment.department}
                  icon={<img src={EditIcon} />}
                />
                <Button
                  onClick={() => onCancelAppointment(appointment)}
                  icon={<img src={TrashIcon} />}
                  minimal
                  small
                />
              </Flex>
            )}
          </Flex>
          {expand && (
            <BodyTextS
              className={getCssClass(
                'appointment-status',
                appointmentStatusCssClass
              )}
              fontWeight="SemiBold"
            >
              {appointmentStatus === 'Confirmed' &&
                t(i18nKeys.upcomingAppointmentConfirmedStatus)}
              {appointmentStatus === 'Pending' &&
                t(i18nKeys.upcomingAppointmentPendingStatus)}
            </BodyTextS>
          )}
        </Flex>
        <Collapse isOpen={expand}>
          <Flex className="appointment">
            <img className="icon" src={ReasonIconUrl} />
            <BodyTextM className="info">{reason}</BodyTextM>
          </Flex>
          {recurrence && RecurrenceType.REPEAT === recurrence.type && (
            <Flex className="appointment">
              <Icon icon="reset" />
              <BodyTextM className="info">
                Repeat every {recurrence.repeatInterval} {recurrence.repeatUnit}
                (s)
              </BodyTextM>
            </Flex>
          )}
          <Flex className="appointment">
            <img className="icon" src={EmployeeIconUrl} />
            {treatmentAttendanceList.join(', ')}
          </Flex>
          <Flex className="appointment">
            <img className="icon" src={NoteIconUrl} />
            <BodyTextM className="info">{note}</BodyTextM>
          </Flex>
          <Flex className="appointment">
            <img className="icon" src={InternalNoteIconUrl} />
            <BodyTextM className="info">{internalNote}</BodyTextM>
          </Flex>
          {/* <Flex className="action-buttons">
            <AppointmentDetailsView
              buttonLabel={t(i18nKeys.upcomingAppointmentEditButton)}
              appointmentId={appointment.id}
              patientId={patientId}
              doctorIds={doctorIds}
              nurseIds={nurseIds}
              recurrenceType={recurrence.type}
              appointmentDateTime={{
                value: new Date(startDateTime),
                hourAndMinuteSpecified,
              }} // currently only applicable for NEVER recurrence type
              reminderValue={reminderValue}
              reminderUnit={reminderUnit}
              reason={appointment.reason}
              note={appointment.note}
              internalNote={appointment.internalNote}
              department={appointment.department}
              onSubmit={() => {}}
            />
            <Button onClick={() => {}}>
              {t(i18nKeys.upcomingAppointmentCancelButton)}
            </Button>
          </Flex> */}
        </Collapse>
      </Flex>
    );
  }

  computeUpcomingAppointmentStatus = (
    startDateTime: number,
    confirmedAppointmentDates: number[]
  ) => {
    if (
      isNotNull(confirmedAppointmentDates) &&
      confirmedAppointmentDates.includes(startDateTime)
    ) {
      return 'Confirmed';
    }
    return 'Pending';
  };
}

export const CollapsibleAppointmentView = withTheme(
  withI18nContext(OriginalCollapsibleAppointmentView, {
    namespace: NAMESPACE_COLLAPSIBLE_APPOINTMENT_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
