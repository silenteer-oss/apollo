import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './collapsible-appointment-view.json';

export const NAMESPACE_COLLAPSIBLE_APPOINTMENT_VIEW =
  'collapsible-appointment-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
