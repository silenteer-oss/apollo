import React from 'react';
import { Subscription } from 'rxjs';
import {
  Intent,
  Alert,
  Radio,
  RadioGroup,
  Button,
  Divider,
} from '@blueprintjs/core';
import { isNotNull, isEmpty } from '@design-system/infrastructure/utils';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  Flex,
  BodyTextM,
  H2,
  LoadingState,
} from '@design-system/core/components';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '@careprovider/modules/appointment/module-config';
import AppointmentService from '../../services/AppointmentService';
import { withAppointmentContext, IAppointmentContext } from '../../context';
import { AppointmentDetailsView } from '../appointment-details';
import {
  IUpcomingAppointmentProps,
  IUpcomingAppointmentState,
} from './UpcomingAppointmentModel';
import { NAMESPACE_APPOINTMENT_UPCOMING_VIEW, i18nKeys } from './i18n';
import { CollapsibleAppointmentView } from './components/collapsible-appointment/CollapsibleAppointmentViewStyled';
import { ModuleToaster } from '../../components';
class OriginUpcomingAppointmentView extends React.PureComponent<
  IUpcomingAppointmentProps & IAppointmentContext & II18nFixedNamespaceContext,
  IUpcomingAppointmentState
  > {
  private _appointmentChangeLogSubscription: Subscription;
  private _reconnectedSubscription: Subscription;

  state: IUpcomingAppointmentState = {
    showFullContent: true,
    upcomingAppointments: undefined,
    doctors: undefined,
    nurses: undefined,
    isCancelAlertOpen: false,
    isCancelReasonOpen: false,
    cancelReasonId: undefined,
    cancelReasonList: [],
    reasonListLoading: true,
    showAllAppointment: false,
    selectedAppointment: undefined,
  };

  componentDidMount() {
    const { getDoctors, getNurses } = this.props;
    const { doctors, nurses } = this.state;

    if (!doctors || !nurses) {
      Promise.all([getDoctors(), getNurses()]).then(([doctors, nurses]) => {
        this.setState(
          {
            doctors: doctors.items,
            nurses: nurses.items,
          },
          () => {
            this.loadUpcomingAppointment();
          }
        );
      });
    }

    this._reconnectedSubscription = this.props.reconnected$.subscribe(() =>
      this.loadUpcomingAppointment()
    );

    this._appointmentChangeLogSubscription = this.props.appointmentChangeLog$.subscribe(
      message => {
        const { upcomingAppointments = [] } = this.state;
        if (
          upcomingAppointments.find(
            appointment => appointment.id === message.appointmentId
          )
        ) {
          this.loadUpcomingAppointment();
        }
      }
    );

    AppointmentService.getCancelReasonList()
      .then(result => {
        const defaultReason = result[0] ? result[0].id : undefined;
        this.setState({
          reasonListLoading: false,
          cancelReasonList: result,
          cancelReasonId: defaultReason,
        });
      })
      .catch(() => {
        this.setState({
          reasonListLoading: false,
          cancelReasonList: [],
          cancelReasonId: undefined,
        });
      });
  }

  componentWillUnmount() {
    if (this._reconnectedSubscription) {
      this._reconnectedSubscription.unsubscribe();
      this._reconnectedSubscription = null;
    }
    if (this._appointmentChangeLogSubscription) {
      this._appointmentChangeLogSubscription.unsubscribe();
      this._appointmentChangeLogSubscription = null;
    }
  }

  componentDidUpdate(
    prevProps: IUpcomingAppointmentProps & IAppointmentContext
  ) {
    const { patientId } = this.props;

    if (patientId !== prevProps.patientId) {
      this.loadUpcomingAppointment();
    }
  }

  render() {
    const { className, patientId, t } = this.props;
    const {
      upcomingAppointments = [],
      cancelReasonId,
      cancelReasonList,
      isCancelReasonOpen,
      reasonListLoading,
    } = this.state;

    if (!patientId) {
      return null;
    }

    if (isCancelReasonOpen && cancelReasonList.length === 0) {
      return null;
    }

    let defaultCancelReasonId = undefined;
    if (cancelReasonList.length > 0) {
      defaultCancelReasonId = cancelReasonList[0].id;
    }

    if (!upcomingAppointments || upcomingAppointments.length === 0) {
      return (
        <Flex className={className} auto column>
          <BodyTextM
            margin={'0 0 16px  0'}
            color={this.props.theme.foreground['02']}
          >
            {t(i18nKeys.noAppointment)}
          </BodyTextM>
          <AppointmentDetailsView
            className="button"
            buttonLabel={t(i18nKeys.setAppointmentButton)}
            patientId={patientId}
            onSubmit={this.loadUpcomingAppointment}
          />
        </Flex>
      );
    }

    return (
      <Flex className={className} auto column>
        <AppointmentDetailsView
          className="button"
          buttonLabel={t(i18nKeys.setAppointmentButton)}
          patientId={patientId}
          onSubmit={this.loadUpcomingAppointment}
        />
        <Flex className="upcoming-appointment">
          {this.renderAppointments()}
        </Flex>
        <Alert
          className="upcoming-cancel-alert bp3-alert-fill"
          cancelButtonText={t(i18nKeys.cancelAppointmentDialogNoButton)}
          confirmButtonText={t(i18nKeys.cancelAppointmentDialogYesButton)}
          intent={Intent.DANGER}
          isOpen={this.state.isCancelAlertOpen}
          onConfirm={this.showCancelReasonAlert}
          onCancel={this.closeCancelAppointmentAlert}
        >
          <H2>{t(i18nKeys.cancelAppointmentDialogMessage)}</H2>
        </Alert>
        <Alert
          className="upcoming-cancel-alert bp3-alert-fill"
          cancelButtonText={t(i18nKeys.cancelReasonDialogNoButton)}
          confirmButtonText={t(i18nKeys.cancelReasonDialogYesButton)}
          intent={Intent.DANGER}
          isOpen={this.state.isCancelReasonOpen}
          onConfirm={this.doCancelAppointment}
          onCancel={this.closeCancelReasonAlert}
        >
          {reasonListLoading ? (
            <LoadingState />
          ) : (
              <Flex column>
                <H2 margin="0 0 12px 0">{t(i18nKeys.editedReasonTitle)}</H2>
                <RadioGroup
                  className="radio-group"
                  onChange={this.setCancelReason}
                  selectedValue={
                    isEmpty(cancelReasonId)
                      ? defaultCancelReasonId
                      : cancelReasonId
                  }
                >
                  {cancelReasonList
                    .filter(cancelReason => 'VI' === cancelReason.language)
                    .map(item => (
                      <Radio
                        key={item.key}
                        label={item.content}
                        value={item.id}
                        className="inline-radio"
                      />
                    ))}
                </RadioGroup>
              </Flex>
            )}
        </Alert>
      </Flex>
    );
  }

  renderAppointments = () => {
    const { patientId, t } = this.props;
    const {
      doctors,
      nurses,
      upcomingAppointments,
      showAllAppointment,
    } = this.state;

    if (showAllAppointment) {
      return upcomingAppointments.map((appointment, index) =>
        this.renderAppointment(appointment, index === 0)
      );
    }

    return (
      <>
        <CollapsibleAppointmentView
          patientId={patientId}
          appointment={upcomingAppointments[0]}
          doctors={doctors}
          nurses={nurses}
          onCancelAppointment={this.showCancelAppointmentAlert}
          expandByDefault
        />
        {upcomingAppointments.length > 1 && (
          <Button
            minimal
            intent="primary"
            alignText="left"
            onClick={this.showAllAppointments}
          >
            {t(i18nKeys.showAllAppointment, {
              count: upcomingAppointments.length,
            })}
          </Button>
        )}
      </>
    );
  };

  renderAppointment = (appointment, expandByDefault) => {
    const { patientId } = this.props;
    const { doctors, nurses } = this.state;

    return (
      <>
        <CollapsibleAppointmentView
          patientId={patientId}
          appointment={appointment}
          doctors={doctors}
          nurses={nurses}
          expandByDefault={expandByDefault}
          onCancelAppointment={this.showCancelAppointmentAlert}
        />
        <Divider className="divider" />
      </>
    );
  };

  showAllAppointments = () => this.setState({ showAllAppointment: true });

  setCancelReason = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ cancelReasonId: event.currentTarget.value });
  };

  loadUpcomingAppointment = async () => {
    const _upcomingAppointments = await AppointmentService.getUpcomingAppointment(
      this.props.patientId
    );

    this.setState({
      upcomingAppointments: _upcomingAppointments,
      isCancelAlertOpen: false,
      isCancelReasonOpen: false,
      showAllAppointment: false,
    });
  };

  showCancelAppointmentAlert = selectedAppointment => {
    this.setState({ isCancelAlertOpen: true, selectedAppointment });
  };

  closeCancelAppointmentAlert = () => {
    this.setState({
      isCancelAlertOpen: false,
      cancelReasonId: undefined,
      selectedAppointment: undefined,
    });
  };

  showCancelReasonAlert = () => {
    this.setState({
      isCancelAlertOpen: false,
      isCancelReasonOpen: true,
    });
  };

  closeCancelReasonAlert = () => {
    this.setState({
      isCancelReasonOpen: false,
      isCancelAlertOpen: false,
      cancelReasonId: undefined,
    });
  };

  doCancelAppointment = () => {
    const { cancelReasonId, selectedAppointment } = this.state;
    if (!selectedAppointment) return;

    const { id, startDateTime } = selectedAppointment;
    const { t } = this.props;
    AppointmentService.cancelAppointment(
      id,
      startDateTime,
      cancelReasonId
    ).then(success => {
      if (success) {
        ModuleToaster.show({
          intent: Intent.SUCCESS,
          message: t(i18nKeys.cancelAppointmentSuccess),
        });
        this.loadUpcomingAppointment();
      }
    });
  };

  computeUpcomingAppointmentStatus = (
    startDateTime: number,
    confirmedAppointmentDates: number[]
  ) => {
    if (
      isNotNull(confirmedAppointmentDates) &&
      confirmedAppointmentDates.includes(startDateTime)
    ) {
      return 'Confirmed';
    }
    return 'Pending';
  };

  expand = () => {
    this.setState({ showFullContent: true });
  };

  collapse = () => {
    this.setState({ showFullContent: false });
  };
}

export const UpcomingAppointmentView = withTheme(
  withAppointmentContext(
    withI18nContext(OriginUpcomingAppointmentView, {
      namespace: NAMESPACE_APPOINTMENT_UPCOMING_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
