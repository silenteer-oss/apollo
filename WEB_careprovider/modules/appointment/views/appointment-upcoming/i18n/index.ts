import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-upcoming-view.json';

export const NAMESPACE_APPOINTMENT_UPCOMING_VIEW = 'appointment-upcoming-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
