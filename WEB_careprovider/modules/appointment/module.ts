import {
  APPOINTMENT_MODULE_ID,
  APPOINTMENT_DETAILS_VIEW_ID,
  APPOINTMENT_CHANGE_LOG_VIEW_ID,
  UPCOMING_APPOINTMENT_VIEW_ID,
  APPOINTMENT_FILTER_VIEW_ID,
  APPOINTMENT_COLUMN_LIST_VIEW_ID,
  APPOINTMENT_CONTEXT_PROVIDER_ID,
  APPOINTMENT_CONTEXT_CONSUMER_ID,
} from './module-config';

import {
  AppointmentDetailsView,
  AppointmentChangeLogView,
  UpcomingAppointmentView,
  AppointmentFilterView,
  AppointmentColumnListView,
} from './views';

import {
  AppointmentContextProvider,
  AppointmentContextConsumer,
} from './context';

export default {
  [APPOINTMENT_MODULE_ID]: {
    [APPOINTMENT_DETAILS_VIEW_ID]: AppointmentDetailsView,
    [APPOINTMENT_CHANGE_LOG_VIEW_ID]: AppointmentChangeLogView,
    [UPCOMING_APPOINTMENT_VIEW_ID]: UpcomingAppointmentView,
    [APPOINTMENT_FILTER_VIEW_ID]: AppointmentFilterView,
    [APPOINTMENT_COLUMN_LIST_VIEW_ID]: AppointmentColumnListView,
    [APPOINTMENT_CONTEXT_PROVIDER_ID]: AppointmentContextProvider,
    [APPOINTMENT_CONTEXT_CONSUMER_ID]: AppointmentContextConsumer,
  },
};
