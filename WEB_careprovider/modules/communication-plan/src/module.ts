import {
  COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID,
  COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID,
  COMMUNICATION_PLAN_MODULE_ID,
  COMMUNICATION_PLAN_LIST_VIEW_ID,
  COMMUNICATION_PLAN_TIMELINE_VIEW_ID,
  COMMUNICATION_PLAN_PATIENT_VIEW_ID,
} from '../module-config';

import {
  CommunicationPlanListView,
  CommunicationPlanTimelineView,
  CommunicationPlanPatienView,
} from './views';

import {
  CommunicationPlanContextProvider,
  CommunicationPlanContextConsumer,
} from './context';

export default {
  [COMMUNICATION_PLAN_MODULE_ID]: {
    [COMMUNICATION_PLAN_LIST_VIEW_ID]: CommunicationPlanListView,
    [COMMUNICATION_PLAN_TIMELINE_VIEW_ID]: CommunicationPlanTimelineView,
    [COMMUNICATION_PLAN_PATIENT_VIEW_ID]: CommunicationPlanPatienView,
    [COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID]: CommunicationPlanContextProvider,
    [COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID]: CommunicationPlanContextConsumer,
  },
};
