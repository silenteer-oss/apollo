import { IEmployeeProfile } from '@careprovider/services/biz';

export interface ICommunicationPlanContext {
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
}

export interface ICommunicationPlanContextProviderProps {
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
}
