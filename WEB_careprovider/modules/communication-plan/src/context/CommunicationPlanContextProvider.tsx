import React from 'react';
import {
  ICommunicationPlanContext,
  ICommunicationPlanContextProviderProps,
} from './models';
import { CommunicationPlanContext } from './CommunicationPlanContext';

export class CommunicationPlanContextProvider extends React.PureComponent<
  ICommunicationPlanContextProviderProps
> {
  private _CommunicationPlanContext: ICommunicationPlanContext;

  constructor(props: ICommunicationPlanContextProviderProps) {
    super(props);

    this._CommunicationPlanContext = {
      getEmployeeProfiles: props.getEmployeeProfiles,
    };
  }

  render() {
    return (
      <CommunicationPlanContext.Provider value={this._CommunicationPlanContext}>
        {this.props.children}
      </CommunicationPlanContext.Provider>
    );
  }
}
