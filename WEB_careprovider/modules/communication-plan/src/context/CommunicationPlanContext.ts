import React from 'react';
import { ICommunicationPlanContext } from './models';

const defaultContext: ICommunicationPlanContext = {
  getEmployeeProfiles: () => Promise.resolve(undefined),
};

export const CommunicationPlanContext = React.createContext(defaultContext);
