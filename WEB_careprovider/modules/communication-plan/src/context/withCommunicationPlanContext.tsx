import React, { ComponentType, FunctionComponent } from 'react';
import { ICommunicationPlanContext } from './models';
import { CommunicationPlanContext } from './CommunicationPlanContext';

export function withCommunicationPlanContext<TProps>(
  Component: ComponentType<TProps & ICommunicationPlanContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <CommunicationPlanContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </CommunicationPlanContext.Consumer>
    );
  };
}
