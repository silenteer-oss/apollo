import React, { ReactNode } from 'react';
import { ICommunicationPlanContext } from './models';
import { CommunicationPlanContext } from './CommunicationPlanContext';

export class CommunicationPlanContextConsumer extends React.PureComponent<{
  children: (value: ICommunicationPlanContext) => ReactNode;
}> {
  render() {
    return (
      <CommunicationPlanContext.Consumer>
        {this.props.children}
      </CommunicationPlanContext.Consumer>
    );
  }
}
