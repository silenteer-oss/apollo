export function debounce(delay: number, originalFunc: Function) {
  let timeout;

  return function(...args) {
    const functionCall = () => originalFunc.apply(this, args);

    clearTimeout(timeout);
    timeout = setTimeout(functionCall, delay);
  };
}
