import { parseBoolean } from '@design-system/infrastructure/utils';
import {
  COMMUNICATION_PLAN_PAGE_PATH,
  COMMUNICATION_PLAN_DETAIL_PAGE_PATH,
  COMMUNICATION_PLAN_PATIENT_PAGE_PATH,
} from '@careprovider/route';

const _isDevelop = process.env.NODE_ENV === 'develop';
const _isLocalMode = parseBoolean(process.env.IS_LOCAL_MODE);

export function getCommunicationPlanPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/communication-plan'
    : COMMUNICATION_PLAN_PAGE_PATH;
}

export function getCommunicationPlanDetailPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/communication-plan/:planId'
    : COMMUNICATION_PLAN_DETAIL_PAGE_PATH;
}

export function getPatientCommunicationPlanPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/communication-plan-patient'
    : COMMUNICATION_PLAN_PATIENT_PAGE_PATH;
}
