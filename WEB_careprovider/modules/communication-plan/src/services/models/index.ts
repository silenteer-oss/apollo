import {
  CommunicationPlanResponse,
  CommunicationPlanSummaryResponse,
  PatientCommunicationPlanResponse,
  PlanWeekResponse,
  PlanDayResponse,
  PlanStatus,
  PatientPlanStatus,
  DurationUnit as PlanDurationUnit,
  CommunicationPlanOrderProperty as PlanOrderBy,
} from 'resource/communication-plan-app-resource/model';

export { PlanStatus, PatientPlanStatus, PlanDurationUnit, PlanOrderBy };

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommunicationPlan extends CommunicationPlanResponse {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommunicationPlanSummary
  extends CommunicationPlanSummaryResponse {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IPatientCommunicationPlan
  extends PatientCommunicationPlanResponse {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IPlanDay extends PlanDayResponse {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IPlanWeek extends PlanWeekResponse {}
