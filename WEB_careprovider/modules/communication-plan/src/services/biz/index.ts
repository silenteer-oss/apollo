import {
  ChangeDurationRequest,
  CommunicationPlanCreateRequest,
  SearchCommunicationPlanRequest,
  PatientCommunicationCreateRequest,
  PatientCommunicationPlanResponse,
  PatientPlanMessageResponse,
  PlanDayCreateRequest,
  EditMessageRequest,
  ErrorMessageAction,
  PatientCommunicationPlanCountResponse,
} from 'resource/communication-plan-app-resource/model';
import { CommunicationPlanApi } from 'resource/CommunicationPlanResource';
import {
  ICommunicationPlan,
  ICommunicationPlanSummary,
  IPatientCommunicationPlan,
  IPlanWeek,
  PlanStatus,
  PlanOrderBy,
} from '../models';

export function search(
  searchText: string,
  orderBy: PlanOrderBy,
  statuses: PlanStatus[],
  limit: number = 50,
  offset: number = 0
): Promise<ICommunicationPlanSummary[]> {
  return CommunicationPlanApi.search({
    statuses,
    limit: Math.max(Math.min(50, limit), 0),
    offset: Math.max(offset, 0),
    nameQuery: searchText,
    orderProperty: orderBy,
  } as SearchCommunicationPlanRequest).then(response => response.data);
}

export function createPlan(
  name: string,
  duration: number,
  durationUnit: string
): Promise<ICommunicationPlanSummary> {
  const request = {
    name,
    duration,
    durationUnit,
  } as CommunicationPlanCreateRequest;

  return CommunicationPlanApi.create(request).then(response => response.data);
}

export function publish(planId: string): Promise<void> {
  return CommunicationPlanApi.changeStatus(planId, {
    status: PlanStatus.PUBLISHED,
  }).then(response => response.data);
}

export function unpublish(planId: string): Promise<void> {
  return CommunicationPlanApi.changeStatus(planId, {
    status: PlanStatus.DRAFT,
  }).then(response => response.data);
}

export function deletePlan(planId: string): Promise<void> {
  return CommunicationPlanApi.deletePlan(planId).then(
    response => response.data
  );
}

export function getActivePatient(
  planId: string
): Promise<PatientCommunicationPlanCountResponse> {
  return CommunicationPlanApi.getActivePatientCount(planId).then(
    response => response.data
  );
}

export function updatePlanTimeline(
  planId: string,
  changes: {
    duration: number;
    retainedOrNewWeeks: Array<{
      id: string;
      ordinal: number;
      days: Array<{
        id: string;
        ordinal: number;
        message: string;
      }>;
    }>;
  }
): Promise<ICommunicationPlan> {
  return CommunicationPlanApi.changeTimeLine(
    planId,
    changes as ChangeDurationRequest
  ).then(response => response.data);
}

export function deleteWeek(planId: string, weekId: string): Promise<void> {
  return CommunicationPlanApi.deleteWeek(planId, weekId).then(
    response => response.data
  );
}

export function createDay(
  planId: string,
  weekOrdinal: number,
  dayOrdinal: number,
  message: string
): Promise<IPlanWeek> {
  const request = {
    weekOrdinal,
    dayOrdinal,
    message,
  } as PlanDayCreateRequest;

  return CommunicationPlanApi.createDay(planId, request).then(
    response => response.data
  );
}

export function editDay(
  planId: string,
  dayId: string,
  message: string
): Promise<void> {
  const request = {
    message,
  } as EditMessageRequest;

  return CommunicationPlanApi.editDay(planId, dayId, request).then(
    response => response.data
  );
}

export function deleteDay(planId: string, dayId: string): Promise<void> {
  return CommunicationPlanApi.deleteDay(planId, dayId).then(
    response => response.data
  );
}

export function getPlanDetail(planId: string): Promise<ICommunicationPlan> {
  return CommunicationPlanApi.get(planId).then(response => response.data);
}

export function getOngoingPlan(
  patientId: string
): Promise<IPatientCommunicationPlan[]> {
  return CommunicationPlanApi.getOngoingPlanForPatient(patientId).then(
    response => response.data
  );
}

export function getUpcomingMessages(
  patientId: string,
  limit,
  offset
): Promise<PatientPlanMessageResponse[]> {
  return CommunicationPlanApi.findUpcomingMessagesForPatient(patientId, {
    limit,
    offset,
  }).then(response => response.data);
}

export function getFailedMessage(
  patientId: string
): Promise<PatientPlanMessageResponse[]> {
  return CommunicationPlanApi.getErrorMessageForPatient(patientId).then(
    response => response.data
  );
}

export function resendFailedMessage(
  patientId: string,
  messageId: string
): Promise<void> {
  return CommunicationPlanApi.handleErrorMessage(patientId, messageId, {
    action: ErrorMessageAction.RESEND,
  }).then(response => response.data);
}

export function skipFailedMessage(
  patientId: string,
  messageId: string
): Promise<void> {
  return CommunicationPlanApi.handleErrorMessage(patientId, messageId, {
    action: ErrorMessageAction.SKIP,
  }).then(response => response.data);
}

export function addPlanToPatient(
  patientId: string,
  request: PatientCommunicationCreateRequest
): Promise<PatientCommunicationPlanResponse> {
  return CommunicationPlanApi.associate(patientId, request).then(
    response => response.data
  );
}

export function disassociateFromPatient(patientId: string): Promise<void> {
  return CommunicationPlanApi.disassociate(patientId).then(
    response => response.data
  );
}
