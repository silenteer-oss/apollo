import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './communication-plan-timeline-day.json';

export const NAMESPACE_COMMUNICATION_PLAN_TIMELINE_DAY =
  'communication-plan-timeline-day';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
