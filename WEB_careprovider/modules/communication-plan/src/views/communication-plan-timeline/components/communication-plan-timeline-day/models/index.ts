import { ICareProviderTheme } from '@careprovider/theme';
import { IPlanDay } from '../../../../../services';

export interface ICommunicationPlanTimelineDayProps {
  className?: string;
  theme?: ICareProviderTheme;
  disallowDnD: boolean;
  isEditable: boolean;
  isEditing: boolean;
  day: IPlanDay;
  onStartEditMessage: (day: IPlanDay) => void;
  onEndEditMessage: (day: IPlanDay) => void;
  onSaveMessage: (day: IPlanDay) => void;
  onDeleteMessage: (day: IPlanDay) => void;
}

export interface ICommunicationPlanTimelineDayState {
  isDisabledSave: boolean;
}
