import React from 'react';
import { Flex, BodyTextM } from '@design-system/core/components';
import { isEmpty, getCssClass } from '@design-system/infrastructure/utils';
import { Button } from '@blueprintjs/core';
import TextareaAutosize from 'react-textarea-autosize';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getModuleConfig } from '../../../../../module-config';
import { NAMESPACE_COMMUNICATION_PLAN_TIMELINE_DAY, i18nKeys } from './i18n';
import {
  ICommunicationPlanTimelineDayProps,
  ICommunicationPlanTimelineDayState,
} from './models';

import DotBlack from '@careprovider/assets/images/dot-black.svg';
import DotGray from '@careprovider/assets/images/dot-gray.svg';

class OriginCommunicationPlanTimelineDay extends React.PureComponent<
  ICommunicationPlanTimelineDayProps & II18nFixedNamespaceContext,
  ICommunicationPlanTimelineDayState
> {
  private _inputRef = React.createRef<HTMLTextAreaElement>();

  constructor(
    props: ICommunicationPlanTimelineDayProps & II18nFixedNamespaceContext
  ) {
    super(props);

    this.state = {
      isDisabledSave: true,
    };
  }

  componentDidUpdate(prevProps: ICommunicationPlanTimelineDayProps) {
    const {
      day: { message },
    } = this.props;

    if (prevProps.day.message !== message) {
      this._updateSaveButtonStatus(message);
    }
  }

  render() {
    const {
      className,
      isEditable,
      isEditing,
      disallowDnD,
      day,
      t,
    } = this.props;

    if (!day) {
      return null;
    }

    return (
      <Flex
        className={getCssClass(
          className,
          isEditable ? 'highlightable' : '',
          isEditing ? 'is-editing' : '',
          day.message && isEditable ? 'editable' : ''
        )}
      >
        <Flex className={'content-wrapper'}>
          <Flex className={disallowDnD ? '' : 'day-drag-handle'}>
            <Flex className={'dot'} align="flex-start">
              <img src={isEditing ? DotBlack : DotGray} />
            </Flex>
            <Flex className={`ordinal`}>
              <BodyTextM
                fontWeight={isEditing ? 'SemiBold' : 'Normal'}
                className={'editing'}
              >
                {t(i18nKeys.day, { number: day.ordinal })}
              </BodyTextM>
            </Flex>
          </Flex>
          {isEditing && this._renderInput()}
          {!isEditing && this._renderText()}
        </Flex>
        <Flex className={'edit-group'} align="flex-start">
          {isEditing && (
            <Flex className={'create-group'}>
              <Button className={'btn-cancel'} onClick={this.endEditMessage}>
                {t(i18nKeys.cancel)}
              </Button>
              <Button
                className={'btn-save'}
                disabled={this.state.isDisabledSave}
                onClick={this.saveMessage}
              >
                {t(i18nKeys.save)}
              </Button>
            </Flex>
          )}
          {!isEditing && (
            <Button className={'btn-delete'} onClick={this.deleteMessage}>
              {t(i18nKeys.delete)}
            </Button>
          )}
        </Flex>
      </Flex>
    );
  }

  startEditMessage = (): void => {
    const { day, isEditable, isEditing, onStartEditMessage } = this.props;

    if (isEditable && !isEditing) {
      onStartEditMessage(day);
    }
  };

  endEditMessage = (): void => {
    const { day, onEndEditMessage } = this.props;

    onEndEditMessage(day);
  };

  saveMessage = (): void => {
    const message = this._inputRef.current.value;
    if (isEmpty(message)) {
      return;
    }

    const { day, onSaveMessage } = this.props;

    onSaveMessage({ ...day, message });
  };

  deleteMessage = (): void => {
    const { day, onDeleteMessage } = this.props;

    onDeleteMessage(day);
  };

  private _renderInput = () => {
    const {
      day: { message },
      t,
    } = this.props;

    return (
      <Flex className={'input-content'}>
        <TextInput
          ref={this._inputRef}
          message={message}
          t={t}
          onChange={this._handleChangedText}
        />
      </Flex>
    );
  };

  private _renderText = () => {
    const {
      isEditing,
      isEditable,
      day: { message },
    } = this.props;

    return (
      <Flex
        className={getCssClass(
          'text-content',
          isEditable && !isEditing ? 'is-editable' : '',
          isEditable && !isEditing && !message ? 'empty' : ''
        )}
        onClick={this.startEditMessage}
      >
        <BodyTextM>{message}</BodyTextM>
      </Flex>
    );
  };

  private _handleChangedText = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ): void => {
    this._updateSaveButtonStatus(this._inputRef.current.value);
  };

  private _updateSaveButtonStatus = (message: string): void => {
    const { isDisabledSave } = this.state;
    const _isEmptyText = isEmpty(message);

    if (_isEmptyText && !isDisabledSave) {
      this.setState({ isDisabledSave: true });
    } else if (!_isEmptyText && isDisabledSave) {
      this.setState({ isDisabledSave: false });
    }
  };
}

const TextInput = React.forwardRef(
  (
    props: {
      message?: string;
      t?: any;
      onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
    },
    ref: React.RefObject<HTMLTextAreaElement>
  ) => {
    React.useEffect(() => {
      ref.current.selectionStart = props.message ? props.message.length : 0;
      ref.current.selectionEnd = props.message ? props.message.length : 0;
    }, []);

    return (
      <TextareaAutosize
        className="input"
        autoFocus
        inputRef={ref}
        minRows={1}
        maxLength={2000}
        placeholder={props.t(i18nKeys.inputTextPlaceholder)}
        defaultValue={props.message}
        onChange={props.onChange}
      />
    );
  }
);

export const CommunicationPlanTimelineDay = withI18nContext(
  OriginCommunicationPlanTimelineDay,
  {
    namespace: NAMESPACE_COMMUNICATION_PLAN_TIMELINE_DAY,
    publicAssetPath: getModuleConfig().publicAssetPath,
  }
);
