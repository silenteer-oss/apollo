import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { CommunicationPlanTimelineDay as OriginCommunicationPlanTimelineDay } from './CommunicationPlanTimelineDay';
import { ICommunicationPlanTimelineDayProps } from './models';
import { scaleSpacePx, scaleSpace } from '@design-system/core/styles';

export const CommunicationPlanTimelineDay = styled(
  OriginCommunicationPlanTimelineDay
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanTimelineDay', className),
}))<ICommunicationPlanTimelineDayProps>`
  ${props => {
    const { foreground, background, space } = props.theme;
    return `
      & {
        min-height: ${scaleSpacePx(12)};
        padding: ${space.xs} 0;

        &.highlightable {
          > .content-wrapper {
            &:hover {
              background-color: ${background.primary.lighter};

              .text-content.empty {
                border: 1px solid #9AA7B2;
                box-sizing: border-box;
                border-radius: 3px;
                background-color: ${background.white};
              }
            }
          }
        }

        &.is-editing {
          > .content-wrapper {
            background-color: ${background.primary.lighter};
          }
        }

        > .content-wrapper {
          flex: 1;
          padding: 5px 17px 5px 3px;

          .day-drag-handle {
            flex-direction: row;

            &:hover {
              cursor: pointer;
            }
          }

          .dot {
            width: 16px;
            margin-top: ${space.xs};
            margin-left: ${space.xs};
            margin-right: ${space.s};
            padding-top: calc(${space.xs} + 1px);
          }

          .ordinal {
            margin-right: ${scaleSpacePx(7)};
            padding-top: calc(${space.xs} + 1px);

            > p {
              min-width: ${scaleSpacePx(14)};
              white-space: nowrap;
            }
          }

          .text-content {
            width: 100%;
            padding: ${scaleSpace(2) + 1}px ${scaleSpace(5) + 2}px;

            > p {
              white-space: pre-wrap;
            }

            &.is-editable {
              cursor: pointer;
            }
          }

          .input-content {
            margin-bottom: 0px;
            width: 100%;

            .input {
              width: 100%;
              overflow: hidden;
              resize: none;
              border: 1px solid #9AA7B2;
              box-sizing: border-box;
              border-radius: 3px;
              line-height: ${scaleSpacePx(5)};
              padding: ${scaleSpacePx(2)} ${scaleSpacePx(5)};
              font-size: 14px;
            }
          }
        }

        > .edit-group {
          width: 130px;
          margin-left: ${scaleSpacePx(4)};
          align-items: center;

          .create-group {
            margin-right: -${scaleSpace(3) - 2}px !important;

            .bp3-button {
              min-width: 0px !important;
              min-height: 0px !important;
              background: none !important;
              padding: ${scaleSpacePx(2)} ${scaleSpacePx(3)};
              font-weight: 600;
            }

            .btn-cancel {
              color: ${foreground['02']} !important;
            }

            .btn-save {
              color: ${foreground.primary.base};
            }
          }

          > .bp3-button {
            min-height: 0px !important;
            min-width: 0px !important;
            background: none !important;
            padding: ${scaleSpacePx(0)} ${scaleSpacePx(2)} !important;
            font-weight: 600;
          }

          .btn-delete {
            display: none;
            color: ${foreground.error.base};
          }
        }

        &.editable {
          &:hover {
            > .edit-group {
              .btn-delete {
                display: block;
              }

              .btn-edit {
                display: block;
              }
            }
          }
        }
      }
    `;
  }}
`;
