import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './change-communication-plan-duration-dialog.json';

export const NAMESPACE_CHANGE_COMMUNICATION_PLAN_DURATION =
  'change-communication-plan-duration-dialog';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
