import React from 'react';
import { Classes, Dialog, Button, NumericInput } from '@blueprintjs/core';
import { parseNumber } from '@design-system/infrastructure/utils';
import { Flex, Box } from '@design-system/core/components';
import { scaleSpacePx } from '@design-system/core/styles';
import {
  IGlobalStyleContext,
  withGlobalStyleContext,
} from '@design-system/themes/context';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../../../module-config';
import { NAMESPACE_CHANGE_COMMUNICATION_PLAN_DURATION, i18nKeys } from './i18n';
import { mixGlobalStyle } from './styles';
import {
  IChangeCommunicationPlanDurationDialogProps,
  IChangeCommunicationPlanDurationDialogState,
} from './models';

import AlertUrl from '@careprovider/assets/images/alert-circle-solid.svg';

class OriginChangeCommunicationPlanDurationDialog extends React.PureComponent<
  IChangeCommunicationPlanDurationDialogProps &
    IGlobalStyleContext &
    II18nFixedNamespaceContext,
  IChangeCommunicationPlanDurationDialogState
> {
  private _inputRef = React.createRef<NumericInput>();

  state: IChangeCommunicationPlanDurationDialogState = {
    isReady: false,
    isInvalidDuration: false,
  };

  componentDidMount() {
    const { theme, addGlobalStyle } = this.props;
    addGlobalStyle(
      'changeCommunicationPlanDurationDialog',
      mixGlobalStyle(theme)
    );
    this.setState({ isReady: true });
  }

  render() {
    const { isReady, isInvalidDuration } = this.state;
    const {
      className,
      isOpen,
      currentDuration,
      t,
      theme: { space },
    } = this.props;

    if (!isReady) {
      return null;
    }

    const _value = currentDuration > 0 ? currentDuration : 1;

    return (
      <Dialog
        className={className}
        title={t(i18nKeys.title)}
        isOpen={isOpen}
        isCloseButtonShown={false}
        canOutsideClickClose={false}
      >
        <div className={Classes.DIALOG_BODY}>
          <Flex mb={scaleSpacePx(2)} align="center">
            <NumericInput
              ref={this._inputRef}
              value={_value}
              buttonPosition="none"
              min={1}
              max={52}
              maxLength={2}
            />
            <Box ml={space.s}>{t(i18nKeys.weeks)}</Box>
          </Flex>
          <Flex className="error-message">
            {isInvalidDuration && <p>{t(i18nKeys.infoMessage)}</p>}
          </Flex>
          <Flex align="center">
            <Box mr={scaleSpacePx(3)}>
              <img src={AlertUrl} />
            </Box>
            {t(i18nKeys.warnMessage)}
          </Flex>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <Button text={t(i18nKeys.cancel)} onClick={this.onCancel} />
          <Button
            intent="primary"
            text={t(i18nKeys.save)}
            onClick={this.save}
          />
        </div>
      </Dialog>
    );
  }

  onCancel = () => {
    this.setState({ isInvalidDuration: false });
    if (this.props.onCancel) this.props.onCancel();
  };

  save = () => {
    const _value = parseNumber(this._inputRef.current.state.value);
    if (_value > 0 && _value < 53) {
      this.props.onSave(_value);
      this.setState({ isInvalidDuration: false });
    } else {
      this.setState({ isInvalidDuration: true });
    }
  };
}

export const ChangeCommunicationPlanDurationDialog = withTheme(
  withGlobalStyleContext(
    withI18nContext(OriginChangeCommunicationPlanDurationDialog, {
      namespace: NAMESPACE_CHANGE_COMMUNICATION_PLAN_DURATION,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
