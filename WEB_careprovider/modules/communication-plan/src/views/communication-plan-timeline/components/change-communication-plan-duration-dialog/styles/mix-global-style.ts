import { ICareProviderTheme } from '@careprovider/theme';

export function mixGlobalStyle(theme: ICareProviderTheme): string {
  const { space } = theme;

  return `
    .sl-ChangeCommunicationPlanDurationDialog.bp3-dialog {
      .bp3-dialog-body {
        .bp3-numeric-input {
          flex: 1;
          > .bp3-input-group {
            flex: 1;
          }
        }
      }

      .bp3-dialog-footer {
        height: auto;
        padding-bottom: ${space.m};

        .bp3-button {
          flex: 1;

          + .bp3-button {
            margin-left: ${space.m};
          }
        }
      }
    }
  `;
}
