import { ICareProviderTheme } from '@careprovider/theme';

export interface IChangeCommunicationPlanDurationDialogProps {
  className?: string;
  theme?: ICareProviderTheme;
  isOpen: boolean;
  currentDuration: number;
  onCancel: () => void;
  onSave: (duration: number) => void;
}

export interface IChangeCommunicationPlanDurationDialogState {
  isReady: boolean;
  isInvalidDuration: boolean;
}
