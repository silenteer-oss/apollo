import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { scaleSpacePx } from '@design-system/core/styles';
import { ChangeCommunicationPlanDurationDialog as OriginChangeCommunicationPlanDurationDialog } from './ChangeCommunicationPlanDurationDialog';
import { IChangeCommunicationPlanDurationDialogProps } from './models';

export const ChangeCommunicationPlanDurationDialog = styled(
  OriginChangeCommunicationPlanDurationDialog
).attrs(({ className }) => ({
  className: getCssClass('sl-ChangeCommunicationPlanDurationDialog', className),
}))<IChangeCommunicationPlanDurationDialogProps>`
  ${props => {
    const { foreground } = props.theme;
    return `
    & {
      .error-message {
        height: ${scaleSpacePx(5)}
        margin-botton: ${scaleSpacePx(2)}
        p {
          color: ${foreground.error.base};
        }

      }
    }
  `;
  }}
`;
