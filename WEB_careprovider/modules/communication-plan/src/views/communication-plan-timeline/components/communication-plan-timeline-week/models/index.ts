import { ICareProviderTheme } from '@careprovider/theme';
import { IPlanDay, IPlanWeek } from '../../../../../services';

export interface ICommunicationPlanTimelineWeekProps {
  className?: string;
  theme?: ICareProviderTheme;
  disallowDnD: boolean;
  disallowDelete: boolean;
  disallowEditDayMessage: boolean;
  isEditable: boolean;
  week: IPlanWeek;
  shouldExpandAtFirstLoad: boolean;
  onDeleteWeek: (week: IPlanWeek) => void;
  onChangedDayOrdinal: (week: IPlanWeek) => void;
  onStartEditDayMessage: (week: IPlanWeek, day: IPlanDay) => void;
  onEndEditDayMessage: (week: IPlanWeek, day: IPlanDay) => void;
  onSaveDayMessage: (week: IPlanWeek, day: IPlanDay) => void;
  onDeleteDayMessage: (week: IPlanWeek, day: IPlanDay) => void;
}

export interface ICommunicationPlanTimelineWeekState {
  collapsed: boolean;
  editingDayId?: string;
}
