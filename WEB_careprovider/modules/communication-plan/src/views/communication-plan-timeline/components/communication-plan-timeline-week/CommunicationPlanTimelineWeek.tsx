import React from 'react';
import { Container, Draggable } from 'react-smooth-dnd';
import { Flex, H4, BodyTextS } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../../../module-config';
import { IPlanDay } from '../../../../services';
import { CommunicationPlanTimelineDay } from '../communication-plan-timeline-day';
import {
  ICommunicationPlanTimelineWeekProps,
  ICommunicationPlanTimelineWeekState,
} from './models';
import { NAMESPACE_COMMUNICATION_PLAN_TIMELINE_WEEK, i18nKeys } from './i18n';

import ArrowDownButton from '@careprovider/assets/images/arrow-down-button.svg';
import DragAnchor from '@careprovider/assets/images/drag-anchor.svg';
import Remove from '@careprovider/assets/images/remove.svg';

class OriginCommunicationPlanTimelineWeek extends React.PureComponent<
  ICommunicationPlanTimelineWeekProps & II18nFixedNamespaceContext,
  ICommunicationPlanTimelineWeekState
> {
  constructor(
    props: ICommunicationPlanTimelineWeekProps & II18nFixedNamespaceContext
  ) {
    super(props);
    this.state = {
      collapsed: !props.shouldExpandAtFirstLoad,
    };
  }

  render() {
    const { collapsed } = this.state;
    const {
      className,
      isEditable,
      disallowDnD,
      disallowDelete,
      week,
      theme: { foreground, background },
      t,
    } = this.props;

    if (!week) {
      return null;
    }

    const _totalMessages = week.days.filter(day => !!day.message).length;

    return (
      <Flex className={`${className} ${collapsed ? 'collapsed' : ''}`}>
        <Flex className={'edit-group'}>
          {isEditable && !disallowDelete && (
            <Flex className={'btn-delete'} onClick={this.deleteWeek}>
              <img src={Remove} style={{ height: 16, width: 16 }} />
            </Flex>
          )}
          {isEditable && !disallowDnD && (
            <Flex className="week-drag-handle">
              <img src={DragAnchor} />
            </Flex>
          )}
        </Flex>
        <Flex className={'content-wrapper'}>
          <Flex
            className={'header'}
            onClick={() => {
              this.setState(prevState => ({
                collapsed: !prevState.collapsed,
              }));
            }}
          >
            <Flex
              className={`arrow-down-button ${collapsed ? 'collapsed' : ''}`}
            >
              <img src={ArrowDownButton} />
            </Flex>
            <H4 className={'ordinal'} fontWeight={'SemiBold'}>
              {t(i18nKeys.week, { number: week.ordinal })}
            </H4>
            <BodyTextS
              color={_totalMessages > 0 ? foreground['01'] : background['01']}
            >
              {t(i18nKeys.message, {
                number: _totalMessages,
              })}
            </BodyTextS>
          </Flex>
          {this._renderDays()}
        </Flex>
      </Flex>
    );
  }

  changedDayOrdinal = (event: {
    removedIndex: number;
    addedIndex: number;
  }): void => {
    const { removedIndex, addedIndex } = event;
    if (
      removedIndex === addedIndex ||
      (removedIndex === null && addedIndex === null)
    ) {
      return;
    }

    const { week, onChangedDayOrdinal } = this.props;
    if (onChangedDayOrdinal) {
      let _weekDays = [...week.days];
      let _changedDay: IPlanDay;

      if (removedIndex !== null) {
        _changedDay = _weekDays.splice(removedIndex, 1)[0];
      }
      if (addedIndex !== null) {
        _changedDay.ordinal = addedIndex + 1;
        _weekDays.splice(addedIndex, 0, _changedDay);
      }

      _weekDays = _weekDays.map((day, index) => ({
        ...day,
        ordinal: index + 1,
      }));

      onChangedDayOrdinal({
        ...week,
        days: _weekDays,
      });
    }
  };

  deleteWeek = (): void => {
    const { week, onDeleteWeek } = this.props;

    onDeleteWeek(week);
  };

  startEditDayMessage = (day: IPlanDay): void => {
    const { week, onStartEditDayMessage } = this.props;
    this.setState({ editingDayId: day.id }, () =>
      onStartEditDayMessage(week, day)
    );
  };

  endEditDayMessage = (day: IPlanDay): void => {
    const { week, onEndEditDayMessage } = this.props;
    this.setState({ editingDayId: undefined }, () =>
      onEndEditDayMessage(week, day)
    );
  };

  saveDayMessage = (day: IPlanDay): void => {
    this.setState({ editingDayId: undefined }, () => {
      this.props.onSaveDayMessage(this.props.week, day);
    });
  };

  deleteDayMessage = (day: IPlanDay): void => {
    this.setState({ editingDayId: undefined }, () => {
      this.props.onDeleteDayMessage(this.props.week, day);
    });
  };

  private _renderDays = () => {
    const {
      isEditable,
      disallowDnD,
      disallowEditDayMessage,
      week,
    } = this.props;
    const { editingDayId } = this.state;

    return (
      <Container
        lockAxis="y"
        dragHandleSelector=".day-drag-handle"
        onDrop={disallowDnD ? undefined : this.changedDayOrdinal}
      >
        {week.days.map(day => {
          return (
            <Draggable key={day.id}>
              <CommunicationPlanTimelineDay
                day={day}
                disallowDnD={disallowDnD || !isEditable}
                isEditing={!disallowEditDayMessage && editingDayId === day.id}
                isEditable={isEditable && !disallowEditDayMessage}
                onStartEditMessage={this.startEditDayMessage}
                onEndEditMessage={this.endEditDayMessage}
                onSaveMessage={this.saveDayMessage}
                onDeleteMessage={this.deleteDayMessage}
              />
            </Draggable>
          );
        })}
      </Container>
    );
  };
}

export const CommunicationPlanTimelineWeek = withTheme(
  withI18nContext(OriginCommunicationPlanTimelineWeek, {
    namespace: NAMESPACE_COMMUNICATION_PLAN_TIMELINE_WEEK,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
