import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './communication-plan-timeline-week.json';

export const NAMESPACE_COMMUNICATION_PLAN_TIMELINE_WEEK =
  'communication-plan-timeline-week';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
