import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { CommunicationPlanTimelineWeek as OriginCommunicationPlanTimelineWeek } from './CommunicationPlanTimelineWeek';
import { ICommunicationPlanTimelineWeekProps } from './models';
import { scaleSpacePx } from '@design-system/core/styles';

export const CommunicationPlanTimelineWeek = styled(
  OriginCommunicationPlanTimelineWeek
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanTimelineWeek', className),
}))<ICommunicationPlanTimelineWeekProps>`
  ${() => {
    return `
      & {
        align-items: flex-start;
        overflow: hidden;
        max-height: auto;

        &.collapsed {
          max-height: 44px;
        }

        > .edit-group {
          min-width: ${scaleSpacePx(16)};
          height: 32px;
          align-items: center;

          .btn-delete {
            display: none;
            padding-right: ${scaleSpacePx(4)};
            align-items: center;

            > img {
              pointer-events: none;
              width: ${scaleSpacePx(4)};
              width: ${scaleSpacePx(4)};
            }
          }

          .week-drag-handle {
            display: none;
            height: ${scaleSpacePx(8)}
            padding-right: ${scaleSpacePx(4)}

            &:hover {
              cursor: pointer;
            }

            > img {
              width: ${scaleSpacePx(3)};
              pointer-events: none;
            }
          }

          &:hover {
            > .btn-delete {
              display: flex;
            }

            > .week-drag-handle {
              display: flex;
              cursor: pointer;
            }
          }
        }

        > .content-wrapper {
          flex: 1;
          flex-direction: column;

          > .header {
            height: 32px;
            margin-bottom: ${scaleSpacePx(3)};
            align-items: center;

            > .arrow-down-button {
              height: 32px;
              padding-right: ${scaleSpacePx(2)};
              justify-content: center;

              &.collapsed {
                > img {
                  transform: rotate(180deg);
                }
              }
            }

            > .ordinal {
              margin-left: ${scaleSpacePx(2)};
              margin-right: ${scaleSpacePx(7)};
              min-width: ${scaleSpacePx(15)};
              white-space: nowrap;
            }
          }
        }
      }
    `;
  }}
`;
