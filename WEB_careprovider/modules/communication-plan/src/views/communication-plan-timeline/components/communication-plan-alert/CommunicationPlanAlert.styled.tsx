import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { CommunicationPlanAlert as OriginCommunicationPlanAlert } from './CommunicationPlanAlert';
import { ICommunicationPlanAlertProps } from './models';

export const CommunicationPlanAlert = styled(
  OriginCommunicationPlanAlert
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanAlert', className),
}))<ICommunicationPlanAlertProps>`
  /* stylelint-disable */
`;
