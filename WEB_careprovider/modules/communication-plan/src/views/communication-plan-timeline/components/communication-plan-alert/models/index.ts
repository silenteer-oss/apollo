import { ICareProviderTheme } from '@careprovider/theme';

export interface ICommunicationPlanAlertProps {
  className?: string;
  theme?: ICareProviderTheme;
  isOpen: boolean;
  intent: 'primary' | 'danger';
  headerText: string;
  content?: string;
  cancelButtonText: string;
  confirmButtonText: string;
  onConfirm: () => void;
  onCancel: () => void;
}
