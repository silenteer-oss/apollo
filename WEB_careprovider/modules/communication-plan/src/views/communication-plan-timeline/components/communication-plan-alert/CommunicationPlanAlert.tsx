import React from 'react';
import { Alert } from '@blueprintjs/core';
import { ICommunicationPlanAlertProps } from './models';

export class CommunicationPlanAlert extends React.PureComponent<
  ICommunicationPlanAlertProps
> {
  render() {
    const {
      intent,
      isOpen,
      headerText,
      content,
      cancelButtonText,
      confirmButtonText,
      onConfirm,
      onCancel,
    } = this.props;

    return (
      <Alert
        cancelButtonText={cancelButtonText}
        confirmButtonText={confirmButtonText}
        intent={intent}
        isOpen={isOpen}
        onConfirm={onConfirm}
        onCancel={onCancel}
      >
        <h2 dangerouslySetInnerHTML={{ __html: headerText }} />
        {content && <p>{content}</p>}
      </Alert>
    );
  }
}
