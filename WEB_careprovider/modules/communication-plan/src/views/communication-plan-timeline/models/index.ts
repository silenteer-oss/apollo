import { ICareProviderTheme } from '@careprovider/theme';
import { ICommunicationPlan, IPlanWeek, IPlanDay } from '../../../services';

export interface ICommunicationPlanTimelineViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  weeks?: IPlanWeek[];
}

export interface ICommunicationPlanTimelineViewState {
  isLoading: boolean;
  isShownChangeDurationDialog: boolean;
  isShownPublishDialog: boolean;
  isShownUnpublishDialog: boolean;
  isShownDeletePlanDialog: boolean;
  isShownDeletePlanWeekDialog: boolean;
  isShownDeletePlanWeekDayMessageDialog: boolean;
  isPublishedPlan: boolean;
  isEditingPublishedPlan: boolean;
  editingDayFromWeekId?: string;
  deletingWeek?: IPlanWeek;
  deletingWeekDay?: IPlanDay;
  communicationPlan: ICommunicationPlan;
  loadingSelectedPlanActivePatient: boolean;
  selectedPlanActivePatient?: number;
}
