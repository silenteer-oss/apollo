import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './communication-plan-timeline.json';

export const NAMESPACE_COMMUNICATION_PLAN_TIMELINE =
  'communication-plan-timeline';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
