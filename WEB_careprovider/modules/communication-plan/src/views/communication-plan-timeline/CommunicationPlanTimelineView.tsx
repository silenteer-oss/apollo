import React from 'react';
import {
  Flex,
  H1,
  BodyTextM,
  LoadingState,
} from '@design-system/core/components';
import { scaleSpacePx } from '@design-system/core/styles';
import { unique } from '@design-system/infrastructure/utils';
import { Container, Draggable } from 'react-smooth-dnd';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { Header } from '@careprovider/components';
import { withTheme } from '@careprovider/theme';
import {
  Button,
  Popover,
  Menu,
  MenuItem,
  Intent,
  Position,
  Alert,
} from '@blueprintjs/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  getPlanDetail,
  publish,
  unpublish,
  deletePlan,
  updatePlanTimeline,
  deleteWeek,
  createDay,
  editDay,
  deleteDay,
  getActivePatient,
} from '../../services';
import { getModuleConfig } from '../../../module-config';
import {
  IPlanWeek,
  IPlanDay,
  ICommunicationPlan,
  PlanStatus,
  getCommunicationPlanPath,
} from '../../services';
import {
  CommunicationPlanAlert,
  ChangeCommunicationPlanDurationDialog,
  CommunicationPlanTimelineWeek,
} from './components';
import {
  ICommunicationPlanTimelineViewProps,
  ICommunicationPlanTimelineViewState,
} from './models';
import { NAMESPACE_COMMUNICATION_PLAN_TIMELINE, i18nKeys } from './i18n';
import AlertIcon from '@careprovider/assets/images/red-alarm.png';

class OriginCommunicationPlanTimelineView extends React.PureComponent<
  ICommunicationPlanTimelineViewProps &
    RouteComponentProps<any> &
    II18nFixedNamespaceContext,
  ICommunicationPlanTimelineViewState
> {
  private _isFirstLoad = true;
  private _previousEditedWeekOrdinal: number;
  state: ICommunicationPlanTimelineViewState = {
    communicationPlan: undefined,
    isShownChangeDurationDialog: false,
    isShownPublishDialog: false,
    isShownUnpublishDialog: false,
    isShownDeletePlanDialog: false,
    isShownDeletePlanWeekDialog: false,
    isShownDeletePlanWeekDayMessageDialog: false,
    isPublishedPlan: false,
    isLoading: true,
    isEditingPublishedPlan: false,
    loadingSelectedPlanActivePatient: false,
    selectedPlanActivePatient: 0,
  };

  componentDidMount() {
    const {
      match: {
        params: { planId },
      },
    } = this.props;

    getPlanDetail(planId).then(data => {
      const communicationPlan = {
        ...data,
        weeks: this._generateWeek(data),
      };

      this.setState({
        isLoading: false,
        isPublishedPlan: data.status === PlanStatus.PUBLISHED,
        communicationPlan,
      });
    });
  }

  render() {
    const {
      className,
      theme: { foreground },
      t,
    } = this.props;
    const {
      communicationPlan,
      isShownChangeDurationDialog,
      isShownPublishDialog,
      isShownUnpublishDialog,
      isShownDeletePlanDialog,
      isEditingPublishedPlan,
      isLoading,
      loadingSelectedPlanActivePatient,
    } = this.state;

    if (!communicationPlan) {
      return null;
    }

    let _isFirstLoad = this._isFirstLoad;
    if (_isFirstLoad) {
      this._isFirstLoad = false;
    }

    const _hasMessage = communicationPlan.weeks.some(week =>
      week.days.some(day => !!day.message)
    );

    return (
      <Flex auto column className={className}>
        <Header>
          <Flex className="header-title" grow={1}>
            <Flex>
              <Button
                icon="chevron-left"
                intent="primary"
                minimal
                onClick={() => this.props.history.push('.')}
              >
                {t(i18nKeys.title)}
              </Button>
              <BodyTextM color={foreground['02']}>
                &nbsp;/&nbsp;{this._getCommunicationPlanStatus()}
              </BodyTextM>
            </Flex>
            <H1>{communicationPlan.name}</H1>
          </Flex>
          {isEditingPublishedPlan && (
            <Button
              className="plan-actions"
              text={t(i18nKeys.doneEdit)}
              intent={Intent.PRIMARY}
              onClick={this.endEditPublishedPlan}
            />
          )}
          {communicationPlan.status === PlanStatus.DRAFT && (
            <>
              <Button
                className="plan-actions"
                style={{ marginRight: scaleSpacePx(6) }}
                text={t(i18nKeys.publish)}
                intent={Intent.PRIMARY}
                onClick={this.showPublishDialog}
                disabled={!_hasMessage}
              />
              <Popover
                className="plan-actions"
                content={
                  <Menu>
                    <MenuItem
                      text={
                        <BodyTextM color={foreground.error.base}>
                          {t(i18nKeys.delete)}
                        </BodyTextM>
                      }
                      onClick={this.showDeletePlanDialog}
                    />
                  </Menu>
                }
                position={Position.BOTTOM_RIGHT}
              >
                <Button icon="more" intent={Intent.PRIMARY} minimal small />
              </Popover>
            </>
          )}
          {communicationPlan.status === PlanStatus.PUBLISHED &&
            !isEditingPublishedPlan && (
              <Popover
                className="plan-actions"
                content={
                  <Menu>
                    <MenuItem
                      text={t(i18nKeys.edit)}
                      onClick={this.startEditPublishedPlan}
                    />
                    <MenuItem
                      text={t(i18nKeys.unpublish)}
                      onClick={this.showUnpublishDialog}
                    />
                    <MenuItem
                      text={
                        <BodyTextM color={foreground.error.base}>
                          {t(i18nKeys.delete)}
                        </BodyTextM>
                      }
                      onClick={this.showDeletePlanDialog}
                    />
                  </Menu>
                }
                position={Position.BOTTOM_RIGHT}
              >
                <Button icon="more" intent={Intent.PRIMARY} minimal small />
              </Popover>
            )}
          <ChangeCommunicationPlanDurationDialog
            isOpen={isShownChangeDurationDialog}
            currentDuration={communicationPlan.duration}
            onCancel={this.hideChangeDurationDialog}
            onSave={this.saveChangedDuration}
          />
        </Header>
        {isLoading && <LoadingState />}
        {this._renderTimeline(_isFirstLoad)}
        <CommunicationPlanAlert
          intent={Intent.PRIMARY}
          isOpen={isShownPublishDialog}
          headerText={t(i18nKeys.publishHeader, {
            planName: communicationPlan.name,
          })}
          content={t(i18nKeys.publishContent)}
          cancelButtonText={t(i18nKeys.cancel)}
          confirmButtonText={t(i18nKeys.publishPlan)}
          onConfirm={this.publishPlan}
          onCancel={this.hidePublishDialog}
        />
        <CommunicationPlanAlert
          intent={Intent.PRIMARY}
          isOpen={isShownUnpublishDialog}
          headerText={t(i18nKeys.unpublishHeader, {
            planName: communicationPlan.name,
          })}
          content={t(i18nKeys.unpublishContent)}
          cancelButtonText={t(i18nKeys.cancel)}
          confirmButtonText={t(i18nKeys.unpublishPlan)}
          onConfirm={this.unpublishPlan}
          onCancel={this.hideUnpublishDialog}
        />
        <CommunicationPlanAlert
          intent={Intent.DANGER}
          isOpen={isShownDeletePlanDialog}
          headerText={t(i18nKeys.deletePlanHeader, {
            planName: communicationPlan.name,
          })}
          content={t(i18nKeys.deletePlanContent)}
          cancelButtonText={t(i18nKeys.cancel)}
          confirmButtonText={t(i18nKeys.deletePlan)}
          onConfirm={this.deletePlan}
          onCancel={this.hideDeletePlanDialog}
        />
        <Alert
          className="delete-alert"
          cancelButtonText={
            loadingSelectedPlanActivePatient ? null : t(i18nKeys.cancel)
          }
          confirmButtonText={
            loadingSelectedPlanActivePatient ? null : t(i18nKeys.deletePlan)
          }
          intent={Intent.DANGER}
          isOpen={isShownDeletePlanDialog}
          onConfirm={this.deletePlan}
          onCancel={this.hideDeletePlanDialog}
          canEscapeKeyCancel
        >
          {loadingSelectedPlanActivePatient ? (
            <Flex className="loading">
              <LoadingState size={24} />
            </Flex>
          ) : (
            this.renderDeletePlanView()
          )}
        </Alert>
      </Flex>
    );
  }

  renderDeletePlanView = () => {
    const { t } = this.props;
    const { communicationPlan, selectedPlanActivePatient } = this.state;

    if (communicationPlan.status === PlanStatus.PUBLISHED) {
      return (
        <>
          <img src={AlertIcon} />
          <h2
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.deletePlanHeader, {
                planName: communicationPlan && communicationPlan.name,
              }),
            }}
          />
          <p
            className="delete-message"
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.deletePublishedPlanContent, {
                patientCount: selectedPlanActivePatient,
              }),
            }}
          />
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription1)}
          </p>
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription2)}
          </p>
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription3)}
          </p>
        </>
      );
    }

    return (
      <>
        <img src={AlertIcon} />
        <h2
          dangerouslySetInnerHTML={{
            __html: t(i18nKeys.deletePlanHeader, {
              planName: communicationPlan && communicationPlan.name,
            }),
          }}
        />
        <p>
          {t(i18nKeys.deletePlanContent, {
            patientCount: selectedPlanActivePatient,
          })}
        </p>
      </>
    );
  };

  publishPlan = (): void => {
    const { communicationPlan } = this.state;

    publish(communicationPlan.id).then(() => {
      this.props.history.push('.');
    });
  };

  unpublishPlan = (): void => {
    const { communicationPlan } = this.state;
    unpublish(communicationPlan.id).then(() => {
      this.props.history.push('.');
    });
  };

  deletePlan = (): void => {
    const { communicationPlan } = this.state;
    deletePlan(communicationPlan.id).then(() => {
      this.props.history.push(getCommunicationPlanPath());
    });
  };

  changedWeekOrdinal = (event: {
    removedIndex: number;
    addedIndex: number;
  }): void => {
    const { removedIndex, addedIndex } = event;
    if (
      removedIndex === addedIndex ||
      (removedIndex === null && addedIndex === null)
    ) {
      return;
    }

    let _weeks = [...this.state.communicationPlan.weeks];
    let _changedWeek: IPlanWeek;

    if (removedIndex !== null) {
      _changedWeek = _weeks.splice(removedIndex, 1)[0];
    }
    if (addedIndex !== null) {
      _changedWeek.ordinal = addedIndex + 1;
      _weeks.splice(addedIndex, 0, _changedWeek);
    }

    _weeks = _weeks.map((week, index) => ({
      ...week,
      ordinal: index + 1,
    }));

    this._previousEditedWeekOrdinal = _weeks.find(
      week => week.id === _changedWeek.id
    ).ordinal;

    this.setState(
      prevState => ({
        communicationPlan: {
          ...prevState.communicationPlan,
          weeks: _weeks,
        },
      }),
      () => {
        this._updatePlanTimeline();
      }
    );
  };

  changedWeekDayOrdinal = (week: IPlanWeek): void => {
    this.setState(
      prevState => ({
        communicationPlan: {
          ...prevState.communicationPlan,
          weeks: prevState.communicationPlan.weeks.map(item =>
            item.id === week.id ? { ...week } : item
          ),
        },
      }),
      () => {
        this._updatePlanTimeline();
      }
    );
  };

  deletePlanWeek = (): void => {
    const { communicationPlan, deletingWeek } = this.state;

    let _deleteWeekPromise: Promise<void>;

    if (deletingWeek.id.indexOf('id-') === 0) {
      _deleteWeekPromise = Promise.resolve();
    } else {
      _deleteWeekPromise = deleteWeek(communicationPlan.id, deletingWeek.id);
    }

    _deleteWeekPromise.then(() => {
      this.setState({
        isShownDeletePlanWeekDialog: false,
        deletingWeek: undefined,
        communicationPlan: {
          ...communicationPlan,
          weeks: communicationPlan.weeks.filter(
            week => week.id !== deletingWeek.id
          ),
        },
      });
    });
  };

  startEditWeekDayMessage = (week: IPlanWeek, day: IPlanDay): void => {
    this.setState({ editingDayFromWeekId: week.id });
  };

  endEditWeekDayMessage = (week: IPlanWeek, day: IPlanDay): void => {
    this.setState({ editingDayFromWeekId: undefined });
  };

  deleteWeekDayMessage = (): void => {
    const { communicationPlan, deletingWeek, deletingWeekDay } = this.state;

    deleteDay(communicationPlan.id, deletingWeekDay.id).then(() => {
      this.setState({
        isShownDeletePlanWeekDayMessageDialog: false,
        deletingWeek: undefined,
        deletingWeekDay: undefined,
        communicationPlan: {
          ...communicationPlan,
          weeks: communicationPlan.weeks.map(week => {
            if (week.id === deletingWeek.id) {
              return {
                ...week,
                days: this._generateDay(
                  week.id,
                  communicationPlan.id,
                  week.days.filter(day => day.id !== deletingWeekDay.id)
                ),
              };
            }
            return week;
          }),
        },
      });
    });
  };

  saveWeekDayMessage = (week: IPlanWeek, day: IPlanDay): void => {
    const { communicationPlan } = this.state;

    if (day.id.indexOf('id-') === 0) {
      createDay(
        communicationPlan.id,
        week.ordinal,
        day.ordinal,
        day.message
      ).then(data => {
        this._previousEditedWeekOrdinal = week.ordinal;

        this.setState(prevState => ({
          editingDayFromWeekId: undefined,
          communicationPlan: {
            ...prevState.communicationPlan,
            weeks: prevState.communicationPlan.weeks
              .filter(item => item.id !== week.id)
              .concat({
                ...data,
                days: unique([...week.days, ...data.days], {
                  doOverride: true,
                  getKey: item => item.ordinal.toString(),
                }),
              })
              .sort((a, b) => {
                return a.ordinal - b.ordinal;
              }),
          },
        }));
      });
    } else {
      editDay(communicationPlan.id, day.id, day.message).then(() => {
        this._previousEditedWeekOrdinal = week.ordinal;

        this.setState(prevState => ({
          editingDayFromWeekId: undefined,
          communicationPlan: {
            ...prevState.communicationPlan,
            weeks: prevState.communicationPlan.weeks.map(weekItem => {
              if (weekItem.id === week.id) {
                return {
                  ...weekItem,
                  days: weekItem.days.map(dayItem =>
                    dayItem.id === day.id ? day : dayItem
                  ),
                };
              }
              return weekItem;
            }),
          },
        }));
      });
    }
  };

  saveChangedDuration = (duration: number): void => {
    const {
      communicationPlan: { duration: currentDuration, weeks },
    } = this.state;
    let _weeks = weeks;

    if (duration < currentDuration) {
      _weeks = _weeks.slice(0, duration);
    } else if (duration > currentDuration) {
      _weeks = this._generateWeek({
        ...this.state.communicationPlan,
        duration,
      });
    }

    this._updatePlanTimeline({ newDuration: duration, newWeeks: _weeks });
  };

  showChangeDurationDialog = () => {
    this.setState({ isShownChangeDurationDialog: true });
  };

  hideChangeDurationDialog = () => {
    this.setState({ isShownChangeDurationDialog: false });
  };

  showPublishDialog = () => {
    const { communicationPlan } = this.state;
    if (
      communicationPlan.weeks.some(week => week.days.some(day => !!day.message))
    ) {
      this.setState({ isShownPublishDialog: true });
    }
  };

  hidePublishDialog = () => {
    this.setState({ isShownPublishDialog: false });
  };

  showUnpublishDialog = () => {
    this.setState({ isShownUnpublishDialog: true });
  };

  hideUnpublishDialog = () => {
    this.setState({ isShownUnpublishDialog: false });
  };

  showDeletePlanDialog = () => {
    const { communicationPlan } = this.state;
    const published = communicationPlan.status === PlanStatus.PUBLISHED;

    this.setState({
      isShownDeletePlanDialog: true,
      loadingSelectedPlanActivePatient: published,
    });

    if (published) {
      getActivePatient(communicationPlan.id)
        .then(response =>
          this.setState({
            selectedPlanActivePatient: response.activePatientCount,
            loadingSelectedPlanActivePatient: false,
          })
        )
        .catch(() => {
          this.setState({
            selectedPlanActivePatient: 0,
            loadingSelectedPlanActivePatient: false,
          });
        });
    }
  };

  hideDeletePlanDialog = () => {
    this.setState({ isShownDeletePlanDialog: false });
  };

  showDeletePlanWeekDialog = (week: IPlanWeek) => {
    this.setState({ isShownDeletePlanWeekDialog: true, deletingWeek: week });
  };

  hideDeletePlanWeekDialog = () => {
    this.setState({
      isShownDeletePlanWeekDialog: false,
      deletingWeek: undefined,
    });
  };

  showDeletePlanWeekDayMessageDialog = (week: IPlanWeek, day: IPlanDay) => {
    this.setState({
      isShownDeletePlanWeekDayMessageDialog: true,
      deletingWeek: week,
      deletingWeekDay: day,
    });
  };

  hideDeletePlanWeekDayMessageDialog = () => {
    this.setState({
      isShownDeletePlanWeekDayMessageDialog: false,
      deletingWeek: undefined,
      deletingWeekDay: undefined,
    });
  };

  startEditPublishedPlan = () => {
    const { communicationPlan } = this.state;
    if (
      communicationPlan &&
      communicationPlan.status === PlanStatus.PUBLISHED
    ) {
      this.setState({ isEditingPublishedPlan: true });
    }
  };

  endEditPublishedPlan = () => {
    this.setState({
      isEditingPublishedPlan: false,
      editingDayFromWeekId: undefined,
    });
  };

  private _renderTimeline = (isFirstLoad: boolean) => {
    const { t } = this.props;
    const {
      isShownDeletePlanWeekDialog,
      isShownDeletePlanWeekDayMessageDialog,
      isPublishedPlan,
      isEditingPublishedPlan,
      editingDayFromWeekId,
      deletingWeek,
      communicationPlan,
    } = this.state;

    const _isReadOnly = isPublishedPlan && !isEditingPublishedPlan;

    return (
      <Flex className="plan-timeline" column>
        <Flex className="plan-timeline-container" column>
          <Flex className="plan-timeline-header" align="center">
            <H1>{t(i18nKeys.timeline)}</H1>
            <Flex className="detail" align="center">
              {t(i18nKeys.duration, { number: communicationPlan.duration })}
              {!isPublishedPlan && (
                <Button
                  intent="primary"
                  minimal
                  onClick={this.showChangeDurationDialog}
                >
                  {t(i18nKeys.changeDuration)}
                </Button>
              )}
            </Flex>
          </Flex>
          <Container
            lockAxis="y"
            dragHandleSelector=".week-drag-handle"
            onDrop={_isReadOnly ? undefined : this.changedWeekOrdinal}
          >
            {communicationPlan.weeks.map((week, index) => {
              return (
                <Draggable key={week.id}>
                  <CommunicationPlanTimelineWeek
                    week={week}
                    isEditable={
                      _isReadOnly ? false : editingDayFromWeekId === undefined
                    }
                    disallowDnD={isEditingPublishedPlan}
                    disallowDelete={isEditingPublishedPlan}
                    disallowEditDayMessage={
                      isPublishedPlan && !isEditingPublishedPlan
                    }
                    shouldExpandAtFirstLoad={
                      (!_isReadOnly && index === 0) ||
                      (isFirstLoad && index === 0) ||
                      this._previousEditedWeekOrdinal === week.ordinal
                    }
                    onDeleteWeek={this.showDeletePlanWeekDialog}
                    onChangedDayOrdinal={this.changedWeekDayOrdinal}
                    onStartEditDayMessage={this.startEditWeekDayMessage}
                    onEndEditDayMessage={this.endEditWeekDayMessage}
                    onSaveDayMessage={this.saveWeekDayMessage}
                    onDeleteDayMessage={this.showDeletePlanWeekDayMessageDialog}
                  />
                </Draggable>
              );
            })}
          </Container>
          <CommunicationPlanAlert
            intent={Intent.DANGER}
            isOpen={isShownDeletePlanWeekDialog}
            headerText={t(i18nKeys.deletePlanWeekHeader, {
              week: deletingWeek ? deletingWeek.ordinal : '',
            })}
            content={t(i18nKeys.deletePlanWeekContent)}
            cancelButtonText={t(i18nKeys.cancel)}
            confirmButtonText={t(i18nKeys.deletePlanWeek)}
            onConfirm={this.deletePlanWeek}
            onCancel={this.hideDeletePlanWeekDialog}
          />
          <CommunicationPlanAlert
            intent={Intent.DANGER}
            isOpen={isShownDeletePlanWeekDayMessageDialog}
            headerText={t(i18nKeys.deletePlanWeekDayMessageHeader)}
            cancelButtonText={t(i18nKeys.cancel)}
            confirmButtonText={t(i18nKeys.deletePlanWeekDayMessage)}
            onConfirm={this.deleteWeekDayMessage}
            onCancel={this.hideDeletePlanWeekDayMessageDialog}
          />
        </Flex>
      </Flex>
    );
  };

  private _updatePlanTimeline = (params?: {
    newDuration: number;
    newWeeks: IPlanWeek[];
  }): void => {
    const { newDuration, newWeeks } =
      params ||
      ({} as {
        newDuration: number;
        newWeeks: IPlanWeek[];
      });
    const { communicationPlan } = this.state;

    updatePlanTimeline(communicationPlan.id, {
      duration: newDuration || communicationPlan.duration,
      retainedOrNewWeeks: (newWeeks || communicationPlan.weeks)
        .filter(week => week.days.some(day => !!day.message))
        .reduce((result, week) => {
          result.push({
            id: week.id,
            ordinal: week.ordinal,
            days: week.days
              .filter(day => !!day.message)
              .map(day => ({
                id: day.id,
                ordinal: day.ordinal,
                message: day.message,
              })),
          });
          return result;
        }, []),
    }).then(data => {
      this.setState(prevState => ({
        isShownChangeDurationDialog: false,
        communicationPlan: {
          ...prevState.communicationPlan,
          ...data,
          weeks: this._generateWeek(data),
        },
      }));
    });
  };

  private _getCommunicationPlanStatus = (): string => {
    const { communicationPlan } = this.state;
    const { t } = this.props;
    switch (communicationPlan.status) {
      case PlanStatus.DRAFT:
        return t(i18nKeys.draft);
      case PlanStatus.PUBLISHED:
        return t(i18nKeys.published);
      default:
        return '';
    }
  };

  private _generateWeek = (
    communicationPlan: ICommunicationPlan
  ): IPlanWeek[] => {
    const { duration, weeks = [] } = communicationPlan;
    const result: IPlanWeek[] = [];

    for (let index = 0; index < duration; index++) {
      const currentWeek = weeks.find(week => week.ordinal === index + 1);

      if (currentWeek) {
        result.push({
          id: currentWeek.id,
          communicationPlanId: communicationPlan.id,
          ordinal: index + 1,
          days: this._generateDay(
            currentWeek.id,
            communicationPlan.id,
            currentWeek.days
          ),
        });
      } else {
        const weekId = `id-${index}`;
        result.push({
          id: weekId,
          communicationPlanId: communicationPlan.id,
          ordinal: index + 1,
          days: this._generateDay(weekId, communicationPlan.id),
        });
      }
    }
    return result;
  };

  private _generateDay = (
    weekId: string,
    communicationPlanId: string,
    planDays: IPlanDay[] = []
  ): IPlanDay[] => {
    const result: IPlanDay[] = [];

    for (let index = 0; index < 7; index++) {
      const currentDay = planDays.find(week => week.ordinal === index + 1);

      if (currentDay) {
        result.push({
          id: currentDay.id,
          communicationPlanId,
          weekId: weekId,
          ordinal: index + 1,
          message: currentDay.message,
        });
      } else {
        result.push({
          id: `id-${index}`,
          communicationPlanId,
          weekId: weekId,
          ordinal: index + 1,
          message: undefined,
        });
      }
    }
    return result;
  };
}

export const CommunicationPlanTimelineView = withRouter(
  withTheme(
    withI18nContext(OriginCommunicationPlanTimelineView, {
      namespace: NAMESPACE_COMMUNICATION_PLAN_TIMELINE,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
