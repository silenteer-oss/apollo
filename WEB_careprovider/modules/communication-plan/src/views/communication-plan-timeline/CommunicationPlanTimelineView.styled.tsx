import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { CommunicationPlanTimelineView as OriginCommunicationPlanTimelineView } from './CommunicationPlanTimelineView';
import { ICommunicationPlanTimelineViewProps } from './models';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';

export const CommunicationPlanTimelineView = styled(
  OriginCommunicationPlanTimelineView
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanTimelineView', className),
}))<ICommunicationPlanTimelineViewProps>`
  ${props => {
    const {
      theme: { background },
    } = props;
    const headerHeight = scaleSpace(20) + 1; // 1 is bottom border width;
    const timelineListPaddingX = scaleSpace(6) * 2;

    const _width = window.innerWidth;

    return `
      & {
        height: 100%;
        width: 100%;

        > .sl-Header {
          border-bottom: 1px solid ${background['01']};
          justify-content: space-between;

          .header-title {
            flex-direction: column;

            .bp3-button {
              min-height: 0px !important;
              min-width: 0px !important;
              padding: ${scaleSpacePx(0)} !important;
            }
          }

          .plan-actions {
            width: auto;
          }
        }

        .plan-timeline {
          padding: ${scaleSpacePx(6)};
          width: calc(100% - ${scaleSpacePx(6)} * 2);
          height: calc(100% - ${headerHeight}px - ${timelineListPaddingX}px);
          overflow-x: auto;

          .plan-timeline-container {
            width: 66%;
            margin-left: calc(${_width}px / 5);

            > .plan-timeline-header {
              flex: 1;
              justify-content: space-between;
              align-content: flex-end;
              padding-left: ${scaleSpacePx(16)};
              padding-right: ${scaleSpacePx(33)};
              padding-bottom: ${scaleSpacePx(6)};

              > h1 {
                font-size: 18px;
              }


              > .detail {
                font-size: 14px;
              }
            }
          }
        }

        .smooth-dnd-draggable-wrapper {
          overflow: visible !important;
        }
      }
    `;
  }}
`;
