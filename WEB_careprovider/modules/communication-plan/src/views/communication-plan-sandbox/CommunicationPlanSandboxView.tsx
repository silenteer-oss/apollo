import React from 'react';
import { CommunicationPlanListView } from '../communication-plan-list';
import {
  ICommunicationPlanSandboxViewProps,
  ICommunicationPlanSandboxViewState,
} from './models';
import { Route, BrowserRouter, Redirect } from 'react-router-dom';
import {
  getCommunicationPlanPath,
  getCommunicationPlanDetailPath,
  getPatientCommunicationPlanPath,
} from '../../services';
import { CommunicationPlanTimelineView } from '../communication-plan-timeline';
import { CommunicationPlanPatienView } from '../communication-plan-patient';

const COMMUNICATION_PLAN_PAGE_PATH = getCommunicationPlanPath();
const COMMUNICATION_PLAN_DETAIL_PAGE_PATH = getCommunicationPlanDetailPath();
const COMMUNICATION_PLAN_PATIENT_PAGE_PATH = getPatientCommunicationPlanPath();

export class CommunicationPlanSandboxView extends React.PureComponent<
  ICommunicationPlanSandboxViewProps,
  ICommunicationPlanSandboxViewState
> {
  state: ICommunicationPlanSandboxViewState = {
    isReady: false,
  };

  componentWillMount() {
    this.setState({ isReady: true });
  }

  render() {
    const { className } = this.props;
    const { isReady } = this.state;

    if (!isReady) {
      return null;
    }

    return (
      <div className={className}>
        <BrowserRouter>
          <Route
            path={COMMUNICATION_PLAN_PAGE_PATH}
            exact
            component={() => <CommunicationPlanListView />}
          />
          <Route
            path={COMMUNICATION_PLAN_DETAIL_PAGE_PATH}
            exact
            component={() => <CommunicationPlanTimelineView />}
          />
          <Route
            path={COMMUNICATION_PLAN_PATIENT_PAGE_PATH}
            exact
            component={() => (
              <CommunicationPlanPatienView patientId="474e4c17-9364-4f8c-949a-f96442c176f7" />
            )}
          />
          {/* Removes trailing slashes */}
          <Route
            path="/:url*(/+)"
            exact
            strict
            render={({ location }) => (
              <Redirect to={location.pathname.replace(/\/+$/, '')} />
            )}
          />
          {/* Removes duplicate slashes in the middle of the URL */}
          <Route
            path="/:url(.*//+.*)"
            exact
            strict
            render={({ match }) => (
              <Redirect to={`/${match.params.url.replace(/\/\/+/, '/')}`} />
            )}
          />
        </BrowserRouter>
      </div>
    );
  }
}
