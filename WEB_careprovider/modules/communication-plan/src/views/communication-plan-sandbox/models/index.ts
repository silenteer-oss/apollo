import { ICareProviderTheme } from '@careprovider/theme';

export interface ICommunicationPlanSandboxViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ICommunicationPlanSandboxViewState {
  isReady: boolean;
}
