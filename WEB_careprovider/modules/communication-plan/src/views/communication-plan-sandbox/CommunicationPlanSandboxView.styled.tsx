import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICommunicationPlanSandboxViewProps } from './models';
import { CommunicationPlanSandboxView as OriginCommunicationPlanSandboxView } from './CommunicationPlanSandboxView';

export const CommunicationPlanSandboxView = styled(
  OriginCommunicationPlanSandboxView
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanSandboxView', className),
}))<ICommunicationPlanSandboxViewProps>`
  ${props => {
    const { background } = props.theme;

    return `
    & {
      display: flex;
      height: 100%;
      width: 100%;
      background-color: ${background.white};
    }
  `;
  }}
`;
