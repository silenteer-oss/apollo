import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './create-communication-plan.json';

export const NAMESPACE_CREATE_COMMUNICATION_PLAN = 'create-communication-plan';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
