import { ICareProviderTheme } from '@careprovider/theme';

export function mixGlobalStyle(theme: ICareProviderTheme): string {
  return `
      .bp3-dialog-header {
        .bp3-heading {
          text-align: center;
        }
      }
  `;
}
