import React from 'react';
import {
  ICreateCommunicationPlanViewProps,
  ICommunicationPlanInformation,
} from './models';
import { Flex, Box } from '@design-system/core/components';
import {
  Button,
  Intent,
  FormGroup,
  Classes,
  InputGroup,
} from '@blueprintjs/core';
import {
  Formik,
  Form,
  Field,
  FormikErrors,
  FormikTouched,
  FormikActions,
} from 'formik';
import { withTheme } from 'theme';
import {
  withGlobalStyleContext,
  IGlobalStyleContext,
} from '@design-system/themes/context';
import { mixGlobalStyle } from './styles/mix-global-style';
import { createPlan } from '../../services';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_CREATE_COMMUNICATION_PLAN, i18nKeys } from './i18n';

class OriginalCreateCommunicationPlanView extends React.PureComponent<
  ICreateCommunicationPlanViewProps &
    IGlobalStyleContext &
    RouteComponentProps &
    II18nFixedNamespaceContext
> {
  componentDidMount() {
    this.props.addGlobalStyle(
      'createCommunicationPlanDialog',
      mixGlobalStyle(this.props.theme)
    );
  }

  render() {
    const {
      className,
      theme: { space },
      t,
    } = this.props;

    return (
      <Flex className={className}>
        <Formik
          initialValues={{} as ICommunicationPlanInformation}
          onSubmit={this.onCreatePlan}
          render={({ errors, touched, isSubmitting }) => {
            const {
              planName: planNameError,
              planDuration: planDurationError,
            } = errors as FormikErrors<ICommunicationPlanInformation>;

            const {
              planName: planNameTouched,
              planDuration: planDurationTouched,
            } = touched as FormikTouched<ICommunicationPlanInformation>;

            const showPlanNameError = planNameTouched && planNameError;
            const showPlanDurationError =
              planDurationTouched && planDurationError;

            return (
              <Form className="create-form">
                <Box mb={space.s}>
                  <Field name="planName" validate={this.validatePlanName}>
                    {({ field }) => (
                      <FormGroup
                        label={t(i18nKeys.planName)}
                        helperText={showPlanNameError ? planNameError : ''}
                        className={
                          showPlanNameError ? Classes.INTENT_DANGER : ''
                        }
                      >
                        <InputGroup
                          {...field}
                          placeholder={t(i18nKeys.planNamePlaceholder)}
                          maxLength={100}
                          intent={
                            showPlanNameError ? Intent.DANGER : Intent.NONE
                          }
                        />
                      </FormGroup>
                    )}
                  </Field>
                </Box>
                <Box mb={space.s}>
                  <FormGroup
                    helperText={showPlanDurationError ? planDurationError : ''}
                    className={
                      showPlanDurationError ? Classes.INTENT_DANGER : ''
                    }
                    label={t(i18nKeys.planDuration)}
                  >
                    <Field
                      name="planDuration"
                      validate={this.validatePlanDuration}
                    >
                      {({ field }) => (
                        <InputGroup
                          {...field}
                          fill
                          placeholder={t(i18nKeys.planDurationPlaceholder)}
                          intent={
                            showPlanDurationError ? Intent.DANGER : Intent.NONE
                          }
                          maxLength={2}
                        />
                      )}
                    </Field>
                  </FormGroup>
                </Box>
                <Button
                  className="submit-button"
                  type="submit"
                  intent={Intent.PRIMARY}
                  loading={isSubmitting}
                  fill
                  disabled={
                    (!planNameTouched && !planDurationTouched) ||
                    !!planNameError ||
                    !!planDurationError
                  }
                >
                  {t(i18nKeys.next)}
                </Button>
              </Form>
            );
          }}
        />
      </Flex>
    );
  }

  onCreatePlan = (
    values: ICommunicationPlanInformation,
    formikBag: FormikActions<ICommunicationPlanInformation>
  ) => {
    const { planName, planDuration } = values;

    formikBag.setSubmitting(true);

    createPlan(planName, planDuration, 'WEEK')
      .then(({ id }) => {
        formikBag.setSubmitting(false);
        const {
          history,
          location: { pathname },
        } = this.props;
        history.push(`${pathname}/${id}`);
      })
      .catch(error => {
        formikBag.setSubmitting(false);
        throw error;
      });
  };

  validatePlanName = planName => {
    if (!planName) return this.props.t(i18nKeys.emptyPlanNameWarningMessage);
  };

  validatePlanDuration = planDuration => {
    if (!planDuration)
      return this.props.t(i18nKeys.emptyPlanDurationWarningMessage);
    if (isNaN(planDuration)) {
      return this.props.t(i18nKeys.invalidPlanDurationWarningMessage);
    }
    if (planDuration > 52 || planDuration < 1) {
      return this.props.t(i18nKeys.invalidRangePlanDurationWarningMessage);
    }
  };
}

export const CreateCommunicationPlanView = withTheme(
  withRouter(
    withGlobalStyleContext(
      withI18nContext(OriginalCreateCommunicationPlanView, {
        namespace: NAMESPACE_CREATE_COMMUNICATION_PLAN,
        publicAssetPath: getModuleConfig().publicAssetPath,
      })
    )
  )
);
