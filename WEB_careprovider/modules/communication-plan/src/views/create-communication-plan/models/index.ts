import { ICareProviderTheme } from '@careprovider/theme';

export interface ICreateCommunicationPlanViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ICommunicationPlanInformation {
  planName: string;
  planDuration: number;
  planUnit: PLAN_UNIT;
}

export type PLAN_UNIT = 'WEEK';
