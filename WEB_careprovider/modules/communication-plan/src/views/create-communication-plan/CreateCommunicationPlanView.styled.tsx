import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICreateCommunicationPlanViewProps } from './models';
import { CreateCommunicationPlanView as OriginalCreateCommunicationPlanView } from './CreateCommunicationPlanView';
import { scaleSpacePx } from '@design-system/core/styles';

export const CreateCommunicationPlanView = styled(
  OriginalCreateCommunicationPlanView
).attrs(({ className }) => ({
  className: getCssClass('sl-CreateCommunicationPlanView', className),
}))<ICreateCommunicationPlanViewProps>`
  ${props => {
    const { background } = props.theme;

    return `
    & {
      justify-content: center;

      > .sl-Header {
        border-bottom: 1px solid ${background['01']};
      }

      > .create-form {
        width: 100%;
        height: 100%;
        max-width: 720px;

        .submit-button {
          margin-top: ${scaleSpacePx(8)};
        }
      }
    }`;
  }}
`;
