import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IRootViewProps } from './models';
import { OriginRootView } from './RootView';

export const RootView = styled(OriginRootView).attrs(({ className }) => ({
  className: getCssClass('sl-RootView', className),
}))<IRootViewProps>`
  ${() => {
    return `
      display: flex;
      height: 100%;
      width: 100%;
    `;
  }}
`;
