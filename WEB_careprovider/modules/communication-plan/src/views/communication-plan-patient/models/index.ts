import { ICareProviderTheme } from '@careprovider/theme';
import { IPatientCommunicationPlan } from '../../../services';

export interface ICommunicationPlanPatientViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId?: string;
}

export interface ICommunicationPlanPatientViewState {
  patientCommunicationPlan?: IPatientCommunicationPlan;
}
