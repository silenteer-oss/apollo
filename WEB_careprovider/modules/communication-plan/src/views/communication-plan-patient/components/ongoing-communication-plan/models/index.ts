import { ICareProviderTheme } from '@careprovider/theme';
import { IPatientCommunicationPlan } from '../../../../../services';
import { PatientPlanMessageResponse } from 'resource/communication-plan-app-resource/model';

export interface IOngoingCommunicationPlanProps {
  className?: string;
  theme?: ICareProviderTheme;
  communicationPlan?: IPatientCommunicationPlan;
  patientId?: string;
  onRemovePlan?: () => void;
}

export interface IOngoingCommunicationPlanState {
  upcomingMessageOffset: number;
  upcomingMessages: PatientPlanMessageResponse[];
  failedMessages?: PatientPlanMessageResponse[];
  nextAvailable: boolean;
  removeDialog: boolean;
}
