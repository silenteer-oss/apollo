import React from 'react';
import { Flex, BodyTextM, BodyTextS, H2 } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { Button } from '@blueprintjs/core/src';
import { dateTimeFormat, now, diff } from '@careprovider/services/util';
import { getModuleConfig } from '../../../../../module-config';
import {
  IOngoingCommunicationPlanProps,
  IOngoingCommunicationPlanState,
} from './models';
import { NAMESPACE_ONGOING_COMMUNICATION_PLAN, i18nKeys } from './i18n';

import AlertCircle from '@careprovider/assets/images/alert-circle-solid.svg';
import ArrowRight from '@careprovider/assets/images/arrow-right.svg';
import InactiveArrowRight from '@careprovider/assets/images/arrow-right-inactive.svg';
import { PatientPlanMessageResponse } from 'resource/communication-plan-app-resource/model';
import { Alert, Intent, Tooltip, ProgressBar } from '@blueprintjs/core';
import {
  getUpcomingMessages,
  disassociateFromPatient,
  getFailedMessage,
  resendFailedMessage,
  skipFailedMessage,
} from 'modules/communication-plan/src/services';

const MESSAGE_PER_VIEW = 3;

class OngoingCommunicationPlan extends React.PureComponent<
  IOngoingCommunicationPlanProps & II18nFixedNamespaceContext,
  IOngoingCommunicationPlanState
> {
  state = {
    upcomingMessageOffset: 0,
    upcomingMessages: [],
    failedMessages: undefined,
    nextAvailable: false,
    removeDialog: false,
  };

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      (this.props.patientId && this.props.patientId !== prevProps.patientId) ||
      this.state.upcomingMessageOffset !== prevState.upcomingMessageOffset
    ) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { patientId } = this.props;

    getUpcomingMessages(
      patientId,
      MESSAGE_PER_VIEW + 1,
      this.state.upcomingMessageOffset
    ).then(data => {
      const nextAvailable = data.length > MESSAGE_PER_VIEW;

      if (nextAvailable) {
        data.splice(-1, 1);
      }

      this.setState({
        upcomingMessages: data,
        nextAvailable,
      });
    });

    getFailedMessage(patientId).then(data => {
      this.setState({
        failedMessages: data,
      });
    });
  };

  render() {
    const {
      className,
      communicationPlan: { communicationPlanSummary: summary, launchDate },
      t,
    } = this.props;

    const { upcomingMessageOffset, nextAvailable, removeDialog } = this.state;

    return (
      <Flex className={className}>
        <Alert
          className="bp3-alert-fill"
          cancelButtonText={t(i18nKeys.cancel)}
          confirmButtonText={t(i18nKeys.ok)}
          intent={Intent.DANGER}
          isOpen={removeDialog}
          onConfirm={this.removePlan}
          onCancel={this.closeRemovePlanDialog}
        >
          <H2>{t(i18nKeys.removePlan)}</H2>
          <BodyTextM>{t(i18nKeys.removePlanDescription)}</BodyTextM>
        </Alert>
        <Flex className={'info'}>
          <BodyTextM fontWeight={'SemiBold'}>{summary.name}</BodyTextM>
          <Button onClick={this.openRemovePlanDialog}>
            {t(i18nKeys.remove)}
          </Button>
        </Flex>
        <Flex className={'progress'}>
          <BodyTextS>
            {t(i18nKeys.startDateMessage, {
              date: dateTimeFormat(launchDate, 'L'),
              interpolation: { escapeValue: false },
            })}
          </BodyTextS>
          <Flex className="bar">
            <ProgressBar
              value={this.calculatePercentage()}
              stripes={false}
              animate={false}
              intent="success"
            />
          </Flex>
        </Flex>
        <Flex className={'upcomming'}>
          <BodyTextM fontWeight={'SemiBold'}>
            {t(i18nKeys.upcomingMessage)}
          </BodyTextM>
          <Flex>
            <Flex
              className={'arrow-left-button'}
              onClick={upcomingMessageOffset > 0 ? this.prev : undefined}
            >
              <img
                src={
                  upcomingMessageOffset > 0 ? ArrowRight : InactiveArrowRight
                }
              />
            </Flex>
            <Flex
              className={'arrow-right-button'}
              onClick={nextAvailable ? this.next : undefined}
            >
              <img src={nextAvailable ? ArrowRight : InactiveArrowRight} />
            </Flex>
          </Flex>
        </Flex>
        {this.renderDayMessage()}
        {this.renderFailedMessage()}
      </Flex>
    );
  }

  calculatePercentage = () => {
    const {
      communicationPlan: { launchDate, communicationPlanSummary },
    } = this.props;

    const durationInDay = communicationPlanSummary.duration * 7;
    const passDuration = diff(now(), launchDate, 'days');
    return passDuration / durationInDay;
  };

  removePlan = () => {
    const { patientId, onRemovePlan } = this.props;
    disassociateFromPatient(patientId).then(() => {
      this.closeRemovePlanDialog();
      onRemovePlan && onRemovePlan();
    });
  };

  openRemovePlanDialog = () =>
    this.setState({
      removeDialog: true,
    });

  closeRemovePlanDialog = () =>
    this.setState({
      removeDialog: false,
    });

  next = () => {
    this.setState(prevState => ({
      upcomingMessageOffset: prevState.upcomingMessageOffset + MESSAGE_PER_VIEW,
    }));
  };

  prev = () => {
    this.setState(prevState => ({
      upcomingMessageOffset: prevState.upcomingMessageOffset - MESSAGE_PER_VIEW,
    }));
  };

  renderDayMessage = () => {
    const { t } = this.props;
    const { upcomingMessages = [] } = this.state;

    return upcomingMessages.map((message: PatientPlanMessageResponse) => {
      return (
        <Flex key={message.id} className={'upcoming-message'}>
          <BodyTextS className={'date'} fontWeight={'SemiBold'}>
            <Tooltip
              content={`${t(i18nKeys.week)} ${message.plannedWeekOrdinal} - ${t(
                i18nKeys.day
              )} ${message.plannedDayOrdinal}`}
              position="right"
            >
              {dateTimeFormat(message.plannedProcessedDate, 'L')}
            </Tooltip>
          </BodyTextS>
          <BodyTextM>{message.message}</BodyTextM>
        </Flex>
      );
    });
  };

  renderFailedMessage = () => {
    const { t } = this.props;
    const { failedMessages } = this.state;

    if (failedMessages && failedMessages.length > 0) {
      const latestMessage = failedMessages.sort((a, b) => {
        return a.plannedProcessedDate - b.plannedProcessedDate;
      })[0];

      return (
        <Flex className={'failed-message'}>
          <Flex className={'title-group'}>
            <img src={AlertCircle} />
            <BodyTextS>
              {t(i18nKeys.messageFailedOn, {
                date: dateTimeFormat(latestMessage.plannedProcessedDate, 'L'),
                interpolation: { escapeValue: false },
              })}
            </BodyTextS>
          </Flex>
          <BodyTextS className={'description'}>
            {latestMessage.message}
          </BodyTextS>
          <Flex className={'act-group'}>
            <Button
              className={'btn-resend'}
              onClick={() => this.onResendFailedMessage(latestMessage.id)}
            >
              {t(i18nKeys.resend)}
            </Button>
            <Button
              className={'btn-skip'}
              onClick={() => this.onSkipFailedMessage(latestMessage.id)}
            >
              {t(i18nKeys.skip)}
            </Button>
          </Flex>
        </Flex>
      );
    }
    return null;
  };

  onResendFailedMessage = messageId => {
    const { patientId } = this.props;
    resendFailedMessage(patientId, messageId).then(() =>
      getFailedMessage(patientId).then(data => {
        this.setState({
          failedMessages: data,
        });
      })
    );
  };

  onSkipFailedMessage = messageId => {
    const { patientId } = this.props;
    skipFailedMessage(patientId, messageId).then(() =>
      getFailedMessage(patientId).then(data => {
        this.setState({
          failedMessages: data,
        });
      })
    );
  };
}

export const OriginOngoingCommunicationPlan = withI18nContext(
  OngoingCommunicationPlan,
  {
    namespace: NAMESPACE_ONGOING_COMMUNICATION_PLAN,
    publicAssetPath: getModuleConfig().publicAssetPath,
  }
);
