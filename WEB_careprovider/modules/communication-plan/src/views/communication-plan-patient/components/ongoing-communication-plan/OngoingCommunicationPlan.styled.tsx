import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IOngoingCommunicationPlanProps } from './models';
import { scaleSpacePx } from '@design-system/core/styles';
import { OriginOngoingCommunicationPlan } from './OngoingCommunicationPlan';

export const OngoingCommunicationPlan = styled(
  OriginOngoingCommunicationPlan
).attrs(({ className }) => ({
  className: getCssClass('sl-OngoingCommunicationPlan', className),
}))<IOngoingCommunicationPlanProps>`
  ${props => {
    const { background, foreground, typography, space } = props.theme;
    return `
    & {
      flex-direction: column;
      .info {
        align-items: center;
        justify-content: space-between;
        padding: ${scaleSpacePx(1)} ${scaleSpacePx(6)};

        > p {
          flex: 1;
        }

        .bp3-button {
          margin-left: ${space.m};
          min-width: 0px !important;
          min-height: 0px !important;
          background: none !important;
          padding: ${scaleSpacePx(0)} ${scaleSpacePx(0)};
          font-weight: ${typography.h4};

          .bp3-button-text {
            color: ${foreground.error.base};
          }
        }
      }

      .progress {
        flex-direction: column;
        padding: ${scaleSpacePx(2)} ${scaleSpacePx(6)} ${scaleSpacePx(
      1
    )} ${scaleSpacePx(6)};

        .bar {
          padding-top: ${scaleSpacePx(2)};
        }

      }
      .upcomming {
        align-items: center;
        justify-content: space-between;
        padding: ${scaleSpacePx(6)} ${scaleSpacePx(6)} ${scaleSpacePx(
      2
    )} ${scaleSpacePx(6)};

        .arrow-left-button {
          align-items: center;
          justify-content: center;
          height: 20px;
          width: 20px;
          margin: ${scaleSpacePx(1)} ${scaleSpacePx(1)};
          cursor: pointer;

          img {
            transform: rotate(180deg);
          }
        }

        .arrow-right-button {
          align-items: center;
          justify-content: center;
          height: 20px;
          width: 20px;
          margin: ${scaleSpacePx(1)} ${scaleSpacePx(1)};
          cursor: pointer;
        }
      }

      .upcoming-message {
        flex-direction: column;
        margin: ${scaleSpacePx(2)} ${scaleSpacePx(6)} ${scaleSpacePx(
      1
    )} ${scaleSpacePx(6)};

        .date {
          color: ${foreground['03']};
        }
      }

      .failed-message {
        margin-top: 8px;
        flex-direction: column;
        background-color: ${background['02']};
        padding: ${scaleSpacePx(2)} ${scaleSpacePx(6)};

        .title-group {
          > img {
            height: 15px;
            width: 15px;
            margin-right: ${scaleSpacePx(2)};
          }
        }

        .description {
          padding: ${scaleSpacePx(1)} ${scaleSpacePx(0)};
        }

        .act-group {
          .bp3-button {
            min-width: 0px !important;
            min-height: 0px !important;
            background: none !important;
            padding: ${scaleSpacePx(0)} ${scaleSpacePx(0)};
            font-weight: ${typography.h4};
          }

          .btn-resend {
            color: ${foreground.primary.base};
            margin-right: ${scaleSpacePx(4)};
          }

          .btn-skip {
            color: ${foreground['02']} !important;
          }
        }
      }
    }`;
  }}
`;
