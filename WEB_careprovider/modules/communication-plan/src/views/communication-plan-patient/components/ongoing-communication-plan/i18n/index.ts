import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './ongoing-communication-plan.json';

export const NAMESPACE_ONGOING_COMMUNICATION_PLAN =
  'ongoing-communication-plan';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
