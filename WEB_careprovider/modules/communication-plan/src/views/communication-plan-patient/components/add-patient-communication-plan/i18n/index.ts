import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './add-patient-communication-plan.json';

export const NAMESPACE_ADD_PATIENT_COMMUNICATION_PLAN =
  'add-patient-communication-plan';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
