import React from 'react';
import {
  IAddPatientCommunicationPlanViewProps,
  IAddPatientCommunicationPlanViewState,
  ICommunicationPlanDateRange,
} from './models';
import { Flex, Box, BodyTextM } from '@design-system/core/components';
import {
  Button,
  Intent,
  Alignment,
  Icon,
  MenuItem,
  RadioGroup,
  Radio,
  Label,
} from '@blueprintjs/core';
import { withTheme } from 'theme';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Select, IItemRendererProps, ItemPredicate } from '@blueprintjs/select';
import {
  CommunicationPlanSummaryResponse,
  CommunicationPlanOrderProperty,
  PlanStatus,
} from 'resource/communication-plan-app-resource/model';
import { DateInput } from '@blueprintjs/datetime';
import { IconNames } from '@blueprintjs/icons';
import { dateTimeFormat, dateToMoment, now } from '@careprovider/services/util';
import MomentLocaleUtils from 'react-day-picker/moment';
import {
  search,
  addPlanToPatient,
} from 'modules/communication-plan/src/services';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { NAMESPACE_ADD_PATIENT_COMMUNICATION_PLAN, i18nKeys } from './i18n';
import { getModuleConfig } from 'modules/communication-plan/module-config';
const CommunicationPlanSelect = Select.ofType<
  CommunicationPlanSummaryResponse
>();

class OriginalAddPatientCommunicationPlanView extends React.PureComponent<
  IAddPatientCommunicationPlanViewProps &
    RouteComponentProps &
    II18nFixedNamespaceContext,
  IAddPatientCommunicationPlanViewState
> {
  state = {
    communicationPlanList: [],
    loading: true,
    selectedCommunicationPlanId: undefined,
    startOption: undefined,
    startDate: now(),
    startWeek: 1,
    startWeekDay: 1,
    endDate: undefined,
  };

  async componentDidMount() {
    const communicationPlanList = await search(
      '',
      CommunicationPlanOrderProperty.NAME,
      [PlanStatus.PUBLISHED]
    );
    this.setState({ communicationPlanList, loading: false });
  }

  render() {
    const {
      className,
      theme: { space },
      t,
    } = this.props;

    const {
      startOption,
      selectedCommunicationPlanId,
      startDate,
      endDate,
    } = this.state;
    const selectedCommunicationPlan = this.getSelectedCommunicationPlan();

    return (
      <Flex className={className}>
        <Flex className="add-plan-form">
          <Box mb={space.s}>
            <Label>{t(i18nKeys.planName)}</Label>
            <CommunicationPlanSelect
              className="sl-select cy-plan-select"
              items={[...this.state.communicationPlanList]}
              activeItem={selectedCommunicationPlan}
              onItemSelect={this.onCommunicationPlanSelect}
              itemRenderer={this.communicationPlanItemRenderer}
              itemPredicate={this.communicationPlanPredicate}
              popoverProps={{
                minimal: true,
                captureDismiss: true,
                fill: true,
              }}
            >
              <Button
                fill
                alignText={Alignment.LEFT}
                text={selectedCommunicationPlan.name}
                rightIcon={<Icon icon="caret-down" />}
              />
            </CommunicationPlanSelect>
          </Box>
          <Box mb={space.s}>
            <RadioGroup
              disabled={!selectedCommunicationPlan.id}
              className="radio-group"
              onChange={this.onStartOptionSelect}
              inline
              selectedValue={startOption}
            >
              <Radio
                label={t(i18nKeys.firstDate)}
                value="firstDay"
                className="inline-radio"
              />
              <Radio
                label={t(i18nKeys.milestone)}
                value="milestone"
                className="inline-radio"
              />
            </RadioGroup>
          </Box>
          {startOption === 'firstDay' && (
            <Box mb={space.s}>{this.renderStartDateOption()}</Box>
          )}
          {startOption === 'milestone' && (
            <Flex mb={space.s}>{this.renderMilestoneOption()}</Flex>
          )}
          {startOption && (
            <BodyTextM color={this.props.theme.foreground['02']}>
              {t(i18nKeys.endDescription, {
                endDate: dateTimeFormat(endDate, 'L'),
                interpolation: { escapeValue: false },
              })}
            </BodyTextM>
          )}
          <Button
            fill
            className="submit-button"
            intent={Intent.PRIMARY}
            disabled={
              !selectedCommunicationPlanId || !startOption || !startDate
            }
            onClick={this.onAddPlan}
          >
            {t(i18nKeys.addPlan)}
          </Button>
        </Flex>
      </Flex>
    );
  }

  onCommunicationPlanSelect = (plan: CommunicationPlanSummaryResponse) => {
    const { duration } = plan;

    const endDate = dateToMoment(now())
      .add(duration, 'weeks')
      .valueOf();

    this.setState({
      selectedCommunicationPlanId: plan.id,
      startOption: undefined,
      startDate: now(),
      startWeek: 1,
      startWeekDay: 1,
      endDate,
    });
  };

  onStartOptionSelect = event => {
    this.setState({
      startOption: event.target.value,
    });
  };

  renderStartDateOption = () => {
    const selectedCommunicationPlan = this.getSelectedCommunicationPlan();

    const today = dateToMoment(now());
    const maxDate = today.toDate();
    const minDate = today
      .clone()
      .add(1, 'day')
      .subtract(selectedCommunicationPlan.duration, 'weeks')
      .toDate();

    const onDateChange = (newDate: Date) => {
      if (newDate) {
        const endDate = dateToMoment(newDate)
          .add(selectedCommunicationPlan.duration, 'weeks')
          .valueOf();
        this.setState({
          startDate: newDate.valueOf(),
          endDate,
        });
      }
    };

    return (
      <DateInput
        inputProps={{
          leftIcon: IconNames.CALENDAR,
        }}
        popoverProps={{
          fill: true,
        }}
        className="cy-start-date-picker"
        canClearSelection={false}
        formatDate={(date: Date) => dateTimeFormat(date, 'L')}
        parseDate={(str: string) => dateToMoment(str, 'L').toDate()}
        defaultValue={new Date(this.state.startDate)}
        placeholder="dd.mm.yyyy"
        locale="vi"
        localeUtils={MomentLocaleUtils}
        minDate={minDate}
        maxDate={maxDate}
        onChange={onDateChange}
      />
    );
  };

  renderMilestoneOption = () => {
    const selectedCommunicationPlan = this.getSelectedCommunicationPlan();
    const { startWeek, startWeekDay } = this.state;
    const { t } = this.props;

    const NumberSelect = Select.ofType<number>();

    return (
      <>
        <Box className="cy-week-select">
          <Label>{t(i18nKeys.week)}</Label>
          <NumberSelect
            filterable={false}
            className="sl-select"
            items={this.toArray(selectedCommunicationPlan.duration)}
            activeItem={startWeek}
            onItemSelect={this.onWeekSelect}
            itemRenderer={(week, { handleClick, modifiers }) => (
              <MenuItem
                active={modifiers.active}
                disabled={modifiers.disabled}
                key={week}
                onClick={handleClick}
                text={week}
                shouldDismissPopover
              />
            )}
            popoverProps={{
              minimal: true,
              captureDismiss: true,
              fill: true,
            }}
          >
            <Button
              fill
              text={startWeek}
              alignText={Alignment.LEFT}
              rightIcon={<Icon icon="caret-down" />}
            />
          </NumberSelect>
        </Box>
        <Box className="cy-week-day-select">
          <Label>{t(i18nKeys.day)}</Label>
          <NumberSelect
            filterable={false}
            className="sl-select"
            items={this.toArray(7)}
            activeItem={startWeekDay}
            onItemSelect={this.onWeekDaySelect}
            itemRenderer={(week, { handleClick, modifiers }) => (
              <MenuItem
                active={modifiers.active}
                disabled={modifiers.disabled}
                key={week}
                onClick={handleClick}
                text={week}
                shouldDismissPopover
              />
            )}
            popoverProps={{
              minimal: true,
              captureDismiss: true,
              fill: true,
            }}
          >
            <Button
              fill
              text={startWeekDay}
              alignText={Alignment.LEFT}
              rightIcon={<Icon icon="caret-down" />}
            />
          </NumberSelect>
        </Box>
      </>
    );
  };

  onAddPlan = async () => {
    const { startDate, endDate, selectedCommunicationPlanId } = this.state;
    const { patientId, callback } = this.props;
    await addPlanToPatient(patientId, {
      communicationPlanId: selectedCommunicationPlanId,
      epochForFirstDate: startDate,
      closeDate: endDate,
    });
    callback && callback();
  };

  onWeekSelect = week => {
    const { startWeekDay } = this.state;
    const { startDate, endDate } = this.calculateStartEndDate(
      week,
      startWeekDay
    );

    this.setState({
      startWeek: week,
      startDate,
      endDate,
    });
  };

  onWeekDaySelect = weekDay => {
    const { startWeek } = this.state;
    const { startDate, endDate } = this.calculateStartEndDate(
      startWeek,
      weekDay
    );
    this.setState({
      startWeekDay: weekDay,
      startDate,
      endDate,
    });
  };

  calculateStartEndDate = (
    startWeek,
    startWeekDay
  ): ICommunicationPlanDateRange => {
    const selectedCommunicationPlan = this.getSelectedCommunicationPlan();
    const planLengthInDay = selectedCommunicationPlan.duration * 7;
    const passedLength = (startWeek - 1) * 7 + startWeekDay;

    const startDate = dateToMoment(now())
      .add(1, 'day')
      .subtract(passedLength, 'days')
      .valueOf();
    const endDate = dateToMoment(startDate)
      .add(planLengthInDay, 'days')
      .valueOf();

    return {
      startDate,
      endDate,
    };
  };

  getSelectedCommunicationPlan = () => {
    const {
      communicationPlanList = [],
      selectedCommunicationPlanId,
    } = this.state;
    return (
      communicationPlanList.find(
        plan => plan.id === selectedCommunicationPlanId
      ) || {
        name: this.props.t(i18nKeys.choosePlanPlaceholder),
      }
    );
  };

  communicationPlanItemRenderer = (
    plan: CommunicationPlanSummaryResponse,
    { handleClick, modifiers }: IItemRendererProps
  ): JSX.Element => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        key={plan.id}
        onClick={handleClick}
        text={plan.name}
        shouldDismissPopover
      />
    );
  };

  toArray = number => [...Array.from({ length: number }, (v, k) => k + 1)];

  communicationPlanPredicate: ItemPredicate<
    CommunicationPlanSummaryResponse
  > = (query, item, _index, exactMatch) => {
    const normalizedName = item.name.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
      return normalizedName === normalizedQuery;
    } else {
      return normalizedName.indexOf(normalizedQuery) > -1;
    }
  };
}

export const AddPatientCommunicationPlanView = withTheme(
  withRouter(
    withI18nContext(OriginalAddPatientCommunicationPlanView, {
      namespace: NAMESPACE_ADD_PATIENT_COMMUNICATION_PLAN,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
