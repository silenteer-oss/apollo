import { ICareProviderTheme } from 'theme';
import { CommunicationPlanSummaryResponse } from 'resource/communication-plan-app-resource/model';

export interface IAddPatientCommunicationPlanViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId: string;
  callback?: () => void;
}

export interface IAddPatientCommunicationPlanViewState {
  communicationPlanList: CommunicationPlanSummaryResponse[];
  loading: boolean;
  selectedCommunicationPlanId: string;
  startOption: string;
  startDate: number;
  endDate?: number;
  startWeek?: number;
  startWeekDay?: number;
}

export interface IAddPatientCommunicationPlanInfo {
  communicationPlanId: string;
  patientId: string;
  startOption: string;
  startDate: number;
  startWeek?: number;
  startWeekDay?: number;
}

export interface ICommunicationPlanDateRange {
  startDate: number;
  endDate: number;
}
