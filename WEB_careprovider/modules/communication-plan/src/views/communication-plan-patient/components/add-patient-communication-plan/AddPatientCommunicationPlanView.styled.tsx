import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IAddPatientCommunicationPlanViewProps } from './models';
import { AddPatientCommunicationPlanView as OriginalAddPatientCommunicationPlanView } from './AddPatientCommunicationPlanView';
import { scaleSpacePx } from '@design-system/core/styles';

export const StyledAddPatientCommunicationPlanView = styled(
  OriginalAddPatientCommunicationPlanView
).attrs(({ className }) => ({
  className: getCssClass('sl-AddPatientCommunicationPlanView', className),
}))<IAddPatientCommunicationPlanViewProps>`
  ${props => {
    const { background } = props.theme;

    return `
    & {
      justify-content: center;

      > .sl-Header {
        border-bottom: 1px solid ${background['01']};
      }

      > .add-plan-form {
        flex: 1;
        max-width: 720px;
        padding-top: ${scaleSpacePx(20)};
        flex-direction: column;

        .cy-start-date-picker {
          flex: 1;
        }

        .radio-group {
          display: flex;
          flex: 1;

          .inline-radio {
            flex: 1;
          }
        }

        .bp3-label {
          font-size: 14px
        }

        .cy-week-select {
          flex: 1;
          padding-right: ${scaleSpacePx(5)};
        }

        .cy-week-day-select {
          flex: 1;
          padding-left: ${scaleSpacePx(5)};
        }

        .submit-button {
          margin-top: ${scaleSpacePx(6)};
        }
      }
    }`;
  }}
`;
