import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICommunicationPlanPatientViewProps } from './models';
import { OriginCommunicationPlanPatientView } from './CommunicationPlanPatientView';
import { scaleSpacePx } from '@design-system/core/styles';

export const CommunicationPlanPatienView = styled(
  OriginCommunicationPlanPatientView
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanPatienView', className),
}))<ICommunicationPlanPatientViewProps>`
  ${props => {
    const { theme } = props;
    return `
    & {
      width: 384px;
      flex-direction: column;
      background-color: ${theme.background.white} !important;

      > .container {
        flex: 1;
        flex-direction: column;
        padding: ${scaleSpacePx(3)} ${scaleSpacePx(6)};
        min-height: ${scaleSpacePx(6)};
      }

      > p {
        color: ${theme.foreground['02']}};
      }

      .btn-add {
        margin-top: ${scaleSpacePx(4)};
        color: ${theme.foreground['01']}} !important;
        background-color: ${theme.background['01']} !important;

        .bp3-button-text {
          font-weight: 600 !important;
        }
      }
    }`;
  }}
`;
