import React from 'react';
import {
  ICommunicationPlanPatientViewProps,
  ICommunicationPlanPatientViewState,
} from './models';
import { Flex, BodyTextM, LoadingState } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { OngoingCommunicationPlan } from './components/ongoing-communication-plan/OngoingCommunicationPlan.styled';
import { NAMESPACE_COMMUNICATION_PLAN_PATIENT, i18nKeys } from './i18n';
import { Button, Dialog } from '@blueprintjs/core';
import { StyledAddPatientCommunicationPlanView } from './components/add-patient-communication-plan/AddPatientCommunicationPlanView.styled';
import { getCssClass } from 'infrastructure/utils';
import { withTheme } from 'theme';
import { getModuleConfig } from 'modules/communication-plan/module-config';
import { getOngoingPlan } from '../../services';

class CommunicationPlanPatientView extends React.PureComponent<
  ICommunicationPlanPatientViewProps & II18nFixedNamespaceContext,
  ICommunicationPlanPatientViewState
> {
  state = {
    patientCommunicationPlan: undefined,
    openAddPatientCommunicationPlanView: false,
    loading: true,
  };

  componentDidMount() {
    this.getOngoingCommunicationPlan();
  }

  componentDidUpdate(prevProps) {
    if (this.props.patientId && this.props.patientId !== prevProps.patientId) {
      this.getOngoingCommunicationPlan();
    }
  }

  render() {
    const { className } = this.props;
    return <Flex className={className}>{this.renderPlan()}</Flex>;
  }

  renderPlan = () => {
    const { t } = this.props;
    const { patientCommunicationPlan, loading } = this.state;
    const { theme } = this.props;
    if (loading) {
      return (
        <Flex className="container">
          <LoadingState size={24} />
        </Flex>
      );
    }

    return patientCommunicationPlan ? (
      <OngoingCommunicationPlan
        patientId={this.props.patientId}
        communicationPlan={patientCommunicationPlan}
        onRemovePlan={this.getOngoingCommunicationPlan}
      />
    ) : (
      <Flex className="container">
        <BodyTextM>{t(i18nKeys.noCommunicationPlan)}</BodyTextM>
        <Button
          className={'btn-add'}
          onClick={this.openAddPatientCommunicationPlanDialog}
        >
          {t(i18nKeys.addCommunicationPlan)}
        </Button>
        <Dialog
          className={getCssClass(
            'bp3-dialog-fullscreen',
            'bp3-dialog-content-scrollable'
          )}
          title={t(i18nKeys.addCommunicationPlan)}
          onClose={this.closeAddPatientCommunicationPlanDialog}
          isOpen={this.state.openAddPatientCommunicationPlanView}
          canOutsideClickClose={false}
        >
          <StyledAddPatientCommunicationPlanView
            theme={theme}
            patientId={this.props.patientId}
            callback={() => {
              this.closeAddPatientCommunicationPlanDialog;
              this.getOngoingCommunicationPlan();
            }}
          />
        </Dialog>
      </Flex>
    );
  };

  openAddPatientCommunicationPlanDialog = () =>
    this.setState({
      openAddPatientCommunicationPlanView: true,
    });

  closeAddPatientCommunicationPlanDialog = () =>
    this.setState({
      openAddPatientCommunicationPlanView: false,
    });

  getOngoingCommunicationPlan = () => {
    const { patientId } = this.props;
    if (!patientId) return;

    this.setState({
      loading: true,
    });
    getOngoingPlan(patientId).then(data => {
      this.setState({
        patientCommunicationPlan: data[0],
        loading: false,
        openAddPatientCommunicationPlanView: false,
      });
    });
  };
}

export const OriginCommunicationPlanPatientView = withTheme(
  withI18nContext(CommunicationPlanPatientView, {
    namespace: NAMESPACE_COMMUNICATION_PLAN_PATIENT,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
