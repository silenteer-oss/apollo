import { ICareProviderTheme } from '@careprovider/theme';
import {
  PatientCommunicationPlanResponse,
  PatientPlanMessageResponse,
} from 'resource/communication-plan-app-resource/model';

export interface ICommunicationPlanPatientViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId?: string;
}

export interface ICommunicationPlanPatientViewState {
  patientCommunicationPlan?: PatientCommunicationPlanResponse;
  openAddPatientCommunicationPlanView: boolean;
  loading: boolean;
}

export interface IOngoingCommunicationPlanViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  patientId: string;
  communicationPlan?: PatientCommunicationPlanResponse;
}

export interface IOngoingCommunicationPlanViewState {
  upcomingMessageOffset: number;
  upcomingMessages: PatientPlanMessageResponse[];
  nextAvailable: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IPublishedCommunicationPlan {}
