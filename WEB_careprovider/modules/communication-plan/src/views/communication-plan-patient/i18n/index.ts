import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './communication-plan-patient.json';

export const NAMESPACE_COMMUNICATION_PLAN_PATIENT =
  'communication-plan-patient';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
