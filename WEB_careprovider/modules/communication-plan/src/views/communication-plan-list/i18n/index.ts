import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './communication-plan-list-view.json';

export const NAMESPACE_COMMUNICATION_PLAN_LIST_VIEW =
  'communication-plan-list-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
