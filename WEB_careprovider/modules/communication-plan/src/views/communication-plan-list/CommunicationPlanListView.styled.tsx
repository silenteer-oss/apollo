import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICommunicationPlanListViewProps } from './models';
import { CommunicationPlanListView as OriginCommunicationPlanListView } from './CommunicationPlanListView';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';

export const CommunicationPlanListView = styled(
  OriginCommunicationPlanListView
).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanListView', className),
}))<ICommunicationPlanListViewProps>`
  ${props => {
    const { space, foreground, background, radius, typography } = props.theme;
    const headerItemHeight = scaleSpace(10);

    return `
      & {
        height: 100%;
        width: 100%;
        flex: 1;
        flex-direction: column;

        > .sl-Header {
          height: 64px;
          border-bottom: 1px solid ${background['01']};

          > .header-title {
            flex-direction: column;
            width: auto;
            flex: 1;

            > h1 {
              flex: 1;
              font-size: 18px;
            }
            > .detail {
              font-size: 12px;
              color: ${foreground['02']};
            }
          }

          > .create-plan-button {
            margin-right: ${space.m};
            width: auto;
            height: ${headerItemHeight}px;
            > .bp3-button-text {
              font-size: 14px;
              font-weight: ${typography.link.fontWeight};
            }
          }
        }

        > .search-box {
          background-color: ${background['02']};
          padding: ${space.xs} ${space.s};
          margin: ${space.s} ${space.m} 0 ${space.m};

          > .search-input {
            height: ${scaleSpacePx(10)};
            width: 512px;

            > .bp3-input {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
          }
          .search-icon {
            margin: ${scaleSpacePx(3)} ${scaleSpacePx(4)} !important;
          }

          > .filters {
            flex: 1;

            > .filter-item {
              + .filter-item {
                margin-left: ${space.l};
              }

              .bp3-checkbox {
                margin-bottom: 0;
                color: ${foreground.primary.base};
              }
            }
          }
        }

        > .table-view {
          overflow: auto;
          margin: 0px ${space.m};

          .bp3-html-table {
            border-bottom: none !important;
          }

          thead {
            background-color: ${background.white};

            th {
              top: 0px;
              position: sticky;
              background-color: ${background.white};
              border-bottom: 1px solid ${background['01']} !important;
            }

            :first-child td {
              box-shadow: none !important;
            }

            .col-name {
              width: ${scaleSpacePx(82)};
            }

            .col-status {
              padding-top: ${scaleSpacePx(4)} !important;
              width: ${scaleSpacePx(36)};
            }

            .col-created-date {
              width: ${scaleSpacePx(48)};
            }

            .col-updated-date {
              width: ${scaleSpacePx(48)};
            }

            .col-action {

            }
          }

          td {
            border-bottom: 1px solid ${background['01']} !important;
            padding: ${space.xs} ${space.s} !important;
          }

          .empty-cell {
            border-bottom: none !important;
            color: ${foreground['03']};
            font-size: ${typography.bodyM.fontSize};
            font-weight: ${typography.link.fontWeight};
          }

          tr {
            height: ${scaleSpacePx(14)};

            :first-child td {
              box-shadow: none !important;
            }

            .col-status {
              padding-top: ${space.s} !important;
            }

            .box {
              height: ${scaleSpacePx(10)};
              justify-content: space-between;
              flex-direction: column;
              align-items: flex-start;

              .plan-name {
                background: none !important;
                border: none !important;
                min-width: 0;
                min-height: 0;
                max-width: ${scaleSpacePx(82)};
                padding: 0;

                > span {
                  color: ${foreground.primary.base}
                  line-height: 20px;
                  font-weight: 600;
                  text-overflow: ellipsis;
                  white-space: nowrap;
                  overflow: hidden;
                }
              }

              .duration {
                color: ${foreground['02']};
              }

              .date-time {
                line-height: 20px;
              }

              .user-name {
                line-height: 20px;
                color: ${foreground['02']};
              }
            }

            .status {
              padding: 0 ${space.xxs};
              border-radius: ${radius['4px']};
              text-align: center;
              width: 72px;

              &.draft {
                color: ${foreground.warn.base};
                background-color: ${background.warn.lighter};
              }

              &.published {
                color: ${foreground.success.base};
                background-color: ${background.success.lighter};
              }
            }

            .more-info {
              background: none;
              border: none;
              height: ${scaleSpacePx(10)};
              align-items: center;
              justify-content: flex-end;

              > .btn-text {
                background: none !important;
                border: none !important;
                min-width: ${scaleSpacePx(30)};
                justify-content: center;
                padding: 0 ${space.m};

                > span {
                  color: ${foreground.primary.base}
                  font-weight: 600;
                }
              }

              .bp3-button-text {
                color: ${foreground.primary.base}
                font-weight: 600;
              }
            }
          }
        }
      }
    `;
  }}
`;
