import { ICareProviderTheme } from '@careprovider/theme';
import { scaleSpacePx } from 'core/styles';

export function mixGlobalStyle(theme: ICareProviderTheme): string {
  return `
      .bp3-dialog.bp3-alert.delete-alert {

        .loading {
          width: 400px;
          min-height: 40px;
          align-items: center;
          justify-content: center;
        }

        img {
          width: 40px;
          height: 40px;
        }

        h2 {
          margin-bottom: ${scaleSpacePx(2)};
        }
        p.delete-message {
          margin-bottom: ${scaleSpacePx(4)};
        }
        p.delete-description {
          color: ${theme.foreground['02']};
        }

        .bp3-alert-footer {
          flex-direction: column-reverse;

          .bp3-button {
            margin: ${scaleSpacePx(2)} 0px;
          }

          > .bp3-button.bp3-intent-danger {
            min-height: 0px !important;
            min-width: 0px !important;
            background: none !important;
            padding: ${scaleSpacePx(0)} ${scaleSpacePx(2)} !important;
            font-weight: 600;

            .bp3-button-text {
              color: ${theme.foreground.error.base}
            }
          }
        }
      }
  `;
}
