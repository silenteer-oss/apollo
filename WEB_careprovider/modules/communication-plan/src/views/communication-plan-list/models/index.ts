import { ICareProviderTheme } from '@careprovider/theme';
import {
  PlanStatus,
  CommunicationPlanOrderProperty,
  CommunicationPlanSummaryResponse,
} from 'resource/communication-plan-app-resource/model';
import { ICommunicationPlanSummary } from 'modules/communication-plan/services';

export interface ICommunicationPlanListViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ICommunicationPlanListViewState {
  loading: boolean;
  communicationPlans: CommunicationPlanSummaryResponse[];
  searchText: string;
  orderBy: CommunicationPlanOrderProperty;
  statuses: PlanStatus[];
  openCreatePlanDialog: boolean;
  deletePlanDialog: boolean;
  selectedPlan?: ICommunicationPlanSummary;
  selectedPlanActivePatient?: number;
  loadingSelectedPlanActivePatient?: boolean;
}
