import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  ICommunicationPlanContext,
  withCommunicationPlanContext,
} from '../../context';
import {
  ICommunicationPlanListViewProps,
  ICommunicationPlanListViewState,
} from './models';
import {
  Flex,
  H1,
  BodyTextM,
  BodyTextS,
  LoadingState,
} from '@design-system/core/components';
import { Header } from '@careprovider/components';
import {
  Intent,
  InputGroup,
  Icon,
  HTMLTable,
  Button,
  Position,
  Popover,
  Menu,
  MenuItem,
  Dialog,
  Classes,
  Spinner,
  Checkbox,
  Tooltip,
  Alert,
} from '@blueprintjs/core';
import { dateTimeFormat } from '@careprovider/services/util';
import { IEmployeeProfile } from '@careprovider/services/biz';
import { getCssClass } from '@design-system/infrastructure/utils';
import {
  IGlobalStyleContext,
  withGlobalStyleContext,
} from '@design-system/themes/context';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import {
  ICommunicationPlanSummary,
  PlanStatus,
  PlanDurationUnit,
  PlanOrderBy,
  debounce,
  search,
  deletePlan,
  getActivePatient,
} from '../../services';
import { CreateCommunicationPlanView } from '../create-communication-plan';
import { NAMESPACE_COMMUNICATION_PLAN_LIST_VIEW, i18nKeys } from './i18n';
import AlertIcon from '@careprovider/assets/images/red-alarm.png';
import { mixGlobalStyle } from './styles/mix-global-style';

class OriginCommunicationPlanListView extends React.PureComponent<
  ICommunicationPlanListViewProps &
    ICommunicationPlanContext &
    RouteComponentProps &
    IGlobalStyleContext &
    II18nFixedNamespaceContext,
  ICommunicationPlanListViewState
> {
  private _searchInputRef: HTMLInputElement;

  state: ICommunicationPlanListViewState = {
    loading: true,
    communicationPlans: [],
    searchText: '',
    orderBy: PlanOrderBy.NAME,
    statuses: [PlanStatus.DRAFT, PlanStatus.PUBLISHED],
    openCreatePlanDialog: false,
    deletePlanDialog: false,
    selectedPlan: undefined,
    selectedPlanActivePatient: 0,
    loadingSelectedPlanActivePatient: false,
  };

  async componentDidMount() {
    await this.performSearch('');
    this.props.addGlobalStyle(
      'deletePlanAlert',
      mixGlobalStyle(this.props.theme)
    );
  }

  render() {
    const { className, theme, t } = this.props;
    const {
      communicationPlans,
      openCreatePlanDialog,
      loading,
      deletePlanDialog,
      statuses,
      selectedPlan,
      loadingSelectedPlanActivePatient,
    } = this.state;

    if (!communicationPlans) {
      return null;
    }

    return (
      <Flex className={className}>
        {selectedPlan && (
          <Alert
            className="delete-alert"
            cancelButtonText={
              loadingSelectedPlanActivePatient ? null : t(i18nKeys.cancel)
            }
            confirmButtonText={
              loadingSelectedPlanActivePatient ? null : t(i18nKeys.deletePlan)
            }
            intent={Intent.DANGER}
            isOpen={deletePlanDialog}
            onConfirm={this.onDelete}
            onCancel={this.onCloseDeletePlanDialog}
            canEscapeKeyCancel
          >
            {loadingSelectedPlanActivePatient ? (
              <Flex className="loading">
                <LoadingState size={24} />
              </Flex>
            ) : (
              this.renderDeletePlanView()
            )}
          </Alert>
        )}
        <Header>
          <Flex className="header-title">
            <H1 margin={0}>{t(i18nKeys.headerTitle)}</H1>
            <Flex className="detail">
              {communicationPlans.length > 0
                ? t(i18nKeys.detail, { number: communicationPlans.length })
                : t(i18nKeys.emptyDetail)}
            </Flex>
          </Flex>
          <Button
            className="create-plan-button"
            text={t(i18nKeys.createPlan)}
            intent={Intent.PRIMARY}
            onClick={this.openCreateDialog}
          />
        </Header>
        <Flex className="search-box" align="center">
          <InputGroup
            className="search-input"
            rightElement={
              loading ? (
                <Spinner size={16} className="search-icon" />
              ) : (
                <Icon icon="search" iconSize={16} className="search-icon" />
              )
            }
            placeholder={t(i18nKeys.search)}
            inputRef={this._getSearchInputRef}
            onChange={this.handleDebounce}
            maxLength={100}
          />
          <Flex className="filters" align="center" justify="flex-end">
            <Tooltip
              className="filter-item"
              content={t(i18nKeys.statusDraftTooltip)}
              position="bottom"
            >
              <Checkbox
                checked={statuses.includes(PlanStatus.DRAFT)}
                label={`${t(i18nKeys.statusDraft)}`}
                onChange={() => this.handleChangeStatusFilter(PlanStatus.DRAFT)}
              />
            </Tooltip>
            <Tooltip
              className="filter-item"
              content={t(i18nKeys.statusPublishedTooltip)}
              position="bottom"
            >
              <Checkbox
                checked={statuses.includes(PlanStatus.PUBLISHED)}
                label={`${t(i18nKeys.statusPublished)}`}
                onChange={() =>
                  this.handleChangeStatusFilter(PlanStatus.PUBLISHED)
                }
              />
            </Tooltip>
          </Flex>
        </Flex>
        {loading ? (
          <LoadingState size={24} />
        ) : (
          <Flex className="table-view">
            <HTMLTable>
              <thead>
                <tr>
                  <th className="col-name">{t(i18nKeys.name)}</th>
                  <th className="col-status">{t(i18nKeys.status)}</th>
                  <th className="col-created-date">
                    {t(i18nKeys.dateCreated)}
                  </th>
                  <th className="col-updated-date">{t(i18nKeys.lastEdited)}</th>
                  <th className="action" />
                </tr>
              </thead>
              <tbody>{this.renderCommunicationPlans()}</tbody>
            </HTMLTable>
            <Dialog
              title={t(i18nKeys.createPlan)}
              onClose={this.closeCreateDialog}
              isOpen={openCreatePlanDialog}
              canOutsideClickClose={false}
            >
              <div className={Classes.DIALOG_BODY}>
                <CreateCommunicationPlanView theme={theme} />
              </div>
            </Dialog>
          </Flex>
        )}
      </Flex>
    );
  }

  renderDeletePlanView = () => {
    const { t } = this.props;
    const { selectedPlan, selectedPlanActivePatient } = this.state;

    if (selectedPlan.status === PlanStatus.PUBLISHED) {
      return (
        <>
          <img src={AlertIcon} />
          <h2
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.deletePlanHeader, {
                planName: selectedPlan && selectedPlan.name,
              }),
            }}
          />
          <p
            className="delete-message"
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.deletePublishedPlanContent, {
                patientCount: selectedPlanActivePatient,
              }),
            }}
          />
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription1)}
          </p>
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription2)}
          </p>
          <p className="delete-description">
            {t(i18nKeys.deletePlanDescription3)}
          </p>
        </>
      );
    }

    return (
      <>
        <img src={AlertIcon} />
        <h2
          dangerouslySetInnerHTML={{
            __html: t(i18nKeys.deletePlanHeader, {
              planName: selectedPlan && selectedPlan.name,
            }),
          }}
        />
        <p>
          {t(i18nKeys.deletePlanContent, {
            patientCount: selectedPlanActivePatient,
          })}
        </p>
      </>
    );
  };

  onOpenDeletePlanDialog = (plan: ICommunicationPlanSummary) => {
    const published = plan.status === PlanStatus.PUBLISHED;

    this.setState({
      deletePlanDialog: true,
      selectedPlan: plan,
      loadingSelectedPlanActivePatient: published,
    });

    if (published) {
      getActivePatient(plan.id)
        .then(response =>
          this.setState({
            selectedPlanActivePatient: response.activePatientCount,
            loadingSelectedPlanActivePatient: false,
          })
        )
        .catch(() => {
          this.setState({
            selectedPlanActivePatient: 0,
            loadingSelectedPlanActivePatient: false,
          });
        });
    }
  };

  onCloseDeletePlanDialog = () => {
    this.setState({
      deletePlanDialog: false,
      selectedPlan: undefined,
    });
  };

  onDelete = () => {
    const { selectedPlan } = this.state;
    if (selectedPlan) {
      deletePlan(selectedPlan.id).then(() => {
        this.onCloseDeletePlanDialog();
        this.performSearch(this._searchInputRef.value);
      });
    }
  };

  renderCommunicationPlans = () => {
    const { communicationPlans } = this.state;
    return communicationPlans.length === 0
      ? this.renderEmptyList()
      : communicationPlans.map(plan => {
          return this.renderRow(plan);
        });
  };

  performSearch = async searchText => {
    const { orderBy, statuses } = this.state;
    this.setState({
      loading: true,
    });
    const communicationPlans = await search(searchText, orderBy, statuses);
    this.setState({ communicationPlans, loading: false, searchText });
  };

  closeCreateDialog = () => this.setState({ openCreatePlanDialog: false });
  openCreateDialog = () => this.setState({ openCreatePlanDialog: true });

  handleChangeStatusFilter = (status: PlanStatus): void => {
    const { statuses } = this.state;

    this.setState(
      {
        statuses: statuses.includes(status)
          ? statuses.filter(item => item !== status)
          : statuses.concat(status),
      },
      () => {
        console.log(this._searchInputRef.value);
        this.performSearch(this._searchInputRef.value);
      }
    );
  };

  handleDebounce = event => {
    event.persist();
    this.onSearchQueryChanged(event);
  };

  onSearchQueryChanged = debounce(500, async event => {
    const searchText = event.target.value;
    await this.performSearch(searchText);
  });

  renderEmptyList = () => {
    const { searchText } = this.state;
    const { t } = this.props;
    return (
      <tr>
        <td className="empty-cell" colSpan={5}>
          {searchText ? t(i18nKeys.noMarchingItem) : t(i18nKeys.emptyMessage)}
        </td>
      </tr>
    );
  };

  renderRow = (plan: ICommunicationPlanSummary) => {
    const { t } = this.props;
    const durationText =
      plan.durationUnit === PlanDurationUnit.WEEK
        ? t(i18nKeys.weeks)
        : t(i18nKeys.days);
    const statusClass = plan.status.toLowerCase();
    const statusText =
      plan.status === PlanStatus.DRAFT
        ? t(i18nKeys.statusDraft)
        : t(i18nKeys.statusPublished);

    const actionsMenu = (
      <Menu>
        <MenuItem
          className={'alert'}
          text={
            <BodyTextM color={this.props.theme.foreground.error.base}>
              {t(i18nKeys.delete)}
            </BodyTextM>
          }
          onClick={() => this.onOpenDeletePlanDialog(plan)}
        />
      </Menu>
    );

    return (
      <tr key={plan.id}>
        <td className={'col-name'}>
          <Flex className={'box'}>
            <Button
              className={'plan-name'}
              onClick={() => this.openPlanDetail(plan)}
            >
              {plan.name}
            </Button>
            <BodyTextS className={'duration'}>
              {`${plan.duration} ${durationText}`}
            </BodyTextS>
          </Flex>
        </td>
        <td className={'col-status'}>
          <BodyTextM
            className={getCssClass('status', statusClass)}
            fontWeight="SemiBold"
          >
            {statusText}
          </BodyTextM>
        </td>
        <td className={'col-created-date'}>
          <Flex className={'box'}>
            <BodyTextM className={'date-time'}>
              {dateTimeFormat(plan.createdTime, 'L LT')}
            </BodyTextM>
            <CreatorName
              creatorId={plan.creatorId}
              getEmployeeProfiles={this.props.getEmployeeProfiles}
            />
          </Flex>
        </td>
        <td className={'col-updated-date'}>
          <Flex className={'box'}>
            <BodyTextM className={'date-time'}>
              {dateTimeFormat(plan.lastModifiedTime, 'L LT')}
            </BodyTextM>
            <CreatorName
              creatorId={plan.lastModifierId}
              getEmployeeProfiles={this.props.getEmployeeProfiles}
            />
          </Flex>
        </td>
        <td className={'col-action'}>
          <Flex className={'more-info'}>
            {/* <Button className={'btn-text'}>Create from this</Button> */}
            <Popover content={actionsMenu} position={Position.BOTTOM_RIGHT}>
              <Button icon="more" intent={Intent.PRIMARY} minimal small />
            </Popover>
          </Flex>
        </td>
      </tr>
    );
  };

  openPlanDetail = plan => {
    const {
      history,
      location: { pathname },
    } = this.props;
    history.push(`${pathname}/${plan.id}`);
  };

  private _getSearchInputRef = (ref: HTMLInputElement) => {
    this._searchInputRef = ref;
  };
}

const CreatorName = ({
  creatorId,
  getEmployeeProfiles,
}: {
  creatorId: string;
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
}) => {
  const [name, setName] = React.useState('');
  React.useEffect(() => {
    getEmployeeProfiles().then(list => {
      const creator = list.find(c => c.id === creatorId);
      if (creator) {
        setName(creator.fullName);
      }
    });
  }, []);
  return <BodyTextS className={'user-name'}>{`${name}`}</BodyTextS>;
};

export const CommunicationPlanListView = withRouter(
  withTheme(
    withCommunicationPlanContext(
      withGlobalStyleContext(
        withI18nContext(OriginCommunicationPlanListView, {
          namespace: NAMESPACE_COMMUNICATION_PLAN_LIST_VIEW,
          publicAssetPath: getModuleConfig().publicAssetPath,
        })
      )
    )
  )
);
