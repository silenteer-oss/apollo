import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const COMMUNICATION_PLAN_MODULE_ID = 'CommunicationPlanModule';

export const COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID =
  'CommunicationPlanContextProvider';
export const COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID =
  'CommunicationPlanContextConsumer';

export const COMMUNICATION_PLAN_LIST_VIEW_ID = 'CommunicationPlanListView';

export const COMMUNICATION_PLAN_TIMELINE_VIEW_ID =
  'CommunicationPlanTimelineView';

export const COMMUNICATION_PLAN_PATIENT_VIEW_ID =
  'CommunicationPlanPatientView';

export interface IModuleViews {
  [COMMUNICATION_PLAN_LIST_VIEW_ID]: string;
  [COMMUNICATION_PLAN_TIMELINE_VIEW_ID]: string;
  [COMMUNICATION_PLAN_PATIENT_VIEW_ID]: string;
}
export interface IModuleProviders {
  [COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID]: string;
}
export interface IModuleConsumers {
  [COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID]: string;
}

export function getModuleConfig(): IModuleConfig<
  IModuleViews,
  IModuleProviders,
  IModuleConsumers
> {
  return {
    id: COMMUNICATION_PLAN_MODULE_ID,
    name: 'Communication Plan Module',
    publicAssetPath: `/module/communication-plan`,
    views: {
      CommunicationPlanListView: COMMUNICATION_PLAN_LIST_VIEW_ID,
      CommunicationPlanTimelineView: COMMUNICATION_PLAN_TIMELINE_VIEW_ID,
      CommunicationPlanPatientView: COMMUNICATION_PLAN_PATIENT_VIEW_ID,
    },
    providers: {
      CommunicationPlanContextProvider: COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID,
    },
    consumers: {
      CommunicationPlanContextConsumer: COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID,
    },
  };
}
