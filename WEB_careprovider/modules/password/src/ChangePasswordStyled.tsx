import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IChangePasswordViewProps } from './ChangePasswordModel';
import { ChangePasswordView as OriginChangePasswordView } from './ChangePasswordView';

export default styled(OriginChangePasswordView).attrs(({ className }) => ({
  className: getCssClass('sl-ChangePasswordView', className),
}))<IChangePasswordViewProps>`
  ${props => {
    const { foreground, space, breakpoint } = props.theme;

    return `
      & {
        padding: ${space.m};

        .change-password-container {
          width: 100%;
          max-width: ${breakpoint['480px']};
        }

        .change-password-row + .change-password-row {
          margin-top: ${space.xs};
        }

        .password-error {
          color: ${foreground.error.base};
          min-height: 20px;
        }

        .bp3-button {
          margin-top: ${space.s};
        }
      }
    `;
  }}
`;
