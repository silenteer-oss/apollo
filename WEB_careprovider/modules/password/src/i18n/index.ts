import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './change-password-view.json';

export const NAMESPACE_CHANGE_PASSWORD_VIEW = 'change-password-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
