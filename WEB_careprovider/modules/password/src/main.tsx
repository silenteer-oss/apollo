import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import {
  ThemeProvider,
  ThemeConsumer,
  theme,
  getAllViewTheme,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
  getAllComponentTheme,
} from '@careprovider/theme';
import {
  initWorker,
  terminateWorker,
  hashPassword,
} from '@careprovider/services/webworker';
import ChangePasswordView from './ChangePasswordStyled';

import '@careprovider/assets/styles/index.scss';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

export class RootView extends React.PureComponent<
  any,
  { isInitedI18n: boolean }
> {
  state = { isInitedI18n: false };

  constructor(props) {
    super(props);

    initWorker();
  }

  componentWillUnmount() {
    terminateWorker();
  }

  render() {
    const { isInitedI18n } = this.state;

    return (
      <I18nContextProvider
        {...getI18nInitOptions()}
        onInitedI18n={this._handleInitedI18n}
      >
        <ThemeProvider theme={theme}>
          <ThemeConsumer>
            {theme => (
              <GlobalStyleContextProvider
                styles={{
                  main: mixGlobalStyle(theme),
                  blueprint: mixCustomBlueprintStyle(theme),
                }}
              >
                {isInitedI18n && (
                  <ChangePasswordView hashPassword={hashPassword} />
                )}
              </GlobalStyleContextProvider>
            )}
          </ThemeConsumer>
        </ThemeProvider>
      </I18nContextProvider>
    );
  }

  private _handleInitedI18n = () => {
    this.setState({ isInitedI18n: true });
  };
}

ReactDOM.render(<RootView />, document.getElementById('root'));
