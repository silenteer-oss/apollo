import {
  AdminApi,
  AdminApiModel,
} from '@infrastructure/resource/AdminResource';

export async function changePassword(
  currentPassword: string,
  newPassword: string,
  accountId: string,
  hashPasswordHandler: (plainPassword: string, salt: string) => Promise<string>
) {
  if (accountId) {
    const seed = await AdminApi.getSeed(accountId).then(({ data }) => data);
    const currentPasswordHash = await hashPasswordHandler(
      currentPassword,
      seed
    );
    const newPasswordHash = await hashPasswordHandler(newPassword, seed);

    const payload: AdminApiModel.EmployeeChangePasswordRequest = {
      currentPassword: currentPasswordHash,
      newPassword: newPasswordHash,
    };

    return AdminApi.changePassword(payload);
  }
  return Promise.reject('NOT_FOUND_ACCOUNT_ID');
}
