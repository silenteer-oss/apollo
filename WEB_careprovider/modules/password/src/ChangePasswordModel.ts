import { ICareProviderTheme } from '@careprovider/theme';

export interface IPasswordRequest {
  currentPassword: string;
  newPassword: string;
  confirmNewPassword: string;
}

export interface IChangePasswordViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  accountId?: string;
  onChange?: () => void;
  hashPassword: (plainPassword: string, salt: string) => Promise<string>;
}
