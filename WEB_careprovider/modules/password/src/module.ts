import {
  CHANGE_PASSWORD_MODULE_ID,
  CHANGE_PASSWORD_VIEW_ID,
} from '../module-config';
import ChangePasswordView from './ChangePasswordStyled';

export default {
  [CHANGE_PASSWORD_MODULE_ID]: {
    [CHANGE_PASSWORD_VIEW_ID]: ChangePasswordView,
  },
};
