import React from 'react';
import { FormikBag, FormikErrors, FormikProps, withFormik } from 'formik';
import { Button, Label, InputGroup } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getModuleConfig } from '../module-config';
import { NAMESPACE_CHANGE_PASSWORD_VIEW, i18nKeys } from './i18n';
import {
  IPasswordRequest,
  IChangePasswordViewProps,
} from './ChangePasswordModel';
import { changePassword } from './ChangePasswordService';
import { isEmpty, isEmptyObject } from '@design-system/infrastructure/utils';

class OriginChangePasswordView extends React.PureComponent<
  IChangePasswordViewProps &
    FormikProps<IPasswordRequest> &
    II18nFixedNamespaceContext
> {
  render() {
    const {
      t,
      isSubmitting,
      handleSubmit,
      className,
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
    } = this.props;

    return (
      <Flex column auto className={className}>
        <Flex
          className="change-password-container"
          column
          auto
          alignSelf="center"
          justify="center"
        >
          <form onSubmit={handleSubmit}>
            <Flex column className="change-password-row">
              <Label>{t(i18nKeys.currentPassword)}</Label>
              <InputGroup
                type="password"
                name="currentPassword"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.currentPassword}
                fill
              />
              <div className="password-error">{errors.currentPassword}</div>
            </Flex>
            <Flex column className="change-password-row">
              <Label>{t(i18nKeys.newPassword)}</Label>
              <InputGroup
                type="password"
                name="newPassword"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPassword}
                fill
              />
              <div className="password-error">{errors.newPassword}</div>
            </Flex>
            <Flex column className="change-password-row">
              <Label>{t(i18nKeys.confirmNewPassword)}</Label>
              <InputGroup
                type="password"
                name="confirmNewPassword"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.confirmNewPassword}
                fill
              />
              <div className="password-error">{errors.confirmNewPassword}</div>
            </Flex>
            <Button
              type="submit"
              fill
              large
              loading={isSubmitting}
              disabled={
                isEmpty(values.currentPassword) ||
                isEmpty(values.newPassword) ||
                isEmpty(values.confirmNewPassword) ||
                !isEmptyObject(errors)
              }
              intent="primary"
              text={t(i18nKeys.changePassword)}
            />
          </form>
        </Flex>
      </Flex>
    );
  }
}

export const ChangePasswordView = withFormik({
  mapPropsToValues: props => ({
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: '',
    auth: {},
    hashPassword: props.hashPassword,
  }),

  validate: (values: IPasswordRequest) => {
    const { t } = this.props;
    const errors: FormikErrors<IPasswordRequest> = {};

    const { currentPassword, newPassword, confirmNewPassword } = values;

    if (isEmpty(currentPassword, true)) {
      errors.currentPassword = t(i18nKeys.requireCurrentPassword);
    } else if (isEmpty(newPassword, true)) {
      errors.newPassword = t(i18nKeys.requireNewPassword);
    } else if (newPassword !== confirmNewPassword) {
      errors.confirmNewPassword = t(
        i18nKeys.requireCurrentPasswordNotSameNewPassword
      );
    }

    return errors;
  },

  handleSubmit: (
    values: IChangePasswordViewProps & IPasswordRequest,
    formikBag: FormikBag<IChangePasswordViewProps, IPasswordRequest>
  ) => {
    const { currentPassword, newPassword, hashPassword } = values;

    const accountId = formikBag.props.accountId;
    changePassword(currentPassword, newPassword, accountId, hashPassword)
      .then(() => formikBag.setSubmitting(false))
      .then(() => {
        const { onChange } = formikBag.props;
        onChange && onChange();
      })
      .catch(error => {
        formikBag.setSubmitting(false);
        throw error;
      });
  },
  displayName: 'ChangePasswordView',
})(
  withI18nContext(OriginChangePasswordView, {
    namespace: NAMESPACE_CHANGE_PASSWORD_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
