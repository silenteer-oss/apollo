import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const CHANGE_PASSWORD_MODULE_ID = 'ChangePasswordModule';

export const CHANGE_PASSWORD_VIEW_ID = 'ChangePasswordView';

export interface IModuleViews {
  [CHANGE_PASSWORD_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: CHANGE_PASSWORD_MODULE_ID,
    name: 'Change Password Module',
    publicAssetPath: `/module/password`,
    views: {
      ChangePasswordView: CHANGE_PASSWORD_VIEW_ID,
    },
  };
}
