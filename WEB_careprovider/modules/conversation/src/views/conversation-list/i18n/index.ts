import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './conversation-list.json';

export const NAMESPACE_CONVERSATION_LIST = 'conversation-list';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
