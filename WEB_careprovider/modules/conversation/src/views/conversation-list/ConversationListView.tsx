import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { unique, isNotEmpty } from '@design-system/infrastructure/utils';
import { InputGroup } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import {
  IConversationLastRead,
  getConversations,
  getConversationsLastRead,
  sendConversationLastRead,
} from '../../services';
import { IConversationContext, withConversationContext } from '../../context';
import {
  IConversationListViewProps,
  IConversationListViewState,
} from './models';
import { ConversationItem, IConversationItem } from './components';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_CONVERSATION_LIST, i18nKeys } from './i18n';

class OriginConversationListView extends React.PureComponent<
  IConversationListViewProps &
    IConversationContext &
    II18nFixedNamespaceContext,
  IConversationListViewState
> {
  state: IConversationListViewState = {
    conversations: [],
    patientProfileMap: {},
    searchPhase: '',
  };

  private _chatSubscription: Subscription;

  async componentDidMount() {
    const { getPatientProfiles, chat$, selectedPatientId } = this.props;

    const conversationsResponse = await getConversations();
    const conversationsLastRead = await getConversationsLastRead();
    const { patientProfileMap } = await getPatientProfiles(
      unique(conversationsResponse.map(item => item.patientId))
    );

    const conversations = conversationsResponse.map(item => {
      const { fullName, dob, gender } = patientProfileMap[item.patientId];
      const {
        id: conversationId,
        careProviderId,
        careProviderUserIds,
        patientId,
        lastMessageTime,
        isPaired,
      } = item;

      const conversationLastRead: IConversationLastRead = conversationsLastRead.find(
        item => item.conversationId === conversationId
      );

      return {
        conversationId,
        careProviderId,
        careProviderUserIds,
        isPaired,
        patientInfo: {
          id: patientId,
          name: fullName,
          dob,
          gender,
        },
        lastMessageTime,
        conversationLastRead: conversationLastRead
          ? conversationLastRead
          : { lastReadTime: 0, userId: undefined },
      };
    });

    if (selectedPatientId) {
      const selectedConversation = conversations.find(
        item => item.patientInfo.id === selectedPatientId
      );
      selectedConversation && this.selectConversation(selectedConversation);
    }

    this.setState({
      conversations,
      patientProfileMap,
    });

    this._chatSubscription = chat$.subscribe(message => {
      if (message) {
        const { selectedConversation } = this.props;
        if (
          selectedConversation &&
          selectedConversation.conversationId === message.conversationId
        ) {
          const payload = {
            conversationId: message.conversationId,
            lastReadTime: message.sendTime,
          };
          sendConversationLastRead(payload);

          return this.updateConversationLastMessageTime(
            message.conversationId,
            message.sendTime,
            message.sendTime
          );
        } else {
          return this.updateConversationLastMessageTime(
            message.conversationId,
            message.sendTime
          );
        }
      }
    });
  }

  componentWillUnmount() {
    this._chatSubscription && this._chatSubscription.unsubscribe();
  }

  render() {
    const {
      className,
      selectedConversation,
      unreadMessageConversationIdMap,
      t,
    } = this.props;
    const { conversations, searchPhase } = this.state;

    return (
      <Flex column className={className}>
        <Flex className="sl-search-box">
          <InputGroup
            value={searchPhase}
            leftIcon="search"
            placeholder={t(i18nKeys.searchPlaceholder)}
            onChange={this.onSearchPatient}
          />
        </Flex>
        <Flex column className="sl-conversation-list">
          <Router>
            {conversations
              .filter(item =>
                item.patientInfo.name
                  .toLowerCase()
                  .includes(searchPhase.toLowerCase().trim())
              )
              .map(item => (
                <ConversationItem
                  key={item.conversationId}
                  conversationItem={item}
                  hasUnreadMessage={
                    !!unreadMessageConversationIdMap[item.conversationId]
                  }
                  isSelected={
                    selectedConversation &&
                    item.conversationId === selectedConversation.conversationId
                  }
                  onSelect={this.selectConversation}
                  onSearchMode={isNotEmpty(searchPhase, true)}
                />
              ))}
          </Router>
        </Flex>
      </Flex>
    );
  }

  onSearchPatient = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ searchPhase: event.currentTarget.value });
  };

  updateConversationLastMessageTime(
    conversationId: string,
    lastMessageTime: number,
    lastReadMessageTime: number = 0
  ) {
    if (!conversationId || !lastMessageTime) {
      return;
    }
    const { conversations } = this.state;
    const foundIndex = conversations.findIndex(
      item => item.conversationId === conversationId
    );
    if (foundIndex !== -1) {
      const conversation = { ...conversations[foundIndex] };
      conversation.lastMessageTime = lastMessageTime;
      if (lastReadMessageTime !== 0) {
        conversation.conversationLastRead = {
          ...conversation.conversationLastRead,
          lastReadTime: lastReadMessageTime,
        };
      }
      const updatedConversations = conversations.slice(0);
      updatedConversations.splice(foundIndex, 1);
      updatedConversations.unshift(conversation);

      this.setState({
        conversations: updatedConversations,
      });
    }
  }

  selectConversation = async (conversationItem: IConversationItem) => {
    this.setState({ searchPhase: '' });

    const { onSelectConversation, getPatientProfiles } = this.props;
    const payload = {
      conversationId: conversationItem.conversationId,
      lastReadTime: conversationItem.lastMessageTime,
    };

    let updatedConversationItem: IConversationItem = conversationItem;

    if (
      !conversationItem.conversationLastRead ||
      conversationItem.conversationLastRead.lastReadTime <
        conversationItem.lastMessageTime
    ) {
      const lastReadTimeResponse: IConversationLastRead = await sendConversationLastRead(
        payload
      );

      updatedConversationItem = {
        ...conversationItem,
        conversationLastRead: {
          userId: lastReadTimeResponse.userId,
          lastReadTime: lastReadTimeResponse.lastReadTime,
        },
      };
      this.updateConversationLastMessageTime(
        conversationItem.conversationId,
        lastReadTimeResponse.lastReadTime,
        lastReadTimeResponse.lastReadTime
      );
    }

    const { patientProfileMap } = await getPatientProfiles([
      updatedConversationItem.patientInfo.id,
    ]);
    const patientProfile =
      patientProfileMap[updatedConversationItem.patientInfo.id];

    return onSelectConversation(updatedConversationItem, patientProfile);
  };
}

export const ConversationListView = withTheme(
  withConversationContext(
    withI18nContext(OriginConversationListView, {
      namespace: NAMESPACE_CONVERSATION_LIST,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
