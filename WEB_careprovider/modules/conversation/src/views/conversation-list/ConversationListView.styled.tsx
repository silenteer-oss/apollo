import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IConversationListViewProps } from './models';
import { ConversationListView as OriginConversationListView } from './ConversationListView';

export const ConversationListView = styled(OriginConversationListView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-ConversationListView', className),
  })
)<IConversationListViewProps>`
  ${props => {
    const { space } = props.theme;
    return `
      & {
        height: 100%;

        .sl-search-box {
          padding-bottom: ${space.s};
          > .bp3-input-group {
            width: 100%;
            margin-left: ${space.s};
            margin-right: ${space.s};
            > .bp3-input {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
            > .bp3-icon {
              margin: 12px 6px 12px 12px;
            }
          }
        }

        .sl-conversation-list {
          flex: 1;
          overflow-x: auto;
        }
      }
    `;
  }}
`;
