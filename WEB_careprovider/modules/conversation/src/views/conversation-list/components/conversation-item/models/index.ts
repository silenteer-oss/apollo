import { ICareProviderTheme } from '@careprovider/theme';
import { Gender } from '@careprovider/services/biz';

export interface IConversationItemProps {
  className?: string;
  theme?: ICareProviderTheme;
  conversationItem: IConversationItem;
  hasUnreadMessage: boolean;
  isSelected: boolean;
  onSelect: (conversation: IConversationItem) => void;
  onSearchMode: boolean;
}

export interface IConversationItem {
  conversationId: string;
  careProviderId: string;
  careProviderUserIds: string[];
  isPaired?: boolean;
  patientInfo: {
    id: string;
    name: string;
    dob: number;
    gender: Gender;
    avatarUrl?: string;
    avatarBackgroundColor?: string;
  };
  lastMessageTime: number;
  conversationLastRead: {
    lastReadTime: number;
    userId: string;
  };
}
