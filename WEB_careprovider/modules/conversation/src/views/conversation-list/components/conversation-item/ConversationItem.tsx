import React from 'react';
import {
  toShortCase,
  toDateFormat,
  equalDate,
  getCssClass,
} from '@design-system/infrastructure/utils';
import { BodyTextS, Flex, Box } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { dateTimeFormat } from '@careprovider/services/util';
import { Gender } from '@careprovider/services/biz';
import { getModuleConfig } from '../../../../../module-config';
import { NAMESPACE_CONVERSATION_ITEM, i18nKeys } from './i18n';
import { IConversationItemProps } from './models';

class OriginConversationItem extends React.PureComponent<
  IConversationItemProps & II18nFixedNamespaceContext
> {
  render() {
    if (!this.props.conversationItem) {
      return null;
    }

    const {
      className,
      theme: { foreground, space },
      conversationItem: { patientInfo, lastMessageTime },
      hasUnreadMessage,
      isSelected,
      t,
      onSearchMode,
    } = this.props;

    let dateFormat: any = 'dd-MM-yyyy';
    let timeFormat: any = undefined;

    const lastReadInSameDay = equalDate(
      new Date(lastMessageTime),
      new Date(),
      true
    );
    const lastReadInSameYear =
      new Date(lastMessageTime).getFullYear() - new Date().getFullYear() === 0;

    if (lastReadInSameDay) {
      dateFormat = undefined;
      timeFormat = 'hh:mm';
    } else if (lastReadInSameYear) {
      dateFormat = 'dd-MM';
    }

    return (
      <Flex
        className={getCssClass(
          className,
          hasUnreadMessage ? 'has-unread' : '',
          isSelected ? 'is-selected' : ''
        )}
        onClick={this._selectConversation}
      >
        <Box className="sl-participant-avatar" mr={space.s}>
          {patientInfo.avatarUrl && <img src={patientInfo.avatarUrl} />}
          {!patientInfo.avatarUrl && (
            <BodyTextS>
              {patientInfo.name
                ? toShortCase(patientInfo.name)
                : t(i18nKeys.unknow)}
            </BodyTextS>
          )}
        </Box>
        <Flex align="center" grow={1}>
          <BodyTextS
            color={foreground['02']}
            fontWeight={hasUnreadMessage ? 'SemiBold' : 'Normal'}
          >
            <div>{patientInfo.name}</div>
            {onSearchMode && (
              <div>
                {dateTimeFormat(patientInfo.dob, 'L')} |{' '}
                {this.getGender(patientInfo.gender)}
              </div>
            )}
          </BodyTextS>
        </Flex>
        {lastMessageTime !== 0 && !onSearchMode && (
          <Flex className="sl-conversation-time" align="center" ml={space.xs}>
            <BodyTextS color={foreground['03']}>
              {toDateFormat(new Date(lastMessageTime), {
                dateFormat,
                timeFormat,
                showAMPM: true,
              })}
            </BodyTextS>
          </Flex>
        )}
      </Flex>
    );
  }

  getGender = (gender: Gender) => {
    const { t } = this.props;
    switch (gender) {
      case 'MALE':
        return t(i18nKeys.maleGender);
      case 'FEMALE':
        return t(i18nKeys.femaleGender);
      case 'OTHERS':
      default:
        return t(i18nKeys.otherGender);
    }
  };

  private _selectConversation = () => {
    const { onSelect, conversationItem } = this.props;

    if (onSelect) {
      onSelect(conversationItem);
    }
  };
}

export const ConversationItem = withTheme(
  withI18nContext(OriginConversationItem, {
    namespace: NAMESPACE_CONVERSATION_ITEM,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
