import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles/';
import { styled } from '@careprovider/theme';
import { IConversationItemProps } from './models';
import { ConversationItem as OriginConversationItem } from './ConversationItem';

export const ConversationItem = styled(OriginConversationItem).attrs(
  ({ className }) => ({
    className: getCssClass('sl-ConversationItem', className),
  })
)<IConversationItemProps>`
  ${props => {
    const { space, background, radius } = props.theme;
    return `
      & {
        padding: ${space.s} ${space.xs} ${space.s} ${space.s};
        border-bottom: 1px solid ${background['01']};
        cursor: pointer;

        &.has-unread:not(.is-selected) {
          background-color: ${background.white};
        }

        &.is-selected {
          background-color: ${background.second.lighter};
          border-right: 2px solid ${background.second.base};
        }

        .sl-participant-avatar {
          display: flex;
          flex-flow: column;
          justify-content: center;
          align-items: center;
          width: ${scaleSpacePx(8)};
          height: ${scaleSpacePx(8)};
          background-color: ${background['01']};
          border-radius: ${radius.circle};
          min-width: ${scaleSpacePx(8)};
        }

        .sl-conversation-time {
          min-width: ${scaleSpacePx(9)};
        }
      }
    `;
  }}
`;
