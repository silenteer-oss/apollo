import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './conversation-item.json';

export const NAMESPACE_CONVERSATION_ITEM = 'conversation-item';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
