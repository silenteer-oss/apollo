import { Observable } from 'rxjs';
import {
  IPatientProfile,
  IDecryptedChatMessage,
} from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { IConversationItem } from '../components';

export interface IConversationListViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  selectedPatientId?: string;
  selectedConversation: IConversationItem;
  unreadMessageConversationIdMap: { [id: string]: boolean };
  chat$: Observable<IDecryptedChatMessage>;
  onSelectConversation: (
    conversationItem: IConversationItem,
    selectedPatientProfile: IPatientProfile
  ) => void;
}

export interface IConversationListViewState {
  conversations: IConversationItem[];
  patientProfileMap: {
    [key: string]: IPatientProfile;
  };
  searchPhase: string;
}
