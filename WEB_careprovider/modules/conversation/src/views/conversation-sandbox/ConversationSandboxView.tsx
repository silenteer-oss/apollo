import React from 'react';
import { Observable, Subject, Subscription, ReplaySubject } from 'rxjs';
import {
  H1,
  H3,
  BodyTextM,
  Flex,
  ErrorState,
} from '@design-system/core/components';
import { Header } from '@careprovider/components';
import {
  IAppointmentChangeLog,
  IDecryptedChatMessage,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';
import {
  startCommonSocket$,
  stopCommonSocket,
} from '@careprovider/services/websocket';
import {
  ensureCareProviderDeviceReady,
  ensureSenderKeysReady,
  encryptMessage,
  decryptMessage,
} from '@careprovider/services/webworker';
import {
  IAppoinmentChangeLogViewProps,
  AppointmentChangeLogView,
} from '@careprovider/modules/appointment';
import { withTheme } from '@careprovider/theme';
import {
  listenChatSocket$,
  listenAppointmentChangeLogSocket$,
} from '../../services';
import { IConversationItem, ConversationListView } from '../conversation-list';
import { ChatBoxView } from '../chat-box';
import {
  IConversationSandboxViewProps,
  IConversationSandboxViewState,
} from './models';
import { IConversationContext, withConversationContext } from '../../context';
import { dateTimeFormat } from '@careprovider/services/util';
import { listenCommunicationPlanSocket$ } from '../../services/sockets/communication-plan';
import { withRouter, RouteComponentProps } from 'react-router-dom';

class OriginConversationSandboxView extends React.PureComponent<
  IConversationSandboxViewProps & IConversationContext & RouteComponentProps,
  IConversationSandboxViewState
> {
  private _chatReplaySubject: ReplaySubject<IDecryptedChatMessage>;
  private _appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  private _reconnectedSubject = new Subject<void>();
  private _commonSocketSubscription: Subscription;
  private _listenChatSocketSubscription: Subscription;
  private _communicationPlanMessageReplaySubject: ReplaySubject<
    IProcessedCommunicationPlanMessage
  >;
  private _listenCommunicationPlanMessageSubscription: Subscription;

  state: IConversationSandboxViewState = {
    selectedConversation: undefined,
    selectedPatientInfo: undefined,
  };

  constructor(
    props: IConversationSandboxViewProps &
      IConversationContext &
      RouteComponentProps
  ) {
    super(props);

    this._commonSocketSubscription = startCommonSocket$().subscribe(
      ({ status }) => {
        if (status === 'RECONNECTED') {
          this._reconnectedSubject.next();
        }
      }
    );

    this._chatReplaySubject = new ReplaySubject<IDecryptedChatMessage>();
    this._listenChatSocketSubscription = listenChatSocket$({
      ensureCareProviderDeviceReadyHandler: ensureCareProviderDeviceReady,
      ensureSenderKeysReadyHandler: ensureSenderKeysReady,
      decryptMessageHandler: decryptMessage,
    }).subscribe(this._chatReplaySubject);

    this._appointmentChangeLog$ = listenAppointmentChangeLogSocket$();

    this._communicationPlanMessageReplaySubject = new ReplaySubject<
      IProcessedCommunicationPlanMessage
    >();
    this._listenCommunicationPlanMessageSubscription = listenCommunicationPlanSocket$().subscribe(
      this._communicationPlanMessageReplaySubject
    );
  }

  componentWillUnmount() {
    stopCommonSocket();

    if (this._commonSocketSubscription) {
      this._commonSocketSubscription.unsubscribe();
      this._commonSocketSubscription = undefined;
    }
    if (this._reconnectedSubject) {
      this._reconnectedSubject.complete();
      this._reconnectedSubject = undefined;
    }
    if (this._chatReplaySubject) {
      this._chatReplaySubject.unsubscribe();
      this._chatReplaySubject = undefined;
    }
    if (this._listenChatSocketSubscription) {
      this._listenChatSocketSubscription.unsubscribe();
      this._listenChatSocketSubscription = undefined;
    }
    if (this._communicationPlanMessageReplaySubject) {
      this._communicationPlanMessageReplaySubject.unsubscribe();
      this._communicationPlanMessageReplaySubject = undefined;
    }
    if (this._listenCommunicationPlanMessageSubscription) {
      this._listenCommunicationPlanMessageSubscription.unsubscribe();
      this._listenCommunicationPlanMessageSubscription = undefined;
    }
  }

  async componentWillUpdate(nextProps, nextState) {
    if (nextState.selectedConversation) {
      const { selectedConversation } = nextState;
      const { getPatientProfiles } = this.props;
      const { patientProfileMap } = await getPatientProfiles([
        selectedConversation.patientInfo.id,
      ]);
      const patientProfile =
        patientProfileMap[selectedConversation.patientInfo.id];
      this.setState({ selectedPatientInfo: patientProfile });
    }
  }

  render() {
    const {
      className,
      theme: { foreground },
      userProfile,
    } = this.props;
    const { selectedConversation, selectedPatientInfo } = this.state;

    return (
      <div className={className}>
        <div className="sl-container-left">
          <Header>
            <H1>Inbox</H1>
          </Header>
          <ConversationListView
            unreadMessageConversationIdMap={{}}
            chat$={this._chatReplaySubject}
            selectedConversation={selectedConversation}
            onSelectConversation={this.selectConversation}
          />
        </div>
        <Flex column auto className="sl-container-right">
          {!selectedConversation && (
            <ErrorState
              title="Chưa chọn bệnh nhân"
              content="Hãy chọn 1 bệnh nhân để bắt đầu trò chuyện"
            />
          )}
          {selectedConversation && (
            <>
              <Header>
                <div className="sl-patient-info">
                  <div>
                    <H3>
                      {selectedPatientInfo && selectedPatientInfo.fullName}
                    </H3>
                    <Flex>
                      <BodyTextM color={foreground['02']}>
                        {selectedPatientInfo &&
                          dateTimeFormat(selectedPatientInfo.dob, 'L')}
                      </BodyTextM>
                      <div className="sl-divider" />
                      <BodyTextM color={foreground['02']}>
                        {selectedPatientInfo && selectedPatientInfo.gender}
                      </BodyTextM>
                      <div className="sl-divider" />
                      <BodyTextM color={foreground['02']}>
                        {selectedPatientInfo && selectedPatientInfo.phone}
                      </BodyTextM>
                    </Flex>
                  </div>
                </div>
              </Header>

              <Flex grow={1}>
                <ChatBoxView
                  userProfile={userProfile}
                  conversation={selectedConversation}
                  chat$={this._chatReplaySubject}
                  appointmentChangeLog$={this._appointmentChangeLog$}
                  reconnected$={this._reconnectedSubject.asObservable()}
                  ensureCareProviderDeviceReady={ensureCareProviderDeviceReady}
                  ensureSenderKeysReady={ensureSenderKeysReady}
                  encryptMessage={encryptMessage}
                  decryptMessage={decryptMessage}
                  renderAppointmentChangeLog={this._renderAppointmentChangeLog}
                  communicationPlanMessage$={
                    this._communicationPlanMessageReplaySubject
                  }
                  history={this.props.history}
                />

                <div className="sl-conversation-info" />
              </Flex>
            </>
          )}
        </Flex>
      </div>
    );
  }

  selectConversation = (conversationItem: IConversationItem) => {
    this.setState({ selectedConversation: conversationItem });
  };

  private _renderAppointmentChangeLog = (
    props: IAppoinmentChangeLogViewProps
  ): React.ReactElement => {
    return <AppointmentChangeLogView {...props} />;
  };
}

export const ConversationSandboxView = withRouter(
  withTheme(withConversationContext(OriginConversationSandboxView))
);
