import { IPatientProfile, IEmployeeProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { IConversationItem } from '../../conversation-list';

export interface IConversationSandboxViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  userProfile: IEmployeeProfile;
}

export interface IConversationSandboxViewState {
  selectedConversation: IConversationItem;
  selectedPatientInfo: IPatientProfile;
}
