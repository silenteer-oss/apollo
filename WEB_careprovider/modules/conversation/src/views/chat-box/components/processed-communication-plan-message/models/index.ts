import { IProcessedCommunicationPlanMessage } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IProcessedCommunicationPlanMessageProps {
  className?: string;
  theme?: ICareProviderTheme;
  senderName: string;
  message: IProcessedCommunicationPlanMessage;
}
