import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IProcessedCommunicationPlanMessageProps } from './models';
import { ProcessedCommunicationPlanMessage as OriginProcessedCommunicationPlanMessage } from './ProcessedCommunicationPlanMessage';

export const ProcessedCommunicationPlanMessage = styled(
  OriginProcessedCommunicationPlanMessage
).attrs(({ className }) => ({
  className: getCssClass('sl-ProcessedCommunicationPlanMessage', className),
}))<IProcessedCommunicationPlanMessageProps>`
  ${props => {
    const { space, background, radius } = props.theme;

    return `
      & {
        display: flex;
        justify-content: flex-end;

        .sl-message-content {
          padding: ${space.xs} ${space.s};
          border-radius: ${radius['4px']};
          background-color: ${background.primary.lighter};
          white-space: pre-line;
        }
      }
    `;
  }}
`;
