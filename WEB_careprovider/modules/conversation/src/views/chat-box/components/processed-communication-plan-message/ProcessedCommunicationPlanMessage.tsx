import React from 'react';
import { BodyTextS } from '@design-system/core/components';
import { withTheme } from '@careprovider/theme';
import { IProcessedCommunicationPlanMessageProps } from './models';
import { MessageContent } from '../message-content';

class OriginProcessedCommunicationPlanMessage extends React.PureComponent<
  IProcessedCommunicationPlanMessageProps
> {
  render() {
    const {
      className,
      theme: { foreground },
      senderName,
      message,
    } = this.props;

    if (!message) {
      return null;
    }

    return (
      <div className={className}>
        <div className="sl-message-content">
          <BodyTextS color={foreground['02']} fontWeight="SemiBold">
            {senderName}
          </BodyTextS>
          <MessageContent message={message.message} />
        </div>
      </div>
    );
  }
}

export const ProcessedCommunicationPlanMessage = withTheme(
  OriginProcessedCommunicationPlanMessage
);
