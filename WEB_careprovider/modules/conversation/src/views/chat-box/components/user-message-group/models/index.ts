import { ReactElement } from 'react';
import { ICareProviderTheme } from '@careprovider/theme';
import { IAppoinmentChangeLogViewProps } from '@careprovider/modules/appointment';
import {
  IEmployeeProfile,
  IPatientProfile,
  IDecryptedChatMessage,
} from '@careprovider/services/biz';
import { IUserMessageGroup } from '../../../../../services';

export interface IUserMessageGroupProps {
  className?: string;
  theme?: ICareProviderTheme;
  dataSource: IUserMessageGroup;
  employeeProfileMap: {
    [key: string]: IEmployeeProfile;
  };
  patientProfileMap: {
    [key: string]: IPatientProfile;
  };
  renderAppointmentChangeLog: (
    props: IAppoinmentChangeLogViewProps
  ) => ReactElement;
  onResendEmployeeMessage: (message: IDecryptedChatMessage) => void;
}
