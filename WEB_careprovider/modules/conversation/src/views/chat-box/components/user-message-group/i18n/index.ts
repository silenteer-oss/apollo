import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './user-message-group.json';

export const NAMESPACE_USER_MESSAGE_GROUP = 'user-message-group';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
