import React from 'react';
import { toShortCase, getCssClass } from '@design-system/infrastructure/utils';
import { BodyTextS } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  IDecryptedChatMessage,
  IAppointmentChangeLog,
  IProcessedCommunicationPlanMessage,
  MessageType,
} from '@careprovider/services/biz';
import { getModuleConfig } from '../../../../../module-config';
import { getEmployeeAvatarUrl } from '../../../../services';
import { EmployeeMessage } from '../employee-message';
import { PatientMessage } from '../patient-message';
import { ProcessedCommunicationPlanMessage } from '../processed-communication-plan-message';
import { NAMESPACE_USER_MESSAGE_GROUP, i18nKeys } from './i18n';
import { IUserMessageGroupProps } from './models';

import defaultHospitalAvatarUrl from '@careprovider/assets/images/default-hospital-avatar.png';

class OriginUserMessageGroup extends React.PureComponent<
  IUserMessageGroupProps & II18nFixedNamespaceContext
> {
  render() {
    const { className, dataSource } = this.props;

    if (!dataSource || dataSource.messages.length === 0) {
      return null;
    }

    const isPatient =
      dataSource.messages[0].type === MessageType.PatientChatMessage;
    const isEmployee =
      dataSource.messages[0].type === MessageType.EmployeeChatMessage;
    const isAppointmentChangeLog =
      dataSource.messages[0].type === MessageType.AppointmentChangeLog;
    const isProcessedCommunicationPlanMessage =
      dataSource.messages[0].type ===
      MessageType.ProcessedCommunicationPlanMessage;

    let _className: string;
    if (isPatient) {
      _className = 'sl-is-patient';
    } else if (isEmployee) {
      _className = 'sl-is-employee';
    } else if (isProcessedCommunicationPlanMessage) {
      _className = 'sl-is-system';
    }

    return (
      <div className={getCssClass(className, _className)}>
        <div className="sl-user-message-container">
          {isPatient && this._renderPatientMessages()}
          {isEmployee && this._renderEmployeeMessages()}
          {isAppointmentChangeLog && this._renderAppointmentChangeLog()}
          {isProcessedCommunicationPlanMessage &&
            this._renderProcessedCommunicationPlanMessage()}
        </div>
      </div>
    );
  }

  private _renderPatientMessages = () => {
    const { dataSource, patientProfileMap, t } = this.props;
    const profile = patientProfileMap[dataSource.userId];

    return (
      <>
        <div className="sl-user-avatar">
          <div>
            <BodyTextS>
              {toShortCase(
                profile && profile.fullName
                  ? profile.fullName
                  : t(i18nKeys.unknow)
              )}
            </BodyTextS>
          </div>
        </div>
        <div className="sl-user-message-list">
          {dataSource.messages.map(message => (
            <PatientMessage
              key={message.id}
              message={message as IDecryptedChatMessage}
            />
          ))}
        </div>
      </>
    );
  };

  private _renderEmployeeMessages = () => {
    const {
      dataSource,
      employeeProfileMap,
      onResendEmployeeMessage,
      t,
    } = this.props;
    const profile = employeeProfileMap[dataSource.userId];

    return (
      <>
        <div className="sl-user-avatar">
          {profile && <img src={getEmployeeAvatarUrl(profile.type)} />}
          {!profile && (
            <div>
              <BodyTextS>{toShortCase(t(i18nKeys.unknow))}</BodyTextS>
            </div>
          )}
        </div>
        <div className="sl-user-message-list">
          {dataSource.messages.map(message => (
            <EmployeeMessage
              key={message.id}
              profile={profile}
              message={message as IDecryptedChatMessage}
              onResend={onResendEmployeeMessage}
            />
          ))}
        </div>
      </>
    );
  };

  private _renderAppointmentChangeLog = () => {
    const {
      renderAppointmentChangeLog: AppointmentChangeLog,
      dataSource,
      patientProfileMap,
    } = this.props;

    return (
      <div className="sl-user-message-list">
        {dataSource.messages.map(message => {
          const {
            id,
            changeMakerId,
            action,
            startDateTime,
            doctorIds,
            note,
            reason,
            cancelReason,
            department,
            hourAndMinuteSpecified,
          } = message as IAppointmentChangeLog;

          const patientProfileIfIsChangeMaker =
            patientProfileMap[changeMakerId];

          return (
            <AppointmentChangeLog
              key={id}
              id={id}
              changeMakerId={changeMakerId}
              action={action}
              startDateTime={startDateTime}
              doctorIds={doctorIds}
              note={note}
              reason={reason}
              cancelReason={cancelReason}
              departmentName={department}
              hourAndMinuteSpecified={hourAndMinuteSpecified}
              patientProfile={patientProfileIfIsChangeMaker}
            />
          );
        })}
      </div>
    );
  };

  private _renderProcessedCommunicationPlanMessage = () => {
    const { dataSource, t } = this.props;

    return (
      <>
        <div className="sl-user-avatar">
          <img src={defaultHospitalAvatarUrl} />
        </div>
        <div className="sl-user-message-list">
          {dataSource.messages.map(message => (
            <ProcessedCommunicationPlanMessage
              key={message.id}
              senderName={t(i18nKeys.hospital)}
              message={message as IProcessedCommunicationPlanMessage}
            />
          ))}
        </div>
      </>
    );
  };
}

export const UserMessageGroup = withI18nContext(OriginUserMessageGroup, {
  namespace: NAMESPACE_USER_MESSAGE_GROUP,
  publicAssetPath: getModuleConfig().publicAssetPath,
});
