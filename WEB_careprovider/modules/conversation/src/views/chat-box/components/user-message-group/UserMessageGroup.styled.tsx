import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IUserMessageGroupProps } from './models';
import { UserMessageGroup as OriginUserMessageGroup } from './UserMessageGroup';

export const UserMessageGroup = styled(OriginUserMessageGroup).attrs(
  ({ className }) => ({
    className: getCssClass('sl-UserMessageGroup', className),
  })
)<IUserMessageGroupProps>`
  ${props => {
    const { space, radius, background } = props.theme;

    return `
      & {
        display: flex;
        justify-content: center;

        &.sl-is-patient,
        &.sl-is-employee,
        &.sl-is-system {
          .sl-user-message-container {
            display: flex;
            max-width: 60%;
            width: 100%;

            .sl-user-avatar {
              display: flex;
              flex-flow: column;
              align-items: center;
              justify-content: flex-end;
              height: 100%;
              width: ${space.l};
            }
            .sl-user-message-list {
              flex: 1;
            }
          }
        }
        &.sl-is-patient {
          justify-content: flex-start;

          .sl-user-message-container {
            .sl-user-avatar {
              order: 0;
              margin-right: ${space.s};

              > div {
                display: flex;
                flex-flow: column;
                align-items: center;
                justify-content: center;
                width: ${space.l};
                height: ${space.l};
                border-radius: ${radius.circle};
                background-color: ${background['01']};
              }
            }
          }
        }
        &.sl-is-employee,
        &.sl-is-system {
          justify-content: flex-end;

          .sl-user-message-container {
            .sl-user-avatar {
              order: 2;
              margin-left: ${space.s};

              > img {
                width: ${space.l};
                height: ${space.l};
                border-radius: ${radius.circle};
                background-color: ${background['01']};
              }
            }
          }
        }

        .sl-user-message-list {
          > div + div {
            margin-top: ${space.xs};
          }
        }
      }
    `;
  }}
`;
