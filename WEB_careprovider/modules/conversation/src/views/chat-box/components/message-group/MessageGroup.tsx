import React from 'react';
import { equalDate } from '@design-system/infrastructure/utils';
import { BodyTextS } from '@design-system/core/components';
import { IDecryptedChatMessage } from '@careprovider/services/biz';
import { dateTimeFormat } from '@careprovider/services/util';
import { UserMessageGroup } from '../user-message-group';
import { IMessageGroupProps } from './models';

export class MessageGroup extends React.PureComponent<IMessageGroupProps> {
  render() {
    const {
      className,
      dataSource,
      patientProfileMap,
      employeeProfileMap,
      renderAppointmentChangeLog,
    } = this.props;

    if (!dataSource) {
      return null;
    }

    return (
      <div className={className}>
        <BodyTextS className="sl-message-group-datetime">
          {this._renderGroupDateTime()}
        </BodyTextS>
        {dataSource.userMessageGroups.map(userMessageGroup => {
          return (
            <UserMessageGroup
              key={userMessageGroup.userId}
              dataSource={userMessageGroup}
              patientProfileMap={patientProfileMap}
              employeeProfileMap={employeeProfileMap}
              renderAppointmentChangeLog={renderAppointmentChangeLog}
              onResendEmployeeMessage={this._resendEmployeeMessage}
            />
          );
        })}
      </div>
    );
  }

  private _renderGroupDateTime = (): string => {
    const _sendTime =
      this.props.dataSource.userMessageGroups[0] &&
      this.props.dataSource.userMessageGroups[0].messages[0] &&
      this.props.dataSource.userMessageGroups[0].messages[0].sendTime;
    const _datetime = new Date(_sendTime);

    if (
      equalDate(new Date(this.props.dataSource.groupDate), new Date(), true)
    ) {
      return dateTimeFormat(_datetime, 'hh:mm A');
    } else {
      return dateTimeFormat(_datetime, 'DD/MM/YYYY, hh:mm A');
    }
  };

  private _resendEmployeeMessage = (message: IDecryptedChatMessage): void => {
    const { dataSource, onResendEmployeeMessage } = this.props;

    onResendEmployeeMessage({
      messageGroup: dataSource,
      message,
    });
  };
}
