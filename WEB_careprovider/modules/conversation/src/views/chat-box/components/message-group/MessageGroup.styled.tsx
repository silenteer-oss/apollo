import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IMessageGroupProps } from './models';
import { MessageGroup as OriginMessageGroup } from './MessageGroup';

export const MessageGroup = styled(OriginMessageGroup).attrs(
  ({ className }) => ({
    className: getCssClass('sl-MessageGroup', className),
  })
)<IMessageGroupProps>`
  ${props => {
    const { foreground, space } = props.theme;

    return `
      & {
        padding: ${space.s} ${space.m};

        .sl-message-group-datetime {
          margin-bottom: ${space.s};
          color: ${foreground['03']};
          text-align: center;
        }

        .sl-UserMessageGroup {
          + .sl-UserMessageGroup {
            margin-top: ${space.s};
          }
        }
      }
    `;
  }}
`;
