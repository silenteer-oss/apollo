import { ReactElement } from 'react';
import { ICareProviderTheme } from '@careprovider/theme';
import { IAppoinmentChangeLogViewProps } from '@careprovider/modules/appointment';
import {
  IEmployeeProfile,
  IPatientProfile,
  IDecryptedChatMessage,
} from '@careprovider/services/biz';
import { IMessageGroup } from '../../../../../services';

export interface IMessageGroupProps {
  className?: string;
  theme?: ICareProviderTheme;
  dataSource: IMessageGroup;
  employeeProfileMap: {
    [key: string]: IEmployeeProfile;
  };
  patientProfileMap: {
    [key: string]: IPatientProfile;
  };
  renderAppointmentChangeLog: (
    props: IAppoinmentChangeLogViewProps
  ) => ReactElement;
  onResendEmployeeMessage: (params: {
    messageGroup: IMessageGroup;
    message: IDecryptedChatMessage;
  }) => void;
}
