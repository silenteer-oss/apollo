import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IEmployeeMessageProps } from './models';
import { EmployeeMessage as OriginEmployeeMessage } from './EmployeeMessage';

export const EmployeeMessage = styled(OriginEmployeeMessage).attrs(
  ({ className }) => ({
    className: getCssClass('sl-EmployeeMessage', className),
  })
)<IEmployeeMessageProps>`
  ${props => {
    const { space, background, radius } = props.theme;

    return `
      & {
        .sl-section-main {
          display: flex;
          justify-content: flex-end;

          &:hover {
            .sl-message-actions {
              display: flex !important;
            }
          }

          .sl-message-status {
            display: flex;
            flex-flow: column;
            justify-content: flex-end;
            align-items: center;
            margin-right: ${space.xs};

            .sl-message-actions {
              display: none;

              .bp3-button {
                min-width: initial;
                min-height: initial;
                padding-top: 0;
                padding-bottom: 0;

                > .bp3-button-text {
                  line-height: 1;
                }
              }
            }
          }

          .sl-message-content {
            padding: ${space.xs} ${space.s};
            border-radius: ${radius['4px']};
            background-color: ${background.primary.lighter};
            white-space: pre-line;
          }
        }

        .sl-rolling {
          display: inline-block;
          position: relative;
          width: ${scaleSpacePx(4)};
          height: ${scaleSpacePx(6)};
        }
        .sl-rolling div {
          box-sizing: border-box;
          display: block;
          position: absolute;
          width: ${scaleSpacePx(4)};
          height: ${scaleSpacePx(4)};
          margin: ${space.xxs};
          border: 2px solid ${background['01']};
          border-radius: 50%;
          animation: sl-rolling 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
          border-color: ${background['01']} transparent transparent transparent;
        }
        .sl-rolling div:nth-child(1) {
          animation-delay: -0.45s;
        }
        .sl-rolling div:nth-child(2) {
          animation-delay: -0.3s;
        }
        .sl-rolling div:nth-child(3) {
          animation-delay: -0.15s;
        }
        @keyframes sl-rolling {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(360deg);
          }
        }
      }
    `;
  }}
`;
