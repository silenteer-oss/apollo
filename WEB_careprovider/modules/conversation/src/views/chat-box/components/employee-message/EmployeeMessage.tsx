import React from 'react';
import { Button, Icon } from '@blueprintjs/core';
import { scaleSpace } from '@design-system/core/styles';
import { BodyTextS, Flex } from '@design-system/core/components';
import { TMessageStatus } from '@careprovider/services/biz';
import { withTheme } from '@careprovider/theme';
import { IEmployeeMessageProps } from './models';
import { MessageContent } from '../message-content';

class OriginEmployeeMessage extends React.PureComponent<IEmployeeMessageProps> {
  render() {
    const {
      className,
      theme: { foreground },
      profile,
      message,
    } = this.props;

    if (!message) {
      return null;
    }

    return (
      <div className={className}>
        <div className="sl-section-main">
          {this._renderStatus(message.status)}
          <div className="sl-message-content">
            <BodyTextS color={foreground['02']} fontWeight="SemiBold">
              {profile && profile.fullName ? profile.fullName : 'Unknown'}
            </BodyTextS>
            <MessageContent message={message.content} />
          </div>
        </div>
      </div>
    );
  }

  private _renderStatus = (status?: TMessageStatus): React.ReactElement => {
    const { onResend, message } = this.props;

    if (!status) {
      return null;
    } else if (status === 'PENDING') {
      return (
        <span className="sl-message-status">
          <div className="sl-rolling">
            <div />
            <div />
            <div />
            <div />
          </div>
        </span>
      );
    } else if (status === 'FAILED') {
      return (
        <span className="sl-message-status">
          <Flex align="flex-end">
            <div className="sl-message-actions">
              <Button
                intent="primary"
                minimal
                onClick={() => onResend(message)}
              >
                Resend
              </Button>
            </div>
            <Icon icon="info-sign" iconSize={scaleSpace(4)} intent="danger" />
          </Flex>
        </span>
      );
    }
  };
}

export const EmployeeMessage = withTheme(OriginEmployeeMessage);
