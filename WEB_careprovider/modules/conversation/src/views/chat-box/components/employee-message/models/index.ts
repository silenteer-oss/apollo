import {
  IEmployeeProfile,
  IDecryptedChatMessage,
} from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IEmployeeMessageProps {
  className?: string;
  theme?: ICareProviderTheme;
  profile: IEmployeeProfile;
  message: IDecryptedChatMessage;
  onResend: (message: IDecryptedChatMessage) => void;
}
