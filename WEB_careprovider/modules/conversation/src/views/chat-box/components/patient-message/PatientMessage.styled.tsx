import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IPatientMessageProps } from './models';
import { PatientMessage as OriginPatientMessage } from './PatientMessage';

export const PatientMessage = styled(OriginPatientMessage).attrs(
  ({ className }) => ({
    className: getCssClass('sl-PatientMessage', className),
  })
)<IPatientMessageProps>`
  ${props => {
    const { space, radius, background } = props.theme;

    return `
      & {
        display: flex;

        .sl-message-content {
          padding: ${space.xs} ${space.s};
          border-radius: ${radius['4px']};
          background-color: ${background['02']};
          white-space: pre-line;
        }
      }
    `;
  }}
`;
