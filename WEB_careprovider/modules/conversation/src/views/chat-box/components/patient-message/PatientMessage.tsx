import React from 'react';
import { BodyTextM } from '@design-system/core/components';
import { IPatientMessageProps } from './models';
import { MessageContent } from '../message-content/MessageContent';

export class PatientMessage extends React.PureComponent<IPatientMessageProps> {
  render() {
    const { className, message } = this.props;

    if (!message) {
      return null;
    }

    return (
      <div className={className}>
        <div className="sl-message-content">
          <MessageContent message={message.content} />
        </div>
      </div>
    );
  }
}
