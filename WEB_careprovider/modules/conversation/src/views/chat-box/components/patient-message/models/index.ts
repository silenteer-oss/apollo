import { IDecryptedChatMessage } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IPatientMessageProps {
  className?: string;
  theme?: ICareProviderTheme;
  message: IDecryptedChatMessage;
}
