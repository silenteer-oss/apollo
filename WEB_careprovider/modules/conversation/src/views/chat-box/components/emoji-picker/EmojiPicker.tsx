import React from 'react';
import { NimblePicker } from 'emoji-mart';
import { IEmojiPickerProps } from './models';

import data from 'emoji-mart/data/apple.json';
import 'emoji-mart/css/emoji-mart.css';

export class OriginEmojiPicker extends React.PureComponent<IEmojiPickerProps> {
  render() {
    const { className, onSelect } = this.props;

    return (
      <div className={className}>
        <NimblePicker
          set="apple"
          data={data}
          include={['search', 'recent', 'people']}
          sheetSize={32}
          onSelect={onSelect}
        />
      </div>
    );
  }
}
