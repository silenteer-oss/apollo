import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IEmojiPickerProps } from './models';
import { OriginEmojiPicker } from './EmojiPicker';

export const EmojiPicker = styled(OriginEmojiPicker).attrs(({ className }) => ({
  className: getCssClass('sl-EmojiPicker', className),
}))<IEmojiPickerProps>`
  /* stylelint-disable */
`;
