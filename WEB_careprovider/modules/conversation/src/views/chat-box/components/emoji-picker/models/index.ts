import { ICareProviderTheme } from '@careprovider/theme';
import { BaseEmoji as IEmoji } from 'emoji-mart';

export { IEmoji };

export interface IEmojiPickerProps {
  className?: string;
  theme?: ICareProviderTheme;
  onSelect: (emoji: IEmoji) => void;
}
