import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { MessageContent as OriginMessageContent } from './MessageContent';
import { IMessageContent } from './models';

export const MessageContent = styled(OriginMessageContent).attrs(
  ({ className }) => ({
    className: getCssClass('sl-MessageContent', className),
  })
)<IMessageContent>``;
