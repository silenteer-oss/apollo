import { ICareProviderTheme } from '@careprovider/theme';

export interface IMessageContent {
  className?: string;
  theme?: ICareProviderTheme;
  message: string;
}
