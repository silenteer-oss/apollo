import React from 'react';
import { BodyTextM, Link } from '@design-system/core/components';
import { IMessageContent } from './models';

const URL_REGEX = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;\(\))]*[-A-Z0-9+&@#\/%=~_|\(\)])/gi;

export class MessageContent extends React.PureComponent<IMessageContent> {
  render() {
    const { message } = this.props;

    if (!message) {
      return null;
    }

    return <>{this._urlify(message)}</>;
  }

  private _urlify = (message: string) => {
    const result = [];
    if (message.match(URL_REGEX)) {
      message.match(URL_REGEX).forEach(url => {
        const endIndex = message.indexOf(url);
        const leftPart = message.slice(0, endIndex);
        result.push(<BodyTextM as="span">{leftPart}</BodyTextM>);
        result.push(
          <Link href={url} target="_blank" rel="noopener noreferrer">
            {url}
          </Link>
        );
        message = message.slice(endIndex + url.length);
      });
    }
    if (message.length > 0) {
      result.push(<BodyTextM as="span">{message}</BodyTextM>);
    }

    return result;
  };
}
