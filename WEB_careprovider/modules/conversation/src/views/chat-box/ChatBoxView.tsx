import React from 'react';
import { Subscription } from 'rxjs';
import { TextArea } from '@blueprintjs/core';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  isEmpty,
  unique,
  doPromiseInDuration,
} from '@design-system/infrastructure/utils';
import {
  Flex,
  LoadingState,
  BodyTextM,
  Svg,
  Link,
} from '@design-system/core/components';
import { scaleSpace } from '@design-system/core/styles';
import { signalStorage } from '@infrastructure/signal/services/biz';
import { IDecryptedChatMessage, IMessage } from '@careprovider/services/biz';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import {
  IMessageGroup,
  createMessage,
  initMessageGroupCache,
  cleanMessageGroupCache,
  getCachedMessageGroups,
  getNewMessageGroups,
  addMessageToGroup,
  removeMessageFromGroup,
  sendMessage,
  updatePendingMessages,
} from '../../services';
import { IConversationContext, withConversationContext } from '../../context';
import { IConversationItem } from '../conversation-list/';
import { IChatBoxViewProps, IChatBoxViewState } from './models';
import { IEmoji, EmojiPicker, MessageGroup } from './components';
import { NAMESPACE_CHAT_BOX_VIEW, i18nKeys } from './i18n';

import DownArrowUrl from '@careprovider/assets/images/arrow-down.svg';
import EmojiPickerUrl from '@careprovider/assets/images/emoji-picker-trigger.svg';

class OriginChatBoxView extends React.PureComponent<
  IChatBoxViewProps & IConversationContext & II18nFixedNamespaceContext,
  IChatBoxViewState
> {
  private _defaultState: IChatBoxViewState = {
    isInitiating: false,
    isLoadingNewMessages: false,
    isLoadingCachedMessages: false,
    isSending: false,
    isLoadAllCachedMessages: false,
    isShowingUnreadMessagesAlert: false,
    isShowingEmojiPicker: false,
    totalUreadMessages: 0,
    employeeProfileMap: {},
    patientProfileMap: {},
    messageGroups: [],
    isFirstLoad: true,
  };
  private _messageListRef: HTMLDivElement;
  private _chatInputRef: HTMLTextAreaElement;
  private _reconnectedSubscription: Subscription;
  private _chatSubscription: Subscription;
  private _appointmentChangeLogSubscription: Subscription;
  private _communicationPlanMessageSubscription: Subscription;

  private get _totalViewableMessages(): number {
    // scaleSpace(13) is min height of AppointmentChangeLog and load more 5 items as buffer
    return Math.round(window.innerHeight / scaleSpace(13) + 5);
  }
  private get _totalLoadedMessages(): number {
    return this.state.messageGroups.reduce((result, messageGroup) => {
      result += messageGroup.userMessageGroups.reduce(
        (result1, userMessageGroup) => {
          result1 += userMessageGroup.messages.length;
          return result1;
        },
        0
      );
      return result;
    }, 0);
  }

  state: IChatBoxViewState = this._defaultState;

  async componentDidMount() {
    const { conversation } = this.props;

    if (conversation) {
      initMessageGroupCache(conversation.conversationId);

      await this._initDatasource();

      this._reconnectedSubscription = this.props.reconnected$.subscribe(() => {
        this._rereshDatasource();
      });

      this._chatSubscription = this.props.chat$.subscribe(message => {
        if (this.props.conversation.conversationId === message.conversationId) {
          if (this.state.isFirstLoad) {
            return;
          }
          this._addNewMessageToGroupFromSocket({
            conversation: this.props.conversation,
            message,
          });
        }
      });

      this._appointmentChangeLogSubscription = this.props.appointmentChangeLog$.subscribe(
        message => {
          if (this.props.conversation.patientInfo.id === message.patientId) {
            if (this.state.isFirstLoad) {
              return;
            }
            this._addNewMessageToGroupFromSocket({
              conversation: this.props.conversation,
              message,
            });
          }
        }
      );

      this._communicationPlanMessageSubscription = this.props.communicationPlanMessage$.subscribe(
        message => {
          if (this.props.conversation.patientInfo.id === message.patientId) {
            if (this.state.isFirstLoad) {
              return;
            }
            this._addNewMessageToGroupFromSocket({
              conversation: this.props.conversation,
              message,
            });
          }
        }
      );
    }
    this.setState({ isFirstLoad: false });
  }

  async componentDidUpdate(
    prevProps: IChatBoxViewProps & IConversationContext
  ) {
    const { conversation, getEmployeeProfiles } = this.props;

    if (prevProps.conversation !== conversation) {
      cleanMessageGroupCache(prevProps.conversation.conversationId);

      if (conversation) {
        this.setState(
          { ...this._defaultState, isFirstLoad: false },
          async () => {
            initMessageGroupCache(conversation.conversationId);

            await this._initDatasource();
          }
        );
      } else {
        this.setState({ ...this._defaultState, isFirstLoad: false });
      }
    }
    if (prevProps.getEmployeeProfiles !== getEmployeeProfiles) {
      const { employeeProfileMap } = await getEmployeeProfiles();
      this.setState({ employeeProfileMap });
    }
  }

  componentWillUnmount() {
    const { conversation } = this.props;

    if (conversation) {
      cleanMessageGroupCache(conversation.conversationId);
    }
    if (this._reconnectedSubscription) {
      this._reconnectedSubscription.unsubscribe();
      this._reconnectedSubscription = null;
    }
    if (this._chatSubscription) {
      this._chatSubscription.unsubscribe();
      this._chatSubscription = null;
    }
    if (this._appointmentChangeLogSubscription) {
      this._appointmentChangeLogSubscription.unsubscribe();
      this._appointmentChangeLogSubscription = null;
    }

    if (this._communicationPlanMessageSubscription) {
      this._communicationPlanMessageSubscription.unsubscribe();
      this._communicationPlanMessageSubscription = null;
    }
  }

  render() {
    const {
      className,
      conversation,
      renderAppointmentChangeLog,
      t,
    } = this.props;
    const {
      isInitiating,
      isSending,
      isShowingUnreadMessagesAlert,
      isShowingEmojiPicker,
      totalUreadMessages,
      messageGroups,
      patientProfileMap,
      employeeProfileMap,
    } = this.state;

    if (!conversation) {
      return null;
    }

    return (
      <Flex className={className} column auto>
        {isInitiating && <LoadingState />}
        {!isInitiating && (
          <>
            {!conversation.isPaired && (
              <div className="sl-un-pair-alert" onClick={this._openParing}>
                <BodyTextM>Bệnh nhân này chưa hoàn tất việc đăng ký</BodyTextM>
                <Link>Bấm để quét mã</Link>
              </div>
            )}
            {isShowingUnreadMessagesAlert && (
              <div
                className="sl-unread-messages-alert"
                onClick={this._scrollToMessageListBottom}
              >
                <BodyTextM>{totalUreadMessages} tin nhắn mới</BodyTextM>
                <Svg src={DownArrowUrl} />
              </div>
            )}
            <div className="sl-messages" ref={this._setMessageListRef}>
              {messageGroups.map(messageGroup => (
                <MessageGroup
                  key={messageGroup.id}
                  dataSource={messageGroup}
                  patientProfileMap={patientProfileMap}
                  employeeProfileMap={employeeProfileMap}
                  renderAppointmentChangeLog={renderAppointmentChangeLog}
                  onResendEmployeeMessage={this.resendMessage}
                />
              ))}
            </div>
            <div className="sl-chatbox">
              <TextArea
                fill
                placeholder={t(i18nKeys.replyMessagePlaceHolder)}
                maxLength={280}
                disabled={isSending || !conversation}
                inputRef={this._setChatInputRef}
              />
              <img
                className="sl-emoji-picker-trigger"
                src={EmojiPickerUrl}
                onClick={this.toggleEmojiPicker}
              />
              <EmojiPicker
                className={isShowingEmojiPicker ? 'is-active' : ''}
                onSelect={this.selectEmoji}
              />
            </div>
          </>
        )}
      </Flex>
    );
  }

  sendMessage = async () => {
    const { conversation, userProfile } = this.props;

    if (
      !conversation ||
      !this._chatInputRef ||
      isEmpty(this._chatInputRef.value, true)
    ) {
      return;
    }

    this.setState({ isSending: true }, async () => {
      const message = await createMessage({
        conversationId: conversation.conversationId,
        userId: userProfile.id,
        message: this._chatInputRef.value.trim(),
      });

      const messageGroup = addMessageToGroup({
        conversationId: conversation.conversationId,
        message,
      });

      this.setState(
        prevState => ({
          isSending: false,
          messageGroups: this._uniqueMessageGroup({
            messageGroups: [messageGroup, ...prevState.messageGroups],
          }),
        }),
        async () => {
          this._chatInputRef.value = '';
          this._chatInputRef.focus();

          await this._sendMessage({
            messageGroup,
            message,
          });
        }
      );
    });
  };

  resendMessage = async (params: {
    messageGroup: IMessageGroup;
    message: IDecryptedChatMessage;
  }): Promise<void> => {
    const { messageGroup, message } = params;

    await this._sendMessage({
      messageGroup,
      message,
    });
  };

  toggleEmojiPicker = (): void => {
    this.setState(prevState => ({
      isShowingEmojiPicker: !prevState.isShowingEmojiPicker,
    }));
  };

  selectEmoji = (emoji: IEmoji): void => {
    this._chatInputRef.value += emoji.native;
    this._chatInputRef.focus();
    this.toggleEmojiPicker();
  };

  private _openParing = () => {
    const {
      history,
      conversation: {
        patientInfo: { id },
      },
    } = this.props;
    history.push(`/app/patient/${id}`);
  };

  private _initDatasource = async () => {
    this.setState({ isInitiating: true }, () => {
      doPromiseInDuration(
        new Promise<void>(async resolve => {
          const {
            conversation,
            ensureCareProviderDeviceReady,
            ensureSenderKeysReady,
            getEmployeeProfiles,
            getPatientProfiles,
          } = this.props;

          await ensureCareProviderDeviceReady();
          const {
            senderId,
            senderDeviceId,
          } = await signalStorage.loadSenderDevice();

          await ensureSenderKeysReady({
            conversationId: conversation.conversationId,
            entityId: conversation.careProviderId,
            senderId,
            senderDeviceId,
          });

          const { patientProfileMap } = await getPatientProfiles([
            conversation.patientInfo.id,
          ]);
          const { employeeProfileMap } = await getEmployeeProfiles();

          const messageGroups = await getCachedMessageGroups({
            conversationId: conversation.conversationId,
            patientId: conversation.patientInfo.id,
            offset: this._totalLoadedMessages,
            limit: this._totalViewableMessages,
          });

          resolve(
            this.setState(
              {
                isInitiating: false,
                isLoadingNewMessages: true,
                messageGroups,
                employeeProfileMap,
                patientProfileMap,
              },
              () =>
                Promise.all([
                  this._loadNewMessageGroup(),
                  this._updateMessageStatus(),
                ])
            )
          );
        })
      );
    });
  };

  private _rereshDatasource = async () => {
    const { conversation } = this.props;

    cleanMessageGroupCache(conversation.conversationId);

    const messageGroups = await getCachedMessageGroups({
      conversationId: conversation.conversationId,
      patientId: conversation.patientInfo.id,
      offset: 0,
      limit: Math.max(this._totalViewableMessages, this._totalLoadedMessages),
    });

    this.setState(
      {
        messageGroups,
      },
      () =>
        Promise.all([this._loadNewMessageGroup(), this._updateMessageStatus()])
    );
  };

  private _loadNewMessageGroup = async (): Promise<void> => {
    const { conversation, decryptMessage, ensureSenderKeysReady } = this.props;

    return this.setState({ isLoadingNewMessages: true }, () => {
      doPromiseInDuration(
        new Promise<void>(async resolve => {
          const messageGroups = await getNewMessageGroups({
            conversationId: conversation.conversationId,
            careProviderId: conversation.careProviderId,
            patientId: conversation.patientInfo.id,
            ensureSenderKeysReadyHandler: ensureSenderKeysReady,
            decryptMessageHandler: decryptMessage,
          });

          resolve(
            this.setState(prevState => ({
              isLoadingNewMessages: false,
              messageGroups: this._uniqueMessageGroup({
                messageGroups: [...messageGroups, ...prevState.messageGroups],
              }),
            }))
          );
        })
      );
    });
  };

  private _updateMessageStatus = async (): Promise<void> => {
    const { conversation } = this.props;

    doPromiseInDuration(
      new Promise<void>(async resolve => {
        const updatedGroups = await updatePendingMessages(
          conversation.conversationId
        );
        if (updatedGroups) {
          resolve(
            this.setState(prevState => ({
              messageGroups: this._uniqueMessageGroup({
                messageGroups: [...updatedGroups, ...prevState.messageGroups],
              }),
            }))
          );
        }
      })
    );
  };

  private _addNewMessageToGroupFromSocket = (params: {
    conversation: IConversationItem;
    message: IMessage;
  }): void => {
    const { conversation, message } = params;
    let totalUreadMessages = this.state.totalUreadMessages;
    let isShowingUnreadMessagesAlert = this.state.isShowingUnreadMessagesAlert;
    const { scrollTop } = this._messageListRef;

    if (scrollTop > 0) {
      isShowingUnreadMessagesAlert = true;
      totalUreadMessages++;
    }

    const messageGroup = addMessageToGroup({
      conversationId: conversation.conversationId,
      message,
    });

    this.setState(prevState => {
      return {
        isShowingUnreadMessagesAlert,
        totalUreadMessages,
        messageGroups: this._uniqueMessageGroup({
          messageGroups: [messageGroup, ...prevState.messageGroups],
        }),
      };
    });
  };

  private _sendMessage = async (params: {
    messageGroup: IMessageGroup;
    message: IDecryptedChatMessage;
  }): Promise<void> => {
    const { conversation, encryptMessage } = this.props;
    const { messageGroup, message } = params;

    await sendMessage({
      conversationId: conversation.conversationId,
      message,
      encryptMessageHandler: encryptMessage,
    }).then(updatedMessage => {
      let _messageGroup = removeMessageFromGroup({
        conversationId: conversation.conversationId,
        groupDate: messageGroup.groupDate,
        groupTime: messageGroup.groupTime,
        message,
      });

      let messageGroups = this._removeMessageGroup({
        source: this.state.messageGroups,
        messageGroup,
      });

      _messageGroup = addMessageToGroup({
        conversationId: conversation.conversationId,
        message: updatedMessage,
      });

      messageGroups = this._uniqueMessageGroup({
        messageGroups: [_messageGroup, ...messageGroups],
      });

      this.setState({ messageGroups, isSending: false });
    });
  };

  private _removeMessageGroup = (prams: {
    source: IMessageGroup[];
    messageGroup: IMessageGroup;
  }): IMessageGroup[] => {
    const { source, messageGroup } = prams;

    return source.filter(item => {
      return item.id !== messageGroup.id;
    });
  };

  private _uniqueMessageGroup = (params: {
    messageGroups: IMessageGroup[];
    doOverride?: boolean;
  }): IMessageGroup[] => {
    const { messageGroups, doOverride } = params;
    return unique(messageGroups, { getKey: item => item.id, doOverride });
  };

  private _setMessageListRef = (ref: HTMLDivElement) => {
    if (ref && this._messageListRef !== ref) {
      this._messageListRef = ref;
      this._messageListRef.addEventListener(
        'scroll',
        this._handleMessageListScroll
      );
      // Use onWhell at here to resolve issue in Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1042151
      this._messageListRef.addEventListener(
        'wheel',
        event => {
          if (event.deltaY) {
            event.preventDefault();
            this._messageListRef.scrollTop -= 20 * (event.deltaY < 0 ? -1 : 1);
          }
        },
        { passive: false }
      );
    }
  };

  private _handleMessageListScroll = (): any => {
    const {
      isLoadingCachedMessages,
      isLoadAllCachedMessages,
      isShowingUnreadMessagesAlert,
      totalUreadMessages,
    } = this.state;
    const { scrollTop, scrollHeight, clientHeight } = this._messageListRef;

    if (
      scrollTop === scrollHeight - clientHeight &&
      !isLoadingCachedMessages &&
      !isLoadAllCachedMessages
    ) {
      this.setState({ isLoadingCachedMessages: true }, async () => {
        const { conversation } = this.props;
        const messageGroups = await getCachedMessageGroups({
          conversationId: conversation.conversationId,
          patientId: conversation.patientInfo.id,
          offset: this._totalLoadedMessages,
          limit: this._totalViewableMessages,
        });

        const _currentScrollTop = scrollTop;

        this.setState(
          prevState => ({
            isLoadingCachedMessages: false,
            isLoadAllCachedMessages: messageGroups.length === 0,
            messageGroups: this._uniqueMessageGroup({
              messageGroups: [...prevState.messageGroups, ...messageGroups],
              doOverride: true,
            }),
          }),
          () => {
            this._messageListRef.scrollTo({
              top: _currentScrollTop,
            });
          }
        );
      });
    }

    if (!isShowingUnreadMessagesAlert && totalUreadMessages > 0) {
      if (scrollTop < scrollHeight - clientHeight) {
        this.setState({ isShowingUnreadMessagesAlert: true });
      }
    } else {
      if (scrollTop === 0) {
        this.setState({
          isShowingUnreadMessagesAlert: false,
          totalUreadMessages: 0,
        });
      }
    }
  };

  private _scrollToMessageListBottom = (): void => {
    this.setState(
      { isShowingUnreadMessagesAlert: false, totalUreadMessages: 0 },
      () => {
        this._messageListRef.scrollTo({ top: 0 });
      }
    );
  };

  private _setChatInputRef = (ref: HTMLTextAreaElement) => {
    if (ref && this._chatInputRef !== ref) {
      this._chatInputRef = ref;
      this._chatInputRef.onkeypress = this._handleEnterPress;
    }
  };

  private _handleEnterPress = async (event: KeyboardEvent) => {
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      event.stopPropagation();
      await this.sendMessage();
    }
  };
}

export const ChatBoxView = withTheme(
  withConversationContext(
    withI18nContext(OriginChatBoxView, {
      namespace: NAMESPACE_CHAT_BOX_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
