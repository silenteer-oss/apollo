import { ReactElement } from 'react';
import { Observable } from 'rxjs';
import {
  IAppointmentChangeLog,
  IEmployeeProfile,
  IPatientProfile,
  IDecryptedChatMessage,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';
import { IAppoinmentChangeLogViewProps } from '@careprovider/modules/appointment';
import { ICareProviderTheme } from '@careprovider/theme';
import { IMessageGroup } from '../../../services';
import { IConversationItem } from '../../conversation-list';
import { History } from 'history';

export interface IChatBoxViewProps {
  className?: string;
  theme: ICareProviderTheme;
  userProfile: IEmployeeProfile;
  conversation: IConversationItem;
  chat$: Observable<IDecryptedChatMessage>;
  appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  communicationPlanMessage$: Observable<IProcessedCommunicationPlanMessage>;
  reconnected$: Observable<void>;
  ensureCareProviderDeviceReady: () => Promise<void>;
  ensureSenderKeysReady: (params: {
    conversationId: string;
    entityId: string; // entityId can be careProviderId or patientId
    senderId: string;
    senderDeviceId: string;
  }) => Promise<void>;
  encryptMessage: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    message: string;
  }) => Promise<{
    senderDevice: { senderId: string; senderDeviceId: string };
    message: string;
  }>;
  decryptMessage: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    content: string;
  }) => Promise<{ message: string }>;
  renderAppointmentChangeLog: (
    props: IAppoinmentChangeLogViewProps
  ) => ReactElement;
  history: History<any>;
}

export interface IChatBoxViewState {
  isInitiating: boolean;
  isLoadingNewMessages: boolean;
  isLoadingCachedMessages: boolean;
  isSending: boolean;
  isLoadAllCachedMessages: boolean;
  isShowingUnreadMessagesAlert: boolean;
  isShowingEmojiPicker: boolean;
  totalUreadMessages: number;
  employeeProfileMap: {
    [key: string]: IEmployeeProfile;
  };
  patientProfileMap: {
    [key: string]: IPatientProfile;
  };
  messageGroups: IMessageGroup[];
  isFirstLoad: boolean;
}

export type TChatMessage = IDecryptedChatMessage | IAppointmentChangeLog;
