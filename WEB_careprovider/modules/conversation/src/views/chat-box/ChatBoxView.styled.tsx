import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IChatBoxViewProps } from './models';
import { ChatBoxView as OriginChatBoxView } from './ChatBoxView';

export const ChatBoxView = styled(OriginChatBoxView).attrs(({ className }) => ({
  className: getCssClass('sl-ChatBoxView', className),
}))<IChatBoxViewProps>`
  ${props => {
    const { background, foreground, space } = props.theme;
    const chatboxHeight = scaleSpace(10);
    const chatboxPaddingX = scaleSpace(2) * 2;

    return `
    & {
      position: relative;
      display: flex;
      min-width: ${scaleSpacePx(80)};

      .sl-unread-messages-alert {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        z-index: 1;
        display: flex;
        align-items: center;
        height: ${scaleSpacePx(8)};
        padding: 0 ${space.m};
        background: ${background.primary.base};
        cursor: pointer;

        > p {
          flex: 1;
          margin-right: ${space.xs};
          color: ${foreground.white};
        }
      }

      .sl-un-pair-alert {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        z-index: 1;
        display: flex;
        align-items: center;
        height: ${scaleSpacePx(8)};
        padding: 0 ${space.m};
        background: ${background.error.base};
        cursor: pointer;

        > a {
          color: ${foreground.white};
        }

        > p {
          flex: 1;
          margin-right: ${space.xs};
          color: ${foreground.white};
        }
      }

      /* Use transform: scaleY(-1) instead of flex-direction: column-reverse at here to resolve issue in Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1042151 */
      .sl-messages {
        height: calc(100vh - ${chatboxHeight}px - ${chatboxPaddingX}px);
        overflow: auto;
        transform: scaleY(-1);

        .sl-MessageGroup {
          transform: scaleY(-1);

          &:nth-last-child(1) {
            padding-top: ${scaleSpacePx(13)};
          }
        }
      }

      .sl-chatbox {
        position: relative;
        display: flex;
        align-items: center;
        height: ${chatboxHeight + chatboxPaddingX}px;
        border-width: 1px;
        border-style: solid;
        border-right: none;
        border-left: none;
        border-bottom: none;
        border-top-color: ${background['01']};

        textarea {
          height: ${chatboxHeight}px;
          border: none !important;
          border-radius: initial;
          border-color: transparent  !important;
          resize: none;
        }

        .sl-emoji-picker-trigger {
          margin-right: ${space.s};
          cursor: pointer;
        }

        .sl-EmojiPicker {
          position: absolute;
          display: none;
          right: 0;
          bottom: ${space.xl};

          &.is-active {
            display: initial;
          }
        }
      }
    }
  `;
  }}
`;
