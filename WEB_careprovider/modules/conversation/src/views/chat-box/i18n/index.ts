import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './chat-box-view.json';

export const NAMESPACE_CHAT_BOX_VIEW = 'chat-box-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
