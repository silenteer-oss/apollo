import { Observable, from } from 'rxjs';
import { mergeMap, filter } from 'rxjs/operators';
import {
  MessageType,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';
import { getCommunicationPlanMessage$ as wsGetCommunicationPlanMessage$ } from '@careprovider/services/websocket';
import { storeMessages } from '../../biz/';

export function listenCommunicationPlanSocket$(): Observable<
  IProcessedCommunicationPlanMessage
> {
  return wsGetCommunicationPlanMessage$().pipe(
    mergeMap(({ data }) => from(_storeCommunicationPlanMessage(data))),
    filter(data => !!data)
  );
}

// ---------------------------------------------------------------- //

async function _storeCommunicationPlanMessage(
  message: IProcessedCommunicationPlanMessage
): Promise<IProcessedCommunicationPlanMessage> {
  const result = {
    ...message,
    type: MessageType.ProcessedCommunicationPlanMessage,
  } as IProcessedCommunicationPlanMessage;

  await storeMessages([result]);

  return result;
}
