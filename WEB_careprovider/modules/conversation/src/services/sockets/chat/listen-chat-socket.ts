import { from, Observable } from 'rxjs';
import { mergeMap, filter } from 'rxjs/operators';
import { SentryService } from '@design-system/core/components';
import { signalStorage } from '@infrastructure/signal/services/biz';
import {
  IDecryptedChatMessage,
  IEncryptedChatMessage,
  MessageType,
} from '@careprovider/services/biz';
import { getChat$ as wsGetChat$ } from '@careprovider/services/websocket';
import { storeMessages } from '../../biz';

export function listenChatSocket$(params: {
  ensureCareProviderDeviceReadyHandler: () => Promise<void>;
  ensureSenderKeysReadyHandler: (params: {
    conversationId: string;
    entityId: string; // entityId can be careProviderId or patientId
    senderId: string;
    senderDeviceId: string;
  }) => Promise<void>;
  decryptMessageHandler: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    content: string;
  }) => Promise<{ message: string }>;
}): Observable<IDecryptedChatMessage> {
  const {
    ensureCareProviderDeviceReadyHandler,
    ensureSenderKeysReadyHandler,
    decryptMessageHandler,
  } = params;

  return wsGetChat$().pipe(
    mergeMap(({ data }) =>
      from(
        _decryptAndStoreMessage({
          encryptedChatMessage: data,
          ensureCareProviderDeviceReadyHandler,
          ensureSenderKeysReadyHandler,
          decryptMessageHandler,
        })
      )
    ),
    filter(data => !!data)
  );
}

// ---------------------------------------------------------------- //

async function _decryptAndStoreMessage(params: {
  encryptedChatMessage: IEncryptedChatMessage;
  ensureCareProviderDeviceReadyHandler: () => Promise<void>;
  ensureSenderKeysReadyHandler: (params: {
    conversationId: string;
    entityId: string; // entityId can be careProviderId or patientId
    senderId: string;
    senderDeviceId: string;
  }) => Promise<void>;
  decryptMessageHandler: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    content: string;
  }) => Promise<{ message: string }>;
}): Promise<IDecryptedChatMessage> {
  const {
    encryptedChatMessage,
    ensureCareProviderDeviceReadyHandler,
    ensureSenderKeysReadyHandler,
    decryptMessageHandler,
  } = params;
  const {
    id,
    conversationId,
    senderId,
    senderDeviceId,
    userId,
    sendTime,
    content,
  } = encryptedChatMessage;

  await ensureCareProviderDeviceReadyHandler();

  await ensureSenderKeysReadyHandler({
    conversationId,
    entityId: senderId,
    senderId,
    senderDeviceId,
  });

  const {
    senderId: currentSenderId,
    senderDeviceId: currentSenderDeviceId,
  } = await signalStorage.loadSenderDevice();

  if (
    senderId === currentSenderId &&
    senderDeviceId === currentSenderDeviceId
  ) {
    return undefined;
  }

  const type =
    senderId === currentSenderId
      ? MessageType.EmployeeChatMessage
      : MessageType.PatientChatMessage;

  let decryptedMessage: IDecryptedChatMessage;
  try {
    decryptedMessage = {
      id,
      conversationId,
      senderId,
      senderDeviceId,
      userId,
      sendTime,
      type,
      content: (
        await decryptMessageHandler({
          conversationId,
          senderId,
          senderDeviceId,
          content,
        })
      ).message,
    };
  } catch (error) {
    SentryService.captureException({
      data: {
        id,
        conversationId,
        senderId,
        senderDeviceId,
        userId,
      },
      error: error.message,
    });
    return undefined;
  }

  await storeMessages([decryptedMessage]);

  return decryptedMessage;
}
