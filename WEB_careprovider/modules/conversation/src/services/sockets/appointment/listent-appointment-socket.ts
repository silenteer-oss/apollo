import { Observable, from } from 'rxjs';
import { mergeMap, filter } from 'rxjs/operators';
import { MessageType, IAppointmentChangeLog } from '@careprovider/services/biz';
import { getAppointmentChangeLog$ as wsGetAppointmentChangeLog$ } from '@careprovider/services/websocket';
import { storeMessages } from '../../biz/';

export function listenAppointmentChangeLogSocket$(): Observable<
  IAppointmentChangeLog
> {
  return wsGetAppointmentChangeLog$().pipe(
    mergeMap(({ data }) => from(_storeAppointmentChangeLog(data))),
    filter(data => !!data)
  );
}

// ---------------------------------------------------------------- //

async function _storeAppointmentChangeLog(
  changeLog: IAppointmentChangeLog
): Promise<IAppointmentChangeLog> {
  const result = {
    ...changeLog,
    type: MessageType.AppointmentChangeLog,
  } as IAppointmentChangeLog;

  await storeMessages([result]);

  return result;
}
