import { UserType } from '@careprovider/services/biz';

import defaultDoctorAvatarUrl from '@careprovider/assets/images/default-doctor-avatar.png';
import defaultNurseAvatarUrl from '@careprovider/assets/images/default-nurse-avatar.png';
import defaultReceptionistAvatarUrl from '@careprovider/assets/images/default-receptionist-avatar.png';

export function getEmployeeAvatarUrl(role: UserType): string {
  switch (role) {
    case UserType.DOCTOR:
      return defaultDoctorAvatarUrl;
    case UserType.NURSE:
      return defaultNurseAvatarUrl;
    case UserType.RECEPTIONIST:
      return defaultReceptionistAvatarUrl;
    default:
      return undefined;
  }
}
