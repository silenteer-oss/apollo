import {
  ConversationLastReadResponse as IConversationLastRead,
  ConversationResponse as IConversation,
} from '@silenteer/hermes/resource/chat-app-resource/model';
import { IMessage } from '@careprovider/services/biz';

export { IConversationLastRead, IConversation };

export interface IMessageGroup {
  id: string;
  groupDate: number;
  groupTime: number;
  userMessageGroups: IUserMessageGroup[];
}

export interface IUserMessageGroup {
  userId?: string;
  messages: IMessage[];
}
