import { unique } from '@design-system/infrastructure/utils';
import { SentryService } from '@design-system/core/components';
import {
  IMessage,
  IDecryptedChatMessage,
  MessageType,
} from '@careprovider/services/biz';
import {
  getChatMessages as apiGetChatMessages,
  getAppointmentChangeLogs as apiGetAppointmentChangeLogs,
  getProcessedCommunicationPlanMessages as apiGetProcessedCommunicationPlanMessages,
  queryConversationMessagesById,
} from '../../../resources';
import { IMessageGroup } from '../../models';
import {
  loadMessages,
  storeMessages,
  loadLastChatMessageTime,
  loadLastAppointmentChangeLogTime,
  loadLastCommunicationPlanMessageProcessedTime,
  loadEmployeePendingMessages,
} from './manage-messages';
import {
  initMessageGroupCache as originInitMessageGroupCache,
  cleanMessageGroupCache as originCleanMessageGroupCache,
  addMessageToGroup as originAddMessageToGroup,
  removeMessageFromGroup as originRemoveMessageFromGroup,
  updateMessageInGroup as originUpdateMessageInGroup,
  findGroupAndUpdateMessage,
} from './manage-message-groups';

export function initMessageGroupCache(conversationId: string): void {
  return originInitMessageGroupCache(conversationId);
}

export function cleanMessageGroupCache(conversationId: string): void {
  return originCleanMessageGroupCache(conversationId);
}

export async function getNewMessageGroups(params: {
  conversationId: string;
  careProviderId: string;
  patientId: string;
  ensureSenderKeysReadyHandler(params: {
    conversationId: string;
    entityId: string; // entityId can be careProviderId or patientId
    senderId: string;
    senderDeviceId: string;
  }): Promise<void>;
  decryptMessageHandler: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    content: string;
  }) => Promise<{ message: string }>;
}): Promise<IMessageGroup[]> {
  // ---------------------- Chat Messages ---------------------- //

  const _decryptedChatMessages: IDecryptedChatMessage[] = [];
  const _ensuredSenderKeyMap: { [key: string]: boolean } = {};
  const lastChatMessageTime = await loadLastChatMessageTime(
    params.conversationId
  );

  const encryptedChatMessages = await apiGetChatMessages({
    conversationId: params.conversationId,
    lastChatMessageTime,
  });

  for (let i = 0; i < encryptedChatMessages.length; i++) {
    const {
      id,
      conversationId,
      senderId,
      senderDeviceId,
      userId,
      content,
      sendTime,
    } = encryptedChatMessages[i];
    const _cacheKey = `${conversationId}.${senderId}.${senderDeviceId}`;
    const type =
      senderId === params.careProviderId
        ? MessageType.EmployeeChatMessage
        : MessageType.PatientChatMessage;

    if (!_ensuredSenderKeyMap[_cacheKey]) {
      await params.ensureSenderKeysReadyHandler({
        conversationId,
        entityId: senderId,
        senderId,
        senderDeviceId,
      });

      _ensuredSenderKeyMap[_cacheKey] = true;
    }

    try {
      const decryptMessage = await params
        .decryptMessageHandler({
          conversationId,
          senderId,
          senderDeviceId,
          content,
        })
        .then(({ message }) => ({
          id,
          conversationId,
          senderId,
          senderDeviceId,
          userId,
          type,
          sendTime,
          content: message,
        }));

      _decryptedChatMessages.push(decryptMessage);
    } catch (error) {
      console.error(error);
      SentryService.captureException({
        data: {
          id,
          conversationId,
          senderId,
          senderDeviceId,
          userId,
        },
        error: error.message,
      });
    }
  }

  await storeMessages(_decryptedChatMessages);

  // ---------------------- Appointment Change Logs ---------------------- //

  const lastAppointmentChangeLogTime = await loadLastAppointmentChangeLogTime(
    params.patientId
  );

  const _appointmentChangeLogs = await apiGetAppointmentChangeLogs({
    patientId: params.patientId,
    lastAppointmentChangeLogTime,
  });

  await storeMessages(_appointmentChangeLogs);

  // ---------------------- Processed Communication Plan Messages ---------------------- //

  const lastProcessedTime = await loadLastCommunicationPlanMessageProcessedTime(
    params.patientId
  );

  let _processedCommunicationPlanMessages = await apiGetProcessedCommunicationPlanMessages(
    {
      patientId: params.patientId,
      lastProcessedTime,
    }
  );

  if (_processedCommunicationPlanMessages) {
    _processedCommunicationPlanMessages = _processedCommunicationPlanMessages.map(
      item => ({
        ...item,
        sendTime: item.processedDate || item.plannedProcessedDate,
      })
    );
  }

  await storeMessages(_processedCommunicationPlanMessages);

  // ---------------------------------------------------------------------------------- //

  return _sortMessageGroups(
    [
      ..._decryptedChatMessages,
      ..._appointmentChangeLogs,
      ..._processedCommunicationPlanMessages,
    ]
      .sort((message1, message2) => message1.sendTime - message2.sendTime)
      .map(message =>
        addMessageToGroup({
          conversationId: params.conversationId,
          message,
        })
      )
  );
}

export async function getCachedMessageGroups(params: {
  conversationId: string;
  patientId: string;
  offset: number;
  limit: number;
}): Promise<IMessageGroup[]> {
  const { conversationId, patientId, offset, limit } = params;

  return loadMessages({
    conversationId,
    patientId,
    offset,
    limit,
  })
    .then(messages =>
      messages.map(message =>
        addMessageToGroup({
          conversationId,
          message,
        })
      )
    )
    .then(messageGroups =>
      _sortMessageGroups(_uniqueMessageGroup(messageGroups))
    );
}

export async function updatePendingMessages(
  conversationId: string
): Promise<IMessageGroup[]> {
  const pendingMessages = await loadEmployeePendingMessages(conversationId);
  const pendingIds = pendingMessages.map(pending => pending.id);
  if (!pendingIds || pendingIds.length <= 0) return;

  const sentMessages = await queryConversationMessagesById(
    conversationId,
    pendingIds
  );
  const updatedIds = sentMessages.map(updated => updated.id);
  const failedMessages = pendingMessages
    .filter(pending => updatedIds.indexOf(pending.id) < 1)
    .map(message => ({
      oldMessage: message,
      newMessage: {
        ...message,
        status: 'FAILED',
      } as IMessage,
    }));

  const updatedMessages = sentMessages.map(updated => {
    const message = pendingMessages.find(
      m => m.id === updated.id
    ) as IDecryptedChatMessage;
    return {
      oldMessage: message,
      newMessage: {
        id: updated.id,
        conversationId: updated.conversationId,
        senderId: updated.senderId,
        senderDeviceId: updated.senderDeviceId,
        userId: updated.userId,
        content: message.content,
        sendTime: updated.sendTime,
        type: 'EMPLOYEE_CHAT_MESSAGE',
      } as IMessage,
    };
  });

  const groups = [...failedMessages, ...updatedMessages].map(message =>
    findGroupAndUpdateMessage({ conversationId, message })
  );
  return groups;
}

export function addMessageToGroup(params: {
  conversationId: string;
  message: IMessage;
}): IMessageGroup {
  return originAddMessageToGroup(params);
}

export function updateMessageInGroup(params: {
  conversationId: string;
  groupDate: number;
  groupTime: number;
  message: IMessage;
}): IMessageGroup {
  return originUpdateMessageInGroup(params);
}

export function removeMessageFromGroup(params: {
  conversationId: string;
  groupDate: number;
  groupTime: number;
  message: IMessage;
}): IMessageGroup {
  return originRemoveMessageFromGroup(params);
}

// ------------------------------------------------------------------ //

function _uniqueMessageGroup(messageGroups: IMessageGroup[]): IMessageGroup[] {
  return unique(messageGroups, { getKey: item => item.id });
}

function _sortMessageGroups(messageGroups: IMessageGroup[]): IMessageGroup[] {
  return messageGroups
    .map(messageGroup => {
      const userMessageGroups = messageGroup.userMessageGroups
        .map(userMessageGroup => ({
          ...userMessageGroup,
          messages: _sortMessages(userMessageGroup.messages),
        }))
        .sort((group1, group2) => {
          let _sendTime1 =
            group1.messages.length > 0
              ? group1.messages[group1.messages.length - 1].sendTime
              : 0;
          let _sendTime2 =
            group2.messages.length > 0
              ? group2.messages[group2.messages.length - 1].sendTime
              : 0;

          return _sendTime1 - _sendTime2;
        });

      return {
        ...messageGroup,
        userMessageGroups,
      };
    })
    .sort(
      (group1, group2) =>
        group2.groupDate +
        group2.groupTime -
        (group1.groupDate + group1.groupTime)
    );
}

function _sortMessages(messages: IMessage[]): IMessage[] {
  return [...messages].sort(
    (message1, message2) => message1.sendTime - message2.sendTime
  );
}
