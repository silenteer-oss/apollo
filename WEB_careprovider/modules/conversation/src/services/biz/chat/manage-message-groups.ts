import { getDateInfo } from '@design-system/infrastructure/utils';
import { IMessage } from '@careprovider/services/biz';
import { IMessageGroup } from '../../models';
import { updateMessage } from './manage-messages';

const _GROUP_TIME = 15 * 60 * 1000;

const _messageGroupMap: {
  [conversationId: string]: {
    [groupDate: number]: {
      [groupTime: number]: IMessageGroup;
    };
  };
} = {};

export function initMessageGroupCache(conversationId: string): void {
  _messageGroupMap[conversationId] = {};

  return;
}

export function cleanMessageGroupCache(conversationId: string): void {
  delete _messageGroupMap[conversationId];

  return;
}

export function addMessageToGroup(params: {
  conversationId: string;
  message: IMessage;
}): IMessageGroup {
  const { conversationId, message } = params;

  _ensureInitializedMessageGroupCache(conversationId);

  const { groupDate, groupTime } = _getGroupDateTimeInfo(message.sendTime);

  if (!_messageGroupMap[conversationId][groupDate]) {
    _messageGroupMap[conversationId][groupDate] = {};
  }

  if (!_messageGroupMap[conversationId][groupDate][groupTime]) {
    _messageGroupMap[conversationId][groupDate][groupTime] = {
      id: `${groupDate}-${groupTime}`,
      groupDate,
      groupTime,
      userMessageGroups: [],
    };
  }

  const _messageGroup = _messageGroupMap[conversationId][groupDate][groupTime];
  const _userMessageGroup =
    _messageGroup.userMessageGroups.length > 0
      ? _messageGroup.userMessageGroups[
          _messageGroup.userMessageGroups.length - 1
        ]
      : undefined;

  if (_userMessageGroup && _userMessageGroup.userId === message.userId) {
    _userMessageGroup.messages.push(message);
  } else {
    _messageGroup.userMessageGroups.push({
      userId: message.userId,
      messages: [message],
    });
  }
  // Create new instance after modified to ensure immutable
  const result = { ..._messageGroup };
  result.userMessageGroups[result.userMessageGroups.length - 1] = {
    ..._messageGroup.userMessageGroups[
      _messageGroup.userMessageGroups.length - 1
    ],
  };
  _messageGroupMap[conversationId][groupDate][groupTime] = result;

  return result;
}

export function updateMessageInGroup(params: {
  conversationId: string;
  groupDate: number;
  groupTime: number;
  message: IMessage;
}): IMessageGroup {
  const { conversationId, groupDate, groupTime, message } = params;

  _ensureInitializedMessageGroupCache(conversationId);

  removeMessageFromGroup({
    conversationId,
    groupDate,
    groupTime,
    message,
  });

  return addMessageToGroup({
    conversationId,
    message,
  });
}

export function findGroupAndUpdateMessage(params: {
  conversationId: string;
  message: {
    oldMessage: IMessage;
    newMessage: IMessage;
  };
}): IMessageGroup {
  const { conversationId, message } = params;
  const { oldMessage, newMessage } = message;

  const { groupDate, groupTime } = _getGroupDateTimeInfo(newMessage.sendTime);
  updateMessage(message);
  _ensureInitializedMessageGroupCache(conversationId);

  removeMessageFromGroup({
    conversationId,
    groupDate,
    groupTime,
    message: oldMessage,
  });

  return addMessageToGroup({
    conversationId,
    message: newMessage,
  });
}

export function removeMessageFromGroup(params: {
  conversationId: string;
  groupDate: number;
  groupTime: number;
  message: IMessage;
}): IMessageGroup {
  const { conversationId, groupDate, groupTime, message } = params;

  _ensureInitializedMessageGroupCache(conversationId);

  let result: IMessageGroup;
  const _messageGroup =
    _messageGroupMap[conversationId][groupDate] &&
    _messageGroupMap[conversationId][groupDate][groupTime];

  if (_messageGroup) {
    for (
      let index = 0;
      index < _messageGroup.userMessageGroups.length;
      index++
    ) {
      const _userMessageGroup = _messageGroup.userMessageGroups[index];

      if (_userMessageGroup.userId === message.userId) {
        const _messageIndex = _userMessageGroup.messages.findIndex(
          item => item.id === message.id
        );

        if (_messageIndex >= 0) {
          _userMessageGroup.messages.splice(_messageIndex, 1);
          // Create new instance after modified to ensure immutable
          const result = { ..._messageGroup };
          result.userMessageGroups[index] = {
            ..._userMessageGroup,
          };
          break;
        }
      }
    }
  }

  return result;
}

// ------------------------------------------------------------- //

function _ensureInitializedMessageGroupCache(conversationId: string): void {
  if (!_messageGroupMap[conversationId]) {
    initMessageGroupCache(conversationId);
  }
}

function _getGroupDateTimeInfo(
  dateTime: number
): {
  groupDate: number;
  groupTime: number;
} {
  const { date, month, year, hours, minutes } = getDateInfo(new Date(dateTime));

  let groupTime: number;
  const _totalGroupTimeBlocks = Math.round((24 * 60 * 60 * 1000) / _GROUP_TIME);
  const _value = Math.round((hours * 60 + minutes) * 60 * 1000);

  for (let blockNo = 1; blockNo <= _totalGroupTimeBlocks; blockNo++) {
    groupTime = _GROUP_TIME * blockNo;

    if (_value <= groupTime) {
      break;
    }
  }

  return {
    groupDate: new Date(year, month, date).getTime(),
    groupTime,
  };
}
