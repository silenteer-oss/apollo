import {
  getConversations as apiGetConversations,
  getConversation as apiGetConversation,
} from '../../../resources';
import { IConversation } from '../../models';

export async function getConversations(): Promise<IConversation[]> {
  return apiGetConversations();
}

export async function getConversation(
  conversationId: string
): Promise<IConversation> {
  return apiGetConversation(conversationId);
}
