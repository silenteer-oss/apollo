import {
  ApplicationDatabase,
  appDatabase,
} from '@careprovider/services/client-database';
import {
  IMessage,
  MessageType,
  IAppointmentChangeLog,
  IDecryptedChatMessage,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';

export async function storeMessages(messages: IMessage[]): Promise<void> {
  messages.forEach(async message => {
    const { type, sendTime } = message;
    let _conversationId: string;
    let _patientId: string;

    if (
      [
        MessageType.AppointmentChangeLog,
        MessageType.ProcessedCommunicationPlanMessage,
      ].includes(message.type)
    ) {
      const { patientId } = message as
        | IAppointmentChangeLog
        | IProcessedCommunicationPlanMessage;
      _patientId = patientId;
    } else if (
      [
        MessageType.EmployeeChatMessage,
        MessageType.PatientChatMessage,
      ].includes(message.type)
    ) {
      const { conversationId } = message as IDecryptedChatMessage;
      _conversationId = conversationId;
    }

    await appDatabase.messageStorage.put({
      id: ApplicationDatabase.getDBMessageId(message),
      conversationId: _conversationId,
      patientId: _patientId,
      sendTime,
      type,
      message,
    });
  });
}

export async function updateMessage(params: {
  oldMessage: IMessage;
  newMessage: IMessage;
}): Promise<void> {
  const { oldMessage, newMessage } = params;

  await appDatabase.messageStorage.delete(
    ApplicationDatabase.getDBMessageId(oldMessage)
  );

  const { sendTime, type } = newMessage;
  let _patientId: string;
  let _conversationId: string;

  if (
    [
      MessageType.AppointmentChangeLog,
      MessageType.ProcessedCommunicationPlanMessage,
    ].includes(newMessage.type)
  ) {
    const { patientId } = newMessage as
      | IAppointmentChangeLog
      | IProcessedCommunicationPlanMessage;
    _patientId = patientId;
  } else if (
    [MessageType.EmployeeChatMessage, MessageType.PatientChatMessage].includes(
      newMessage.type
    )
  ) {
    const { conversationId } = newMessage as IDecryptedChatMessage;
    _conversationId = conversationId;
  }

  await appDatabase.messageStorage.put({
    id: ApplicationDatabase.getDBMessageId(newMessage),
    conversationId: _conversationId,
    patientId: _patientId,
    type,
    sendTime,
    message: newMessage,
  });
}

export async function loadMessages(params: {
  conversationId: string;
  patientId: string;
  offset: number;
  limit: number;
}): Promise<IMessage[]> {
  const { conversationId, patientId, offset, limit } = params;

  return appDatabase.messageStorage
    .reverse()
    .filter(
      schema =>
        schema.conversationId === conversationId ||
        schema.patientId === patientId
    )
    .offset(offset)
    .limit(limit)
    .toArray()
    .then(datasource => datasource.map(data => data.message).reverse());
}

export async function loadEmployeePendingMessages(
  conversationId: string
): Promise<IMessage[]> {
  return appDatabase.messageStorage
    .filter(
      schema =>
        schema.conversationId === conversationId &&
        schema.message.status === 'PENDING' &&
        schema.type === MessageType.EmployeeChatMessage
    )
    .toArray()
    .then(datasource => datasource.map(data => data.message));
}

export async function loadLastMessageTime(params: {
  conversationId: string;
  patientId: string;
}): Promise<number> {
  const { conversationId, patientId } = params;

  return appDatabase.messageStorage
    .filter(
      schema =>
        schema.conversationId === conversationId ||
        schema.patientId === patientId
    )
    .last()
    .then(data => (data ? data.sendTime : 0));
}

// ----------------- Chat Message ----------------- //

export function loadLastChatMessageTime(
  conversationId: string
): Promise<number> {
  return appDatabase.messageStorage
    .filter(
      schema =>
        schema.conversationId === conversationId &&
        [
          MessageType.EmployeeChatMessage,
          MessageType.PatientChatMessage,
        ].includes(schema.type)
    )
    .last()
    .then(data => (data ? data.sendTime : 0));
}

// ----------------- Appointment Change Log ----------------- //

export function loadLastAppointmentChangeLogTime(
  patientId: string
): Promise<number> {
  return appDatabase.messageStorage
    .filter(
      schema =>
        schema.patientId === patientId &&
        schema.type === MessageType.AppointmentChangeLog
    )
    .last()
    .then(data => (data ? data.sendTime : 0));
}

// ----------------- Processed Communication Plan Message ----------------- //

export function loadLastCommunicationPlanMessageProcessedTime(
  patientId: string
): Promise<number> {
  return appDatabase.messageStorage
    .filter(
      schema =>
        schema.patientId === patientId &&
        schema.type === MessageType.ProcessedCommunicationPlanMessage
    )
    .last()
    .then(data => (data ? data.sendTime : 0));
}
