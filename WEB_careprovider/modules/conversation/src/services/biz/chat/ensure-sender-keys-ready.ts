import { SignalAddress, SignalSessionBuilder } from '@infrastructure/signal';
import { signalStorage } from '@infrastructure/signal/services/biz';
import { unseal } from '@infrastructure/crypto';
import { cryptoDatabase } from '@careprovider/services/client-database';
import { getSenderKeys as apiGetSenderKeys } from '../../../resources';

export async function ensureSenderKeysReady(params: {
  conversationId: string;
  entityId: string; // entityId can be careProviderId or patientId
  senderId: string;
  senderDeviceId: string;
}): Promise<void> {
  const { conversationId, entityId, senderId, senderDeviceId } = params;

  const address = new SignalAddress(conversationId, senderId, senderDeviceId);
  const record = await signalStorage.loadRecord(address);

  if (record.isEmpty) {
    // Need get keys at here for case multi device for 1 careProvider
    const senderKeys = await apiGetSenderKeys({
      conversationId,
      entityId,
    });

    const careProviderKeyPair = await cryptoDatabase.keyPairStorage
      .get('CARE_PROVIDER')
      .then(data => (data ? data.value : undefined));

    for (let i = 0; i < senderKeys.length; i++) {
      const { senderKey, signingKey } = senderKeys[i];

      const careProviderSenderKey = await unseal(
        senderKey,
        careProviderKeyPair
      );

      const privateSigningKey =
        signingKey && (await unseal(signingKey, careProviderKeyPair));

      await new SignalSessionBuilder(signalStorage).process(
        address,
        careProviderSenderKey,
        privateSigningKey
      );
    }
  }
}
