import { SentryService } from '@design-system/core/components';
import { getUUID } from '@design-system/infrastructure/utils';
import { signalStorage } from '@infrastructure/signal/services/biz';
import { IDecryptedChatMessage, MessageType } from '@careprovider/services/biz';

import { sendMessage as apiSendMessage } from '../../../resources';
import { storeMessages, updateMessage } from './manage-messages';

export async function createMessage(params: {
  conversationId: string;
  userId: string;
  message: string;
}): Promise<IDecryptedChatMessage> {
  const { conversationId, userId, message } = params;

  const { senderId, senderDeviceId } = await signalStorage.loadSenderDevice();

  return {
    id: getUUID(),
    conversationId,
    senderId,
    senderDeviceId,
    userId: userId,
    type: MessageType.EmployeeChatMessage,
    sendTime: Date.now(),
    content: message,
    status: 'PENDING',
  } as IDecryptedChatMessage;
}

export async function sendMessage(params: {
  conversationId: string;
  message: IDecryptedChatMessage;
  encryptMessageHandler: (params: {
    conversationId: string;
    senderId: string;
    senderDeviceId: string;
    message: string;
  }) => Promise<{
    senderDevice: { senderId: string; senderDeviceId: string };
    message: string;
  }>;
}): Promise<IDecryptedChatMessage> {
  const { conversationId, message, encryptMessageHandler } = params;

  const { senderId, senderDeviceId } = await signalStorage.loadSenderDevice();

  try {
    const {
      message: encryptedChatMessage,
      senderDevice,
    } = await encryptMessageHandler({
      conversationId,
      senderId,
      senderDeviceId,
      message: message.content,
    });

    // Need to store message before doing api call, if api call failed then it doesn't make signal protocol run wrong
    await storeMessages([message]);

    const { id, userId, lastMessageTime } = await apiSendMessage({
      conversationId,
      senderId: senderDevice.senderId,
      senderDeviceId: senderDevice.senderDeviceId,
      message: encryptedChatMessage,
    });

    const updatedMessage = {
      ...message,
      id,
      userId,
      sendTime: lastMessageTime,
    };
    delete updatedMessage.status;

    await updateMessage({
      oldMessage: message,
      newMessage: updatedMessage,
    });

    return updatedMessage;
  } catch (error) {
    console.error(error);
    const updatedMessage = {
      ...message,
      status: 'FAILED',
    } as IDecryptedChatMessage;

    SentryService.captureException({
      data: {
        conversationId: message.conversationId,
        senderId: message.senderId,
        senderDeviceId: message.senderDeviceId,
        userId: message.userId,
      },
      error: error.message,
    });

    await updateMessage({
      oldMessage: message,
      newMessage: updatedMessage,
    });

    return updatedMessage;
  }
}
