import {
  getConversationsLastRead as apiGetConversationsLastRead,
  sendConversationLastRead as apiSendConversationLastRead,
} from '../../../resources';
import { IConversationLastRead } from '../../models';

export function getConversationsLastRead(): Promise<IConversationLastRead[]> {
  return apiGetConversationsLastRead();
}

export function sendConversationLastRead(params: {
  conversationId: string;
  lastReadTime: number;
}): Promise<IConversationLastRead> {
  const { conversationId, lastReadTime } = params;
  return apiSendConversationLastRead({ conversationId, lastReadTime });
}
