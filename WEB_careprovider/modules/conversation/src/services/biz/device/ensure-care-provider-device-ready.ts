import { getCareProviderDevice as apiGetCareProviderDevice } from '../../../resources';
import { signalStorage } from '@infrastructure/signal/services/biz';

export async function ensureCareProviderDeviceReady(): Promise<void> {
  let careProviderDevice = await signalStorage.loadSenderDevice();

  if (!careProviderDevice) {
    careProviderDevice = await apiGetCareProviderDevice();
    await signalStorage.storeSenderDevice(careProviderDevice);
  }
}
