import {
  CONVERSATION_CONTEXT_PROVIDER_ID,
  CONVERSATION_CONTEXT_CONSUMER_ID,
  CONVERSATION_MODULE_ID,
  CONVERSATION_LIST_VIEW_ID,
  CHAT_BOX_VIEW_ID,
} from '../module-config';

import { ConversationListView, ChatBoxView } from './views';

import {
  ConversationContextProvider,
  ConversationContextConsumer,
} from './context';

export default {
  [CONVERSATION_MODULE_ID]: {
    [CONVERSATION_LIST_VIEW_ID]: ConversationListView,
    [CHAT_BOX_VIEW_ID]: ChatBoxView,
    [CONVERSATION_CONTEXT_PROVIDER_ID]: ConversationContextProvider,
    [CONVERSATION_CONTEXT_CONSUMER_ID]: ConversationContextConsumer,
  },
};
