import React, { ReactNode } from 'react';
import { IConversationContext } from './models';
import { ConversationContext } from './ConversationContext';

export class ConversationContextConsumer extends React.PureComponent<{
  children: (value: IConversationContext) => ReactNode;
}> {
  render() {
    return (
      <ConversationContext.Consumer>
        {this.props.children}
      </ConversationContext.Consumer>
    );
  }
}
