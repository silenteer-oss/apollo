import React from 'react';
import { IEmployeeProfile, IPatientProfile } from '@careprovider/services/biz';
import {
  IConversationContext,
  IConversationContextProviderProps,
} from './models';
import { ConversationContext } from './ConversationContext';

export class ConversationContextProvider extends React.PureComponent<
  IConversationContextProviderProps
> {
  private _conversationContext: IConversationContext;

  constructor(props: IConversationContextProviderProps) {
    super(props);

    this._conversationContext = {
      getEmployeeProfiles: this._getEmployeeProfiles,
      getPatientProfiles: this._getPatientProfiles,
    };
  }

  componentDidUpdate(prevProps: IConversationContextProviderProps) {
    const { getEmployeeProfiles } = this.props;

    if (prevProps.getEmployeeProfiles !== getEmployeeProfiles) {
      this._conversationContext = {
        ...this._conversationContext,
        // Create a new funtion to force rerender child component
        getEmployeeProfiles: () => this._getEmployeeProfiles(),
      };
      this.forceUpdate();
    }
  }

  render() {
    return (
      <ConversationContext.Provider value={this._conversationContext}>
        {this.props.children}
      </ConversationContext.Provider>
    );
  }

  private _getPatientProfiles = (
    ids: string[]
  ): Promise<{
    patientProfileMap: { [key: string]: IPatientProfile };
    patientProfileList: IPatientProfile[];
  }> => {
    return this.props.getPatientProfiles(ids).then(profiles => ({
      patientProfileMap: profiles.reduce((result, profile) => {
        result[profile.id] = profile;
        return result;
      }, {} as { [key: string]: IPatientProfile }),
      patientProfileList: profiles,
    }));
  };

  private _getEmployeeProfiles = (): Promise<{
    employeeProfileMap: { [key: string]: IEmployeeProfile };
    employeeProfileList: IEmployeeProfile[];
  }> => {
    return this.props.getEmployeeProfiles().then(profiles => ({
      employeeProfileMap: profiles.reduce((result, profile) => {
        result[profile.id] = profile;
        return result;
      }, {} as { [key: string]: IEmployeeProfile }),
      employeeProfileList: profiles,
    }));
  };
}
