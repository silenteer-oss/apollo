/* eslint-disable prettier/prettier */
import React from 'react';
import { IConversationContext } from './models';

const defaultContext: IConversationContext = {
  getEmployeeProfiles: () => Promise.resolve(undefined),
  getPatientProfiles: () => Promise.resolve(undefined),
};

export const ConversationContext = React.createContext<IConversationContext>(
  defaultContext
);