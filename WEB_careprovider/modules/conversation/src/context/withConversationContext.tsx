import React, { ComponentType, FunctionComponent } from 'react';
import { IConversationContext } from './models';
import { ConversationContext } from './ConversationContext';

export function withConversationContext<TProps>(
  Component: ComponentType<TProps & IConversationContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <ConversationContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </ConversationContext.Consumer>
    );
  };
}
