/* eslint-disable @typescript-eslint/camelcase */
import { CommunicationPlanApi } from '@infrastructure/resource/CommunicationPlanResource';
import {
  PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID,
  IProcessedCommunicationPlanMessage,
  MessageType,
} from '@careprovider/services/biz';

export async function getProcessedCommunicationPlanMessages(params: {
  patientId: string;
  lastProcessedTime: number;
}): Promise<IProcessedCommunicationPlanMessage[]> {
  const { patientId, lastProcessedTime = 0 } = params;

  return CommunicationPlanApi.getProcessedMessagesForPatientSince(
    patientId,
    lastProcessedTime
  ).then(response =>
    response.data.map(item => {
      const { processedDate, status, ...rest } = item;

      return {
        ...rest,
        type: MessageType.ProcessedCommunicationPlanMessage,
        sendTime: processedDate,
        userId: PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID,
      } as IProcessedCommunicationPlanMessage;
    })
  );
}
