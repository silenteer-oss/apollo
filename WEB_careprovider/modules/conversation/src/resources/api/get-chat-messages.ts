import { ChatApi } from '@infrastructure/resource/ChatAppResource';
import { IEncryptedChatMessage } from '@careprovider/services/biz';
import { ConversationMessageResponse } from 'resource/chat-app-resource/model';

export async function getChatMessages(params: {
  conversationId: string;
  lastChatMessageTime: number;
}): Promise<IEncryptedChatMessage[]> {
  const { conversationId, lastChatMessageTime } = params;
  return ChatApi.getConversationMessage(
    conversationId,
    lastChatMessageTime
  ).then(response => response.data);
}

export async function queryConversationMessagesById(
  conversationId: string,
  messageIds: string[]
): Promise<ConversationMessageResponse[]> {
  return ChatApi.queryConversationMessagesById(conversationId, {
    messageIds,
  }).then(response => response.data);
}
