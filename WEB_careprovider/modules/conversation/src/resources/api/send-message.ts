import {
  ChatApiModel,
  ChatApi,
} from '@infrastructure/resource/ChatAppResource';
import { parseNumber } from '@design-system/infrastructure/utils';

export async function sendMessage(params: {
  conversationId: string;
  senderId: string;
  senderDeviceId: string;
  message: string;
}): Promise<{
  id: string;
  userId: string;
  lastMessageTime: number;
}> {
  const { conversationId, senderId, senderDeviceId, message } = params;
  const payload = {
    conversationId,
    senderId,
    senderDeviceId,
    content: message,
  } as ChatApiModel.ConversationMessageResponse;

  return ChatApi.createConversationMessage(payload).then(response => ({
    ...response.data,
    lastMessageTime: parseNumber(response.data.sendTime),
  }));
}
