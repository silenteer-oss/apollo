import {
  ChatApiModel,
  ChatApi,
} from '@infrastructure/resource/ChatAppResource';

export function getConversations(): Promise<
  ChatApiModel.ConversationResponse[]
> {
  return ChatApi.getCareProviderConversation(undefined, {
    query: { max: 1000 },
  }).then(response => response.data);
}

export function getConversation(
  conversationId: string
): Promise<ChatApiModel.ConversationResponse> {
  return ChatApi.getConversation(conversationId).then(({ data }) => data);
}

export async function getConversationsLastRead(): Promise<
  ChatApiModel.ConversationLastReadResponse[]
> {
  return ChatApi.getConversationLastRead().then(response => response.data);
}

export async function sendConversationLastRead(params: {
  conversationId: string;
  lastReadTime: number;
}): Promise<ChatApiModel.ConversationLastReadResponse> {
  return ChatApi.upsertLastRead(params).then(response => response.data);
}
