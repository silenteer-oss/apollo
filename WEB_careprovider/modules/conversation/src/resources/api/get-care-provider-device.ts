import {
  ChatApiModel,
  ChatApi,
} from '@infrastructure/resource/ChatAppResource';

export function getCareProviderDevice(): Promise<
  ChatApiModel.MyDeviceResponse
> {
  return ChatApi.getMyDevice().then(response => response.data);
}
