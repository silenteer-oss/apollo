/* eslint-disable @typescript-eslint/camelcase */
import { AppointmentApi } from '@infrastructure/resource/AppointmentResource';
import {
  APPOINTMENT_CHANGE_lOG_USER_ID,
  IAppointmentChangeLog,
  MessageType,
} from '@careprovider/services/biz';

export async function getAppointmentChangeLogs(params: {
  patientId: string;
  lastAppointmentChangeLogTime: number;
}): Promise<IAppointmentChangeLog[]> {
  const { patientId, lastAppointmentChangeLogTime } = params;
  return AppointmentApi.getChangeLogs(
    patientId,
    lastAppointmentChangeLogTime
  ).then(response =>
    response.data.map(changeLog => {
      const { logTime, ...rest } = changeLog;

      return {
        ...rest,
        type: MessageType.AppointmentChangeLog,
        sendTime: logTime,
        userId: APPOINTMENT_CHANGE_lOG_USER_ID,
      } as IAppointmentChangeLog;
    })
  );
}
