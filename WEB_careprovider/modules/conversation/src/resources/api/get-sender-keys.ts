import { ChatApi } from '@infrastructure/resource/ChatAppResource';

export async function getSenderKeys(params: {
  conversationId: string;
  entityId: string; // entityId can be careProviderId or patientId
}): Promise<
  Array<{
    senderKey: string;
    signingKey: string;
  }>
> {
  const { conversationId, entityId } = params;
  return ChatApi.getKey(conversationId, entityId).then(response =>
    response && response.data
      ? response.data.map(item => ({
          senderKey: item.key,
          signingKey: item.signingKey,
        }))
      : []
  );
}
