import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const CONVERSATION_MODULE_ID = 'ConversationModule';

export const CONVERSATION_CONTEXT_PROVIDER_ID = 'ConversationContextProvider';
export const CONVERSATION_CONTEXT_CONSUMER_ID = 'ConversationContextConsumer';

export const CONVERSATION_LIST_VIEW_ID = 'ConversationListView';
export const CHAT_BOX_VIEW_ID = 'ChatBoxView';

export interface IModuleViews {
  [CONVERSATION_LIST_VIEW_ID]: string;
  [CHAT_BOX_VIEW_ID]: string;
}
export interface IModuleProviders {
  [CONVERSATION_CONTEXT_PROVIDER_ID]: string;
}
export interface IModuleConsumers {
  [CONVERSATION_CONTEXT_CONSUMER_ID]: string;
}

export function getModuleConfig(): IModuleConfig<
  IModuleViews,
  IModuleProviders,
  IModuleConsumers
> {
  return {
    id: CONVERSATION_MODULE_ID,
    name: 'Conversation Module',
    publicAssetPath: `/module/conversation`,
    views: {
      ConversationListView: CONVERSATION_LIST_VIEW_ID,
      ChatBoxView: CHAT_BOX_VIEW_ID,
    },
    providers: {
      ConversationContextProvider: CONVERSATION_CONTEXT_PROVIDER_ID,
    },
    consumers: {
      ConversationContextConsumer: CONVERSATION_CONTEXT_CONSUMER_ID,
    },
  };
}
