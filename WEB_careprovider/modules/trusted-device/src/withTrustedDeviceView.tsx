import { Button, Toaster, InputGroup } from '@blueprintjs/core';
import { UNKNOWN_ERROR } from '@careprovider/common/constants';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import { Flex, H1, H2, BodyTextM } from '@design-system/core/components';
import { scaleFontSize } from '@design-system/core/styles';
import React, { useEffect, useRef, useState } from 'react';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import deviceService from './TrustedDeviceService';
import { ITrustedDeviceViewProps } from './TrustedDeviceModel';
import { i18nKeys } from './i18n';

export function withTrustedDeviceView(params: {
  storeCareProviderKeyPair: (
    unsealedCareproviderKeyPair: string
  ) => Promise<void>;
}): React.FunctionComponent<
  ITrustedDeviceViewProps & II18nFixedNamespaceContext
> {
  let timeout: number;

  return function TrustedDeviceView(
    props: ITrustedDeviceViewProps & II18nFixedNamespaceContext
  ): React.FunctionComponentElement<
    ITrustedDeviceViewProps & II18nFixedNamespaceContext
  > {
    const [loading, setLoading] = React.useState(false);
    const [yourCode, setYourCode] = useState(deviceService.getPhraseCode());
    const [errorMessage, setErrorMessage] = useState('');
    const toasterRef = useRef<Toaster>(null);
    const { t } = props;
    const { storeCareProviderKeyPair } = params;
    let codeInputRef: HTMLInputElement = null;

    const bindInputRef = (ref: HTMLInputElement) => {
      codeInputRef = ref;
    };

    const onAdminCodeInputChange = () => {
      if (errorMessage) {
        setErrorMessage('');
      }
    };

    useEffect(() => {
      if (loading) {
        timeout = setTimeout(() => {
          setLoading(false);
          deviceService.closeParingSocket();
          toasterRef.current.show({
            message: t(i18nKeys.pairingErrorMessage),
            intent: 'danger',
            icon: 'error',
          });
        }, WEB_SOCKET_PAIRING_TIMEOUT);
      }
      return () => clearTimeout(timeout);
    }, [loading]);

    const onTrustDevice = () => {
      if (!codeInputRef.value.trim()) {
        setErrorMessage(t(i18nKeys.pairingValidationMessage));
        return;
      }
      setLoading(true);
      return deviceService.onTrustDevice(
        yourCode + codeInputRef.value.trim().toUpperCase(),
        (_, err) => {
          if (err) {
            deviceService.closeParingSocket();
            toasterRef.current.show(UNKNOWN_ERROR);
            setLoading(false);
          }
        },
        unsealedCareproviderKeyPair =>
          storeCareProviderKeyPair(unsealedCareproviderKeyPair)
      );
    };

    const onRenew = () => {
      setYourCode(deviceService.getPhraseCode());
    };

    const onCancel = () => {
      clearTimeout(timeout);
      setLoading(false);
      deviceService.closeParingSocket();
    };

    return (
      <Flex auto className={props.className} column align={'center'}>
        <H1>{t(i18nKeys.title)}</H1>
        <BodyTextM margin={'16px 0 0 0'}>
          {t(i18nKeys.pairingCodeLabel)}
        </BodyTextM>
        <InputGroup
          autoFocus
          inputRef={bindInputRef}
          onChange={onAdminCodeInputChange}
          disabled={loading}
          maxLength={6}
          placeholder="_ _ _ _ _ _"
          className="cy-admin-code"
        />
        <BodyTextM color={props.theme.foreground.error.base}>
          {errorMessage}
        </BodyTextM>
        <BodyTextM>{t(i18nKeys.pairingInfoMessage)}</BodyTextM>
        <Flex align="center" justify="center" my={props.theme.space.l}>
          <H2
            fontSize={scaleFontSize(24)}
            lineHeight={'100%'}
            letterSpacing={'0.1em'}
            className="cy-code-label"
          >
            {yourCode}
          </H2>
        </Flex>
        <BodyTextM textAlign="center">
          {t(i18nKeys.pairingCodeWarningMessage)}&nbsp;
          {loading ? (
            <a onClick={onCancel}>{t(i18nKeys.pairingCodeCancel)}</a>
          ) : (
            <a onClick={onRenew}>{t(i18nKeys.pairingCodeRenew)}</a>
          )}
        </BodyTextM>

        <Button
          intent="primary"
          onClick={onTrustDevice}
          disabled={loading}
          fill
        >
          {loading ? t(i18nKeys.trusting) : t(i18nKeys.trust)}
        </Button>
        <Toaster ref={toasterRef} />
      </Flex>
    );
  };
}
