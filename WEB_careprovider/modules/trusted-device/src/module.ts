import {
  TRUSTED_DEVICE_MODULE_ID,
  TRUSTED_DEVICE_VIEW_ID,
} from '../module-config';
import TrustDevicePage from './TrustedDeviceStyled';

export default {
  [TRUSTED_DEVICE_MODULE_ID]: {
    [TRUSTED_DEVICE_VIEW_ID]: TrustDevicePage,
  },
};
