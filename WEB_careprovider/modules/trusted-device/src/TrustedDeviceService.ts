/* eslint-disable no-undef */
import { Subscription } from 'rxjs';
import { generateSealKeyPair, unseal } from '@infrastructure/crypto';
import { signalStorage } from '@infrastructure/signal/services/biz';
import {
  startPairingSocket$,
  stopPairingSocket,
  sendPairingData,
  getSendKeysSubject$,
} from '@careprovider/services/websocket';
import { AdminApi } from '@infrastructure/resource/AdminResource';
import { ChatApi } from '@infrastructure/resource/ChatAppResource';

function getPhraseCode(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6)
    .toUpperCase();
}

let _getSendKeysSubscription: Subscription;
let _getPairingsSubscription: Subscription;
const _generateSealKeyPair = generateSealKeyPair();

async function onTrustDevice(
  identity: string,
  callback: (data: any, err?: Error) => void,
  storeCareProviderKeyPair: (
    unsealedCareproviderKeyPair: string
  ) => Promise<void>
) {
  _getPairingsSubscription = startPairingSocket$(identity).subscribe(
    ({ status }) => {
      if (status === 'CONNECTED') {
        console.log(`Websocket opened at ${identity}`);
        AdminApi.addUserTicket({ identity }).catch(e => callback(null, e));
      }
    }
  );

  _getSendKeysSubscription = getSendKeysSubject$().subscribe(
    async ({ data }) => {
      const tempCareproviderKeyPair = await _generateSealKeyPair;
      const { type } = data;
      if (type === 'DEVICE_ADDED') {
        const { ticketId } = data;
        await tempCareproviderKeyPair;

        AdminApi.deviceLogin({ ticket: ticketId, identity })
          .then(async () => {
            const myDevice = await ChatApi.getMyDevice();
            const { senderId, senderDeviceId } = myDevice.data;

            await signalStorage.storeSenderDevice({
              senderId,
              senderDeviceId,
            });

            sendPairingData(
              JSON.stringify({
                type: 'CARE_PROVIDER_DEVICE_PUBLIC_KEY',
                careProviderDevicePublicKey: tempCareproviderKeyPair.pubKey,
              })
            );
          })
          .catch(e => callback(null, e));
      } else if (type === 'DEVICE_EXISTED') {
        sendPairingData(
          JSON.stringify({
            type: 'CARE_PROVIDER_DEVICE_PUBLIC_KEY',
            careProviderDevicePublicKey: tempCareproviderKeyPair.pubKey,
          })
        );
      } else if (type === 'CARE_PROVIDER_KEY_PAIR') {
        const { careProviderKeyPair } = data;

        const unsealedCareproviderKeyPair = await unseal(
          careProviderKeyPair,
          tempCareproviderKeyPair
        );

        await storeCareProviderKeyPair(unsealedCareproviderKeyPair);

        sendPairingData(JSON.stringify({ type: 'CARE_PROVIDER_PAIRED' }));
      } else if (type === 'ADMIN_PAIRED') {
        closeParingSocket();
        location.href = location.origin + '/login';
      }
    }
  );
}

function closeParingSocket(): void {
  stopPairingSocket();

  if (_getSendKeysSubscription) {
    _getSendKeysSubscription.unsubscribe();
    _getSendKeysSubscription = null;
  }

  if (_getPairingsSubscription) {
    _getPairingsSubscription.unsubscribe();
    _getPairingsSubscription = null;
  }
}

export default {
  getPhraseCode,
  onTrustDevice,
  closeParingSocket,
};
