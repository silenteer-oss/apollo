import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './trusted-device-view.json';

export const NAMESPACE_TRUSTED_DEVICE_VIEW = 'trusted-device-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
