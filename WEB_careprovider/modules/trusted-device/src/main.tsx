import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import { Flex } from '@design-system/core/components';
import {
  ThemeProvider,
  ThemeConsumer,
  theme,
  getAllViewTheme,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
  getAllComponentTheme,
} from '@careprovider/theme';

import TrustDevicePage from './TrustedDeviceStyled';

import '@careprovider/assets/styles/index.scss';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

export class RootView extends React.PureComponent<
  any,
  { isInitedI18n: boolean }
> {
  state = { isInitedI18n: false };

  render() {
    const { isInitedI18n } = this.state;

    return (
      <I18nContextProvider
        {...getI18nInitOptions()}
        onInitedI18n={this._handleInitedI18n}
      >
        <ThemeProvider theme={theme}>
          <ThemeConsumer>
            {theme => (
              <GlobalStyleContextProvider
                styles={{
                  main: mixGlobalStyle(theme),
                  blueprint: mixCustomBlueprintStyle(theme),
                }}
              >
                {isInitedI18n && (
                  <Flex align="center" style={{ width: '50%', margin: 'auto' }}>
                    <TrustDevicePage />
                  </Flex>
                )}
              </GlobalStyleContextProvider>
            )}
          </ThemeConsumer>
        </ThemeProvider>
      </I18nContextProvider>
    );
  }

  private _handleInitedI18n = () => {
    this.setState({ isInitedI18n: true });
  };
}

ReactDOM.render(<RootView />, document.getElementById('root'));
