import { ICareProviderTheme } from '@careprovider/theme';

export interface ITrustedDeviceViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}
