import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const TRUSTED_DEVICE_MODULE_ID = 'TrustedDeviceModule';

export const TRUSTED_DEVICE_VIEW_ID = 'TrustedDeviceView';

export interface IModuleViews {
  [TRUSTED_DEVICE_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: TRUSTED_DEVICE_MODULE_ID,
    name: 'Trusted Device Module',
    publicAssetPath: `/module/trusted-device`,
    views: {
      TrustedDeviceView: TRUSTED_DEVICE_VIEW_ID,
    },
  };
}
