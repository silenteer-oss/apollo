import { withTheme } from '@careprovider/theme';
import { withI18nContext } from '@design-system/i18n/context';
import { storeCareProviderKeyPairToIndexDB } from '@careprovider/services/client-database';
import { getModuleConfig } from '../module-config';
import { NAMESPACE_TRUSTED_DEVICE_VIEW } from './i18n';
import { withTrustedDeviceView } from './withTrustedDeviceView';

export default withTheme(
  withI18nContext(
    withTrustedDeviceView({
      storeCareProviderKeyPair: storeCareProviderKeyPairToIndexDB,
    }),
    {
      namespace: NAMESPACE_TRUSTED_DEVICE_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    }
  )
);
