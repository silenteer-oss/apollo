import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IBroadcastingEditorViewProps } from './models';
import { BroadcastingEditorView as OriginBroadcastingListView } from './BroadcastingEditorView';

export const BroadcastingEditorView = styled(OriginBroadcastingListView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-BroadcastingEditorView', className),
  })
)<IBroadcastingEditorViewProps>`
  ${props => {
    const { space } = props.theme;
    return `
      & {
        height: 100%;

      }
    `;
  }}
`;
