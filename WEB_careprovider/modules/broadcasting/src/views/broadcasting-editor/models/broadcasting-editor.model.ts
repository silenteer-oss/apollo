import { ICareProviderTheme } from '@careprovider/theme';
import { BroadcastItemResponse } from 'modules/broadcasting/services/models';

export interface IBroadcastingEditorViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface IBroadcastingEditorViewState {
  broadcastItem?: BroadcastItemResponse;
}
