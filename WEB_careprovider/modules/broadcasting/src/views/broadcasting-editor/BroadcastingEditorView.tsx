import React from 'react';
import { Observable, Subject, Subscription, ReplaySubject } from 'rxjs';
import { Flex, ErrorState } from '@design-system/core/components';
import { Header } from '@careprovider/components';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  IBroadcastingEditorViewProps,
  IBroadcastingEditorViewState,
} from './models';
import {
  IBroadcastingContext,
  IBroadcastingContextProviderProps,
  withBroadcastingContext,
} from '../../context';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from 'core/models';
import { getModuleConfig } from '../../../module-config';

import {
  Classes,
  Dialog,
  InputGroup,
  Button,
  Intent,
  FormGroup,
  MenuItem,
  Icon,
  Alignment,
} from '@blueprintjs/core';
import {
  NAMESPACE_BROADCASTING_EDITOR,
  i18nKeys,
} from '../broadcasting-editor/i18n';

class BroadcastingEditorViewComponent extends React.PureComponent<
  IBroadcastingContext &
    II18nFixedNamespaceContext &
    IBroadcastingEditorViewProps,
  IBroadcastingEditorViewState
> {
  state: IBroadcastingEditorViewState = {
    broadcastItem: undefined,
  };

  render() {
    const { className, t } = this.props;

    return (
      <Flex column className={className}>
        <Flex column className="sl-broadcasting-editor">
          I'm in Broadcast Editor
        </Flex>
      </Flex>
    );
  }
}

export const BroadcastingEditorView = withTheme(
  withBroadcastingContext(
    withI18nContext(BroadcastingEditorViewComponent, {
      namespace: NAMESPACE_BROADCASTING_EDITOR,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
