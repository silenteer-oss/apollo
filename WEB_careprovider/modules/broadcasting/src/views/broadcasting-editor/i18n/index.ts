import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './broadcasting-editor.json';

export const NAMESPACE_BROADCASTING_EDITOR = 'broadcasting-editor';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
