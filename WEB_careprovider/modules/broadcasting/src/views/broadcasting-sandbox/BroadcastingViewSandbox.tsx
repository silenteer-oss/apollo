import React from 'react';
import { Observable, Subject, Subscription, ReplaySubject } from 'rxjs';
import {
  Route,
  BrowserRouter,
  withRouter,
  RouteComponentProps,
} from 'react-router-dom';

import { withTheme } from '@careprovider/theme';

import { BroadcastingListView, IBroadcastingItem } from '../broadcasting-list';

import { IBroadcastingContext, withBroadcastingContext } from '../../context';
import {
  IBroadcastingSandboxViewProps,
  IBroadcastingSandboxViewState,
} from './models';

import { getBroadcastingPath, getBroadcastingAddingPath } from '../../services';
import { BroadcastingEditorView } from '../broadcasting-editor/BroadcastingEditorView';

const BROADCASTING_PAGE_PATH = getBroadcastingPath();
const BROADCASTING_EDITOR_PAGE_PATH = getBroadcastingAddingPath();

class BroadcastingSandboxViewComponent extends React.PureComponent<
  IBroadcastingSandboxViewProps & IBroadcastingContext & RouteComponentProps,
  IBroadcastingSandboxViewState
> {
  state: IBroadcastingSandboxViewState = {
    name: '',
    selectedBroadcastingItem: undefined,
  };

  constructor(
    props: IBroadcastingSandboxViewProps &
      IBroadcastingContext &
      RouteComponentProps
  ) {
    super(props);
  }

  render() {
    const {
      className,
      theme: { foreground },
      userProfile,
    } = this.props;
    const { name, selectedBroadcastingItem } = this.state;

    return (
      <div className={className}>
        <BrowserRouter>
          <Route
            path={BROADCASTING_PAGE_PATH}
            exact
            component={() => (
              <BroadcastingListView
                selectedBroadcastingItem={selectedBroadcastingItem}
                onSelectBroadcastingListView={() => {}}
              />
            )}
          />
          <Route
            path={BROADCASTING_EDITOR_PAGE_PATH}
            exact
            component={() => <BroadcastingEditorView />}
          />
        </BrowserRouter>
      </div>
    );
  }

  selectBroadcastingListView = (broadcastingItem: IBroadcastingItem) => {
    this.setState({ selectedBroadcastingItem: broadcastingItem });
  };
}

export const BroadcastingSandboxView = withRouter(
  withTheme(withBroadcastingContext(BroadcastingSandboxViewComponent))
);
