import { IEmployeeProfile } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { IBroadcastingItem } from '../../broadcasting-list';

export interface IBroadcastingSandboxViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  userProfile: IEmployeeProfile;
}

export interface IBroadcastingSandboxViewState {
  name: string;
  selectedBroadcastingItem: IBroadcastingItem;
}
