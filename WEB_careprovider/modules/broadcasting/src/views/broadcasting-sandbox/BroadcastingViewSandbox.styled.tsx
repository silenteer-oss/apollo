import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { scaleSpacePx, scaleSpace } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IBroadcastingSandboxViewProps } from './models';
import { BroadcastingSandboxView as OriginBroadcastingSandboxView } from './BroadcastingViewSandbox';

export const BroadcastingListViewSandboxView = styled(
  OriginBroadcastingSandboxView
).attrs(({ className }) => ({
  className: getCssClass('sl-BroadcastingListViewSandboxView', className),
}))<IBroadcastingSandboxViewProps>`
  ${props => {
    const { background, space } = props.theme;
    const chatboxClientHeight = scaleSpace(10) + scaleSpace(2) * 2 + 1; //1px = top border
    const patientInfoClientHeight = scaleSpace(20) + 1; //1px = bottom border

    return `
    & {
      display: flex;
      height: 100%;
      width: 100%;

      .sl-container-left {
        display: flex;
        flex-flow: column;
        min-width: ${scaleSpacePx(80)};
        max-width: ${scaleSpacePx(80)};
        border-right: 1px solid ${background['01']};
        background-color: ${setAlpha(background.primary.lighter, 0.5)};

        h1 {
          display: flex;
          align-items: center;
        }
      }

      .sl-container-right {
        .sl-Header {
          border-bottom: 1px solid ${background['01']};
        }

        .sl-patient-info {
          display: flex;
          justify-content: space-between;
          h3 {
            margin-bottom: ${space.xs};
          }

          .sl-divider {
            margin: 0 ${space.xs};
            border-left: 1px solid ${background['02']};
          }
        }

        .sl-ChatBoxView .sl-messages {
          max-height: calc(100vh - ${patientInfoClientHeight}px - ${chatboxClientHeight}px);
          height: calc(100vh - ${patientInfoClientHeight}px - ${chatboxClientHeight}px);
        }

        .sl-conversation-info {
          display: flex;
          flex-flow: column;
          min-width: ${scaleSpacePx(95)};
          max-width: ${scaleSpacePx(95)};
          border-left: 1px solid ${background['01']};
          background-color: ${background.white};
        }
      }
    }
  `;
  }}
`;
