import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IBroadcastingListViewProps } from './models';
import { BroadcastingListView as OriginBroadcastingListView } from './BroadcastingListView';

export const BroadcastingListView = styled(OriginBroadcastingListView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-broadcasting-list-view', className),
  })
)<IBroadcastingListViewProps>`
  ${props => {
    const { background, space, typography } = props.theme;
    const headerHeight = scaleSpace(20);
    return `
      & {
        height: 100%;

        .sl-Header {
          padding: 0 ${space.m};
          border-bottom: 1px solid ${background['01']};
          > .btn-add-message {
            margin-right: ${space.m};
            width: auto;
            > .bp3-button-text {
              font-size: 14px;
              font-weight: ${typography.link.fontWeight};
            }
          }
        }

        .sl-search-box {
          background-color: #F3F5F6;
          margin: 0 ${space.m};
          padding: ${space.xs} ${space.xs};
          > .bp3-input-group {
            width: 40%;
            margin-left: ${space.xxs};
            margin-right: ${space.s};
            > .bp3-input {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
            > .bp3-icon {
              margin: 12px 6px 12px 12px;
            }
          }
        }


        .table-container {
          flex: 1;
          padding: 0 ${space.m};

          table {
            border-collapse: collapse;
          }
          table, th, td {
            border-bottom: 1px solid ${background[`01`]};
          }

          .table-view {
            width: 100%;
            max-height: calc(100vh - ${headerHeight}px);
            overflow: auto;
            thead {
              background-color: #ffffff;
              tr {
                th {
                  vertical-align: middle;
                  box-shadow: none !important;
                }
              }
            }

            tbody {
              tr {
                td {
                  vertical-align: middle;
                  box-shadow: none !important;

                }
              }
            }
          }
        }
      }
    `;
  }}
`;
