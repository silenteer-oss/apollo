import React from 'react';
import {
  toShortCase,
  toDateFormat,
  equalDate,
  getCssClass,
} from '@design-system/infrastructure/utils';
import { BodyTextS, Flex, Box } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../../../module-config';
import { NAMESPACE_BROADCASTING_ITEM, i18nKeys } from './i18n';
import { IBroadcastingItemProps } from './models';

class BroadcastingItemComponent extends React.PureComponent<
  IBroadcastingItemProps & II18nFixedNamespaceContext
> {
  render() {
    return <Flex>Broadcasting Item</Flex>;
  }

  private _selectBroadcastingListView = () => {
    const { onSelect, broadcastingItem } = this.props;

    if (onSelect) {
      onSelect(broadcastingItem);
    }
  };
}

export const BroadcastingItemView = withTheme(
  withI18nContext(BroadcastingItemComponent, {
    namespace: NAMESPACE_BROADCASTING_ITEM,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
