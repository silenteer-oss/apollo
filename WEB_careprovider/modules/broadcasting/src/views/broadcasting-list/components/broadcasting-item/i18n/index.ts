import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './broadcasting-item.json';

export const NAMESPACE_BROADCASTING_ITEM = 'broadcasting-item';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
