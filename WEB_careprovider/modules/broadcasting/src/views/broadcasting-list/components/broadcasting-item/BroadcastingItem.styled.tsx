import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles/';
import { styled } from '@careprovider/theme';
import { IBroadcastingItemProps } from './models';
import { BroadcastingItemView as OriginBroadcastingItemView } from './BroadcastingItemView';

export const BroadcastingItem = styled(OriginBroadcastingItemView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-BroadcastingItemView', className),
  })
)<IBroadcastingItemProps>`
  ${props => {
    const { space, background, radius } = props.theme;
    return `
      & {
        padding: ${space.s} ${space.xs} ${space.s} ${space.s};
        border-bottom: 1px solid ${background['01']};
        cursor: pointer;

        &.has-unread:not(.is-selected) {
          background-color: ${background.white};
        }

        &.is-selected {
          background-color: ${background.second.lighter};
          border-right: 2px solid ${background.second.base};
        }

      }
    `;
  }}
`;
