import { ICareProviderTheme } from '@careprovider/theme';

export interface IBroadcastingItem {
  id: string;
  message: string;
  type: string;
  dateSend: Date;
  dateCreate: Date;
}

export interface IBroadcastingItemProps {
  className?: string;
  theme?: ICareProviderTheme;
  broadcastingItem: IBroadcastingItem;
  isSelected: boolean;
  onSelect: (broadcasting: IBroadcastingItem) => void;
  onSearchMode: boolean;
}
