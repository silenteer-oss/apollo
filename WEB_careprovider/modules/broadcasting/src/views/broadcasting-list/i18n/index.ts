import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './broadcasting-list.json';

export const NAMESPACE_BROADCASTING_LIST = 'broadcasting-list';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
