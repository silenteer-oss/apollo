import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { isEmpty } from '@design-system/infrastructure/utils';
import { InputGroup, Button, Intent, HTMLTable } from '@blueprintjs/core';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { NAMESPACE_BROADCASTING_LIST, i18nKeys } from './i18n';
import { getModuleConfig } from '../../../module-config';
import { H1, Flex } from '@design-system/core/components';
import { Header } from '@careprovider/components';
import { dateTimeFormat } from '@careprovider/services/util';
import {
  IBroadcastingListViewProps,
  IBroadcastingListViewState,
} from './models';
import { IBroadcastingContext, withBroadcastingContext } from '../../context';

class BroadcastingListViewComponent extends React.PureComponent<
  IBroadcastingContext &
    II18nFixedNamespaceContext &
    IBroadcastingListViewProps,
  IBroadcastingListViewState
> {
  state: IBroadcastingListViewState = {
    broadcastingItems: [
      {
        id: 'abc1',
        message: 'Thông báo dịch cúm ncovid-19',
        type: 'draft',
        dateSend: new Date(),
        dateCreate: new Date(),
      },
      {
        id: 'abc2',
        message: 'Thông báo dịch cúm H5N6',
        type: 'draft',
        dateSend: new Date(),
        dateCreate: new Date(),
      },
    ],
    searchPhase: '',
    loading: false,
  };

  onSearchBroadcasting = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ searchPhase: event.currentTarget.value });
  };

  selectBroadcastingItem() {}

  render() {
    const { className, t } = this.props;
    const { broadcastingItems, searchPhase, loading } = this.state;

    return (
      <Flex auto column className={className}>
        <Header>
          <H1>Broadcasting</H1>
          <Button
            className="btn-add-message"
            intent={Intent.PRIMARY}
            text={t(i18nKeys.addMessage)}
          />
        </Header>
        <hr />
        <Flex className="sl-search-box">
          <InputGroup
            value={searchPhase}
            leftIcon="search"
            placeholder={t(i18nKeys.searchPlaceholder)}
            onChange={this.onSearchBroadcasting}
          />
        </Flex>
        <Flex className="table-container">
          <HTMLTable className="table-view">
            <thead>
              <tr>
                <th>{t(i18nKeys.message)}</th>
                <th>{t(i18nKeys.type)}</th>
                <th>{t(i18nKeys.dateSend)}</th>
                <th>{t(i18nKeys.dateCreate)}</th>
                {/* <th /> */}
              </tr>
            </thead>
            <tbody>
              {!loading && broadcastingItems.length ? (
                broadcastingItems
                  .filter(item =>
                    searchPhase
                      ? item.message
                          .toLowerCase()
                          .includes(searchPhase.toLowerCase().trim())
                      : item
                  )
                  .map(broadcast => {
                    return (
                      <tr key={broadcast.id}>
                        <td className="col-message">{broadcast.message}</td>
                        <td>{broadcast.type}</td>
                        <td>{dateTimeFormat(broadcast.dateSend, 'L')}</td>
                        <td>{dateTimeFormat(broadcast.dateCreate, 'L')}</td>
                        {/* <td className="action-column"> {renderActions(broadcast)}</td> */}
                      </tr>
                    );
                  })
              ) : (
                <tr>
                  <td> {t(i18nKeys.noBroadcastMessage)} </td>
                </tr>
              )}
            </tbody>
          </HTMLTable>
        </Flex>
      </Flex>
    );
  }
}

export const BroadcastingListView = withTheme(
  withBroadcastingContext(
    withI18nContext(BroadcastingListViewComponent, {
      namespace: NAMESPACE_BROADCASTING_LIST,
      publicAssetPath: getModuleConfig().publicAssetPath,
    })
  )
);
