import { ICareProviderTheme } from '@careprovider/theme';
import { IBroadcastingItem } from '../components';

export interface IBroadcastingListViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  selectedBroadcastingItem: IBroadcastingItem;
  onSelectBroadcastingListView: (broadcastingItem: IBroadcastingItem) => void;
}

export interface IBroadcastingListViewState {
  broadcastingItems: IBroadcastingItem[];
  searchPhase: string;
  loading: boolean;
}
