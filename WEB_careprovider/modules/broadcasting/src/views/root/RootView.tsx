import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import {
  GlobalContextProvider,
  GlobalContextConsumer,
} from '@careprovider/context';
import { getApplicationInitial } from '@careprovider/services/biz';
import { AppointmentContextProvider } from '@careprovider/modules/appointment';
import { BroadcastingContextProvider } from '../../context';
import {
  ThemeProvider,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
} from '@careprovider/theme';
import { BroadcastingListViewSandboxView } from '../broadcasting-sandbox';
import { IRootViewProps, IRootViewState } from './models';

export class OriginRootView extends React.PureComponent<
  IRootViewProps,
  IRootViewState
> {
  state: IRootViewState = {};

  componentDidMount() {
    getApplicationInitial().then(appInitial => {
      this.setState({ appInitial });
    });
  }

  render() {
    const { className, theme } = this.props;
    const { appInitial, isInitedI18n } = this.state;

    return (
      <div className={className}>
        <I18nContextProvider
          {...getI18nInitOptions()}
          onInitedI18n={this._handleInitedI18n}
        >
          <ThemeProvider theme={theme}>
            <GlobalStyleContextProvider
              styles={{
                main: mixGlobalStyle(theme),
                blueprint: mixCustomBlueprintStyle(theme),
              }}
            >
              <Router>
                {appInitial && isInitedI18n && (
                  <GlobalContextProvider userProfile={appInitial.userProfile}>
                    <GlobalContextConsumer>
                      {context => (
                        <BroadcastingContextProvider
                          getEmployeeProfiles={context.getEmployeeProfiles}
                          getPatientProfiles={context.getPatientProfiles}
                        >
                          <AppointmentContextProvider
                            getEmployeeProfiles={context.getEmployeeProfiles}
                          >
                            <BroadcastingListViewSandboxView
                              userProfile={context.userProfile}
                            />
                          </AppointmentContextProvider>
                        </BroadcastingContextProvider>
                      )}
                    </GlobalContextConsumer>
                  </GlobalContextProvider>
                )}
              </Router>
            </GlobalStyleContextProvider>
          </ThemeProvider>
        </I18nContextProvider>
      </div>
    );
  }

  private _handleInitedI18n = () => {
    this.setState({ isInitedI18n: true });
  };
}
