import {
  BROADCASTING_MODULE_ID,
  BROADCASTING_CONTEXT_PROVIDER_ID,
  BROADCASTING_CONTEXT_CONSUMER_ID,
  BROADCASTING_LIST_VIEW_ID,
  BROADCASTING_EDITOR_VIEW_ID,
} from '../module-config';

import { BroadcastingListView, BroadcastingEditorView } from './views';

import {
  BroadcastingContextProvider,
  BroadcastingContextConsumer,
} from './context';

export default {
  [BROADCASTING_MODULE_ID]: {
    [BROADCASTING_CONTEXT_PROVIDER_ID]: BroadcastingContextProvider,
    [BROADCASTING_CONTEXT_CONSUMER_ID]: BroadcastingContextConsumer,
    [BROADCASTING_LIST_VIEW_ID]: BroadcastingListView,
    [BROADCASTING_EDITOR_VIEW_ID]: BroadcastingEditorView,
  },
};
