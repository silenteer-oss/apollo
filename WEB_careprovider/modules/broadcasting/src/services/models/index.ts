export interface BroadcastItemResponse {
  id: string;
  message: string;
  type: string;
  dateSend: Date;
  dateCreate: Date;
}
