import { parseBoolean } from '@design-system/infrastructure/utils';
import {
  BROADCASTING_PAGE_PATH,
  BROADCASTING_ADDING_PAGE_PATH,
  BROADCASTING_EDITING_PAGE_PATH,
} from '@careprovider/route';

const _isDevelop = process.env.NODE_ENV === 'develop';
const _isLocalMode = parseBoolean(process.env.IS_LOCAL_MODE);

export function getBroadcastingPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/broadcasting'
    : BROADCASTING_PAGE_PATH;
}
export function getBroadcastingAddingPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/broadcasting/add'
    : BROADCASTING_ADDING_PAGE_PATH;
}
export function getBroadcastingEditingPath(): string {
  return _isDevelop && !_isLocalMode
    ? '/module/broadcasting/edit/:id'
    : BROADCASTING_EDITING_PAGE_PATH;
}
