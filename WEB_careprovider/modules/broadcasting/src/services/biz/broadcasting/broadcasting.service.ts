import {
  getBroadcastingItem as apiGetBroadcastingItem,
  getBroadcastingList as apiGetBroadcastingList,
} from '../../../resources';
import { BroadcastItemResponse } from '../../models';

export async function getBroadcastingItem(
  id: string
): Promise<BroadcastItemResponse> {
  return apiGetBroadcastingItem(id);
}

export async function getBroadcastingList(): Promise<BroadcastItemResponse[]> {
  return apiGetBroadcastingList();
}
