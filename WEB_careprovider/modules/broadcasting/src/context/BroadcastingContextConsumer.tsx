import React, { ReactNode } from 'react';
import { IBroadcastingContext } from './models';
import { BroadcastingContext } from './BroadcastingContext';

export class BroadcastingContextConsumer extends React.PureComponent<{
  children: (value: IBroadcastingContext) => ReactNode;
}> {
  render() {
    return (
      <BroadcastingContext.Consumer>
        {this.props.children}
      </BroadcastingContext.Consumer>
    );
  }
}
