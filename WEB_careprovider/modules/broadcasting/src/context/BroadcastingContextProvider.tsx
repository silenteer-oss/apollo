import React from 'react';
import { IEmployeeProfile, IPatientProfile } from '@careprovider/services/biz';
import {
  IBroadcastingContext,
  IBroadcastingContextProviderProps,
} from './models';
import { BroadcastingContext } from './BroadcastingContext';

export class BroadcastingContextProvider extends React.PureComponent<
  IBroadcastingContextProviderProps
> {
  private _broadcastingContext: IBroadcastingContext;

  constructor(props: IBroadcastingContextProviderProps) {
    super(props);

    this._broadcastingContext = {
      getEmployeeProfiles: this._getEmployeeProfiles,
      getPatientProfiles: this._getPatientProfiles,
    };
  }

  componentDidUpdate(prevProps: IBroadcastingContextProviderProps) {
    const { getEmployeeProfiles } = this.props;

    if (prevProps.getEmployeeProfiles !== getEmployeeProfiles) {
      this._broadcastingContext = {
        ...this._broadcastingContext,
        // Create a new funtion to force rerender child component
        getEmployeeProfiles: () => this._getEmployeeProfiles(),
      };
      this.forceUpdate();
    }
  }

  render() {
    return (
      <BroadcastingContext.Provider value={this._broadcastingContext}>
        {this.props.children}
      </BroadcastingContext.Provider>
    );
  }

  private _getPatientProfiles = (
    ids: string[]
  ): Promise<{
    patientProfileMap: { [key: string]: IPatientProfile };
    patientProfileList: IPatientProfile[];
  }> => {
    return this.props.getPatientProfiles(ids).then(profiles => ({
      patientProfileMap: profiles.reduce(
        (result, profile) => {
          result[profile.id] = profile;
          return result;
        },
        {} as { [key: string]: IPatientProfile }
      ),
      patientProfileList: profiles,
    }));
  };

  private _getEmployeeProfiles = (): Promise<{
    employeeProfileMap: { [key: string]: IEmployeeProfile };
    employeeProfileList: IEmployeeProfile[];
  }> => {
    return this.props.getEmployeeProfiles().then(profiles => ({
      employeeProfileMap: profiles.reduce(
        (result, profile) => {
          result[profile.id] = profile;
          return result;
        },
        {} as { [key: string]: IEmployeeProfile }
      ),
      employeeProfileList: profiles,
    }));
  };
}
