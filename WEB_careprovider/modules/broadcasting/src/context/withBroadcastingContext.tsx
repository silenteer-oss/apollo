import React, { ComponentType, FunctionComponent } from 'react';
import { IBroadcastingContext } from './models';
import { BroadcastingContext } from './BroadcastingContext';

export function withBroadcastingContext<TProps>(
  Component: ComponentType<TProps & IBroadcastingContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <BroadcastingContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </BroadcastingContext.Consumer>
    );
  };
}
