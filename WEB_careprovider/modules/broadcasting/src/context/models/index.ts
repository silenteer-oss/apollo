import { IEmployeeProfile, IPatientProfile } from '@careprovider/services/biz';

export interface IBroadcastingContext {
  getEmployeeProfiles: () => Promise<{
    employeeProfileMap: { [key: string]: IEmployeeProfile };
    employeeProfileList: IEmployeeProfile[];
  }>;
  getPatientProfiles: (
    ids: string[]
  ) => Promise<{
    patientProfileMap: { [key: string]: IPatientProfile };
    patientProfileList: IPatientProfile[];
  }>;
}

export interface IBroadcastingContextProviderProps {
  getEmployeeProfiles: () => Promise<IEmployeeProfile[]>;
  getPatientProfiles: (ids: string[]) => Promise<IPatientProfile[]>;
}
