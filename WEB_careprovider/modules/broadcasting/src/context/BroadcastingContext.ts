/* eslint-disable prettier/prettier */
import React from 'react';
import { IBroadcastingContext } from './models';

const defaultContext: IBroadcastingContext = {
  getEmployeeProfiles: () => Promise.resolve(undefined),
  getPatientProfiles: () => Promise.resolve(undefined),
};

export const BroadcastingContext = React.createContext<IBroadcastingContext>(
  defaultContext
);