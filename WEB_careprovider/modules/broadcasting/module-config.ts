import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const BROADCASTING_MODULE_ID = 'BroadcastingModule';

export const BROADCASTING_LIST_VIEW_ID = 'BroadcastingListView';
export const BROADCASTING_EDITOR_VIEW_ID = 'BroadcastingEditorView';

export const BROADCASTING_CONTEXT_PROVIDER_ID = 'BroadcastingContextProvider';
export const BROADCASTING_CONTEXT_CONSUMER_ID = 'BroadcastingContextConsumer';

export interface IModuleViews {
  [BROADCASTING_LIST_VIEW_ID]: string;
  [BROADCASTING_EDITOR_VIEW_ID]: string;
}
export interface IModuleProviders {
  [BROADCASTING_CONTEXT_PROVIDER_ID]: string;
}
export interface IModuleConsumers {
  [BROADCASTING_CONTEXT_CONSUMER_ID]: string;
}

export function getModuleConfig(): IModuleConfig<
  IModuleViews,
  IModuleProviders,
  IModuleConsumers
> {
  return {
    id: BROADCASTING_MODULE_ID,
    name: 'Broadcasting Module',
    publicAssetPath: `/module/broadcasting`,
    views: {
      BroadcastingListView: BROADCASTING_LIST_VIEW_ID,
      BroadcastingEditorView: BROADCASTING_EDITOR_VIEW_ID,
    },
    providers: {
      BroadcastingContextProvider: BROADCASTING_CONTEXT_PROVIDER_ID,
    },
    consumers: {
      BroadcastingContextConsumer: BROADCASTING_CONTEXT_CONSUMER_ID,
    },
  };
}
