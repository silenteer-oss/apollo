import { IModuleConfig } from '@design-system/infrastructure/configs/environment';

export const PATIENT_MANAGEMENT_MODULE_ID = 'PatientManagementModule';

export const PATIENT_LIST_VIEW_ID = 'PatientListView';
export const CREATE_PATIENT_INFO_VIEW_ID = 'CreatePatientInfoView';
export const PATIENT_PAIRING_VIEW_ID = 'PatientPairingView';
export const PATIENT_QR_PAIRING_VIEW_ID = 'PatientQRPairingView';

export interface IModuleViews {
  [PATIENT_LIST_VIEW_ID]: string;
  [CREATE_PATIENT_INFO_VIEW_ID]: string;
  [PATIENT_PAIRING_VIEW_ID]: string;
  [PATIENT_QR_PAIRING_VIEW_ID]: string;
}

export function getModuleConfig(): IModuleConfig<IModuleViews> {
  return {
    id: PATIENT_MANAGEMENT_MODULE_ID,
    name: 'Patient Management Module',
    publicAssetPath: `/module/patient-management`,
    views: {
      PatientListView: PATIENT_LIST_VIEW_ID,
      CreatePatientInfoView: CREATE_PATIENT_INFO_VIEW_ID,
      PatientPairingView: PATIENT_PAIRING_VIEW_ID,
      PatientQRPairingView: PATIENT_QR_PAIRING_VIEW_ID,
    },
  };
}
