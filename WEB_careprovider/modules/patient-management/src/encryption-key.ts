import * as forge from 'node-forge';

export class EncryptionKey {
  private _tmpPrivateKey: forge.pki.rsa.PrivateKey;
  private _tmpPublicKey: forge.pki.rsa.PublicKey;

  constructor() {
    const keyPair = forge.pki.rsa.generateKeyPair({ bits: 2048, e: 0x10001 });

    this._tmpPrivateKey = keyPair.privateKey;
    this._tmpPublicKey = keyPair.publicKey;
  }

  public encryptFrom(publicKeyPem: string, content: string): string {
    const publicKey: forge.pki.rsa.PublicKey = forge.pki.publicKeyFromPem(
      publicKeyPem
    ) as forge.pki.rsa.PublicKey;

    return publicKey.encrypt(content);
  }

  public decryptFrom(content: string, privateKeyPem: string): string {
    const privateKey: forge.pki.rsa.PrivateKey = forge.pki.privateKeyFromPem(
      privateKeyPem
    ) as forge.pki.rsa.PrivateKey;

    return privateKey.decrypt(content);
  }

  public encrypt(content: string): string {
    return this._tmpPublicKey.encrypt(content);
  }

  public decrypt(content: string): string {
    return this._tmpPrivateKey.decrypt(content);
  }
}
