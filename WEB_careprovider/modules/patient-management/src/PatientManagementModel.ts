export type IGender = 'MALE' | 'FEMALE' | 'OTHERS';

type IEmployeeType = 'DOCTOR' | 'NURSE' | 'RECEPTIONIST' | 'PATIENT';

export interface IProfile {
  id: string;
  fullName?: string;
  nickName?: string;
  title?: string;
  dob?: number;
  gender?: IGender;
  phone?: string;
  email?: string;
  address?: string;
  verified?: boolean;
  accountId?: string;
  seed?: string;
  conversationId?: string;
  type?: IEmployeeType;
  careProvideId?: string;
  paired?: boolean;
}

export interface IPatientCreateRequest {
  fullName: string;
  nickName: string;
  title: string;
  dob: number;
  gender: IGender;
  phone: string;
}

export interface IPatientUpdateRequest {
  originalId: string;
  fullName: string;
  nickName: string;
  title: string;
  dob: number;
  gender: IGender;
  phone: string;
}

export interface IPatient {
  id: string;
  careProviderId: string;
  accountId: string;
}

export interface IPatientPairing {
  patientId: string;
  identity: string;
  conversationId: string;
  // type?: IEmployeeType;
  // verified?: boolean;
}

export interface IEmployeeDevice {
  id: string;
  careProviderId: string;
  userId: string;
  code: string;
  deviceId: number;
  verified: boolean;
}

export interface ISenderKeyResponse {
  senderKey: string;
  publicKey: string;
  privateKey: string;
  lastUpdated: number;
}
