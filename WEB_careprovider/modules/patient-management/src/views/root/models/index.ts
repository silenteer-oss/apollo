import { IGlobalInitial } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IRootViewProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface IRootViewState {
  appInitial?: IGlobalInitial;
  isInitedI18n?: boolean;
}
