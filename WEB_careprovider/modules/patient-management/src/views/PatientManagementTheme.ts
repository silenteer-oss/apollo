export interface IPatientManagementViewTheme {
  item: {
    border: {
      default: string;
    };
    background: {
      default: string;
      hover: string;
    };
  };
}
