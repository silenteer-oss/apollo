import React, { useEffect, useRef, useState } from 'react';
import { Button, Toaster, InputGroup } from '@blueprintjs/core';
import { Flex, BodyTextM, H2, H3 } from '@design-system/core/components';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import { scaleFontSize } from '@design-system/core/styles';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import { UNKNOWN_ERROR } from '@careprovider/common/constants';
import { ICryptoKeyPairSchema } from '@careprovider/services/client-database';
import { IPatientPairingViewProps } from './PatientPairingModel';
import PatientPairingService from './PatientPairingService';
import { i18nKeys } from './i18n';

export function withPatientPairingView(params: {
  getCareProviderKeyPair: () => Promise<ICryptoKeyPairSchema>;
}): React.FunctionComponent<IPatientPairingViewProps> {
  let timeout: number;

  return function PatientPairingView(
    props: IPatientPairingViewProps & II18nFixedNamespaceContext
  ): React.FunctionComponentElement<IPatientPairingViewProps> {
    const [loading, setLoading] = React.useState(false);
    const [careProviderCode, setHospitalCode] = useState(
      PatientPairingService.getPhraseCode()
    );
    const [errorMessage, setErrorMessage] = useState('');
    const { t } = props;
    const { getCareProviderKeyPair } = params;
    const toasterRef = useRef<Toaster>(null);
    let codeInputRef: HTMLInputElement = null;

    const bindInputRef = (ref: HTMLInputElement) => {
      codeInputRef = ref;
    };

    const { patient, className, onSubmit } = props;

    const onPatientCodeInputChange = () => {
      if (errorMessage) {
        setErrorMessage('');
      }
    };

    useEffect(() => {
      if (loading) {
        timeout = setTimeout(() => {
          setLoading(false);
          PatientPairingService.closeWebsocket();
          toasterRef.current.show({
            message: t(i18nKeys.pairingTimeoutError),
            intent: 'danger',
            icon: 'error',
          });
        }, WEB_SOCKET_PAIRING_TIMEOUT);
      }

      return () => clearTimeout(timeout);
    }, [loading]);

    const onTrustPatientDevice = () => {
      if (!codeInputRef.value.trim()) {
        setErrorMessage(t(i18nKeys.requiredError));
        return;
      }

      setLoading(true);

      const patientDeviceCode = codeInputRef.value.trim().toUpperCase();

      PatientPairingService.onPairing(
        patient.id,
        patient.phone,
        patientDeviceCode + careProviderCode,
        patient.conversationId,
        getCareProviderKeyPair,
        error => {
          console.log(error);
          if (error) {
            setLoading(false);
            toasterRef.current.show(UNKNOWN_ERROR);
          } else {
            onSubmit();
          }
        }
      );
    };

    const onRenew = () => {
      setHospitalCode(PatientPairingService.getPhraseCode());
      if (timeout) {
        clearTimeout(timeout);
        setLoading(false);
        PatientPairingService.closeWebsocket();
      }
    };

    const onCancel = () => {
      clearTimeout(timeout);
      setLoading(false);
      PatientPairingService.closeWebsocket();
    };

    return (
      <Flex auto className={className} column align={'center'}>
        <BodyTextM margin={0}>{t(i18nKeys.intro1)}</BodyTextM>
        <InputGroup
          autoFocus
          inputRef={bindInputRef}
          onChange={onPatientCodeInputChange}
          disabled={loading}
          maxLength={6}
          placeholder="_ _ _ _ _ _"
        />
        <BodyTextM color={props.theme.foreground.error.base}>
          {errorMessage}
        </BodyTextM>
        <H3>{t(i18nKeys.intro2)}</H3>
        <BodyTextM>
          <span
            dangerouslySetInnerHTML={{
              __html: t(i18nKeys.intro3, {
                fullName: patient.fullName,
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </BodyTextM>
        <Flex
          align="center"
          justify="center"
          my={props.theme.space.l}
          className="careprovider-code"
        >
          <H2
            fontSize={scaleFontSize(24)}
            lineHeight={'100%'}
            letterSpacing={'0.1em'}
          >
            {careProviderCode}
          </H2>
        </Flex>
        <BodyTextM textAlign="center">
          {t(i18nKeys.intro4)}?&nbsp;
          {loading ? (
            <a onClick={onCancel}>{t(i18nKeys.pairingCodeCancel)}</a>
          ) : (
            <a onClick={onRenew}>{t(i18nKeys.pairingCodeRenew)}</a>
          )}
        </BodyTextM>

        <Button
          intent="primary"
          onClick={onTrustPatientDevice}
          fill
          disabled={loading}
        >
          {loading
            ? t(i18nKeys.waitingForPatientPair)
            : t(i18nKeys.completedPairing)}
        </Button>

        <Toaster ref={toasterRef} />
      </Flex>
    );
  };
}
