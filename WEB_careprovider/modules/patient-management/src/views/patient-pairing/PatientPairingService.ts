import { Subscription } from 'rxjs';

import {
  startPairingSocket$,
  stopPairingSocket,
  sendPairingData,
  getSendKeysSubject$,
} from '@careprovider/services/websocket';
import { SignalAddress, SignalSessionBuilder } from '@infrastructure/signal';
import { signalStorage } from '@infrastructure/signal/services/biz';
import { seal, unseal } from '@infrastructure/crypto';
import { PatientPairing } from './PatientPairingModel';
import { getSenderKeys } from '@careprovider/modules/conversation/resources';
import { ICryptoKeyPairSchema } from '@careprovider/services/client-database';
import { CareProviderApi } from '@infrastructure/resource/CareProviderAppResource';
import { ChatApi } from '@infrastructure/resource/ChatAppResource';

let getSendKeysSubscription: Subscription;
let getPairingSubscription: Subscription;
let patientPairing: PatientPairing;

/* DO NOT CHANGE, IF CHANGE MUST BE SYNC WITH mock.data.ts IN MOBILE APP */
const MOCK_ID = '2468AB';

function getPhraseCode(): string {
  const value = Math.random()
    .toString(36)
    .substr(2, 6)
    .toUpperCase();

  if (value === MOCK_ID) {
    return getPhraseCode();
  }

  return value;
}

function onPairing(
  patientId: string,
  patientPhone: string,
  identity: string,
  conversationId: string,
  getCareProviderKeyPair: () => Promise<ICryptoKeyPairSchema>,
  callBack?: (err: Error) => void
) {
  getPairingSubscription = startPairingSocket$(identity).subscribe(
    ({ status }) => {
      if (status === 'CONNECTED') {
        patientPairing = new PatientPairing(
          conversationId,
          identity,
          patientId
        );
        CareProviderApi.pairing({
          conversationId: conversationId,
          identity,
          patientId,
        }).catch(e => callBack(e));
      }
    }
  );

  getSendKeysSubscription = getSendKeysSubject$().subscribe(
    async ({ data }) => {
      const careProviderKeyPair: {
        pubKey: string;
        privKey: string;
      } = await getCareProviderKeyPair().then(data => data.value);

      if (data.type === 'DEVICE_READY') {
        CareProviderApi.createTicket(identity);
      } else if (data.type === 'PATIENT_DEVICE_PAIR') {
        const myDevice = await ChatApi.getMyDevice();
        const { senderId, senderDeviceId } = myDevice.data;

        const conversationkeys = await getSenderKeys({
          conversationId,
          entityId: senderId,
        });
        const { senderKey } = conversationkeys[0];

        const distributionMessage = await unseal(
          senderKey,
          careProviderKeyPair
        );

        await signalStorage.storeSenderDevice({
          senderId,
          senderDeviceId,
        });

        sendPairingData(
          JSON.stringify({
            type: 'CARE_PROVIDER_SENDER_KEY',
            careProviderSenderKey: await seal(
              distributionMessage,
              data.publicKey
            ),
            careProviderPublicKey: careProviderKeyPair.pubKey,
            careProviderDeviceNumber: senderDeviceId,
          })
        );
      } else if (data.type === 'PATIENT_SENDER_KEY') {
        const unsealedPatientSenderKey = await unseal(
          data.patientSenderKey,
          careProviderKeyPair
        );

        const { patientDeviceNumber, conversationId } = data;

        await new SignalSessionBuilder(signalStorage).process(
          new SignalAddress(conversationId, patientId, patientDeviceNumber),
          unsealedPatientSenderKey
        );

        const encryptedPatientSenderKeys = await seal(
          unsealedPatientSenderKey,
          careProviderKeyPair.pubKey
        );

        await backupPatientKey(encryptedPatientSenderKeys);
        // await markPatientPaired();

        sendPairingData(
          JSON.stringify({ type: 'CARE_PROVIDER_PAIRING_SUCCESS' })
        );
      } else if (data.type === 'PATIENT_PAIRING_SUCCESS') {
        const { senderDeviceId } = data;
        try {
          await CareProviderApi.pairedSuccess(patientId, {
            patientId,
            phone: patientPhone,
            deviceId: senderDeviceId,
          });
        } catch (e) { }
        if (callBack) {
          callBack(null);
        }

        stopPairingSocket();
        if (getSendKeysSubscription) {
          getSendKeysSubscription.unsubscribe();
          getSendKeysSubscription = null;
        }
        if (getPairingSubscription) {
          getPairingSubscription.unsubscribe();
          getPairingSubscription = null;
        }
      }
    }
  );
}

async function generateAndBackupKey(
  conversationId: string,
  getCareProviderKeyPair: () => Promise<ICryptoKeyPairSchema>
): Promise<any> {
  const myDevice = await ChatApi.getMyDevice();
  const { senderId, senderDeviceId } = myDevice.data;

  const { distributionMessage, signingPrivateKey } = await _generateSenderKey(
    conversationId,
    senderId,
    senderDeviceId
  );

  const careProviderKeyPair: {
    pubKey: string;
    privKey: string;
  } = await getCareProviderKeyPair().then(data => data.value);

  return ChatApi.createMyKey({
    conversationId,
    key: await seal(distributionMessage, careProviderKeyPair.pubKey),
    signingKey: await seal(signingPrivateKey, careProviderKeyPair.pubKey),
  });
}

function backupPatientKey(encryptedPatientSenderKeys: string) {
  return ChatApi.createKey({
    conversationId: patientPairing.conversationId,
    key: encryptedPatientSenderKeys,
    entityId: patientPairing.patientId,
  });
}

async function _generateSenderKey(
  conversationId: string,
  senderId: string,
  senderDeviceId: string
): Promise<{ distributionMessage: string; signingPrivateKey: string }> {
  const sessionBuilder = new SignalSessionBuilder(signalStorage);
  return await sessionBuilder.create(
    new SignalAddress(conversationId, senderId, senderDeviceId)
  );
}

function closeWebsocket() {
  stopPairingSocket();
}

export default {
  generateAndBackupKey,
  getPhraseCode,
  onPairing,
  backupPatientKey,
  closeWebsocket,
};
