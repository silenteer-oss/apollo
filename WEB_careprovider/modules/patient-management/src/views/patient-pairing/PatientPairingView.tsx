import { withI18nContext } from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getCareProviderKeyPairFromIndexDB } from '@careprovider/services/client-database';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_PATIENT_PAIRING_VIEW } from './i18n';
import { withPatientPairingView } from './withPatientPairingView';

export default withTheme(
  withI18nContext(
    withPatientPairingView({
      getCareProviderKeyPair: getCareProviderKeyPairFromIndexDB,
    }),
    {
      namespace: NAMESPACE_PATIENT_PAIRING_VIEW,
      publicAssetPath: getModuleConfig().publicAssetPath,
    }
  )
);
