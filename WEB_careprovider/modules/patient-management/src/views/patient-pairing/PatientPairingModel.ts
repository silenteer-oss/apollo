import { ICareProviderTheme } from '@careprovider/theme';
import { IProfile } from '../../PatientManagementModel';
export interface IPatientPairingViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  onSubmit?: (err?: Error) => void;
  patient: IProfile;
}
export interface IPatientQRPairingViewProps {
  className?: string;
  theme?: ICareProviderTheme;
  onSubmit?: (err?: Error) => void;
  onEnterCode?: (IProfile) => void;
  patient: IProfile;
}
export interface IPairingRequestPayload {
  patientId: string;
  identity: string;
  conversationId: string;
}

export interface IConversationResponse {
  id: string;
  userIds: string[];
  careProviderId: string;
  patientId: string;
}

export interface IPatientBackupKeyRequest {
  conversationId: string;
  entityId: string;
  key: string;
}

export class PatientPairing {
  private _conversationId: string;
  private _identity: string;
  private _patientId: string;

  constructor(conversationId: string, identity: string, patientId: string) {
    this._conversationId = conversationId;
    this._identity = identity;
    this._patientId = patientId;
  }

  set conversationId(value: string) {
    this._conversationId = value;
  }

  get conversationId(): string {
    return this._conversationId;
  }

  set identity(value: string) {
    this._identity = value;
  }

  get identity(): string {
    return this._identity;
  }

  set patientId(value: string) {
    this._patientId = value;
  }

  get patientId(): string {
    return this._patientId;
  }
}
