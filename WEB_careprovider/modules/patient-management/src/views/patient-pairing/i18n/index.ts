import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './patient-pairing-view.json';

export const NAMESPACE_PATIENT_PAIRING_VIEW = 'patient-pairing-view';
export const NAMESPACE_PATIENT_QR_PAIRING_VIEW = 'patient-qr-pairing-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
