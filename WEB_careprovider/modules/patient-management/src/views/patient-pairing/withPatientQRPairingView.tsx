import React, { useEffect, useRef, useState } from 'react';
import { Toaster } from '@blueprintjs/core';
import { Flex, BodyTextM } from '@design-system/core/components';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import { WEB_SOCKET_PAIRING_TIMEOUT } from '@silenteer/hermes/websocket';
import { UNKNOWN_ERROR } from '@careprovider/common/constants';
import { ICryptoKeyPairSchema } from '@careprovider/services/client-database';
import { IPatientQRPairingViewProps } from './PatientPairingModel';
import PatientPairingService from './PatientPairingService';
import { i18nKeys } from './i18n';
import Scanner from '../../common/Scanner';

export function withPatientQRPairingView(params: {
  getCareProviderKeyPair: () => Promise<ICryptoKeyPairSchema>;
}): React.FunctionComponent<IPatientQRPairingViewProps> {
  let timeout: number;

  return function PatientQRPairingView(
    props: IPatientQRPairingViewProps & II18nFixedNamespaceContext
  ): React.FunctionComponentElement<IPatientQRPairingViewProps> {
    const [loading, setLoading] = React.useState(false);

    const [errorMessage, setErrorMessage] = useState('');
    const { t } = props;
    const { getCareProviderKeyPair } = params;
    const toasterRef = useRef<Toaster>(null);

    const { patient, className, onSubmit, onEnterCode } = props;

    useEffect(() => {
      if (loading) {
        timeout = setTimeout(() => {
          setLoading(false);
          PatientPairingService.closeWebsocket();
          toasterRef.current.show({
            message: t(i18nKeys.pairingTimeoutError),
            intent: 'danger',
            icon: 'error',
          });
        }, WEB_SOCKET_PAIRING_TIMEOUT);
      }

      return () => clearTimeout(timeout);
    }, [loading]);

    const onTrustPatientDevice = value => {
      setLoading(true);

      const patientDeviceCode = value.trim().toUpperCase();

      PatientPairingService.onPairing(
        patient.id,
        patient.phone,
        patientDeviceCode,
        patient.conversationId,
        getCareProviderKeyPair,
        error => {
          console.log(error);
          if (error) {
            setLoading(false);
            toasterRef.current.show(UNKNOWN_ERROR);
          } else {
            onSubmit();
          }
        }
      );
    };
    const onPatientCodeInputChange = value => {
      if (errorMessage) {
        setErrorMessage('');
      }
      onTrustPatientDevice(value);
    };

    const onCancel = () => {
      clearTimeout(timeout);
      setLoading(false);
      PatientPairingService.closeWebsocket();
      onEnterCode(patient);
    };

    return (
      <Flex auto className={className} column align={'center'}>
        <BodyTextM margin={0}>{t(i18nKeys.intro1)}</BodyTextM>
        <Scanner onChange={onPatientCodeInputChange} />

        <BodyTextM textAlign="center">{t(i18nKeys.intro4)}</BodyTextM>
        <BodyTextM textAlign="center">
          {<a onClick={onCancel}>{t(i18nKeys.enterCode)}</a>}
        </BodyTextM>
        <Toaster ref={toasterRef} />
      </Flex>
    );
  };
}
