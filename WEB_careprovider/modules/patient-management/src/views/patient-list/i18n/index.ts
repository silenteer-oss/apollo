import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './patient-list-view.json';

export const NAMESPACE_PATIENT_LIST_VIEW = 'patient-list-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
