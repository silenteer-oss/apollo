import { ICareProviderTheme } from '@careprovider/theme';
import { IProfile } from '../../PatientManagementModel';
import { IAppointmentDetailsViewProps } from '@careprovider/modules/appointment';

export interface IPatientListProps {
  className?: string;
  theme?: ICareProviderTheme;
  onViewDetail?: (id: number) => void;
  onUpdatedPatient?: (id: string) => void;
  onCreateAppointment?: (id: string) => void;
  renderSetAppointmentView?: (
    props: IAppointmentDetailsViewProps
  ) => JSX.Element;
}

export interface IPatientListState {
  patientList: IProfile[];
  isOpenPatientInfoDialog: boolean;
  isOpenPatientPairingDialog: boolean;
  isOpenPatientQRPairingDialog: boolean;
  isOpenCloseEditPatientInfoAlert: boolean;
  selectedPatient: IProfile;
  offset: number;
  openDeletionDialog: boolean;
  searchPhrase: string;
  isLoading: boolean;
}
