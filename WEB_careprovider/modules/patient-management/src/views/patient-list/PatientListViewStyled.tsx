import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { scaleSpace, scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { PatientListView as OriginPatientListView } from './PatientListView';
import { IPatientListProps } from './PatientListModel';

export const PatientListView = styled(OriginPatientListView).attrs(
  ({ className }) => ({
    className: getCssClass('sl-PatientListView', className),
  })
)<IPatientListProps>`
  ${props => {
    const { space, foreground, background } = props.theme;
    const headerHeight = scaleSpace(20);
    const headerItemHeight = scaleSpace(10);

    return `
      & {
        height: 100%;

        .header-title {
          > h1 {
            flex: 1;
          }

          > .bp3-input-group {
            margin-right: ${space.m};
            > .bp3-input {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
            > .bp3-icon {
              margin: 12px 6px 12px 12px;
            }
          }

          .add-patient-button {
            height: ${headerItemHeight}px;
          }
        }

        .sl-LoadingState {
          position: absolute;
          width: auto;
          height: auto;
          top: 80px;
          left: 24px;
          right: 24px;
          bottom: 0;
          background-color: ${setAlpha(background.white, 0.5)};
        }

        .empty-container {
          flex: 1;
          justify-content: center;
          align-items: center;
          padding: 0 ${space.m};
          text-align: center;
        }

        .table-container {
          flex: 1;
          padding: 0 ${space.m};

          .table-view {
            width: 100%;
            max-height: calc(100vh - ${headerHeight}px);
            overflow: auto;

            th {
              box-shadow: none !important;
            }

            td {
              vertical-align: middle;
              box-shadow: none !important;
              border-bottom: 1px solid ${background[`01`]};
            }

            .name-column {
              width: 300px;
            }

            .action-column {
              padding: ${space.xs};

              .sl-AppointmentDetailsView {
                .btn-set-appointment {
                  background: none;
                  border: none;
                  min-width: ${scaleSpacePx(30)};
                  justify-content: center;
                  padding: 0 ${space.m};

                  .bp3-button-text {
                    color: ${foreground.primary.base}
                    font-weight: 600;
                  }
                }
              }

              .sl-btn-code-scan {
                min-width: ${scaleSpacePx(25)};
                background: none;
                border: none;
                justify-content: center;
                padding: 0 ${space.m};

                .bp3-button-text {
                  color: ${foreground.primary.base}
                  font-weight: 600;
                }
              }

              .sl-btn-more-info {
                min-width: ${scaleSpacePx(10)};
                background: none;
                border: none;
                justify-content: center;
                padding: 0 ${space.m};

                .bp3-button-text {
                  color: ${foreground.primary.base}
                  font-weight: 600;
                }
              }
            }
          }
        }
      }
    `;
  }}
`;
