import { ICareProviderTheme } from '@careprovider/theme';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import {
  IPatientCreateRequest,
  IPatientUpdateRequest,
  IProfile,
  IGender,
} from '../../PatientManagementModel';

export interface ICreatePatientInfoProps {
  className?: string;
  theme?: ICareProviderTheme;
  selectedPatient?: IProfile;
  onAddPatient?: (
    createPatientInfo: IPatientCreateRequest
  ) => Promise<IProfile>;
  onEditPatient?: (
    updatePatientInfo: IPatientUpdateRequest
  ) => Promise<IProfile>;
  getInstance?: (
    instance: React.PureComponent<
      ICreatePatientInfoProps & II18nFixedNamespaceContext,
      ICreatePatientInfoState
    >
  ) => void;
}

export interface ICreatePatientInfoState {
  isSubmissionConfirmed: boolean;
  isOpenPatientWarning: boolean;
  errors?: any;
}

export interface IPatientInfoValues {
  fullName?: string;
  nickName?: string;
  title?: string;
  dob?: number;
  dayOfBirth?: number;
  monthOfBirth?: number;
  yearOfBirth?: number;
  phone?: string;
  gender: IGender;
}
