import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { CreatePatientInfoView as OriginCreatePatientInfoView } from './CreatePatientInfoView';
import { ICreatePatientInfoProps } from './CreatePatientInfoModel';

export const StyledCreatePatientInfoView = styled(
  OriginCreatePatientInfoView
).attrs(({ className }) => ({
  className: getCssClass('sl-CreatePatientInfoView', className),
}))<ICreatePatientInfoProps>`
  ${props => {
    const { space, foreground } = props.theme;

    return `
      & {
        width: ${scaleSpacePx(128)};
        margin: auto;
        padding: ${space.m};

        .require-label {
          color: ${foreground.error.base};
        }

        .bp3-form-group {
          flex: 1;
          margin-bottom: 0px;

          > .bp3-input-group {
            display: flex;

            > input {
              flex: 1;
            }
          }
        }

        .title-group {
          width: ${scaleSpacePx(65)};
          margin-right: ${space.m};
          margin-bottom: ${space.s};
        }

        .full-name-group {
          margin-bottom: ${space.s};
        }

        .nick-name-group {
          margin-bottom: ${space.s};
        }

        .birthday-group {
          width: ${scaleSpacePx(65)};
          margin-right: ${space.m};
          .birthday-input {
            margin-right: ${space.xs};

            .bp3-input {
              width: ${scaleSpacePx(10)};
            }
          }
        }

        .phone-group {
          flex: 1;
        }

        .submit-button {
          margin-top: ${space.m};
        }
    `;
  }}
`;
