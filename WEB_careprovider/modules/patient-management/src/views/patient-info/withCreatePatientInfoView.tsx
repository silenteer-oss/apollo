import React from 'react';
import {
  Formik,
  FormikActions,
  Form,
  Field,
  FormikErrors,
  FieldProps,
} from 'formik';
import {
  InputGroup,
  RadioGroup,
  Radio,
  Button,
  Intent,
  FormGroup,
  Classes,
  NumericInput,
  Label,
  Alert,
  Alignment,
  Icon,
  MenuItem,
} from '@blueprintjs/core';
import { Select, IItemRendererProps } from '@blueprintjs/select';
import { Flex } from '@design-system/core/components';
import { getCssClass } from '@design-system/infrastructure/utils';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import {
  isEmptyObject,
  isEmpty,
  isValidPhoneNumber,
} from '@design-system/infrastructure/utils';
import { ErrorService } from '@design-system/core/components';
import {
  now,
  dateTimeFormat,
  isBetween,
  dateToMoment,
} from '@careprovider/services/util';
import { IGender, IProfile } from '../../PatientManagementModel';
import PatientService from '../../PatientManagementService';
import {
  ICreatePatientInfoProps,
  IPatientInfoValues,
  ICreatePatientInfoState,
} from './CreatePatientInfoModel';
import { i18nKeys } from './i18n';

const TitleSelect = Select.ofType<string>();
const SELECTABLE_TITLES = ['', 'Ông', 'Bà', 'Anh', 'Chị'];

export function withCreatePatientInfoView(): React.ComponentClass<
  ICreatePatientInfoProps & II18nFixedNamespaceContext,
  ICreatePatientInfoState
> {
  return class OriginCreatePatientInfoView extends React.PureComponent<
    ICreatePatientInfoProps & II18nFixedNamespaceContext,
    ICreatePatientInfoState
  > {
    private submitButton: any;

    constructor(props: ICreatePatientInfoProps & II18nFixedNamespaceContext) {
      super(props);
      this.state = {
        isOpenPatientWarning: false,
        isSubmissionConfirmed: false,
        errors: {},
      };
      if (props.getInstance) {
        props.getInstance(this);
      }
    }

    render() {
      const { t } = this.props;

      const selectedPatient = this.props.selectedPatient || ({} as IProfile);

      const initialValues = {
        fullName: selectedPatient.fullName || '',
        nickName: selectedPatient.nickName || '',
        title: selectedPatient.title || '',
        gender: selectedPatient.gender || ('FEMALE' as IGender),
        phone: selectedPatient.phone || '',
        dob: selectedPatient.dob || new Date().getTime(),
        dayOfBirth: undefined,
        monthOfBirth: undefined,
        yearOfBirth: undefined,
      };

      const dateOfBirth = selectedPatient.dob;
      if (dateOfBirth) {
        initialValues.dayOfBirth = new Date(dateOfBirth).getDate();
        initialValues.monthOfBirth = new Date(dateOfBirth).getMonth() + 1;
        initialValues.yearOfBirth = new Date(dateOfBirth).getFullYear();
      }

      return (
        <Flex auto column className={this.props.className}>
          <Alert
            className="bp3-alert-fill"
            confirmButtonText={t(i18nKeys.alertOkButton)}
            intent={Intent.DANGER}
            isOpen={this.state.isOpenPatientWarning}
            onClose={this.doCancelPatientEnrollment}
          >
            <p>{t(i18nKeys.alertInfo1)}</p>
          </Alert>
          <Formik
            initialValues={initialValues}
            onSubmit={this.onSaveForm}
            render={({ values, isSubmitting, dirty }) => {
              return (
                <Form>
                  <Flex auto>
                    <Flex className="title-group">
                      {this.renderTitleField()}
                    </Flex>
                    <Flex>{this.renderGenderField(values)}</Flex>
                  </Flex>
                  <Flex className="full-name-group">
                    {this.renderFullNameField()}
                  </Flex>
                  <Flex className="nick-name-group">
                    {this.renderNickNameField()}
                  </Flex>
                  <Flex auto>
                    <Flex className="birthday-group">
                      {this.renderBirthdayField()}
                    </Flex>
                    <Flex className="phone-group">
                      {this.renderPhoneField()}
                    </Flex>
                  </Flex>
                  <Button
                    className="submit-button"
                    ref={ref => (this.submitButton = ref)}
                    type="submit"
                    intent={Intent.PRIMARY}
                    loading={isSubmitting}
                    fill
                    disabled={
                      isEmpty(values.fullName) ||
                      isEmpty(values.phone) ||
                      isEmpty(values.dayOfBirth) ||
                      isEmpty(values.monthOfBirth) ||
                      isEmpty(values.yearOfBirth)
                    }
                  >
                    {t(i18nKeys.submitButton)}
                  </Button>
                </Form>
              );
            }}
          />
        </Flex>
      );
    }

    renderTitleField = () => {
      const { t } = this.props;
      const { errors } = this.state;

      return (
        <Field
          name="title"
          render={({ field, form }: FieldProps<{}>) => {
            const onSelect = (item: string) => {
              const { setFieldValue } = form;
              setFieldValue(field.name, item);
            };

            return (
              <FormGroup
                label={t(i18nKeys.titleLabel)}
                helperText={errors.title}
                className={errors.title ? Classes.INTENT_DANGER : ''}
              >
                <TitleSelect
                  className={getCssClass(
                    'sl-select cy-title-select',
                    errors.title ? Classes.INTENT_DANGER : ''
                  )}
                  filterable={false}
                  items={SELECTABLE_TITLES}
                  activeItem={field.value}
                  onItemSelect={onSelect}
                  itemRenderer={this.renderTitleItem}
                  popoverProps={{
                    minimal: true,
                    captureDismiss: true,
                    fill: true,
                  }}
                >
                  <Button
                    fill
                    alignText={Alignment.LEFT}
                    text={
                      field.value || (
                        <div
                          style={{
                            height: 20,
                            color: '#9ea9b2',
                          }}
                        >
                          {t(i18nKeys.titlePlaceholer)}
                        </div>
                      )
                    }
                    rightIcon={<Icon icon="caret-down" />}
                  />
                </TitleSelect>
              </FormGroup>
            );
          }}
        />
      );
    };

    renderFullNameField = () => {
      const { t } = this.props;
      const { errors } = this.state;
      return (
        <Field name="fullName">
          {({ field }) => (
            <FormGroup
              label={this.renderRequireLabel(t(i18nKeys.fullNameLabel))}
              helperText={errors.fullName}
              className={errors.fullName ? Classes.INTENT_DANGER : ''}
            >
              <InputGroup
                {...field}
                intent={errors.fullName ? Intent.DANGER : Intent.NONE}
                placeholder={t(i18nKeys.namePlaceholder)}
              />
            </FormGroup>
          )}
        </Field>
      );
    };

    renderNickNameField = () => {
      const { t } = this.props;
      const { errors } = this.state;
      return (
        <Field name="nickName">
          {({ field }) => (
            <FormGroup
              label={t(i18nKeys.nickNameLabel)}
              helperText={errors.nickName}
              className={errors.nickName ? Classes.INTENT_DANGER : ''}
            >
              <InputGroup
                {...field}
                intent={errors.nickName ? Intent.DANGER : Intent.NONE}
                placeholder={t(i18nKeys.nickNamePlaceholder)}
              />
            </FormGroup>
          )}
        </Field>
      );
    };

    renderGenderField = values => {
      const { t } = this.props;
      const { errors } = this.state;
      return (
        <Field name="gender">
          {({ field, form }) => {
            const { setFieldValue } = form;
            const onGenderChange = (
              event: React.FormEvent<HTMLInputElement>
            ) => {
              setFieldValue(field.name, event.currentTarget.value);
            };
            return (
              <FormGroup
                label={this.renderRequireLabel(t(i18nKeys.genderLabel))}
                helperText={errors.gender}
                className={errors.gender ? Classes.INTENT_DANGER : ''}
              >
                <RadioGroup
                  onChange={onGenderChange}
                  inline
                  selectedValue={values.gender}
                >
                  <Radio
                    label={t(i18nKeys.femaleLabel)}
                    value="FEMALE"
                    className="inline-radio"
                  />
                  <Radio
                    label={t(i18nKeys.maleLabel)}
                    value="MALE"
                    className="inline-radio"
                  />
                  <Radio
                    label={t(i18nKeys.otherGenderLabel)}
                    value="OTHERS"
                    className="inline-radio"
                  />
                </RadioGroup>
              </FormGroup>
            );
          }}
        </Field>
      );
    };

    renderBirthdayField = () => {
      const { t } = this.props;
      const { errors } = this.state;
      return (
        <FormGroup
          helperText={errors.dob}
          className={errors.dob ? Classes.INTENT_DANGER : ''}
        >
          <Label>{this.renderRequireLabel(t(i18nKeys.birthdayLabel))}</Label>
          <Flex>
            <Flex className="birthday-input">
              <Field name="dayOfBirth">
                {({ field, form }) => (
                  <NumericInput
                    name={field.name}
                    value={field.value}
                    buttonPosition="none"
                    placeholder={t(i18nKeys.dayPlaceholder)}
                    maxLength={2}
                    className={errors.dob ? Classes.INTENT_DANGER : ''}
                    intent={errors.dob ? Intent.DANGER : Intent.NONE}
                    pattern="\d*"
                    onValueChange={(
                      valueAsNumber: number,
                      valueAsString: string
                    ) => {
                      form.setFieldValue(field.name, valueAsString);
                    }}
                  />
                )}
              </Field>
            </Flex>
            <Field name="monthOfBirth">
              {({ field, form }) => (
                <NumericInput
                  name={field.name}
                  value={field.value}
                  buttonPosition="none"
                  placeholder={t(i18nKeys.monthPlaceholder)}
                  maxLength={2}
                  pattern="\d*"
                  className="birthday-input"
                  intent={errors.dob ? Intent.DANGER : Intent.NONE}
                  onValueChange={(
                    valueAsNumber: number,
                    valueAsString: string
                  ) => {
                    form.setFieldValue(field.name, valueAsString);
                  }}
                />
              )}
            </Field>
            <Field name="yearOfBirth">
              {({ field, form }) => (
                <NumericInput
                  name={field.name}
                  value={field.value}
                  buttonPosition="none"
                  placeholder={t(i18nKeys.yearPlaceholder)}
                  maxLength={4}
                  pattern="\d*"
                  className="birthday-input"
                  intent={errors.dob ? Intent.DANGER : Intent.NONE}
                  onValueChange={(
                    valueAsNumber: number,
                    valueAsString: string
                  ) => {
                    form.setFieldValue(field.name, valueAsString);
                  }}
                />
              )}
            </Field>
          </Flex>
        </FormGroup>
      );
    };

    renderPhoneField = () => {
      const { t } = this.props;
      const { errors } = this.state;
      return (
        <FormGroup
          label={this.renderRequireLabel(t(i18nKeys.phoneLabel))}
          helperText={errors.phone}
          className={errors.phone ? Classes.INTENT_DANGER : ''}
        >
          <Field name="phone">
            {({ field, form }) => (
              <NumericInput
                type="tel"
                name={field.name}
                value={field.value || ''}
                buttonPosition="none"
                maxLength={10}
                intent={errors.phone ? Intent.DANGER : Intent.NONE}
                onValueChange={(
                  valueAsNumber: number,
                  valueAsString: string
                ) => {
                  form.setFieldValue(field.name, valueAsString);
                }}
                fill
                placeholder={t(i18nKeys.phonePlaceholder)}
              />
            )}
          </Field>
        </FormGroup>
      );
    };

    renderRequireLabel = (label: string) => {
      return (
        <Flex>
          {label}&nbsp;
          <Flex className="require-label">*</Flex>
        </Flex>
      );
    };

    renderTitleItem = (
      title: string,
      { handleClick, modifiers }: IItemRendererProps
    ): JSX.Element => {
      if (!modifiers.matchesPredicate) {
        return null;
      }
      return (
        <MenuItem
          active={modifiers.active}
          key={title}
          onClick={handleClick}
          text={isEmpty(title) ? <div style={{ height: 20 }} /> : title}
          shouldDismissPopover
        />
      );
    };

    doCancelPatientEnrollment = () => {
      this.setState({ isOpenPatientWarning: false } as ICreatePatientInfoState);
    };

    doConfirmPatientEnrollment = () => {
      this.setState(
        {
          isOpenPatientWarning: false,
          isSubmissionConfirmed: true,
        } as ICreatePatientInfoState,
        () => {
          this.submitButton.buttonRef.click();
        }
      );
    };

    onSaveForm = (
      values: IPatientInfoValues,
      formikBag: FormikActions<IPatientInfoValues>
    ) => {
      const { selectedPatient, t } = this.props;
      const {
        fullName,
        nickName,
        title,
        phone,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        gender,
      } = values;

      let errors = {} as FormikErrors<IPatientInfoValues>;

      if (isEmpty(fullName, true) || fullName.length > 40) {
        errors.fullName = t(i18nKeys.fullNameError);
      }

      if (nickName.length > 40) {
        errors.fullName = t(i18nKeys.nickNameError);
      }

      if (!isValidPhoneNumber(phone)) {
        errors.phone = t(i18nKeys.phoneError);
      }

      const dobString = `${dayOfBirth}-${monthOfBirth}-${yearOfBirth}`;
      const dateOfBirth = +dateToMoment(dobString, 'DD-MM-YYYY');
      const currentTime = +now();
      if (
        dayOfBirth <= 0 ||
        monthOfBirth <= 0 ||
        yearOfBirth <= 0 ||
        dateOfBirth.toLocaleString() === 'Invalid date'
      ) {
        errors.dob = t(i18nKeys.birthdayError);
      }
      if (
        !isBetween(
          dateOfBirth,
          +dateToMoment('01-01-1900', 'DD-MM-YYYY'),
          currentTime,
          'year',
          '[]'
        )
      ) {
        errors.dob = t(i18nKeys.birthdayRangeError, {
          date: dateTimeFormat(currentTime, 'YYYY'),
        });
      }

      if (isEmptyObject(errors)) {
        const patientInfo = {
          fullName,
          nickName,
          title,
          dob: new Date(yearOfBirth, monthOfBirth - 1, dayOfBirth).getTime(),
          gender,
          phone,
        };
        const ignorePatientId = selectedPatient
          ? selectedPatient.id
          : undefined;
        PatientService.checkExistProfileByPhoneNumber(
          this.state.isSubmissionConfirmed,
          ignorePatientId,
          phone
        ).then(result => {
          if (result.data) {
            formikBag.setSubmitting(false);
            this.setState({
              isOpenPatientWarning: true,
            } as ICreatePatientInfoState);
            return;
          }
          this.onSavePatient(formikBag, patientInfo, selectedPatient);
        });
      } else {
        this.setState({ errors });
        formikBag.setSubmitting(false);
      }
    };

    onSavePatient = (formikBag, patientInfo, selectedPatient) => {
      if (selectedPatient) {
        this.props
          .onEditPatient({
            ...patientInfo,
            originalId: selectedPatient.id,
          })
          .then(() => formikBag.setSubmitting(false))
          .catch(error => {
            formikBag.setSubmitting(false);
            this.setState({
              errors: ErrorService.catchValidationErrors(error),
            });
          });
      } else {
        this.props
          .onAddPatient(patientInfo)
          .then(() => formikBag.setSubmitting(false))
          .catch(error => {
            formikBag.setSubmitting(false);
            this.setState({
              errors: ErrorService.catchValidationErrors(error),
            });
          });
      }
    };
  };
}
