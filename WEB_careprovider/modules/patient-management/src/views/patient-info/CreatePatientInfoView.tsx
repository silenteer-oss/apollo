import { withI18nContext } from '@design-system/i18n/context';
import { withTheme } from '@careprovider/theme';
import { getModuleConfig } from '../../../module-config';
import { NAMESPACE_PATIENT_INFO_VIEW } from './i18n';
import { withCreatePatientInfoView } from './withCreatePatientInfoView';

export const CreatePatientInfoView = withTheme(
  withI18nContext(withCreatePatientInfoView(), {
    namespace: NAMESPACE_PATIENT_INFO_VIEW,
    publicAssetPath: getModuleConfig().publicAssetPath,
  })
);
