import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './patient-info-view.json';

export const NAMESPACE_PATIENT_INFO_VIEW = 'patient-info-view';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
