import {
  PATIENT_MANAGEMENT_MODULE_ID,
  PATIENT_LIST_VIEW_ID,
  CREATE_PATIENT_INFO_VIEW_ID,
  PATIENT_PAIRING_VIEW_ID,
} from '../module-config';
import {
  PatientListView,
  StyledCreatePatientInfoView,
  PatientPairingViewStyled,
} from './views/';

export default {
  [PATIENT_MANAGEMENT_MODULE_ID]: {
    [PATIENT_LIST_VIEW_ID]: PatientListView,
    [CREATE_PATIENT_INFO_VIEW_ID]: StyledCreatePatientInfoView,
    [PATIENT_PAIRING_VIEW_ID]: PatientPairingViewStyled,
  },
};
