export interface IScannerModel {
  scannerActive?: boolean;
  onChange(value: string);
}
export interface IScannerState {
  inputBuffer?: string;
  currentScan?: { date: string; catchAllScans: string };
  entryTimeout?: number;
}

export interface IBarcodeViewModel {
  scanner?: {
    data?: { currentScan: { date: string; catchAllScans: string } };
    functions?: {};
  };
}
