import React from 'react';
import { IBarcodeViewModel } from './ScannerModel';
import QRCode from 'qrcode.react';
import imgUrl from '@careprovider/assets/images/qrcodescanner.jpg';

class BarcodeView extends React.PureComponent<IBarcodeViewModel> {
  constructor(props: IBarcodeViewModel) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {}
  renderQRCode() {
    let scanner = this.props.scanner.data;

    let currentScan = scanner.currentScan;
    if (currentScan.catchAllScans) {
      return (
        <QRCode
          bgColor="#FFFFFF"
          fgColor="#000000"
          level="Q"
          size={256}
          value={currentScan.catchAllScans}
        />
      );
    } else {
      return <span />;
    }
  }
  render() {
    return (
      <div
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          margin: 10,
        }}
      >
        <div
          style={{
            width: 400,
            height: 400,
            borderRadius: '50%',
            backgroundImage: 'url(' + imgUrl + ')',
            backgroundSize: 'cover',
            overflow: 'hidden',
          }}
        >
          <div
            style={{
              position: 'relative',
              left: 0,
              top: 70,
              textAlign: 'center',
            }}
          >
            {this.renderQRCode()}
          </div>
        </div>
      </div>
    );
  }
}

export default BarcodeView;
