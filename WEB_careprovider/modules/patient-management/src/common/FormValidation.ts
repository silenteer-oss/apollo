import { isEmpty } from '@design-system/infrastructure/utils';

export function validateRequired(value: string) {
  if (isEmpty(value, true)) {
    return 'This field cannot be empty';
  }
  return undefined;
}
