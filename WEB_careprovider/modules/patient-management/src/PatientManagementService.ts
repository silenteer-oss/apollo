/* eslint-disable no-undef */
import {
  IProfile,
  IPatientCreateRequest,
  IPatientUpdateRequest,
} from './PatientManagementModel';
import { isEmpty } from '@design-system/infrastructure/utils';
import { CareProviderApi } from '@infrastructure/resource/CareProviderAppResource';
import { ProfileApi } from '@infrastructure/resource/ProfileResource';
import { Gender, Pagination } from 'resource/careprovider-app-resource/model';

function getPatientList(offset: number): Promise<IProfile[]> {
  return CareProviderApi.getList({ offset } as Pagination).then(result => {
    const patientList = result.data;
    if (isEmpty(patientList)) {
      return Promise.resolve([]);
    }
    return ProfileApi.getPatientProfileByIds({
      originalIds: patientList.map(item => item.id),
    }).then(profiles =>
      profiles.data.map(profile => {
        const patient = patientList.find(p => p.id === profile.id);
        return patient ? { ...patient, ...profile } : profile;
      })
    );
  });
}

function searchPatientProfile(
  query: string,
  offset: number
): Promise<IProfile[]> {
  return ProfileApi.searchPatientProfile(query, {
    offset,
  } as Pagination).then(response => {
    const profiles = response.data;
    if (isEmpty(profiles)) {
      return Promise.resolve([]);
    }
    return CareProviderApi.getPatientByIds({
      ids: profiles.map(item => item.id),
    }).then(patientList =>
      profiles.map(profile => {
        const patient = patientList.data.find(p => p.id === profile.id);
        return patient ? { ...patient, ...profile } : profile;
      })
    );
  });
}

function createPatient(request: IPatientCreateRequest): Promise<IProfile> {
  return CareProviderApi.create({
    ...request,
    email: '',
    address: '',
    gender: request.gender as Gender,
  }).then(response => ({
    ...response.data,
    gender: response.data.gender as any, //TODO: workaround
  }));
}

function updatePatientProfile(
  request: IPatientUpdateRequest
): Promise<IProfile> {
  return ProfileApi.updatePatientProfile({
    ...request,
    gender: request.gender as Gender,
    email: '',
    address: '',
  }).then(response => ({
    ...response.data,
    gender: response.data.gender as any, //TODO: workaround
  }));
}

function checkExistProfileByBirthdayAndPhoneNumber(
  isSubmissionConfirmed: boolean,
  ignorePatientId: string,
  phoneNumber: string,
  birthday: number
) {
  if (isSubmissionConfirmed) {
    return Promise.resolve({ data: false });
  }
  return ProfileApi.checkExistByBirthdayAndPhoneNumber({
    ignorePatientId,
    phoneNumber,
    birthday,
  });
}

function checkExistProfileByPhoneNumber(
  isSubmissionConfirmed: boolean,
  ignorePatientId: string,
  phoneNumber: string
) {
  if (isSubmissionConfirmed) {
    return Promise.resolve({ data: false });
  }
  return ProfileApi.checkExistByPhoneNumber({
    ignorePatientId,
    phoneNumber,
  });
}
export default {
  getPatientList,
  searchPatientProfile,
  createPatient,
  updatePatientProfile,
  checkExistProfileByBirthdayAndPhoneNumber,
  checkExistProfileByPhoneNumber,
};
