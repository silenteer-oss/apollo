import { parseBoolean } from '@design-system/infrastructure/utils';
import { DEFAULT_LANGUAGE, SUPPORTED_LANGUAGES } from '@infrastructure/i18n';

const _isDevelop = process.env.NODE_ENV === 'develop';
const _isLocalMode = parseBoolean(process.env.IS_LOCAL_MODE);

export function getI18nInitOptions(): {
  whitelist: string[];
  fallbackLng: string;
  debug: boolean;
} {
  return {
    whitelist: SUPPORTED_LANGUAGES,
    fallbackLng: DEFAULT_LANGUAGE,
    debug: _isDevelop || _isLocalMode,
  };
}
