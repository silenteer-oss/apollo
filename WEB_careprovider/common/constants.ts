import { IToastProps, Intent } from '@blueprintjs/core';

export const UNKNOWN_ERROR: IToastProps = {
  message: 'Oops!! Server error. Please try again',
  intent: Intent.DANGER,
  icon: 'error',
};
