import path from 'path';
import { Configuration } from 'webpack';
import merge from 'webpack-merge';
import WorkerPlugin from 'worker-plugin';
import { ProxyConfigMap, ProxyConfigArray } from 'webpack-dev-server';
import { parseBoolean } from '../LIB_design-system/infrastructure/utils';
import { getWebpackConfig as getCoreWebpackConfig } from '../LIB_design-system/infrastructure/configs/webpack/models';
import { configCopy } from '../LIB_design-system/infrastructure/configs/webpack/parts';
import {
  setupProjectPaths,
  getProjectPaths,
} from '../LIB_design-system/infrastructure/configs/webpack/services';
import {
  DEFAULT_LANGUAGE,
  SUPPORTED_LANGUAGES,
} from '../LIB_infrastructure/i18n';
import { getApplicationConfig } from './application/application-config';

const _env = process.env.npm_config_env as 'develop' | 'production';
const { host, port, proxy } = getApplicationConfig();

let _devServer: {
  host: string;
  port: number;
  proxy?: ProxyConfigMap | ProxyConfigArray;
};
if (parseBoolean(process.env.npm_config_dev_server)) {
  _devServer = {
    host,
    port,
    proxy,
  };
}
const _namespace = '@careprovider';
const _projectDir = path.resolve(__dirname, '.');

_setupProjectPaths({
  namespace: _namespace,
  projectDir: _projectDir,
  appName: process.env.npm_config_app,
});

const _paths = getProjectPaths({
  namespace: _namespace,
  projectDir: _projectDir,
});

const _webpackConfig = merge(
  {
    plugins: [
      new WorkerPlugin({
        globalObject: 'self',
      }),
    ],
  } as Configuration,
  configCopy([
    {
      patterns: [
        {
          from: path.resolve(__dirname, './assets/favicon-dot.ico'),
          to: _paths.dist,
        },
      ],
    },
  ])
);

export default merge(
  getCoreWebpackConfig(_env, {
    projectDir: _projectDir,
    namespace: _namespace,
    pageTitle: 'Careprovider Application',
    internalDependencies: [
      {
        projectDir: path.resolve(__dirname, '../../hermes'),
        namespace: '@silenteer/hermes',
      },
      {
        projectDir: path.resolve(__dirname, '../LIB_infrastructure'),
        namespace: '@infrastructure',
      },
    ],
    i18n: {
      defaultLanguage: DEFAULT_LANGUAGE,
      supportedLanguages: SUPPORTED_LANGUAGES,
    },
    devServer: _devServer,
  }),
  _webpackConfig
);

// ------------------------------------------------------------ //

export function _setupProjectPaths(params: {
  namespace: string;
  projectDir: string;
  appName: string;
}): void {
  const { namespace, projectDir, appName } = params;

  if (appName) {
    setupProjectPaths({
      namespace,
      paths: {
        tsconfig: path.join(projectDir, 'tsconfig.json'),
        nodeModule: path.join(projectDir, 'node_modules'),
        translation: path.join(projectDir, `node_modules/@${namespace}/i18n`),
        build: path.join(projectDir, 'build'),
        dist: path.join(projectDir, 'dist'),
        distAssets: path.join(projectDir, 'dist/assets'),
        distModule: path.join(projectDir, 'dist/module'),
        project: path.join(projectDir, 'src'),
        assets: path.join(projectDir, 'src/assets'),
        application: path.join(projectDir, `src/application.${appName}/src`),
        modules: path.join(projectDir, `src/modules.${appName}`),
      },
    });
  }
}
