import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICommunicationPlanTimelinePageProps } from './models';
import { OriginCommunicationPlanTimelinePage } from './CommunicationPlanTimelinePage';

export default styled(OriginCommunicationPlanTimelinePage).attrs(
  ({ className }) => ({
    className: getCssClass('sl-CommunicationPlanTimelinePage', className),
  })
)<ICommunicationPlanTimelinePageProps>`
  & {
    display: flex;
    height: 100%;
    width: 100%;
    overflow-x: auto;
  }
`;
