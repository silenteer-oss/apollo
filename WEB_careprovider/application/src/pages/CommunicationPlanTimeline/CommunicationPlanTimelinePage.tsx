import React from 'react';
import { MicroCommunicationPlanTimelineView } from '../../micro-modules';
import {
  ICommunicationPlanTimelinePageProps,
  ICommunicationPlanTimelinePageState,
} from './models';

export class OriginCommunicationPlanTimelinePage extends React.PureComponent<
  ICommunicationPlanTimelinePageProps,
  ICommunicationPlanTimelinePageState
> {
  state: ICommunicationPlanTimelinePageState = {
    isReady: false,
  };

  componentDidMount() {
    this.setState({ isReady: true });
  }

  render() {
    const { className } = this.props;
    const { isReady } = this.state;

    if (!isReady) {
      return null;
    }

    return (
      <div className={className}>
        <MicroCommunicationPlanTimelineView />
      </div>
    );
  }
}
