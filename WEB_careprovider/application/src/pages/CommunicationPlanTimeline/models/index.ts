import { ICareProviderTheme } from '@careprovider/theme';

export interface ICommunicationPlanTimelinePageProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ICommunicationPlanTimelinePageState {
  isReady: boolean;
}
