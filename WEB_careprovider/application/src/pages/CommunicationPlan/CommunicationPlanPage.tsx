import React from 'react';
import { MicroCommunicationPlanListView } from '../../micro-modules';
import {
  ICommunicationPlanPageProps,
  ICommunicationPlanPageState,
} from './models';

export class OriginCommunicationPlanPage extends React.PureComponent<
  ICommunicationPlanPageProps,
  ICommunicationPlanPageState
> {
  state: ICommunicationPlanPageState = {
    isReady: false,
  };

  componentDidMount() {
    this.setState({ isReady: true });
  }

  render() {
    const { className } = this.props;
    const { isReady } = this.state;

    if (!isReady) {
      return null;
    }

    return (
      <div className={className}>
        <MicroCommunicationPlanListView />
      </div>
    );
  }
}
