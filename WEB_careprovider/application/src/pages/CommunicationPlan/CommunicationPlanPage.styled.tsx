import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ICommunicationPlanPageProps } from './models';
import { OriginCommunicationPlanPage } from './CommunicationPlanPage';

export default styled(OriginCommunicationPlanPage).attrs(({ className }) => ({
  className: getCssClass('sl-CommunicationPlanPage', className),
}))<ICommunicationPlanPageProps>`
  & {
    display: flex;
    height: 100%;
    width: 100%;
  }
`;
