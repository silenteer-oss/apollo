import { ICareProviderTheme } from '@careprovider/theme';

export interface ICommunicationPlanPageProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ICommunicationPlanPageState {
  isReady: boolean;
}
