import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {
  Menu,
  MenuItem,
  InputGroup,
  Divider,
  Button,
  Intent,
} from '@blueprintjs/core';
import { Flex, H1, LoadingState } from '@design-system/core/components';
import { toShortCase } from '@design-system/infrastructure/utils';
import { II18nFixedNamespaceContext } from '@design-system/i18n/context';
import { hashPassword } from '@careprovider/services/webworker';
import { IEmployeeProfile } from '@careprovider/services/biz';
import { IGlobalContext } from '@careprovider/context';
import { ILoginPageProps, ILoginPageState } from './LoginPageModel';
import { i18nKeys } from './i18n';
import { login } from './LoginServices';

import Logo from '@careprovider/assets/images/logo.svg';
import LeftArrow from '@careprovider/assets/images/arrow-left.svg';

const Employee = React.memo(
  (props: IEmployeeProfile): React.ReactElement<IEmployeeProfile> => {
    const { fullName, email } = props;

    return (
      <Flex align="center">
        <div className="avatar">
          <p>{toShortCase(fullName)}</p>
        </div>
        <Flex column>
          <H1 margin={0}>{fullName}</H1>
          {email && <p>{email}</p>}
        </Flex>
      </Flex>
    );
  }
);

export function withLoginPage(
  showLoginButton: boolean = false
): React.ComponentClass<
  ILoginPageProps &
    IGlobalContext &
    RouteComponentProps<any> &
    II18nFixedNamespaceContext
> {
  return class LoginPage extends React.PureComponent<
    ILoginPageProps &
      IGlobalContext &
      RouteComponentProps<any> &
      II18nFixedNamespaceContext,
    ILoginPageState
  > {
    state: ILoginPageState = {
      password: undefined,
      step: 1,
      selectedUser: undefined,
      userList: [],
      isProcessing: false,
    };

    componentDidMount() {
      this.props
        .getEmployeeProfiles()
        .then(userList => this.setState({ userList }));
    }

    render() {
      const { className, t } = this.props;
      const { userList = [], isProcessing } = this.state;

      return (
        <div className={className}>
          {isProcessing && <LoadingState />}
          <div className="left-section">
            <img src={Logo} />
            <h1>{t(i18nKeys.guide)}</h1>
          </div>
          <Divider />
          <div className="right-section">
            {this.state.step === 1 && this.renderUserList(userList)}
            {this.state.step === 2 && this.renderAuthentication()}
          </div>
        </div>
      );
    }

    setUser = (selectedUser: IEmployeeProfile) => {
      this.setState({
        selectedUser,
      });
    };

    goToAuthenticationStep = (selectedUser: IEmployeeProfile) => {
      this.setState({
        selectedUser,
        step: 2,
      });
    };

    goToUserList = () => {
      this.setState({
        step: 1,
      });
    };

    renderUserList = (userList: IEmployeeProfile[]) => {
      return (
        <Menu>
          {userList.map(user => (
            <MenuItem
              key={user.id}
              text={<Employee {...user} />}
              onClick={() => this.goToAuthenticationStep(user)}
            />
          ))}
        </Menu>
      );
    };

    login = (selectedUser: IEmployeeProfile) => {
      if (
        this.state.isProcessing ||
        !this.state.password ||
        this.state.password.length === 0
      ) {
        return;
      }

      this.setState({ isProcessing: true }, () => {
        const accountId = selectedUser.accountId;
        const employeeId = selectedUser.id;

        login(employeeId, accountId, this.state.password, hashPassword)
          .then(() => {
            this.props.getInitialState();
            this.props.setUserProfile(selectedUser);
            this.props.history.push(`app`);
          })
          .catch(error => {
            this.setState({ isProcessing: false });
            throw error;
          });
      });
    };

    onPasswordChange = (event: React.FormEvent<HTMLInputElement>) => {
      this.setState({ password: event.currentTarget.value });
    };

    renderAuthentication = () => {
      const { selectedUser, isProcessing } = this.state;
      const { t } = this.props;
      return (
        <Flex column>
          <Flex align="center">
            <img src={LeftArrow} onClick={this.goToUserList} />
            <Employee {...selectedUser} />
          </Flex>
          <form
            onSubmit={e => {
              e.preventDefault();
              this.login(selectedUser);
            }}
          >
            <InputGroup
              placeholder={t(i18nKeys.passwordPlaceholder)}
              type={'password'}
              onChange={this.onPasswordChange}
              autoFocus={true}
              disabled={isProcessing}
            />
            {showLoginButton && (
              <Flex auto className="login-button">
                <Button
                  type="submit"
                  text={t(i18nKeys.login)}
                  intent={Intent.PRIMARY}
                />
              </Flex>
            )}
          </form>
        </Flex>
      );
    };
  };
}
