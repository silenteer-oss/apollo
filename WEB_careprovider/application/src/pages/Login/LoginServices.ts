import { UserIdPasswordCredentials } from '@silenteer/hermes/resource/authentication-resource/model';
import { AuthenticationApi } from '@infrastructure/resource/AuthenticationResource';
import { AdminApi } from '@infrastructure/resource/AdminResource';

export function getAllEmployeeIdList(): Promise<string[]> {
  return AdminApi.getAll().then(response => response.data.map(item => item.id));
}

export async function login(
  employeeId: string,
  accountId: string,
  password: string,
  hashPasswordHandler: (plainPassword: string, salt: string) => Promise<string>
) {
  const seed = await AdminApi.getSeed(accountId).then(
    response => response.data
  );
  const hashedPassword = await hashPasswordHandler(password, seed);
  return AuthenticationApi.login({
    userId: employeeId,
    password: hashedPassword,
  } as UserIdPasswordCredentials);
}
