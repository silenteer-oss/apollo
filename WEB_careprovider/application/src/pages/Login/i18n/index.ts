import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './login-page.json';

export const NAMESPACE_LOGIN_PAGE = 'login-page';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
