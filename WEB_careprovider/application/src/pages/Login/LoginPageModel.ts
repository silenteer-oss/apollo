import { ICareProviderTheme } from '@careprovider/theme';
import { IEmployeeProfile } from '@careprovider/services/biz';

export interface ILoginPageProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface ILoginPageState {
  step: number;
  password: string;
  selectedUser: IEmployeeProfile;
  userList: IEmployeeProfile[];
  isProcessing: boolean;
}
