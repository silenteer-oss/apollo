import { withRouter } from 'react-router-dom';
import { withI18nContext } from '@design-system/i18n/context';
import { withGlobalContext } from '@careprovider/context';
import { getApplicationConfig } from '../../../application-config';
import { NAMESPACE_LOGIN_PAGE } from './i18n';

import { withLoginPage } from './withLoginPage';

export const LoginPage = withRouter(
  withGlobalContext(
    withI18nContext(withLoginPage(), {
      namespace: NAMESPACE_LOGIN_PAGE,
      publicAssetPath: getApplicationConfig().publicAssetPath,
    })
  )
);
