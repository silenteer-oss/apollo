import { scaleSpacePx, scaleFontSizePx } from '@design-system/core/styles';
import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ILoginPageProps } from './LoginPageModel';
import { LoginPage as OriginLoginPage } from './LoginPage';

export const LoginPage = styled(OriginLoginPage).attrs(({ className }) => ({
  className: getCssClass('sl-LoginPage', className),
}))<ILoginPageProps>`
  ${props => {
    const { space, radius, foreground, background } = props.theme;

    return `
      & {
        display: flex;
        align-items: center;
        width: calc(100vw - ${space.m});
        height: calc(100vh - ${space.m} * 2);
        padding: ${space.m} 0 ${space.m} ${space.m};
        background: linear-gradient(180deg, rgba(255, 255, 255, 0.8) -1.8%, rgba(235, 241, 245, 0.8) 101.25%), #FFFFFF;

        .sl-LoadingState {
          position: absolute;
          top: 0;
          bottom: 0;
          left: 0;
          right: 0;
          background-color: ${setAlpha(background.white, 0.5)};
        }

        .left-section {
          display: flex;
          flex-flow: column;
          align-items: flex-end;
          justify-content: center;
          width: 50%;
          max-height: 60%;

          > img {
            width: ${scaleSpacePx(80)};
            height: ${scaleSpacePx(20)};
            margin-bottom: ${space.l};
          }

          > h1 {
            font-size: ${scaleFontSizePx(14)};
            font-weight: normal;
            line-height: ${scaleSpacePx(10)};
          }
        }

        .bp3-divider {
          height: 60%;
          margin: 0 ${scaleSpacePx(14)};
        }

        .right-section {
          display: flex;
          flex-flow: column;
          width: 50%;
          max-height: 60%;
          overflow: auto;

          img {
            width: ${scaleSpacePx(4)};
            height: ${scaleSpacePx(6)};
            &:hover {
              cursor: pointer;
            }
          }

          .bp3-menu {
            box-shadow: none;
            background: none;

            > li + li {
              margin-top: ${space.m};
            }

            .bp3-menu-item {
              align-items: center;
            }
          }

          .bp3-icon-chevron-left {
            cursor: pointer;
          }

          form {
            margin-top: ${space.s};

            .bp3-input-group {
              width: ${scaleSpacePx(60)};
              margin-left: ${scaleSpacePx(30)};
            }
          }
        }

        .avatar {
          display: flex;
          align-items: center;
          justify-content: center;
          width: ${scaleSpacePx(16)};
          height: ${scaleSpacePx(16)};
          margin: 0 ${space.m} 0  16px;
          border-radius: ${radius.circle};
          background-color: ${background['01']};

          > p {
            font-size: ${scaleFontSizePx(12)};
            line-height: ${scaleSpacePx(5)};
            color: ${foreground['02']}
          }
        }
      }
    `;
  }}
`;
