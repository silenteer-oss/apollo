import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  RouteProps,
} from 'react-router-dom';
import moment from 'moment';
import i18next from 'i18next';
import { Dialog, Toaster } from '@blueprintjs/core';
import { ErrorBoundary, LoadingState } from '@design-system/core/components';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import { getI18nInitOptions } from '@careprovider/i18n';
import {
  GlobalContextProvider,
  GlobalContextConsumer,
} from '@careprovider/context';
import {
  getApplicationInitial,
  IGlobalInitial,
} from '@careprovider/services/biz';
import {
  ThemeProvider,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
} from '@careprovider/theme';
import { IRootPageProps, IRootPageState } from './models';
import { IAuthorizedRouteProps, RouterProcessor } from './components';

export function withRootPage(params: {
  EnsureSupportedBrowser?: React.FunctionComponent;
  AuthorizedRoute: React.ComponentClass<IAuthorizedRouteProps & RouteProps>;
  AppRoutePage: React.FunctionComponent;
  LoginPage: React.FunctionComponent;
  TrustedDevicePage: React.FunctionComponent;
}): React.ComponentClass<IRootPageProps, IRootPageState> {
  return class RootPage extends React.PureComponent<
    IRootPageProps,
    IRootPageState
  > {
    private _apiErrorRedirect = {
      401: '/devices',
      403: '/login',
    };
    state: IRootPageState = {};

    componentDidMount() {
      getApplicationInitial().then(appInitial => this.setState({ appInitial }));
    }

    render() {
      const { className } = this.props;
      const { theme } = this.props;
      const { isInitedI18n, appInitial } = this.state;
      const { EnsureSupportedBrowser } = params;

      return (
        <div className={className}>
          <I18nContextProvider
            {...getI18nInitOptions()}
            onInitedI18n={this._handleInitedI18n}
            onChangedLanguage={this._handleChagedLanguage}
          >
            <ThemeProvider theme={theme}>
              <GlobalStyleContextProvider
                styles={{
                  main: mixGlobalStyle(theme),
                  blueprint: mixCustomBlueprintStyle(theme),
                }}
              >
                {(!appInitial || !isInitedI18n) && <LoadingState />}
                {appInitial && isInitedI18n && EnsureSupportedBrowser && (
                  <EnsureSupportedBrowser>
                    {this._renderRouters(appInitial)}
                  </EnsureSupportedBrowser>
                )}
                {appInitial &&
                  isInitedI18n &&
                  !EnsureSupportedBrowser &&
                  this._renderRouters(appInitial)}
              </GlobalStyleContextProvider>
            </ThemeProvider>
          </I18nContextProvider>
        </div>
      );
    }

    private _renderRouters = (appInitial: IGlobalInitial) => {
      const {
        AuthorizedRoute,
        AppRoutePage,
        LoginPage,
        TrustedDevicePage,
      } = params;

      return (
        <GlobalContextProvider
          getInitialState={this._getInitialState}
          userProfile={appInitial.userProfile}
        >
          <GlobalContextConsumer>
            {({ userProfile }) => (
              <ErrorBoundary
                user={{
                  userId: userProfile.id,
                  careProviderId: userProfile.careProvideId,
                }}
                sentry={appInitial.configs.sentry}
                Dialog={Dialog}
                Toaster={Toaster}
                apiErrorRedirect={this._apiErrorRedirect}
              >
                <Router>
                  <RouterProcessor
                    apiErrorRedirect={this._apiErrorRedirect}
                    status={appInitial.status}
                  >
                    <Route path="/login" exact component={LoginPage} />
                    <Route
                      path="/devices"
                      exact
                      component={TrustedDevicePage}
                    />
                    <AuthorizedRoute path="/app" component={AppRoutePage} />
                    <Redirect exact path="/" to="/app" />
                  </RouterProcessor>
                </Router>
              </ErrorBoundary>
            )}
          </GlobalContextConsumer>
        </GlobalContextProvider>
      );
    };

    private _getInitialState = () => {
      getApplicationInitial().then(appInitial => this.setState({ appInitial }));
    };

    private _handleInitedI18n = () => {
      moment.locale(i18next.language);
      this.setState({ isInitedI18n: true });
    };

    private _handleChagedLanguage = (newLanguage: string) => {
      moment.locale(newLanguage);
    };
  };
}
