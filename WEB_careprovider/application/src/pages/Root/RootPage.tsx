import { lazyComponent } from '@design-system/core/components';
import { AppRoutePage } from '../AppRoute';
import { LoginPage } from '../Login';
import { EnsureSupportedBrowser, AuthorizedRoute } from './components';
import { withRootPage } from './withRootPage';

const TrustedDevicePage = lazyComponent(
  import('../TrustedDevice/TrustedDevice.styled')
);

export const OriginRootPage = withRootPage({
  EnsureSupportedBrowser,
  AuthorizedRoute,
  AppRoutePage,
  LoginPage,
  TrustedDevicePage,
});
