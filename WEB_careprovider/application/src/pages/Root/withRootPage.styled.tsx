import React from 'react';
import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IRootPageProps } from './models';

export function withStyledRootPage(
  component: React.ComponentClass<IRootPageProps>
) {
  return styled(component).attrs(({ className }) => ({
    className: getCssClass('sl-RootPage', className),
  }))<IRootPageProps>`
    ${() => {
      return `
        & {
          width: 100%;
          height: 100%;
        }
      `;
    }}
  `;
}
