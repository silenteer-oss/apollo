import { OriginRootPage } from './RootPage';
import { withStyledRootPage } from './withRootPage.styled';

export const RootPage = withStyledRootPage(OriginRootPage);
