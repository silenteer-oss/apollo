import { IGlobalInitial } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IRootPageProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface IRootPageState {
  appInitial?: IGlobalInitial;
  isInitedI18n?: boolean;
}
