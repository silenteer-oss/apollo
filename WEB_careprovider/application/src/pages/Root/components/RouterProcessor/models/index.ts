import type { ReactNode } from 'react';

export interface IRouterProcessorProps {
  apiErrorRedirect: { [key: number]: string };
  status: number;
  children?: ReactNode;
}
