import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { IRouterProcessorProps } from './models';

export const RouterProcessor = React.memo((props: IRouterProcessorProps) => {
  const { status, apiErrorRedirect } = props;

  if (status) {
    const path = apiErrorRedirect[status];

    if (path && location.pathname.toLowerCase() !== path) {
      return (
        <Switch {...props}>
          <Redirect exact path={location.pathname} to={path} />
          {props.children}
        </Switch>
      );
    }
    if (
      status === 200 &&
      (location.pathname.toLowerCase() === apiErrorRedirect[401] ||
        location.pathname.toLowerCase() === apiErrorRedirect[403])
    ) {
      return (
        <Switch {...props}>
          <Redirect exact path={location.pathname} to="/app" />
          {props.children}
        </Switch>
      );
    }
  }
  return <Switch {...props}>{props.children}</Switch>;
});
