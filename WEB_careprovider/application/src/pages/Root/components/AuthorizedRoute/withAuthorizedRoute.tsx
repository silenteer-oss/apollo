import React from 'react';
import { RouteProps, Route, Redirect } from 'react-router-dom';
import { ICryptoKeyPairSchema } from '@infrastructure/crypto/services/client-database';
import { IAuthorizedRouteProps, IAuthorizedRouteState } from './models';

export function withAuthorizedRoute(params: {
  getCareProviderKeyPair: () => Promise<ICryptoKeyPairSchema>;
}): React.ComponentClass<IAuthorizedRouteProps & RouteProps> {
  return class AuthorizedRoute extends React.PureComponent<
    IAuthorizedRouteProps & RouteProps,
    IAuthorizedRouteState
  > {
    state: IAuthorizedRouteState = {
      hasAlreadyPaired: undefined,
    };

    componentDidMount() {
      params.getCareProviderKeyPair().then(data => {
        this.setState({ hasAlreadyPaired: !!data && !!data.value });
      });
    }

    render() {
      const { path } = this.props;
      const { hasAlreadyPaired } = this.state;

      if (hasAlreadyPaired === undefined) {
        return null;
      }

      let redirect = path;

      if (!hasAlreadyPaired) {
        redirect = '/devices';
      }

      if (redirect === path) {
        return <Route {...this.props} />;
      }

      return <Redirect path={path} to={redirect} />;
    }
  };
}
