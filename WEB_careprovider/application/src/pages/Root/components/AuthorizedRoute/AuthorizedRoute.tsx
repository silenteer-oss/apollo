import { getCareProviderKeyPairFromIndexDB } from '@careprovider/services/client-database';
import { withAuthorizedRoute } from './withAuthorizedRoute';

export const AuthorizedRoute = withAuthorizedRoute({
  getCareProviderKeyPair: getCareProviderKeyPairFromIndexDB,
});
