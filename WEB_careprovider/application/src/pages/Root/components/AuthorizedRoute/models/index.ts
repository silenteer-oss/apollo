export interface IAuthorizedRouteProps {
  path: string;
}

export interface IAuthorizedRouteState {
  hasAlreadyPaired: boolean;
}
