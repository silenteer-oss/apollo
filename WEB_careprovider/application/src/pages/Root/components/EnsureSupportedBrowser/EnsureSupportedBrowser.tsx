import React from 'react';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { EnsureSupportedBrowser as CoreEnsureSupportedBrowser } from '@design-system/core/components';
import { setupBrowser } from '@infrastructure/browser';
import { getApplicationConfig } from '../../../../../application-config';
import { NAMESPACE_ENSURE_SUPPORTED_BROWSER, i18nKeys } from './i18n';

class OriginEnsureSupportedBrowser extends React.PureComponent<
  {} & II18nFixedNamespaceContext
> {
  render() {
    const { t } = this.props;

    return (
      <CoreEnsureSupportedBrowser
        features={['indexDB', 'storageManager']}
        unsupportedBrowserMessages={{
          title: t(i18nKeys.unsupportBrowserTitle),
          content: t(i18nKeys.unsupportBrowserContent),
        }}
        deniedPermissionMessages={{
          title: t(i18nKeys.deniedPermissionTitle),
          content: t(i18nKeys.deniedPermissionContent),
        }}
        onRequestPermission={setupBrowser}
      >
        {this.props.children}
      </CoreEnsureSupportedBrowser>
    );
  }
}

export const EnsureSupportedBrowser = withI18nContext(
  OriginEnsureSupportedBrowser,
  {
    namespace: NAMESPACE_ENSURE_SUPPORTED_BROWSER,
    publicAssetPath: getApplicationConfig().publicAssetPath,
  }
);
