import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './ensure-supported-browser.json';

export const NAMESPACE_ENSURE_SUPPORTED_BROWSER = 'ensure-supported-browser';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
