import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { IPatientPageProps } from './models';
import { MicroPatientListView } from '../../micro-modules';
import { MicroAppointmentDetailsView } from '../../micro-modules';
import { IGlobalContext, withGlobalContext } from '@careprovider/context';
import { IAppointmentDetailsViewProps } from '@careprovider/modules/appointment';

class OriginPatientPage extends React.PureComponent<
  IPatientPageProps & IGlobalContext & RouteComponentProps<any>
> {
  render() {
    const { className } = this.props;

    return (
      <div className={className}>
        <MicroPatientListView
          onUpdatedPatient={this._updatePatientProfile}
          renderSetAppointmentView={(props: IAppointmentDetailsViewProps) => (
            <MicroAppointmentDetailsView {...props} />
          )}
        />
      </div>
    );
  }

  private _updatePatientProfile = (id: string): void => {
    this.props.updatePatientProfiles([id]);
  };
}

export const PatientPage = withRouter(withGlobalContext(OriginPatientPage));
