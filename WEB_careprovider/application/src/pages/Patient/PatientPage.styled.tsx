import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IPatientPageProps } from './models';
import { PatientPage as OriginPatientPage } from './PatientPage';

export default styled(OriginPatientPage).attrs(({ className }) => ({
  className: getCssClass('sl-PatientPage', className),
}))<IPatientPageProps>`
  ${props => {
    const { background } = props.theme;
    return `
      & {
        width: 100%;
        height: 100%;

        .sl-PatientListView {
          .sl-LoadingState {
            position: absolute;
            width: auto;
            height: auto;
            top: 80px;
            left: 88px;
            right: 24px;
            bottom: 0;
            background-color: ${setAlpha(background.white, 0.5)};
          }
        }
      }
    `;
  }}
`;
