import { ICareProviderTheme } from '@careprovider/theme';

export interface IPatientPageProps {
  className?: string;
  theme?: ICareProviderTheme;
  history: any;
}
