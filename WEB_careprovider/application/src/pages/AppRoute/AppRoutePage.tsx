import React from 'react';
import {
  Route,
  Switch,
  NavLink,
  Redirect,
  RouteComponentProps,
} from 'react-router-dom';
import { Subject, Subscription, ReplaySubject } from 'rxjs';
import moment from 'moment';
import { Tooltip } from '@blueprintjs/core';
import {
  Flex,
  lazyComponent,
  Svg,
  LoadingState,
} from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { AppointmentApiModel } from '@infrastructure/resource/AppointmentResource';
import {
  PATIENT_PAGE_PATH,
  APPOINTMENT_PAGE_PATH,
  CONVERSATION_PAGE_PATH,
  CONVERSATION_DETAIL_PAGE_PATH,
  COMMUNICATION_PLAN_PAGE_PATH,
  COMMUNICATION_PLAN_DETAIL_PAGE_PATH,
  PATIENT_PAGE_PARING_PAGE_PATH,
} from '@careprovider/route';
import {
  UserType,
  IAppointmentChangeLog,
  IDecryptedChatMessage,
  getPatientProfiles,
  IEncryptedChatMessage,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';
import {
  startCommonSocket$,
  stopCommonSocket,
  getEmployeeChanged$ as listenEmployeeChanged$,
} from '@careprovider/services/websocket';
import {
  ensureCareProviderDeviceReady,
  ensureSenderKeysReady,
  decryptMessage,
} from '@careprovider/services/webworker';
import { withGlobalContext, IGlobalContext } from '@careprovider/context';
import { IAppoinmentChangeLogViewProps } from '@careprovider/modules/appointment';
import {
  listenChatSocket$,
  listenAppointmentChangeLogSocket$,
  getConversation,
  listenCommunicationPlanSocket$,
} from '@careprovider/modules/conversation';
import {
  MicroAppointmentModuleProvider,
  MicroConversationModuleProvider,
  MicroAppointmenChangeLogView,
  MicroCommunicationPlanModuleProvider,
} from '../../micro-modules';
import { getApplicationConfig } from '../../../application-config';
import { IAppRoutePageProps, IAppRoutePageState } from './models';
import { logout, getConversationUnreadMessages } from './services';
import { ProfileButtonMenu } from './components';
import { NAMESPACE_APP_ROUTE_PAGE, i18nKeys } from './i18n';

import ConversationLogoActive from '@careprovider/assets/images/conversation-logo-active.svg';
import ConversationLogoActiveUnreadMessage from '@careprovider/assets/images/conversation-logo-active-unread-message.svg';
import ConversationLogoInactive from '@careprovider/assets/images/conversation-logo-inactive.svg';
import ConversationLogoInactiveUnreadMessage from '@careprovider/assets/images/conversation-logo-inactive-unread-message.svg';
import AppointmentLogo from '@careprovider/assets/images/appointment-logo-active.svg';
import AppointmentLogoInactive from '@careprovider/assets/images/appointment-logo-inactive.svg';
import PatientManagementLogo from '@careprovider/assets/images/patient-management-logo-active.svg';
import PatientManagementLogoInactive from '@careprovider/assets/images/patient-management-logo-inactive.svg';
import CommunicationPlanLogo from '@careprovider/assets/images/communication-plan-logo-active.svg';
import CommunicationPlanLogoInactive from '@careprovider/assets/images/communication-plan-logo-inactive.svg';
import CareProviderDeskNotiLogo from '@careprovider/assets/images/desk-notification-logo.jpg';

moment.locale('vi'); // TODO replace this hardcoded locale once we have i18n and user can pick desired locale
const LazyPatientPage = lazyComponent(import('../Patient/PatientPage.styled'));

const LazyAppointmentPage = lazyComponent(
  import('../Appointment/AppointmentPage.styled')
);
const LazyConversationPage = lazyComponent(
  import('../Conversation/ConversationPageStyled'),
  <LoadingState />
);
const LazyCommunicationPlanPage = lazyComponent(
  import('../CommunicationPlan/CommunicationPlanPage.styled')
);
const LazyCommunicationPlanTimelinePage = lazyComponent(
  import('../CommunicationPlanTimeline/CommunicationPlanTimelinePage.styled')
);

class OriginAppRoutePage extends React.PureComponent<
  IAppRoutePageProps &
    IGlobalContext &
    RouteComponentProps &
    II18nFixedNamespaceContext,
  IAppRoutePageState
> {
  private _communicationPlanSubscription: Subscription;
  private _communicationPlanReplaySubject: ReplaySubject<
    IProcessedCommunicationPlanMessage
  >;
  private _chatSubscription: Subscription;
  private _chatReplaySubject: ReplaySubject<IDecryptedChatMessage>;
  private _chatReplaySubjectSubscription: Subscription;
  private _appointmentChangeLogReplaySubjectSubscription: Subscription;
  private _appointmentChangeLogSubscription: Subscription;
  private _appointmentChangeLogReplaySubject: ReplaySubject<
    IAppointmentChangeLog
  >;
  private _reconnectedSubject = new Subject<void>();
  private _reconnected$ = this._reconnectedSubject.asObservable();
  private _commonSocketSubscription: Subscription;
  private _employeeChangedSubscription: Subscription;

  private get _isActiveConversationPage(): boolean {
    return window.location.pathname.indexOf(CONVERSATION_PAGE_PATH) === 0;
  }

  private get _isActiveAppointmentPage(): boolean {
    return window.location.pathname.indexOf(APPOINTMENT_PAGE_PATH) === 0;
  }

  private get _isActivePatientManagementPage(): boolean {
    return window.location.pathname.indexOf(PATIENT_PAGE_PATH) === 0;
  }

  private get _isActiveCommunicationPlanPage(): boolean {
    return window.location.pathname.indexOf(COMMUNICATION_PLAN_PAGE_PATH) === 0;
  }

  state: IAppRoutePageState = {
    unreadMessageConversationIdMap: {},
  };

  componentDidUpdate(prevProps: IAppRoutePageProps & IGlobalContext) {
    // This case will run if user has just loged in
    if (!prevProps.userProfile?.id && !!this.props.userProfile?.id) {
      this._init();
    }
  }

  componentDidMount() {
    if (this.props.userProfile?.id) {
      this._init();
    }
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  render() {
    const {
      className,
      userProfile,
      getEmployeeProfiles,
      getPatientProfiles,
      t,
    } = this.props;

    return (
      <Flex className={className}>
        <MicroAppointmentModuleProvider
          getEmployeeProfiles={getEmployeeProfiles}
        >
          <MicroConversationModuleProvider
            getEmployeeProfiles={getEmployeeProfiles}
            getPatientProfiles={getPatientProfiles}
          >
            <MicroCommunicationPlanModuleProvider
              getEmployeeProfiles={getEmployeeProfiles}
            >
              <Flex className="sl-navbar" column>
                <Flex column align="center">
                  <ProfileButtonMenu
                    className="sl-nav-item"
                    accountId={userProfile ? userProfile.accountId : ''}
                    userName={userProfile ? userProfile.fullName : ''}
                    userType={userProfile ? userProfile.type : UserType.DOCTOR}
                    onLogout={() => this.handleLogout()}
                  />
                  <NavLink className="sl-nav-item" to={CONVERSATION_PAGE_PATH}>
                    <Tooltip
                      content={t(i18nKeys.conversationTooltip)}
                      position="right"
                    >
                      <Svg src={this.getConversationLogo()} />
                    </Tooltip>
                  </NavLink>
                  <NavLink className="sl-nav-item" to={APPOINTMENT_PAGE_PATH}>
                    <Tooltip
                      content={t(i18nKeys.appointmentTooltip)}
                      position="right"
                    >
                      <Svg src={this.getAppointmentLogo()} />
                    </Tooltip>
                  </NavLink>
                  <NavLink className="sl-nav-item" to={PATIENT_PAGE_PATH}>
                    <Tooltip
                      content={t(i18nKeys.patientTooltip)}
                      position="right"
                    >
                      <Svg src={this.getPatientManagementLogo()} />
                    </Tooltip>
                  </NavLink>
                  <NavLink
                    className="sl-nav-item"
                    to={COMMUNICATION_PLAN_PAGE_PATH}
                  >
                    <Tooltip
                      content={t(i18nKeys.communicationPlanTooltip)}
                      position="right"
                    >
                      <Svg src={this.getCommunicationPlanLogo()} />
                    </Tooltip>
                  </NavLink>
                </Flex>
              </Flex>
              <Flex auto>
                <Switch>
                  <Route
                    path={CONVERSATION_PAGE_PATH}
                    exact
                    component={this.renderConversationPage}
                  />
                  <Route
                    path={CONVERSATION_DETAIL_PAGE_PATH}
                    exact
                    component={this.renderConversationPage}
                  />
                  <Route
                    path={APPOINTMENT_PAGE_PATH}
                    exact
                    component={this.renderAppointmentPage}
                  />
                  <Route
                    path={COMMUNICATION_PLAN_PAGE_PATH}
                    exact
                    component={this.renderCommunicationPlanPage}
                  />
                  <Route
                    path={COMMUNICATION_PLAN_DETAIL_PAGE_PATH}
                    exact
                    component={this.renderCommunicationPlanTimelinePage}
                  />
                  <Route
                    path={PATIENT_PAGE_PARING_PAGE_PATH}
                    exact
                    component={this.renderPatientManagementPage}
                  />
                  <Route
                    exact
                    path={PATIENT_PAGE_PATH}
                    component={this.renderPatientManagementPage}
                  />
                  <Redirect exact path="/app" to={PATIENT_PAGE_PATH} />
                  {/* Removes trailing slashes */}
                  <Route
                    path="/:url*(/+)"
                    exact
                    strict
                    render={({ location }) => (
                      <Redirect to={location.pathname.replace(/\/+$/, '')} />
                    )}
                  />
                  {/* Removes duplicate slashes in the middle of the URL */}
                  <Route
                    path="/:url(.*//+.*)"
                    exact
                    strict
                    render={({ match }) => (
                      <Redirect
                        to={`/${match.params.url.replace(/\/\/+/, '/')}`}
                      />
                    )}
                  />
                </Switch>
              </Flex>
            </MicroCommunicationPlanModuleProvider>
          </MicroConversationModuleProvider>
        </MicroAppointmentModuleProvider>
      </Flex>
    );
  }

  getConversationLogo = () => {
    const { unreadMessageConversationIdMap } = this.state;
    const hasUnreadMessage =
      Object.keys(unreadMessageConversationIdMap).length > 0;

    const isActiveConversationPage = this._isActiveConversationPage;

    if (isActiveConversationPage) {
      if (hasUnreadMessage) {
        return ConversationLogoActiveUnreadMessage;
      }
      return ConversationLogoActive;
    }
    if (hasUnreadMessage) {
      return ConversationLogoInactiveUnreadMessage;
    }
    return ConversationLogoInactive;
  };

  getAppointmentLogo = () => {
    return this._isActiveAppointmentPage
      ? AppointmentLogo
      : AppointmentLogoInactive;
  };

  getPatientManagementLogo = () => {
    return this._isActivePatientManagementPage
      ? PatientManagementLogo
      : PatientManagementLogoInactive;
  };

  getCommunicationPlanLogo = () => {
    return this._isActiveCommunicationPlanPage
      ? CommunicationPlanLogo
      : CommunicationPlanLogoInactive;
  };

  renderConversationPage = () => {
    const { unreadMessageConversationIdMap } = this.state;

    return (
      <LazyConversationPage
        unreadMessageConversationIdMap={unreadMessageConversationIdMap}
        chat$={this._chatReplaySubject}
        communicationPlan$={this._communicationPlanReplaySubject}
        appointmentChangeLog$={this._appointmentChangeLogReplaySubject}
        reconnected$={this._reconnected$}
        seenConversationUnreadMessages={this._deleteConversationUnreadMessages}
        renderAppointmentChangeLog={this._renderAppointmentChangeLog}
      />
    );
  };

  renderAppointmentPage = () => {
    return (
      <LazyAppointmentPage
        appointmentChangeLog$={this._appointmentChangeLogReplaySubject}
        reconnected$={this._reconnected$}
      />
    );
  };

  renderPatientManagementPage = () => {
    return <LazyPatientPage history={this.props.history} />;
  };

  renderCommunicationPlanPage = () => {
    return <LazyCommunicationPlanPage />;
  };

  renderCommunicationPlanTimelinePage = () => {
    return <LazyCommunicationPlanTimelinePage />;
  };

  private _renderAppointmentChangeLog = (
    props: IAppoinmentChangeLogViewProps
  ): React.ReactElement => {
    return <MicroAppointmenChangeLogView {...props} />;
  };

  askNotificationPermission = () => {
    if (Notification && Notification.permission === 'default') {
      Notification.requestPermission();
    }
  };

  sendDesktopNotification = (body: string) => {
    const iconUrl = CareProviderDeskNotiLogo;
    let notification = new Notification(`Zoop`, {
      body: body,
      icon: iconUrl,
    });
    notification.onclick = function() {
      parent.focus();
      window.focus(); // just in case, older browsers
      this.close();
    };
    setTimeout(notification.close.bind(notification), 5000);
  };

  sendMesageDesktopNotification = async (message: IEncryptedChatMessage) => {
    const conversation = await getConversation(message.conversationId);
    const patientProfiles = await getPatientProfiles([conversation.patientId]);
    const body = this.props.t(i18nKeys.newChatMessageNotification, {
      patientName: patientProfiles[0].fullName,
    });
    this.sendDesktopNotification(body);
  };

  sendAppointmentDesktopNotification = async (
    message: IAppointmentChangeLog
  ) => {
    const patientProfiles = await getPatientProfiles([message.patientId]);
    const body = `${patientProfiles[0].fullName} ${
      message.action === AppointmentApiModel.ActionType.CONFIRM
        ? 'confirmed'
        : 'cancel'
    }`;
    this.sendDesktopNotification(body);
  };

  handleLogout = () => {
    this._unsubscribe();
    logout().then(() => {
      this.props.setUserProfile(null);
      this.props.history.push('/login');
    });
  };

  private _checkUnreadMessageFromConversations = () => {
    getConversationUnreadMessages().then(conversationIds => {
      this.setState(
        {
          unreadMessageConversationIdMap: conversationIds.reduce(
            (result, conversationId) => {
              result[conversationId] = true;
              return result;
            },
            {}
          ),
        },
        () => this._updateFavicon()
      );
    });
  };

  private _setConversationUnreadMessages = (
    conversationIds: string[]
  ): void => {
    this.setState(
      prevState => ({
        unreadMessageConversationIdMap: {
          ...prevState.unreadMessageConversationIdMap,
          ...conversationIds.reduce((result, conversationId) => {
            result[conversationId] = true;
            return result;
          }, {}),
        },
      }),
      () => this._updateFavicon()
    );
  };

  private _deleteConversationUnreadMessages = (
    conversationIds: string[]
  ): void => {
    this.setState(
      prevState => ({
        unreadMessageConversationIdMap: {
          ...conversationIds.reduce(
            (result, conversationId) => {
              delete result[conversationId];
              return result;
            },
            { ...prevState.unreadMessageConversationIdMap }
          ),
        },
      }),
      () => this._updateFavicon()
    );
  };

  private _updateFavicon = () => {
    const _currentLink: HTMLLinkElement = document.querySelector(
      'link[rel*="shortcut icon"]'
    );
    if (_currentLink) {
      const _hasUnreadMessages =
        Object.keys(this.state.unreadMessageConversationIdMap).length > 0;
      const _linkHref = _hasUnreadMessages
        ? '/favicon-dot.ico'
        : '/favicon.ico';

      if (_currentLink.href.split('/').pop() !== _linkHref.split('/').pop()) {
        const _link = document.createElement('link');
        _link.rel = 'shortcut icon';
        _link.href = _linkHref;
        document.head.removeChild(_currentLink);
        document.head.appendChild(_link);
      }
    }
  };

  private _init = () => {
    this._checkUnreadMessageFromConversations();

    this.askNotificationPermission();

    this._commonSocketSubscription = startCommonSocket$().subscribe(
      ({ status }) => {
        if (status === 'RECONNECTED') {
          this._reconnectedSubject.next();
        }
      }
    );

    // chat
    this._chatReplaySubject = new ReplaySubject<IDecryptedChatMessage>();
    this._chatReplaySubjectSubscription = this._chatReplaySubject.subscribe(
      message => {
        if (message) {
          this._setConversationUnreadMessages([message.conversationId]);

          if (!this._isActiveConversationPage) {
            this.sendMesageDesktopNotification(message);
          }
        }
      }
    );
    this._chatSubscription = listenChatSocket$({
      ensureCareProviderDeviceReadyHandler: ensureCareProviderDeviceReady,
      ensureSenderKeysReadyHandler: ensureSenderKeysReady,
      decryptMessageHandler: decryptMessage,
    }).subscribe(this._chatReplaySubject);

    // appointment
    this._appointmentChangeLogReplaySubject = new ReplaySubject<
      IAppointmentChangeLog
    >();
    this._appointmentChangeLogReplaySubjectSubscription = this._appointmentChangeLogReplaySubject.subscribe(
      message => {
        if (
          message.changeMakerId === message.patientId &&
          (message.action === AppointmentApiModel.ActionType.CONFIRM ||
            message.action === AppointmentApiModel.ActionType.CANCEL) &&
          Notification.permission === 'granted'
        ) {
          this.sendAppointmentDesktopNotification(message);
        }
      }
    );
    this._appointmentChangeLogSubscription = listenAppointmentChangeLogSocket$().subscribe(
      this._appointmentChangeLogReplaySubject
    );

    //communication plan
    this._communicationPlanReplaySubject = new ReplaySubject<
      IProcessedCommunicationPlanMessage
    >();
    this._communicationPlanSubscription = listenCommunicationPlanSocket$().subscribe(
      this._communicationPlanReplaySubject
    );

    //employee changed
    this._employeeChangedSubscription = listenEmployeeChanged$().subscribe(
      () => {
        this.props.reloadEmployeeProfiles();
      }
    );
  };

  private _unsubscribe = () => {
    stopCommonSocket();

    if (this._commonSocketSubscription) {
      this._commonSocketSubscription.unsubscribe();
      this._commonSocketSubscription = undefined;
    }
    if (this._reconnectedSubject) {
      this._reconnectedSubject.complete();
      this._reconnectedSubject = undefined;
    }
    this._chatSubscription && this._chatSubscription.unsubscribe();
    this._chatReplaySubject && this._chatReplaySubject.unsubscribe();
    this._chatReplaySubjectSubscription &&
      this._chatReplaySubjectSubscription.unsubscribe();

    this._appointmentChangeLogSubscription &&
      this._appointmentChangeLogSubscription.unsubscribe();
    this._appointmentChangeLogReplaySubject &&
      this._appointmentChangeLogReplaySubject.unsubscribe();
    this._appointmentChangeLogReplaySubjectSubscription &&
      this._appointmentChangeLogReplaySubjectSubscription.unsubscribe();

    this._communicationPlanSubscription &&
      this._communicationPlanSubscription.unsubscribe();
    this._communicationPlanReplaySubject &&
      this._communicationPlanReplaySubject.unsubscribe();

    this._employeeChangedSubscription &&
      this._employeeChangedSubscription.unsubscribe();
  };
}

export const AppRoutePage = withGlobalContext(
  withI18nContext(OriginAppRoutePage, {
    namespace: NAMESPACE_APP_ROUTE_PAGE,
    publicAssetPath: getApplicationConfig().publicAssetPath,
  })
);
