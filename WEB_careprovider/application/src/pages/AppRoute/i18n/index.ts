import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './app-route-page.json';

export const NAMESPACE_APP_ROUTE_PAGE = 'app-route-page';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
