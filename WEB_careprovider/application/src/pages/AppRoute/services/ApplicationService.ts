import { AuthenticationApi } from '@infrastructure/resource/AuthenticationResource';
import { ChatApi } from '@infrastructure/resource/ChatAppResource';

export function logout() {
  return AuthenticationApi.careProviderLogout();
}

export function getConversationUnreadMessages(): Promise<string[]> {
  return ChatApi.getUnreadConversationIds().then(response => response.data);
}
