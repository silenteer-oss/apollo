import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './profile-button-menu.json';

export const NAMESPACE_PROFILE_BUTTON_MENU = 'profile-button-menu';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
