import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IProfileButtonMenuProps } from './models';
import { ProfileButtonMenu as OriginProfileButtonMenu } from './ProfileButtonMenu';

export const ProfileButtonMenu = styled(OriginProfileButtonMenu).attrs(
  ({ className }) => ({
    className: getCssClass('sl-ProfileButtonMenu', className),
  })
)<IProfileButtonMenuProps>`
  ${props => {
    const { background, radius } = props.theme;

    return `
      & {
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        cursor: pointer;

        .bp3-popover-target {
          display: flex;
        }

        .sl-avatar {
          border: 2px solid ${background.white};
          border-radius: ${radius.circle};
        }

        .sl-arrow {
          position: absolute;
          bottom: 0;
          right: ${scaleSpacePx(2)};
          width: 0;
          height: 0;
          border-left: ${scaleSpacePx(1)} solid transparent;
          border-right: ${scaleSpacePx(1)} solid transparent;
          border-top: ${scaleSpacePx(1)} solid ${background.white};
        }
      }
    `;
  }}
`;
