import React, { useState, useRef } from 'react';
import {
  Dialog,
  Menu,
  MenuItem,
  Popover,
  Toaster,
  Tooltip,
  Alert,
  Intent,
} from '@blueprintjs/core';
import {
  IGlobalStyleContext,
  withGlobalStyleContext,
} from '@design-system/themes/context';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { UserType } from '@careprovider/services/biz';
import { withTheme } from '@careprovider/theme';
import { getApplicationConfig } from '../../../../../application-config';
import { ChangePasswordPage } from '../../../password';
import { IProfileButtonMenuProps } from './models';
import { mixGlobalStyle } from './styles';
import { NAMESPACE_PROFILE_BUTTON_MENU, i18nKeys } from './i18n';

import DoctorAvatar from '@careprovider/assets/images/default-doctor-avatar.png';
import NurseAvatar from '@careprovider/assets/images/default-nurse-avatar.png';
import ReceptionistAvatar from '@careprovider/assets/images/default-receptionist-avatar.png';

const OriginProfileButtonMenu = (
  props: IProfileButtonMenuProps &
    IGlobalStyleContext &
    II18nFixedNamespaceContext
) => {
  const [open, setOpen] = useState(false);
  const [isLogout, setLogout] = useState(false);
  const toasterRef = useRef<Toaster>(null);
  const { t } = props;
  const onCloseChangePasswordDialog = () => setOpen(false);
  const onShowChangePasswordDialog = () => setOpen(true);
  const onCloseLogoutDialog = () => setLogout(false);
  const onShowLogoutDialog = () => setLogout(true);
  const onChangePassword = () => {
    setOpen(false);
    toasterRef.current.show({
      message: t(i18nKeys.updatedPasswordMessage),
      intent: 'success',
      icon: 'info-sign',
    });
  };

  let avatarUrl;
  switch (props.userType) {
    case UserType.DOCTOR:
      avatarUrl = DoctorAvatar;
      break;
    case UserType.NURSE:
      avatarUrl = NurseAvatar;
      break;
    case UserType.RECEPTIONIST:
      avatarUrl = ReceptionistAvatar;
      break;
  }

  props.addGlobalStyle('profileButtonMenu', mixGlobalStyle(props.theme));

  return (
    <div className={props.className}>
      <Dialog
        icon="info-sign"
        onClose={onCloseChangePasswordDialog}
        title={t(i18nKeys.title)}
        isOpen={open}
        canOutsideClickClose={false}
      >
        <ChangePasswordPage
          accountId={props.accountId}
          onChange={onChangePassword}
        />
      </Dialog>
      <Alert
        icon="log-out"
        cancelButtonText={t(i18nKeys.alertCancelButton)}
        confirmButtonText={t(i18nKeys.alertSignOutButton)}
        intent={Intent.DANGER}
        isOpen={isLogout}
        onConfirm={() => props.onLogout()}
        onCancel={onCloseLogoutDialog}
      >
        <p>{t(i18nKeys.alertMessage)}</p>
      </Alert>
      <Popover
        popoverClassName="sl-ProfileButtonMenu"
        content={
          <Menu>
            <MenuItem
              text={t(i18nKeys.changePasswordOption)}
              onClick={onShowChangePasswordDialog}
            />
            <MenuItem
              className="sl-logout"
              text={t(i18nKeys.signOutOption)}
              onClick={onShowLogoutDialog}
            />
          </Menu>
        }
      >
        <Tooltip content={props.userName} position="right">
          <img className="sl-avatar" src={avatarUrl} />
        </Tooltip>
      </Popover>
      <Toaster ref={toasterRef} />
      <i className="sl-arrow" />
    </div>
  );
};

export const ProfileButtonMenu = withTheme(
  withGlobalStyleContext(
    withI18nContext(OriginProfileButtonMenu, {
      namespace: NAMESPACE_PROFILE_BUTTON_MENU,
      publicAssetPath: getApplicationConfig().publicAssetPath,
    })
  )
);
