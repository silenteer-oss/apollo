import { ICareProviderTheme } from '@careprovider/theme';
import { scaleSpacePx } from '@design-system/core/styles';

export function mixGlobalStyle(theme: ICareProviderTheme): string {
  return `
    .sl-ProfileButtonMenu {
      .sl-logout {
        .bp3-text-overflow-ellipsis.bp3-fill {
          color: ${theme.foreground.error.base};
        }
      }
    }
    .sl-Svg {
      width: ${scaleSpacePx(6)};
      height: ${scaleSpacePx(6)};
      cursor: pointer;
    }
  `;
}
