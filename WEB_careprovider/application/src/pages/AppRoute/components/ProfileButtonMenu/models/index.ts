import { UserType } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IProfileButtonMenuProps {
  className?: string;
  theme?: ICareProviderTheme;
  userType: UserType;
  userName: string;
  accountId: string;
  onLogout: () => void;
}
