import { ICareProviderTheme } from '@careprovider/theme';

export interface IAppRoutePageProps {
  className?: string;
  theme?: ICareProviderTheme;
}

export interface IAppRoutePageState {
  unreadMessageConversationIdMap: { [id: string]: boolean };
}
