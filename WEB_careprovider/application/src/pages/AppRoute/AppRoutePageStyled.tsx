import { getCssClass } from '@design-system/infrastructure/utils';
import { scaleSpacePx } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IAppRoutePageProps } from './models';
import { AppRoutePage as OriginAppRoutePage } from './AppRoutePage';

export const AppRoutePage = styled(OriginAppRoutePage).attrs(
  ({ className }) => ({
    className: getCssClass('sl-AppRoutePage', className),
  })
)<IAppRoutePageProps>`
  ${props => {
    const { space, foreground } = props.theme;

    return `
      & {
        width: 100%;
        height: 100%;

        .sl-navbar {
          min-width: ${scaleSpacePx(16)};
          padding: ${space.s} 0;
          color: ${foreground.white};
          background-color: ${foreground['01']};

          .sl-nav-item {
            + .sl-nav-item {
              margin-top: ${scaleSpacePx(10)};
            }

            .sl-Svg > svg {
              width: ${scaleSpacePx(6)};
              height: ${scaleSpacePx(6)};
              cursor: pointer;
            }
          }
        }
      }
    `;
  }}
`;
