import React from 'react';
import { ITrustedDevicePageProps } from './models';
import { MicroTrustedDeviceView } from '../../micro-modules';

export class TrustedDevicePage extends React.PureComponent<
  ITrustedDevicePageProps
> {
  render() {
    const { className } = this.props;

    return (
      <div className={className}>
        <MicroTrustedDeviceView />
      </div>
    );
  }
}
