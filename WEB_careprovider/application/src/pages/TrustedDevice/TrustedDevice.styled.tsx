import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { ITrustedDevicePageProps } from './models';
import { TrustedDevicePage as OriginTrustedDevicePage } from './TrustedDevicePage';

export default styled(OriginTrustedDevicePage).attrs(({ className }) => ({
  className: getCssClass('sl-TrustedDevicePage', className),
}))<ITrustedDevicePageProps>`
  ${() => {
    return `
      & {
        width: 50%;
        height: 100%;
        margin: auto;
      }
    `;
  }}
`;
