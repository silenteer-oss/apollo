import { ReactElement } from 'react';
import { Observable } from 'rxjs';
import {
  IAppointmentChangeLog,
  IPatientProfile,
  IDecryptedChatMessage,
  IProcessedCommunicationPlanMessage,
} from '@careprovider/services/biz';
import { IAppoinmentChangeLogViewProps } from '@careprovider/modules/appointment';
import { IConversationItem } from '@careprovider/modules/conversation';
import { ICareProviderTheme } from '@careprovider/theme';

export interface IConversationPageProps {
  className?: string;
  theme?: ICareProviderTheme;
  unreadMessageConversationIdMap: { [id: string]: boolean };
  chat$: Observable<IDecryptedChatMessage>;
  communicationPlan$: Observable<IProcessedCommunicationPlanMessage>;
  appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  reconnected$: Observable<void>;
  seenConversationUnreadMessages: (conversationId: string[]) => void;
  renderAppointmentChangeLog: (
    props: IAppoinmentChangeLogViewProps
  ) => ReactElement;
}

export interface IConversationPageState {
  selectedConversation: IConversationItem;
  selectedPatientInfo: IPatientProfile;
}
