import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Divider } from '@blueprintjs/core';
import {
  H1,
  H3,
  BodyTextM,
  Flex,
  ErrorState,
  Svg,
} from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { IPatientProfile } from '@careprovider/services/biz';
import { IGlobalContext, withGlobalContext } from '@careprovider/context';
import {
  ensureCareProviderDeviceReady,
  ensureSenderKeysReady,
  decryptMessage,
  encryptMessage,
} from '@careprovider/services/webworker';
import { Header, Collapse } from '@careprovider/components';
import { dateTimeFormat } from '@careprovider/services/util';
import { IConversationItem } from '@careprovider/modules/conversation';
import { getApplicationConfig } from '../../../application-config';
import {
  MicroConversationListView,
  MicroChatBoxView,
  MicroUpcomingAppointmentView,
  MicroCommunicationPlanPatientView,
} from '../../micro-modules';
import { withTheme } from '@careprovider/theme';
import { IConversationPageProps, IConversationPageState } from './models';
import { NAMESPACE_CONVERSATION_PAGE, i18nKeys } from './i18n';
import PatientBirthdayIcon from '@careprovider/assets/images/patient-birthday-icon.svg';
import PatientGenderIcon from '@careprovider/assets/images/patient-gender-icon.svg';
import PatientPhoneIcon from '@careprovider/assets/images/patient-phone-icon.svg';

class OriginConversationPage extends React.PureComponent<
  IConversationPageProps &
    IGlobalContext &
    RouteComponentProps<any> &
    II18nFixedNamespaceContext,
  IConversationPageState
> {
  state: IConversationPageState = {
    selectedConversation: undefined,
    selectedPatientInfo: undefined,
  };

  render() {
    const {
      className,
      theme: { foreground },
      unreadMessageConversationIdMap,
      chat$,
      appointmentChangeLog$,
      reconnected$,
      communicationPlan$,
      renderAppointmentChangeLog,
      history,
      match,
      t,
      userProfile,
    } = this.props;
    const { selectedConversation, selectedPatientInfo } = this.state;
    const {
      params: { patientId },
    } = match;

    return (
      <div className={className}>
        <div className="sl-container-left">
          <Header>
            <H1>{t(i18nKeys.conversationListTitle)}</H1>
          </Header>
          <MicroConversationListView
            chat$={chat$}
            unreadMessageConversationIdMap={unreadMessageConversationIdMap}
            selectedPatientId={patientId}
            selectedConversation={selectedConversation}
            onSelectConversation={this.selectConversation}
          />
        </div>
        <Flex column auto className="sl-container-right">
          {!selectedConversation && (
            <ErrorState
              title={t(i18nKeys.noSelectedPatientStateTitle)}
              content={t(i18nKeys.noSelectedPatientStateContent)}
            />
          )}
          {selectedConversation && (
            <>
              <Header>
                <div className="sl-patient-info">
                  <div>
                    <H3>
                      {selectedPatientInfo && selectedPatientInfo.fullName}
                    </H3>
                    <Flex>
                      <Svg src={PatientBirthdayIcon} />
                      <BodyTextM color={foreground['02']} margin={'0 24px 0 0'}>
                        {selectedPatientInfo &&
                          this.renderPatientBirthday(selectedPatientInfo.dob)}
                      </BodyTextM>
                      <Svg src={PatientGenderIcon} />
                      <BodyTextM color={foreground['02']} margin={'0 24px 0 0'}>
                        {selectedPatientInfo &&
                          selectedPatientInfo.gender === 'MALE' &&
                          t(i18nKeys.maleGender)}
                        {selectedPatientInfo &&
                          selectedPatientInfo.gender === 'FEMALE' &&
                          t(i18nKeys.femaleGender)}
                        {selectedPatientInfo &&
                          selectedPatientInfo.gender === 'OTHERS' &&
                          t(i18nKeys.otherGender)}
                      </BodyTextM>
                      <Svg src={PatientPhoneIcon} />
                      <BodyTextM color={foreground['02']}>
                        {selectedPatientInfo && selectedPatientInfo.phone}
                      </BodyTextM>
                    </Flex>
                  </div>
                </div>
              </Header>
              <Flex grow={1}>
                <MicroChatBoxView
                  userProfile={userProfile}
                  conversation={selectedConversation}
                  chat$={chat$}
                  appointmentChangeLog$={appointmentChangeLog$}
                  reconnected$={reconnected$}
                  ensureCareProviderDeviceReady={ensureCareProviderDeviceReady}
                  ensureSenderKeysReady={ensureSenderKeysReady}
                  encryptMessage={encryptMessage}
                  decryptMessage={decryptMessage}
                  renderAppointmentChangeLog={renderAppointmentChangeLog}
                  communicationPlanMessage$={communicationPlan$}
                  history={history}
                />
                <div className="sl-conversation-right-panel">
                  {selectedConversation && this.renderRightPanel()}
                </div>
              </Flex>
            </>
          )}
        </Flex>
      </div>
    );
  }

  renderPatientBirthday = (birthday: number) => {
    const birthDateTime = new Date(birthday);
    const currentDateTime = new Date();
    const yearDifference =
      currentDateTime.getFullYear() - birthDateTime.getFullYear();
    let monthDifference =
      currentDateTime.getMonth() -
      birthDateTime.getMonth() +
      12 * yearDifference;
    const dayInMonthDifference =
      currentDateTime.getDate() - birthDateTime.getDate();
    if (monthDifference === 0) {
      monthDifference++;
    }
    let ageString = '';
    if (monthDifference === 24) {
      if (dayInMonthDifference < 0) {
        ageString = this.props.t(i18nKeys.ageMonth, {
          number: monthDifference,
        });
      } else {
        ageString = this.props.t(i18nKeys.age, {
          number: yearDifference,
        });
      }
    } else if (monthDifference < 24) {
      ageString = this.props.t(i18nKeys.ageMonth, {
        number: monthDifference,
      });
    } else {
      if (currentDateTime.getMonth() < birthDateTime.getMonth()) {
        ageString = this.props.t(i18nKeys.age, {
          number: yearDifference - 1,
        });
      } else {
        ageString = this.props.t(i18nKeys.age, {
          number: yearDifference,
        });
      }
    }
    return `${dateTimeFormat(birthday, 'L')} - ${ageString}`;
  };

  renderRightPanel() {
    const { appointmentChangeLog$, reconnected$, t, match } = this.props;
    const { selectedConversation } = this.state;
    const {
      params: { patientId },
    } = match;

    return (
      <Flex column>
        <Collapse title={t(i18nKeys.apppointmentTitle)}>
          <Flex className="sl-upcoming-appointment ">
            <MicroUpcomingAppointmentView
              appointmentChangeLog$={appointmentChangeLog$}
              reconnected$={reconnected$}
              patientId={selectedConversation.patientInfo.id}
            />
          </Flex>
        </Collapse>
        <Divider />
        <Collapse title={t(i18nKeys.formTitle)}>
          <Flex className="sl-form">
            <BodyTextM
              padding={'0 0 8px 0'}
              color={this.props.theme.foreground['02']}
            >
              {t(i18nKeys.formInProgressStatus)}
            </BodyTextM>
            <BodyTextM color={this.props.theme.foreground['01']}>
              {t(i18nKeys.formDescription)}
            </BodyTextM>
          </Flex>
        </Collapse>
        <Divider />
        <Collapse title={t(i18nKeys.communicationPlanTitle)}>
          <Flex className="sl-communication-plan">
            <MicroCommunicationPlanPatientView patientId={patientId} />
          </Flex>
        </Collapse>
        <Divider />
      </Flex>
    );
  }

  selectConversation = (
    conversationItem: IConversationItem,
    selectedPatientInfo: IPatientProfile
  ) => {
    const { history, seenConversationUnreadMessages } = this.props;
    history.replace(`/app/conversation/${conversationItem.patientInfo.id}`);

    seenConversationUnreadMessages([conversationItem.conversationId]);

    this.setState({
      selectedConversation: conversationItem,
      selectedPatientInfo,
    });
  };
}

export const ConversationPage = withTheme(
  withRouter(
    withGlobalContext(
      withI18nContext(OriginConversationPage, {
        namespace: NAMESPACE_CONVERSATION_PAGE,
        publicAssetPath: getApplicationConfig().publicAssetPath,
      })
    )
  )
);
