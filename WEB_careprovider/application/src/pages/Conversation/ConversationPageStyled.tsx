import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { scaleSpacePx, scaleSpace } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IConversationPageProps } from './models';
import { ConversationPage as OriginConversationPage } from './ConversationPage';

export default styled(OriginConversationPage).attrs(({ className }) => ({
  className: getCssClass('sl-ConversationPage', className),
}))<IConversationPageProps>`
  ${props => {
    const { background, space, foreground } = props.theme;
    const chatboxClientHeight = scaleSpace(10) + scaleSpace(2) * 2 + 1; //1px = top border
    const patientInfoClientHeight = scaleSpace(20) + 1; //1px = bottom border
    const pageHeaderHeight = scaleSpace(20);
    const searchBoxHeight = scaleSpace(14);

    return `
      & {
        display: flex;
        height: 100%;
        width: 100%;

        .sl-container-left {
          display: flex;
          flex-flow: column;
          height: 100%;
          min-width: ${scaleSpacePx(80)};
          max-width: ${scaleSpacePx(80)};
          border-right: 1px solid ${background['01']};
          background-color: ${setAlpha(background.primary.lighter, 0.5)};

          h1 {
            display: flex;
            align-items: center;
          }

          .sl-conversation-list {
            flex: 1;
            overflow-x: auto;
            height: calc(100vh - ${pageHeaderHeight}px - ${searchBoxHeight}px);
            max-height: calc(100vh - ${pageHeaderHeight}px - ${searchBoxHeight}px);
          }
        }

        .sl-container-right {
          .sl-Header {
            border-bottom: 1px solid ${background['01']};
          }

          .sl-patient-info {
            display: flex;
            justify-content: space-between;
            h3 {
              margin-bottom: ${space.xs};
            }

            .sl-Svg {
              width: ${scaleSpacePx(3)};
              height: ${scaleSpacePx(3)};
              cursor: auto;
              margin-right: ${space.xs};
            }

            .sl-divider {
              margin: 0 ${space.xs};
              border-left: 1px solid ${background['02']};
            }
          }

          .sl-ChatBoxView .sl-messages {
            height: calc(100vh - ${patientInfoClientHeight}px - ${chatboxClientHeight}px);
            max-height: calc(100vh - ${patientInfoClientHeight}px - ${chatboxClientHeight}px);
          }

          .sl-conversation-right-panel {
            display: flex;
            flex-flow: column;
            min-width: ${scaleSpacePx(95)};
            max-width: ${scaleSpacePx(95)};
            border-left: 1px solid ${background['01']};
            background-color: ${background.white};
            max-height: calc(100vh - ${patientInfoClientHeight}px);
            overflow: auto;

            .sl-collapse-header {
              justify-content: space-between;
              padding: ${space.xs} ${space.m};
              &:hover {
                cursor: pointer;
              }
            }

            .sl-upcoming-appointment {
              display: flex;
              flex-flow: column;
              justify-content: space-between;
              padding: ${space.xs} ${space.m} ${space.s} ${space.m};
              max-height: 500px;

              .bp3-button-text {
                color: ${foreground['01']} !important;
                font-weight: 600;
              }
            }

            .sl-form {
              display: flex;
              flex-flow: column;
              justify-content: space-between;
              padding: 0 ${space.m} ${space.s} ${space.m};
            }

            .sl-communication-plan {
              display: flex;
            }
          }
        }
      }
    `;
  }}
`;
