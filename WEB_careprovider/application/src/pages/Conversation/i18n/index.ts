import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './conversation-page.json';

export const NAMESPACE_CONVERSATION_PAGE = 'conversation-page';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
