import { ICareProviderTheme } from '@careprovider/theme';
import { IProfile } from '@careprovider/modules/patient-management';

export interface IChangePasswordPageProps {
  className?: string;
  theme?: ICareProviderTheme;
  onChange?: () => void;
  accountId?: string;
}
