import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '@careprovider/theme';
import { IChangePasswordPageProps } from './model';
import ChangePasswordPage from './ChangePassword.page';

export default styled(ChangePasswordPage).attrs(({ className }) => ({
  className: getCssClass('sl-change-password-page', className),
}))<IChangePasswordPageProps>`
  ${() => {
    return `
      & {
      }
    `;
  }}
`;
