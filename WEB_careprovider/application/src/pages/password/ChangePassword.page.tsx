import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { withGlobalContext, IGlobalContext } from '@careprovider/context';
import { hashPassword } from '@careprovider/services/webworker';
import { MicroChangePasswordModule } from '../../micro-modules';
import { IChangePasswordPageProps } from './model';

class ChangePasswordPage extends React.PureComponent<
  IChangePasswordPageProps & IGlobalContext & RouteComponentProps<any>
> {
  render() {
    const { className, onChange, accountId } = this.props;

    return (
      <div className={className}>
        <MicroChangePasswordModule
          onChange={onChange}
          accountId={accountId}
          hashPassword={hashPassword}
        />
      </div>
    );
  }
}

export default withRouter(withGlobalContext(ChangePasswordPage));
