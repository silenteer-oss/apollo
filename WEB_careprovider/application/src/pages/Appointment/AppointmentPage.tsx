import React from 'react';
import moment from 'moment';
import { Flex, H1, H3 } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  now,
  dateTimeFormat,
  startOf,
  endOf,
} from '@careprovider/services/util';
import { withTheme } from '@careprovider/theme';
import {
  TAppointmentStatus,
  UNASSIGNED_DOCTOR_ID,
} from '@careprovider/modules/appointment';
import { Header } from '@careprovider/components';
import { getApplicationConfig } from '../../../application-config';
import {
  MicroAppointmentDetailsView,
  MicroAppointmentFilterView,
  MicroAppointmentColumnListView,
} from '../../micro-modules';
import { IAppointmentPageProps, IAppointmentPageState } from './models';
import { NAMESPACE_APPOINTMENT_PAGE, i18nKeys } from './i18n';

class OriginAppointmentPage extends React.PureComponent<
  IAppointmentPageProps & II18nFixedNamespaceContext,
  IAppointmentPageState
> {
  state: IAppointmentPageState = {
    startDate: now(),
    autoRefreshTime: undefined,
  };

  render() {
    const { className, appointmentChangeLog$, reconnected$, t } = this.props;
    const {
      startDate,
      includeUnassigned,
      filteredDoctorIds,
      includeConfirmed,
      includePending,
      autoRefreshTime,
    } = this.state;

    return (
      <Flex className={className}>
        <Flex className="filter-panel" column>
          <Header>
            <Flex align="center">
              <H1>{t(i18nKeys.title)}</H1>
            </Flex>
          </Header>
          <MicroAppointmentFilterView
            onStatusSelected={this.onStatusSelected}
            onDateSelected={this.onDateSelected}
            onDoctorsSelected={this.onDoctorsSelected}
          />
        </Flex>
        <div className="result-panel">
          <Header>
            <Flex align="center" justify="space-between">
              <H3>{dateTimeFormat(startDate, 'LL')}</H3>
              <MicroAppointmentDetailsView
                appointmentDateTime={{
                  value: moment(startDate).toDate(),
                  hourAndMinuteSpecified: false,
                }}
                onSubmit={this.forceUpdateList}
              />
            </Flex>
          </Header>
          <MicroAppointmentColumnListView
            autoRefreshTime={autoRefreshTime}
            from={startOf(startDate, 'day')
              .clone()
              .valueOf()}
            to={endOf(startDate, 'day')
              .clone()
              .valueOf()}
            filteredDoctorIds={filteredDoctorIds}
            includeConfirmed={includeConfirmed}
            includePending={includePending}
            includeUnassigned={includeUnassigned}
            appointmentChangeLog$={appointmentChangeLog$}
            reconnected$={reconnected$}
          />
        </div>
      </Flex>
    );
  }

  onDateSelected = (date: number) => {
    this.setState({ startDate: date });
  };

  onDoctorsSelected = (doctorIds: string[]) => {
    let includeUnassigned = doctorIds.includes(UNASSIGNED_DOCTOR_ID);
    if (includeUnassigned) {
      doctorIds.splice(doctorIds.indexOf(UNASSIGNED_DOCTOR_ID), 1);
    }
    this.setState({ filteredDoctorIds: doctorIds, includeUnassigned });
  };

  onStatusSelected = (status: TAppointmentStatus[]) => {
    let includeConfirmed = status.includes('Confirmed');
    let includePending = status.includes('Pending');
    this.setState({ includeConfirmed, includePending });
  };

  forceUpdateList = () => {
    // change the startDate (exact to ms) to force update
    this.setState({
      autoRefreshTime: now(),
    });
  };
}

export const AppointmentPage = withTheme(
  withI18nContext(OriginAppointmentPage, {
    namespace: NAMESPACE_APPOINTMENT_PAGE,
    publicAssetPath: getApplicationConfig().publicAssetPath,
  })
);
