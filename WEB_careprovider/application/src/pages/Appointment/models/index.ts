import { IAppointmentChangeLog } from '@careprovider/services/biz';
import { ICareProviderTheme } from '@careprovider/theme';
import { Observable } from 'rxjs';

export interface IAppointmentPageProps {
  className?: string;
  theme?: ICareProviderTheme;
  appointmentChangeLog$: Observable<IAppointmentChangeLog>;
  reconnected$: Observable<void>;
}

export interface IAppointmentPageState {
  filteredDoctorIds?: string[];
  startDate: number;
  autoRefreshTime?: number;
  includePending?: boolean;
  includeConfirmed?: boolean;
  includeUnassigned?: boolean;
}
