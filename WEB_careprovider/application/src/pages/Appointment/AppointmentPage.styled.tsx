import { getCssClass, setAlpha } from '@design-system/infrastructure/utils';
import { scaleSpacePx, scaleSpace } from '@design-system/core/styles';
import { styled } from '@careprovider/theme';
import { IAppointmentPageProps } from './models';
import { AppointmentPage as OriginAppointmentPage } from './AppointmentPage';

export default styled(OriginAppointmentPage).attrs(({ className }) => ({
  className: getCssClass('sl-AppointmentPage', className),
}))<IAppointmentPageProps>`
  ${props => {
    const { background } = props.theme;
    const navbarClientWidth = scaleSpace(16);
    const filterPanelClientWidth = scaleSpace(80);
    const appointmentColumnListViewPaddingX = scaleSpace(6) * 2;
    const headerClientHeight = scaleSpace(20);

    return `
      & {
        width: 100%;
        height: 100%;

        .filter-panel {
          width: ${scaleSpacePx(80)};
          background-color: ${setAlpha(background.primary.lighter, 0.5)};

          .sl-AppointmentFilterView {
            .filter-group {
              .doctors-list {
                max-height: calc(100vh - ${scaleSpacePx(129)});
                overflow: auto;
              }
            }
          }
        }

        .result-panel {
          flex:1 ;

          .sl-LoadingState {
            position: absolute;
            width: auto;
            height: auto;
            top: 80px;
            bottom: 0;
            left: 408px;
            right: 24px;
            background-color: ${setAlpha(background.white, 0.5)};
          }

          .sl-AppointmentColumnListView {
            max-width: calc(100vw - ${navbarClientWidth}px - ${filterPanelClientWidth}px - ${appointmentColumnListViewPaddingX}px);
            max-height: calc(100vh - ${headerClientHeight}px);
            width: 100%;
            height: 100%;
            overflow: auto;
          }
        }
      }
    `;
  }}
`;
