import { getPropertyAsKeyMap } from '@design-system/infrastructure/utils';
import i18nSource from './appointment-page.json';

export const NAMESPACE_APPOINTMENT_PAGE = 'appointment-page';

export const i18nKeys = getPropertyAsKeyMap(i18nSource);
