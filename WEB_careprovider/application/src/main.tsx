import React from 'react';
import ReactDOM from 'react-dom';
import {
  theme,
  getAllViewTheme,
  getAllComponentTheme,
} from '@careprovider/theme';
import { RootPage } from './pages/Root';

import '@careprovider/assets/styles/index.scss';

theme.setComponentTheme(getAllComponentTheme);
theme.setViewTheme(getAllViewTheme);

ReactDOM.render(<RootPage theme={theme} />, document.getElementById('root'));
