import React from 'react';
import {
  loadMicroModuleView,
  loadMicroModuleProvider,
  loadMicroModuleConsumer,
} from '@design-system/core/components';
import type {
  IAppointmentContext,
  IAppoinmentChangeLogViewProps,
  IAppointmentDetailsViewProps,
  IUpcomingAppointmentProps,
  IAppointmentFilterProps,
  ColumnListViewProps,
  AppointmentContextProviderProps,
} from '@careprovider/modules/appointment';
import {
  APPOINTMENT_CHANGE_LOG_VIEW_ID,
  APPOINTMENT_DETAILS_VIEW_ID,
  UPCOMING_APPOINTMENT_VIEW_ID,
  APPOINTMENT_FILTER_VIEW_ID,
  APPOINTMENT_COLUMN_LIST_VIEW_ID,
  APPOINTMENT_CONTEXT_PROVIDER_ID,
  APPOINTMENT_CONTEXT_CONSUMER_ID,
} from '@careprovider/modules/appointment/module-config';
import type {
  IConversationContext,
  IConversationContextProviderProps,
  IConversationListViewProps,
  IChatBoxViewProps,
} from '@careprovider/modules/conversation';
import {
  CONVERSATION_CONTEXT_PROVIDER_ID,
  CONVERSATION_CONTEXT_CONSUMER_ID,
  CONVERSATION_LIST_VIEW_ID,
  CHAT_BOX_VIEW_ID,
} from '@careprovider/modules/conversation/module-config';
import type {
  ICommunicationPlanContext,
  ICommunicationPlanContextProviderProps,
  ICommunicationPlanListViewProps,
} from '@careprovider/modules/communication-plan';
import {
  COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID,
  COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID,
  COMMUNICATION_PLAN_LIST_VIEW_ID,
  COMMUNICATION_PLAN_TIMELINE_VIEW_ID,
  COMMUNICATION_PLAN_PATIENT_VIEW_ID,
} from '@careprovider/modules/communication-plan/module-config';
import { withTheme } from '@careprovider/theme';
import type { IChangePasswordViewProps } from '@careprovider/modules/password';
import { CHANGE_PASSWORD_VIEW_ID } from '@careprovider/modules/password/module-config';
import type { IPatientListProps } from '@careprovider/modules/patient-management';
import { PATIENT_LIST_VIEW_ID } from '@careprovider/modules/patient-management/module-config';
import type { ITrustedDeviceViewProps } from '@careprovider/modules/trusted-device';
import { TRUSTED_DEVICE_VIEW_ID } from '@careprovider/modules/trusted-device/module-config';
import { getApplicationConfig } from '../application-config';
import type { ICommunicationPlanTimelineViewProps } from 'modules/communication-plan/src/views/communication-plan-timeline';
import type { ICommunicationPlanPatientViewProps } from 'modules/communication-plan/src/views';

const {
  modules: {
    AppointmentModule,
    ConversationModule,
    ChangePasswordModule,
    PatientManagementModule,
    TrustedDeviceModule,
    CommunicationPlanModule,
  },
} = getApplicationConfig();

export const MicroAppointmentModuleProvider = (
  props: AppointmentContextProviderProps & {
    children: React.ReactNode;
  }
) => {
  const Provider = loadMicroModuleProvider({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    provider: AppointmentModule.providers[APPOINTMENT_CONTEXT_PROVIDER_ID],
  });
  return <Provider {...props} />;
};

export const MicroAppointmentModuleConsumer = (props: {
  children: (value: IAppointmentContext) => React.ReactNode;
}) => {
  const Consumer = loadMicroModuleConsumer<IAppointmentContext>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    consumer: AppointmentModule.consumers[APPOINTMENT_CONTEXT_CONSUMER_ID],
  });
  return <Consumer {...props} />;
};

export const MicroAppointmenChangeLogView = withTheme<
  React.FunctionComponent<IAppoinmentChangeLogViewProps>
>(props => {
  const VIEW = loadMicroModuleView<IAppoinmentChangeLogViewProps>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    view: AppointmentModule.views[APPOINTMENT_CHANGE_LOG_VIEW_ID],
  });
  return <VIEW {...props} />;
});

export const MicroAppointmentDetailsView = withTheme<
  React.FunctionComponent<IAppointmentDetailsViewProps>
>(props => {
  const VIEW = loadMicroModuleView<IAppointmentDetailsViewProps>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    view: AppointmentModule.views[APPOINTMENT_DETAILS_VIEW_ID],
  });
  return <VIEW {...props} />;
});

export const MicroUpcomingAppointmentView = withTheme(
  loadMicroModuleView<IUpcomingAppointmentProps>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    view: AppointmentModule.views[UPCOMING_APPOINTMENT_VIEW_ID],
  })
);

export const MicroAppointmentFilterView = withTheme<
  React.FunctionComponent<IAppointmentFilterProps>
>(props => {
  const VIEW = loadMicroModuleView<IAppointmentFilterProps>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    view: AppointmentModule.views[APPOINTMENT_FILTER_VIEW_ID],
  });
  return <VIEW {...props} />;
});

export const MicroAppointmentColumnListView = withTheme<
  React.FunctionComponent<ColumnListViewProps>
>(props => {
  const VIEW = loadMicroModuleView<ColumnListViewProps>({
    url: AppointmentModule.publicAssetPath,
    module: AppointmentModule.id,
    view: AppointmentModule.views[APPOINTMENT_COLUMN_LIST_VIEW_ID],
  });
  return <VIEW {...props} />;
});
//--------------------------------------------------------------------//

export const MicroPatientListView = withTheme<
  React.FunctionComponent<IPatientListProps>
>(props => {
  const View = loadMicroModuleView<IPatientListProps>({
    url: PatientManagementModule.publicAssetPath,
    module: PatientManagementModule.id,
    view: PatientManagementModule.views[PATIENT_LIST_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//

export const MicroConversationModuleProvider = (
  props: IConversationContextProviderProps & {
    children: React.ReactNode;
  }
) => {
  const Provider = loadMicroModuleProvider({
    url: ConversationModule.publicAssetPath,
    module: ConversationModule.id,
    provider: ConversationModule.providers[CONVERSATION_CONTEXT_PROVIDER_ID],
  });
  return <Provider {...props} />;
};

export const MicroConversationModuleConsumer = (props: {
  children: (value: IConversationContext) => React.ReactNode;
}) => {
  const Consumer = loadMicroModuleConsumer<IConversationContext>({
    url: ConversationModule.publicAssetPath,
    module: ConversationModule.id,
    consumer: ConversationModule.consumers[CONVERSATION_CONTEXT_CONSUMER_ID],
  });
  return <Consumer {...props} />;
};

export const MicroConversationListView = withTheme<
  React.FunctionComponent<IConversationListViewProps>
>(props => {
  const View = loadMicroModuleView<IConversationListViewProps>({
    url: ConversationModule.publicAssetPath,
    module: ConversationModule.id,
    view: ConversationModule.views[CONVERSATION_LIST_VIEW_ID],
  });

  return <View {...props} />;
});

export const MicroChatBoxView = withTheme<
  React.FunctionComponent<IChatBoxViewProps>
>(props => {
  const View = loadMicroModuleView<IChatBoxViewProps>({
    url: ConversationModule.publicAssetPath,
    module: ConversationModule.id,
    view: ConversationModule.views[CHAT_BOX_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//

export const MicroCommunicationPlanModuleProvider = (
  props: ICommunicationPlanContextProviderProps & {
    children: React.ReactNode;
  }
) => {
  const Provider = loadMicroModuleProvider({
    url: CommunicationPlanModule.publicAssetPath,
    module: CommunicationPlanModule.id,
    provider:
      CommunicationPlanModule.providers[COMMUNICATION_PLAN_CONTEXT_PROVIDER_ID],
  });
  return <Provider {...props} />;
};

export const MicroCommunicationPlanModuleConsumer = (props: {
  children: (value: ICommunicationPlanContext) => React.ReactNode;
}) => {
  const Consumer = loadMicroModuleConsumer<ICommunicationPlanContext>({
    url: CommunicationPlanModule.publicAssetPath,
    module: CommunicationPlanModule.id,
    consumer:
      CommunicationPlanModule.consumers[COMMUNICATION_PLAN_CONTEXT_CONSUMER_ID],
  });
  return <Consumer {...props} />;
};

export const MicroCommunicationPlanListView = withTheme<
  React.FunctionComponent<ICommunicationPlanListViewProps>
>(props => {
  const View = loadMicroModuleView<ICommunicationPlanListViewProps>({
    url: CommunicationPlanModule.publicAssetPath,
    module: CommunicationPlanModule.id,
    view: CommunicationPlanModule.views[COMMUNICATION_PLAN_LIST_VIEW_ID],
  });

  return <View {...props} />;
});

export const MicroCommunicationPlanTimelineView = withTheme<
  React.FunctionComponent<ICommunicationPlanTimelineViewProps>
>(props => {
  const View = loadMicroModuleView<ICommunicationPlanTimelineViewProps>({
    url: CommunicationPlanModule.publicAssetPath,
    module: CommunicationPlanModule.id,
    view: CommunicationPlanModule.views[COMMUNICATION_PLAN_TIMELINE_VIEW_ID],
  });

  return <View {...props} />;
});

export const MicroCommunicationPlanPatientView = withTheme<
  React.FunctionComponent<ICommunicationPlanPatientViewProps>
>(props => {
  const View = loadMicroModuleView<ICommunicationPlanPatientViewProps>({
    url: CommunicationPlanModule.publicAssetPath,
    module: CommunicationPlanModule.id,
    view: CommunicationPlanModule.views[COMMUNICATION_PLAN_PATIENT_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//

export const MicroTrustedDeviceView = withTheme<
  React.FunctionComponent<ITrustedDeviceViewProps>
>(props => {
  const View = loadMicroModuleView<ITrustedDeviceViewProps>({
    url: TrustedDeviceModule.publicAssetPath,
    module: TrustedDeviceModule.id,
    view: TrustedDeviceModule.views[TRUSTED_DEVICE_VIEW_ID],
  });

  return <View {...props} />;
});

//--------------------------------------------------------------------//

export const MicroChangePasswordModule = withTheme(props => {
  const View = loadMicroModuleView<IChangePasswordViewProps>({
    url: ChangePasswordModule.publicAssetPath,
    module: ChangePasswordModule.id,
    view: ChangePasswordModule.views[CHANGE_PASSWORD_VIEW_ID],
  });

  return <View {...props} />;
});
