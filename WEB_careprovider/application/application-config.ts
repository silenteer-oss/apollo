import {
  IApplicationConfig,
  IModuleConfig,
} from '../../../LIB_design-system/infrastructure/configs/environment';
import {
  APPOINTMENT_MODULE_ID,
  IModuleViews as IAppointmentModuleViews,
  IModuleProviders as IAppointmentModuleProviders,
  IModuleConsumers as IAppointmentModuleConsumers,
  getModuleConfig as getAppointmentModuleConfig,
} from '../modules/appointment/module-config';
import {
  CONVERSATION_MODULE_ID,
  IModuleViews as IConversationModuleViews,
  IModuleProviders as IConversationModuleProviders,
  IModuleConsumers as IConversationModuleConsumers,
  getModuleConfig as getConversationModuleConfig,
} from '../modules/conversation/module-config';
import {
  CHANGE_PASSWORD_MODULE_ID,
  IModuleViews as IChangePasswordModuleViews,
  getModuleConfig as getChangePasswordModuleConfig,
} from '../modules/password/module-config';
import {
  PATIENT_MANAGEMENT_MODULE_ID,
  IModuleViews as IPatientManagementModuleViews,
  getModuleConfig as getPatientManagementModuleConfig,
} from '../modules/patient-management/module-config';
import {
  TRUSTED_DEVICE_MODULE_ID,
  IModuleViews as ITrustedDeviceModuleViews,
  getModuleConfig as getTrustedDeviceModuleConfig,
} from '../modules/trusted-device/module-config';
import {
  COMMUNICATION_PLAN_MODULE_ID,
  IModuleViews as ICommunicationPlanModuleViews,
  IModuleProviders as ICommunicationPlanModuleProviders,
  IModuleConsumers as ICommunicationPlanModuleConsumers,
  getModuleConfig as getCommunicationPlanModuleConfig,
} from '../modules/communication-plan/module-config';

export interface IApplicationModules {
  [APPOINTMENT_MODULE_ID]: IModuleConfig<
    IAppointmentModuleViews,
    IAppointmentModuleProviders,
    IAppointmentModuleConsumers
  >;
  [CONVERSATION_MODULE_ID]: IModuleConfig<
    IConversationModuleViews,
    IConversationModuleProviders,
    IConversationModuleConsumers
  >;
  [CHANGE_PASSWORD_MODULE_ID]: IModuleConfig<IChangePasswordModuleViews>;
  [PATIENT_MANAGEMENT_MODULE_ID]: IModuleConfig<IPatientManagementModuleViews>;
  [TRUSTED_DEVICE_MODULE_ID]: IModuleConfig<ITrustedDeviceModuleViews>;
  [COMMUNICATION_PLAN_MODULE_ID]: IModuleConfig<
    ICommunicationPlanModuleViews,
    ICommunicationPlanModuleProviders,
    ICommunicationPlanModuleConsumers
  >;
}

export function getApplicationConfig(): IApplicationConfig<
  IApplicationModules
> {
  return {
    id: 'Care Provider Application',
    name: 'Care Provider Application',
    host: '0.0.0.0',
    port: 12345,
    publicAssetPath: `/`,
    modules: {
      AppointmentModule: getAppointmentModuleConfig(),
      ConversationModule: getConversationModuleConfig(),
      ChangePasswordModule: getChangePasswordModuleConfig(),
      PatientManagementModule: getPatientManagementModuleConfig(),
      TrustedDeviceModule: getTrustedDeviceModuleConfig(),
      CommunicationPlanModule: getCommunicationPlanModuleConfig(),
    },
  };
}
