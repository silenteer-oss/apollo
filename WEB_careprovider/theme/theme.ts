import originStyled, {
  css as originCss,
  createGlobalStyle as originCreateGlobalStyle,
  withTheme as originWithTheme,
  ThemeProvider as OriginThemeProvider,
  ThemeConsumer as OriginThemeConsumer,
  keyframes,
  ThemedBaseStyledInterface,
  BaseThemedCssFunction,
  BaseWithThemeFnInterface,
  ThemeProviderProps,
  ThemedStyledProps,
  Interpolation,
  CSSObject,
  InterpolationFunction,
  GlobalStyleComponent,
} from 'styled-components';
import {
  IDefaultTheme,
  BaseDefaultTheme,
} from '@infrastructure/themes/default';

export { getAllComponentTheme } from '@infrastructure/themes/default';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IViewsTheme {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICareProviderTheme extends IDefaultTheme<IViewsTheme> {}

export class CareProviderTheme extends BaseDefaultTheme<IViewsTheme> {}

// -------------------------------------------------------------------- //

export function getAllViewTheme(theme: ICareProviderTheme): IViewsTheme {
  return {};
}

// -------------------------------------------------------------------- //

const styled = originStyled as ThemedBaseStyledInterface<ICareProviderTheme>;
const css = originCss as BaseThemedCssFunction<ICareProviderTheme>;
const withTheme = originWithTheme as BaseWithThemeFnInterface<
  ICareProviderTheme
>;
const createGlobalStyle = originCreateGlobalStyle as <P extends object = {}>(
  first:
    | TemplateStringsArray
    | CSSObject
    | InterpolationFunction<ThemedStyledProps<P, ICareProviderTheme>>,
  ...interpolations: Array<
    Interpolation<ThemedStyledProps<P, ICareProviderTheme>>
  >
) => GlobalStyleComponent<P, ICareProviderTheme>;
const ThemeProvider = OriginThemeProvider as React.ComponentClass<
  ThemeProviderProps<ICareProviderTheme, ICareProviderTheme>,
  any
>;
const ThemeConsumer = OriginThemeConsumer as React.ExoticComponent<
  React.ConsumerProps<ICareProviderTheme>
>;

export {
  styled,
  css,
  createGlobalStyle,
  keyframes,
  withTheme,
  ThemeProvider,
  ThemeConsumer,
};

export const theme = new CareProviderTheme();
