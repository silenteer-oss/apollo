import { DateInputType, InclusivityType } from './models';
import moment from 'moment';

export const dateTimeFormat = (
  date: DateInputType,
  format: string,
  locale = 'vi'
) => {
  if (!date) {
    return '-';
  }
  return moment(date)
    .locale(locale)
    .format(format);
};

export const utc = (date: DateInputType) => {
  return +moment.utc(date);
};

export const now = () => moment.now();

export const startOf = (
  date: DateInputType,
  unitOfTime: moment.unitOfTime.StartOf
) => moment(date).startOf(unitOfTime);

export const strToDate = (str: string, format: string) =>
  moment(str, format).toDate();

export const endOf = (
  date: DateInputType,
  unitOfTime: moment.unitOfTime.StartOf
) => moment(date).endOf(unitOfTime);

export const dateToMoment = (date: DateInputType, format?: string) =>
  moment(date, format);

export const isBefore = (date: DateInputType, comparedDate: DateInputType) => {
  return moment(date).isBefore(comparedDate);
};

export const diff = (
  date: DateInputType,
  comparedDate: DateInputType,
  unitOfTime?: moment.unitOfTime.Diff
) => {
  return moment(date).diff(comparedDate, unitOfTime);
};

export const isBetween = (
  date: DateInputType,
  startDate: DateInputType,
  endDate: DateInputType,
  granularity?: moment.unitOfTime.StartOf,
  inclusivity?: InclusivityType
) => {
  return moment(date).isBetween(startDate, endDate, granularity, inclusivity);
};
