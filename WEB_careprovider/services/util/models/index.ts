export type DateInputType =
  | Date
  | string
  | number
  | Array<number | string>
  | void; // null | undefined

export type InclusivityType = '()' | '[)' | '(]' | '[]';
