import { Subject, Observable } from 'rxjs';
import {
  MicronautsWebSocketClient,
  createPairingWs,
} from '@infrastructure/websocket';
import { TWebSocketStatus } from './models';

let _pairingSocket: MicronautsWebSocketClient;

let _pairingSubject: Subject<{ status: TWebSocketStatus }>;
let _sendKeysSubject: Subject<{
  data: any;
}>;

export function startPairingSocket$(
  pairingAddress: string
): Observable<{ status: TWebSocketStatus }> {
  if (!_pairingSubject) {
    _pairingSubject = new Subject();
  }
  if (!_pairingSocket) {
    _pairingSocket = createPairingWs(pairingAddress);
    _pairingSocket.onmessage = payload => {
      _getSendKeysSubject().next({
        data: JSON.parse(payload.data),
      });
    };
    _pairingSocket.onreconnected = () => {
      _pairingSubject.next({ status: 'RECONNECTED' });
    };
    _pairingSocket.onopen = () => {
      _pairingSubject.next({ status: 'CONNECTED' });
    };
  }

  return _pairingSubject.asObservable();
}

export function stopPairingSocket(): void {
  if (_pairingSocket) {
    _pairingSocket.close();
    _pairingSocket = undefined;
  }
  if (_pairingSubject) {
    _pairingSubject.complete();
    _pairingSubject = undefined;
  }
  if (_sendKeysSubject) {
    _sendKeysSubject.complete();
    _sendKeysSubject = undefined;
  }
}

export function sendPairingData(data: string): void {
  if (_pairingSocket) {
    _pairingSocket.send(data);
  }
}

export function getSendKeysSubject$(): Observable<{
  data: any;
}> {
  return _getSendKeysSubject().asObservable();
}

// ------------------------------------------------------------- //

function _getSendKeysSubject(): Subject<{
  data: any;
}> {
  if (!_sendKeysSubject) {
    _sendKeysSubject = new Subject();
  }
  return _sendKeysSubject;
}
