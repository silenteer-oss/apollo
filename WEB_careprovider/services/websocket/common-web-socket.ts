/* eslint-disable @typescript-eslint/camelcase */
import { Subject, Observable } from 'rxjs';
import {
  MicronautsWebSocketClient,
  createCommonWs,
} from '@infrastructure/websocket';
import {
  APPOINTMENT_CHANGE_lOG_USER_ID,
  PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID,
  IEncryptedChatMessage,
  IAppointmentChangeLog,
  IProcessedCommunicationPlanMessage,
} from '../biz';
import { TWebSocketStatus } from './models';

let _commonSocket: MicronautsWebSocketClient;

let _commonSubject: Subject<{ status: TWebSocketStatus }>;
let _chatSubject: Subject<{
  data: IEncryptedChatMessage;
}>;
let _appointmentChangeLogSubject: Subject<{
  data: IAppointmentChangeLog;
}>;
let _communicationPlanMessageSubject: Subject<{
  data: IProcessedCommunicationPlanMessage;
}>;
let _employeeChangedSubject: Subject<void>;

export function startCommonSocket$(): Observable<{ status: TWebSocketStatus }> {
  if (!_commonSocket) {
    _commonSocket = createCommonWs();
    _commonSocket.onmessage = payload => {
      const { messageBody, topic } = JSON.parse(payload.data);
      const parsedMessageBody = JSON.parse(messageBody);

      if (topic === 'CHAT') {
        _getChatSubject().next({
          data: parsedMessageBody,
        });
      } else if (topic === 'APPOINTMENT') {
        const { logTime, ...rest } = parsedMessageBody;

        _getAppointmentChangeLogSubject().next({
          data: {
            ...rest,
            sendTime: logTime,
            userId: APPOINTMENT_CHANGE_lOG_USER_ID,
          },
        });
      } else if (topic === 'COMMUNICATION_PLAN') {
        const { processedDate, ...rest } = parsedMessageBody;

        _getCommunicationPlanMessageSubject().next({
          data: {
            ...rest,
            sendTime: processedDate,
            userId: PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID,
          },
        });
      } else if (topic === 'EMPLOYEE_CHANGED') {
        _getEmployeeChangedSubject().next();
      }
    };
    _commonSocket.onreconnected = () => {
      _getCommonSubject().next({ status: 'RECONNECTED' });
    };
  }

  return _getCommonSubject().asObservable();
}

export function stopCommonSocket(): void {
  if (_commonSocket) {
    _commonSocket.close();
    _commonSocket = undefined;
  }
  if (_commonSubject) {
    _commonSubject.complete();
    _commonSubject = undefined;
  }
  if (_chatSubject) {
    _chatSubject.complete();
    _chatSubject = undefined;
  }
  if (_appointmentChangeLogSubject) {
    _appointmentChangeLogSubject.complete();
    _appointmentChangeLogSubject = undefined;
  }
  if (_communicationPlanMessageSubject) {
    _communicationPlanMessageSubject.complete();
    _communicationPlanMessageSubject = undefined;
  }
  if (_employeeChangedSubject) {
    _employeeChangedSubject.complete();
    _employeeChangedSubject = undefined;
  }
}

export function getChat$(): Observable<{
  data: IEncryptedChatMessage;
}> {
  return _getChatSubject().asObservable();
}

export function getAppointmentChangeLog$(): Observable<{
  data: IAppointmentChangeLog;
}> {
  return _getAppointmentChangeLogSubject().asObservable();
}

export function getCommunicationPlanMessage$(): Observable<{
  data: IProcessedCommunicationPlanMessage;
}> {
  return _getCommunicationPlanMessageSubject().asObservable();
}

export function getEmployeeChanged$(): Observable<void> {
  return _getEmployeeChangedSubject().asObservable();
}

// ------------------------------------------------------------- //

function _getCommonSubject(): Subject<{ status: TWebSocketStatus }> {
  if (!_commonSubject) {
    _commonSubject = new Subject();
  }
  return _commonSubject;
}

function _getChatSubject(): Subject<{
  data: IEncryptedChatMessage;
}> {
  if (!_chatSubject) {
    _chatSubject = new Subject();
  }
  return _chatSubject;
}

function _getAppointmentChangeLogSubject(): Subject<{
  data: IAppointmentChangeLog;
}> {
  if (!_appointmentChangeLogSubject) {
    _appointmentChangeLogSubject = new Subject();
  }
  return _appointmentChangeLogSubject;
}

function _getCommunicationPlanMessageSubject(): Subject<{
  data: IProcessedCommunicationPlanMessage;
}> {
  if (!_communicationPlanMessageSubject) {
    _communicationPlanMessageSubject = new Subject();
  }
  return _communicationPlanMessageSubject;
}

function _getEmployeeChangedSubject(): Subject<void> {
  if (!_employeeChangedSubject) {
    _employeeChangedSubject = new Subject();
  }
  return _employeeChangedSubject;
}
