import { Gender } from '@silenteer/hermes/resource/profile-resource/model';
import { UserType } from '@silenteer/hermes/resource/careprovider-app-resource/model';

export { Gender, UserType };

export interface IProfile {
  id: string;
  fullName: string;
  dob: number;
  gender: Gender;
  phone: string;
  email: string;
  address: string;
}

export interface IEmployeeProfile extends IProfile {
  accountId: string;
  careProvideId: string;
  type: UserType;
}

export interface IPatientProfile extends IProfile {
  conversationId: string;
  nickName: string;
  title: string;
}
