import { AppointmentChangeLogResponse } from '@silenteer/hermes/resource/appointment-app-resource/model';
import { IMessage } from '../message';

// eslint-disable-next-line @typescript-eslint/camelcase
export const APPOINTMENT_CHANGE_lOG_USER_ID = 'APPOINTMENT_CHANGE_lOG_USER_ID';

export interface IAppointmentChangeLog
  extends Omit<AppointmentChangeLogResponse, 'logTime'>,
    IMessage {}
