import { IEmployeeProfile } from '../profile';

export interface IGlobalInitial {
  configs: { [key: string]: object };
  status: number;
  userProfile: IEmployeeProfile;
}
