export enum MessageType {
  AppointmentChangeLog = 'APPOINTMENT_CHANGE_LOG',
  EmployeeChatMessage = 'EMPLOYEE_CHAT_MESSAGE',
  PatientChatMessage = 'PATIENT_CHAT_MESSAGE',
  ProcessedCommunicationPlanMessage = 'PROCESSED_COMMUNICATION_PLAN_MESSAGE',
}

export type TMessageStatus = 'PENDING' | 'FAILED';

export interface IMessage {
  id: string;
  type: MessageType;
  sendTime: number;
  status?: TMessageStatus;
  userId: string;
}
