import { ConversationMessageResponse as IEncryptedChatMessage } from '@silenteer/hermes/resource/chat-app-resource/model';
import { IMessage } from '../message';

export interface IDecryptedChatMessage extends IMessage {
  conversationId: string;
  senderId: string;
  senderDeviceId: string;
  userId: string;
  content: string;
}

export { IEncryptedChatMessage };
