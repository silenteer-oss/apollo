import { PatientPlanMessageResponse } from '@silenteer/hermes/resource/communication-plan-app-resource/model';
import { IMessage } from '../message';

// eslint-disable-next-line @typescript-eslint/camelcase
export const PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID =
  'PROCESSED_COMMUNICATION_PLAN_MESSAGE_USER_ID';
export interface IProcessedCommunicationPlanMessage
  extends Omit<PatientPlanMessageResponse, 'processedDate' | 'status'>,
    IMessage {
  processedDate?: number;
}
