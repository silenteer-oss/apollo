import { ProfileApi } from '@infrastructure/resource/ProfileResource';
import { IPatientProfile } from '../models';

export function getPatientProfiles(ids: string[]): Promise<IPatientProfile[]> {
  return ProfileApi.getPatientProfileByIds({
    originalIds: ids,
  }).then(response => response.data);
}
