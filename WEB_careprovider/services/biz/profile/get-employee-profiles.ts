import { ProfileApi } from '@infrastructure/resource/ProfileResource';
import {
  CareProviderApi,
  CareProviderApiModel,
} from '@infrastructure/resource/CareProviderAppResource';
import { IEmployeeProfile } from '../models';

export function getEmployeeProfiles(
  ids: string[]
): Promise<IEmployeeProfile[]> {
  return Promise.all([
    ProfileApi.getEmployeeProfileByIds({
      originalIds: ids,
    }).then(response => response.data),
    CareProviderApi.getEmployeeByIds({
      ids,
    }).then(response => response.data),
  ]).then(([profiles, employees]) => {
    const result: IEmployeeProfile[] = [];
    const employeeMap = employees.reduce(
      (result, employee) => {
        result[employee.id] = employee;
        return result;
      },
      {} as { [key: string]: CareProviderApiModel.EmployeeDto }
    );

    profiles.forEach(profile => {
      result.push({
        ...profile,
        ...employeeMap[profile.id],
      });
    });

    return result;
  });
}
