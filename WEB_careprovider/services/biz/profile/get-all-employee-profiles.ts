import { ProfileApi } from '@infrastructure/resource/ProfileResource';
import { CareProviderApiModel } from '@infrastructure/resource/CareProviderAppResource';
import { IEmployeeProfile } from '../models';
import { AdminApi } from '@infrastructure/resource/AdminResource';

export async function getAllEmployeeProfiles(): Promise<IEmployeeProfile[]> {
  const employees = await AdminApi.getAll().then(response => response.data);

  const employeeProfiles = await ProfileApi.getEmployeeProfileByIds({
    originalIds: employees.map(item => item.id),
  }).then(response => response.data);

  const result: IEmployeeProfile[] = [];
  const employeeMap = employees.reduce(
    (result, employee) => {
      result[employee.id] = employee;
      return result;
    },
    {} as { [key: string]: CareProviderApiModel.EmployeeDto }
  );

  employeeProfiles.forEach(profile => {
    result.push({
      ...profile,
      ...employeeMap[profile.id],
    });
  });

  return result;
}
