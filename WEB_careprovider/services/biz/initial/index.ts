import { ProfileApi } from '@infrastructure/resource/ProfileResource';
import { CareProviderApi } from '@infrastructure/resource/CareProviderAppResource';
import { IGlobalInitial } from '../models';

export function getApplicationInitial(): Promise<IGlobalInitial> {
  return CareProviderApi.getCareProviderInitial()
    .then(response => response.data)
    .then(initial => {
      const { configs, status, userInfo } = initial;

      if (status === 200) {
        return ProfileApi.getMyEmployeeProfile()
          .then(response => response.data)
          .then(profile => {
            return {
              configs,
              status,
              userProfile: {
                ...userInfo,
                ...profile,
              },
            };
          });
      } else {
        return {
          configs,
          status,
          userProfile: { ...userInfo },
        } as IGlobalInitial;
      }
    });
}
