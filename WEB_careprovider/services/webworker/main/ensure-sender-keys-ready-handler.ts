import { IMessageEvent } from '@infrastructure/webworker';
import { handleMessage } from '@infrastructure/webworker/main';
import { CONVERSATION_MESSAGE_EVENT_TYPES } from '../models';

export function ensureSenderKeysReadyHandler(
  messageEvent: IMessageEvent
): void {
  if (
    messageEvent.type ===
    CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_SENDER_KEYS_READY
  ) {
    handleMessage(messageEvent);
  }
}
