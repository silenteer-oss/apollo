import { postMessage } from '@infrastructure/webworker/main';
import { CONVERSATION_MESSAGE_EVENT_TYPES } from '../models';

export function ensureCareProviderDeviceReady(): Promise<void> {
  return postMessage({
    type: CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_CARE_PROVIDER_DEVICE_READY,
  });
}
