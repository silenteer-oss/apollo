import { initMainThread } from '@infrastructure/webworker/main';
import { hashPasswordHandler } from '@infrastructure/crypto/services/webworker/main';
import {
  decryptMessageHandler,
  encryptMessageHandler,
} from '@infrastructure/signal/services/webworker/main';
import { ensureCareProviderDeviceReadyHandler } from './ensure-care-provider-device-ready-handler';
import { ensureSenderKeysReadyHandler } from './ensure-sender-keys-ready-handler';

export function initWorker() {
  initMainThread({
    worker: new Worker('../background/index.ts', {
      type: 'module',
    }),
    messageEventHandlers: [
      hashPasswordHandler,
      decryptMessageHandler,
      encryptMessageHandler,
      ensureCareProviderDeviceReadyHandler,
      ensureSenderKeysReadyHandler,
    ],
  });
}

export * from './ensure-care-provider-device-ready';
export * from './ensure-sender-keys-ready';
