import { IMessageEvent } from '@infrastructure/webworker';
import { handleMessage } from '@infrastructure/webworker/main';
import { CONVERSATION_MESSAGE_EVENT_TYPES } from '../models';

export function ensureCareProviderDeviceReadyHandler(
  messageEvent: IMessageEvent
): void {
  if (
    messageEvent.type ===
    CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_CARE_PROVIDER_DEVICE_READY
  ) {
    handleMessage(messageEvent);
  }
}
