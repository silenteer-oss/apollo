import { postMessage } from '@infrastructure/webworker/main';
import { CONVERSATION_MESSAGE_EVENT_TYPES } from '../models';

export function ensureSenderKeysReady(params: {
  conversationId: string;
  entityId: string; // entityId can be careProviderId or patientId
  senderId: string;
  senderDeviceId: string;
}): Promise<void> {
  return postMessage({
    type: CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_SENDER_KEYS_READY,
    data: params,
  });
}
