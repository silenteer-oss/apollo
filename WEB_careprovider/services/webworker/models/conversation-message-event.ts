import { TMessageEventStatus, IMessageEvent } from '@infrastructure/webworker';

export const CONVERSATION_MESSAGE_EVENT_TYPES = {
  ENSURE_CARE_PROVIDER_DEVICE_READY: 'ENSURE_CARE_PROVIDER_DEVICE_READY',
  ENSURE_SENDER_KEYS_READY: 'ENSURE_SENDER_KEYS_READY',
};

export abstract class BaseEnsureCareProviderDeviceReadyMessageEvent
  implements IMessageEvent {
  id: string;
  status: TMessageEventStatus;
  type: string =
    CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_CARE_PROVIDER_DEVICE_READY;
}

export abstract class BaseEnsureSenderKeysReadyMessageEvent
  implements IMessageEvent {
  id: string;
  status: TMessageEventStatus;
  type: string = CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_SENDER_KEYS_READY;
  data: {
    conversationId: string;
    entityId: string; // entityId can be careProviderId or patientId
    senderId: string;
    senderDeviceId: string;
  };
}
