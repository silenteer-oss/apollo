import { DefaultApi } from '@silenteer/hermes/resource/chat-app-resource';
import { IMessageEvent } from '@infrastructure/webworker';
import { dedicatedWorker } from '@infrastructure/webworker/background';
import { execute } from '@infrastructure/webworker/background';
import { signalStorage } from '@infrastructure/signal/services/biz';
import {
  CONVERSATION_MESSAGE_EVENT_TYPES,
  BaseEnsureCareProviderDeviceReadyMessageEvent,
} from '../models';

export const ChatApi = new DefaultApi({
  basePath: dedicatedWorker.location.origin,
});

export async function ensureCareProviderDeviceReadyHandler(
  messageEvent: IMessageEvent
): Promise<void> {
  if (
    messageEvent.type ===
    CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_CARE_PROVIDER_DEVICE_READY
  ) {
    const {
      id,
      type,
    } = messageEvent as BaseEnsureCareProviderDeviceReadyMessageEvent;

    await execute({
      id,
      type,
      func: async () => {
        let careProviderDevice = await signalStorage.loadSenderDevice();

        if (!careProviderDevice) {
          careProviderDevice = await ChatApi.getMyDevice().then(
            response => response.data
          );
          await signalStorage.storeSenderDevice(careProviderDevice);
        }
      },
    });
  }
}
