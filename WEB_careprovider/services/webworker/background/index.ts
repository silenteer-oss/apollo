import { initBackgroundThread } from '@infrastructure/webworker/background';
import { hashPasswordHandler } from '@infrastructure/crypto/services/webworker/background';
import {
  decryptMessageHandler,
  encryptMessageHandler,
} from '@infrastructure/signal/services/webworker/background';
import { ensureCareProviderDeviceReadyHandler } from './ensure-care-provider-device-ready-handler';
import { ensureSenderKeysReadyHandler } from './ensure-sender-keys-ready-handler';

initBackgroundThread(
  hashPasswordHandler,
  decryptMessageHandler,
  encryptMessageHandler,
  ensureCareProviderDeviceReadyHandler,
  ensureSenderKeysReadyHandler
);
