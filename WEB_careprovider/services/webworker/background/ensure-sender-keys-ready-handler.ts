import { DefaultApi } from '@silenteer/hermes/resource/chat-app-resource';
import { IMessageEvent } from '@infrastructure/webworker';
import { dedicatedWorker } from '@infrastructure/webworker/background';
import { execute } from '@infrastructure/webworker/background';
import { SignalAddress, SignalSessionBuilder } from '@infrastructure/signal';
import { signalStorage } from '@infrastructure/signal/services/biz';
import { unseal } from '@infrastructure/crypto';
import { cryptoDatabase } from '@careprovider/services/client-database';
import {
  CONVERSATION_MESSAGE_EVENT_TYPES,
  BaseEnsureSenderKeysReadyMessageEvent,
} from '../models';

export const ChatApi = new DefaultApi({
  basePath: dedicatedWorker.location.origin,
});

export async function ensureSenderKeysReadyHandler(
  messageEvent: IMessageEvent
): Promise<void> {
  if (
    messageEvent.type ===
    CONVERSATION_MESSAGE_EVENT_TYPES.ENSURE_SENDER_KEYS_READY
  ) {
    const {
      id,
      type,
      data: { conversationId, entityId, senderId, senderDeviceId },
    } = messageEvent as BaseEnsureSenderKeysReadyMessageEvent;

    await execute({
      id,
      type,
      func: async () => {
        const address = new SignalAddress(
          conversationId,
          senderId,
          senderDeviceId
        );
        const record = await signalStorage.loadRecord(address);

        if (record.isEmpty) {
          // Need get keys at here for case multi device for 1 careProvider
          const senderKeys = await ChatApi.getKey(
            conversationId,
            entityId
          ).then(response =>
            response && response.data
              ? response.data.map(item => ({
                  senderKey: item.key,
                  signingKey: item.signingKey,
                }))
              : []
          );

          const careProviderKeyPair = await cryptoDatabase.keyPairStorage
            .get('CARE_PROVIDER')
            .then(data => (data ? data.value : undefined));

          for (let i = 0; i < senderKeys.length; i++) {
            const { senderKey, signingKey } = senderKeys[i];

            const careProviderSenderKey = await unseal(
              senderKey,
              careProviderKeyPair
            );

            const privateSigningKey =
              signingKey && (await unseal(signingKey, careProviderKeyPair));

            await new SignalSessionBuilder(signalStorage).process(
              address,
              careProviderSenderKey,
              privateSigningKey
            );
          }
        }
      },
    });
  }
}
