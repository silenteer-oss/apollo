import Dexie from 'dexie';
import { IMessage } from '../biz';
import { IMessageSchema } from './models';

export class ApplicationDatabase extends Dexie {
  static getDBMessageId(message: IMessage): string {
    const { sendTime, id } = message;
    return `${sendTime}-${id}`;
  }
  messageStorage: Dexie.Table<IMessageSchema, string>;

  public constructor() {
    super('application-database');
    this.version(1).stores({
      'message-storage':
        'id,conversationId,patientId,status,type,sendTime,message',
    });
    this.messageStorage = this.table('message-storage');
  }
}
