import {
  CryptoDatabase,
  ICryptoKeyPairSchema,
} from '@infrastructure/crypto/services/client-database';
import { ApplicationDatabase } from './application-database';

export * from '@infrastructure/crypto/services/client-database';
export const cryptoDatabase = new CryptoDatabase();

export function storeCareProviderKeyPairToIndexDB(
  unsealedCareproviderKeyPair: string
): Promise<void> {
  return cryptoDatabase.keyPairStorage
    .put({
      key: 'CARE_PROVIDER',
      value: JSON.parse(unsealedCareproviderKeyPair),
    })
    .then(() => {
      return;
    });
}
export function getCareProviderKeyPairFromIndexDB(): Promise<
  ICryptoKeyPairSchema
> {
  return cryptoDatabase.keyPairStorage.get('CARE_PROVIDER');
}

export function storeCareProviderKeyPairToLocalStorage(
  unsealedCareproviderKeyPair: string
): Promise<void> {
  return Promise.resolve(
    localStorage.setItem(
      'CARE_PROVIDER',
      JSON.stringify({
        key: 'CARE_PROVIDER',
        value: JSON.parse(unsealedCareproviderKeyPair),
      })
    )
  );
}
export function getCareProviderKeyPairFromLocalStorage(): Promise<
  ICryptoKeyPairSchema
> {
  const _data = localStorage.getItem('CARE_PROVIDER');
  return Promise.resolve(_data ? JSON.parse(_data) : undefined);
}

// -------------------------------------------------- //

export * from './models';
export * from './application-database';

export const appDatabase = new ApplicationDatabase();
