import { TMessageStatus, IMessage, MessageType } from '../../biz';
export interface IMessageSchema {
  id: string;
  conversationId?: string;
  patientId?: string;
  status?: TMessageStatus;
  type: MessageType;
  sendTime: number;
  message: IMessage;
}
