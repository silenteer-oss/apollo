import type {
  ICoreTheme,
  ICoreThemeTypography,
  ICoreThemeSpace,
  ICoreThemeBreakpoint,
  ICoreThemeOpacity,
  ICoreThemeRadius,
  ICoreComponentsTheme,
} from '@design-system/core/models';

export interface IMicroFrontendTheme<TViewsTheme> extends ICoreTheme {
  views: TViewsTheme;
  setViewTheme(
    ...funcs: Array<(theme: IMicroFrontendTheme<TViewsTheme>) => TViewsTheme>
  ): void;
  setComponentTheme(
    ...funcs: Array<
      (theme: IMicroFrontendTheme<TViewsTheme>) => ICoreComponentsTheme
    >
  ): void;
}

export abstract class BaseMicroFrontendTheme<TViewsTheme>
  implements IMicroFrontendTheme<TViewsTheme> {
  abstract id: string;
  abstract isLightTheme: boolean;
  abstract isDarkTheme: boolean;
  abstract typography: ICoreThemeTypography;
  abstract space: ICoreThemeSpace;
  abstract breakpoint: ICoreThemeBreakpoint;
  abstract opacity: ICoreThemeOpacity;
  abstract radius: ICoreThemeRadius;
  components: ICoreComponentsTheme = {};
  views = {} as TViewsTheme;

  setComponentTheme(
    ...funcs: Array<
      (theme: IMicroFrontendTheme<TViewsTheme>) => ICoreComponentsTheme
    >
  ): void {
    this.components = {
      ...this.components,
      ...funcs.reduce(
        (result, func) => ({
          ...result,
          ...func(this),
        }),
        {} as ICoreComponentsTheme
      ),
    };
  }

  setViewTheme(
    ...funcs: Array<(theme: IMicroFrontendTheme<TViewsTheme>) => TViewsTheme>
  ): void {
    this.views = {
      ...this.views,
      ...funcs.reduce(
        (result, func) => ({
          ...result,
          ...func(this),
        }),
        {} as TViewsTheme
      ),
    };
  }
}
