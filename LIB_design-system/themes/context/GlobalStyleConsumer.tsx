import React from 'react';
import type { ReactNode } from 'react';
import type { IGlobalStyleContext } from './models';
import { GlobalStyleContext } from './GlobalStyleContext';

export class GlobalStyleContextConsumer extends React.PureComponent<{
  children: (value: IGlobalStyleContext) => ReactNode;
}> {
  render() {
    return (
      <GlobalStyleContext.Consumer>
        {this.props.children}
      </GlobalStyleContext.Consumer>
    );
  }
}
