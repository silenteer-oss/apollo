import React from 'react';
import { createGlobalStyle } from 'styled-components';
import type {
  IGlobalStyleContext,
  IGlobalStyleContextProviderProps,
  IGlobalStyleContextProviderState,
} from './models';
import { GlobalStyleContext } from './GlobalStyleContext';

export const GlobalStyle = createGlobalStyle<{
  styles: string[];
}>`
  ${({ styles }) => {
    return styles.join(' ');
  }}
`;

export class GlobalStyleContextProvider extends React.PureComponent<
  IGlobalStyleContextProviderProps,
  IGlobalStyleContextProviderState
  > {
  private _conversationContext: IGlobalStyleContext;

  state: IGlobalStyleContextProviderState = { styleMap: {} };

  constructor(props: IGlobalStyleContextProviderProps) {
    super(props);

    if (props && props.styles) {
      this.state.styleMap = { ...props.styles };
    }

    this._conversationContext = {
      putGlobalStyle: this._putGlobalStyle,
      addGlobalStyle: this._addGlobalStyle,
    };
  }

  render() {
    const { styleMap } = this.state;
    return (
      <GlobalStyleContext.Provider value={this._conversationContext}>
        <>
          <GlobalStyle styles={Object.values(styleMap)} />
          {this.props.children}
        </>
      </GlobalStyleContext.Provider>
    );
  }

  private _putGlobalStyle = (id: string, style: string): void => {
    const { styleMap } = this.state;

    this.setState({ styleMap: { ...styleMap, [id]: style } });
  };

  private _addGlobalStyle = (id: string, style: string): void => {
    const { styleMap } = this.state;
    if (styleMap[id]) {
      return;
    }

    this.setState({ styleMap: { ...styleMap, [id]: style } });
  };
}
