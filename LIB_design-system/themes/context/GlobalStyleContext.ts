/* eslint-disable prettier/prettier */
import React from 'react';
import type { IGlobalStyleContext } from './models';

const defaultContext: IGlobalStyleContext = {
  putGlobalStyle: () => { return; },
  addGlobalStyle: () => { return; },
};

export const GlobalStyleContext = React.createContext<IGlobalStyleContext>(
  defaultContext
);