import type {
  Newable,
  ThirdPartyModule,
  FallbackLngObjList,
} from 'i18next';

import type {
  Module, TFunction
} from 'i18next';

export interface II18nContext {
  currentLanguage: string;
  changeLanguage: (lng: string) => Promise<void>;
  loadNamespace: (params: { id: string; url: string }) => Promise<void>;
}

export interface II18nFixedNamespaceContext extends II18nContext {
  t: TFunction;
}

export interface II18nContextProviderProps {
  whitelist: string[] | false;
  fallbackLng: string | false | string[] | FallbackLngObjList;
  ns?: string | string[];
  defaultNS?: string;
  fallbackNS?: string | string[] | false;
  debug?: boolean;
  modules?: Array<
    | Module
    | Newable<Module>
    | ThirdPartyModule[]
    | Array<Newable<ThirdPartyModule>>
  >;
  onInitedI18n?: () => void;
  onChangedLanguage?: (newLanguage: string) => void;
}
