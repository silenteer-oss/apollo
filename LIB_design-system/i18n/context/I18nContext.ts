/* eslint-disable prettier/prettier */
import React from 'react';
import type { II18nContext } from './models';

const error = new Error('Need prodive Context for I18nProvider');

const defaultContext: II18nContext = {
  currentLanguage: undefined,
  changeLanguage: () => Promise.reject(error),
  loadNamespace: () => Promise.reject(error),
};

export const I18nContext = React.createContext<II18nContext>(
  defaultContext
);