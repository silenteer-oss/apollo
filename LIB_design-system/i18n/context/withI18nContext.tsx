/* eslint-disable no-undef */
import React from 'react';
import type {
  ReactNode,
  PureComponent,
  ComponentType,
  FunctionComponent,
} from 'react';
import i18next from 'i18next';
import type { TFunction } from 'i18next';
import type { II18nContext, II18nFixedNamespaceContext } from './models';
import { I18nContext } from './I18nContext';

interface I18nContainerProps {
  namespace: string;
  publicAssetPath: string;
  fallback?: ReactNode;
  children: (t: TFunction) => ReactNode;
}

interface I18nContainerState {
  isReady: boolean;
}

export function withI18nContext<TProps>(
  Component: ComponentType<TProps & II18nFixedNamespaceContext>,
  options: {
    namespace: string;
    publicAssetPath: string;
    fallback?: ReactNode;
  }
): FunctionComponent<TProps> {
  class I18nContainer extends React.PureComponent<
    I18nContainerProps & II18nContext,
    I18nContainerState
    > {
    private _t: TFunction;
    state: I18nContainerState = {
      isReady: false,
    };

    componentDidMount() {
      this._loadNamespace().then(() => this.setState({ isReady: true }));
    }

    componentDidUpdate(prevProps: I18nContainerProps & II18nContext) {
      if (this.props.currentLanguage !== prevProps.currentLanguage) {
        this.setState({ isReady: false }, () => {
          this._loadNamespace().then(() => this.setState({ isReady: true }));
        });
      }
    }

    render() {
      const { fallback } = this.props;
      const { isReady } = this.state;

      if (!isReady) {
        return fallback || null;
      }

      return this.props.children(this._t);
    }

    private _loadNamespace = (): Promise<void> => {
      const { namespace, publicAssetPath, loadNamespace } = this.props;
      const _language = i18next.language;
      const __I18N_HASHS_MAP__ = window['__I18N_HASHS_MAP__'];
      const _hash =
        __I18N_HASHS_MAP__ &&
          __I18N_HASHS_MAP__[_language] &&
          __I18N_HASHS_MAP__[_language][namespace]
          ? __I18N_HASHS_MAP__[_language][namespace]
          : undefined;
      let _publicPath = publicAssetPath === '/' ? '' : publicAssetPath;
      const _namespaceUrl = _hash
        ? `${_publicPath}/i18n/${_language}/${namespace}.${_hash}.json`
        : `${_publicPath}/i18n/${_language}/${namespace}.json`;

      return loadNamespace({
        id: namespace,
        url: _namespaceUrl,
      })
        .then(() => {
          this._t = i18next.getFixedT(i18next.language, namespace);
        })
        .catch(() => {
          this._t = i18next.getFixedT(i18next.language, namespace);
        });
    };
  }

  return (props: TProps) => {
    return (
      <I18nContext.Consumer>
        {contexts => (
          <I18nContainer {...options} {...contexts}>
            {t => <Component {...props} {...contexts} t={t} />}
          </I18nContainer>
        )}
      </I18nContext.Consumer>
    );
  };
}
