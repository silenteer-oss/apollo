import React from 'react';
import type { ReactNode } from 'react';
import type { II18nContext } from './models';
import { I18nContext } from './I18nContext';

export class I18nContextConsumer extends React.PureComponent<{
  children: (value: II18nContext) => ReactNode;
}> {
  render() {
    return <I18nContext.Consumer>{this.props.children}</I18nContext.Consumer>;
  }
}
