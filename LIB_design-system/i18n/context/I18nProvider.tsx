import React from 'react';
import axios from 'axios';
import i18next, { TFunction } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { SentryService } from '@design-system/core/components';
import type { II18nContext, II18nContextProviderProps } from './models';
import { I18nContext } from './I18nContext';

export class I18nContextProvider extends React.PureComponent<
  II18nContextProviderProps
  > {
  private _i18nContext: II18nContext;
  private _loadNamespaceMap: { [key: string]: Promise<void> } = {};

  constructor(props: II18nContextProviderProps) {
    super(props);

    this._i18nContext = {
      currentLanguage: undefined,
      changeLanguage: this._changeLanguage,
      loadNamespace: this._loadNamespace,
    };
  }

  componentDidMount() {
    const { onInitedI18n } = this.props;

    this._init().then(() => {
      if (onInitedI18n) {
        onInitedI18n();
      }
    });
  }

  render() {
    return (
      <I18nContext.Provider value={this._i18nContext}>
        {this.props.children}
      </I18nContext.Provider>
    );
  }

  private _changeLanguage = (lng: string): Promise<void> => {
    if (lng === i18next.language) {
      return Promise.resolve();
    }

    const { onChangedLanguage } = this.props;

    return new Promise((resolve, reject) => {
      i18next
        .changeLanguage(lng)
        .then(() => {
          if (onChangedLanguage) {
            onChangedLanguage(lng);
          }

          this._loadNamespaceMap = {};
          this._i18nContext = { ...this._i18nContext, currentLanguage: lng };
          this.forceUpdate(() => resolve());
        })
        .catch(error => {
          console.error(error);
          SentryService.captureException(error);
          reject(error);
        });
    });
  };

  private _loadNamespace = (params: {
    id: string;
    url: string;
  }): Promise<void> => {
    const { id, url } = params;

    if (url.split('.').pop() !== 'json') {
      console.error('Resource bundle for i18n is required to be json file');
    }

    if (!this._loadNamespaceMap[url]) {
      const _lang = i18next.language;

      this._loadNamespaceMap[url] = axios.get(url).then(response => {
        i18next.addResourceBundle(_lang, id, response.data, true, true);
      });
    }

    return this._loadNamespaceMap[url];
  };

  private _init = (): Promise<TFunction> => {
    const {
      whitelist,
      fallbackLng,
      ns = '',
      defaultNS = '',
      fallbackNS = '',
      debug,
      modules,
    } = this.props;

    const _languageDetector = new LanguageDetector(null, {
      order: ['querystring'],
    });

    const result = i18next.use(_languageDetector);

    if (modules) {
      modules.forEach(i18nModule => result.use(i18nModule));
    }

    return result.init({
      whitelist,
      fallbackLng,
      ns,
      defaultNS,
      fallbackNS,
      debug,
      parseMissingKeyHandler: key => {
        return `__${key}__`;
      },
    });
  };
}
