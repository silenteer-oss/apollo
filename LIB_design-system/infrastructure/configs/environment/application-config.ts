import type { ProxyConfigMap, ProxyConfigArray } from 'webpack-dev-server';

export interface IApplicationConfig<TModules> {
  id: string;
  name: string;
  host: string;
  port?: number;
  publicAssetPath: string;
  proxy?: ProxyConfigMap | ProxyConfigArray;
  modules: TModules;
}
