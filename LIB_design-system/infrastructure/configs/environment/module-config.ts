export interface IModuleConfig<TViews, TProviders = any, TConsumers = any> {
  id: string;
  name: string;
  publicAssetPath: string;
  vendors?: string[];
  views: TViews;
  providers?: TProviders;
  consumers?: TConsumers;
}
