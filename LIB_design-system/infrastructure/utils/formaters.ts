import type { Nullable } from '../models';
import { getDateInfo } from './date';
import { isEmpty } from './validators';

export function lineBreakToBr(target: string): string {
  return target ? target.replace(/(?:\r\n|\r|\n)/g, '<br/>') : target;
}

export function toCamelCase(target: string): string {
  return target.replace(/(-[a-z])/g, char =>
    char.toUpperCase().replace('-', '')
  );
}

export function toHypenCase(target: string): string {
  return target
    .replace(/([A-Z])/g, char => `-${char.toLowerCase()}`)
    .split('-')
    .filter(item => !!item)
    .join('-');
}

export function toShortCase(target: string, divider: string = ' '): string {
  if (isEmpty(target)) {
    return target;
  }

  const items = target.split(divider);
  return items
    .filter((value, index) => index === 0 || index === items.length - 1)
    .reduce((result, name) => {
      if (!isEmpty(name)) {
        result.push(name[0].toUpperCase());
      }
      return result;
    }, [])
    .join('');
}

export function padNumber(target: number, length: number = 2): string {
  let _prefix = '0';
  let _max = 10;

  for (let i = 1; i < length - 1; i++) {
    _prefix += _prefix;
    _max *= _max;
  }

  return target < _max ? `${_prefix}${target}` : `${target}`;
}

export function toISO8601String(target: Date): Nullable<string> {
  if (!target) {
    return undefined;
  }

  const _timezone = -target.getTimezoneOffset();
  const _diff = _timezone >= 0 ? '+' : '-';
  const _pad = value => {
    const _norm = Math.floor(Math.abs(value));
    return (_norm < 10 ? '0' : '') + _norm;
  };

  return (
    target.getFullYear() +
    '-' +
    _pad(target.getMonth() + 1) +
    '-' +
    _pad(target.getDate()) +
    'T' +
    _pad(target.getHours()) +
    ':' +
    _pad(target.getMinutes()) +
    ':' +
    _pad(target.getSeconds()) +
    _diff +
    _pad(_timezone / 60) +
    ':' +
    _pad(_timezone % 60)
  );
}

export function toTimestamp(
  target: Date,
  unit: 'millisecond' | 'second' = 'millisecond'
): number {
  return target.getTime() / (unit && unit === 'second' ? 1000 : 1);
}

export function toDateFormat(
  target: Date,
  options: {
    dateFormat?:
    | 'dd/MM/yyyy'
    | 'dd-MM-yyyy'
    | 'yyyy/MM/dd'
    | 'yyyy-MM-dd'
    | 'dd/MM'
    | 'dd-MM';
    timeFormat?: 'hh:mm' | 'hh:mm:ss';
    showAMPM?: boolean;
  }
): string {
  const { dateFormat, timeFormat, showAMPM = false } = options;
  const { date, month, year, hours, minutes, seconds } = getDateInfo(target);

  let ampm = '';
  let _hours = hours;
  if (showAMPM) {
    ampm = hours > 12 ? ' CH' : ' SA';
    _hours = hours > 12 ? hours - 12 : hours;
  }

  const _month = month + 1;

  let result: string[] = [];

  if (dateFormat) {
    switch (dateFormat) {
      case 'dd/MM':
        result[0] = `${padNumber(date)}/${padNumber(_month)}`;
        break;
      case 'dd/MM/yyyy':
        result[0] = `${padNumber(date)}/${padNumber(_month)}/${year}`;
        break;
      case 'yyyy/MM/dd':
        result[0] = `${year}/${padNumber(_month)}/${padNumber(date)}`;
        break;
      case 'dd-MM':
        result[0] = `${padNumber(date)}-${padNumber(_month)}`;
        break;
      case 'dd-MM-yyyy':
        result[0] = `${padNumber(date)}-${padNumber(_month)}-${year}`;
        break;
      case 'yyyy-MM-dd':
        result[0] = `${year}-${padNumber(_month)}-${padNumber(date)}`;
        break;
    }
  }

  if (timeFormat) {
    switch (timeFormat) {
      case 'hh:mm':
        result[1] = `${padNumber(_hours)}:${padNumber(minutes)}${ampm}`;
        break;
      case 'hh:mm:ss':
        result[1] = `${padNumber(_hours)}:${padNumber(minutes)}:${padNumber(
          seconds
        )}${ampm}`;
        break;
    }
  }

  return result.filter(item => !!item).join(' ');
}
