export function getUUID(): string {
  let date = new Date().getTime();
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, char => {
    let random = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);
    return (char == 'x' ? random : (random & 0x3) | 0x8).toString(16);
  });
  return uuid;
}

export function generateRandomStr(): string {
  return Math.random()
    .toString(36)
    .substr(2, 6);
}

export function stripHTMLtags(input: string = '', allowed: string) {
  allowed = (
    ((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []
  ).join('');

  const tags = /<\/?([a-z0-9]*)\b[^>]*>?/gi;

  input =
    input.substring(input.length - 1) === '<'
      ? input.substring(0, input.length - 1)
      : input;

  // eslint-disable-next-line no-constant-condition
  while (true) {
    const before = input;
    input = before.replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });

    // return once no more tags are removed
    if (before === input) {
      return input;
    }
  }
}
