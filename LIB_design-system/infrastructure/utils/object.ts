import type { NonNullable } from '../models';
import { isNull } from './validators';

/**
 * Creates an new object from the object properties
 */
export function pick(target: object, keys: string[]): { [key: string]: any } {
  return keys.reduce((result, key) => {
    if (target[key]) {
      result[key] = target[key];
    }
    return result;
  }, {});
}
/**
 * Creates an object composed of the object properties predicate returns truthy for.
 */
export function pickBy<T>(target: object): { [key: string]: any } {
  const obj = {};
  for (const key in target) {
    if (
      target[key] !== null &&
      target[key] !== false &&
      target[key] !== undefined
    ) {
      obj[key] = target[key];
    }
  }
  return obj;
}

export function getPropertyAsKeyMap<T>(
  target: T
): { [key in keyof T]: string } {
  return Object.keys(target).reduce(
    (result, key) => {
      result[key] = key;
      return result;
    },
    {} as { [P in keyof T]: string }
  );
}

/**
 * get property value by property path
 */
export function getPropertyValue(
  target: any,
  propPath: string
): NonNullable<any> {
  if (isNull(target)) {
    return target;
  }

  const _props = propPath.split('.');
  let _prop;
  let result;

  for (let i = 0; i < _props.length; i++) {
    _prop = i === 0 ? target[_props[i]] : _prop[_props[i]];

    if (isNull(_prop)) {
      break;
    }
  }

  result = _prop;

  return result;
}
