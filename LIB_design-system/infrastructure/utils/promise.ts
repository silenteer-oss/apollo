export function doPromiseInDuration<T>(
  promise: Promise<T>,
  duration: number = 1000
): Promise<T> {
  const startTime = Date.now();

  return new Promise<T>((resolve, reject) => {
    return promise
      .then(result => {
        _execute({
          handler: () => resolve(result),
          startTime,
          duration,
        });
      })
      .catch(error => {
        _execute({
          handler: () => reject(error),
          startTime,
          duration,
        });
      });
  });
}

// -------------------------------- //

function _execute(params: {
  handler: () => void;
  startTime: number;
  duration: number;
}): void {
  const { handler, startTime, duration } = params;
  const endTime = Date.now();

  if (endTime - startTime >= duration) {
    handler();
  } else {
    setTimeout(() => {
      handler();
    }, duration - (endTime - startTime));
  }
}
