export * from './array';
export * from './color';
export * from './css-class';
export * from './date';
export * from './formaters';
export * from './object';
export * from './parsers';
export * from './promise';
export * from './string';
export * from './validators';

export function debounce(delay: number, originalFunc: Function) {
  let timeout;

  return function(...args) {
    const functionCall = () => originalFunc.apply(this, args);

    clearTimeout(timeout);
    timeout = setTimeout(functionCall, delay);
  };
}
