export function isString(target: any): boolean {
  return target && typeof target === 'string';
}

export function isNumber(target: any): boolean {
  return isNotEmpty(target, true) && !isNaN(target);
}

export function isInt(target: any): boolean {
  return isNumber(target) && +target % 1 === 0;
}

export function isObject(target: any): boolean {
  return target && typeof target === 'object';
}

export function isDate(target: any): boolean {
  return target && Object.prototype.toString.call(target) === '[object Date]';
}

export function isArray(target: any): boolean {
  return target && Object.prototype.toString.call(target) === '[object Array]';
}

export function isFunction(target: any): boolean {
  return target && typeof target === 'function';
}

export function isNull(target: any): boolean {
  return target === undefined || target === null;
}

export function isNotNull(target: any): boolean {
  return !isNull(target);
}

export function isEmpty(
  target: any,
  ignoreWhiteSpace: boolean = false,
  noZero: boolean = false
): boolean {
  return (
    isNull(target) ||
    target === '' ||
    (ignoreWhiteSpace
      ? isString(target) && target.trim().length === 0
      : false) ||
    (noZero ? target === 0 : false)
  );
}

export function isNotEmpty(
  target: any,
  ignoreWhiteSpace: boolean = false,
  noZero: boolean = false
): boolean {
  return !isEmpty(target, ignoreWhiteSpace, noZero);
}

export function isEmptyObject(target: object): boolean {
  return (
    isNull(target) ||
    (isObject(target) && Object.getOwnPropertyNames(target).length === 0)
  );
}

export function isEmail(target: any): boolean {
  const regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

  return isNotEmpty(target) && regex.test(target.toLowerCase());
}

export function isValidPhoneNumber(target: any): boolean {
  const regex = /(0[3|5|7|8|9])+([0-9]{8})\b/;

  return isNumber(target) && regex.test(target);
}
