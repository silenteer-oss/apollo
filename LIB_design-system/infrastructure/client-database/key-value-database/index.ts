import Dexie from 'dexie';

export interface IKeyValueSchema<TKey, TValue> {
  key: TKey;
  value: TValue;
}

export class KeyValueDatabase<TKey, TValue> extends Dexie {
  storage: Dexie.Table<IKeyValueSchema<TKey, TValue>, TKey>;

  public constructor(
    databaseName: string,
    options?: {
      addons?: Array<(db: Dexie) => void>;
      autoOpen?: boolean;
      indexedDB?: IDBFactory;
      IDBKeyRange?: new () => IDBKeyRange;
    }
  ) {
    super(databaseName, options);
    this.version(1).stores({
      storage: 'key,value',
    });
    this.storage = this.table('storage');
  }
}
