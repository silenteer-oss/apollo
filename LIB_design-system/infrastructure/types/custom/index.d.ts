declare module '*.svg' {
  const content: any;
  export default content;
}

declare module '*.jpg' {
  const content: any;
  export default content;
}

declare module '*.png' {
  const content: any;
  export default content;
}

declare module '*.md' {
  const content: any;
  export default content;
}

declare module 'console' {
  export = typeof import('console');
}

declare var __I18N_HASHS_MAP__: {
  [lang: string]: { [namespace: string]: string };
};
