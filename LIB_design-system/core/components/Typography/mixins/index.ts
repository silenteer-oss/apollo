import { isNumber } from '@design-system/infrastructure/utils';
import type {
  ITypographyProps,
  ITypographyTheme,
  TTypographyFontWeight,
} from '../models';

export function mixTypography(
  props: ITypographyProps,
  type:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'bodyL'
    | 'bodyM'
    | 'bodyS'
    | 'link'
    | 'quote'
): string {
  const {
    padding = '',
    margin,
    color,
    fontFamily,
    fontSize,
    fontWeight,
    lineHeight,
    letterSpacing,
    textAlign,
    textTransform,
    whiteSpace,
    wordBreak,
  } = props;
  const typography = props.theme.components.typography as ITypographyTheme;

  return `
    margin: ${margin ? (isNumber(margin) ? `${margin}px` : margin) : 0};
    padding: ${isNumber(padding) ? `${padding}px` : padding};
    color: ${!color ? typography[type].color : color === 'none' ? '' : color};
    font-family: ${fontFamily || typography[type].fontFamily};
    font-size: ${
    fontSize
      ? isNumber(fontSize)
        ? `${fontSize}px`
        : fontSize
      : typography[type].fontSize
    };
    font-weight: ${
    fontWeight ? _getFontWeight(fontWeight) : typography[type].fontWeight
    };
    line-height: ${lineHeight || typography[type].lineHeight};
    letter-spacing: ${letterSpacing || typography[type].letterSpacing};
    text-align: ${textAlign || ''};
    text-transform: ${textTransform || ''};
    white-space: ${whiteSpace || ''};
    word-break: ${wordBreak || ''};
  `;
}

function _getFontWeight(fontWeight: TTypographyFontWeight): number {
  if (isNumber(fontWeight)) {
    return fontWeight as number;
  }

  switch (fontWeight) {
    case 'Thin':
      return 100;
    case 'ExtraLight':
      return 200;
    case 'Light':
      return 300;
    case 'Normal':
      return 400;
    case 'Medium':
      return 500;
    case 'SemiBold':
      return 600;
    case 'Bold':
      return 700;
    case 'ExtraBold':
      return 800;
    case 'UltraBold':
      return 900;
    default:
      return 400;
  }
}
