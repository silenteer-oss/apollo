import React from 'react';
import { Flex } from '../Flexbox';
import { H2, BodyTextM } from '../Typography/Typography.styled';
import type { IErrorStateProps } from './ErrorStateModel';

export class OriginErrorState extends React.PureComponent<IErrorStateProps> {
  render() {
    const { className, title, content, element } = this.props;

    return (
      <Flex auto column justify="center" align="center" className={className}>
        {title && <H2>{title}</H2>}
        {content && <BodyTextM>{content}</BodyTextM>}
        {element}
      </Flex>
    );
  }
}
