import type { ReactElement } from 'react';

export interface IErrorStateProps {
  className?: string;
  title?: string;
  content?: string;
  element?: ReactElement;
}
