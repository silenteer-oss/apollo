import { styled } from '../../models';
import { getCssClass } from '../../../infrastructure/utils/css-class';
import type { IErrorStateProps } from './ErrorStateModel';
import { OriginErrorState } from './ErrorState';

export const ErrorState = styled(OriginErrorState).attrs(({ className }) => ({
  className: getCssClass('sl-ErrorState', className),
})) <IErrorStateProps>`
  ${props => {
    const { space } = props.theme;

    return `
    & {
      height: 100%;
      width: 100%;

      h2 {
        padding-bottom: ${space.xs};
      }
    }
  `;
  }}
`;
