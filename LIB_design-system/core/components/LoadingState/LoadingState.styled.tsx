import { getCssClass } from '../../../infrastructure/utils';
import { scaleSpace, scaleSpacePx } from '../../styles';
import { styled } from '../../models';
import { OriginLoadingState } from './LoadingState';
import type { ILoadingStateProps } from './LoadingStateModel';

export const LoadingState = styled(OriginLoadingState).attrs(
  ({ className }) => ({
    className: getCssClass('sl-LoadingState', className),
  })
) <ILoadingStateProps>`
  ${props => {
    const {
      radius,
      components: {
        loadingState: {
          backgroundColor,
          primaryBorderColor,
          secondaryBorderColor,
        },
      },
    } = props.theme;
    const { size } = props;

    return `
      &{
        position: relative;
        width: 100%;
        height: 100%;
        background-color: ${backgroundColor};

        .loader {
          position: absolute;
          top:0;
          bottom: 0;
          left: 0;
          right: 0;
          margin: auto;
          width: ${size || scaleSpace(16)}px;
          height: ${size || scaleSpace(16)}px;
          border: ${scaleSpacePx(2)} solid ${secondaryBorderColor};
          border-top: ${scaleSpacePx(2)} solid ${primaryBorderColor};
          border-radius: ${radius.circle};
          animation: spin 2s linear infinite;
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
      }
    `;
  }}
`;
