export interface ILoadingStateProps {
  className?: string;
  text?: string;
  size?: number;
}

export interface ILoaingStateTheme {
  backgroundColor: string;
  primaryBorderColor: string;
  secondaryBorderColor: string;
}
