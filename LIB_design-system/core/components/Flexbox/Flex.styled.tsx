import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '../../models';
import { mixFlexbox } from './mixins';
import type { IFlexProps } from './models';

export const Flex = styled('div').attrs(({ className }) => ({
  className: getCssClass('sl-Flex', className),
})) <IFlexProps>`
  ${props => `
    display: flex;
    ${mixFlexbox(props)}
  `}
`;
