import { setAsset, getAsset } from './manage-caches';

export function loadStyle(url: string): Promise<void> {
  let promise = getAsset(url);

  if (!promise) {
    promise = new Promise((resolve, reject) => {
      let link = document.createElement('link');
      link.onerror = () => reject(new Error(`Failed to load '${url}'`));
      link.onload = resolve;
      link.href = url;
      link.rel = 'stylesheet';
      (document.head || document.getElementsByTagName('head')[0]).appendChild(
        link
      );
    });

    setAsset({ url, promise });
  }

  return promise;
}
