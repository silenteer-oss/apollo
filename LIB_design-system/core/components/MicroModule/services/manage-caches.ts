const _moduleViewCache = new Map<string, React.ComponentClass<any>>();
const _moduleProviderCache = new Map<string, React.ComponentClass<any>>();
const _moduleConsumerCache = new Map<string, React.ComponentClass<any>>();
const _assetCache = new Map<string, Promise<any>>();

export function setAsset(params: { url: string; promise: Promise<any> }): void {
  const { url, promise } = params;
  _assetCache.set(url, promise);
}

export function getAsset(url: string): Promise<any> {
  return _assetCache.get(url);
}

// ----------------------------------------------------- //

export function setModuleView(params: {
  key: string;
  view: React.ComponentClass<any>;
}): void {
  const { key, view } = params;
  _moduleViewCache.set(key, view);
}

export function getModuleView<TProps>(
  key: string
): React.ComponentClass<TProps> {
  return _moduleViewCache.get(key);
}

// ----------------------------------------------------- //

export function setModuleProvider(params: {
  key: string;
  provider: React.ComponentClass<any>;
}): void {
  const { key, provider } = params;
  _moduleProviderCache.set(key, provider);
}

export function getModuleProvider<TProps>(
  key: string
): React.ComponentClass<TProps> {
  return _moduleProviderCache.get(key);
}

// ----------------------------------------------------- //

export function setModuleConsumer(params: {
  key: string;
  consumer: React.ComponentClass<any>;
}): void {
  const { key, consumer } = params;
  _moduleConsumerCache.set(key, consumer);
}

export function getModuleConsumer<TProps>(
  key: string
): React.ComponentClass<TProps> {
  return _moduleConsumerCache.get(key);
}
