import React from 'react';
import {
  setAsset,
  getAsset,
  setModuleView,
  getModuleView
} from './services';
import { MANIFEST_FILE_NAME } from './models';

export function loadMicroModuleView<TProps>(params: {
  url: string;
  module: string;
  view: string;
  app?: string;
}): React.ComponentClass<TProps & { fallback?: React.ReactNode }> {
  const _cacheKey = `${params.url}_${params.module}_${params.view}`;
  const _moduleView = getModuleView<TProps & { fallback?: React.ReactNode }>(
    _cacheKey
  );

  if (_moduleView) {
    return _moduleView;
  }

  class MicroModuleView<TProps> extends React.PureComponent<
    TProps & { fallback?: React.ReactNode },
    {
      microModule: any;
    }
    > {
    state = {
      microModule: undefined,
    };

    componentDidMount() {
      const { app, url, module: moduleName } = params;
      import(`/dist/${app}/module_${moduleName}/module.js`)
        .then(module => {
          this.setState({
            microModule: module.default
          });
        })
        .catch(console.error);
    }

    render() {
      const { view, module: moduleName } = params;
      const { fallback = <div>Failed to load {view} {JSON.stringify(this.state.microModule)} </div> } = this.props;
      const { microModule } = this.state;
      if (microModule && microModule[moduleName] && microModule[moduleName][view]) {
        const ModuleView = microModule[moduleName][view];
        return <ModuleView {...this.props}>{this.props.children}</ModuleView>;
      } else {
        return fallback || null;
      }
    }
  }

  setModuleView({
    key: _cacheKey,
    view: MicroModuleView,
  });

  return MicroModuleView;
}
