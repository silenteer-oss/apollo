import React from 'react';
import type { ReactNode, ComponentType, FunctionComponent } from 'react';

export function withMicroModuleContext<TProps, TContext>(params: {
  Component: ComponentType<TProps & TContext>;
  Consumer: ComponentType<{
    children: (value: TContext) => ReactNode;
  }>;
}): FunctionComponent<TProps> {
  const { Component, Consumer } = params;

  return (props: TProps) => {
    return (
      <Consumer>{contexts => <Component {...props} {...contexts} />}</Consumer>
    );
  };
}
