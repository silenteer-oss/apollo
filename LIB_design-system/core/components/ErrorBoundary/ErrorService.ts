import i18n from './i18n';

function getCustomMessage(
  customMessages: object = {},
  field: string,
  messageKey: string
) {
  const fieldData = customMessages[field] || {};
  return fieldData[messageKey];
}

function getCommonMessage(commonMessages: object = {}, messageKey: string) {
  return commonMessages[messageKey];
}

function catchValidationErrors(
  error: any,
  customI18n: { [field: string]: { [key: string]: string } } = {},
  locale: string = 'vi'
): { [key: string]: string } {
  if (!error.response) {
    throw error;
  }
  const validationErrors = error.response.data.validationErrors;
  if (!validationErrors) {
    throw error;
  }
  for (const field in validationErrors) {
    const messageKey = validationErrors[field];
    const translatedMessage =
      getCustomMessage(customI18n[locale], field, messageKey) ||
      getCommonMessage(i18n[locale], messageKey) ||
      messageKey;
    validationErrors[field] = translatedMessage;
  }
  return validationErrors;
}

export const ErrorService = {
  catchValidationErrors,
};
