export default {
  vi: {
    SOME_THINGS_WENT_WRONG:
      'Có lỗi xảy ra! Nếu lỗi này tiếp tục xảy ra, vui lòng liên hệ bộ phận kỹ thuật.',
    'Credentials Do Not Match': 'Đăng nhập thất bại!',
    NotBlank: 'Trường này không được để trống',
    NotEmpty: 'Trường này không được để trống',
    NotNull: 'Trường này không được để trống',
    PositiveOrZero: 'Giá trị phải lớn hơn hoặc bằng không',
    Email: 'Email phải đúng định dạng.',
    'incorrect.current.password': 'Mật khẩu hiện tại không đúng',
    NetworkOffline:
      'Bạn đang ngoại tuyến. Vui lòng kiểm tra lại kết nối mạng và tải lại trang',
  },
  en: {
    SOME_THINGS_WENT_WRONG:
      'There seems to be some issue. If this keeps happening, contact our support team.',
    'Credentials Do Not Match': 'Login failed!',
    NotBlank: 'This field cannot be blank.',
    NotEmpty: 'This field cannot be empty.',
    NotNull: 'This field cannot be null.',
    PositiveOrZero: 'The value must be greater than or equal zero.',
    Email: 'Email must be in valid format.',
    'incorrect.current.password': 'Incorrect curent password',
    NetworkOffline:
      'You are offline. Please check your internet connection and reload',
  },
};
