export interface IErrorBoundaryProps {
  user?: any;
  sentry?: any;
  Dialog: any;
  Toaster: any;
  apiErrorRedirect?: { [key: number]: string };
}

export interface IErrorBoundaryState {
  hasError: boolean;
  message: string;
  traceId: string;
  uiError: boolean;
  eventId: string;
}

export interface IErrorData {
  serverError?: string;
  validationErrors?: object;
}
