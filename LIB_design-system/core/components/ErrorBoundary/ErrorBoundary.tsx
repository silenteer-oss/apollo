import React from 'react';
import axios from 'axios';
import type {
  IErrorBoundaryState,
  IErrorBoundaryProps,
  IErrorData,
} from './ErrorBoundaryModel';
import i18n from './i18n';
import { SentryService } from './SentryService';

axios.interceptors.response.use(
  response => response,
  error => {
    return Promise.reject(error);
  }
);

const styles = {
  dialog: {
    marginTop: 10,
  },
};

export class ErrorBoundary extends React.PureComponent<
  IErrorBoundaryProps,
  IErrorBoundaryState
  > {
  private toaster: any;
  private refHandlers = {
    toaster: (ref: any) => (this.toaster = ref),
  };

  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = {
      hasError: false,
      uiError: false,
      message: '',
      traceId: '',
      eventId: undefined,
    };
  }

  componentDidMount() {
    const { sentry, user } = this.props;
    if (sentry) {
      SentryService.initService({ sentry, user });
    }

    window.addEventListener(
      'unhandledrejection',
      this._unhandledRejectionEventHandler
    );

    window.addEventListener('error', this._errorEventHandler);

    window.addEventListener('offline', this._offlineEventHandler);
  }

  componentDidUpdate() {
    SentryService.setUserContext(this.props.user);
  }

  componentDidCatch(error, info) {
    const eventId = this.sendToSentry(error);
    this.setState({ hasError: true, uiError: true, eventId });
  }

  componentWillUnmount() {
    window.removeEventListener(
      'unhandledrejection',
      this._unhandledRejectionEventHandler
    );

    window.removeEventListener('error', this._errorEventHandler);

    window.removeEventListener('offline', this._offlineEventHandler);
  }

  render() {
    const { Toaster } = this.props;
    return (
      <>
        <Toaster position="top-right" ref={this.refHandlers.toaster} />
        {this.renderErrorDialog()}
        {!this.state.uiError && this.props.children}
      </>
    );
  }

  renderErrorDialog = () => {
    const { Dialog } = this.props;
    const { hasError, traceId, uiError, eventId } = this.state;
    return (
      <Dialog
        icon="error"
        title="Hoppla"
        onClose={this.handleCloseDialog}
        isCloseButtonShown={!uiError}
        canEscapeKeyClose={!uiError}
        isOpen={hasError}
        canOutsideClickClose={false}
      >
        <div className="bp3-dialog-body">
          <p>{this.lang.SOME_THINGS_WENT_WRONG}</p>
          {traceId && (
            <p style={styles.dialog}>
              Trace Id: <strong>{traceId}</strong>
            </p>
          )}
          {eventId && (
            <p style={styles.dialog}>
              Event Id: <strong>{eventId}</strong>
            </p>
          )}
        </div>
      </Dialog>
    );
  };

  renderToasterMessage = (message: string, eventId: string) => (
    <>
      <div>{message}</div>
      {eventId && <div>Event Id: {eventId}</div>}
    </>
  );

  handleCloseDialog = () => {
    this.setState({ hasError: false, uiError: false, eventId: undefined });
  };

  get lang() {
    const locale = 'vi'; //TODO:
    return i18n[locale];
  }

  sendToSentry = event => {
    const { sentry } = this.props;
    if (!sentry) {
      return undefined;
    }
    const response = event.reason ? event.reason.response : undefined;
    if (response && response.status === 400) {
      return undefined;
    }
    if (response && response.status === 500) {
      const title = response.config
        ? response.statusText + ': ' + response.config.url
        : response.status;
      return SentryService.captureMessage(title, response.data);
    }
    return SentryService.captureException(event);
  };

  handlePromiseResponseError = (response, eventId: string) => {
    const traceId = response.data ? response.data.traceId : undefined;
    const { apiErrorRedirect } = this.props;

    if (response.status === 500) {
      this.setState({ hasError: true, traceId, eventId });
      return;
    }
    const redirectPath = apiErrorRedirect[response.status];

    if (redirectPath && location.pathname === redirectPath) {
      return;
    }
    if (redirectPath) {
      location.href = `${location.origin}${redirectPath}`;
      return;
    }
    this.processExpectedServerError(response.data, eventId);
  };

  processUnhandledError = (event, eventId: string) => {
    if (navigator && !navigator.onLine) {
      event.message = this.lang.NetworkOffline;
    }

    if (!event.message || !this.toaster) {
      this.setState({ hasError: true, eventId });
      return;
    }
    this.toaster.show({
      message: this.renderToasterMessage(event.message, eventId),
      intent: 'danger',
    });
  };

  processExpectedServerError = (errorData: IErrorData, eventId: string) => {
    if (!errorData.serverError || !this.toaster) {
      this.setState({ hasError: true, eventId });
      return;
    }
    const message =
      this.lang[errorData.serverError] || this.lang.SOME_THINGS_WENT_WRONG;

    this.toaster.show({
      message: this.renderToasterMessage(message, eventId),
      intent: 'danger',
    });
  };

  private _unhandledRejectionEventHandler = (
    event: PromiseRejectionEvent | CustomEvent
  ) => {
    const eventId = this.sendToSentry(event);

    let _shouldHandleError;
    let _response;

    if (event instanceof PromiseRejectionEvent) {
      _shouldHandleError = event.reason && event.reason.response;
      _response = event.reason ? event.reason.response : undefined;
    } else if (event instanceof CustomEvent) {
      _shouldHandleError =
        event.detail && event.detail.reason && event.detail.reason.response;
      _response =
        event.detail && event.detail.reason
          ? event.detail.reason.response
          : undefined;
    }

    if (_shouldHandleError) {
      this.handlePromiseResponseError(_response, eventId);
      return;
    }
    this.processUnhandledError(event, eventId);
  };

  private _errorEventHandler = (event: ErrorEvent) => {
    const eventId = this.sendToSentry(event);
    this.processUnhandledError(event, eventId);
  };

  private _offlineEventHandler = () => {
    this.toaster.show({
      message: this.renderToasterMessage(this.lang.NetworkOffline, undefined),
      intent: 'danger',
    });
  };
}
