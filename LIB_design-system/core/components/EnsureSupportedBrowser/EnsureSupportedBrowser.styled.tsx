import { styled } from '../../models';
import { getCssClass } from '../../../infrastructure/utils/css-class';
import type { IEnsureSupportedBrowserProps } from './models';
import { OriginEnsureSupportedBrowser } from './EnsureSupportedBrowser';

export const EnsureSupportedBrowser = styled(
  OriginEnsureSupportedBrowser
).attrs(({ className }) => ({
  className: getCssClass('sl-EnsureSupportedBrowser', className),
})) <IEnsureSupportedBrowserProps>`
  /* stylelint-disable */
`;
