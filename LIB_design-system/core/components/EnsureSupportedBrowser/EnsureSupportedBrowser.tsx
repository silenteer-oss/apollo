import React from 'react';
import { ErrorState } from '../ErrorState';
import type {
  IEnsureSupportedBrowserProps,
  IEnsureSupportedBrowserState,
} from './models';

export class OriginEnsureSupportedBrowser extends React.PureComponent<
  IEnsureSupportedBrowserProps,
  IEnsureSupportedBrowserState
  > {
  state: IEnsureSupportedBrowserState = {
    status: undefined,
  };

  componentDidMount() {
    const { onRequestPermission, onUnspported } = this.props;

    if (!this._checkSupportedFeatures()) {
      (onUnspported ? onUnspported() : Promise.reject()).then(() => {
        this.setState({ status: 'UNSUPPORTED_BROWSER' });
      });
    } else {
      (onRequestPermission
        ? onRequestPermission()
        : Promise.resolve(true)
      ).then(isGranted => {
        this.setState({ status: isGranted ? 'READY' : 'DENIED_PERMISSION' });
      });
    }
  }

  render() {
    const {
      className,
      unsupportedBrowserMessages,
      deniedPermissionMessages,
    } = this.props;
    const { status } = this.state;

    if (!status) {
      return null;
    }
    if (status === 'UNSUPPORTED_BROWSER') {
      return (
        <ErrorState className={className} {...unsupportedBrowserMessages} />
      );
    }
    if (status === 'DENIED_PERMISSION') {
      return <ErrorState className={className} {...deniedPermissionMessages} />;
    }
    if (status === 'READY') {
      return <>{this.props.children}</>;
    }
  }

  private _checkSupportedFeatures = (): boolean => {
    const { features } = this.props;

    let result = true;

    features.forEach(feature => {
      if (
        (feature === 'indexDB' && !window.indexedDB) ||
        (feature === 'storageManager' &&
          !(
            navigator.storage &&
            navigator.storage.estimate &&
            navigator.storage.persist &&
            navigator.storage.persisted
          ))
      ) {
        result = false;
      }
    });

    return result;
  };
}
