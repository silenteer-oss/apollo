import type { ReactElement } from 'react';
import type { ICoreTheme } from '../../../models';

export type TBrowserFeature = 'indexDB' | 'storageManager';

export interface IEnsureSupportedBrowserProps {
  className?: string;
  theme: ICoreTheme;
  features: TBrowserFeature[];
  unsupportedBrowserMessages: {
    title?: string;
    content?: string;
    element?: ReactElement;
  };
  deniedPermissionMessages: {
    title?: string;
    content?: string;
    element?: ReactElement;
  };
  onRequestPermission: () => Promise<boolean>;
  onUnspported?: () => Promise<void>;
}

export interface IEnsureSupportedBrowserState {
  status?: 'UNSUPPORTED_BROWSER' | 'DENIED_PERMISSION' | 'READY';
}
