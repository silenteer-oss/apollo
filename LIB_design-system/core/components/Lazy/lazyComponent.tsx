import React, { Suspense } from 'react';

export function lazyComponent<T>(
  dynamicImport: () => Promise<{ default: React.ComponentType<T> }>,
  fallback?: React.ReactElement
): React.FunctionComponent<T> {
  const Component = React.lazy(
    dynamicImport
  ) as React.LazyExoticComponent<React.ComponentType<any>>;
  return props => (
    <Suspense fallback={fallback || <div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}
