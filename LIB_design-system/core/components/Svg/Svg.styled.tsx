import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '../../models';
import type { ISvgProps } from './models';
import { OriginSvg } from './Svg';

export const Svg = styled(OriginSvg).attrs(({ className }) => ({
  className: getCssClass('sl-Svg', className),
})) <ISvgProps>`
  /* stylelint-disable */
`;
