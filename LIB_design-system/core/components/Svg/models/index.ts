import type { ReactElement } from 'react';
import type { ICoreTheme } from '../../../models';

export interface ISvgProps {
  className?: string;
  theme: ICoreTheme;
  src?: string;
  element?: ReactElement;
}
