import React from 'react';
import type { SVGProps } from 'react';
import { getCssClass } from '../../../infrastructure/utils';
import type { ISvgProps } from './models';

export class OriginSvg extends React.PureComponent<ISvgProps & SVGProps<any>> {
  private _svgHtml: string;

  shouldComponentUpdate(nextProps: ISvgProps & SVGProps<any>) {
    const hasUpdates = nextProps !== this.props;

    if (hasUpdates && nextProps.src !== this.props.src) {
      this._loadSvg(nextProps.src);
    }
    return hasUpdates;
  }

  componentDidMount() {
    const { src } = this.props;

    if (src) {
      this._loadSvg(src);
    }
  }

  render() {
    const { className, src, element, ...rest } = this.props;
    const _className = getCssClass('sl-Svg', className);

    if (!src && !element) {
      return null;
    } else if (element) {
      return (
        <div className={_className} {...rest}>
          {element}
        </div>
      );
    } else if (src) {
      return (
        <div
          className={_className}
          {...rest}
          dangerouslySetInnerHTML={{ __html: this._svgHtml }}
        />
      );
    }
  }

  private _loadSvg = (url: string) => {
    const httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = () => {
      if (httpRequest.readyState == 4) {
        const { status, response } = httpRequest;

        if (status == 200) {
          const _svgHtml = response.trim();

          if (
            _svgHtml.indexOf('<svg') === 0 &&
            _svgHtml.indexOf('</svg>') === _svgHtml.length - 6
          ) {
            this._svgHtml = _svgHtml;
            this.forceUpdate();
          }
        } else if (status >= 400 || status == 0) {
          console.error(`Cannot load svg from ${url}`, httpRequest);
        }
      }
    };

    httpRequest.open('GET', url, true);
    httpRequest.send();
  };
}
